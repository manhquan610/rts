package com.cmc.rts.controller;

import com.cmc.rts.dto.request.AreaRequest;
import com.cmc.rts.dto.request.CommentRequest;
import com.cmc.rts.entity.Request;
import com.cmc.rts.entity.UserDTO;
import com.cmc.rts.helper.AreaIntegrationHelper;
import com.cmc.rts.helper.CommentIntegrationHelper;
import com.cmc.rts.helper.RequestIntegrationHelper;
import com.cmc.rts.helper.UserIntegrationHelper;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.Assertions.assertThat;

//@RunWith(SpringRunner.class)
//@AutoConfigureMockMvc
//@SpringBootTest
//@ActiveProfiles("test")
public class CommentControllerTest {
    @Autowired
    private MockMvc mvc;
    //get  all comment
//    @Test
//    public void getAllCommentSuccessWithDuLeadAccount() throws Exception{
//        Gson gson = new Gson();
//        UserDTO userDTO = logicAndGetToken("txtrung","Tt@@160820");
//        MvcResult result = CommentIntegrationHelper.getAllComment(mvc,userDTO.getToken(),371L,10,1);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void getAllCommentSuccessWithGleadAccount() throws Exception{
//        Gson gson = new Gson();
//        UserDTO userDTO = logicAndGetToken("ntnam3","MeoCon98@");
//        MvcResult result = CommentIntegrationHelper.getAllComment(mvc,userDTO.getToken(),371L,10,1);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void getAllCommentSuccessWithOtherAccount() throws Exception{
//        Gson gson = new Gson();
//        UserDTO userDTO = logicAndGetToken("datu3","Dang@nhtu1999");
//        MvcResult result = CommentIntegrationHelper.getAllComment(mvc,userDTO.getToken(),371L,10,1);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void getAllCommentSuccessWithHRAccount() throws Exception{
//        Gson gson = new Gson();
//        UserDTO userDTO = logicAndGetToken("nptien1","Qwertyuiop@");
//        MvcResult result = CommentIntegrationHelper.getAllComment(mvc,userDTO.getToken(),371L,10,1);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void getAllCommentWhenRequestIdNotExist() throws Exception{
//        Gson gson = new Gson();
//        UserDTO userDTO = logicAndGetToken("nptien1","Qwertyuiop@");
//        MvcResult result = CommentIntegrationHelper.getAllComment(mvc,userDTO.getToken(),3711L,10,1);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);//no content
//    }
//    //get comment for candidate
//    @Test
//    public void getAllCommentForCandidateSuccessWithDuLeadAccount() throws Exception{
//        Gson gson = new Gson();
//        UserDTO userDTO = logicAndGetToken("txtrung","Tt@@160820");
//        MvcResult result = CommentIntegrationHelper.getCommentForCandidate(mvc,userDTO.getToken(),371L,371L,10,1);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void getAllCommentForCandidateSuccessWithGleadAccount() throws Exception{
//        Gson gson = new Gson();
//        UserDTO userDTO = logicAndGetToken("ntnam3","MeoCon98@");
//        MvcResult result = CommentIntegrationHelper.getCommentForCandidate(mvc,userDTO.getToken(),371L,371L,10,1);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void getAllCommentForCandidateSuccessWithOtherAccount() throws Exception{
//        Gson gson = new Gson();
//        UserDTO userDTO = logicAndGetToken("datu3","Dang@nhtu1999");
//        MvcResult result = CommentIntegrationHelper.getCommentForCandidate(mvc,userDTO.getToken(),371L,371L,10,1);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void getAllCommentForCandidateSuccessWithHRAccount() throws Exception{
//        Gson gson = new Gson();
//        UserDTO userDTO = logicAndGetToken("nptien1","Qwertyuiop@");
//        MvcResult result = CommentIntegrationHelper.getCommentForCandidate(mvc,userDTO.getToken(),371L,371L,10,1);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void getAllCommentForCandidateWhenRequestIdNotExist() throws Exception{
//        Gson gson = new Gson();
//        UserDTO userDTO = logicAndGetToken("nptien1","Qwertyuiop@");
//        MvcResult result = CommentIntegrationHelper.getCommentForCandidate(mvc,userDTO.getToken(),371L,371L,10,1);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);//no content
//    }
//    //create comment
//    @Test
//    public void shouldCreateCommentSuccessWithDuleadAccount() throws Exception {
//        Gson gson = new Gson();
//        UserDTO userDTO = logicAndGetToken("txtrung","Tt@@160820");
//        CommentRequest commentRequest = new CommentRequest("Hello" +System.currentTimeMillis(),371L,371L);
//        MvcResult result = CommentIntegrationHelper.createComment(mvc,userDTO.getToken(),commentRequest);
//        assertThat(result.getResponse().getStatus()).isEqualTo(201);
//    }
//    @Test
//    public void shouldCreateCommentSuccessWithHrAccount() throws Exception {
//        Gson gson = new Gson();
//        UserDTO userDTO = logicAndGetToken("nptien1","Qwertyuiop@");
//        CommentRequest commentRequest = new CommentRequest("Hello" +System.currentTimeMillis(),371L,371L);
//        MvcResult result = CommentIntegrationHelper.createComment(mvc,userDTO.getToken(),commentRequest);
//        assertThat(result.getResponse().getStatus()).isEqualTo(201);
//    }
//    @Test
//    public void shouldCreateCommentSuccessWithOtherAccount() throws Exception {
//        Gson gson = new Gson();
//        UserDTO userDTO = logicAndGetToken("datu3","Dang@nhtu1999");
//        CommentRequest commentRequest = new CommentRequest("Hello" +System.currentTimeMillis(),371L,371L);
//        MvcResult result = CommentIntegrationHelper.createComment(mvc,userDTO.getToken(),commentRequest);
//        assertThat(result.getResponse().getStatus()).isEqualTo(201);
//    }
//
//
//    public UserDTO logicAndGetToken(String username, String password) throws Exception{
//        MvcResult user = UserIntegrationHelper.loginWithDuLeadAccount(mvc, username, password);
//        Gson gson = new Gson();
//        UserDTO userDto = gson.fromJson(user.getResponse().getContentAsString(), UserDTO.class);
//        return userDto;
//    }
}
