package com.cmc.rts.helper;

import com.cmc.rts.dto.DeleteDTO;
import com.cmc.rts.dto.request.AreaRequest;
import com.google.gson.Gson;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
public class AreaIntegrationHelper {
    public static MvcResult createArea(MockMvc mvc, AreaRequest areaRequest) throws Exception {
        Gson gson = new Gson();
        String request = gson.toJson(areaRequest);
        return mvc.perform(post("/createArea")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andReturn();
    }
    public static MvcResult updateArea(MockMvc mvc, AreaRequest areaRequest) throws Exception {
        Gson gson = new Gson();
        String request = gson.toJson(areaRequest);
        return mvc.perform(put("/updateArea")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andReturn();
    }
    public static MvcResult getAreaExact(MockMvc mvc,Integer limit, Integer offset, String searchName) throws Exception{
        String url ="/manager-area/exact?limit="+limit+"&nameSearch="+searchName+"&page="+offset;
        return mvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
    }
    public static MvcResult deleteArea(MockMvc mvc, DeleteDTO deleteDTO) throws Exception{
        Gson gson = new Gson();
        String request = gson.toJson(deleteDTO);
        return mvc.perform(post("/deleteAreas")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andReturn();
    }
}
