package com.cmc.rts.controller;

import com.cmc.rts.dto.DeleteDTO;
import com.cmc.rts.dto.request.ListNameRequest;
import com.cmc.rts.entity.ManagerExperience;
import com.cmc.rts.helper.ExperienceIntegrationHelper;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class ManagerExperienceControllerTest {
    @Autowired
    private MockMvc mvc;

    //create
    @Test
    public void shouldCreateExperienceSuccessWithUniqueName() throws Exception {
        ManagerExperience managerExperience = new ManagerExperience("Experience" + System.currentTimeMillis(), "unittest");
        MvcResult result = ExperienceIntegrationHelper.createExperience(mvc, managerExperience);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    public void shouldCreateExperienceSuccessWithNameLikeDescription() throws Exception {
        ManagerExperience managerExperience = new ManagerExperience("Experience" + System.currentTimeMillis(), "Experience" + System.currentTimeMillis());
        MvcResult result = ExperienceIntegrationHelper.createExperience(mvc, managerExperience);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }

    //get
    @Test
    public void getExperienceSuccessWithExactName() throws Exception {
        ListNameRequest listNameRequest = new ListNameRequest();
        List<String> name = new ArrayList<>();
        name.add("junior");
        listNameRequest.setLstName(name);
        listNameRequest.setLimit(5);
        listNameRequest.setOffset(1);
        MvcResult result = ExperienceIntegrationHelper.getExperienceExact(mvc, listNameRequest);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    public void getExperienceSuccessWithExactNameReturnSizeObjectResponse() throws Exception {
        Gson gson = new Gson();
        ListNameRequest listNameRequest = new ListNameRequest();
        List<String> name = new ArrayList<>();
        name.add("fresher");
        name.add("junior");
        listNameRequest.setLstName(name);
        listNameRequest.setLimit(5);
        listNameRequest.setOffset(1);
        MvcResult result = ExperienceIntegrationHelper.getExperienceExact(mvc, listNameRequest);
        Object object = gson.fromJson(result.getResponse().getContentAsString(), Object.class);
        ArrayList list = (ArrayList) ((LinkedTreeMap) object).get("manager");
        assertThat(list).isNotNull();
    }

    @Test
    public void getExperienceSuccessWithExactNameWithEmptyName() throws Exception {
        ListNameRequest listNameRequest = new ListNameRequest();
        List<String> name = new ArrayList<>();
        listNameRequest.setLstName(name);
        listNameRequest.setLimit(5);
        listNameRequest.setOffset(1);
        MvcResult result = ExperienceIntegrationHelper.getExperienceExact(mvc, listNameRequest);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    public void getExperienceWithNotExactName() throws Exception {
        Gson gson = new Gson();
        ListNameRequest listNameRequest = new ListNameRequest();
        List<String> name = new ArrayList<>();
        name.add("Ba");
        listNameRequest.setLstName(name);
        listNameRequest.setLimit(5);
        listNameRequest.setOffset(1);
        MvcResult result = ExperienceIntegrationHelper.getExperienceExact(mvc, listNameRequest);
        Object object = gson.fromJson(result.getResponse().getContentAsString(), Object.class);
        ArrayList list = (ArrayList) ((LinkedTreeMap) object).get("manager");
        assertThat(list.size()).isEqualTo(0);
    }

    //update
    @Test
    public void shouldUpdateExperienceSuccessWithUniqueName() throws Exception {
        ManagerExperience managerExperience = new ManagerExperience("Experience" + System.currentTimeMillis() +System.currentTimeMillis(), "unittest");
        MvcResult result = ExperienceIntegrationHelper.updateExperience(mvc, managerExperience);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    public void shouldUpdateExperienceSuccessWithNameLikeDescription() throws Exception {
        ManagerExperience managerExperience = new ManagerExperience(1L, "unittest" + System.currentTimeMillis(), "unittest"+ System.currentTimeMillis());
        MvcResult result = ExperienceIntegrationHelper.updateExperience(mvc, managerExperience);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    //delete
    @Test
    public void shouldDeleteOneExperienceSuccess() throws Exception {
        Gson gson = new Gson();
        ManagerExperience ExperienceRequest = new ManagerExperience("Experience" + System.currentTimeMillis(), "unittest");
        ManagerExperience managerExperience = gson.fromJson(createExperience(ExperienceRequest).getResponse().getContentAsString(),ManagerExperience.class);
        System.out.println("Day laaaaaaaaa:" +managerExperience.getId());
        DeleteDTO dto = new DeleteDTO();
        Long[] ids = {managerExperience.getId()};
        dto.setIds(ids);
        MvcResult result = ExperienceIntegrationHelper.deleteExperience(mvc,dto);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    @Test
    public void shouldDeleteManyExperienceSuccess() throws Exception {
        Gson gson = new Gson();
        ManagerExperience ExperienceRequest = new ManagerExperience("Experience" + System.currentTimeMillis(), "unittest");
        ManagerExperience ExperienceRequest1 = new ManagerExperience("Experience1" + System.currentTimeMillis(), "unittest");
        ManagerExperience managerExperience1 = gson.fromJson(createExperience(ExperienceRequest).getResponse().getContentAsString(),ManagerExperience.class);
        ManagerExperience managerExperience2 = gson.fromJson(createExperience(ExperienceRequest1).getResponse().getContentAsString(),ManagerExperience.class);
        System.out.println("Day laaaaaaaaa:" +managerExperience1.getId() + "," + managerExperience2.getId());
        DeleteDTO dto = new DeleteDTO();
        Long[] ids = {managerExperience1.getId(),managerExperience2.getId()};
        dto.setIds(ids);
        MvcResult result = ExperienceIntegrationHelper.deleteExperience(mvc,dto);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    public MvcResult createExperience(ManagerExperience ExperienceRequest) throws Exception{
        return ExperienceIntegrationHelper.createExperience(mvc, ExperienceRequest);
    }
}
