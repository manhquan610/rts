package com.cmc.rts.helper;

import com.cmc.rts.dto.DeleteDTO;
import com.cmc.rts.dto.request.ListNameRequest;
import com.cmc.rts.entity.ManagerExperience;
import com.google.gson.Gson;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
public class ExperienceIntegrationHelper {
    public static MvcResult createExperience(MockMvc mvc, ManagerExperience managerExperience) throws Exception {
        Gson gson = new Gson();
        String request = gson.toJson(managerExperience);
        return mvc.perform(post("/manager-experience")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andReturn();
    }
    public static MvcResult updateExperience(MockMvc mvc, ManagerExperience managerExperience) throws Exception {
        Gson gson = new Gson();
        String request = gson.toJson(managerExperience);
        return mvc.perform(put("/manager-experience")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andReturn();
    }
    public static MvcResult getExperienceExact(MockMvc mvc, ListNameRequest listNameRequest) throws Exception{
        Gson gson = new Gson();
        String request = gson.toJson(listNameRequest);
        return mvc.perform(post("/manager-experience/exact")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andReturn();
    }
    public static MvcResult deleteExperience(MockMvc mvc, DeleteDTO deleteDTO) throws Exception{
        Gson gson = new Gson();
        String request = gson.toJson(deleteDTO);
        return mvc.perform(post("/manager-experience/delete")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andReturn();
    }
}
