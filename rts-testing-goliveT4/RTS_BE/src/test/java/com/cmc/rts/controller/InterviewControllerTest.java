package com.cmc.rts.controller;

import com.cmc.rts.dto.request.AreaRequest;
import com.cmc.rts.dto.request.InterviewRequest;
import com.cmc.rts.helper.AreaIntegrationHelper;
import com.cmc.rts.helper.InterviewIntegrationHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class InterviewControllerTest {
    @Autowired
    private MockMvc mvc;
    //create Interview
    @Test
    public void shouldCreateInterviewSuccessWithUniqueName() throws Exception {
        InterviewRequest interviewRequest = new InterviewRequest(1000L,250L,1L,"pv PM position", "2021-12-29 16:30:00","nthieu5,","duyet","datu3");
        MvcResult result = InterviewIntegrationHelper.createInterview(mvc,interviewRequest);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    //get all interview by requestId
    @Test
    public void getAllInterviewSuccessWithExistRequestId() throws Exception {
        MvcResult result = InterviewIntegrationHelper.getAllInterview(mvc,434);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    @Test
    public void getAllInterviewFailWithNoExistRequestId() throws Exception {
        MvcResult result = InterviewIntegrationHelper.getAllInterview(mvc,1000);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);//NO Response body
    }
    //get all interview by name
    //get all interview
    @Test
    public void getAllInterviewSuccessByExistName() throws Exception {
        MvcResult result = InterviewIntegrationHelper.getAllInterviewByInterviewerName(mvc,"a");
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    @Test
    public void getAllInterviewFailWithNoExistName() throws Exception {
        MvcResult result = InterviewIntegrationHelper.getAllInterviewByInterviewerName(mvc,"agcacccc");
        assertThat(result.getResponse().getStatus()).isEqualTo(200); //no response body
    }
    //get all interview by candidateId and requestId
    @Test
    public void getAllInterviewSuccessByExistCandidateIdAndRequestId() throws Exception {
        MvcResult result = InterviewIntegrationHelper.getInterviewByCandidateIdAndRequestId(mvc,434,377);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    @Test
    public void getAllInterviewFailWithNoExistCandidateIdAndRequestId() throws Exception {
        MvcResult result = InterviewIntegrationHelper.getInterviewByCandidateIdAndRequestId(mvc,0,0);
        assertThat(result.getResponse().getStatus()).isEqualTo(200); //no response body
    }
    //get all interview one week
    //get all interview
    @Test
    public void getAllInterviewOneWeekSuccess() throws Exception {
        MvcResult result = InterviewIntegrationHelper.getAllInterviewOneWeek(mvc,"2021-10-01","2021-12-01","a");
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    @Test
    public void getAllInterviewOneWeekFailWithNoExistName() throws Exception {
        MvcResult result = InterviewIntegrationHelper.getAllInterviewByInterviewerName(mvc,"agcacccc");
        assertThat(result.getResponse().getStatus()).isEqualTo(200); //no response body
    }
    public Date convertStringToDate(String dateStr) throws Exception {
        SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return formatter.parse(dateStr);
    }

}
