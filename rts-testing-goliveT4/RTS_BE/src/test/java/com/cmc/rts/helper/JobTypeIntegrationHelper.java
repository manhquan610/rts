package com.cmc.rts.helper;

import com.cmc.rts.dto.DeleteDTO;
import com.cmc.rts.dto.request.JobTypeRequest;
import com.google.gson.Gson;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
public class JobTypeIntegrationHelper {
    public static MvcResult createJobType(MockMvc mvc, JobTypeRequest JobTypeRequest) throws Exception {
        Gson gson = new Gson();
        String request = gson.toJson(JobTypeRequest);
        return mvc.perform(post("/createJobType")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andReturn();
    }
    public static MvcResult updateJobType(MockMvc mvc, JobTypeRequest JobTypeRequest) throws Exception {
        Gson gson = new Gson();
        String request = gson.toJson(JobTypeRequest);
        return mvc.perform(put("/updateJobType")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andReturn();
    }
    public static MvcResult getJobTypeExact(MockMvc mvc,Integer limit, Integer offset, String searchName) throws Exception{
        String url ="/manager-jobType/exact?limit="+limit+"&nameSearch="+searchName+"&page="+offset;
        return mvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
    }
    public static MvcResult deleteJobType(MockMvc mvc, DeleteDTO deleteDTO) throws Exception{
        Gson gson = new Gson();
        String request = gson.toJson(deleteDTO);
        return mvc.perform(post("/deleteJobTypes")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andReturn();
    }
}
