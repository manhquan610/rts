package com.cmc.rts.controller;

import com.cmc.rts.helper.ChannelIntegrationHelper;
import com.cmc.rts.helper.SkillIntegrationHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.Assertions.assertThat;
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class ChannelControllerTest {
    @Autowired
    private MockMvc mvc;
    //get get channel
    @Test
    public void getAllChannelSuccessCompareCode() throws Exception{
        MvcResult result = ChannelIntegrationHelper.getChannel(mvc);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
}
