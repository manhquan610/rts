package com.cmc.rts.helper;

import com.cmc.rts.dto.DeleteDTO;
import com.cmc.rts.dto.request.ListNameRequest;
import com.cmc.rts.entity.ManagerRequestType;
import com.google.gson.Gson;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
public class RequestTypeIntegrationHelper {
    public static MvcResult createRequestType(MockMvc mvc, ManagerRequestType managerRequestType) throws Exception {
        Gson gson = new Gson();
        String request = gson.toJson(managerRequestType);
        return mvc.perform(post("/manager-request-type")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andReturn();
    }
    public static MvcResult updateRequestType(MockMvc mvc, ManagerRequestType managerRequestType) throws Exception {
        Gson gson = new Gson();
        String request = gson.toJson(managerRequestType);
        return mvc.perform(put("/manager-request-type")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andReturn();
    }
    public static MvcResult getRequestTypeExact(MockMvc mvc, ListNameRequest listNameRequest) throws Exception{
        Gson gson = new Gson();
        String request = gson.toJson(listNameRequest);
        return mvc.perform(post("/manager-request-type/exact")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andReturn();
    }
    public static MvcResult deleteRequestType(MockMvc mvc, DeleteDTO deleteDTO) throws Exception{
        Gson gson = new Gson();
        String request = gson.toJson(deleteDTO);
        return mvc.perform(post("/manager-request-type/delete")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andReturn();
    }
}
