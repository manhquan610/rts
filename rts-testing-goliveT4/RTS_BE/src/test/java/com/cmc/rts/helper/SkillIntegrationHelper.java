package com.cmc.rts.helper;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
public class SkillIntegrationHelper {
    public static MvcResult getSkill(MockMvc mvc, String skill , Integer offset, Integer limit ) throws Exception{
        String url = "/skill-name?limit="+limit+"&name="+skill+"&offset="+offset;
        return mvc.perform(get(url)
                .header("Content-Type", "application/json")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
    }
}
