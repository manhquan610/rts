package com.cmc.rts.helper;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
public class TimelineIntegrationHelper {
    public static MvcResult getTimeline(MockMvc mvc, Long requestId, Long candidateId) throws Exception{
        String url = "/timeline?candidateId="+candidateId+"&requestId="+requestId;
        return mvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON)).andReturn();

    }

}
