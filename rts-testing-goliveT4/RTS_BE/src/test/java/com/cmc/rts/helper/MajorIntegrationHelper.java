package com.cmc.rts.helper;

import com.cmc.rts.dto.DeleteDTO;
import com.cmc.rts.dto.request.ListNameRequest;
import com.cmc.rts.entity.ManagerMajor;
import com.google.gson.Gson;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
public class MajorIntegrationHelper {
    public static MvcResult createMajor(MockMvc mvc, ManagerMajor managerMajor) throws Exception {
        Gson gson = new Gson();
        String request = gson.toJson(managerMajor);
        return mvc.perform(post("/manager-major")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andReturn();
    }
    public static MvcResult updateMajor(MockMvc mvc, ManagerMajor managerMajor) throws Exception {
        Gson gson = new Gson();
        String request = gson.toJson(managerMajor);
        return mvc.perform(put("/manager-major")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andReturn();
    }
    public static MvcResult getMajorExact(MockMvc mvc, ListNameRequest listNameRequest) throws Exception{
        Gson gson = new Gson();
        String request = gson.toJson(listNameRequest);
        return mvc.perform(post("/manager-major/exact")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andReturn();
    }
    public static MvcResult deleteMajor(MockMvc mvc, DeleteDTO deleteDTO) throws Exception{
        Gson gson = new Gson();
        String request = gson.toJson(deleteDTO);
        return mvc.perform(post("/manager-major/delete")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andReturn();
    }
}
