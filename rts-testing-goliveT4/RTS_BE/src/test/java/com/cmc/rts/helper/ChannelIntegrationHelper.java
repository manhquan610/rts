package com.cmc.rts.helper;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class ChannelIntegrationHelper {
    public static MvcResult getChannel(MockMvc mvc) throws Exception{
        return mvc.perform(get("/getChannel")
                .header("Content-Type", "application/json")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
    }
}
