package com.cmc.rts.helper;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
public class LanguageIntegrationHelper {
    public static MvcResult getLanguage(MockMvc mvc, String language ,Integer offset, Integer limit ) throws Exception{
        String url = "/language?limit="+limit+"&name="+language+"&offset="+offset;
        return mvc.perform(get(url)
                .header("Content-Type", "application/json")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
    }
}
