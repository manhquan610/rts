package com.cmc.rts.controller;

import com.cmc.rts.dto.request.CVRequest;
import com.cmc.rts.entity.CandidateStateEnum;
import com.cmc.rts.helper.CVIntegrationHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.Assertions.assertThat;

//@RunWith(SpringRunner.class)
//@AutoConfigureMockMvc
//@SpringBootTest
//@ActiveProfiles("test")
public class CVControllerTest {
//    @Autowired
//    private MockMvc mvc;
//
//    @Test
//    public void givenCV_whenGetCV_thenStatus200AndInputCandidateEnumIsNone() throws Exception{
//        MvcResult result = CVIntegrationHelper.getAllCV(mvc,CandidateStateEnum.none,"",10,1);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//
//    @Test
//    public void givenCV_whenGetCV_thenStatus200AndSearchInput() throws Exception{
//        MvcResult result = CVIntegrationHelper.getAllCV(mvc,CandidateStateEnum.none,"aa",10,1);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//
//    @Test
//    public void givenCV_whenGetCV_thenStatus200AndInputCandidateEnumIsOther() throws Exception{
//        MvcResult result = CVIntegrationHelper.getAllCV(mvc,CandidateStateEnum.interview,"",10,1);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//
//    @Test
//    public void shouldCreateCVBeConflict() throws Exception{
//        CVRequest cvRequest = new CVRequest("Dang Anh Tu","tudahe12@gmail.com","0928282727","dvsdvdvsdvlink",16L,"26-11-2021","Dev","464");
//        MvcResult result = CVIntegrationHelper.createCV(mvc,cvRequest);
//        assertThat(result.getResponse().getStatus()).isEqualTo(409);
//    }
//
//    @Test
//    public void shouldCreateCVFailed() throws Exception{
//        CVRequest cvRequest = new CVRequest("Dang Anh Tu","tudahe12@gmail.com","0928282727","dvsdvdvsdvlink",16L,"SSSSS","Dev","464");
//        MvcResult result = CVIntegrationHelper.createCV(mvc,cvRequest);
//        assertThat(result.getResponse().getStatus()).isEqualTo(500);
//    }
//
//    @Test
//    public void shouldCreateCVSuccessWhenEmailIsUnique() throws Exception{
//        CVRequest cvRequest = new CVRequest("Dang Anh Tu","tudahe12@gmail.com" +System.currentTimeMillis(),"0928282727","dvsdvdvsdvlink",16L,"26-11-2021","Dev","464");
//        MvcResult result = CVIntegrationHelper.createCV(mvc,cvRequest);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void shouldCreateCVSuccess() throws Exception{
//        CVRequest cvRequest = new CVRequest("Dang Anh Tu","tudahe12@gmail.com" +System.currentTimeMillis(),""+System.currentTimeMillis(),"dvsdvdvsdvlink",16L,"26-11-2021","Dev","464");
//        MvcResult result = CVIntegrationHelper.createCV(mvc,cvRequest);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void shouldUpdateCVSuccess() throws Exception{
//        CVRequest cvRequest = new CVRequest(2942L,"Dang Anh Tu","",""+System.currentTimeMillis(),"dvsdvdvsdvlink",16L,"26-11-2021","Dev","464");
//        MvcResult result = CVIntegrationHelper.updateCV(mvc,cvRequest);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void shouldUpdateCVWhenEmailBeChangeThenSuccess() throws Exception{
//        CVRequest cvRequest = new CVRequest(2942L,"Dang Anh Tu","thuymtt@gmail.com",""+System.currentTimeMillis(),"dvsdvdvsdvlink",16L,"26-11-2021","Dev","464");
//        MvcResult result = CVIntegrationHelper.updateCV(mvc,cvRequest);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void shouldUpdateCVFailed() throws Exception{
//        CVRequest cvRequest = new CVRequest(2942L,"Dang Anh Tu","",""+System.currentTimeMillis(),"dvsdvdvsdvlink",16L,"cscscss","Dev","464");
//        MvcResult result = CVIntegrationHelper.updateCV(mvc,cvRequest);
//        assertThat(result.getResponse().getStatus()).isEqualTo(500);
//    }
//    @Test
//    public void shouldUpdateCVWhenIdNotExistThenFailed() throws Exception{
//        CVRequest cvRequest = new CVRequest(115L,"Dang Anh Tu","",""+System.currentTimeMillis(),"dvsdvdvsdvlink",16L,"26-11-2021","Dev","464");
//        MvcResult result = CVIntegrationHelper.updateCV(mvc,cvRequest);
//        assertThat(result.getResponse().getStatus()).isEqualTo(500);
//    }
}
