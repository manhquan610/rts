package com.cmc.rts.controller;

import com.cmc.rts.entity.UserAuthen;
import com.cmc.rts.entity.UserDTO;
import com.cmc.rts.helper.UserIntegrationHelper;
import com.cmc.rts.rest.UserController;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class UserControllerTest {
    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserController userController;

    @Autowired
    private RestTemplate restTemplate;

    private MockRestServiceServer mockServer;

    @Before
    public void init() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
    }

    @Test
    public void shouldLoginSuccessWithDuLeadAccount() throws Exception {
//        String username = "txtrung";
//        String password = "Tt@@160820";
//        MvcResult result = UserIntegrationHelper.loginWithDuLeadAccount(mvc, username, password);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//        UserDTO userDto = objectMapper.readValue(result.getResponse().getContentAsByteArray(), UserDTO.class);
//        assertEquals(username, userDto.getUserName());
    }

    @Test
    public void userClientSuccessfullyReturnsUser() throws Exception {
        UserAuthen userAuthen = new UserAuthen("dulead", "dulead2021", "", "");
        mockServer.expect(ExpectedCount.once(),
                requestTo(new URI("/authen/login")))
                .andExpect(method(HttpMethod.POST))
                .andRespond(
                        withStatus(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(objectMapper.writeValueAsString(userAuthen))
                );

//        this.mockServer
//                .expect(requestTo("/authen/login"))
//                .andRespond(withSuccess(json, MediaType.APPLICATION_JSON));

//        User result = userClient.getSingleUser(1L);
//
//        assertNotNull(result);
    }

}
