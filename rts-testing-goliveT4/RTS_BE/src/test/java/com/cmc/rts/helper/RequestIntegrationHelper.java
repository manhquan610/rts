package com.cmc.rts.helper;

import com.cmc.rts.dto.request.CreateAssignRequest;
import com.cmc.rts.dto.requestDto.RequestDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.Gson;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

public class RequestIntegrationHelper {
    public static MvcResult createRequestWithDuLeadAccount(MockMvc mvc, String token, RequestDTO requestDTO) throws Exception {
        Gson gson = new Gson();
//        String request = "{\"title\":\"DUKEAAád4\",\"positionId\":136,\"deadline\":\"2021-11-08\",\"number\":\"123\",\"description\":\"<p>1231</p>\",\"skillId\":\"3\",\"priorityId\":51,\"experienceId\":36,\"certificate\":\"\",\"major\":null,\"salary\":\"\",\"benefit\":\"\",\"languageId\":\"\",\"requestTypeId\":24,\"projectId\":5,\"managerGroupId\":44,\"departmentId\":51,\"area\":{\"id\":3,\"name\":\"Đà Nẵng\",\"description\":\"\",\"deleted\":false},\"jobType\":{\"id\":15,\"name\":\"Leader\",\"description\":null,\"deleted\":false},\"workingType\":\"FullTime\"}";
        String request = gson.toJson(requestDTO);
        return mvc.perform(post("/request")
                .header("Authorization", "Bearer " + token)
                .header("Content-Type", "application/json")
                .contentType(MediaType.APPLICATION_JSON).content(request)).andReturn();
    }
    public static MvcResult updateRequestWithDuLeadAccount(MockMvc mvc, String token, RequestDTO requestDTO, Long id) throws Exception {
        Gson gson = new Gson();
        String request = gson.toJson(requestDTO);
        String url = "/request/"+id;
        return mvc.perform(post(url)
                .header("Authorization", "Bearer " + token)
                .header("Content-Type", "application/json")
                .contentType(MediaType.APPLICATION_JSON).content(request)).andReturn();
    }
    public static MvcResult getRequest(MockMvc mvc,String token,String name, Integer limit, Integer offset) throws Exception{
       String url = "/request?limit="+limit+"10&name="+name+"&offset="+offset;
        return mvc.perform(get(url)
                .header("Authorization", "Bearer " + token)
                .header("Content-Type", "application/json")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
    }
    public static MvcResult getGlead(MockMvc mvc,String token,String name) throws Exception{
        String url = "/get-glead?username="+name;
        return mvc.perform(get(url)
                .header("Authorization", "Bearer " + token)
                .header("Content-Type", "application/json")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
    }
    public static MvcResult getHrMember(MockMvc mvc) throws Exception{
        return mvc.perform(get("/hr-member")
                .header("Content-Type", "application/json")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
    }
    public static MvcResult updateShareJob(MockMvc mvc,Long id) throws Exception{
        String url = "/request/"+id;
        return mvc.perform(put(url)
                .header("Content-Type", "application/json")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
    }
    public static MvcResult createRequestAssign(MockMvc mvc, String token,List<CreateAssignRequest> listRequestAssign, Long id) throws Exception{
        String url = "/request/"+id+"/assign";
        Gson gson = new Gson();
        String request = gson.toJson(listRequestAssign);
        return mvc.perform(post(url)
                .header("Authorization", "Bearer " + token)
                .header("Content-Type", "application/json")
                .contentType(MediaType.APPLICATION_JSON).content(request)).andReturn();
    }
    public static MvcResult getRequestById(MockMvc mvc,Long id) throws Exception{
        String url = "/request/"+id;
        return mvc.perform(get(url)
                .header("Content-Type", "application/json")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
    }
    public static MvcResult submitRequest(MockMvc mvc, String token, String id, String receiver) throws Exception{
        String url = "/request-submit/"+id+"?receiver="+receiver;
        return mvc.perform(get(url)
                .header("Authorization", "Bearer " + token)
                .header("Content-Type", "application/json")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
    }
    public static MvcResult approveRequest(MockMvc mvc, String token, String id) throws Exception{
        String url = "/request-approve/"+id;
        return mvc.perform(get(url)
                .header("Authorization", "Bearer " + token)
                .header("Content-Type", "application/json")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
    }
    public static MvcResult rejectRequest(MockMvc mvc, String token, String id,String reason) throws Exception{
        String url = "/request-reject/"+id+"?reason="+reason;
        return mvc.perform(get(url)
                .header("Authorization", "Bearer " + token)
                .header("Content-Type", "application/json")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
    }
    public static MvcResult closeRequest(MockMvc mvc, String token, String id) throws Exception{
        String url = "/request-close/"+id;
        return mvc.perform(get(url)
                .header("Authorization", "Bearer " + token)
                .header("Content-Type", "application/json")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
    }
}
