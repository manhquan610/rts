package com.cmc.rts.controller;

import com.cmc.rts.dto.DeleteDTO;
import com.cmc.rts.dto.request.JobTypeRequest;
import com.cmc.rts.entity.ManagerJobType;
import com.cmc.rts.helper.JobTypeIntegrationHelper;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class ManagerJobTypeControllerTest {
    @Autowired
    private MockMvc mvc;

    //create
    @Test
    public void shouldCreateJobTypeSuccessWithUniqueName() throws Exception {
        JobTypeRequest jobTypeRequest = new JobTypeRequest("JobType" + System.currentTimeMillis(), "unittest");
        MvcResult result = JobTypeIntegrationHelper.createJobType(mvc, jobTypeRequest);
        assertThat(result.getResponse().getStatus()).isEqualTo(201);
    }

    @Test
    public void shouldCreateJobTypeSuccessWithNameLikeDescription() throws Exception {
        JobTypeRequest JobTypeRequest = new JobTypeRequest("JobType" + System.currentTimeMillis()+"like", "JobType" + System.currentTimeMillis());
        MvcResult result = JobTypeIntegrationHelper.createJobType(mvc, JobTypeRequest);
        assertThat(result.getResponse().getStatus()).isEqualTo(201);
    }

    //get
    @Test
    public void getJobTypeSuccessWithExactName() throws Exception {
        MvcResult result = JobTypeIntegrationHelper.getJobTypeExact(mvc, 5, 1, "Developer");
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    public void getJobTypeSuccessWithExactNameReturnSizeObjectResponse() throws Exception {
        Gson gson = new Gson();
        MvcResult result = JobTypeIntegrationHelper.getJobTypeExact(mvc, 5, 1, "Marketing,Project Management");
        Object object = gson.fromJson(result.getResponse().getContentAsString(), Object.class);
        ArrayList list = (ArrayList) ((LinkedTreeMap) object).get("content");
        assertThat(list).isNotNull();
    }

    @Test
    public void getJobTypeSuccessWithExactNameWithEmptyName() throws Exception {
        Gson gson = new Gson();
        MvcResult result = JobTypeIntegrationHelper.getJobTypeExact(mvc, 5, 1, "");
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    public void getJobTypeWithNotExactName() throws Exception {
        Gson gson = new Gson();
        MvcResult result = JobTypeIntegrationHelper.getJobTypeExact(mvc, 5, 1, "an");
        Object object = gson.fromJson(result.getResponse().getContentAsString(), Object.class);
        ArrayList list = (ArrayList) ((LinkedTreeMap) object).get("content");
        assertThat(list.size()).isEqualTo(0);
    }

    //update
    @Test
    public void shouldUpdateJobTypeSuccessWithUniqueName() throws Exception {
        JobTypeRequest jobTypeRequest = new JobTypeRequest(1L, "JobType" + System.currentTimeMillis(), "unittest");
        MvcResult result = JobTypeIntegrationHelper.updateJobType(mvc, jobTypeRequest);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    public void shouldUpdateJobTypeSuccessWithNameLikeDescription() throws Exception {
        JobTypeRequest jobTypeRequest = new JobTypeRequest(1L, "unittest" + System.currentTimeMillis()+"like", "unittest"+ System.currentTimeMillis()+"like");
        MvcResult result = JobTypeIntegrationHelper.updateJobType(mvc, jobTypeRequest);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    //delete
    @Test
    public void shouldDeleteOneJobTypeSuccess() throws Exception {
        Gson gson = new Gson();
        JobTypeRequest jobTypeRequest = new JobTypeRequest("JobType" + System.currentTimeMillis(), "unittest");
        ManagerJobType managerJobType = gson.fromJson(createJobType(jobTypeRequest).getResponse().getContentAsString(),ManagerJobType.class);
        System.out.println("Day laaaaaaaaa:" +managerJobType.getId());
        DeleteDTO dto = new DeleteDTO();
        Long[] ids = {managerJobType.getId()};
        dto.setIds(ids);
        MvcResult result = JobTypeIntegrationHelper.deleteJobType(mvc,dto);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    @Test
    public void shouldDeleteManyJobTypeSuccess() throws Exception {
        Gson gson = new Gson();
        JobTypeRequest jobTypeRequest = new JobTypeRequest("JobType" + System.currentTimeMillis()+"test", "unittest");
        JobTypeRequest jobTypeRequest1 = new JobTypeRequest("JobType1" + System.currentTimeMillis(), "unittest");
        ManagerJobType managerJobType = gson.fromJson(createJobType(jobTypeRequest).getResponse().getContentAsString(),ManagerJobType.class);
        ManagerJobType managerJobType2 = gson.fromJson(createJobType(jobTypeRequest1).getResponse().getContentAsString(),ManagerJobType.class);
        DeleteDTO dto = new DeleteDTO();
        Long[] ids = {managerJobType.getId(),managerJobType2.getId()};
        dto.setIds(ids);
        MvcResult result = JobTypeIntegrationHelper.deleteJobType(mvc,dto);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    public MvcResult createJobType(JobTypeRequest JobTypeRequest) throws Exception{
        return JobTypeIntegrationHelper.createJobType(mvc, JobTypeRequest);
    }
}
