package com.cmc.rts.controller;

import com.cmc.rts.helper.GroupIntegrationHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class ManagerGroupControllerTest {
    @Autowired
    private MockMvc mvc;

    @Test
    public void getAllGroupSuccessWithEmptyName() throws Exception{
        MvcResult result = GroupIntegrationHelper.getAllGroup(mvc,"",5,1);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    @Test
    public void getAllGroupSuccessWithNameContainCharacter() throws Exception{
        MvcResult result = GroupIntegrationHelper.getAllGroup(mvc,"a",5,1);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    @Test
    public void getAllGroupSuccessNoExistName() throws Exception{
        MvcResult result = GroupIntegrationHelper.getAllGroup(mvc,"!cscs",5,1);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
}
