package com.cmc.rts.controller;

import com.cmc.rts.dto.DeleteDTO;
import com.cmc.rts.dto.request.ListNameRequest;
import com.cmc.rts.entity.ManagerPriority;
import com.cmc.rts.helper.PriorityIntegrationHelper;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class ManagerPriorityControllerTest {
    @Autowired
    private MockMvc mvc;

    //create
    @Test
    public void shouldCreatePrioritySuccessWithUniqueName() throws Exception {
        ManagerPriority managerPriority = new ManagerPriority("Priority" + System.currentTimeMillis(), "unittest");
        MvcResult result = PriorityIntegrationHelper.createPriority(mvc, managerPriority);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    public void shouldCreatePrioritySuccessWithNameLikeDescription() throws Exception {
        ManagerPriority managerPriority = new ManagerPriority("Priority" + System.currentTimeMillis()+ System.currentTimeMillis(), "Priority" + System.currentTimeMillis());
        MvcResult result = PriorityIntegrationHelper.createPriority(mvc, managerPriority);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }

    //get
    @Test
    public void getPrioritySuccessWithExactName() throws Exception {
        ListNameRequest listNameRequest = new ListNameRequest();
        List<String> name = new ArrayList<>();
        name.add("medium");
        listNameRequest.setLstName(name);
        listNameRequest.setLimit(5);
        listNameRequest.setOffset(1);
        MvcResult result = PriorityIntegrationHelper.getPriorityExact(mvc, listNameRequest);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    public void getPrioritySuccessWithExactNameReturnSizeObjectResponse() throws Exception {
        Gson gson = new Gson();
        ListNameRequest listNameRequest = new ListNameRequest();
        List<String> name = new ArrayList<>();
        name.add("high");
        name.add("medium");
        listNameRequest.setLstName(name);
        listNameRequest.setLimit(5);
        listNameRequest.setOffset(1);
        MvcResult result = PriorityIntegrationHelper.getPriorityExact(mvc, listNameRequest);
        Object object = gson.fromJson(result.getResponse().getContentAsString(), Object.class);
        ArrayList list = (ArrayList) ((LinkedTreeMap) object).get("manager");
        assertThat(list.size()).isGreaterThan(0);
    }

    @Test
    public void getPrioritySuccessWithExactNameWithEmptyName() throws Exception {
        ListNameRequest listNameRequest = new ListNameRequest();
        List<String> name = new ArrayList<>();
        listNameRequest.setLstName(name);
        listNameRequest.setLimit(5);
        listNameRequest.setOffset(1);
        MvcResult result = PriorityIntegrationHelper.getPriorityExact(mvc, listNameRequest);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    public void getPriorityWithNotExactName() throws Exception {
        Gson gson = new Gson();
        ListNameRequest listNameRequest = new ListNameRequest();
        List<String> name = new ArrayList<>();
        name.add("Ba");
        listNameRequest.setLstName(name);
        listNameRequest.setLimit(5);
        listNameRequest.setOffset(1);
        MvcResult result = PriorityIntegrationHelper.getPriorityExact(mvc, listNameRequest);
        Object object = gson.fromJson(result.getResponse().getContentAsString(), Object.class);
        ArrayList list = (ArrayList) ((LinkedTreeMap) object).get("manager");
        assertThat(list).isNotNull();
    }

    //update
    @Test
    public void shouldUpdatePrioritySuccessWithUniqueName() throws Exception {
        ManagerPriority managerPriority = new ManagerPriority("Priority" + System.currentTimeMillis(), "unittest");
        MvcResult result = PriorityIntegrationHelper.updatePriority(mvc, managerPriority);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    public void shouldUpdatePrioritySuccessWithNameLikeDescription() throws Exception {
        ManagerPriority managerPriority = new ManagerPriority(1L, "unittest" + System.currentTimeMillis(), "unittest"+ System.currentTimeMillis());
        MvcResult result = PriorityIntegrationHelper.updatePriority(mvc, managerPriority);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    //delete
    @Test
    public void shouldDeleteOnePrioritySuccess() throws Exception {
        Gson gson = new Gson();
        ManagerPriority PriorityRequest = new ManagerPriority("Priority" + System.currentTimeMillis(), "unittest");
        ManagerPriority managerPriority = gson.fromJson(createPriority(PriorityRequest).getResponse().getContentAsString(),ManagerPriority.class);
        System.out.println("Day laaaaaaaaa:" +managerPriority.getId());
        DeleteDTO dto = new DeleteDTO();
        Long[] ids = {managerPriority.getId()};
        dto.setIds(ids);
        MvcResult result = PriorityIntegrationHelper.deletePriority(mvc,dto);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    @Test
    public void shouldDeleteManyPrioritySuccess() throws Exception {
        Gson gson = new Gson();
        ManagerPriority PriorityRequest = new ManagerPriority("Priority" + System.currentTimeMillis(), "unittest");
        ManagerPriority PriorityRequest1 = new ManagerPriority("Priority1" + System.currentTimeMillis(), "unittest");
        ManagerPriority managerPriority1 = gson.fromJson(createPriority(PriorityRequest).getResponse().getContentAsString(),ManagerPriority.class);
        ManagerPriority managerPriority2 = gson.fromJson(createPriority(PriorityRequest1).getResponse().getContentAsString(),ManagerPriority.class);
        System.out.println("Day laaaaaaaaa:" +managerPriority1.getId() + "," + managerPriority2.getId());
        DeleteDTO dto = new DeleteDTO();
        Long[] ids = {managerPriority1.getId(),managerPriority2.getId()};
        dto.setIds(ids);
        MvcResult result = PriorityIntegrationHelper.deletePriority(mvc,dto);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    public MvcResult createPriority(ManagerPriority PriorityRequest) throws Exception{
        return PriorityIntegrationHelper.createPriority(mvc, PriorityRequest);
    }
}
