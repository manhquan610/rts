package com.cmc.rts.helper;

import com.cmc.rts.dto.DeleteDTO;
import com.cmc.rts.dto.request.ListNameRequest;
import com.cmc.rts.entity.ManagerPriority;
import com.google.gson.Gson;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
public class PriorityIntegrationHelper {
    public static MvcResult createPriority(MockMvc mvc, ManagerPriority managerPriority) throws Exception {
        Gson gson = new Gson();
        String request = gson.toJson(managerPriority);
        return mvc.perform(post("/manager-priority")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andReturn();
    }
    public static MvcResult updatePriority(MockMvc mvc, ManagerPriority managerPriority) throws Exception {
        Gson gson = new Gson();
        String request = gson.toJson(managerPriority);
        return mvc.perform(put("/manager-priority")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andReturn();
    }
    public static MvcResult getPriorityExact(MockMvc mvc, ListNameRequest listNameRequest) throws Exception{
        Gson gson = new Gson();
        String request = gson.toJson(listNameRequest);
        return mvc.perform(post("/manager-priority/exact")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andReturn();
    }
    public static MvcResult deletePriority(MockMvc mvc, DeleteDTO deleteDTO) throws Exception{
        Gson gson = new Gson();
        String request = gson.toJson(deleteDTO);
        return mvc.perform(post("/manager-priority/delete")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andReturn();
    }
}
