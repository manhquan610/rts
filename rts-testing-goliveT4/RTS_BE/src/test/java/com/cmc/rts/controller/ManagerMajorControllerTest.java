package com.cmc.rts.controller;

import com.cmc.rts.dto.DeleteDTO;
import com.cmc.rts.dto.request.ListNameRequest;
import com.cmc.rts.entity.ManagerMajor;
import com.cmc.rts.helper.MajorIntegrationHelper;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class ManagerMajorControllerTest {
    @Autowired
    private MockMvc mvc;

    //create
    @Test
    public void shouldCreateMajorSuccessWithUniqueName() throws Exception {
        ManagerMajor managerMajor = new ManagerMajor("Major" + System.currentTimeMillis()+"a", "unittest");
        MvcResult result = MajorIntegrationHelper.createMajor(mvc, managerMajor);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    public void shouldCreateMajorSuccessWithNameLikeDescription() throws Exception {
        ManagerMajor managerMajor = new ManagerMajor("Major" + System.currentTimeMillis() +System.currentTimeMillis()+"b", "Major" + System.currentTimeMillis());
        MvcResult result = MajorIntegrationHelper.createMajor(mvc, managerMajor);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }

    //get
    @Test
    public void getMajorSuccessWithExactName() throws Exception {
        ListNameRequest listNameRequest = new ListNameRequest();
        List<String> name = new ArrayList<>();
        name.add("Marketing");
        listNameRequest.setLstName(name);
        listNameRequest.setLimit(5);
        listNameRequest.setOffset(1);
        MvcResult result = MajorIntegrationHelper.getMajorExact(mvc, listNameRequest);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    public void getMajorSuccessWithExactNameReturnSizeObjectResponse() throws Exception {
        Gson gson = new Gson();
        ListNameRequest listNameRequest = new ListNameRequest();
        List<String> name = new ArrayList<>();
        name.add("Dev PHP");
        name.add("Quản trị nhân lực");
        listNameRequest.setLstName(name);
        listNameRequest.setLimit(5);
        listNameRequest.setOffset(1);
        MvcResult result = MajorIntegrationHelper.getMajorExact(mvc, listNameRequest);
        Object object = gson.fromJson(result.getResponse().getContentAsString(), Object.class);
        ArrayList list = (ArrayList) ((LinkedTreeMap) object).get("manager");
        assertThat(list).isNotNull();
    }

    @Test
    public void getMajorSuccessWithExactNameWithEmptyName() throws Exception {
        ListNameRequest listNameRequest = new ListNameRequest();
        List<String> name = new ArrayList<>();
        listNameRequest.setLstName(name);
        listNameRequest.setLimit(5);
        listNameRequest.setOffset(1);
        MvcResult result = MajorIntegrationHelper.getMajorExact(mvc, listNameRequest);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    public void getMajorWithNotExactName() throws Exception {
        Gson gson = new Gson();
        ListNameRequest listNameRequest = new ListNameRequest();
        List<String> name = new ArrayList<>();
        name.add("Ba");
        listNameRequest.setLstName(name);
        listNameRequest.setLimit(5);
        listNameRequest.setOffset(1);
        MvcResult result = MajorIntegrationHelper.getMajorExact(mvc, listNameRequest);
        Object object = gson.fromJson(result.getResponse().getContentAsString(), Object.class);
        ArrayList list = (ArrayList) ((LinkedTreeMap) object).get("manager");
        assertThat(list.size()).isEqualTo(0);
    }

    //update
    @Test
    public void shouldUpdateMajorSuccessWithUniqueName() throws Exception {
        ManagerMajor managerMajor = new ManagerMajor("Major" + System.currentTimeMillis(), "unittest");
        MvcResult result = MajorIntegrationHelper.updateMajor(mvc, managerMajor);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    public void shouldUpdateMajorSuccessWithNameLikeDescription() throws Exception {
        ManagerMajor managerMajor = new ManagerMajor(1L, "unittest" + System.currentTimeMillis(), "unittest"+ System.currentTimeMillis());
        MvcResult result = MajorIntegrationHelper.updateMajor(mvc, managerMajor);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    //delete
    @Test
    public void shouldDeleteOneMajorSuccess() throws Exception {
        Gson gson = new Gson();
        ManagerMajor MajorRequest = new ManagerMajor("Major" + System.currentTimeMillis(), "unittest");
        ManagerMajor managerMajor = gson.fromJson(createMajor(MajorRequest).getResponse().getContentAsString(),ManagerMajor.class);
        System.out.println("Day laaaaaaaaa:" +managerMajor.getId());
        DeleteDTO dto = new DeleteDTO();
        Long[] ids = {managerMajor.getId()};
        dto.setIds(ids);
        MvcResult result = MajorIntegrationHelper.deleteMajor(mvc,dto);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    @Test
    public void shouldDeleteManyMajorSuccess() throws Exception {
        Gson gson = new Gson();
        ManagerMajor MajorRequest = new ManagerMajor("Major" + System.currentTimeMillis(), "unittest");
        ManagerMajor MajorRequest1 = new ManagerMajor("Major1" + System.currentTimeMillis(), "unittest");
        ManagerMajor managerMajor1 = gson.fromJson(createMajor(MajorRequest).getResponse().getContentAsString(),ManagerMajor.class);
        ManagerMajor managerMajor2 = gson.fromJson(createMajor(MajorRequest1).getResponse().getContentAsString(),ManagerMajor.class);
        System.out.println("Day laaaaaaaaa:" +managerMajor1.getId() + "," + managerMajor2.getId());
        DeleteDTO dto = new DeleteDTO();
        Long[] ids = {managerMajor1.getId(),managerMajor2.getId()};
        dto.setIds(ids);
        MvcResult result = MajorIntegrationHelper.deleteMajor(mvc,dto);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    public MvcResult createMajor(ManagerMajor MajorRequest) throws Exception{
        return MajorIntegrationHelper.createMajor(mvc, MajorRequest);
    }
}
