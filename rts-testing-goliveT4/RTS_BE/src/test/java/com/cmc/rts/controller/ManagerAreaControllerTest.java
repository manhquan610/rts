package com.cmc.rts.controller;

import com.cmc.rts.dto.DeleteDTO;
import com.cmc.rts.dto.request.AreaRequest;
import com.cmc.rts.entity.ManagerArea;
import com.cmc.rts.helper.AreaIntegrationHelper;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class ManagerAreaControllerTest {
    @Autowired
    private MockMvc mvc;

    //create
    @Test
    public void shouldCreateAreaSuccessWithUniqueName() throws Exception {
        AreaRequest areaRequest = new AreaRequest("area" + System.currentTimeMillis(), "unittest");
        MvcResult result = AreaIntegrationHelper.createArea(mvc, areaRequest);
        assertThat(result.getResponse().getStatus()).isEqualTo(201);
    }

    @Test
    public void shouldCreateAreaSuccessWithNameLikeDescription() throws Exception {
        AreaRequest areaRequest = new AreaRequest("area" + System.currentTimeMillis(), "area" + System.currentTimeMillis());
        MvcResult result = AreaIntegrationHelper.createArea(mvc, areaRequest);
        assertThat(result.getResponse().getStatus()).isEqualTo(201);
    }

    //get
    @Test
    public void getAreaSuccessWithExactName() throws Exception {
        MvcResult result = AreaIntegrationHelper.getAreaExact(mvc, 5, 1, "Japan");
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    public void getAreaSuccessWithExactNameReturnSizeObjectResponse() throws Exception {
        Gson gson = new Gson();
        MvcResult result = AreaIntegrationHelper.getAreaExact(mvc, 5, 1, "Đà Nẵng,Hồ Chí Minh");
        Object object = gson.fromJson(result.getResponse().getContentAsString(), Object.class);
        ArrayList list = (ArrayList) ((LinkedTreeMap) object).get("content");
        assertThat(list).isNotNull();
    }

    @Test
    public void getAreaSuccessWithExactNameWithEmptyName() throws Exception {
        Gson gson = new Gson();
        MvcResult result = AreaIntegrationHelper.getAreaExact(mvc, 5, 1, "");
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    public void getAreaWithNotExactName() throws Exception {
        Gson gson = new Gson();
        MvcResult result = AreaIntegrationHelper.getAreaExact(mvc, 5, 1, "an");
        Object object = gson.fromJson(result.getResponse().getContentAsString(), Object.class);
        ArrayList list = (ArrayList) ((LinkedTreeMap) object).get("content");
        assertThat(list.size()).isEqualTo(0);
    }

    //update
    @Test
    public void shouldUpdateAreaSuccessWithUniqueName() throws Exception {
        AreaRequest areaRequest = new AreaRequest(1L, "area" + System.currentTimeMillis(), "unittest");
        MvcResult result = AreaIntegrationHelper.updateArea(mvc, areaRequest);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    public void shouldUpdateAreaSuccessWithNameLikeDescription() throws Exception {
        AreaRequest areaRequest = new AreaRequest(1L, "unittest" + System.currentTimeMillis(), "unittest"+ System.currentTimeMillis());
        MvcResult result = AreaIntegrationHelper.updateArea(mvc, areaRequest);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    //delete
    @Test
    public void shouldDeleteOneAreaSuccess() throws Exception {
        Gson gson = new Gson();
        AreaRequest areaRequest = new AreaRequest("area" + System.currentTimeMillis() +"test1", "unittest");
        ManagerArea managerArea = gson.fromJson(createArea(areaRequest).getResponse().getContentAsString(),ManagerArea.class);
        System.out.println("Day laaaaaaaaa:" +managerArea.getId());
        DeleteDTO dto = new DeleteDTO();
        Long[] ids = {managerArea.getId()};
        dto.setIds(ids);
        MvcResult result = AreaIntegrationHelper.deleteArea(mvc,dto);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    @Test
    public void shouldDeleteManyAreaSuccess() throws Exception {
        Gson gson = new Gson();
        AreaRequest areaRequest = new AreaRequest("area1" + System.currentTimeMillis(), "unittest");
        AreaRequest areaRequest1 = new AreaRequest("area2" + System.currentTimeMillis(), "unittest");
        ManagerArea managerArea1 = gson.fromJson(createArea(areaRequest).getResponse().getContentAsString(),ManagerArea.class);
        ManagerArea managerArea2 = gson.fromJson(createArea(areaRequest1).getResponse().getContentAsString(),ManagerArea.class);
        System.out.println("Day laaaaaaaaa:" +managerArea1.getId() + "," + managerArea2.getId());
        DeleteDTO dto = new DeleteDTO();
        Long[] ids = {managerArea1.getId(),managerArea2.getId()};
        dto.setIds(ids);
        MvcResult result = AreaIntegrationHelper.deleteArea(mvc,dto);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    public MvcResult createArea(AreaRequest areaRequest) throws Exception{
       return AreaIntegrationHelper.createArea(mvc, areaRequest);
    }
}
