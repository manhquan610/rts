package com.cmc.rts.controller;

import com.cmc.rts.helper.LanguageIntegrationHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class LanguageControllerTest {
    @Autowired
    private MockMvc mvc;
    //get get language
    @Test
    public void getAllLanguageSuccessWithName() throws Exception{
        MvcResult result = LanguageIntegrationHelper.getLanguage(mvc,"a",1,10);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    @Test
    public void getAllLanguageSuccess() throws Exception{
        MvcResult result = LanguageIntegrationHelper.getLanguage(mvc,"",1,10);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    @Test
    public void getAllLanguageWithNameAndlimit0() throws Exception{
        MvcResult result = LanguageIntegrationHelper.getLanguage(mvc,"a",1,0);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);//no response body language
    }
}
