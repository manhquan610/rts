package com.cmc.rts.helper;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class LikeIntegrationHelper {
    public static MvcResult getLike(MockMvc mvc, Integer candidateId, Boolean liked) throws Exception{
        String url = "/liked?candidateID="+candidateId+"&liked="+liked;
        return mvc.perform(post(url)
                .header("Content-Type", "application/json")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
    }
    public static MvcResult getTotalLike(MockMvc mvc, Integer candidateId) throws Exception{
        String url = "/totalReact?candidateID="+candidateId;
        return mvc.perform(get(url)
                .header("Content-Type", "application/json")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
    }
}
