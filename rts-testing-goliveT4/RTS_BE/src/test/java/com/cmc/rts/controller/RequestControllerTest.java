package com.cmc.rts.controller;

import com.cmc.rts.dto.request.CreateAssignRequest;
import com.cmc.rts.dto.requestDto.RequestDTO;
import com.cmc.rts.entity.*;
import com.cmc.rts.helper.RequestIntegrationHelper;
import com.cmc.rts.helper.UserIntegrationHelper;
import com.cmc.rts.repository.ManagerAreaRepository;
import com.cmc.rts.repository.ManagerJobTypeRepository;
import com.cmc.rts.repository.ManagerRequestTypeRepository;
import com.google.gson.Gson;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

import java.lang.System;

//@RunWith(SpringRunner.class)
//@AutoConfigureMockMvc
//@SpringBootTest
//@ActiveProfiles("test")
public class RequestControllerTest {

    private static final Logger log = LoggerFactory.getLogger(RequestControllerTest.class);
    @Autowired
    private MockMvc mvc;

    private final String requestTitle = "Request";

    private final String description = "<p>1231</p>";

    @Autowired
    private ManagerAreaRepository managerAreaRepository;

    @Autowired
    private ManagerJobTypeRepository managerJobTypeRepository;

    @Autowired
    private ManagerRequestTypeRepository managerRequestTypeRepository;
//
//    //Todo get id of project and skill
//    //Create request
//    @Test
//    public void shouldCreateRequestSuccessWithDuLeadAccount() throws Exception {
//        MvcResult result = createRequest("txtrung","Tt@@160820");
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void shouldCreateRequestSuccessCompareNumber() throws Exception {
//        Gson gson = new Gson();
//        UserDTO userDTO = logicAndGetToken("txtrung","Tt@@160820");
//        ManagerArea area = managerAreaRepository.findOne(1L);
//        ManagerJobType jobType = managerJobTypeRepository.findOne(1L);
//        ManagerRequestType managerRequestType =  managerRequestTypeRepository.findOne(1L);
//        RequestDTO requestDTO = createRequestDTO(20, 2L, "1", 1L, 1L,
//                managerRequestType.getId(),5L ,44l, 51L, WorkingTypeEnum.FullTime, area,jobType,"");
//        MvcResult result = RequestIntegrationHelper.createRequestWithDuLeadAccount(mvc, userDTO.getToken(), requestDTO);
//        Request request = gson.fromJson(result.getResponse().getContentAsString(),Request.class);
//        assertThat(request.getNumber()).isEqualTo(20);
//    }
//    @Test
//    public void shouldCreateRequestSuccessCompareSkill() throws Exception {
//        String username = "txtrung";
//        String password = "Tt@@160820";
//        MvcResult user = UserIntegrationHelper.loginWithDuLeadAccount(mvc, username, password);
//        assertThat(user.getResponse().getStatus()).isEqualTo(200);
//        Gson gson = new Gson();
//        UserDTO userDto = gson.fromJson(user.getResponse().getContentAsString(), UserDTO.class);
//        ManagerArea area = managerAreaRepository.findOne(1L);
//        ManagerJobType jobType = managerJobTypeRepository.findOne(1L);
//        ManagerRequestType managerRequestType =  managerRequestTypeRepository.findOne(1L);
//        RequestDTO requestDTO = createRequestDTO(20, 2L, "1", 1L, 1L,
//                managerRequestType.getId(),5L ,44l, 51L, WorkingTypeEnum.FullTime, area,jobType,"");
//        MvcResult result = RequestIntegrationHelper.createRequestWithDuLeadAccount(mvc, userDto.getToken(), requestDTO);
//        Request request = gson.fromJson(result.getResponse().getContentAsString(),Request.class);
//        assertThat(request.getSkillNameId()).isEqualTo("1");
//    }
//    //Submit request
//    @Test
//    public void shouldSubmitRequestSuccess() throws Exception{
//        MvcResult result = submitRequest("txtrung","Tt@@160820");
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void shouldSubmitRequestSuccessWithGleadAccount() throws Exception{
//        MvcResult result = submitRequest("txtrung","Tt@@160820");
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//
//    @Test
//    public void shouldSubmitRequestFailed() throws Exception{
//        UserDTO userDto = logicAndGetToken("txtrung","Tt@@160820");
//        MvcResult result = RequestIntegrationHelper.submitRequest(mvc,userDto.getToken(),"1000","ntnam3");
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);// return response error
//    }
//    // get request by id
//    @Test
//    public void getRequestByIdSuccess() throws Exception{
//        Gson gson = new Gson();
//        Request request = gson.fromJson(createRequest("txtrung","Tt@@160820").getResponse().getContentAsString(),Request.class);
//        MvcResult result = RequestIntegrationHelper.getRequestById(mvc,request.getId());
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void getRequestByIdSuccessWhenNotExistId() throws Exception{
//        MvcResult result = RequestIntegrationHelper.getRequestById(mvc,10000L);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);//response error body
//    }
//    @Test
//    public void getRequestByIdSuccessCompareCreatedBy() throws Exception{
//        Gson gson = new Gson();
//        Request request = gson.fromJson(createRequest("txtrung","Tt@@160820").getResponse().getContentAsString(),Request.class);
//        MvcResult result = RequestIntegrationHelper.getRequestById(mvc,request.getId());
//        Request rq = gson.fromJson(result.getResponse().getContentAsString(),Request.class);
//        assertThat(rq.getCreatedBy()).isEqualTo("txtrung");
//    }
//    @Test
//    public void getRequestByIdSuccessCompareManagerGroupId() throws Exception{
//        Gson gson = new Gson();
//        Request request = gson.fromJson(createRequest("txtrung","Tt@@160820").getResponse().getContentAsString(),Request.class);
//        MvcResult result = RequestIntegrationHelper.getRequestById(mvc,request.getId());
//        Request rq = gson.fromJson(result.getResponse().getContentAsString(),Request.class);
//        assertThat(rq.getManagerGroupId()).isEqualTo(44L);
//    }
//    //Update Request
//    @Test
//    public void shouldUpdateRequestSuccessWithDuLeadAccount() throws Exception {
//        UserDTO userDto = logicAndGetToken("ntnam3","MeoCon98@");
//        ManagerArea area = managerAreaRepository.findOne(1L);
//        ManagerJobType jobType = managerJobTypeRepository.findOne(1L);
//        ManagerRequestType managerRequestType =  managerRequestTypeRepository.findOne(1L);
//        RequestDTO requestDTO = createRequestDTO(20, 2L, "1", 1L, 1L,
//                managerRequestType.getId(),5L ,44l, 51L, WorkingTypeEnum.FullTime, area,jobType,"");
//        MvcResult result = RequestIntegrationHelper.updateRequestWithDuLeadAccount(mvc, userDto.getToken(), requestDTO,464L);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//
//    @Test
//    public void shouldUpdateRequestFailedWithDuLeadAccountInputNotExistId() throws Exception {
//        UserDTO userDto = logicAndGetToken("txtrung","Tt@@160820");
//        ManagerArea area = managerAreaRepository.findOne(1L);
//        ManagerJobType jobType = managerJobTypeRepository.findOne(1L);
//        ManagerRequestType managerRequestType =  managerRequestTypeRepository.findOne(1L);
//        RequestDTO requestDTO = createRequestDTO(20, 2L, "1", 1L, 1L,
//                managerRequestType.getId(),5L ,44l, 51L, WorkingTypeEnum.FullTime, area,jobType,"");
//        MvcResult result = RequestIntegrationHelper.updateRequestWithDuLeadAccount(mvc, userDto.getToken(), requestDTO,1000L);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);//response body is error
//    }
//
//
//    private RequestDTO createRequestDTO(Integer number, Long positionId, String skillId, Long priorityId,
//                                        Long experienceId, Long requestTypeId, Long projectId, Long managerGroupId,
//                                        Long departmentId, WorkingTypeEnum workingType, ManagerArea area, ManagerJobType jobType, String requirement){
//        String title = requestTitle + System.currentTimeMillis();
//        String deadline = LocalDate.now().plusDays(1).toString();
//        RequestDTO requestDTO = new RequestDTO(title, positionId, deadline, number, description, skillId, priorityId, experienceId,
//                requestTypeId, projectId, managerGroupId, departmentId, area, jobType, workingType, requirement);
//        return requestDTO;
//    }
//
//    @Test
//    public void getRequestSuccessWithAnyAccount() throws Exception{
//        String username = "nptien1";
//        String password = "Qwertyuiop@";
//        MvcResult user = UserIntegrationHelper.loginWithDuLeadAccount(mvc, username, password);
//        assertThat(user.getResponse().getStatus()).isEqualTo(200);
//        Gson gson = new Gson();
//        UserDTO userDto = gson.fromJson(user.getResponse().getContentAsString(), UserDTO.class);
//        MvcResult result = RequestIntegrationHelper.getRequest(mvc,userDto.getToken(),"",5,1);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void getRequestSuccessWithAnyAccountWhenInputName() throws Exception{
//        String username = "nptien1";
//        String password = "Qwertyuiop@";
//        MvcResult user = UserIntegrationHelper.loginWithDuLeadAccount(mvc, username, password);
//        assertThat(user.getResponse().getStatus()).isEqualTo(200);
//        Gson gson = new Gson();
//        UserDTO userDto = gson.fromJson(user.getResponse().getContentAsString(), UserDTO.class);
//        MvcResult result = RequestIntegrationHelper.getRequest(mvc,userDto.getToken(),"aaaa",5,1);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void getRequestSuccessWithAnyAccountWhenLimit0() throws Exception{
//        UserDTO userDto = logicAndGetToken("nptien1","Qwertyuiop@");
//        MvcResult result = RequestIntegrationHelper.getRequest(mvc,userDto.getToken(),"aaaa",0,1);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    //get glead
//    @Test
//    public void getGleadSuccessWithDuLeadAccount() throws Exception{
//        UserDTO userDto = logicAndGetToken("txtrung","Tt@@160820");
//        MvcResult result = RequestIntegrationHelper.getGlead(mvc,userDto.getToken(),"txtrung");
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void getGleadSuccessWithGleadAccount() throws Exception{
//        UserDTO userDto = logicAndGetToken("ntnam3","MeoCon98@");
//        MvcResult result = RequestIntegrationHelper.getGlead(mvc,userDto.getToken(),"ntnam3");
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void getGleadSuccessWithOtherAccount() throws Exception{
//        UserDTO userDto = logicAndGetToken("nptien1","Qwertyuiop@");
//        MvcResult result = RequestIntegrationHelper.getGlead(mvc,userDto.getToken(),"ntnam3");
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void getHrMember() throws Exception{
//        MvcResult result = RequestIntegrationHelper.getHrMember(mvc);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//
//    @Test
//    public void updateShareJobSuccessWithExistId() throws Exception{
//        MvcResult result = RequestIntegrationHelper.updateShareJob(mvc,464L);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void updateShareJobWithNotExistId() throws Exception{
//        MvcResult result = RequestIntegrationHelper.updateShareJob(mvc,4644L);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);//but have not response body
//    }
//    //request assign
//    @Test
//    public void createRequestAssignSuccessWithRecAccount() throws Exception{
//        Gson gson = new Gson();
//        Request[] requestList = gson.fromJson(submitRequest("txtrung","Tt@@160820").getResponse().getContentAsString(),Request[].class);
//        System.out.println("day la :"+requestList);
//        MvcResult resultApprove = approveRequest(requestList,"ntnam3","MeoCon98@");
//        assertThat(resultApprove.getResponse().getStatus()).isEqualTo(200);
//        Long id = requestList[0].getId();
//        UserDTO userDto = logicAndGetToken("htanh6","Hunken22");
//        List<CreateAssignRequest> list = new ArrayList<>();
//        CreateAssignRequest createAssignRequest = new CreateAssignRequest();
//        createAssignRequest.setAssignLdap("nptien1");
//        createAssignRequest.setTarget(1);
//        list.add(createAssignRequest);
//        MvcResult result = RequestIntegrationHelper.createRequestAssign(mvc,userDto.getToken(),list,id);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void createRequestAssignSuccessWithDuleadAccount() throws Exception{
//        Gson gson = new Gson();
//        Request[] requestList = gson.fromJson(submitRequest("txtrung","Tt@@160820").getResponse().getContentAsString(),Request[].class);
//        System.out.println("day la :"+requestList);
//        MvcResult resultApprove = approveRequest(requestList,"ntnam3","MeoCon98@");
//        assertThat(resultApprove.getResponse().getStatus()).isEqualTo(200);
//        Long id = requestList[0].getId();
//        UserDTO userDto = logicAndGetToken("txtrung","Tt@@160820");
//        List<CreateAssignRequest> list = new ArrayList<>();
//        CreateAssignRequest createAssignRequest = new CreateAssignRequest();
//        createAssignRequest.setAssignLdap("nptien1");
//        createAssignRequest.setTarget(1);
//        list.add(createAssignRequest);
//        MvcResult result = RequestIntegrationHelper.createRequestAssign(mvc,userDto.getToken(),list,id);
//        assertThat(result.getResponse().getStatus()).isEqualTo(403);
//    }
//    @Test
//    public void createRequestAssignSuccessWithOtherAccount() throws Exception{
//        Gson gson = new Gson();
//        Request[] requestList = gson.fromJson(submitRequest("txtrung","Tt@@160820").getResponse().getContentAsString(),Request[].class);
//        System.out.println("day la :"+requestList);
//        MvcResult resultApprove = approveRequest(requestList,"ntnam3","MeoCon98@");
//        assertThat(resultApprove.getResponse().getStatus()).isEqualTo(200);
//        Long id = requestList[0].getId();
//        UserDTO userDto = logicAndGetToken("nptien1","Qwertyuiop@");
//        List<CreateAssignRequest> list = new ArrayList<>();
//        CreateAssignRequest createAssignRequest = new CreateAssignRequest();
//        createAssignRequest.setAssignLdap("nptien1");
//        createAssignRequest.setTarget(1);
//        list.add(createAssignRequest);
//        MvcResult result = RequestIntegrationHelper.createRequestAssign(mvc,userDto.getToken(),list,id);
//        assertThat(result.getResponse().getStatus()).isEqualTo(403);
//    }
//    @Test
//    public void createRequestAssignSuccessWithGleadAccount() throws Exception{
//        Gson gson = new Gson();
//        Request[] requestList = gson.fromJson(submitRequest("txtrung","Tt@@160820").getResponse().getContentAsString(),Request[].class);
//        System.out.println("day la :"+requestList);
//        MvcResult resultApprove = approveRequest(requestList,"ntnam3","MeoCon98@");
//        assertThat(resultApprove.getResponse().getStatus()).isEqualTo(200);
//        Long id = requestList[0].getId();
//        UserDTO userDto = logicAndGetToken("ntnam3","MeoCon98@");
//        List<CreateAssignRequest> list = new ArrayList<>();
//        CreateAssignRequest createAssignRequest = new CreateAssignRequest();
//        createAssignRequest.setAssignLdap("nptien1");
//        createAssignRequest.setTarget(1);
//        list.add(createAssignRequest);
//        MvcResult result = RequestIntegrationHelper.createRequestAssign(mvc,userDto.getToken(),list,id);
//        assertThat(result.getResponse().getStatus()).isEqualTo(403);
//    }
//    @Test
//    public void createRequestAssignSuccessReturnLdap() throws Exception{
//        Gson gson = new Gson();
//        Request[] requestList = gson.fromJson(submitRequest("txtrung","Tt@@160820").getResponse().getContentAsString(),Request[].class);
//        MvcResult resultApprove = approveRequest(requestList,"ntnam3","MeoCon98@");
//        assertThat(resultApprove.getResponse().getStatus()).isEqualTo(200);
//        Long id = requestList[0].getId();
//        UserDTO userDto = logicAndGetToken("htanh6","Hunken22");
//        List<CreateAssignRequest> list = new ArrayList<>();
//        CreateAssignRequest createAssignRequest = new CreateAssignRequest();
//        createAssignRequest.setAssignLdap("nptien1");
//        createAssignRequest.setTarget(1);
//        list.add(createAssignRequest);
//        MvcResult result = RequestIntegrationHelper.createRequestAssign(mvc,userDto.getToken(),list,id);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//        RequestAssign[] requestAssigns = gson.fromJson(result.getResponse().getContentAsString(),RequestAssign[].class);
//        assertThat(requestAssigns[0].getAssignName()).isEqualTo(null);
//    }
//    //Approve
//    @Test
//    public void approveRequestSuccessWithGleadAccount() throws Exception{
//        Gson gson = new Gson();
//        Request[] requestList = gson.fromJson(submitRequest("txtrung","Tt@@160820").getResponse().getContentAsString(),Request[].class);
//        MvcResult result = approveRequest(requestList,"ntnam3","MeoCon98@");
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void approveRequestSuccessWithDuleadAccount() throws Exception{
//        Gson gson = new Gson();
//        Request[] requestList = gson.fromJson(submitRequest("txtrung","Tt@@160820").getResponse().getContentAsString(),Request[].class);
//        MvcResult result = approveRequest(requestList,"txtrung","Tt@@160820");
//        assertThat(result.getResponse().getStatus()).isEqualTo(403);
//    }
//    @Test
//    public void approveRequestSuccessWithOtherAccount() throws Exception{
//        Gson gson = new Gson();
//        Request[] requestList = gson.fromJson(submitRequest("txtrung","Tt@@160820").getResponse().getContentAsString(),Request[].class);
//        MvcResult result = approveRequest(requestList,"nptien1","Qwertyuiop@");
//        assertThat(result.getResponse().getStatus()).isEqualTo(403);
//    }
//    //Reject
//    @Test
//    public void rejectRequestSuccessWithOtherAccount() throws Exception{
//        Gson gson = new Gson();
//        UserDTO userDTO = logicAndGetToken("nptien1","Qwertyuiop@");
//        Request[] requestList = gson.fromJson(submitRequest("txtrung","Tt@@160820").getResponse().getContentAsString(),Request[].class);
//          MvcResult result = RequestIntegrationHelper.rejectRequest(mvc,userDTO.getToken(),requestList[0].getId().toString(),"notpass");
//          assertThat(result.getResponse().getStatus()).isEqualTo(403);
//    }
//    @Test
//    public void rejectRequestSuccessWithDuleadAccount() throws Exception{
//        Gson gson = new Gson();
//        UserDTO userDTO = logicAndGetToken("txtrung","Tt@@160820");
//        Request[] requestList = gson.fromJson(submitRequest("txtrung","Tt@@160820").getResponse().getContentAsString(),Request[].class);
//        MvcResult result = RequestIntegrationHelper.rejectRequest(mvc,userDTO.getToken(),requestList[0].getId().toString(),"notpass");
//        assertThat(result.getResponse().getStatus()).isEqualTo(403);
//    }
//    @Test
//    public void rejectRequestSuccessWithGleadAccount() throws Exception{
//        Gson gson = new Gson();
//        UserDTO userDTO = logicAndGetToken("ntnam3","MeoCon98@");
//        Request[] requestList = gson.fromJson(submitRequest("txtrung","Tt@@160820").getResponse().getContentAsString(),Request[].class);
//        MvcResult result = RequestIntegrationHelper.rejectRequest(mvc,userDTO.getToken(),requestList[0].getId().toString(),"notpass");
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
////    @Test
////    public void rejectRequestSuccessWhenNotExistRequestId() throws Exception{
////        Gson gson = new Gson();
////        UserDTO userDTO = logicAndGetToken("ntnam3","MeoCon98@");
////        MvcResult result = RequestIntegrationHelper.rejectRequest(mvc,userDTO.getToken(),"1000","notpass");
////        assertThat(result.getResponse().getStatus()).isEqualTo(200);//response error number
////    }
//    //Close
//    @Test
//    public void closeRequestSuccessWithGleadAccount() throws Exception{
//        Gson gson = new Gson();
//        UserDTO userDTO = logicAndGetToken("ntnam3","MeoCon98@");
//        Request[] requestList = gson.fromJson(submitRequest("txtrung","Tt@@160820").getResponse().getContentAsString(),Request[].class);
//        MvcResult resultApprove = approveRequest(requestList,"ntnam3","MeoCon98@");
//        MvcResult result = RequestIntegrationHelper.closeRequest(mvc,userDTO.getToken(),requestList[0].getId().toString());
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void closeRequestSuccessWithDuleadAccount() throws Exception{
//        Gson gson = new Gson();
//        UserDTO userDTO = logicAndGetToken("txtrung","Tt@@160820");
//        Request[] requestList = gson.fromJson(submitRequest("txtrung","Tt@@160820").getResponse().getContentAsString(),Request[].class);
//        MvcResult resultApprove = approveRequest(requestList,"ntnam3","MeoCon98@");
//        MvcResult result = RequestIntegrationHelper.closeRequest(mvc,userDTO.getToken(),requestList[0].getId().toString());
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void closeRequestSuccessWithOtherAccount() throws Exception{
//        Gson gson = new Gson();
//        UserDTO userDTO = logicAndGetToken("nptien1","Qwertyuiop@");
//        Request[] requestList = gson.fromJson(submitRequest("txtrung","Tt@@160820").getResponse().getContentAsString(),Request[].class);
//        MvcResult resultApprove = approveRequest(requestList,"ntnam3","MeoCon98@");
//        MvcResult result = RequestIntegrationHelper.closeRequest(mvc,userDTO.getToken(),requestList[0].getId().toString());
//        assertThat(result.getResponse().getStatus()).isEqualTo(403);
//    }
//    public MvcResult createRequest( String username, String password) throws Exception{
//        UserDTO userDTO = logicAndGetToken(username,password);
//        ManagerArea area = managerAreaRepository.findOne(1L);
//        ManagerJobType jobType = managerJobTypeRepository.findOne(1L);
//        ManagerRequestType managerRequestType =  managerRequestTypeRepository.findOne(1L);
//        RequestDTO requestDTO = createRequestDTO(20, 2L, "1", 1L, 1L,
//                managerRequestType.getId(),5L ,44l, 51L, WorkingTypeEnum.FullTime, area,jobType,"");
//        return  RequestIntegrationHelper.createRequestWithDuLeadAccount(mvc, userDTO.getToken(), requestDTO);
//    }
//    public MvcResult submitRequest(String username, String password) throws Exception{
//        Gson gson = new Gson();
//        UserDTO userDto = logicAndGetToken(username,password);
//        Request request = gson.fromJson(createRequest(username,password).getResponse().getContentAsString(),Request.class);
//        return RequestIntegrationHelper.submitRequest(mvc,userDto.getToken(),request.getId().toString(),"ntnam3");
//    }
//    public UserDTO logicAndGetToken(String username, String password) throws Exception{
//        MvcResult user = UserIntegrationHelper.loginWithDuLeadAccount(mvc, username, password);
//        Gson gson = new Gson();
//        UserDTO userDto = gson.fromJson(user.getResponse().getContentAsString(), UserDTO.class);
//        return userDto;
//    }
//
//    public MvcResult approveRequest(Request[] listRequest, String username, String password) throws Exception{
//        UserDTO userDto = logicAndGetToken(username,password);
//        return RequestIntegrationHelper.approveRequest(mvc,userDto.getToken(),listRequest[0].getId().toString());
//    }

}
