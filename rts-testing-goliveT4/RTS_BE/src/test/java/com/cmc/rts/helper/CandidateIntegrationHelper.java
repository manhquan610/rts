package com.cmc.rts.helper;

import com.cmc.rts.dto.requestDto.RequestUpdateDto;
import com.cmc.rts.entity.CandidateStateEnum;
import com.google.gson.Gson;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;

public class CandidateIntegrationHelper {
    public static MvcResult getAllCandidate(MockMvc mvc, String name, Integer limit, Integer offset) throws Exception{
        String url = "/candidate/search?candidateState=none&limit="+limit+"&offset="+offset+"&requestId=377&searchString="+name;
        return mvc.perform(post(url)
                .header("Content-Type", "application/json")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
    }
    public static MvcResult getCandidateById(MockMvc mvc, Long id) throws Exception{
        String url = "/candidate/"+id;
        return mvc.perform(get(url)
                .header("Content-Type", "application/json")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
    }
    public static MvcResult updateState(MockMvc mvc, String candidateStateBefore, String id, RequestUpdateDto requestUpdateDto) throws Exception{
        Gson gson = new Gson();
        String url = "/candidate/"+id+"?candidateStateCurrent="+candidateStateBefore;
        String request = gson.toJson(requestUpdateDto);
        return mvc.perform(put(url)
                .header("Content-Type", "application/json")
                .contentType(MediaType.APPLICATION_JSON).content(request)).andReturn();
    }
    public static MvcResult deleteCandidate(MockMvc mvc, Long requestId, String candidateId ) throws Exception{
        String url = "/candidate/"+candidateId+"?requestId="+requestId;
        return mvc.perform(delete(url)
                .header("Content-Type", "application/json")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
    }
}
