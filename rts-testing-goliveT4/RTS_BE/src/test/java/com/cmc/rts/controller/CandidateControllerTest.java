package com.cmc.rts.controller;

import com.cmc.rts.dto.request.CVRequest;
import com.cmc.rts.dto.requestDto.RequestUpdateDto;
import com.cmc.rts.entity.CandidateStateEnum;
import com.cmc.rts.helper.CVIntegrationHelper;
import com.cmc.rts.helper.CandidateIntegrationHelper;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.Assertions.assertThat;

//@RunWith(SpringRunner.class)
//@AutoConfigureMockMvc
//@SpringBootTest
//@ActiveProfiles("test")
public class CandidateControllerTest {
//    @Autowired
//    private MockMvc mvc;
//    //get all candidate
//    @Test
//    public void getAllCandidateSuccessWithAllNameSearch() throws Exception{
//        MvcResult result = CandidateIntegrationHelper.getAllCandidate(mvc,"",5,1);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void getAllCandidateSuccessByName() throws Exception{
//        MvcResult result = CandidateIntegrationHelper.getAllCandidate(mvc,"a",5,1);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void getAllCandidateSuccessByPhone() throws Exception{
//        MvcResult result = CandidateIntegrationHelper.getAllCandidate(mvc,"09",5,1);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void getAllCandidateSuccessByEmail() throws Exception{
//        MvcResult result = CandidateIntegrationHelper.getAllCandidate(mvc,"gmail",5,1);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//
//    //get candidate by id
//    @Test
//    public void getAllCandidateSuccessByID() throws Exception{
//        MvcResult result = CandidateIntegrationHelper.getCandidateById(mvc,3016L);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void updateCandidateStateSuccess() throws Exception{
//        Gson gson = new Gson();
//        MvcResult candidateRS = createCandidate();
//        Object object = gson.fromJson(candidateRS.getResponse().getContentAsString(), Object.class);
//        Double id = Double.parseDouble(((LinkedTreeMap) object).get("candidateId").toString());
//        Long candidateId = id.longValue();
//        System.out.println(id);
//        RequestUpdateDto requestUpdateDto = new RequestUpdateDto();
//        requestUpdateDto.setRequestId(377L);
//        requestUpdateDto.setCandidateStateEnum(CandidateStateEnum.offer);
//        requestUpdateDto.setAssignees("datu3");
//        MvcResult result =  CandidateIntegrationHelper.updateState(mvc, CandidateStateEnum.none.toString(),candidateId.toString(),requestUpdateDto);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//        result = deleteCandidate(candidateId);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//    @Test
//    public void updateCandidateStateSuccessWithSameState() throws Exception{
//        Gson gson = new Gson();
//        MvcResult candidateRS = createCandidate();
//        Object object = gson.fromJson(candidateRS.getResponse().getContentAsString(), Object.class);
//        Double id = Double.parseDouble(((LinkedTreeMap) object).get("candidateId").toString());
//        Long candidateId = id.longValue();
//        System.out.println(id);
//        RequestUpdateDto requestUpdateDto = new RequestUpdateDto();
//        requestUpdateDto.setRequestId(377L);
//        requestUpdateDto.setCandidateStateEnum(CandidateStateEnum.none);
//        requestUpdateDto.setAssignees("datu3");
//        MvcResult result =  CandidateIntegrationHelper.updateState(mvc, CandidateStateEnum.none.toString(),candidateId.toString(),requestUpdateDto);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//        result = deleteCandidate(candidateId);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//
//    //delete candidate
//    @Test
//    public void deleteCandidateSuccess() throws Exception{
//        Gson gson = new Gson();
//        MvcResult candidateRS = createCandidate();
//        Object object = gson.fromJson(candidateRS.getResponse().getContentAsString(), Object.class);
//        Double id = Double.parseDouble(((LinkedTreeMap) object).get("candidateId").toString());
//        Long candidateId = (new Double(id)).longValue();
//        System.out.println(id);
//        MvcResult result = deleteCandidate(candidateId);
//        assertThat(result.getResponse().getStatus()).isEqualTo(200);
//    }
//
//    public MvcResult createCandidate() throws Exception{
//        CVRequest cvRequest = new CVRequest("Dang Anh Tu","tudahe12@gmail.com" +System.currentTimeMillis(),""+System.currentTimeMillis(),"dvsdvdvsdvlink",16L,"26-11-2021","Dev","377");
//        return CVIntegrationHelper.createCV(mvc,cvRequest);
//    }
//    public MvcResult  deleteCandidate(Long candidateId) throws Exception{
//       return CandidateIntegrationHelper.deleteCandidate(mvc,377L, candidateId.toString());
//    }
}
