package com.cmc.rts.controller;

import com.cmc.rts.helper.ChannelIntegrationHelper;
import com.cmc.rts.helper.LikeIntegrationHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.Assertions.assertThat;
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class LikeControllerTest {
    @Autowired
    private MockMvc mvc;
    //like
    @Test
    public void likeAction() throws Exception{
        MvcResult result = LikeIntegrationHelper.getLike(mvc,279,true);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    @Test
    public void disLikeAction() throws Exception{
        MvcResult result = LikeIntegrationHelper.getLike(mvc,279,true);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    //GET Total like
    @Test
    public void getTotalLike() throws Exception{
        MvcResult result = LikeIntegrationHelper.getTotalLike(mvc,279);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
}
