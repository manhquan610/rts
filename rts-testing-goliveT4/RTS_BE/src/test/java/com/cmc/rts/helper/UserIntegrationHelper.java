package com.cmc.rts.helper;

import com.cmc.rts.entity.UserAuthen;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class UserIntegrationHelper {

    public static MvcResult loginWithDuLeadAccount(MockMvc mvc, String username, String password) throws Exception {
        UserAuthen userAuthen = new UserAuthen(username, password, "", "");
        ObjectMapper mapper = new ObjectMapper();
        String request = mapper.writeValueAsString(userAuthen);
        return mvc.perform(post("/authen/login")
                .header("Content-Type", "application/json")
                .contentType(MediaType.APPLICATION_JSON).content(request)).andReturn();
    }
}