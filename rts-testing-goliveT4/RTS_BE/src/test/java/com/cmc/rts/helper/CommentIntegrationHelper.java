package com.cmc.rts.helper;

import com.cmc.rts.dto.request.CommentRequest;
import com.cmc.rts.dto.requestDto.RequestDTO;
import com.google.gson.Gson;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class CommentIntegrationHelper {
    public static MvcResult getAllComment(MockMvc mvc, String token, Long requestId, Integer page, Integer limit ) throws Exception{
        Gson gson = new Gson();
        String url = "/comment?limit="+limit+"&page="+page+"&requestId="+requestId;
        return mvc.perform(get(url)
                .header("Authorization", "Bearer " + token)
                .header("Content-Type", "application/json")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
    }
    public static MvcResult getCommentForCandidate(MockMvc mvc, String token, Long requestId, Long candidateId, Integer page, Integer limit ) throws Exception {
        Gson gson = new Gson();
        String url = "/comment/candidate?candidateId="+candidateId+"&limit="+limit+"&page="+page+"&requestId="+requestId;
        return mvc.perform(get(url)
                .header("Authorization", "Bearer " + token)
                .header("Content-Type", "application/json")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
    }
    public static MvcResult createComment(MockMvc mvc, String token, CommentRequest commentRequest) throws Exception {
        Gson gson = new Gson();
        String request = gson.toJson(commentRequest);
        return mvc.perform(post("/comment")
                .header("Authorization", "Bearer " + token)
                .header("Content-Type", "application/json")
                .contentType(MediaType.APPLICATION_JSON).content(request)).andReturn();
    }
}
