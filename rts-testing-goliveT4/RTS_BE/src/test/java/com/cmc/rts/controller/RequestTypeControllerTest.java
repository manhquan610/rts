package com.cmc.rts.controller;

import com.cmc.rts.dto.DeleteDTO;
import com.cmc.rts.dto.request.ListNameRequest;
import com.cmc.rts.entity.ManagerRequestType;
import com.cmc.rts.helper.RequestTypeIntegrationHelper;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class RequestTypeControllerTest {
    @Autowired
    private MockMvc mvc;

    //create
    @Test
    public void shouldCreateRequestTypeSuccessWithUniqueName() throws Exception {
        ManagerRequestType managerRequestType = new ManagerRequestType("RequestType" + System.currentTimeMillis(), "unittest");
        MvcResult result = RequestTypeIntegrationHelper.createRequestType(mvc, managerRequestType);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    public void shouldCreateRequestTypeSuccessWithNameLikeDescription() throws Exception {
        ManagerRequestType managerRequestType = new ManagerRequestType("RequestType" + System.currentTimeMillis(), "RequestType" + System.currentTimeMillis());
        MvcResult result = RequestTypeIntegrationHelper.createRequestType(mvc, managerRequestType);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }

    //get
    @Test
    public void getRequestTypeSuccessWithExactName() throws Exception {
        ListNameRequest listNameRequest = new ListNameRequest();
        List<String> name = new ArrayList<>();
        name.add("normal");
        listNameRequest.setLstName(name);
        listNameRequest.setLimit(5);
        listNameRequest.setOffset(1);
        MvcResult result = RequestTypeIntegrationHelper.getRequestTypeExact(mvc, listNameRequest);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    public void getRequestTypeSuccessWithExactNameReturnSizeObjectResponse() throws Exception {
        Gson gson = new Gson();
        ListNameRequest listNameRequest = new ListNameRequest();
        List<String> name = new ArrayList<>();
        name.add("urgent");
        name.add("normal");
        listNameRequest.setLstName(name);
        listNameRequest.setLimit(5);
        listNameRequest.setOffset(1);
        MvcResult result = RequestTypeIntegrationHelper.getRequestTypeExact(mvc, listNameRequest);
        Object object = gson.fromJson(result.getResponse().getContentAsString(), Object.class);
        ArrayList list = (ArrayList) ((LinkedTreeMap) object).get("manager");
        assertThat(list).isNotNull();
    }

    @Test
    public void getRequestTypeSuccessWithExactNameWithEmptyName() throws Exception {
        ListNameRequest listNameRequest = new ListNameRequest();
        List<String> name = new ArrayList<>();
        listNameRequest.setLstName(name);
        listNameRequest.setLimit(5);
        listNameRequest.setOffset(1);
        MvcResult result = RequestTypeIntegrationHelper.getRequestTypeExact(mvc, listNameRequest);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    public void getRequestTypeWithNotExactName() throws Exception {
        Gson gson = new Gson();
        ListNameRequest listNameRequest = new ListNameRequest();
        List<String> name = new ArrayList<>();
        name.add("Ba");
        listNameRequest.setLstName(name);
        listNameRequest.setLimit(5);
        listNameRequest.setOffset(1);
        MvcResult result = RequestTypeIntegrationHelper.getRequestTypeExact(mvc, listNameRequest);
        Object object = gson.fromJson(result.getResponse().getContentAsString(), Object.class);
        ArrayList list = (ArrayList) ((LinkedTreeMap) object).get("manager");
        assertThat(list.size()).isEqualTo(0);
    }

    //update
    @Test
    public void shouldUpdateRequestTypeSuccessWithUniqueName() throws Exception {
        ManagerRequestType managerRequestType = new ManagerRequestType("RequestType" + System.currentTimeMillis(), "unittest");
        MvcResult result = RequestTypeIntegrationHelper.updateRequestType(mvc, managerRequestType);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    public void shouldUpdateRequestTypeSuccessWithNameLikeDescription() throws Exception {
        ManagerRequestType managerRequestType = new ManagerRequestType(1L, "unittest" + System.currentTimeMillis(), "unittest"+ System.currentTimeMillis());
        MvcResult result = RequestTypeIntegrationHelper.updateRequestType(mvc, managerRequestType);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    //delete
    @Test
    public void shouldDeleteOneRequestTypeSuccess() throws Exception {
        Gson gson = new Gson();
        ManagerRequestType RequestTypeRequest = new ManagerRequestType("RequestType" + System.currentTimeMillis()+"one", "unittest");
        ManagerRequestType managerRequestType = gson.fromJson(createRequestType(RequestTypeRequest).getResponse().getContentAsString(),ManagerRequestType.class);
        System.out.println("Day laaaaaaaaa:" +managerRequestType.getId());
        DeleteDTO dto = new DeleteDTO();
        Long[] ids = {managerRequestType.getId()};
        dto.setIds(ids);
        MvcResult result = RequestTypeIntegrationHelper.deleteRequestType(mvc,dto);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    @Test
    public void shouldDeleteManyRequestTypeSuccess() throws Exception {
        Gson gson = new Gson();
        ManagerRequestType RequestTypeRequest = new ManagerRequestType("RequestType" + System.currentTimeMillis() +"test1", "unittest");
        ManagerRequestType RequestTypeRequest1 = new ManagerRequestType("RequestType1" + System.currentTimeMillis() +"test2", "unittest");
        ManagerRequestType managerRequestType1 = gson.fromJson(createRequestType(RequestTypeRequest).getResponse().getContentAsString(),ManagerRequestType.class);
        ManagerRequestType managerRequestType2 = gson.fromJson(createRequestType(RequestTypeRequest1).getResponse().getContentAsString(),ManagerRequestType.class);
        System.out.println("Day laaaaaaaaa:" +managerRequestType1.getId() + "," + managerRequestType2.getId());
        DeleteDTO dto = new DeleteDTO();
        Long[] ids = {managerRequestType1.getId(),managerRequestType2.getId()};
        dto.setIds(ids);
        MvcResult result = RequestTypeIntegrationHelper.deleteRequestType(mvc,dto);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    public MvcResult createRequestType(ManagerRequestType RequestTypeRequest) throws Exception{
        return RequestTypeIntegrationHelper.createRequestType(mvc, RequestTypeRequest);
    }
    
}
