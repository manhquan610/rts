package com.cmc.rts.helper;

import com.cmc.rts.dto.request.CVRequest;
import com.cmc.rts.dto.request.InterviewRequest;
import com.cmc.rts.entity.CandidateStateEnum;
import com.cmc.rts.entity.Interview;
import com.google.gson.Gson;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class InterviewIntegrationHelper {
    public static MvcResult createInterview(MockMvc mvc, InterviewRequest interviewRequest) throws Exception{
        Gson gson = new Gson();
        String request = gson.toJson(interviewRequest);
        return mvc.perform(post("/createInterview")
                 .contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andReturn();
    }
    public static MvcResult getAllInterview(MockMvc mvc,Integer requestId) throws Exception{
        String url = "/interview?request_id="+requestId;
        return mvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
    }
    public static MvcResult getInterviewByCandidateIdAndRequestId(MockMvc mvc,Integer requestId, Integer candidateId) throws Exception{
        String url = "/interview/get?candidate_id="+candidateId+"&request_id="+requestId;
        return mvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
    }
    public static MvcResult getAllInterviewByInterviewerName(MockMvc mvc,String name) throws Exception{
        String url = "/interviewer?name="+name;
        return mvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
    }
    public static MvcResult getAllInterviewOneWeek(MockMvc mvc,String startDate, String endDate, String nameSearch) throws Exception{
        String url = "/interviewer/getInterview/timeline?endDate="+startDate+"&startDate="+endDate+"&text_search="+nameSearch;
        return mvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
    }
}
