package com.cmc.rts.helper;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
public class GroupIntegrationHelper {
    public static MvcResult getAllGroup(MockMvc mvc, String name, Integer limit, Integer offset) throws Exception{
        String url = "/manager-group?limit="+limit+"&name="+name+"&offset="+offset;
        return mvc.perform(get(url)
                .header("Content-Type", "application/json")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
    }
}
