package com.cmc.rts.helper;

import com.cmc.rts.dto.request.CVRequest;
import com.cmc.rts.entity.CandidateStateEnum;
import com.google.gson.Gson;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

public class CVIntegrationHelper {
    public static MvcResult getAllCV(MockMvc mvc, CandidateStateEnum candidateStateEnum, String searchString, Integer limit, Integer offset) throws Exception{
        String url = "/allCV?candidateState="+candidateStateEnum+"&limit="+limit+"&offset="+offset+"&searchString="+searchString;
        return mvc.perform(post(url)
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
    }
    public static MvcResult createCV(MockMvc mvc, CVRequest cvRequest) throws Exception{
        Gson gson = new Gson();
        String request = gson.toJson(cvRequest);
        return mvc.perform(post("/createCV")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andReturn();
    }

    public static MvcResult updateCV(MockMvc mvc, CVRequest cvRequest) throws Exception{
        Gson gson = new Gson();
        String request = gson.toJson(cvRequest);
        return mvc.perform(put("/updateCV")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andReturn();
    }

}
