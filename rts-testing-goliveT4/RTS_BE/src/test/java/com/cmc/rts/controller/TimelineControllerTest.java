package com.cmc.rts.controller;

import com.cmc.rts.dto.response.TimelineDTO;
import com.cmc.rts.helper.TimelineIntegrationHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class TimelineControllerTest {
    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper mapper;

    @Test
    public void givenTimeline_whenGetTimeline_thenStatus200() throws Exception{
        Long requestId = 434L;
        Long candidateId = 377L;
        MvcResult result = TimelineIntegrationHelper.getTimeline(mvc,requestId,candidateId);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
    }
    @Test
    public void givenTimeline_whenGetTimeline_thenStatus200_requestIdNotExist() throws Exception{
        Long requestId = 4388L;
        Long candidateId = 377L;
        MvcResult result = TimelineIntegrationHelper.getTimeline(mvc,requestId,candidateId);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
        TimelineDTO timelineDTO = mapper.readValue(result.getResponse().getContentAsByteArray(),TimelineDTO.class);
        assertEquals(0,timelineDTO.getCommentDTO().size());
    }
    @Test
    public void givenTimeline_whenGetTimeline_thenStatus200_candidateIdNotExist() throws Exception{
        Long requestId = 434L;
        Long candidateId = 3777L;
        MvcResult result = TimelineIntegrationHelper.getTimeline(mvc,requestId,candidateId);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
        TimelineDTO timelineDTO = mapper.readValue(result.getResponse().getContentAsByteArray(),TimelineDTO.class);
        assertEquals(0,timelineDTO.getCommentDTO().size());
    }
    @Test
    public void givenTimeline_whenGetTimeline_thenStatus200_candidateIdAndRequestIdNotExist() throws Exception{
        Long requestId = 4343L;
        Long candidateId = 3777L;
        MvcResult result = TimelineIntegrationHelper.getTimeline(mvc,requestId,candidateId);
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
        TimelineDTO timelineDTO = mapper.readValue(result.getResponse().getContentAsByteArray(),TimelineDTO.class);
        assertEquals(0,timelineDTO.getCommentDTO().size());
    }

}
