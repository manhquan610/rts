INSERT INTO manager_area(id,description,name) VALUES
(1, '', 'Hà Nội'),
(2, '', 'Hồ Chí Minh'),
(3, '', 'Đà Nẵng'),
(4, '', 'Japan');

INSERT INTO manager_job_type(id,description,name) VALUES
(1, '' , 'Developer'),
(2, '' , 'Accounting / Finance'),
(3, '', 'Marketing'),
(4, '' , 'Project Management');

INSERT INTO manager_priority(id,description,name) VALUES
(1,'' , 'high'),
(2, '' , 'medium'),
(3, '' , 'low');

INSERT INTO manager_experience(id,description,name) VALUES
(1, '' , 'fresher'),
(2, '' , 'junior'),
(3, '' , 'middle');

INSERT INTO manager_request_type(id,description,name) VALUES
(1,'' , 'urgent'),
(2, '' , 'normal');
INSERT INTO manager_major(id,description,name) VALUES
(1,'' , 'Marketing'),
(2, '' , 'Quản trị nhân lực'),
(3, '' , 'Dev PHP');
insert into request (id, description, name, manager_major_id, benefit, certificate, created_by, created_date, deadline, last_updated, last_updated_by, number, others, salary, department_id, manager_experience_id, manager_priority_id, manager_request_type_id, project_id, deleted, status, version, code, manager_group_id, reject_reason_id, request_status_name, published, deadline_str, manager_name,  skill_name_id, area_id, job_type_id, share_job, last_group_submit_ldap, requirement)
values  (1000, '<p>aba</p>', 'demo ti', null , '', 'string', 'nlqhuy', '2021-10-25 17:38:00', '2021-11-30 00:00:00', '2021-10-25 17:38:00', 'nlqhuy', 0, null, '10000000',  6, 1, 1, 1, null , null, null, 0, null, 3, null, 'Shared', null, '2021-11-30', null, '4',  1, 1, true, null, null);
insert into interview (id, created_by, created_date, deleted, description, last_updated, last_updated_by, status, version, comment, end_time, interviewer, name, start_time, title, candidate_id, location_id, request_id)
values  (1, 'nlqhuy', '2021-11-02 10:32:46', null, null, '2021-11-02 10:32:46', null, null, 0, 'abc', null, 'tvtai,tvtai1,', null, '2021-11-10 08:32:00', 'Phỏng vấn test 2', 279, null, 1000),
        (2, 'nptien1', '2021-11-03 07:13:00', null, null, '2021-11-03 07:13:00', null, null, 0, '', null, 'tvtai,tvtai1,', null, '2021-11-03 22:10:00', 'Hẹn phỏng vấn ứng viên', 377, null, 1000),
        (3, 'nptien1', '2021-11-03 08:03:52', null, null, '2021-11-03 08:03:52', null, null, 0, 'fdgf', null, 'tvtai,tvtai1,', null, '2021-11-04 20:05:00', 'Hẹn phỏng vấn ứng viên', 379, null, 1000),
        (4, 'nptien1', '2021-11-03 08:53:42', null, null, '2021-11-03 08:53:42', null, null, 0, null, null, 'tvtai,tvtai1,', null, '2021-11-05 01:50:00', 'Hẹn phỏng vấn ứng viên', 340, null, 1000),
        (5, 'nptien1', '2021-11-03 09:04:26', null, null, '2021-11-03 09:04:26', null, null, 0, 'abcd', null, 'tvtai,tvtai1,', null, '2021-11-12 01:03:00', 'Hẹn phỏng vấn ứng viên', 459, null, 1000),
        (6, 'nptien1', '2021-11-03 09:07:42', null, null, '2021-11-03 09:07:42', null, null, 0, null, null, 'tvtai,tvtai1,', null, '2021-11-10 01:05:00', 'Hẹn phỏng vấn ứng viên', 459, null, 1000),
        (7, 'nptien1', '2021-11-03 09:08:46', null, null, '2021-11-03 09:08:46', null, null, 0, 'abcd', null, 'tvtai,tvtai1,', null, '2021-11-10 01:05:00', 'Hẹn phỏng vấn ứng viên', 459, null, 1000),
        (8, 'nptien1', '2021-11-03 09:09:30', null, null, '2021-11-03 09:09:30', null, null, 0, 'abcd', null, 'tvtai,tvtai1,', null, '2021-11-10 01:05:00', 'Hẹn phỏng vấn ', 459, null, 1000),
        (9, 'nptien1', '2021-11-03 09:10:00', null, null, '2021-11-03 09:10:00', null, null, 0, 'abcd', null, 'tvtai,tvtai1,', null, '2021-11-10 01:05:00', 'Hẹn phỏng vấn ', 459, null, 1000),
        (10, 'nptien1', '2021-11-03 09:10:00', null, null, '2021-11-03 09:10:00', null, null, 0, '', null, 'tvtai,tvtai1,', null, '2021-11-10 01:05:00', 'Hẹn phỏng vấn ', 459, null, 1000),
        (11, 'nptien1', '2021-11-03 09:10:23', null, null, '2021-11-03 09:10:23', null, null, 0, 'ghgfh', null, 'tvtai,tvtai1,', null, '2021-11-10 01:05:00', 'Hẹn phỏng vấn ', 459, null, 1000);