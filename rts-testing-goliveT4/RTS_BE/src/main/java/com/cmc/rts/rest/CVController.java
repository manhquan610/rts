package com.cmc.rts.rest;

import com.cmc.rts.dto.request.CVRequest;
import com.cmc.rts.dto.response.CVRootResponse;
import com.cmc.rts.dto.response.CandidateCheckDTO;
import com.cmc.rts.entity.CandidateStateEnum;
import com.cmc.rts.response.SingleResponseDTO;
import com.cmc.rts.service.CVService;
import com.cmc.rts.service.CandidateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;


@RestController
public class CVController {
    @Autowired
    private CVService CVService;
    @Autowired
    private CandidateService candidateService;

    @PostMapping(value = "/allCV")
    public ResponseEntity<CVRootResponse> getAllCV(@RequestParam(required = false) CandidateStateEnum candidateState,
                                                   @RequestParam(required = false, defaultValue = "") String searchString,
                                                   @RequestParam(required = false) String hasCV,
                                                   @RequestParam(required = false) String startDate,
                                                   @RequestParam(required = false) String endDate,
                                                   @RequestParam(required = false, defaultValue = "1") Integer limit,
                                                   @RequestParam(required = false, defaultValue = "15") Integer offset) {
        return new ResponseEntity<>(CVService.getAllCV(candidateState, startDate, endDate, searchString, limit, offset, hasCV), HttpStatus.OK);
    }

    @PostMapping(value = "/createCV")
    public ResponseEntity<?> createCV(@RequestBody CVRequest cvRequest) throws Exception {
        if (cvRequest.getFullName() == null)
            return new ResponseEntity<>("Name không được null!", HttpStatus.NOT_FOUND);
        if (cvRequest.getEmail() == null)
            return new ResponseEntity<>("Email không được null!", HttpStatus.NOT_FOUND);
        if (cvRequest.getPhone() == null)
            return new ResponseEntity<>("Phone không được null!", HttpStatus.NOT_FOUND);
        if (cvRequest.getApplySince() == null)
            return new ResponseEntity<>("Apply Since không được null!", HttpStatus.NOT_FOUND);
        return CVService.createCV(cvRequest);
    }

    @PutMapping(value = "/updateCV")
    public ResponseEntity<?> updateCV(@RequestBody CVRequest cvRequest) throws Exception {
        try {
            if (cvRequest.getFullName() == null)
                return new ResponseEntity<>("Name không được null!", HttpStatus.NOT_FOUND);
            if (cvRequest.getPhone() == null)
                return new ResponseEntity<>("Phone không được null!", HttpStatus.NOT_FOUND);
            if (cvRequest.getApplySince() == null)
                return new ResponseEntity<>("Apply Since không được null!", HttpStatus.NOT_FOUND);
            return ResponseEntity.ok(CVService.updateCV(cvRequest));
        } catch (Exception e) {
            return new ResponseEntity<>("Error server update CV", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/candidate-check")
    public ResponseEntity<SingleResponseDTO<CandidateCheckDTO>> candidateCheck(@RequestParam String candidateCheck) {
        SingleResponseDTO<CandidateCheckDTO> response = candidateService.candidateCheck(candidateCheck);
        return ResponseEntity.status(Objects.requireNonNull(HttpStatus.OK)).body(response);
    }
}
