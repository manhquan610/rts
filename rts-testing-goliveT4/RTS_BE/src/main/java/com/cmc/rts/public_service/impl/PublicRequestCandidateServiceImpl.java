package com.cmc.rts.public_service.impl;

import com.cmc.rts.dto.public_dto.RequestCandidateDTO;
import com.cmc.rts.entity.CandidateStateEnum;
import com.cmc.rts.entity.v2.V2Request;
import com.cmc.rts.entity.v2.V2RequestCandidateLog;
import com.cmc.rts.entity.v2.V2RequestReportTicket;
import com.cmc.rts.public_service.PublicRequestCandidateService;
import com.cmc.rts.repository.v2.V2RequestCandidateLogRepository;
import com.cmc.rts.repository.v2.V2RequestReportTicketRepository;
import com.cmc.rts.repository.v2.V2RequestRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.sql.Timestamp;
import java.time.*;

@Service
@Slf4j
public class PublicRequestCandidateServiceImpl implements PublicRequestCandidateService {

    @Autowired
    private V2RequestReportTicketRepository v2RequestReportTicketRepository;

    @Autowired
    private V2RequestRepository v2RequestRepository;

    @Autowired
    private V2RequestCandidateLogRepository v2RequestCandidateLogRepository;

    @Override
    public void requestCandidateLog(RequestCandidateDTO dto) {
        V2Request v2Request = v2RequestRepository.findV2RequestById(Long.parseLong(dto.getJobRtsId()));
        V2RequestCandidateLog v2RequestCandidateLog = v2RequestCandidateLogRepository
                .findV2RequestCandidateLogByRequestIdAndCandidateIdAndCurrentStepAndPreviousStep(Long.parseLong(dto.getJobRtsId()), Long.parseLong(dto.getCandidateId()),
                        String.valueOf(dto.getCurrentStep()), String.valueOf(dto.getPreviousStep()));

        ZoneId zone = ZoneId.of("Asia/Ho_Chi_Minh");
        Timestamp t = new Timestamp(System.currentTimeMillis());
        OffsetDateTime offsetDateTime = ZonedDateTime
                .ofInstant(Instant.ofEpochMilli(t.getTime()), zone)
                .toOffsetDateTime();

        if (ObjectUtils.isEmpty(v2RequestCandidateLog)) {
            v2RequestCandidateLog = V2RequestCandidateLog.builder()
                    .candidateId(Long.parseLong(dto.getCandidateId()))
                    .requestId(Long.parseLong(dto.getJobRtsId()))
                    .previousStep(dto.getPreviousStep().toString())
                    .currentStep(dto.getCurrentStep().toString())
                    .taLead(v2Request.getCreatedBy())
                    .taMember(dto.getTaMember())
                    .createdBy(dto.getCreatedBy())
                    .createdDate(Timestamp.from(offsetDateTime.toInstant()))
                    .build();
            v2RequestCandidateLogRepository.save(v2RequestCandidateLog);
        }
    }

    public void requestStatisticalCalculation(RequestCandidateDTO dto) {
        V2RequestReportTicket v2RequestReportTicket = v2RequestReportTicketRepository.findV2RequestReportTicketByRequestId(Long.parseLong(dto.getJobRtsId()));
        V2Request v2Request = v2RequestRepository.findV2RequestById(Long.parseLong(dto.getJobRtsId()));
    }

    private void requestStatisticalCalculationProcess(RequestCandidateDTO dto, V2RequestReportTicket reportTicket) {
        switch (dto.getCurrentStep()) {
            case qualify:
                V2RequestReportTicket.Qualify qualify = reportTicket.getQualify();
                qualify.setQualifying(qualify.getQualifying() + 1);
                break;
            case confirm:
                V2RequestReportTicket.Confirm confirm = reportTicket.getConfirm();
                confirm.setConfirming(confirm.getConfirming() + 1);
                break;
            case interview:
                V2RequestReportTicket.Interview interview = reportTicket.getInterview();
                interview.setInterviewing(interview.getInterviewing() + 1);
                break;
            case offer:
                V2RequestReportTicket.Offer offer = reportTicket.getOffer();
                offer.setOffering(offer.getOffering() + 1);
                break;
            case onboard:
                reportTicket.setOnBoarding(reportTicket.getOnBoarding() + 1);
                break;
            default:
                break;
        }

        switch (dto.getPreviousStep()) {
            case qualify:
                V2RequestReportTicket.Qualify qualify = reportTicket.getQualify();
                qualify.setQualifying(qualify.getQualifying() - 1);
                break;
            case confirm:
                V2RequestReportTicket.Confirm confirm = reportTicket.getConfirm();
                confirm.setConfirming(confirm.getConfirming() - 1);
                break;
            case interview:
                V2RequestReportTicket.Interview interview = reportTicket.getInterview();
                interview.setInterviewing(interview.getInterviewing() - 1);
                break;
            case offer:
                V2RequestReportTicket.Offer offer = reportTicket.getOffer();
                offer.setOffering(offer.getOffering() - 1);
                break;
            case onboard:
                reportTicket.setOnBoarding(reportTicket.getOnBoarding() - 1);
                break;
            default:
                break;
        }
    }

}
