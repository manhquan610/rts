package com.cmc.rts.rest;

import com.cmc.rts.dto.response.ManageKpiDTO;
import com.cmc.rts.service.ManageKPIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.util.IOUtils;

@RestController
@RequestMapping("/manage-kpi")
public class ManageKPIController {
    private final ManageKPIService manageKPIService;
    @Autowired
    public ManageKPIController(ManageKPIService manageKPIService){
        this.manageKPIService = manageKPIService;
    }

    @GetMapping
    public ResponseEntity<Page<ManageKpiDTO>> getManageKPIByNameAndYear(@RequestParam(value = "name", required = false, defaultValue = "") String name,
                                                               @RequestParam(value = "year") String year,
                                                               @RequestParam(value = "offset") int offset,
                                                               @RequestParam(value = "limit") int limit) {
        Pageable pageable = new PageRequest(offset - 1, limit);
        Page<ManageKpiDTO> manageKpiDTOs = manageKPIService.getAllByNameAndYear(name,year,pageable);
        return new ResponseEntity<>(manageKpiDTOs, HttpStatus.OK);
    }

    @PatchMapping("/update-kpi")
    public ResponseEntity<?> updateKpiTa(@Validated @RequestBody ManageKpiDTO manageKpiDTO, BindingResult bindingResult,
                                         @RequestParam(value = "username") String username,
                                         @RequestParam(value = "year") String year) {
        new ManageKpiDTO().validate(manageKpiDTO, bindingResult);
        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>(bindingResult.getFieldError(), HttpStatus.NOT_ACCEPTABLE);
        }
        manageKPIService.updateManageKpi(username, year, manageKpiDTO);
        return new ResponseEntity<>(manageKpiDTO, HttpStatus.OK);
    }


    @GetMapping("/report")
	public void reportKpi(HttpServletResponse response,
			@RequestParam(value = "name", required = false, defaultValue = "") String name,
			@RequestParam(value = "year") String year) throws ParseException, IOException {

		List<ManageKpiDTO> manageKpiList = manageKPIService.getAllByNameAndYear(name, year);

		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd_HHmmss");
		Date date = new Date();
		String filename = "Report_KPI_" + formatter.format(date) + ".xlsx";

		ByteArrayInputStream byteArrayInputStream = manageKPIService.exportExcel(manageKpiList);
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", "attachment; filename=" + filename);
		IOUtils.copy(byteArrayInputStream, response.getOutputStream());
	}
    
}
