package com.cmc.rts.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "manage_kpi")
public class ManageKpi extends BaseDomain {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "username")
    private String username;
    @Column(name = "full_name")
    private String fullName;
    @Column(name = "jan_quantity", columnDefinition = "int default 0", insertable = false)
    private Long janQuantity;
    @Column(name = "feb_quantity", columnDefinition = "int default 0", insertable = false)
    private Long febQuantity;
    @Column(name = "mar_quantity", columnDefinition = "int default 0", insertable = false)
    private Long marQuantity;
    @Column(name = "apr_quantity", columnDefinition = "int default 0", insertable = false)
    private Long aprQuantity;
    @Column(name = "may_quantity", columnDefinition = "int default 0", insertable = false)
    private Long mayQuantity;
    @Column(name = "jun_quantity", columnDefinition = "int default 0", insertable = false)
    private Long junQuantity;
    @Column(name = "jul_quantity", columnDefinition = "int default 0", insertable = false)
    private Long julQuantity;
    @Column(name = "aug_quantity", columnDefinition = "int default 0", insertable = false)
    private Long augQuantity;
    @Column(name = "sep_quantity", columnDefinition = "int default 0", insertable = false)
    private Long sepQuantity;
    @Column(name = "oct_quantity", columnDefinition = "int default 0", insertable = false)
    private Long octQuantity;
    @Column(name = "nov_quantity", columnDefinition = "int default 0", insertable = false)
    private Long novQuantity;
    @Column(name = "dec_quantity", columnDefinition = "int default 0", insertable = false)
    private Long decQuantity;
}
