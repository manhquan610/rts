package com.cmc.rts.service.impl;

import com.cmc.rts.dto.request.CreateAssignRequest;
import com.cmc.rts.dto.requestDto.*;
import com.cmc.rts.dto.response.*;
import com.cmc.rts.entity.*;
import com.cmc.rts.entity.v2.V2ManagerJobLevel;
import com.cmc.rts.entity.v2.V2Request;
import com.cmc.rts.entity.v2.V2RequestAssign;
import com.cmc.rts.entity.v2.V2RequestManagerJobLevels;
import com.cmc.rts.exception.CustomException;
import com.cmc.rts.repository.*;
import com.cmc.rts.repository.v2.V2ManagerJobLevelRepository;
import com.cmc.rts.repository.v2.V2RequestAssignRepository;
import com.cmc.rts.repository.v2.V2RequestManagerJobLevelRepository;
import com.cmc.rts.repository.v2.V2RequestRepository;
import com.cmc.rts.service.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.messaging.MessagingException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.IOException;
import java.lang.System;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static com.cmc.rts.constant.Constant.languagueMap;
import static com.cmc.rts.entity.RequestStatusEnum.*;
import static com.cmc.rts.utils.Constants.ROLE.*;


@Service
@Slf4j
public class RequestServiceImpl implements RequestService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RequestServiceImpl.class);
    private final ModelMapper modelMapper = new ModelMapper();
    private final String CANDIDATE_TOKEN = "Bearer Oo2V-DGze-";
    @Autowired
    RequestRepository requestRepository;
    @Autowired
    ProjectRepository projectRepository;
    @Autowired
    DepartmentRepository departmentRepository;
    @Autowired
    ManagerGroupRepository managerGroupRepository;
    @Autowired
    SkillNameService skillNameService;
    @Autowired
    ManagerPriorityService managerPriorityService;
    @Autowired
    ManagerExperienceService managerExperienceService;
    @Autowired
    LanguageService languageService;
    @Autowired
    ManagerRequestTypeService managerRequestTypeService;
    @Autowired
    ManagerMajorService managerMajorService;
    @Autowired
    ManagerGroupService managerGroupService;
    @Autowired
    DepartmentService departmentService;
    @Autowired
    ProjectService projectService;
    @Autowired
    CommonService commonService;
    @Autowired
    LogExceptionRepository logExceptionRepository;
    @Autowired
    RejectReasonRepository rejectReasonRepository;
    @Autowired
    RtsService rtsService;
    @Autowired
    UserInfoRepository userInfoRepository;
    @Autowired
    RequestSubmitRepository requestSubmitRepository;
    @Autowired
    RequestAssignRepository requestAssignRepository;
    @Autowired
    InterviewServiceImpl interviewService;
    @Autowired
    ManagerJobLevelRepository jobLevelRepository;
    @Autowired
    CandidateService candidateService;

    @Autowired
    V2RequestRepository v2RequestRepository;

    @Autowired
    V2RequestAssignRepository v2RequestAssignRepository;

    @Autowired
    ManagerAreaRepository managerAreaRepository;

    @Autowired
    ManagerJobTypeRepository managerJobTypeRepository;

    @Autowired
    ManagerPriorityRepository managerPriorityRepository;

    @Autowired
    V2RequestManagerJobLevelRepository v2RequestManagerJobLevelRepository;

    @Autowired
    V2ManagerJobLevelRepository v2ManagerJobLevelRepository;

    @Value("${cmc.global.api.url}")
    private String cmcGlobalApiUrl;
    @Value("${cmc.global.hrmember.path}")
    private String getHRMember;
    @Value("${hrms.api.url}")
    private String hrmsApiUrl;
    @Value("${cmc.global.gleadbydulead.path}")
    private String getGleadByDulead;
    @Value("${candidate.list.api.url}")
    private String candidateApiUrl;
    @Value("${candidate.getTotal.step}")
    private String totalCandidateStep;

    @Autowired
    private UserDetailsService userDetailsService;

    private final String CMC_GLOBAL_TOKEN = "Bearer eyJTeXN0ZW0iOiJSVFMiLCJLZXkiOiJHeGFUYTVTK0lSMGx0OTBJYmZnQVJRPT06Q29CMG91OWZmOHptMCswQ2Z3S0Q5dz09In0=";
    private final String HRMS_TOKEN = "Bearer f336202f1d5a9d9f8427ab1e237099e8";
    private final ObjectMapper objectMapper = new ObjectMapper();

    @PersistenceContext
    private EntityManager entityManager;


    @Transactional
    public void updateApiLanguageSkill() throws IOException {
        languageService.getFromApi();
        skillNameService.getFromApi();
    }

    @Transactional
    public Request saveRequestDetails(Request request, RequestDTO requestDTO) {
        try {
            String errorMessage;
            if (requestDTO.getDeadline() != null && !requestDTO.getDeadline().isEmpty()) {
                LocalDate localDate = LocalDate.parse(requestDTO.getDeadline());
                ZoneId systemTimeZone = ZoneId.systemDefault();
                ZonedDateTime zonedDateTime = localDate.atStartOfDay(systemTimeZone);
                if (localDate.isBefore(LocalDate.now())) {
                    errorMessage = "Ngày Deadline không được nhỏ hơn ngày hiện tại!";
                    throw new Exception(throwException(errorMessage));
                } else {
                    request.setDeadline(Date.from(zonedDateTime.toInstant()));
                    request.setDeadlineStr(localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                }

            } else {
                errorMessage = "Deadline không được null!";
                throw new Exception(throwException(errorMessage));
            }
            if (requestDTO.getTitle() != null && !requestDTO.getTitle().isEmpty()) {
                request.setName(requestDTO.getTitle());
            } else {
                errorMessage = "Title không được null!";
                throw new Exception(throwException(errorMessage));
            }

            if (String.valueOf(requestDTO.getNumber()) != null && !String.valueOf(requestDTO.getNumber()).isEmpty()) {
                request.setNumber(requestDTO.getNumber());
            } else {
                errorMessage = "Number không được null!";
                throw new Exception(throwException(errorMessage));
            }

            if (requestDTO.getDescription() != null && !requestDTO.getDescription().isEmpty()) {
                request.setDescription(requestDTO.getDescription());
            } else {
                errorMessage = "Description không được null!";
                throw new Exception(throwException(errorMessage));
            }

            if (requestDTO.getSkillId() != null && !requestDTO.getSkillId().isEmpty()) {
                try {
                    List<Long> skillIds = Arrays.stream(requestDTO.getSkillId().split(",")).map(x -> Long.valueOf(x)).collect(Collectors.toList());
                    request.setSkillNameId(requestDTO.getSkillId());
                } catch (CustomException exception) {
                    errorMessage = "Skill không được null!";
                    throw new Exception(throwException(errorMessage));
                }
            } else {
                errorMessage = "Skill không được null!";
                throw new Exception(throwException(errorMessage));
            }

            if (requestDTO.getPriorityId() != null && !requestDTO.getPriorityId().toString().isEmpty()) {
                request.setManagerPriority(managerPriorityService.findOne(requestDTO.getPriorityId()));
            } else {
                errorMessage = "Priority không được null!";
                throw new Exception(throwException(errorMessage));
            }
            if (requestDTO.getExperienceId() != null && !requestDTO.getExperienceId().toString().isEmpty()) {
                request.setManagerExperience(managerExperienceService.findOne(requestDTO.getExperienceId()));
            } else {
                errorMessage = "Experience không được null!";
                throw new Exception(throwException(errorMessage));
            }
            if (requestDTO.getRequestTypeId() != null && !requestDTO.getRequestTypeId().toString().isEmpty()) {
                request.setManagerRequestType(managerRequestTypeService.findOne(requestDTO.getRequestTypeId()));
            } else {
                errorMessage = "Request Type không được null!";
                throw new Exception(throwException(errorMessage));
            }
            if (requestDTO.getManagerGroupId() != null && !requestDTO.getManagerGroupId().toString().isEmpty()) {
                request.setManagerGroupId(requestDTO.getManagerGroupId());
            } else {
                errorMessage = "Group không được null!";
                throw new Exception(throwException(errorMessage));
            }

            if (requestDTO.getDepartmentId() != null && !requestDTO.getDepartmentId().toString().isEmpty()) {
                request.setDepartmentId(requestDTO.getDepartmentId());
            } else {
                request.setDepartmentId(requestDTO.getManagerGroupId());
            }

            request.setProjectId(requestDTO.getProjectId());

            // Cac truong khong bat buoc
            request.setCertificate(requestDTO.getCertificate());
            if (requestDTO.getMajor() != null && !requestDTO.getMajor().isEmpty()) {
                request.setManagerMajor(managerMajorService.findOne(Long.valueOf(requestDTO.getMajor())));
            }
            request.setOthers(requestDTO.getOthers());
            request.setSalary(requestDTO.getSalary());
            request.setBenefit(requestDTO.getBenefit());
            request.setWorkingType(requestDTO.getWorkingType());
            request.setArea(requestDTO.getArea());
            request.setJobType(requestDTO.getJobType());
            request.setCode(requestDTO.getCode());
            request.setCodePosition(requestDTO.getCodePosition());

            if (requestDTO.getLanguageId() != null && !requestDTO.getLanguageId().isEmpty()) {
                try {
                    Set<Long> languageIds = Arrays.stream(requestDTO.getLanguageId().split(",")).map(x -> Long.valueOf(x)).collect(Collectors.toSet());
                    request.setLanguageId(requestDTO.getLanguageId());
                } catch (CustomException e) {
                    throw new CustomException("Không tìm thấy language với id: " + requestDTO.getLanguageId());
                }
            }
            if (request.getRequestStatusName() != null) {
                if (request.getRequestStatusName() == RequestStatusEnum.Rejected) {
                    setNewStatusRequest(request);
                }
            } else {
                setNewStatusRequest(request);
            }
            request.setRequirement(requestDTO.getRequirement());
            requestRepository.save(request);
        } catch (Exception e) {
            e.printStackTrace();
            throwException(e.getMessage());
        } finally {
            return request;
        }

    }

    @Transactional
    public Request saveAllRequestDetails(Request request, RequestDTO requestDTO) {
        try {

            int length;
            String errorMessage;
            System.out.println(requestDTO.getDeadline());
            System.out.println(!requestDTO.getDeadline().isEmpty());
            if (requestDTO.getDeadline() != null && !requestDTO.getDeadline().isEmpty()) {
                LocalDate localDate = LocalDate.parse(requestDTO.getDeadline());
                ZoneId systemTimeZone = ZoneId.systemDefault();
                ZonedDateTime zonedDateTime = localDate.atStartOfDay(systemTimeZone);
                request.setDeadline(Date.from(zonedDateTime.toInstant()));
                request.setDeadlineStr(localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            } else {
                errorMessage = "Deadline không được null!";
                throw new Exception(throwException(errorMessage));
            }
            if (requestDTO.getStartDate() != null && !requestDTO.getStartDate().isEmpty()) {
                LocalDate localDate = LocalDate.parse(requestDTO.getStartDate());
                ZoneId systemTimeZone = ZoneId.systemDefault();
                ZonedDateTime zonedDateTime = localDate.atStartOfDay(systemTimeZone);
                request.setCreatedDate(Timestamp.from(zonedDateTime.toInstant()));
            } else {
                errorMessage = "start date không được null!";
                throw new Exception(throwException(errorMessage));
            }
            if (requestDTO.getSalary() != null && !requestDTO.getSalary().isEmpty()) {
                try {
                    Long.parseLong(requestDTO.getSalary());
                } catch (Exception e) {
                    throw new Exception(throwException("Salary is number!"));
                }
            }
            if (requestDTO.getTitle() != null && !requestDTO.getTitle().isEmpty()) {
                request.setName(requestDTO.getTitle());
            } else {
                errorMessage = "Title không được null!";
                throw new Exception(throwException(errorMessage));
            }

//            if (requestDTO.getPositionId() != null && !requestDTO.getPositionId().toString().isEmpty()) {
//                request.setManagerPosition(managerPositionService.findOne(requestDTO.getPositionId()));
//            } else {
//                errorMessage = "Position không được null!";
//                throw new Exception(throwException(errorMessage));
//            }

            if (String.valueOf(requestDTO.getNumber()) != null && !String.valueOf(requestDTO.getNumber()).isEmpty()) {
                request.setNumber(requestDTO.getNumber());
            } else {
                errorMessage = "Number không được null!";
                throw new Exception(throwException(errorMessage));
            }

            if (requestDTO.getDescription() != null && !requestDTO.getDescription().isEmpty()) {
                request.setDescription(requestDTO.getDescription());
            } else {
                errorMessage = "Description không được null!";
                throw new Exception(throwException(errorMessage));
            }

            if (requestDTO.getSkillId() != null && !requestDTO.getSkillId().isEmpty()) {
                try {
                    List<Long> skillIds = Arrays.stream(requestDTO.getSkillId().split(",")).map(x -> Long.valueOf(x)).collect(Collectors.toList());
                    request.setSkillNameId(requestDTO.getSkillId());
                } catch (CustomException exception) {
                    errorMessage = "Skill không được null!";
                    throw new Exception(throwException(errorMessage));
                }
            } else {
                errorMessage = "Skill không được null!";
                throw new Exception(throwException(errorMessage));
            }

            if (requestDTO.getPriorityId() != null && !requestDTO.getPriorityId().toString().isEmpty()) {
                request.setManagerPriority(managerPriorityService.findOne(requestDTO.getPriorityId()));
            } else {
                errorMessage = "Priority không được null!";
                throw new Exception(throwException(errorMessage));
            }
            if (requestDTO.getExperienceId() != null && !requestDTO.getExperienceId().toString().isEmpty()) {
                request.setManagerExperience(managerExperienceService.findOne(requestDTO.getExperienceId()));
            } else {
                errorMessage = "Experience không được null!";
                throw new Exception(throwException(errorMessage));
            }
            if (requestDTO.getRequestTypeId() != null && !requestDTO.getRequestTypeId().toString().isEmpty()) {
                request.setManagerRequestType(managerRequestTypeService.findOne(requestDTO.getRequestTypeId()));
            } else {
                errorMessage = "Request Type không được null!";
                throw new Exception(throwException(errorMessage));
            }
            if (requestDTO.getManagerGroupId() != null && !requestDTO.getManagerGroupId().toString().isEmpty()) {
                request.setManagerGroupId(requestDTO.getManagerGroupId());
            } else {
                errorMessage = "Group không được null!";
                throw new Exception(throwException(errorMessage));
            }

            if (requestDTO.getDepartmentId() != null && !requestDTO.getDepartmentId().toString().isEmpty()) {
                request.setDepartmentId(requestDTO.getDepartmentId());
            } else {
                errorMessage = "Department không được null!";
                throw new Exception(throwException(errorMessage));
            }

            request.setProjectId(requestDTO.getProjectId());

//            if (requestDTO.getProjectId() != null && !requestDTO.getProjectId().toString().isEmpty()) {
//                request.setProjectId(requestDTO.getProjectId());
//            } else {
//                errorMessage = "Project không được null!";
//                throw new Exception(throwException(errorMessage));
//            }

            // Cac truong khong bat buoc
            request.setCertificate(requestDTO.getCertificate());
            if (requestDTO.getMajor() != null && !requestDTO.getMajor().isEmpty()) {
                request.setManagerMajor(managerMajorService.findOne(Long.valueOf(requestDTO.getMajor())));
            }
            request.setOthers(requestDTO.getOthers());
            request.setSalary(requestDTO.getSalary());
            request.setBenefit(requestDTO.getBenefit());
            request.setWorkingType(requestDTO.getWorkingType());
            request.setArea(requestDTO.getArea());
            request.setJobType(requestDTO.getJobType());
            request.setCode(requestDTO.getCode());
            if (requestDTO.getLanguageId() != null && !requestDTO.getLanguageId().isEmpty()) {
                try {
                    Set<Long> languageIds = Arrays.stream(requestDTO.getLanguageId().split(",")).map(x -> Long.valueOf(x)).collect(Collectors.toSet());
                    request.setLanguageId(requestDTO.getLanguageId());
                } catch (CustomException e) {
                    throw new CustomException("Không tìm thấy language với id: " + requestDTO.getLanguageId());
                }
            }
            if (request.getRequestStatusName() != null) {
                if (request.getRequestStatusName() == RequestStatusEnum.Rejected) {
                    setNewStatusRequest(request);
                }
            } else {
                setNewStatusRequest(request);
            }
            request.setCreatedBy("htanh6");
            request.setRequestStatusName(Approved2);
            request.setRequirement(requestDTO.getRequirement());
            requestRepository.save(request);
        } catch (Exception e) {
            e.printStackTrace();
            throwException(e.getMessage());
        } finally {
            return request;
        }

    }

    public void setNewStatusRequest(Request request) {
        request.setRequestStatusName(Assigned);
    }

    @Override
    public String throwException(RequestDTO requestDTO) {
        String errorMessage;
        if (requestDTO.getDeadline() != null && !requestDTO.getDeadline().isEmpty()) {
            LocalDate localDate = LocalDate.parse(requestDTO.getDeadline());
            if (localDate.isBefore(LocalDate.now())) {
                errorMessage = "Ngày Deadline không được nhỏ hơn ngày hiện tại!";
                return throwException(errorMessage);
            }
        } else {
            errorMessage = "Deadline không được null!";
            return throwException(errorMessage);
        }

        if (requestDTO.getTitle() == null && !requestDTO.getTitle().isEmpty()) {
            errorMessage = "Title không được null!";
            return throwException(errorMessage);
        }

//        if (requestDTO.getPositionId() == null && !requestDTO.getPositionId().toString().isEmpty()) {
//            errorMessage = "Position không được null!";
//            return throwException(errorMessage);
//        }

        if (String.valueOf(requestDTO.getNumber()) == null && !String.valueOf(requestDTO.getNumber()).isEmpty()) {
            errorMessage = "Number không được null!";

            return throwException(errorMessage);
        }

        if (requestDTO.getSalary() != null || !requestDTO.equals("")) {
            try {
                Long.parseLong(requestDTO.getSalary());
            } catch (Exception e) {
                throwException("Salary is number!");
            }
        }

        if (requestDTO.getDescription() == null && !requestDTO.getDescription().isEmpty()) {
            errorMessage = "Description không được null!";

            return throwException(errorMessage);
        }

        if (requestDTO.getSkillId() == null && !requestDTO.getSkillId().isEmpty()) {
            errorMessage = "Skill không được null!";

            return throwException(errorMessage);
        }

        if (requestDTO.getPriorityId() == null) {
            errorMessage = "Priority không được null!";
            return throwException(errorMessage);
        }
        if (requestDTO.getExperienceId() == null) {
            errorMessage = "Experience không được null!";
            return throwException(errorMessage);
        }
        if (requestDTO.getRequestTypeId() == null) {
            errorMessage = "Request Type không được null!";

            return throwException(errorMessage);
        }
//        requestDTO.setManagerGroupId(requestDTO.getGroup().getId());
//        requestDTO.setDepartmentId(requestDTO.getDepartment().getId());
//        requestDTO.setProjectId(requestDTO.getProject().getProjectId());
        if (requestDTO.getManagerGroupId() == null) {
            errorMessage = "Group không được null!";
            return throwException(errorMessage);
        }

        if (requestDTO.getDepartmentId() == null) {
            errorMessage = "Department không được null!";
            return throwException(errorMessage);
        }

//        if (requestDTO.getProjectId() == null) {
//            errorMessage = "Project không được null!";
//            return throwException(errorMessage);
//        }
        return null;
    }

    @Override
    public String throwException(String errorMessage) {
        return errorMessage;
    }

    @Override
    public String validateUpdateRequest(Long requestId, RequestDTO requestDTO) {
        Request currentRequest = requestRepository.findOne(requestId);
        if (currentRequest == null) {
            return "Cannot find request with ID " + requestId;
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Set<String> roles = authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toSet());
        if (!roles.contains(ROLE_ADMIN) && !roles.contains(ROLE_TA_MANAGER)) {
            String updaterUsername = requestDTO.getUsername();
            if (!org.apache.commons.lang3.StringUtils.equalsIgnoreCase(updaterUsername, currentRequest.getCreatedBy())) {
                return "Current user " + updaterUsername + " cannot edit requests created by other user (" + currentRequest.getCreatedBy() + ")";
            }
        }
        return null;
    }

    @Override
    @Transactional
    public Request addRequest(RequestDTO requestDTO) {
        Request request = new Request();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        request.setCreatedBy(authentication.getPrincipal().toString());
        Set<RequestSubmit> requestSubmits = request.getRequestSubmits() == null
                ? new HashSet<>() : request.getRequestSubmits();
        RequestSubmit requestSubmit = new RequestSubmit();
        requestSubmit.setRequestStatus(Approved2);
        requestSubmit.setCreatedBy(authentication.getPrincipal().toString());
        requestSubmits.add(requestSubmit);
        request.setRequestSubmits(requestSubmits);
        if (requestDTO.getIdJobLevel().stream().filter(x -> x != null).collect(Collectors.toList()).size() != 0) {
            Set<ManagerJobLevel> managerJobLevels = new HashSet<>();
            for (Long m : requestDTO.getIdJobLevel()) {
                managerJobLevels.add(jobLevelRepository.findOne(m));
            }
            request.setManagerJobLevels(managerJobLevels);
        }
        return saveRequestDetails(request, requestDTO);
    }

    @Override
    @Transactional
    public Request addAllRequest(RequestDTO requestDTO) {
        Request request = new Request();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        request.setCreatedBy(authentication.getPrincipal().toString());
        Set<RequestSubmit> requestSubmits = request.getRequestSubmits() == null
                ? new HashSet<>() : request.getRequestSubmits();
        RequestSubmit requestSubmit = new RequestSubmit();
        requestSubmit.setRequestStatus(New);
        requestSubmit.setCreatedBy("datu3");
        requestSubmits.add(requestSubmit);
        request.setRequestSubmits(requestSubmits);
        return saveAllRequestDetails(request, requestDTO);
    }

    @Override
    public Request updateRequest(RequestDTO requestDTO) {
        Request request = requestRepository.findById(requestDTO.getId());
        if (request == null) {
            return null;
        }
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        request.setLastUpdatedBy(authentication.getPrincipal().toString());
        RequestSubmit requestSubmit = new RequestSubmit();
        requestSubmit.setRequestStatus(RequestStatusEnum.Edited);
        requestSubmitRepository.save(requestSubmit);
        Set<ManagerJobLevel> managerJobLevels = request.getManagerJobLevels() == null
                ? new HashSet<>() : request.getManagerJobLevels();
        if (requestDTO.getIdJobLevel().stream().filter(x -> x != null).collect(Collectors.toList()).size() != 0) {
            for (Long m : requestDTO.getIdJobLevel()) {
                managerJobLevels.add(jobLevelRepository.findOne(m));
            }
        }
        if (!managerJobLevels.isEmpty()) {
            request.setManagerJobLevels(managerJobLevels);
        }
        return saveRequestDetails(request, requestDTO);
    }

    @Override
    public Request saveRequest(RequestDTO requestDTO) {
        if (requestDTO.getId() == null) {
            return addRequest(requestDTO);
        } else {
            return updateRequest(requestDTO);
        }
    }

    @Override
    public Request saveAllRequest(RequestDTO requestDTO) {
        if (requestDTO.getId() == null) {
            return addAllRequest(requestDTO);
        } else {
            return updateRequest(requestDTO);
        }
    }

    @Override
    public Request updateRequest(Long id, RequestDTO requestDTO) {
        Request request = requestRepository.findOne(id);
        if (request == null) {
            return null;
        }
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        request.setLastUpdatedBy(authentication.getPrincipal().toString());
        RequestSubmit requestSubmit = new RequestSubmit();
        requestSubmit.setRequestStatus(RequestStatusEnum.Edited);
        requestSubmit.setCreatedBy(authentication.getPrincipal().toString());
        Set<RequestSubmit> requestSubmits = request.getRequestSubmits();
        requestSubmits.add(requestSubmit);
        request.setRequestSubmits(requestSubmits);
        Set<ManagerJobLevel> managerJobLevels = request.getManagerJobLevels();
        //if (requestDTO.getIdJobLevel().stream().filter(x -> x != null).collect(Collectors.toList()).size() != 0) {
        if (requestDTO.getIdJobLevel().stream().anyMatch(Objects::nonNull)) {
            managerJobLevels = new HashSet<>();
            for (Long m : requestDTO.getIdJobLevel()) {
                managerJobLevels.add(jobLevelRepository.findOne(m));
            }
        }
        if (!managerJobLevels.isEmpty()) {
            request.setManagerJobLevels(managerJobLevels);
        }
        return saveRequestDetails(request, requestDTO);
    }

    @Override
    public List<Request> findAll(JSONObject jsonObject) {
        return requestRepository.findByName(jsonObject);
    }

    private String getLanguage(int languageId) {
        if (ObjectUtils.isEmpty(languageId)) return null;
        return languagueMap.get(languageId);
    }
    private String getLanguageV2(String languegeId) {
//        if (ObjectUtils.isEmpty(languegeId)) return null;
//        return languagueMap.get(languegeId);
        StringBuilder listLanguage = new StringBuilder();
        List<Integer> listLanguageId = Arrays.stream(languegeId.split(",")).map(s -> Integer.parseInt(s.trim())).collect(Collectors.toList());
        for (Integer integer : listLanguageId) {
            listLanguage.append(languagueMap.get(integer)).append(",");
        }
        return listLanguage.substring(0, listLanguage.length() - 1);
    }

    @Deprecated
    //<editor-fold desc="Old Code">
    @Override
    public Page<RequestResponseDTO> searchRequest(UserDTO userLogin, String requests, String name, String groups, String departments, String statuses,
                                                  String areas, String hrs, String priority, Date startDate, Date endDate, String languageIds,
                                                  Date startCreate, Date endCreate, Pageable pageable) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        boolean taMember = false;
        for (GrantedAuthority authority : authentication.getAuthorities()) {
            if (ROLE_GROUP_LEAD.equals(authority.toString())) {
                groups = userLogin.getGroupIds();
            }
            if (ROLE_DU_LEAD.equals(authority.toString())) {
                groups = "";
                departments = userLogin.getDepartmentGroup() == null ?
                        userLogin.getGroupIds() : userLogin.getDepartmentGroup().getDepartmentId().toString();
            }
            if (ROLE_TA_MEMBER.equals(authority.toString())) {
                taMember = true;
            }
        }

        List<Long> requestIds = extractIdFromString(requests);
        List<Long> groupIds = extractIdFromString(groups);
        List<Long> departmentIds = extractIdFromString(departments);
        List<RequestStatusEnum> statusList = StringUtils.isEmpty(statuses) ? null : Arrays.stream(statuses.split(",")).map(RequestStatusEnum::valueOf).collect(Collectors.toList());
        List<Long> areaIds = extractIdFromString(areas);
        List<Long> priorities = extractIdFromString(priority);
//        List<Long> languageId = extractIdFromString(languageIds);
        List<String> languageId = StringUtils.isEmpty(languageIds) ? null : Arrays.stream(languageIds.split(",")).map(String::trim).collect(Collectors.toList());
        List<String> hrLdaps = StringUtils.isEmpty(hrs) ? null : Arrays.stream(hrs.split(",")).map(String::trim).collect(Collectors.toList());
        // get total candidateStep customize
//        String url = candidateApiUrl + totalCandidateStep;
//        RestTemplate restTemplate = new RestTemplate();
//        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
//        HttpHeaders headers = new HttpHeaders();
//        headers.set("Authorization", CANDIDATE_TOKEN);
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        HttpEntity requestEntity = new HttpEntity(requestRepository.getAllRequestIDs(),headers);
//        TotalCandidateStepResponse totalCandidateStepResponse = new TotalCandidateStepResponse();
//        try {
//            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity , String.class);
//            totalCandidateStepResponse = objectMapper.readValue(response.getBody(), TotalCandidateStepResponse.class);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        Page<Request> requestList;

        String startDay = "1000-01-01";
        String endDay = "9999-12-31";
        java.sql.Date startCreates = java.sql.Date.valueOf("1000-01-01");
        java.sql.Date endCreates = java.sql.Date.valueOf("9999-12-31");
        if (startDate != null && endDate != null) {
            startDay = simpleDateFormat.format(startDate);
            endDay = simpleDateFormat.format(endDate);
        }
        if (startCreate != null && endCreate != null) {
            startCreates = new java.sql.Date(startCreate.getTime());
            endCreates = new java.sql.Date(endCreate.getTime() + 86399999);
        }


        log.error("======= START SEARCH REQUEST =====");
        if (taMember) {
            requestList = requestRepository.findAllBySearchParams(requestIds, name, groupIds, departmentIds, statusList, areaIds,
                    hrLdaps, priorities, startDay, endDay, languageId, startCreates, endCreates,
                    !CollectionUtils.isEmpty(requestIds), !StringUtils.isEmpty(name), !CollectionUtils.isEmpty(groupIds), !CollectionUtils.isEmpty(departmentIds),
                    !CollectionUtils.isEmpty(statusList), !CollectionUtils.isEmpty(areaIds), !CollectionUtils.isEmpty(hrLdaps), !CollectionUtils.isEmpty(priorities),
                    !CollectionUtils.isEmpty(languageId), pageable);
        } else {
            requestList = requestRepository.findAllBySearchParamsRoleTa(requestIds, name, groupIds, departmentIds, statusList, areaIds,
                    hrLdaps, priorities, startDay, endDay, languageId, startCreates, endCreates,
                    !CollectionUtils.isEmpty(requestIds), !StringUtils.isEmpty(name), !CollectionUtils.isEmpty(groupIds), !CollectionUtils.isEmpty(departmentIds),
                    !CollectionUtils.isEmpty(statusList), !CollectionUtils.isEmpty(areaIds), !CollectionUtils.isEmpty(hrLdaps), !CollectionUtils.isEmpty(priorities),
                    !CollectionUtils.isEmpty(languageId), pageable);
        }

        log.error("======= END SEARCH REQUEST LIST =====");


        Map<Long, Set<RequestAssign>> map = new HashMap<>();
        requestList.getContent().forEach(x -> map.put(x.getId(), x.getRequestAssigns()));


        List<RequestResponseDTO> requestResponse = new ArrayList<>();
        Set<String> TAs = new HashSet<>();
        List<RequestAssign> requestAssignList = new ArrayList<>();

        log.error("======= START GET TA =====");
        if (requestList.getContent() != null) {
            List<Long> finalRequestIds = requestList.getContent().stream().map(Request::getId).collect(Collectors.toList());
            HRMemberRootResponse hrMemberRootResponse = getAllHRMemberActive();
            for (Map.Entry<Long, Set<RequestAssign>> entry : map.entrySet()) {
                TAs.addAll(entry.getValue().stream().map(RequestAssign::getAssignLdap).collect(Collectors.toSet()));
                requestAssignList.addAll(entry.getValue());
            }
            List<TotalCandidateTAResponse> candidateTAResponses = candidateService.getTotalCandidateInStepInTAByIdRequest(new ArrayList<>(TAs));
            Map<String, TotalCandidateTAResponse> candidateTAMap = candidateTAResponses.stream().collect(Collectors.toMap(TotalCandidateTAResponse::getTa, v -> v));

            Map<String, HRMemberResponse> hrMemberResponseMap = hrMemberRootResponse.getItem().stream().collect(Collectors.toMap(HRMemberResponse::getUsername, v -> v));
            for (RequestAssign requestAssign : requestAssignList) {
                TotalCandidateTAResponse totalCandidateTAResponse = candidateTAMap.get(requestAssign.getAssignLdap());
                HRMemberResponse hrMemberResponse = hrMemberResponseMap.get(requestAssign.getAssignLdap());
                if (!ObjectUtils.isEmpty(totalCandidateTAResponse)) {
                    for (CandidateListTotalStepResponse item : totalCandidateTAResponse.getCandidateByJobRtsList()) {
                        if (!ObjectUtils.isEmpty(item.getJobRts()) && (requestAssign.getRequest().getId().intValue() == Integer.parseInt(item.getJobRts()))) {
                            requestAssign.setApplied(item.getQualify().intValue());
                            requestAssign.setContacting(item.getConfirm().intValue());
                            requestAssign.setInterview(item.getInterview().intValue());
                            requestAssign.setOther(item.getOffer().intValue());
                            requestAssign.setOnboard(item.getOnboard().intValue());
                        }
                    }
                }
                requestAssign.setAssignName(!ObjectUtils.isEmpty(hrMemberResponse) ? hrMemberResponse.getFullName() : requestAssign.getAssignName());
            }
            log.error("======= END GET TA =====");


            log.error("======= START CANDIDATE =====");
            List<TotalCandidateOfRequestResponse> candidateRootResponses = candidateService.searchCandidateList(finalRequestIds, CandidateStateEnum.none);
            Map<Long, TotalCandidateOfRequestResponse> candidateResponseMap = candidateRootResponses.stream().filter(v -> !ObjectUtils.isEmpty(v.getJobRtsId())).collect(Collectors.toMap(v -> Long.parseLong(v.getJobRtsId()), v -> v));

            log.error("======= END CANDIDATE =====");
            Map<Long, Set<RequestAssign>> requestAssignMap = requestAssignList.stream().collect(Collectors.groupingBy(v -> v.getRequest().getId(), Collectors.toSet()));
            Map<Long, Request> requestMap = requestList.getContent().stream().collect(Collectors.toMap(BaseDomain::getId, v -> v));
            for (Map.Entry<Long, Request> entry : requestMap.entrySet()) {
                Set<RequestAssign> requestAssigns = requestAssignMap.get(entry.getKey());
                if (ObjectUtils.isEmpty(requestAssigns)) {
                    entry.getValue().setRequestAssigns(requestAssigns);
                }
                RequestResponseDTO requestResponseDTO = new RequestResponseDTO(entry.getValue().getId(), entry.getValue().getName(), entry.getValue().getCreatedBy(),
                        entry.getValue().getRequestStatusName(), entry.getValue().getDeadline(), entry.getValue().getProjectId(),
                        entry.getValue().getNumber(), entry.getValue().getDepartmentId(), entry.getValue().getManagerGroupId(),
                        entry.getValue().getRequestAssigns(), entry.getValue().getArea(), entry.getValue().getJobType(), entry.getValue().getCodePosition(),
                        entry.getValue().getManagerJobLevels(), (long) candidateResponseMap.get(entry.getKey()).getAllQualify(), entry.getValue().getManagerPriority(),
                        !ObjectUtils.isEmpty(entry.getValue().getLanguageId()) ? getLanguage(Integer.parseInt(entry.getValue().getLanguageId())) : null, entry.getValue().getCreatedDate(),
                        (long) candidateResponseMap.get(entry.getKey()).getTotalOnboard(), null, null);
                requestResponse.add(requestResponseDTO);
            }
        }
        return new PageImpl<>(requestResponse, pageable, requestList.getTotalElements());
    }
    //</editor-fold>

    @Override
    public Page<RequestResponseDTO> v2SearchRequest(UserDTO userLogin, String requests, String name, String groups, String departments, String statuses,
                                                    String areas, String hrs, String priority, Date startDate, Date endDate, String languageIds,
                                                    Date startCreate, Date endCreate, Pageable pageable) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        boolean taMember = false;
        List<String> grantedAuthorities =  authentication.getAuthorities().stream().map(String::valueOf).collect(Collectors.toList());

        if (grantedAuthorities.contains(ROLE_GROUP_LEAD)) {
            groups = userLogin.getGroupIds();
        }

        if (grantedAuthorities.contains(ROLE_DU_LEAD)) {
            groups = "";
            departments = userLogin.getDepartmentGroup() == null ?
                    userLogin.getGroupIds() : userLogin.getDepartmentGroup().getDepartmentId().toString();
        }

        if ((!grantedAuthorities.contains(ROLE_TA_MANAGER)
                || !grantedAuthorities.contains(ROLE_TA_LEADER)) && grantedAuthorities.contains(ROLE_TA_MEMBER)) {
            taMember = true;
        }

        log.info("ta {}", taMember);

        List<Long> requestIds = extractIdFromString(requests);
        List<Long> groupIds = extractIdFromString(groups);
        List<Long> departmentIds = extractIdFromString(departments);
        List<RequestStatusEnum> statusList = StringUtils.isEmpty(statuses) ? null : Arrays.stream(statuses.split(",")).map(RequestStatusEnum::valueOf).collect(Collectors.toList());
        List<Long> areaIds = extractIdFromString(areas);
        List<Long> priorities = extractIdFromString(priority);
        List<String> languageId = StringUtils.isEmpty(languageIds) ? null : Arrays.stream(languageIds.split(",")).map(String::trim).collect(Collectors.toList());
        List<String> hrLdaps = StringUtils.isEmpty(hrs) ? null : Arrays.stream(hrs.split(",")).map(String::trim).collect(Collectors.toList());

        Page<V2Request> requestList;
        Page<V2Request> requestListByAssignLdaps;
        long totalElement = 0L;
        String startDay = "1000-01-01";
        String endDay = "9999-12-31";
        java.sql.Date startCreates = java.sql.Date.valueOf("1000-01-01");
        java.sql.Date endCreates = java.sql.Date.valueOf("9999-12-31");
        if (startDate != null && endDate != null) {
            startDay = simpleDateFormat.format(startDate);
            endDay = simpleDateFormat.format(endDate);
        }
        if (startCreate != null && endCreate != null) {
            startCreates = new java.sql.Date(startCreate.getTime());
            endCreates = new java.sql.Date(endCreate.getTime() + 86399999);
        }
        List<V2RequestAssign> pageRequestAssigns = null;
        if (!ObjectUtils.isEmpty(hrLdaps)) {
            pageRequestAssigns = v2RequestAssignRepository.findV2RequestAssignByParamsPage(requestIds, hrLdaps, !ObjectUtils.isEmpty(requestIds), !ObjectUtils.isEmpty(hrLdaps));
            if (!ObjectUtils.isEmpty(pageRequestAssigns)) {
                requestIds = pageRequestAssigns.stream().map(V2RequestAssign::getRequestId).collect(Collectors.toList());
            }
        }
        List<V2Request> requestListFinal;
        // get Request assigns
//        if (taMember) {
//            requestList = v2RequestRepository.findAllBySearchParams(requestIds, name, groupIds, departmentIds, statusList, areaIds, priorities, startDay, endDay, languageId, startCreates, endCreates,
//                    !CollectionUtils.isEmpty(requestIds), !StringUtils.isEmpty(name), !CollectionUtils.isEmpty(groupIds), !CollectionUtils.isEmpty(departmentIds),
//                    !CollectionUtils.isEmpty(statusList), !CollectionUtils.isEmpty(areaIds), !CollectionUtils.isEmpty(priorities),
//                    !CollectionUtils.isEmpty(languageId), pageable);
//            requestListFinal = new ArrayList<>(requestList.getContent());
//            totalElement = requestList.getTotalElements();
//        } else {
            requestList = v2RequestRepository.findAllBySearchParamsRoleTa(requestIds, name, groupIds, departmentIds, statusList, areaIds,
                    hrLdaps, priorities, startDay, endDay, languageId, startCreates, endCreates,
                    !CollectionUtils.isEmpty(requestIds), !StringUtils.isEmpty(name), !CollectionUtils.isEmpty(groupIds), !CollectionUtils.isEmpty(departmentIds),
                    !CollectionUtils.isEmpty(statusList), !CollectionUtils.isEmpty(areaIds), !CollectionUtils.isEmpty(hrLdaps), !CollectionUtils.isEmpty(priorities),
                    !CollectionUtils.isEmpty(languageId), pageable);
            requestListFinal = new ArrayList<>(requestList.getContent());
            totalElement = requestList.getTotalElements();
            if (!ObjectUtils.isEmpty(pageRequestAssigns)) {
                requestListByAssignLdaps = v2RequestRepository.findAllBySearchParams(requestIds, name, groupIds, departmentIds, statusList, areaIds, priorities, startDay, endDay, languageId, startCreates, endCreates,
                        !CollectionUtils.isEmpty(requestIds), !StringUtils.isEmpty(name), !CollectionUtils.isEmpty(groupIds), !CollectionUtils.isEmpty(departmentIds),
                        !CollectionUtils.isEmpty(statusList), !CollectionUtils.isEmpty(areaIds), !CollectionUtils.isEmpty(priorities),
                        !CollectionUtils.isEmpty(languageId), pageable);
                requestListFinal.addAll(requestListByAssignLdaps.getContent());
                totalElement+=requestListByAssignLdaps.getTotalElements();
            }
//        }
        List<Long> finalRequestIds = requestListFinal.stream().map(BaseDomain::getId).distinct().collect(Collectors.toList());
        List<String> finalRequestStringIds = finalRequestIds.stream().map(String::valueOf).collect(Collectors.toList());
        List<RequestResponseDTO> requestResponse = new ArrayList<>();
        if (!ObjectUtils.isEmpty(finalRequestIds)) {
            GroupResponse managerGroups = managerGroupService.allManagerGroups();
            List<V2RequestAssign> requestAssignList = v2RequestAssignRepository.findV2RequestAssignByParams(finalRequestIds, !CollectionUtils.isEmpty(finalRequestIds));
            Map<Long, Set<V2RequestAssign>> requestAssignMap = requestAssignList.stream().collect(Collectors.groupingBy(V2RequestAssign::getRequestId, Collectors.toSet()));

            //get manager area
            List<ManagerArea> managerAreas = managerAreaRepository.findManagerAreaByParams(areaIds, !ObjectUtils.isEmpty(areaIds));
            Map<Long, ManagerArea> managerAreaMap = managerAreas.stream().collect(Collectors.toMap(ManagerArea::getId, v -> v));

            //get request manager job level
            List<V2RequestManagerJobLevels> requestManagerJobLevels = v2RequestManagerJobLevelRepository.findV2RequestManagerJobLevelsByRequestIdIn(finalRequestIds);
            List<Long> managerJobLevelIds = requestManagerJobLevels.stream().map(V2RequestManagerJobLevels::getManagerJobLevelsId).distinct().collect(Collectors.toList());
            List<V2ManagerJobLevel> managerJobLevels = v2ManagerJobLevelRepository.findV2ManagerJobLevelByIdIn(managerJobLevelIds);
            Map<Long, List<V2RequestManagerJobLevels>> v2RequestManagerJobLevelMap = requestManagerJobLevels.stream().collect(Collectors.groupingBy(V2RequestManagerJobLevels::getRequestId, Collectors.toList()));

            Map<Long, V2ManagerJobLevel> managerJobLevelWithIdMap = managerJobLevels.stream().collect(Collectors.toMap(V2ManagerJobLevel::getId, v -> v));
            Set<Long> managerJobTypeIds = managerJobLevels.stream().map(V2ManagerJobLevel::getJobTypeId).collect(Collectors.toSet());

            //get job type
            managerJobTypeIds.addAll(requestListFinal.stream().map(V2Request::getJobTypeId).collect(Collectors.toList()));
            List<ManagerJobType> managerJobTypes = managerJobTypeRepository.findManagerJobTypeByParams(new ArrayList<>(managerJobTypeIds), !ObjectUtils.isEmpty(managerJobTypeIds));
            Map<Long, ManagerJobType> managerJobTypeMap = managerJobTypes.stream().collect(Collectors.toMap(ManagerJobType::getId, v -> v));

            //get manager priority
            List<Long> managerPriorityIds = requestListFinal.stream().map(V2Request::getManagerPriorityId).distinct().collect(Collectors.toList());
            List<ManagerPriority> managerPriorities = managerPriorityRepository.findManagerPrioritiesByParam(managerPriorityIds, !ObjectUtils.isEmpty(managerPriorityIds));
            Map<Long, ManagerPriority> managerPriorityMap = managerPriorities.stream().collect(Collectors.toMap(ManagerPriority::getId, v -> v));
            Map<Long, V2Request> requestMap = requestListFinal.stream()
                    //.collect(Collectors.toMap(BaseDomain::getId, v -> v))
                    .collect(LinkedHashMap::new, // Supplier LinkedHashMap to keep the order
                            (map, item) -> map.put(item.getId(), item),
                            Map::putAll);

            long startTime = System.currentTimeMillis();
            HRMemberRootResponse hrMemberRootResponse = getAllHRMemberHRLeadActive();
            long endTime = System.currentTimeMillis();
            long timeElapsed = endTime - startTime;
            System.out.println("Execution time in milliseconds 1: " + timeElapsed);
            Map<String, HRMemberResponse> hrMemberResponseMap = hrMemberRootResponse.getItem().stream().collect(Collectors.toMap(HRMemberResponse::getUsername, v -> v));
            List<TotalCandidateOfRequestResponse> candidateRootResponses = candidateService.searchCandidateList(finalRequestIds, CandidateStateEnum.none);
            Map<Long, TotalCandidateOfRequestResponse> candidateResponseMap = candidateRootResponses.stream().filter(v -> !ObjectUtils.isEmpty(v.getJobRtsId())).collect(Collectors.toMap(v -> Long.parseLong(v.getJobRtsId()), v -> v));
//
            long startTime1 = System.currentTimeMillis();
            List<String> TAs = requestAssignList.stream().map(V2RequestAssign::getAssignLdap).distinct().collect(Collectors.toList());
            List<TotalCandidateTAResponseV2> candidateTAResponses = candidateService.getTotalCandidateInStepInTAByIdRequestV2(new ArrayList<>(TAs), finalRequestStringIds);
            long endTime1 = System.currentTimeMillis();
            long timeElapsed1 = endTime1 - startTime1;
            System.out.println("Execution time in milliseconds 2: " + timeElapsed1);
            Map<Long, List<TotalCandidateTAResponseV2>> candidateTAMapV2 = candidateTAResponses.stream().collect(Collectors.groupingBy(v -> v.getCustomCandidateDTO().getJobRtsId(), Collectors.toList()));
            TotalCandidateStepResponse totalCandidateStepResponse = candidateService.getTotalCandidateInStepByIdRequest(finalRequestIds.stream().map(String::valueOf).collect(Collectors.toList()));

            for (Map.Entry<Long, V2Request> entry : requestMap.entrySet()) {
                ManagerArea area = managerAreaMap.get(entry.getValue().getAreaId());
                ManagerJobType jobType = managerJobTypeMap.get(entry.getValue().getJobTypeId());
                ManagerPriority managerPriority = managerPriorityMap.get(entry.getValue().getManagerPriorityId());
                List<V2RequestManagerJobLevels> v2RequestManagerJobLevels = v2RequestManagerJobLevelMap.get(entry.getKey());
                List<ManagerJobLevelDTO> managerJobLevelDTOS = setManagerJobLevelWithRequest(v2RequestManagerJobLevels, managerJobLevelWithIdMap, managerJobTypeMap);
                Set<V2RequestAssign> requestAssigns = requestAssignMap.get(entry.getKey());
                List<TotalCandidateTAResponseV2> totalCandidateTAResponseV2s = candidateTAMapV2.get(entry.getKey());
                CandidateListTotalStepResponse candidateListTotalStepResponse = totalCandidateStepResponse.getCandidateList().stream().filter(x -> x.getJobRts().equals(String.valueOf(entry.getKey()))).findAny().orElse(null);


                TotalCandidateOfRequestResponse totalCandidateOfRequest = candidateResponseMap.get(entry.getKey());

                RequestResponseDTO requestResponseDTO = new RequestResponseDTO(entry.getValue().getId(), entry.getValue().getName(), entry.getValue().getCreatedBy(),
                        entry.getValue().getRequestStatusName(), entry.getValue().getDeadline(), entry.getValue().getProjectId(),
                        entry.getValue().getNumber(), entry.getValue().getDepartmentId(), entry.getValue().getManagerGroupId(),
                        null, area, jobType, entry.getValue().getCodePosition(),
                        null, !ObjectUtils.isEmpty(totalCandidateOfRequest) ? (long) totalCandidateOfRequest.getAllQualify() : null, managerPriority,
                        !ObjectUtils.isEmpty(entry.getValue().getLanguageId()) ? getLanguageV2(entry.getValue().getLanguageId()) : null, entry.getValue().getCreatedDate(),
                        !ObjectUtils.isEmpty(totalCandidateOfRequest) ? (long) totalCandidateOfRequest.getTotalOnboard() : null,
                        getGroupName(managerGroups, entry.getValue().getManagerGroupId()), getDUName(managerGroups, entry.getValue().getManagerGroupId(), entry.getValue().getDepartmentId()));
                Map<String, List<TotalCandidateTAResponseV2>> totalCandidateTAResponseMap = null;
                if (!ObjectUtils.isEmpty(totalCandidateTAResponseV2s)) {
                    totalCandidateTAResponseMap = totalCandidateTAResponseV2s.stream().collect(Collectors.groupingBy(v -> v.getCustomCandidateDTO().getTa(), Collectors.toList()));
                }

                Set<V2RequestAssignDTO> requestAssignDTOS = new HashSet<>();

                if (!ObjectUtils.isEmpty(requestAssigns)) {
                    for (V2RequestAssign v2RequestAssign : requestAssigns) {
                        HRMemberResponse hrMemberResponse = hrMemberResponseMap.get(v2RequestAssign.getAssignLdap());
                        List<TotalCandidateTAResponseV2> totalCandidateTAResponseV2ByTA = null;
                        if (!ObjectUtils.isEmpty(totalCandidateTAResponseMap)) {
                            totalCandidateTAResponseV2ByTA = totalCandidateTAResponseMap.get(v2RequestAssign.getAssignLdap());
                        }

                        Map<String, TotalCandidateTAResponseV2> totalCandidateTAResponseV2MapByStep = null;
                        if (!ObjectUtils.isEmpty(totalCandidateTAResponseV2ByTA)) {
                            totalCandidateTAResponseV2MapByStep = totalCandidateTAResponseV2ByTA.stream().collect(Collectors.toMap(v -> v.getCustomCandidateDTO().getStepRts(), v -> v));
                        }

                        V2RequestAssignDTO v2RequestAssignDTO = new V2RequestAssignDTO();
                        v2RequestAssignDTO.setRequestId(v2RequestAssign.getRequestId());
                        v2RequestAssignDTO.setAssignId(v2RequestAssign.getId());
                        v2RequestAssignDTO.setAssignLdap(v2RequestAssign.getAssignLdap());
                        v2RequestAssignDTO.setAssignName(v2RequestAssign.getAssignName());
                        v2RequestAssignDTO.setTarget(v2RequestAssign.getTarget());

                        if (!ObjectUtils.isEmpty(totalCandidateTAResponseV2MapByStep)) {
                            TotalCandidateTAResponseV2 qualify = totalCandidateTAResponseV2MapByStep.get("qualify");
                            TotalCandidateTAResponseV2 confirm = totalCandidateTAResponseV2MapByStep.get("confirm");
                            TotalCandidateTAResponseV2 interview = totalCandidateTAResponseV2MapByStep.get("interview");
                            TotalCandidateTAResponseV2 offer = totalCandidateTAResponseV2MapByStep.get("offer");
                            TotalCandidateTAResponseV2 onboard = totalCandidateTAResponseV2MapByStep.get("onboard");
                            int failed = 0;

                            if (!ObjectUtils.isEmpty(onboard)) {
                                v2RequestAssignDTO.setOnBoarding(onboard.getCustomCandidateDTO().getCount().intValue());
                                v2RequestAssignDTO.setOnBoard(v2RequestAssignDTO.getOnBoarding() + onboard.getFailed().intValue());
                                failed = onboard.getFailed().intValue();
                            }

                            int offerFailed = 0;
                            if (!ObjectUtils.isEmpty(offer)) {
                                v2RequestAssignDTO.setOffering(offer.getCustomCandidateDTO().getCount().intValue());
                                offerFailed = offer.getFailed().intValue();
                            }
                            v2RequestAssignDTO.setOffer(v2RequestAssignDTO.getOffering() + offerFailed + v2RequestAssignDTO.getOnBoard());
                            failed += offerFailed;

                            int interviewFailed = 0;
                            if (!ObjectUtils.isEmpty(interview)) {
                                v2RequestAssignDTO.setInterviewing(interview.getCustomCandidateDTO().getCount().intValue());
                                interviewFailed =  interview.getFailed().intValue();
                            }
                            v2RequestAssignDTO.setInterview(v2RequestAssignDTO.getInterviewing() + interviewFailed + v2RequestAssignDTO.getOffer());
                            failed += interviewFailed;

                            int confirmFailed = 0;
                            if (!ObjectUtils.isEmpty(confirm)) {
                                v2RequestAssignDTO.setContacting( confirm.getCustomCandidateDTO().getCount().intValue());
                                confirmFailed =  confirm.getFailed().intValue();
                            }
                            v2RequestAssignDTO.setContact(v2RequestAssignDTO.getContacting() + confirmFailed + v2RequestAssignDTO.getInterview());
                            failed += confirmFailed;

                            int qualifyFailed = 0;
                            if (!ObjectUtils.isEmpty(qualify)) {
                                v2RequestAssignDTO.setQualifying(qualify.getCustomCandidateDTO().getCount().intValue());
                                qualifyFailed = qualify.getFailed().intValue();
                            }
                            v2RequestAssignDTO.setQualify(v2RequestAssignDTO.getQualifying() + qualifyFailed + v2RequestAssignDTO.getContact());
                            failed += qualifyFailed;

                            v2RequestAssignDTO.setRejected(failed);
                        }

                        if (!ObjectUtils.isEmpty(hrMemberResponse)) {
                            v2RequestAssignDTO.setAssignName(hrMemberResponse.getFullName());
                        }
                        requestAssignDTOS.add(v2RequestAssignDTO);
                    }
                }

                requestResponseDTO.setRequestAssigns(requestAssignDTOS);

                Long totalApplicant = 0L;
                Long totalCurrentApplicant = 0L;

                Long totalOnboarding = 0L;
                Long totalCurrentOnboarding = 0L;

                if (!ObjectUtils.isEmpty(candidateListTotalStepResponse)) {
                    totalApplicant = candidateListTotalStepResponse.getQualify();
                    totalCurrentApplicant = candidateListTotalStepResponse.getQualifying();

                    totalOnboarding = candidateListTotalStepResponse.getOnboard();
                    totalCurrentOnboarding = candidateListTotalStepResponse.getOnboarding();
                }
                requestResponseDTO.setTotalApplicant(totalApplicant);
                requestResponseDTO.setTotalCurrentApplicant(totalCurrentApplicant);
                requestResponseDTO.setTotalOnboarding(totalOnboarding);
                requestResponseDTO.setTotalCurrentOnboarding(totalCurrentOnboarding);
                if (!ObjectUtils.isEmpty(v2RequestManagerJobLevels)) {
                    requestResponseDTO.setManagerJobLevel(new HashSet<>(managerJobLevelDTOS));
                }
                requestResponse.add(requestResponseDTO);
            }
        }
        return new PageImpl<>(requestResponse, pageable, totalElement);
    }

    private List<ManagerJobLevelDTO> setManagerJobLevelWithRequest(List<V2RequestManagerJobLevels> requestManagerJobLevels,
                                                                   Map<Long, V2ManagerJobLevel> managerJobLevelWithIdMap,
                                                                   Map<Long, ManagerJobType> managerJobTypeMap) {
        List<ManagerJobLevelDTO> managerJobLevelDTOS = new ArrayList<>();
        for (V2RequestManagerJobLevels v2RequestManagerJobLevels : requestManagerJobLevels) {
            V2ManagerJobLevel v2ManagerJobLevel = managerJobLevelWithIdMap.get(v2RequestManagerJobLevels.getManagerJobLevelsId());
            if (!ObjectUtils.isEmpty(v2ManagerJobLevel)) {
                ManagerJobType jobType = managerJobTypeMap.get(v2ManagerJobLevel.getJobTypeId());
                ManagerJobLevelDTO managerJobLevelDTO = new ManagerJobLevelDTO();
                managerJobLevelDTO.setName(v2ManagerJobLevel.getName());
                managerJobLevelDTO.setId(v2ManagerJobLevel.getId());
                managerJobLevelDTO.setSalaryMax(v2ManagerJobLevel.getSalaryMax());
                managerJobLevelDTO.setSalaryMin(v2ManagerJobLevel.getSalaryMin());
                managerJobLevelDTO.setDescription(v2ManagerJobLevel.getDescription());
                if (!ObjectUtils.isEmpty(jobType)) {
                    managerJobLevelDTO.setJobType(jobType);
                }
                managerJobLevelDTOS.add(managerJobLevelDTO);
            }
        }
        return managerJobLevelDTOS;
    }

    private List<RequestResponseDTO> filterRequestByRole(UserDTO userLogin, List<RequestResponseDTO> requestResponseList) {
        if (userLogin.getUserRoles().stream().anyMatch(r -> ROLE_TA_LEADER.equals(r))
                && userLogin.getUserName() != null) {
            requestResponseList = requestResponseList.stream().filter(r -> userLogin.getUserName().equals(r.getCreatedBy()))
                    .collect(Collectors.toList());
        }
        ;
        return requestResponseList;
    }

    private List<Long> extractIdFromString(String idString) {
        List<Long> result = StringUtils.isEmpty(idString) ? null : Arrays.asList(idString.split(","))
                .stream().map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
        return result;
    }

    @Override
    public Integer totalRequest(JSONObject jsonObject) {
        return requestRepository.totalCount(jsonObject);
    }

    @Override
    public Request findOne(Long id) {
        Request request = requestRepository.findById(id);
        if (request != null) {
            HRMemberRootResponse hrMemberRootResponse = getAllHRMemberActive();
            Set<RequestAssign> requestAssigns = request.getRequestAssigns();
            requestAssigns.forEach(requestAssign -> {
                if (requestAssign.getAssignLdap() != null) {
                    HRMemberResponse tempHR = hrMemberRootResponse.getItem().stream()
                            .filter(hrMemberResponse -> requestAssign.getAssignLdap().equals(hrMemberResponse.getUsername()))
                            .findAny()
                            .orElse(null);
                    if (tempHR != null) {
                        requestAssign.setAssignName(tempHR.getFullName());
                    }
                }
            });
            request.setRequestAssigns(requestAssigns);
        }
        return request;
    }

    @Override
    @Transactional
    public void delete(Long id) {
        commonService.updateDeletedRowTable("request", id);
    }

    @Override
    @Transactional
    public void delete(String ids) {
        commonService.updateDeletedRowTable("request", ids);
    }

    @Transactional
    @Override
    public List<Request> close(String ids, String username) throws Exception {
        List<Request> lstResult;
        List<Long> listID = Arrays.asList(ids.split(","))
                .stream().map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
        List<Request> lstRequest = requestRepository.findAllByIdIn(listID);
        for (Request request : lstRequest) {
            if (request.getRequestStatusName() != RequestStatusEnum.Closed) {
                Set<RequestSubmit> requestSubmits = request.getRequestSubmits() == null
                        ? new HashSet<>() : request.getRequestSubmits();
                RequestSubmit requestSubmit = new RequestSubmit();
                requestSubmit.setRequestStatus(RequestStatusEnum.Closed);
                requestSubmit.setCreatedBy(username);
                requestSubmits.add(requestSubmit);
                request.setRequestStatusName(RequestStatusEnum.Closed);
                requestSubmitRepository.save(requestSubmits);
            }
        }
        lstResult = requestRepository.save(lstRequest);
        return lstResult;
    }

    @Transactional
    @Override
    public void open(String ids) {
        requestRepository.open(ids);
    }

    @Override
    public void approveRequest(String ids, String username) throws Exception {
        List<String> lstID = Arrays.asList(ids.trim().split(","));
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Set<String> roles = authentication.getAuthorities().stream()
                .map(r -> r.getAuthority()).collect(Collectors.toSet());
        HRMemberRootResponse taMNG = getTAManagement();
        for (String idString : lstID) {
            Long id = Long.valueOf(idString);
            Request request = requestRepository.findOne(id);
            UserResponse userReceive = interviewService.searchUser(request.getCreatedBy()).getUserResponses().get(0);
            request.setLastGroupSubmit(interviewService.searchUser(request.getLastGroupSubmitLdap()).getUserResponses().get(0));
            UserResponse lastGroupLeaderSubmit = request.getLastGroupSubmit();
            if (lastGroupLeaderSubmit != null) {
                if (roles.contains(ROLE_ADMIN) || username.equalsIgnoreCase(lastGroupLeaderSubmit.getUsername())) {
                    RequestStatusEnum currentRequestStatus = request.getRequestStatusName();
                    if (currentRequestStatus != RequestStatusEnum.Pending) {
                        throw new Exception("Status of Request must is pending");
                    }
                    RequestSubmit requestSubmit = new RequestSubmit();
                    requestSubmit.setRequestStatus(Approved);
                    requestSubmit.setCreatedBy(username);

                    Set<RequestSubmit> requestSubmits = request.getRequestSubmits() != null ? request.getRequestSubmits() : new HashSet<>();
                    requestSubmits.add(requestSubmit);
                    request.setRequestSubmits(requestSubmits);

                    request.setRequestStatusName(Approved);
                    requestRepository.save(request);


                    if (request != null) {
                        //Todo api get email for group lead
//                            String emailTo = userInfo.getEmail();
                        Thread thread = new Thread(() -> {
                            String subject = "Yêu cầu xử lý request tuyển dụng";
                            String body = "Dear " + taMNG.getItem().get(0).getFullName()
                                    + "<br/>Bạn nhận được một yêu cầu xử lý request tuyển dụng từ " + userReceive.getFullName()
                                    + "<br/>Vui lòng nhấn vào <a>đây</a> hoặc truy cập https://recruitment.cmcglobal.com.vn/ để xem thông tin chi tiết"
                                    + "<br/>Trân trọng!";
                            try {
                                rtsService.sendHtmlMessage(taMNG.getItem().get(0).getEmailAddress(), userReceive.getEmailAddress(), subject, body);
                            } catch (javax.mail.MessagingException e) {
                                e.printStackTrace();
                            }
                        });
                        thread.start();
                    }
                } else {
                    throw new Exception("Bạn không phải group leader được submit!");
                }

            } else {
                throw new Exception("Request chưa được submit!");
            }

        }
    }

    @Override
    public void approve2Request(String ids, String username) throws Exception {
        List<String> lstID = Arrays.asList(ids.trim().split(","));
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Set<String> roles = authentication.getAuthorities().stream()
                .map(r -> r.getAuthority()).collect(Collectors.toSet());
        for (String idString : lstID) {
            Long id = Long.valueOf(idString);
            Request request = requestRepository.findOne(id);
            UserResponse currentUser = interviewService.searchUser(request.getCreatedBy()).getUserResponses().get(0);
            HRMemberRootResponse taMNG = getTAManagement();
            if (request.getRequestStatusName().equals(Approved)) {
                if (roles.contains(ROLE_ADMIN) || username.equalsIgnoreCase(taMNG.getItem().get(0).getUsername())) {
                    RequestSubmit requestSubmit = new RequestSubmit();
                    requestSubmit.setRequestStatus(Approved2);
                    requestSubmit.setCreatedBy(username);

                    Set<RequestSubmit> requestSubmits = request.getRequestSubmits() != null ? request.getRequestSubmits() : new HashSet<>();
                    requestSubmits.add(requestSubmit);
                    request.setRequestSubmits(requestSubmits);

                    request.setRequestStatusName(Approved2);
                    requestRepository.save(request);
                    HRMemberRootResponse taLeadReponse = getTALead();
                    String email = "";
                    for (HRMemberResponse mail : taLeadReponse.getItem()) {
                        email += mail.getEmailAddress() + ",";
                    }
                    if (request != null) {
                        String finalEmail = email;
                        Thread thread = new Thread(() -> {
                            String subject = "Yêu cầu xử lý request tuyển dụng";
                            String body = "Dear anh/chị, "
                                    + "<br/>Anh/chị nhận được một yêu cầu xử lý request tuyển dụng từ " + currentUser.getFullName()
                                    + "<br/>Vui lòng nhấn vào <a>đây</a> hoặc truy cập https://recruitment.cmcglobal.com.vn/ để xem thông tin chi tiết"
                                    + "<br/>Trân trọng!";
                            try {
                                rtsService.sendHtmlMessage(finalEmail, currentUser.getEmailAddress(), subject, body);
                            } catch (javax.mail.MessagingException e) {
                                e.printStackTrace();
                            }
                        });
                        thread.start();
                    }
                } else {
                    throw new Exception("Bạn không phải TA Manager được Approve2!");
                }
            } else {
                throw new Exception("Request chưa được Approve1!");
            }

        }
    }


    @Transactional
    @Override
    public void rejectRequest(String ids, String reason, String username) throws Exception {
        UserResponse currentUser = interviewService.searchUser(username).getUserResponses().get(0);
        // add new reject reason if it isn't exist in db
        RejectReason rejectReason = rejectReasonRepository.findByReason(reason);
        if (rejectReason == null) {
            RejectReason newReason = new RejectReason();
            newReason.setReason(reason);
            newReason.setName(reason);
            rejectReason = rejectReasonRepository.saveAndFlush(newReason);
        }

        List<String> lstID = Arrays.asList(ids.trim().split(","));
        for (String idString : lstID) {
            Long id = Long.valueOf(idString);
            Request request = requestRepository.findById(id);
            UserResponse userReceive = interviewService.searchUser(request.getCreatedBy()).getUserResponses().get(0);

            if (request == null) {
                throw new Exception("Request id: " + id + " không tồn tại!!");
            }
            if (request.getRequestStatusName() != RequestStatusEnum.Pending && request.getRequestStatusName() != Approved) {
                throw new Exception("Request id: " + id + " phải có trạng thái là pending or approved !!");
            }

            Set<RequestSubmit> requestSubmitsOld = request.getRequestSubmits();
            RequestSubmit requestSubmitOld = null;
            if (requestSubmitsOld != null && requestSubmitsOld.size() > 0) {
                requestSubmitOld = requestSubmitsOld.stream().skip(requestSubmitsOld.size() - 1).findFirst().get();
            }
            if (requestSubmitOld == null) {
                throw new Exception("Request id: " + id + " chưa được submit!!");
            }
            if (requestSubmitOld.getUserReceiver() != null
                    && requestSubmitOld.getUserReceiver().getId() != currentUser.getId()) {
                throw new Exception("Bạn không có quyền REJECT với Request này");
            }
            UserResponse userSentRequest = requestSubmitOld.getUserSent() == null
                    ? currentUser : requestSubmitOld.getUserSent();
            String userSentName = userSentRequest.getFullName();
            String userSentMail = userSentRequest.getEmailAddress();

            request.setRequestStatusName(RequestStatusEnum.Rejected);

            RequestSubmit submit = new RequestSubmit();
            submit.setRequestStatus(RequestStatusEnum.Rejected);
            submit.setRejectReason(rejectReason);
            submit.setDescription("Reject request id: " + id);
            submit.setCreatedBy(username);

            Set<RequestSubmit> requestSubmits = request.getRequestSubmits() == null
                    ? new HashSet<>() : request.getRequestSubmits();
            requestSubmits.add(submit);

            request.setRequestSubmits(requestSubmits);
            request.setRejectReason(rejectReason);

            requestRepository.save(request);

            if (request != null) {
                Thread thread = new Thread(() -> {
                    try {
                        String subject = "Từ chối yêu cầu tuyển dụng";
                        String htmlBody = "Dear " + userReceive.getFullName()
                                + "<br/>Yêu cầu tuyển dụng của bạn đã bị từ chổi bởi " + currentUser.getFullName() + " bởi lý do:" + reason
                                + "<br/>Vui lòng nhấn vào <a>đây</a> hoặc truy cập https://recruitment.cmcglobal.com.vn/ để xem thông tin chi tiết"
                                + "<br/>Trân trọng!";
                        rtsService.sendHtmlMessage(userReceive.getEmailAddress(), "", subject, htmlBody);
                    } catch (MessagingException me) {
                        me.printStackTrace();
                    } catch (javax.mail.MessagingException jme) {
                        jme.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                });
                thread.start();
            }

        }
    }


    @Transactional
    @Override
    public List<Request> submitRequest(String ids, String receiver, UserDTO userSentDTO) throws Exception {
        List<Request> lstResult = null;
        String userName = userSentDTO.getUserName();
        UserResponse userSent = interviewService.searchUser(userName).getUserResponses().get(0);
        UserResponse userReceive = interviewService.searchUser(receiver).getUserResponses().get(0);
        List<Long> listID = Arrays.asList(ids.split(","))
                .stream().map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
        List<Request> lstRequest = requestRepository.findAll(listID);
        RequestSubmit submit = createRequestSubmit(userSentDTO, userSent, userReceive, RequestStatusEnum.Pending);
        for (Request request : lstRequest) {
            Set<RequestSubmit> requestSubmits = request.getRequestSubmits() == null
                    ? new HashSet<>() : request.getRequestSubmits();
            requestSubmits.add(submit);
            request.setRequestStatusName(RequestStatusEnum.Pending);
            request.setLastGroupSubmitLdap(submit.getUserReceiver().getUsername());
            request.setRequestSubmits(requestSubmits);
            String email = userReceive.getEmailAddress();
            String subject = "Yêu cầu xử lý request tuyển dụng";
            String htmlBody = "Dear " + userReceive.getFullName()
                    + "<br/>Bạn nhận được một yêu cầu xử lý request tuyển dụng từ " + userSent.getFullName()
                    + "<br/>Vui lòng nhấn vào <a>đây</a> hoặc truy cập https://recruitment.cmcglobal.com.vn/ để xem thông tin chi tiết"
                    + "<br/>Trân trọng!";
            rtsService.sendHtmlMessage(email, "", subject, htmlBody);
        }
        lstResult = requestRepository.save(lstRequest);
        return lstResult;
    }

    private RequestSubmit createRequestSubmit(UserDTO userSentDTO, UserResponse userSent,
                                              UserResponse userReceive, RequestStatusEnum requestStatus) {
        RequestSubmit submit = new RequestSubmit();
        submit.setDescription("Submit requests");
        submit.setUserSent(userSent);
        submit.setUserReceiverLdap(userSent.getUsername());
        submit.setUserReceiver(userReceive);
        submit.setUserReceiverLdap(userReceive.getUsername());
        submit.setDeleted(false);
        submit.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        submit.setCreatedBy(userSent.getUsername());
        submit.setRequestStatus(requestStatus);
        return submit;
    }

    @Override
    @Transactional
    public List<RequestAssign> createRequestAssign(Long id, List<CreateAssignRequest> assignRequest) throws javax.mail.MessagingException {
        Request request = requestRepository.findById(id);
        if (Objects.isNull(request)) {
            throw new CustomException("Request not found", HttpStatus.NOT_FOUND.value());
        }
        UserResponse userSent = interviewService.searchUser(request.getCreatedBy()).getUserResponses().get(0);
        if (request.getRequestStatusName() != Approved2 && request.getRequestStatusName() != Assigned && request.getRequestStatusName() != Shared) {
            throw new CustomException("Request not Approved2 or Assigned or Posted", HttpStatus.FORBIDDEN.value());
        }
        int totalTargetAssignCreate = assignRequest.stream().reduce(0, (v, k) -> k.getTarget(), Integer::sum);
    //    validateTargetAssign(totalTargetAssignCreate, request);
        validateAssigneeExisted(assignRequest, request);
        List<RequestAssign> listRequestAssign =
                assignRequest.stream().map(
                                r -> {
                                    RequestAssign requestAssign = new RequestAssign();
                                    requestAssign.setTarget(r.getTarget());
                                    requestAssign.setRequest(request);
                                    requestAssign.setAssignId(0L);
                                    requestAssign.setAssignLdap(r.getAssignLdap());
                                    return requestAssign;
                                })
                        .collect(Collectors.toList());


        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        RequestSubmit requestSubmit = new RequestSubmit();
        requestSubmit.setRequestStatus(RequestStatusEnum.Assigned);
        requestSubmit.setCreatedBy(authentication.getPrincipal().toString());
        Set<RequestSubmit> requestSubmits = request.getRequestSubmits();
        requestSubmits.add(requestSubmit);
        if (request.getRequestStatusName() != Shared) {
            request.setRequestStatusName(RequestStatusEnum.Assigned);
        } else {
            request.setRequestStatusName(Shared);
        }
        requestAssignRepository.save(listRequestAssign);
        String mail = "";
        for (CreateAssignRequest c : assignRequest) {
            UserResponse currentUser = interviewService.searchUser(c.getAssignLdap()).getUserResponses().get(0);
            mail += currentUser.getEmailAddress() + ",";
        }

        String subject = "Thông báo tuyển ứng viên";
        String htmlBody = "Dear anh/chị, "
                + "<br/>Anh/chị nhận được một yêu cầu tuyển dụng từ " + userSent.getFullName()
                + "<br/>Vui lòng nhấn vào <a>đây</a> hoặc truy cập https://recruitment.cmcglobal.com.vn/ để xem thông tin chi tiết"
                + "<br/>Trân trọng!";
        rtsService.sendHtmlMessage(mail, userSent.getEmailAddress(), subject, htmlBody);

        return listRequestAssign;
    }

    private void validateTargetAssign(int totalTargetAssignCreate, Request request) {
        Set<RequestAssign> requestAssigns = request.getRequestAssigns();

        int totalTargetAssignExisted = 0;

        if (!ObjectUtils.isEmpty(requestAssigns)) {
            totalTargetAssignExisted = requestAssigns.stream().reduce(0, (v, k) -> k.getTarget(), Integer::sum);

        }
        int totalTargetActual = request.getNumber() - totalTargetAssignExisted;

        if (totalTargetAssignCreate > totalTargetActual) {
            throw new CustomException("Total target assign must be greater than remaining target request (<= " + totalTargetActual +")", HttpStatus.BAD_REQUEST.value());
        }
    }

    private void validateAssigneeExisted(List<CreateAssignRequest> assignRequests, Request request) {
        List<String> ldapAssgined = new ArrayList<>();
        List<String> ldapExisted = new ArrayList<>();
        Set<RequestAssign> requestAssigns = request.getRequestAssigns();
        if (!ObjectUtils.isEmpty(requestAssigns)) {
            for (RequestAssign requestAssign : requestAssigns) {
                ldapAssgined.add(requestAssign.getAssignLdap());
            }
            if (!ObjectUtils.isEmpty(ldapAssgined)) {
                for (CreateAssignRequest createAssignRequest : assignRequests) {
                    if(ldapAssgined.contains(createAssignRequest.getAssignLdap())) {
                        ldapExisted.add(createAssignRequest.getAssignLdap());
                    }
                }
            }
            if (!ObjectUtils.isEmpty(ldapExisted)) {
                throw new CustomException("Request Assignee existed " + ldapExisted, HttpStatus.BAD_REQUEST.value());
            }
        }
    }


    @Override
    @Transactional
    public List<RequestAssign> createAllRequestAssign(Long id, List<CreateAssignRequest> assignRequest) {
        Request request = requestRepository.findById(id);
        if (Objects.isNull(request)) {
            throw new CustomException("Request not found", HttpStatus.NOT_FOUND.value());
        }
        if (request.getRequestStatusName() != Approved2) {
            throw new CustomException("Request not approved", HttpStatus.FORBIDDEN.value());
        }
        List<RequestAssign> listRequestAssign =
                assignRequest.stream().map(
                                r -> {
                                    RequestAssign requestAssign = new RequestAssign();
                                    requestAssign.setTarget(r.getTarget());
                                    requestAssign.setRequest(request);
                                    requestAssign.setAssignId(0L);
                                    requestAssign.setAssignLdap(r.getAssignLdap());
                                    return requestAssign;
                                })
                        .collect(Collectors.toList());
        RequestSubmit requestSubmit = new RequestSubmit();
        requestSubmit.setRequestStatus(RequestStatusEnum.Assigned);
        requestSubmit.setCreatedBy("datu3");
        Set<RequestSubmit> requestSubmits = request.getRequestSubmits();
        requestSubmits.add(requestSubmit);
        request.setRequestStatusName(RequestStatusEnum.Assigned);
        requestAssignRepository.save(listRequestAssign);
        return listRequestAssign;
    }

    @Override
    public ResponseAPI reassignRequest(Long id, String name, Integer target) {
        if (id == null) {
            return new ResponseAPI(404, "id assign not found");
        }
        if (name == null || name.equals("")) {
            return new ResponseAPI(404, "account name TAD not found");
        }
        if (target == null) {
            return new ResponseAPI(404, "fail to change target");
        }

        try {
            RequestAssign requestAssign = requestAssignRepository.getOne(id);
            if (!name.equals(requestAssign.getAssignLdap())) {
                RequestAssign requestAssignExisted = requestAssignRepository.findRequestAssignByAssignLdapAndRequestId(name, requestAssign.getRequest().getId());
                if (!ObjectUtils.isEmpty(requestAssignExisted)) {
                    throw new CustomException("Request Assignee existed " + name, HttpStatus.BAD_REQUEST.value());
                }
            }

//             if (target != requestAssign.getTarget() && target > 0) {
//                 List<RequestAssign> requestAssignsExisted = requestAssign.getRequest().getRequestAssigns().stream().filter(v -> !Objects.equals(v.getId(), requestAssign.getId())).collect(Collectors.toList());
//                 int totalTargetAssignExisted = 0;
//                 if (!ObjectUtils.isEmpty(requestAssignsExisted)) {
//                     totalTargetAssignExisted = requestAssignsExisted.stream().reduce(0, (v, k) -> k.getTarget(), Integer::sum);
//                 }
//                 int totalTargetActual = requestAssign.getRequest().getNumber() - totalTargetAssignExisted;
//                 if (target > totalTargetActual) {
//                     throw new CustomException("Total target assign must be greater than remaining target request (<= " + totalTargetActual +")", HttpStatus.BAD_REQUEST.value());
//                 }
//             }


            requestAssign.setAssignLdap(name);
            requestAssign.setTarget(target);
            requestAssignRepository.save(requestAssign);

            return new ResponseAPI(200, "Reassigned successfully!");
        } catch (Exception e) {
            return new ResponseAPI(500, e.getMessage());
        }
    }

    @Override
    public ResponseAPI updateRequestCreatedBy(Long requestId, String newCreatedBy) {
        if (requestId == null) {
            return new ResponseAPI(400, "Request not found");
        }
        if (newCreatedBy == null) {
            return new ResponseAPI(400, "TA Lead not found");
        }
        HRMemberRootResponse hrLeads = this.getAllHRLeadActive();
        HRMemberResponse matchHrLead = hrLeads.getItem().stream()
                .filter(e -> org.apache.commons.lang3.StringUtils.equalsIgnoreCase(newCreatedBy, e.getUsername()))
                .findAny().orElse(null);
        if (matchHrLead == null) {
            return new ResponseAPI(422, "User " + newCreatedBy + " is not a TA Lead");
        }

        Request request = requestRepository.getOne(requestId);
        request.setCreatedBy(newCreatedBy);
        request.setLastUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        request.setLastUpdated(Timestamp.from(Instant.now()));
        requestRepository.save(request);
        return new ResponseAPI(200, "Update successfully!");
    }

    @Override
    public RequestProcessDTO getRequestHistory(Long id) {
        List<RequestSubmit> requestSubmits = requestSubmitRepository.findAllByRequestId(id);
        List<RequestHistoryDTO> requestHistoryList = new ArrayList<>();
        requestSubmits.stream().sorted(Comparator.comparing(BaseDomain::getCreatedDate).reversed()).forEach(x -> {
            String reason = "";
            if (x.getRequestStatus() == RequestStatusEnum.Rejected) {
                reason = x.getRejectReason().getReason();
            }
            RequestHistoryDTO requestHistory = new RequestHistoryDTO(x.getId(), x.getCreatedBy(), reason, x.getRequestStatus(), x.getCreatedDate());
            requestHistoryList.add(requestHistory);
        });
        List<RequestAssignDTO> assignees = requestAssignRepository.findAssignByRequestId(id);
        HRMemberRootResponse hrMemberRootResponse = getAllHRMemberActive();
        assignees.forEach(a -> {
            if (a.getLdap() != null) {
                HRMemberResponse tempHR = hrMemberRootResponse.getItem().stream()
                        .filter(hrMemberResponse -> a.getLdap().equals(hrMemberResponse.getUsername()))
                        .findAny()
                        .orElse(null);
                if (tempHR != null) {
                    a.setAssignee(tempHR.getFullName());
                }
            }
        });
        RequestProcessDTO requestProcessDTO = new RequestProcessDTO(assignees, requestHistoryList.stream().distinct().sorted(Comparator.comparing(RequestHistoryDTO::getPriority)).collect(Collectors.toList()));
        return requestProcessDTO;
    }

    public JSONObject getJsonObject(String name, int offset, int limit, int like, String requestIds, String group,
                                    String department, String project, String statusIds, String orderBy, String orderType, String type, String hrIds) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", name);
        jsonObject.put("offset", offset);
        jsonObject.put("limit", limit);
        jsonObject.put("like", like);
        jsonObject.put("requestIds", requestIds);
        jsonObject.put("group", group);
        jsonObject.put("department", department);
        jsonObject.put("project", project);
        jsonObject.put("statusIds", statusIds);
        jsonObject.put("orderBy", orderBy);
        jsonObject.put("orderType", orderType);
        jsonObject.put("type", type);
        jsonObject.put("hrIds", hrIds);
        return jsonObject;
    }

    @Transactional
    public JSONObject updateJsonObjectProject(JSONObject jsonObject, String projectApiIds) {
        if (projectApiIds != null && !projectApiIds.isEmpty()) {
            StringBuilder projectIds = new StringBuilder("");
            String[] strings = projectApiIds.split(",");
            for (String id : strings) {
                Project project = projectService.findByProjectId(Long.valueOf(id));
                if (project != null) {
                    projectIds.append(project.getId());
                    projectIds.append(",");
                }

            }
            if (projectIds.length() > 0) {
                projectIds.setLength(projectIds.length() - 1);
            }
            if (!projectIds.toString().isEmpty()) {
                jsonObject.put("projectIds", projectIds.toString());
            } else {
                jsonObject.put("projectIds", "-1");
            }
        }
        return jsonObject;
    }

    @Transactional
    public JSONObject updateJsonObjectDepartment(JSONObject jsonObject, String departmentApiIdss) {
        if (departmentApiIdss != null && !departmentApiIdss.isEmpty()) {
            StringBuilder departmentIds = new StringBuilder("");
            String[] strings = departmentApiIdss.split(",");
            for (String id : strings) {
                Department department = departmentService.findByDepartmentId(Long.valueOf(id));
                if (department != null) {
                    departmentIds.append(department.getId());
                    departmentIds.append(",");
                }
            }
            if (departmentIds.length() > 0) {
                departmentIds.setLength(departmentIds.length() - 1);
            }
            if (!departmentIds.toString().isEmpty()) {
                jsonObject.put("departmentIds", departmentIds.toString());
            } else {
                jsonObject.put("departmentIds", "-1");
            }
        }
        return jsonObject;
    }

    @Transactional
    public JSONObject updateJsonObjectGroup(JSONObject jsonObject, String groupApiIds) {
        if (groupApiIds != null && !groupApiIds.isEmpty()) {
            StringBuilder groupIds = new StringBuilder("");
            String[] strings = groupApiIds.split(",");
            for (String id : strings) {
                ManagerGroup managerGroup = managerGroupService.findByGroupId(Long.valueOf(id));
                if (managerGroup != null) {
                    groupIds.append(managerGroup.getId());
                    groupIds.append(",");
                }
            }
            if (groupIds.length() > 0) {
                groupIds.setLength(groupIds.length() - 1);
            }
            if (!groupIds.toString().isEmpty()) {
                jsonObject.put("groupIds", groupIds.toString());
            } else {
                jsonObject.put("groupIds", "-1");
            }
        }
        return jsonObject;
    }


    @Override
    public HRMemberRootResponse getAllHRMember() {
        String url = cmcGlobalApiUrl + getHRMember + "?Rolekey=rts-hrmember&SystemKey=rts-test&pageSize=1000&pageIndex=0";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", CMC_GLOBAL_TOKEN);
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<HRMemberRootResponse> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity, HRMemberRootResponse.class);
        HRMemberRootResponse hrMemberRootResponse = response.getBody();
        hrMemberRootResponse.setItem(hrMemberRootResponse.getItem().stream().distinct().collect(Collectors.toList()));
        return hrMemberRootResponse;
    }

    @Override
    public HRMemberRootResponse getAllHRMemberActive() {
        String url = cmcGlobalApiUrl + getHRMember + "?Rolekey=rts-hrmember&SystemKey=rts-test&pageSize=1000&pageIndex=0";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", CMC_GLOBAL_TOKEN);
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<HRMemberRootResponse> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity, HRMemberRootResponse.class);
        HRMemberRootResponse hrMemberRootResponse = response.getBody();
        hrMemberRootResponse.setItem(hrMemberRootResponse.getItem().stream().distinct().filter(HRMemberResponse::isActive).collect(Collectors.toList()));
        return hrMemberRootResponse;
    }

    @Override
    public HRMemberRootResponse getAllHRMemberHRLeadActive() {
        String urlMember = cmcGlobalApiUrl + getHRMember + "?Rolekey=rts-hrmember&SystemKey=rts-test&pageSize=1000&pageIndex=0";
        String urlLead = cmcGlobalApiUrl + getHRMember + "?Rolekey=rts-hrlead&SystemKey=rts-test&pageSize=1000&pageIndex=0";
        String urlTaMNG = cmcGlobalApiUrl + getHRMember + "?Rolekey=RTS-TA-MNG&SystemKey=rts-test&pageSize=1000&pageIndex=0";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", CMC_GLOBAL_TOKEN);
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<HRMemberRootResponse> responseMember = restTemplate.exchange(urlMember, HttpMethod.GET, requestEntity, HRMemberRootResponse.class);
        ResponseEntity<HRMemberRootResponse> responseLead = restTemplate.exchange(urlLead, HttpMethod.GET, requestEntity, HRMemberRootResponse.class);
        ResponseEntity<HRMemberRootResponse> responseManager = restTemplate.exchange(urlTaMNG, HttpMethod.GET, requestEntity, HRMemberRootResponse.class);
        HRMemberRootResponse hrMemberRootResponse = responseMember.getBody();
        for (int i = 0; i < responseLead.getBody().getItem().size(); i++) {
            hrMemberRootResponse.getItem().add(responseLead.getBody().getItem().get(i));
        }
        for (int i = 0; i < responseManager.getBody().getItem().size(); i++) {
            hrMemberRootResponse.getItem().add(responseManager.getBody().getItem().get(i));
        }
        hrMemberRootResponse.setItem(hrMemberRootResponse.getItem().stream().distinct().filter(HRMemberResponse::isActive).collect(Collectors.toList()));
        return hrMemberRootResponse;
    }

    @Override
    public HRMemberRootResponse getAllHRLeadActive() {
        String url = cmcGlobalApiUrl + getHRMember + "?Rolekey=rts-hrlead&SystemKey=rts-test&pageSize=1000&pageIndex=0";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", CMC_GLOBAL_TOKEN);
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<HRMemberRootResponse> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity, HRMemberRootResponse.class);
        HRMemberRootResponse hrMemberRootResponse = response.getBody();
        hrMemberRootResponse.setItem(hrMemberRootResponse.getItem().stream().distinct().filter(HRMemberResponse::isActive).collect(Collectors.toList()));
        return hrMemberRootResponse;
    }

    @Override
    public GLeadResponse getGleadByDulead(String username) {
        String url = hrmsApiUrl + getGleadByDulead + "?username=" + username;
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", HRMS_TOKEN);
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<GLeadResponse> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity, GLeadResponse.class);
        return response.getBody();
    }

    @Override
    public Request updateShareJob(Long id) {
        Request request = requestRepository.findOne(id);
        if (request == null) {
            return null;
        }
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        request.setLastUpdatedBy(authentication.getPrincipal().toString());
        request.setShareJob(true);
        request.setRequestStatusName(RequestStatusEnum.Shared);
        return requestRepository.save(request);
    }

    @Override
    public List<Long> getAllIdRequests(String ids) {
        if (ids != null) {
            return requestRepository.findAllByIds(ids);
        }
        return null;
    }

    public HRMemberRootResponse getTAManagement() {
        String url = cmcGlobalApiUrl + getHRMember + "?Rolekey=RTS-TA-MNG&SystemKey=rts-test&pageSize=1000&pageIndex=0";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", CMC_GLOBAL_TOKEN);
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<HRMemberRootResponse> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity, HRMemberRootResponse.class);
        HRMemberRootResponse hrMemberRootResponse = response.getBody();
        hrMemberRootResponse.setItem(hrMemberRootResponse.getItem().stream().distinct().collect(Collectors.toList()));
        return hrMemberRootResponse;
    }

    public HRMemberRootResponse getTALead() {
        String url = cmcGlobalApiUrl + getHRMember + "?Rolekey=rts-hrlead&SystemKey=rts-test&pageSize=1000&pageIndex=0";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", CMC_GLOBAL_TOKEN);
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<HRMemberRootResponse> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity, HRMemberRootResponse.class);
        HRMemberRootResponse hrMemberRootResponse = response.getBody();
        hrMemberRootResponse.setItem(hrMemberRootResponse.getItem().stream().distinct().collect(Collectors.toList()));
        return hrMemberRootResponse;
    }

    @Override
    public List<TaInfoDto> getTaInfoByRequestId(Long requestId) {
        List<RequestAssign> assigneeList = requestAssignRepository.findByRequestId(requestId);
        HRMemberRootResponse hrMemberRootResponse = getAllHRMemberActive();
        assigneeList.forEach(a -> {
            if (a.getAssignLdap() != null) {
                HRMemberResponse tempHR = hrMemberRootResponse.getItem().stream()
                        .filter(hrMemberResponse -> a.getAssignLdap().equals(hrMemberResponse.getUsername()))
                        .findAny()
                        .orElse(null);
                if (tempHR != null) {
                    a.setAssignName(tempHR.getFullName());
                }
            }
        });

        return assigneeList.stream().map(a -> {
            TaInfoDto taInfoDto = new TaInfoDto();
            BeanUtils.copyProperties(a, taInfoDto);
            return taInfoDto;
        }).collect(Collectors.toList());
    }


    private String getGroupName(GroupResponse response, Long idGroup) {
        if (ObjectUtils.isEmpty(response) || ObjectUtils.isEmpty(response.getListChild())) {
            return null;
        }
        return response.getListChild().stream().filter(v -> v.getId().equals(idGroup)).map(GroupResponse::getName).findAny().orElse(null);
    }

    private String getDUName(GroupResponse response, Long idGroup, Long idDU) {
        if (response == null) {
            return null;
        }
        for (GroupResponse responseGroup : response.getListChild()) {
            if (Objects.equals(responseGroup.getId(), idGroup)) {
                if (org.apache.commons.collections.CollectionUtils.isNotEmpty(responseGroup.getListChild())) {
                    for (GroupResponse responseDU : responseGroup.getListChild()) {
                        if (Objects.equals(responseDU.getId(), idDU)) {
                            return responseDU.getName();
                        }
                    }
                }
            }
        }
        return null;
    }
}
