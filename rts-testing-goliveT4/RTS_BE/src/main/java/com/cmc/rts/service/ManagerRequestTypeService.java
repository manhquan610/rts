package com.cmc.rts.service;

import com.cmc.rts.dto.request.ListNameRequest;
import com.cmc.rts.dto.request.NameRequest;
import com.cmc.rts.dto.response.RequestTypeDTO;
import com.cmc.rts.entity.ManagerRequestType;
import org.springframework.web.bind.annotation.PathVariable;

public interface ManagerRequestTypeService {
    ManagerRequestType addManagerRequestType(ManagerRequestType managerRequestType);
    RequestTypeDTO findAll(NameRequest nameRequest);
    RequestTypeDTO findAll(ListNameRequest listNameRequest);
    ManagerRequestType findOne(Long id);
    void delete(Long id);
}
