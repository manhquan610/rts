package com.cmc.rts.utils;

import com.cmc.rts.dto.requestDto.RequestDTO;
import com.cmc.rts.entity.ManagerJobType;
import com.cmc.rts.service.ManagerAreaService;
import com.cmc.rts.service.ManagerJobTypeService;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import static com.cmc.rts.entity.WorkingTypeEnum.FullTime;

@Component
public class ExcelHelper {

    @Autowired
    ManagerAreaService areaService;

    @Autowired
    ManagerJobTypeService managerJobTypeService;

    //    public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    public static String TYPE = "application/vnd.ms-excel.sheet.macroEnabled.12";
    static String[] HEADERs = {"JobFullName", "StartingDate", "Deadline", "No/Headcount", "Department_ID", "Priority",
            "JR_ID", "Division"};
    static String SHEET = "JobProcess";

    public static boolean hasExcelFormat(MultipartFile file) {
        if (!TYPE.equals(file.getContentType())) {
            return false;
        }
        return true;
    }

    public List<RequestDTO> excelToRequests(InputStream is) {
        try {

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            Workbook workbook = new XSSFWorkbook(is);

            Sheet sheet = workbook.getSheet(SHEET);
            Iterator<Row> rows = sheet.iterator();

            List<RequestDTO> requestDTOS = new ArrayList<>();

            int rowNumber = 0;
            while (rows.hasNext()) {
                Row currentRow = rows.next();

                // skip header
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }

                Iterator<Cell> cellsInRow = currentRow.iterator();

                RequestDTO requestDTO = new RequestDTO();

                int cellIdx = 0;
                while (cellsInRow.hasNext()) {
                    Cell currentCell = cellsInRow.next();

                    switch (cellIdx) {
                        case 1:
                            requestDTO.setCode(currentCell.getStringCellValue());
                            break;
                        case 2:
                            requestDTO.setTarget(currentCell.getStringCellValue().replaceAll("\\s+", ""));
                            break;
                        case 12:
                            requestDTO.setDeadline(dateFormat.format(currentCell.getDateCellValue()));
                            break;
                        case 3:
                            requestDTO.setTitle(currentCell.getStringCellValue());
                            break;
                        case 11:
                            requestDTO.setStartDate(dateFormat.format(currentCell.getDateCellValue()));
                            break;
                        case 26:
                            requestDTO.setManagerGroupId((long) currentCell.getNumericCellValue());
                            break;
                        case 27:
                            requestDTO.setDepartmentId((long) currentCell.getNumericCellValue());
                            break;
                        case 28:
                            requestDTO.setArea(areaService.getOne((long) currentCell.getNumericCellValue()));
                            break;
                        case 29:
                            requestDTO.setPriorityId((long) currentCell.getNumericCellValue());
                            break;
                        default:
                            break;
                    }
//                    requestDTO.setPositionId(172L);
                    requestDTO.setExperienceId(40L);
                    requestDTO.setRequestTypeId(24L);
                    requestDTO.setSkillId("11");
                    requestDTO.setWorkingType(FullTime);
                    requestDTO.setJobType(new ManagerJobType(1L, "Developer", "", false));
                    requestDTO.setDescription("<p>done</p>");
                    cellIdx++;
                }

                requestDTOS.add(requestDTO);
            }

            workbook.close();

            List<RequestDTO> requestChecking = requestDTOS.stream().filter(r->{
                String[] parts = r.getTarget().split("/");
                String part1 = parts[0];
                String part2 = parts[1];
                if (part1.equals(part2)) {
                    r.setNumber(Integer.parseInt(part2));
                    return true;
                }
                return false;
            }).collect(Collectors.toList());



            return requestChecking;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
        }
    }


    public List<RequestDTO> getAllDataExcelRequests(InputStream is) {
        try {

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            Workbook workbook = new XSSFWorkbook(is);

            Sheet sheet = workbook.getSheet(SHEET);
            Iterator<Row> rows = sheet.iterator();

            List<RequestDTO> requestDTOS = new ArrayList<>();

            int rowNumber = 0;
            while (rows.hasNext()) {
                Row currentRow = rows.next();

                // skip header
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }

                Iterator<Cell> cellsInRow = currentRow.iterator();

                RequestDTO requestDTO = new RequestDTO();

                int cellIdx = 0;
                while (cellsInRow.hasNext()) {
                    Cell currentCell = cellsInRow.next();

                    switch (cellIdx) {
                        case 1:
                            requestDTO.setCode(currentCell.getStringCellValue());
                            break;
                        case 2:
                            String[] parts = currentCell.getStringCellValue().replaceAll("\\s+", "").split("/");
                            requestDTO.setNumber(Integer.parseInt(parts[1]));
                            break;
                        case 12:
                            requestDTO.setDeadline(dateFormat.format(currentCell.getDateCellValue()));
                            break;
                        case 3:
                            requestDTO.setTitle(currentCell.getStringCellValue());
                            break;
                        case 11:
                            requestDTO.setStartDate(dateFormat.format(currentCell.getDateCellValue()));
                            break;
                        case 18:
                            requestDTO.setAssigned(currentCell.getStringCellValue());
                            break;
                        case 26:
                            requestDTO.setManagerGroupId((long) currentCell.getNumericCellValue());
                            break;
                        case 27:
                            requestDTO.setDepartmentId((long) currentCell.getNumericCellValue());
                            break;
                        case 28:
                            requestDTO.setArea(areaService.getOne((long) currentCell.getNumericCellValue()));
                            break;
                        case 29:
                            requestDTO.setPriorityId((long) currentCell.getNumericCellValue());
                            break;
                        default:
                            break;
                    }
//                    requestDTO.setPositionId(172L);
                    requestDTO.setExperienceId(40L);
                    requestDTO.setRequestTypeId(24L);
                    requestDTO.setSkillId("11");
                    requestDTO.setWorkingType(FullTime);
                    requestDTO.setJobType(new ManagerJobType(1L, "Developer", "", false));
                    requestDTO.setDescription("<p>done</p>");
                    cellIdx++;
                }

                requestDTOS.add(requestDTO);
            }

            workbook.close();

            List<RequestDTO> requestChecking = requestDTOS.stream().filter(r -> {
                if (r.getAssigned().isEmpty() || r.getAssigned().trim().equals("") || r.getAssigned() == null) {
                    return false;
                }
                return true;
            }).collect(Collectors.toList());

            return requestChecking;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
        }
    }

    public static ByteArrayInputStream tutorialsToExcel(List<RequestDTO> tutorials) {

        try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            Sheet sheet = workbook.createSheet(SHEET);

            // Header
            Row headerRow = sheet.createRow(0);

            for (int col = 0; col < HEADERs.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(HEADERs[col]);
            }

            int rowIdx = 1;
            for (RequestDTO tutorial : tutorials) {
                Row row = sheet.createRow(rowIdx++);
//                row.createCell(0).setCellValue(tutorial.getId());
//                row.createCell(1).setCellValue(tutorial.get);
//                row.createCell(2).setCellValue(tutorial.getDescription());
//                row.createCell(3).setCellValue(tutorial.isPublished());
            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
        }
    }
}
