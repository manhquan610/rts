package com.cmc.rts.dto.requestDto;

import com.cmc.rts.entity.ManagerArea;
import com.cmc.rts.entity.ManagerJobType;
import com.cmc.rts.entity.WorkingTypeEnum;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class RequestDTO {
    private Long id;
    private String title;
//    private Long positionId;
    private String deadline;
    private String startDate;
    private int number;
    private String target;
    private String description;
    private String skillId;
    private Long priorityId;
    private Long experienceId;
    private String certificate;
    private String major;
    private String others;
    private String salary;
    private String benefit;
    private String languageId;
    private Long requestTypeId;
    private Long projectId;
    private Long managerGroupId;
    private Long departmentId;
    private String permissionNames;
    private String username;
    private ManagerArea area;
    private ManagerJobType jobType;
    private WorkingTypeEnum workingType;
    private String requirement;
    private String code;
    private String assigned;
    private String codePosition;
    private Set<Long> idJobLevel;
    public RequestDTO() {
    }


    public RequestDTO(String title, String deadline, Integer number, String description, String skillId,
                      Long priorityId, Long experienceId, Long requestTypeId, Long projectId, Long managerGroupId, Long departmentId,
                      ManagerArea area, ManagerJobType jobType, WorkingTypeEnum workingType, String requirement, String codePosition) {
        this.title = title;
//        this.positionId = positionId;
        this.deadline = deadline;
        this.number = number;
        this.description = description;
        this.skillId = skillId;
        this.priorityId = priorityId;
        this.experienceId = experienceId;
        this.requestTypeId = requestTypeId;
        this.projectId = projectId;
        this.managerGroupId = managerGroupId;
        this.departmentId = departmentId;
        this.area = area;
        this.jobType = jobType;
        this.workingType = workingType;
        this.requirement = requirement;
        this.codePosition = codePosition;
    }
}
