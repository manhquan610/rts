package com.cmc.rts.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ChannelSourceResponse {

    @JsonProperty("source")
    private String source;
    @JsonProperty("channelList")
    private List<ChannelResponse> channelResponseList;
}
