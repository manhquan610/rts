package com.cmc.rts.repository.custom.impl;

import com.cmc.rts.entity.EmailQueue;
import com.cmc.rts.repository.custom.EmailQueueRepositoryCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class EmailQueueRepositoryImpl implements EmailQueueRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<EmailQueue> findByName(String name, int offset, int limit) {
        List<EmailQueue> list = null;
        try {
            StringBuilder sql = new StringBuilder("select * from email_queue m where deleted = 0");

            if (name != null && !name.isEmpty()) {
                sql.append(" and m.name like '" + name + "'");
            }
            if (!String.valueOf(limit).isEmpty() && !String.valueOf(offset).isEmpty()) {
                offset = offset > 0 ? --offset : offset;
                offset = offset * limit;
                sql.append(" limit  " + limit);
                sql.append(" offset  " + offset);
            }
            Query query = entityManager.createNativeQuery(sql.toString(), EmailQueue.class);

            if (query.getResultList().size() > 0) {
                list = query.getResultList();
            }
        } catch (Exception e) {
            return new ArrayList<>();
        } finally {
            return list;
        }
    }
}
