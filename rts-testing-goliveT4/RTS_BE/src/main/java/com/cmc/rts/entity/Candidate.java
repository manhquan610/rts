package com.cmc.rts.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Candidate extends BaseDomain{
    @Column
    private Long candidateId;
    @Column(columnDefinition = "varchar(50)")
    private String fullName;
    @Column
    private Date birthday;
    @Column
    private String email;
    @Column
    private String phone;
    @Column
    private String statusCandidate;
    @Column
    private Date sendDate;
    @Column
    private Date applySince;
    @Enumerated(EnumType.STRING)
    @Column
    private CandidateStateEnum candidateState;
}
