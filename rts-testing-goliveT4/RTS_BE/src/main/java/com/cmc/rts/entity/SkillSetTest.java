//package com.cmc.rts.entity;
//
//import lombok.*;
//
//import javax.persistence.*;
//import java.io.Serializable;
//
///**
// * @author: nthieu10
// * 7/19/2022
// **/
//@Entity
//@Table(schema = "skillset", name = "test")
//@Data
//public class SkillSetTest implements Serializable {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Integer id;
//    @Column(name = "user")
//    private String user;
//}
