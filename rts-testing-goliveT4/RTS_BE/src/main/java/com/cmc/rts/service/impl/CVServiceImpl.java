package com.cmc.rts.service.impl;


import com.cmc.rts.dto.request.CVRequest;
import com.cmc.rts.dto.response.*;
import com.cmc.rts.entity.*;
import com.cmc.rts.entity.v2.V2ManagerJobLevel;
import com.cmc.rts.entity.v2.V2Request;
import com.cmc.rts.entity.v2.V2RequestManagerJobLevels;
import com.cmc.rts.exception.CustomException;
import com.cmc.rts.repository.CandidateLogRepository;
import com.cmc.rts.repository.ManagerJobLevelRepository;
import com.cmc.rts.repository.ManagerJobTypeRepository;
import com.cmc.rts.repository.RequestRepository;
import com.cmc.rts.repository.v2.V2ManagerJobLevelRepository;
import com.cmc.rts.repository.v2.V2RequestManagerJobLevelRepository;
import com.cmc.rts.repository.v2.V2RequestRepository;
import com.cmc.rts.response.BaseResponse;
import com.cmc.rts.service.CVService;
import com.cmc.rts.service.CandidateService;
import com.cmc.rts.service.ChannelService;
import com.cmc.rts.service.RequestService;
import com.cmc.rts.utils.Constants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
@PropertySource("classpath:application-dev.properties")
@Slf4j
public class CVServiceImpl implements CVService {

    private static final java.util.logging.Logger LOGGER = Logger.getLogger(CVServiceImpl.class.getName());

    @Autowired
    private ChannelService channelService;

    @Autowired
    private ManagerJobLevelRepository jobLevelRepository;

    @Autowired
    RequestService requestService;

    @Autowired
    private CandidateService candidateService;

    private final String CV_TOKEN = "Bearer BBpqW81iAO0wHN7N7tvWVY82AJF6_i6QJAJs4M1tcbE";

    private final String CANDIDATE_TOKEN = "Bearer Oo2V-DGze-";

    
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Value("${candidate.list.api.url}")
    private String cmcGlobalApiUrl;

    @Autowired
    private CandidateLogRepository candidateLogRepository;

    @Autowired
    private RequestRepository requestRepository;

    @Autowired
    private V2RequestRepository v2RequestRepository;

    @Autowired
    private V2ManagerJobLevelRepository v2ManagerJobLevelRepository;

    @Autowired
    private V2RequestManagerJobLevelRepository v2RequestManagerJobLevelRepository;

    @Autowired
    private ManagerJobTypeRepository managerJobTypeRepository;

    @Value("${candidate.find.path}")
    private String candidateFind;

    @Value("${cv.create}")
    private String createCV;

    @Value("${cv.update}")
    private String updateCV;

    @Override
    public CandidateRootResponse searchCandidates(CandidateStateEnum candidateState, String startDate, String endDate, String searchValues, Integer page, Integer size, String hasCV) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        List<String> roles = authentication.getAuthorities().stream()
                .map(r -> r.getAuthority()).collect(Collectors.toList());
        StringBuilder url = new StringBuilder(cmcGlobalApiUrl + candidateFind + "?page=" + page
                + "&size=" + size + "&findValue=" + searchValues);
        if (candidateState != null) {
            url.append("&stepRts=" + candidateState);
        }
        if (hasCV != null) {
            url.append("&hasCV=" + hasCV);
        }
        url.append("&orderBy=candidate_id&sort=DESC&ta=" + authentication.getPrincipal().toString() + "&role_name=" + roles.get(1));
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", CANDIDATE_TOKEN);
        headers.setContentType(MediaType.APPLICATION_JSON);
        LOGGER.info("URL: "+url);
        String response = restTemplate.getForObject(url.toString(), String.class, headers);
        CandidateRootResponse candidateRootResponse = new CandidateRootResponse();
        try {
            candidateRootResponse = objectMapper.readValue(response, CandidateRootResponse.class);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return candidateRootResponse;
    }

    @Override
    public CVRootResponse getAllCV( CandidateStateEnum candidateState, String startDate, String endDate, String searchString, Integer limit, Integer offset, String hasCV) {
        ModelMapper mapper = new ModelMapper();
        if (searchString == null || searchString.isEmpty()) {
            searchString = "";
        }
        searchString = searchString.trim();
        CandidateRootResponse candidateDTOS = searchCandidates(candidateState, startDate, endDate, searchString, offset, limit, hasCV);
        CVRootResponse cvRootResponse = mapper.map(candidateDTOS, CVRootResponse.class);

        /*Comparator<CVResponse> comparator = (c1, c2) -> {
            return Long.valueOf(c2.getCreatedDate().getTime()).compareTo(c1.getCreatedDate().getTime());
        };
        Collections.sort(cvRootResponse.getCandidateList(), comparator);*/

        List<ChannelResponse> channels = channelService.getChannel().getChannelResponseList();
        Map<String,String> channelMap = channels.stream().collect(Collectors.toMap(v->v.getId().toString(),v -> v.getName()));

        List<CVResponse> cvs = cvRootResponse.getCandidateList();
        List<Long> requestIds = cvs.stream().filter(v -> !ObjectUtils.isEmpty(v.getJobRtsId())).map(v -> v.getJobRtsId()).collect(Collectors.toList());
        List<V2Request> requests = v2RequestRepository.findV2RequestByIdIn(requestIds);
        Map<Long, V2Request> requestMap = requests.stream().collect(Collectors.toMap(v -> v.getId(), v -> v));

        List<V2RequestManagerJobLevels> jobLevels = v2RequestManagerJobLevelRepository.findV2RequestManagerJobLevelsByRequestIdIn(requestIds);
        List<Long> managerJobLevelIds = jobLevels.stream().map(v-> v.getManagerJobLevelsId()).collect(Collectors.toList());

        List<V2ManagerJobLevel> managerJobLevels = v2ManagerJobLevelRepository.findV2ManagerJobLevelByIdIn(managerJobLevelIds);
        Map<Long, V2ManagerJobLevel> managerJobLevelMap = managerJobLevels.stream().collect(Collectors.toMap(v-> v.getId(), v-> v));
        Map<Long, List<V2RequestManagerJobLevels>> v2RequestManagerJobLevelMap = jobLevels.stream().collect(Collectors.groupingBy(V2RequestManagerJobLevels::getRequestId, Collectors.toList()));

        List<Long> managerJobTypeIds = managerJobLevels.stream().map(v -> v.getJobTypeId()).distinct().collect(Collectors.toList());
        List<ManagerJobType> managerJobTypes = managerJobTypeRepository.findManagerJobTypeByIdIn(managerJobTypeIds);
        Map<Long, ManagerJobType> managerJobTypeMap = managerJobTypes.stream().collect(Collectors.toMap(v -> v.getId(), v -> v));


        for (CVResponse cv : cvs) {
            if (!ObjectUtils.isEmpty(cv.getStepRts()) && cv.getStepRts().equals("none")) {
                cv.setStepRts("Source");
            }
            cv.setChannelName(channelMap.get(cv.getChannel().toString()));
            if (cv.getJobRtsId() != null) {
                V2Request request = requestMap.get(cv.getJobRtsId());
                if (request != null) {
                    List<V2RequestManagerJobLevels> v2RequestManagerJobLevel = v2RequestManagerJobLevelMap.get(request.getId());
                    if (!ObjectUtils.isEmpty(v2RequestManagerJobLevel)) {
                        cv.setJobLevel(jobLevel(managerJobLevelMap, v2RequestManagerJobLevel, managerJobTypeMap));
                    }
                    cv.setJob(request.getName());
                } else {
                    cv.setJob("");
                    cv.setJobLevel("");
                }
            }
        }
        return cvRootResponse;
    }

    private String jobLevel(Map<Long, V2ManagerJobLevel> managerJobLevelMap, List<V2RequestManagerJobLevels> v2RequestManagerJobLevels, Map<Long, ManagerJobType> managerJobTypeMap){
        List<String> jobs = new ArrayList<>();
        String jobLevel = null;
        for(V2RequestManagerJobLevels managerJobLevels : v2RequestManagerJobLevels){
            V2ManagerJobLevel v2ManagerJobLevel = managerJobLevelMap.get(managerJobLevels.getManagerJobLevelsId());
            if(!ObjectUtils.isEmpty(v2ManagerJobLevel) && !ObjectUtils.isEmpty(managerJobTypeMap)) {
                ManagerJobType managerJobType = managerJobTypeMap.get(v2ManagerJobLevel.getJobTypeId());
                if (!ObjectUtils.isEmpty(managerJobType)) {
                    jobs.add(managerJobType.getName() + " - " + v2ManagerJobLevel.getName());
                }
            }
        }
        if(!ObjectUtils.isEmpty(jobs)) {
            jobs = jobs.stream().filter(v-> !ObjectUtils.isEmpty(v)).collect(Collectors.toList());
            jobLevel = jobs.get(0);
        }
        return jobLevel;
    }

    @Override
    public ResponseEntity<?> createCV(CVRequest cvRequest) throws Exception {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        cvRequest.setAssignees(authentication.getPrincipal().toString());
        cvRequest.setTa(authentication.getPrincipal().toString());
        String url = cmcGlobalApiUrl + createCV;
        log.info("url {}", url);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        headers.add("Authorization", CV_TOKEN);
        headers.add("userName","bnchau");
        ObjectMapper op = new ObjectMapper();
        log.info("cv request {}", cvRequest);
        HttpEntity response = new HttpEntity<String>(op.writeValueAsString(cvRequest), headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<CandidateResponse> requestBody = restTemplate.exchange(url, HttpMethod.POST, response, CandidateResponse.class);
        if (requestBody.getStatusCode().value() == 200 && requestBody.getBody().getErrorMessage().equals("success")){
            CandidateLog candidateLog = new CandidateLog();
            long requestId = 0L;
            if (cvRequest.getRequestId() != null && !cvRequest.getRequestId().equals("")) {
                requestId = Long.parseLong(cvRequest.getRequestId());
            }
            candidateLog.setRequestId(requestId);
            candidateLog.setCandidateId(requestBody.getBody().getCandidateId());
            candidateLog.setCreatedBy(authentication.getPrincipal().toString());
            candidateLog.setLog("Create candidate successfully!!!");
            candidateLogRepository.save(candidateLog);
            return new ResponseEntity<>(new BaseResponse(HttpStatus.OK.value(), String.valueOf(HttpStatus.OK.value()), "Create candidate successfully!", null), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new BaseResponse(HttpStatus.CONFLICT.value(), String.valueOf(HttpStatus.CONFLICT.value()), requestBody.getBody().getErrorMessage(), null), HttpStatus.CONFLICT);
        }
    }

    @Override
    public CandidateResponse updateCV(CVRequest cvRequest) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        try{
            CandidateResponse candidateResponse = candidateService.findCandidateById(cvRequest.getId());

            CVRequest cvRequestUpdate = new CVRequest();

            BeanUtils.copyProperties(candidateResponse, cvRequestUpdate);

            cvRequestUpdate.setFullName(cvRequest.getFullName());
            cvRequestUpdate.setLastCompany(cvRequest.getLastCompany());
            cvRequestUpdate.setSkill(cvRequest.getSkill());
            cvRequestUpdate.setExperience(cvRequest.getExperience());
            cvRequestUpdate.setLinkCv(cvRequest.getLinkCv());
            cvRequestUpdate.setTa(cvRequest.getTa());
            cvRequestUpdate.setChannel(cvRequest.getChannel());
            cvRequestUpdate.setAssignees(cvRequest.getAssignees());
            if (authentication.getAuthorities().stream().anyMatch(x -> Constants.ROLE.ROLE_EB.equals(x.toString()) || Constants.ROLE.ROLE_ADMIN.equals(x.toString()) || Constants.ROLE.ROLE_TA_MANAGER.equals(x.toString()))) {
                cvRequestUpdate.setRefererUser(cvRequest.getRefererUser());
            }

            log.info("cvRequestUpdate {}", cvRequestUpdate);

            String url = cmcGlobalApiUrl + updateCV + "?candidateId=" + cvRequest.getId();
            log.info("url {}", url);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
            headers.add("Authorization", CV_TOKEN);

            ObjectMapper op = new ObjectMapper();
            HttpEntity<String> response = new HttpEntity<String>(op.writeValueAsString(cvRequestUpdate), headers);
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<CandidateResponse> requestBody = restTemplate.exchange(url, HttpMethod.PUT, response, CandidateResponse.class);
            if (requestBody.getStatusCode().value() == 200){
                requestBody.getBody();
            }
            return null;
        }catch (Exception e){
            e.printStackTrace();
            throw new CustomException("Update failure!", HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }
}
