package com.cmc.rts.entity;

import com.cmc.rts.dto.response.UserResponse;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RequestSubmit extends BaseDomain {
//    @ManyToOne
//    @JoinColumn
//    private UserInfo userSent;
//    @ManyToOne
//    @JoinColumn
//    private UserInfo userReceiver;
    @Column
    private String groupName;
    @Enumerated(EnumType.STRING)
    @Column
    private RequestStatusEnum requestStatus;
    @ManyToOne
    private RejectReason rejectReason;
    @ManyToOne
    @JoinColumn
    private EmailQueue emailQueue;

    private transient UserResponse userSent;
    private String userSentLdap;
    private transient UserResponse userReceiver;
    private String userReceiverLdap;
}
