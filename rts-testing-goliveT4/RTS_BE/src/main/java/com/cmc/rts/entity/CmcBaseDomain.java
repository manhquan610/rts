package com.cmc.rts.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@MappedSuperclass
@Getter
@Setter
@NoArgsConstructor
public class CmcBaseDomain extends CategoryBaseDomain {
    @Column
    private Boolean developmentUnit;
    @Column
    private Boolean internalDu;
    @Column
    private Boolean groupSale;
    @Column
    private Long parentId;
    @Column
    private String depth;
    @Column
    private String manager;
//    @ManyToOne
//    @JoinColumn
//    private UserInfo userManager;
}
