package com.cmc.rts.service;


import com.cmc.rts.dto.requestDto.JobLevelRequest;
import com.cmc.rts.dto.response.ResponseAPI;
import com.cmc.rts.entity.ManagerJobLevel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Set;

public interface ManagerJobLevelService {

    Page<ManagerJobLevel> getAllJobLevels(Pageable pageable);

    ResponseAPI insertJobLevel(JobLevelRequest jobLevel);

    ResponseAPI updateJobType(JobLevelRequest jobLevel);

    ResponseAPI deleteJobType(Long idJobLevel);

    Page<ManagerJobLevel> findAllJobLevelsByPosition(Long id, Pageable pageable);

    Page<ManagerJobLevel> searchByNameLevel(String searchValues, Pageable pageable);

    ManagerJobLevel searchById(Long id);


}
