package com.cmc.rts.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.sql.Timestamp;

@Data
@Builder
public class UserResponse {
    @JsonProperty("username")
    private String username;
    @JsonProperty("idUser")
    private String idUser;
    @JsonProperty("accessFailedCount")
    private long accessFailedCount;

    @JsonProperty("lastName")
    private String lastName;
    @JsonProperty("firstName")
    private String firstName;
    @JsonProperty("fullName")
    private String fullName;
    @JsonProperty("createdDate")
    private Timestamp lastAccess;
    @JsonProperty("isActive")
    private boolean isActive;
    @JsonProperty("secret")
    private String secret;
    @JsonProperty("password")
    private String password;
    @JsonProperty("emailAddress")
    private String emailAddress;
    @JsonProperty("isDeleted")
    private Boolean isDeleted;
    @JsonProperty("gender")
    private int gender;
    @JsonProperty("employeeId")
    private String employeeId;
    @JsonProperty("mobile")
    private String mobile;
    @JsonProperty("employeeType")
    private Long employeeType;

    @JsonProperty("ldapModifiedOn")
    private Timestamp ldapModifiedOn;
    @JsonProperty("currentUserName")
    private String currentUserName;
    @JsonProperty("id")
    private Long id;
    @JsonProperty("createdBy")
    private String createdBy;
    @JsonProperty("createdOn")
    private String createdOn;

    public UserResponse() {
    }

    public UserResponse(String username, String idUser, long accessFailedCount, String lastName, String firstName, String fullName, Timestamp lastAccess, boolean isActive, String secret, String password, String emailAddress, Boolean isDeleted, int gender, String employeeId, String mobile, Long employeeType, Timestamp ldapModifiedOn, String currentUserName, Long id, String createdBy, String createdOn) {
        this.username = username;
        this.idUser = idUser;
        this.accessFailedCount = accessFailedCount;
        this.lastName = lastName;
        this.firstName = firstName;
        this.fullName = fullName;
        this.lastAccess = lastAccess;
        this.isActive = isActive;
        this.secret = secret;
        this.password = password;
        this.emailAddress = emailAddress;
        this.isDeleted = isDeleted;
        this.gender = gender;
        this.employeeId = employeeId;
        this.mobile = mobile;
        this.employeeType = employeeType;
        this.ldapModifiedOn = ldapModifiedOn;
        this.currentUserName = currentUserName;
        this.id = id;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
    }
}
