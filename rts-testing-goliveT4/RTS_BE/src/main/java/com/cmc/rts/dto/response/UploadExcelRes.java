package com.cmc.rts.dto.response;


import com.cmc.rts.entity.CandidateListRes;
import lombok.Getter;
import lombok.Setter;

/**
 * Create by trungtx
 * Date: 11/14/2021
 * Time: 10:27 PM
 * Project: CMC_RTS
 */

@Getter
@Setter
public class UploadExcelRes {

    private String selectedCandidate;
    private CandidateListRes[] candidateList;
    private String errorMessage;
    private String[] emailDuplicatedList;
    private String selectedCandidateId;
    private String projectroleList;
    private String totalElements;
}
