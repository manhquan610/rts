package com.cmc.rts.dto.response;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.time.DateUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;


@Getter @Setter
public class TimelineInterviewDTO {

    private String title;
    private String startTime;
    private String endTime;
    private Long requestId;
    private Long candidateId;
    private String name;
    private String createBy;

    public TimelineInterviewDTO(String title, Date startTime, Date endTime, Long requestId, Long candidateId, String name, String createBy) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.title = title;
        if (startTime == null){
            this.startTime = null;
        } else {
            this.startTime = dateFormat.format(startTime);
        }
        if (endTime == null){
            this.endTime = null;
        } else {
            this.endTime = dateFormat.format(endTime);
        }
        this.requestId = requestId;
        this.candidateId = candidateId;
        this.name = name;
        this.createBy = createBy;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimelineInterviewDTO that = (TimelineInterviewDTO) o;
        return Objects.equals(requestId, that.requestId) && Objects.equals(candidateId, that.candidateId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestId, candidateId);
    }
}
