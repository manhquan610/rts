package com.cmc.rts.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;

import java.sql.Timestamp;

@Mapper(componentModel = "spring", unmappedSourcePolicy = ReportingPolicy.IGNORE)
public abstract class CommonMapper {


    @Named("CONVERT_TIMESTAMP_TO_STRING")
    String convertObjectIdToString(Timestamp timestamp) {
        return timestamp.toString();
    }
}
