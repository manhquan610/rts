package com.cmc.rts.dto.response;

import com.cmc.rts.entity.ManagerPriority;

import java.util.List;

public class PriorityDTO {
    private List<ManagerPriority> manager;
    private Integer total_item;
    private Integer total_page;

    public List<ManagerPriority> getManager() {
        return manager;
    }

    public void setManager(List<ManagerPriority> manager) {
        this.manager = manager;
    }

    public Integer getTotal_item() {
        return total_item;
    }

    public void setTotal_item(Integer total_item) {
        this.total_item = total_item;
    }

    public Integer getTotal_page() {
        return total_page;
    }

    public void setTotal_page(Integer total_page) {
        this.total_page = total_page;
    }
}
