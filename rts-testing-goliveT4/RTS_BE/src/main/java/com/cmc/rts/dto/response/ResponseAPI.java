package com.cmc.rts.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseAPI {

    private int status;
    private String message;

    public ResponseAPI(int status, String message) {
        this.status = status;
        this.message = message;
    }
}
