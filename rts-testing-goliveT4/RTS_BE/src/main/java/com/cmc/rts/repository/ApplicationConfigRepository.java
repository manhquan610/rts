package com.cmc.rts.repository;

import com.cmc.rts.entity.ApplicationConfig;
import com.cmc.rts.repository.custom.ApplicationConfigCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationConfigRepository extends JpaRepository<ApplicationConfig, Long>, ApplicationConfigCustom {
}
