package com.cmc.rts.repository;

import com.cmc.rts.dto.response.InterviewDTO;
import com.cmc.rts.dto.response.TimelineInterviewDTO;
import com.cmc.rts.entity.Interview;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface InterviewRepository extends JpaRepository<Interview, Long> {
    @Query(value = "SELECT new com.cmc.rts.dto.response.InterviewDTO(i.id, i.request.name, i.title, " +
            " i.interviewer, i.comment, i.startTime, i.endTime, i.status, i.createdDate,i.createdBy) " +
            "FROM Interview as i " +
            "WHERE i.request.id = :request_id ")
    List<InterviewDTO> getAllInterview(@Param(value = "request_id") Long request_id);

    @Query(value = "SELECT new com.cmc.rts.dto.response.InterviewDTO(i.id, i.request.name, i.title, " +
            " i.interviewer, i.comment, i.startTime, i.endTime, i.status, i.createdDate,i.createdBy) " +
            " FROM Interview as i " +
            " WHERE (:searchValue = '' or (lower(i.title) LIKE lower(concat('%',:searchValue,'%'))) ) " +
            " AND (:location = '' or i.location.address = :location ) " +
            " AND (:interviewer = '' or i.interviewer = :interviewer ) " +
            " AND (:status is null or i.status = :status ) " +
            " AND i.request.id = :request_id ")
    Page<InterviewDTO> searchInterview(@Param(value = "searchValue") String searchValue,
                                       @Param(value = "location") String location,
                                       @Param(value = "interviewer") String interviewer,
                                       @Param(value = "status") Boolean status,
                                       @Param(value = "request_id") Long request_id,
                                       Pageable pageable);

    @Query(value = "SELECT new com.cmc.rts.dto.response.InterviewDTO(i.id, i.request.name, i.title, " +
            "i.interviewer, i.comment, i.startTime, i.endTime, i.status, i.createdDate,i.createdBy) " +
            "FROM Interview i " +
            "WHERE i.request.id = :request_id AND i.candidateId = :candidate_id ")
    List<InterviewDTO> getInterviewByCandidateIdAndRequestId(@Param(value = "request_id") Long request_id,
                                                             @Param(value = "candidate_id") Long candidate_id);


    @Query(value = "SELECT * FROM interview as i " +
                    "inner join (SELECT itv.request_id, itv.candidate_id, MAX(itv.created_date) as maxTime " +
                                "from interview as itv " +
                                "group by itv.request_id, itv.candidate_id " +
                                ") intview on i.request_id = intview.request_id and " +
                                           "i.candidate_id = intview.candidate_id and " +
                                           "i.created_date = intview.maxTime " +
                    "WHERE " +
                    "(:textSearch is null or ((lower(i.title) LIKE CONCAT('%',:textSearch,'%'))or (lower(i.interviewer) LIKE CONCAT('%',:textSearch,'%')))) " +
                    "and (i.start_time between :startDate and :endDate)", nativeQuery = true)
    List<Interview> getAllInterviewOneWeek(@Param(value = "startDate") Date startDate,
                                                      @Param(value = "endDate") Date endDate,
                                                      @Param(value = "textSearch") String textSearch);

    @Query(value = "select * from interview as i " +
                   "where i.candidate_id = :candidateId and (:startTime between i.start_time and DATE_ADD(i.start_time, INTERVAL 1 HOUR))", nativeQuery = true)
    List<Interview> checkDuplicateCandidate(@Param(value = "candidateId") Long candidateId,
                                            @Param(value = "startTime") Date startTime);

    @Query(value = "select * from interview as i " +
            "where i.candidate_id = :candidateId and " +
            "(:startTime between i.start_time and DATEADD('HOURS',1, i.start_time))", nativeQuery = true)
    List<Interview> checkDuplicateCandidateH2(@Param(value = "candidateId") Long candidateId,
                                            @Param(value = "startTime") Date startTime);
}
