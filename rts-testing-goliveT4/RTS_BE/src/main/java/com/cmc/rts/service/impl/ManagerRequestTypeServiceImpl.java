package com.cmc.rts.service.impl;

import com.cmc.rts.dto.request.ListNameRequest;
import com.cmc.rts.dto.request.NameRequest;
import com.cmc.rts.dto.response.RequestTypeDTO;
import com.cmc.rts.entity.ManagerRequestType;
import com.cmc.rts.repository.ManagerRequestTypeRepository;
import com.cmc.rts.service.ManagerRequestTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class ManagerRequestTypeServiceImpl implements ManagerRequestTypeService {

    @Autowired
    ManagerRequestTypeRepository managerRequestTypeRepository;

    @Override
    @Transactional
    public ManagerRequestType addManagerRequestType(ManagerRequestType managerRequestType) {
        return managerRequestTypeRepository.save(managerRequestType);
    }

    @Override
    public RequestTypeDTO findAll(NameRequest nameRequest) {
        RequestTypeDTO requestTypeDTO = new RequestTypeDTO();
         String lstName = "";
        if(nameRequest !=null && nameRequest.getLstName() !=null){
            lstName = nameRequest.getLstName();
        }
        Integer offset = null;
        if(nameRequest !=null && nameRequest.getOffset() !=null){
            offset = nameRequest.getOffset();
        }
        Integer limit = null;
        if(nameRequest !=null && nameRequest.getLimit()!=null){
            limit = nameRequest.getLimit();
        }

        Integer first = 0;
        if(offset !=null && limit !=null){
            first = (offset-1)*limit;
        }else {
            limit = 10;
        }
        requestTypeDTO.setManager(managerRequestTypeRepository.findByNameSame(first,limit,lstName));
        Integer total_item = (managerRequestTypeRepository.findByNameSame(null,null,lstName)).size();
        Integer total_page = 0;
        if(limit !=0){
            total_page = total_item / limit ;
            if((total_item % limit) != 0){
                total_page = total_page + 1;
            }
        }
        requestTypeDTO.setTotal_item(total_item);
        requestTypeDTO.setTotal_page(total_page);
        return requestTypeDTO;
    }


    @Override
    public RequestTypeDTO findAll(ListNameRequest listNameRequest) {
        RequestTypeDTO requestTypeDTO = new RequestTypeDTO();
        List<String> lstName = new ArrayList<>();
        if(listNameRequest !=null && listNameRequest.getLstName() !=null){
            lstName = listNameRequest.getLstName();
        }
        Integer offset = null;
        if(listNameRequest !=null && listNameRequest.getOffset() !=null){
            offset = listNameRequest.getOffset();
        }
        Integer limit = null;
        if(listNameRequest !=null && listNameRequest.getLimit()!=null){
            limit = listNameRequest.getLimit();
        }

        Integer first = 0;
        if(offset !=null && limit !=null){
            first = (offset-1)*limit;
        }else {
            limit = 10;
        }
        requestTypeDTO.setManager(managerRequestTypeRepository.findByName(first,limit,lstName));
        Integer total_item = (managerRequestTypeRepository.findByName(null,null,lstName)).size();
        Integer total_page = 0;
        if(limit !=0){
            total_page = total_item / limit ;
            if((total_item % limit) != 0){
                total_page = total_page + 1;
            }
        }
        requestTypeDTO.setTotal_item(total_item);
        requestTypeDTO.setTotal_page(total_page);
        return requestTypeDTO;
    }

    @Override
    public ManagerRequestType findOne(Long id) {
        return managerRequestTypeRepository.findOne(id);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        managerRequestTypeRepository.delete(id);
    }
}
