package com.cmc.rts.rest;

import com.cmc.rts.dto.DeleteDTO;
import com.cmc.rts.dto.request.AreaRequest;
import com.cmc.rts.dto.request.InterviewRequest;
import com.cmc.rts.dto.response.ResponseAPI;
import com.cmc.rts.entity.ManagerArea;
import com.cmc.rts.exception.CustomException;
import com.cmc.rts.repository.ManagerAreaRepository;
import com.cmc.rts.service.ManagerAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ManagerAreaController {
   @Autowired
   ManagerAreaService managerAreaService;

   ManagerAreaRepository managerAreaRepository;


    @GetMapping(value = "/manager-area/getAll")
    public ResponseEntity<Page<ManagerArea>> getAllArea(@RequestParam(value = "limit") Integer limit,
                                                        @RequestParam(value = "page") Integer page) {
        Pageable pageable = new PageRequest(page - 1, limit);
        return new ResponseEntity<>(managerAreaService.getAllArea(pageable), HttpStatus.OK);
    }

    @GetMapping(value = "/manager-area/same")
    public ResponseEntity<Page<ManagerArea>> searchByName(@RequestParam(value = "nameSearch", required = false) String name,
                                                             @RequestParam(value = "limit") Integer limit,
                                                             @RequestParam(value = "page") Integer page) {
        Pageable pageable = new PageRequest(page - 1, limit);
        return new ResponseEntity<>(managerAreaService.findByName(name,pageable), HttpStatus.OK);
    }

    @GetMapping(value = "/manager-area/exact")
    public ResponseEntity<Page<ManagerArea>> searchByListName(@RequestParam(value = "nameSearch", required = false) String listName,
                                                             @RequestParam(value = "limit") Integer limit,
                                                             @RequestParam(value = "page") Integer page) {
        Pageable pageable = new PageRequest(page - 1, limit);
        return new ResponseEntity<>(managerAreaService.findByListName(listName,pageable), HttpStatus.OK);
    }

    @GetMapping(value = "/manager-area/getByID")
    public ResponseEntity<ManagerArea> searchByID(@RequestParam(value = "id", required = false) Long id) {
        return new ResponseEntity<>(managerAreaService.getOne(id), HttpStatus.OK);
    }

    @PostMapping(value = "/createArea")
    public ResponseEntity<?> createArea(@RequestBody AreaRequest request) throws Exception {
        return new ResponseEntity<ManagerArea>(managerAreaService.createArea(request),HttpStatus.CREATED);
    }

    @PutMapping(value = "/updateArea")
    public ResponseEntity<?> updateArea(@RequestBody AreaRequest request) throws Exception {

        return new ResponseEntity<ManagerArea>(managerAreaService.updateArea(request),HttpStatus.OK);
    }

    @PostMapping(value = "/deleteAreas")
    public void delete(@RequestBody DeleteDTO deleteDTO) throws Exception {
        Long[] ids = deleteDTO.getIds();
        for(Long id: ids) {
            try {
                managerAreaService.deleteAreas(id);
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception("Can't Delete! Because this item already exists in other information");
            }
        }
    }


}
