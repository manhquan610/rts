package com.cmc.rts.dto.request;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class InterviewRequest {
    private Long id;
    private Long requestId;
    private Long candidateId;
    private Long locationId;
    private String title;
    private String startTime;
    private String interviewer;
    private String comment;
    private String createdBy;

    public InterviewRequest(Long requestId, Long candidateId, Long locationId, String title, String startTime, String interviewer, String comment, String createdBy) {
        this.requestId = requestId;
        this.candidateId = candidateId;
        this.locationId = locationId;
        this.title = title;
        this.startTime = startTime;
        this.interviewer = interviewer;
        this.comment = comment;
        this.createdBy = createdBy;
    }
}
