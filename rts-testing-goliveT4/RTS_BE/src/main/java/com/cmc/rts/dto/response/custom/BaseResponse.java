package com.cmc.rts.dto.response.custom;

import lombok.Data;

@Data
public class BaseResponse {
    private Object data;
    private Object message;
    private Object statusCode;
}
