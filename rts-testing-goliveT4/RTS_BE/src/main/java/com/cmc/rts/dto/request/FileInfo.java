package com.cmc.rts.dto.request;


import lombok.Getter;
import lombok.Setter;

/**
 * Create by trungtx
 * Date: 11/14/2021
 * Time: 10:18 PM
 * Project: CMC_RTS
 */

@Getter
@Setter
public class FileInfo {

    private String fileName;
    private String previewUrl;
}
