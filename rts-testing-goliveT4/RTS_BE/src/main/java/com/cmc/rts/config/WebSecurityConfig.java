package com.cmc.rts.config;

import com.cmc.rts.security.CustomAccessDeniedHandler;
import com.cmc.rts.security.JWTAuthorizationFilter;
import com.cmc.rts.service.ReportService;
import com.cmc.rts.service.impl.ReportTAServiceImpl;
import com.cmc.rts.service.impl.ReportTicketServiceImpl;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.web.client.RestTemplate;

import static com.cmc.rts.utils.Constants.ROLE.*;

@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private UserDetailsService userDetailsService;

    public WebSecurityConfig(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public ReportService TicketReport(){
        return new ReportTicketServiceImpl();
    }

    @Bean
    public ReportService TAReport(){
        return new ReportTAServiceImpl();
    }

//    @Override
//    public void configure(WebSecurity web){
//        web.ignoring().antMatchers("swagger-ui.html").antMatchers("/v2/api-docs");
//    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors()
                .and().csrf().disable().authorizeRequests()
                .antMatchers(HttpMethod.POST, "/request/**/assign").hasAnyAuthority(ROLE_TA_LEADER, ROLE_ADMIN, ROLE_TA_MANAGER)
                .antMatchers(HttpMethod.POST, "/request/", "/request").hasAnyAuthority(ROLE_TA_MANAGER, ROLE_DU_LEAD, ROLE_ADMIN, ROLE_TA_LEADER)
                .antMatchers(HttpMethod.POST, "/request/**").hasAnyAuthority(ROLE_TA_MANAGER, ROLE_DU_LEAD, ROLE_GROUP_LEAD, ROLE_ADMIN, ROLE_TA_LEADER, ROLE_EB)
                .antMatchers(HttpMethod.POST, "/createInterview").hasAnyAuthority(ROLE_TA_MANAGER, ROLE_TA_LEADER, ROLE_TA_MEMBER, ROLE_ADMIN)
                .antMatchers(HttpMethod.GET, "/request-close/**").hasAnyAuthority(ROLE_TA_MANAGER, ROLE_DU_LEAD, ROLE_GROUP_LEAD, ROLE_TA_LEADER, ROLE_EB, ROLE_ADMIN)
                .antMatchers(HttpMethod.GET, "/request-approve/**").hasAnyAuthority(ROLE_TA_MANAGER, ROLE_GROUP_LEAD, ROLE_ADMIN)
                .antMatchers(HttpMethod.GET, "/request-approve2/**").hasAnyAuthority(ROLE_TA_MANAGER, ROLE_ADMIN)
                .antMatchers(HttpMethod.GET, "/request-reject/**").hasAnyAuthority(ROLE_TA_MEMBER, ROLE_TA_LEADER, ROLE_GROUP_LEAD, ROLE_ADMIN, ROLE_TA_MANAGER)
                .antMatchers(HttpMethod.GET, "/request-submit/**").hasAnyAuthority(ROLE_TA_MANAGER, ROLE_DU_LEAD, ROLE_ADMIN)
                .antMatchers(HttpMethod.GET, "/sf4c/**").hasAnyAuthority(ROLE_TA_MANAGER, ROLE_DU_LEAD, ROLE_ADMIN, ROLE_TA_LEADER, ROLE_EB)
                .antMatchers(HttpMethod.PUT, "/updateCV").hasAnyAuthority(ROLE_TA_MANAGER,ROLE_TA_LEADER,ROLE_EB, ROLE_TA_MEMBER, ROLE_ADMIN)
                .antMatchers(HttpMethod.PUT, "/candidate/**").hasAnyAuthority(ROLE_TA_MANAGER, ROLE_TA_LEADER, ROLE_TA_MEMBER, ROLE_ADMIN)
                .antMatchers(HttpMethod.DELETE, "/candidate/**").hasAnyAuthority(ROLE_TA_MANAGER, ROLE_ADMIN)
                .antMatchers(HttpMethod.PUT, "/updateCV").hasAnyAuthority(ROLE_TA_MANAGER,ROLE_TA_LEADER,ROLE_EB,ROLE_TA_MEMBER)
                .antMatchers(HttpMethod.PUT, "/candidate/**").hasAnyAuthority(ROLE_TA_MANAGER, ROLE_TA_LEADER, ROLE_TA_MEMBER)
                .antMatchers(HttpMethod.DELETE, "/candidate/**").hasAnyAuthority(ROLE_TA_MANAGER)
                .antMatchers(HttpMethod.GET, "/history/**").authenticated()
                .antMatchers("/comment/", "/comment").authenticated()
                .anyRequest().permitAll()
                .and().exceptionHandling().accessDeniedHandler(accessDeniedHandler())
                .and().addFilter(new JWTAuthorizationFilter(authenticationManager()));
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public AccessDeniedHandler accessDeniedHandler(){
        return new CustomAccessDeniedHandler();
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}