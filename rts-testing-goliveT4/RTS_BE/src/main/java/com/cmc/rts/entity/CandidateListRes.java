package com.cmc.rts.entity;


import lombok.Getter;
import lombok.Setter;

/**
 * Create by trungtx
 * Date: 11/14/2021
 * Time: 10:28 PM
 * Project: CMC_RTS
 */

@Getter
@Setter
public class CandidateListRes {

    private Long candidateId;
    private String fullName;
    private String email;
    private String roleName;
    private String status;
    private String createBy;
    private String sendDate;
    private String createdDate;
    private String updatedDate;
    private String linkRequest;
    private String accessToken;
    private String accessCode;
    private String phone;
    private int step;
    private String address;
    private String linkCv;
    private int channel;
    private String applySince;
    private String introducer;
    private String location;
    private String school;
    private String degree;
    private String skill;
    private String language;
    private String job;
    private String level;
    private String experience;
    private String sex;
    private String birthDay;
    private String stepRts;
    private String assignees;
    private int jobRtsId;
    private String removeJobRtsIds;
}
