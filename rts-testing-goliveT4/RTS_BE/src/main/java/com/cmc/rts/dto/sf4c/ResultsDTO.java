package com.cmc.rts.dto.sf4c;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ResultsDTO {
    @SerializedName("results")
    private List<Sf4cResponseDTO> results;
}
