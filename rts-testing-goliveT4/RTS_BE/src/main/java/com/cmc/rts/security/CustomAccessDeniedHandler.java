package com.cmc.rts.security;

import com.google.gson.JsonObject;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AccessDeniedException e) throws IOException, ServletException {
        httpServletResponse.setContentType("application/json;charset=UTF-8");
        httpServletResponse.setStatus(403);
        JsonObject dataRes = new JsonObject();
        dataRes.addProperty("timestamp", System.currentTimeMillis());
        dataRes.addProperty("status", 403);
        dataRes.addProperty("error", "Forbidden");
        dataRes.addProperty("exception", "Forbidden");
        dataRes.addProperty("message", "Don't have permission!");
        dataRes.addProperty("path", httpServletRequest.getRequestURL().toString());
        httpServletResponse.getWriter().write(dataRes.toString());
    }
}