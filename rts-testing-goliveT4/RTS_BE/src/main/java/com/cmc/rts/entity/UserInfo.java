package com.cmc.rts.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"code", "permissions", "roles", "systems", "images"})
public class UserInfo extends BaseDomain {
    @Column
    private String createdDateStr;
    @Column
    private String startDateStr;
    @Column
    private String endDateStr;
    @Column
    private String startWorkingDayStr;
    @Column
    private String updatedDateStr;
    @Column
    private String userId;
    @Column
    private Long userInfoId;
    @Column
    private String employeeId;
    @Column
    private String fullName;
    @Column
    private String firstName;
    @Column
    private String lastName;
    @Column
    private String userName;
    @Column
    private String email;
    @Column
    private String phone;
    @Column
    private String dateOfBirth;
    @Column
    private String externalInfo;
    @Column
    private String errorMessage;
    @ManyToOne
    @JoinColumn
    private Images images;
    @ManyToOne
    @JoinColumn
    private Systems systems;
    @ManyToOne
    @JoinColumn
    private Department department;
    @Column
    private String departmentName;
    @ManyToOne
    @JoinColumn
    private ManagerGroup managerGroup;

}
