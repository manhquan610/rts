package com.cmc.rts.repository;

import com.cmc.rts.entity.RequestSubmit;
import com.cmc.rts.repository.custom.RequestSubmitRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RequestSubmitRepository extends JpaRepository<RequestSubmit, Long>, RequestSubmitRepositoryCustom {
    @Query(value = "Select * from request_submit rs " +
            "left join request_request_submits rrs on rs.id = rrs.request_submits_id " +
            "where rrs.request_id = :requestId ", nativeQuery = true)
    List<RequestSubmit> findAllByRequestId(@Param(value = "requestId") Long requestId);
}
