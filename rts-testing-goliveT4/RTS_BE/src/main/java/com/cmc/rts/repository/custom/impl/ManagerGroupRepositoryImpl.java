package com.cmc.rts.repository.custom.impl;

import com.cmc.rts.entity.ManagerGroup;
import com.cmc.rts.repository.custom.ManagerGroupRepositoryCustom;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ManagerGroupRepositoryImpl implements ManagerGroupRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<ManagerGroup> findByName(String name, int offset, int limit) {
        try {
            StringBuilder sql = new StringBuilder("select * from manager_group m where deleted = 0");

            if (name != null && !name.isEmpty()) {
                sql.append(" and m.name like '" + name + "'");
            }
            if (!String.valueOf(limit).isEmpty() && !String.valueOf(offset).isEmpty()) {
                offset = offset > 0 ? --offset : offset;
                offset = offset * limit;
                sql.append(" limit  " + limit);
                sql.append(" offset  " + offset);
            }
            Query query = entityManager.createNativeQuery(sql.toString(), ManagerGroup.class);
            return query.getResultList();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    @Override
    public ManagerGroup findByGroupId(Long id) {
        ManagerGroup managerGroup = null;
        try {
            StringBuilder sql = new StringBuilder("select * from manager_group m where group_id = ");
            sql.append(id);
            sql.append(" limit 1 ");
            Query query = entityManager.createNativeQuery(sql.toString(), ManagerGroup.class);
            List<Object> object = query.getResultList();
            if (object.size() > 0) {
                managerGroup = (ManagerGroup) object.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return managerGroup;
        }

    }
}
