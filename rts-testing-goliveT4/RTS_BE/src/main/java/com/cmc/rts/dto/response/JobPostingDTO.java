package com.cmc.rts.dto.response;

import com.cmc.rts.entity.WorkingTypeEnum;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JobPostingDTO {
    private Long id;
    private String avatar;
    private String name;
    private String position;
    private String location;
    private String experience;
    private String salary;
    private Integer totalUser;
    private WorkingTypeEnum workingType;
    private String date;

    public JobPostingDTO(Long id, String name, String position, String location,
                         String experience, String salary, Integer totalUser, WorkingTypeEnum workingType, Date date) {
        this.id = id;
        this.avatar = "https://www.cmc.com.vn/main/imgs/logo.svg";
        this.name = name;
        this.position = position;
        this.location = location;
        this.experience = experience;
        this.salary = salary;
        this.totalUser = totalUser;
        this.workingType = workingType;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        if (date == null){
            this.date = null;
        } else {
            this.date = dateFormat.format(date);
        }
    }

}
