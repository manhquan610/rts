package com.cmc.rts.repository;


import com.cmc.rts.entity.ManagerJobLevel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ManagerJobLevelRepository extends JpaRepository<ManagerJobLevel, Long> {

    @Query(value = "SELECT m FROM ManagerJobLevel m order by m.id desc")
    Page<ManagerJobLevel> findAllJobLevels(Pageable pageable);

    @Query(value = "SELECT m FROM ManagerJobLevel m " +
            "where m.name like %:searchValue% order by m.id desc")
    Page<ManagerJobLevel> findAllBySearchParams(@Param(value = "searchValue") String searchValues, Pageable pageable);

    @Query(value = "SELECT m FROM ManagerJobLevel m " +
            "where m.jobType.id = :idJobType order by m.id desc")
    Page<ManagerJobLevel> findAllBySearchParams(@Param(value = "idJobType") Long idJobType, Pageable pageable);

    @Query(value = "SELECT m FROM ManagerJobLevel m " +
            "where m.name like %:searchValue% and m.jobType.id = :idJobType")
    List<ManagerJobLevel> checkDuplicateJobLevels(@Param(value = "searchValue") String searchValue,
                                                  @Param(value = "idJobType") Long idJobType);

    @Query(value = "SELECT m FROM ManagerJobLevel m " +
            "where m.id = :idJobType order by m.id desc")
    ManagerJobLevel findAllBySearchParams(@Param(value = "idJobType") Long idJobType);
}
