package com.cmc.rts.service;


import com.cmc.rts.entity.HistoryMoveLog;

import java.util.List;

public interface HistoryService {
    void createHistory(List<HistoryMoveLog> request);

    void updateHistory(HistoryMoveLog request);

    List<HistoryMoveLog> getHistoryByCandidateId(Long candidateId);
}
