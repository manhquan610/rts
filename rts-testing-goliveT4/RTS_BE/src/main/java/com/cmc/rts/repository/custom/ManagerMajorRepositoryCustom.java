package com.cmc.rts.repository.custom;

import com.cmc.rts.entity.ManagerMajor;

import java.util.List;

public interface ManagerMajorRepositoryCustom {
    List<ManagerMajor> findByNameSame(Integer first, Integer limit, String name);
    List<ManagerMajor> findByName(Integer first, Integer limit, List<String> name);
}

