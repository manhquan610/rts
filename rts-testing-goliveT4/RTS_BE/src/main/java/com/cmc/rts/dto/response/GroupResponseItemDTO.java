package com.cmc.rts.dto.response;

import com.cmc.rts.dto.response.GroupResponse;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GroupResponseItemDTO {
    @JsonProperty("id")
    private Long id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("administrative")
    private Boolean administrative;
    @JsonProperty("description")
    private String description;
    @JsonProperty("parentId")
    private Long parentId;
    @JsonProperty("parentName")
    private String parentDepartmentName;
    @JsonProperty("location")
    private String location;
}
