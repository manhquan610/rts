package com.cmc.rts.utils.DIConfig;

import com.cmc.rts.utils.Common;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SF4CInject {

    public Common common;

    @Autowired
    public SF4CInject(Common common) {
        this.common = common;
    }
}
