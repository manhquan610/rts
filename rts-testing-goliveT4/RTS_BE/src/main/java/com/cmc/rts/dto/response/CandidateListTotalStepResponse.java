package com.cmc.rts.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CandidateListTotalStepResponse {

    private String jobRts;
    private Long none = 0L;
    private Long qualify = 0L;
    private Long confirm = 0L;
    private Long interview = 0L;
    private Long offer = 0L;
    private Long onboard = 0L;
    private Long failed = 0L;
    private Long qualifying = 0L;
    private Long confirming = 0L;
    private Long interviewing = 0L;
    private Long offering = 0L;
    private Long onboarding = 0L;

    public CandidateListTotalStepResponse() {
    }
}
