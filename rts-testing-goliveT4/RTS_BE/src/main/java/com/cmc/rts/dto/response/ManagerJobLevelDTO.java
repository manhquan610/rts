package com.cmc.rts.dto.response;

import com.cmc.rts.entity.ManagerJobType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ManagerJobLevelDTO {
    private Long id;
    private ManagerJobType jobType;
    private String name;
    private int salaryMin;
    private int salaryMax;
    private String description;
}
