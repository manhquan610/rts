package com.cmc.rts.repository;

import com.cmc.rts.entity.Project;
import com.cmc.rts.repository.custom.ProjectRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long>, ProjectRepositoryCustom {

    @Modifying
    @Query("delete from Project p where p.id in ?1")
    void deleteUsersWithIds(List<Long> ids);
}
