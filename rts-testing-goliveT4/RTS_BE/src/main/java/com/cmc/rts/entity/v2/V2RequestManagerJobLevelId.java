package com.cmc.rts.entity.v2;


import lombok.Data;

import java.io.Serializable;


@Data
public class V2RequestManagerJobLevelId implements Serializable {
    private Long requestId;
    private Long managerJobLevelsId;
}
