package com.cmc.rts.dto.sf4c.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PositionListResponse {

    private String code;
    private String name;
    private String codeName;

}
