package com.cmc.rts.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name="manager_job_level")
public class ManagerJobLevel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    private ManagerJobType jobType;

    @Column(name = "name", length = 50)
    private String name;

    @Column(name = "salaryMin")
    private int salaryMin;

    @Column(name = "salaryMax")
    private int salaryMax;

    @Column(name = "description" , length = 250)
    private String description ;

    public ManagerJobLevel(Long id, String name, String description, Boolean deleted) {
        this.id = id;
        this.name = name;
        this.description = description;
    }
}
