package com.cmc.rts.rest;

import com.cmc.rts.dto.response.ReportResponse;
import com.cmc.rts.dto.response.ReportResponseDTO;
import com.cmc.rts.service.ReportService;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/report/")
public class ReportController {

//    @Autowired
    public ReportService reportTicketService;
    public ReportService reportTA;

    @Autowired
    public void ReportTicket(@Qualifier("TicketReport") ReportService reportTicket){
        this.reportTicketService = reportTicket;
    }

    @Autowired
    public void ReportTA(@Qualifier("TAReport") ReportService reportTA){
        this.reportTA = reportTA;
    }

    //Ticket
    @GetMapping(value = "/ticket")
    public ResponseEntity<?> reportTicket(@RequestParam(value = "projectIds", required = false) String projectIds,
                                          @RequestParam(value = "groupIds", required = false) String groupIds,
                                          @RequestParam(value = "departmentIds", required = false) String departmentIds,
                                          @RequestParam(value = "locationIds", required = false) String locationIds,
                                          @RequestParam(value = "startTime", required = false) Date startTime,
                                          @RequestParam(value = "endTime", required = false) Date endTime,
                                          @RequestParam(value = "offset") Integer offset,
                                          @RequestParam(value = "limit") Integer limit) throws ParseException {
        Pageable pageable = new PageRequest(offset -1, limit);
        List<ReportResponse> reportResponseList = reportTicketService.reportRTS(startTime, endTime, projectIds,
                null, groupIds, departmentIds, locationIds);
        Page<ReportResponse> reportResponses = reportTicketService.pageReport(reportResponseList, pageable);
        return new ResponseEntity<>(reportResponses, HttpStatus.OK);
    }

    //Ticket
    @GetMapping(value = "/ticket-v2")
    public ResponseEntity<?> reportTicket2(@RequestParam(value = "projectIds", required = false) String projectIds,
                                          @RequestParam(value = "groupIds", required = false) String groupIds,
                                          @RequestParam(value = "departmentIds", required = false) String departmentIds,
                                          @RequestParam(value = "locationIds", required = false) String locationIds,
                                          @RequestParam(value = "startTime", required = false) Date startTime,
                                          @RequestParam(value = "endTime", required = false) Date endTime,
                                          @RequestParam(value = "statuses", required = false) String statuses,
                                          @RequestParam(value = "priorityIds", required = false) String priorityIds,
                                          @RequestParam(value = "page") Integer offset,
                                          @RequestParam(value = "size") Integer limit) throws ParseException {
        Pageable pageable = new PageRequest(offset -1, limit);
        ReportResponseDTO reportRTS = reportTicketService.v2ReportRTS(startTime, endTime, projectIds,
                null, groupIds, departmentIds, locationIds, statuses, priorityIds, pageable);
        return new ResponseEntity<>(reportRTS, HttpStatus.OK);
    }

    @GetMapping("/ticket/export")
    public void downloadExcelFile(@RequestParam(value = "projectIds", required = false) String projectIds,
                                  @RequestParam(value = "groupIds", required = false) String groupIds,
                                  @RequestParam(value = "departmentIds", required = false) String departmentIds,
                                  @RequestParam(value = "locationIds", required = false) String locationIds,
                                  @RequestParam(value = "startTime", required = false) Date startTime,
                                  @RequestParam(value = "endTime", required = false) Date endTime,
            HttpServletResponse response) throws IOException {
        List<ReportResponse> reportResponseList = reportTicketService.reportRTS(startTime, endTime, projectIds,
                null, groupIds, departmentIds, locationIds);
//        List<ReportResponse> reportRespons =  reportTicketService.reportRTS();
        ByteArrayInputStream byteArrayInputStream = reportTicketService.exportExcel(reportResponseList);
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename=ReportTicket.xlsx");
        IOUtils.copy(byteArrayInputStream, response.getOutputStream());
    }


    //TA
    @GetMapping(value = "/ta")
    public ResponseEntity<?> reportTA(@RequestParam(value = "skills", required = false) String skills,
                                          @RequestParam(value = "groupIds", required = false) String groups,
                                          @RequestParam(value = "departmentIds", required = false) String departments,
                                          @RequestParam(value = "locationIds", required = false) String locations,
                                          @RequestParam(value = "startTime", required = false) Date startTime,
                                          @RequestParam(value = "endTime", required = false) Date endTime,
                                          @RequestParam(value = "offset") Integer offset,
                                          @RequestParam(value = "limit") Integer limit) throws ParseException {
        Pageable pageable = new PageRequest(offset -1, limit);
        return new ResponseEntity<>(reportTA.pageReport(startTime, endTime, null,
                skills, groups, departments, locations, pageable), HttpStatus.OK);
    }

    @GetMapping("/ta/export")
    public void downloadExcelFileTA(@RequestParam(value = "skills", required = false) String skills,
                                    @RequestParam(value = "groupIds", required = false) String groups,
                                    @RequestParam(value = "departmentIds", required = false) String departments,
                                    @RequestParam(value = "locationIds", required = false) String locations,
                                    @RequestParam(value = "startTime", required = false) Date startTime,
                                    @RequestParam(value = "endTime", required = false) Date endTime,
                                    HttpServletResponse response) throws IOException, ParseException {
        List<ReportResponse> reportResponse =  reportTA.reportRTS(startTime, endTime, null, skills, groups, departments, locations);
        ByteArrayInputStream byteArrayInputStream = reportTA.exportExcel(reportResponse);
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename=ReportTA.xlsx");
        IOUtils.copy(byteArrayInputStream, response.getOutputStream());
    }
}
