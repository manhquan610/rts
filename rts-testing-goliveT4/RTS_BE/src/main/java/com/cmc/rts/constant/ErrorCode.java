package com.cmc.rts.constant;

import com.cmc.rts.annotation.ErrorMessage;

/**
 * @author: nthieu10
 * 8/10/2022
 **/
public class ErrorCode {
    @ErrorMessage("Success")
    public static final String SUCCESS = "200";
}
