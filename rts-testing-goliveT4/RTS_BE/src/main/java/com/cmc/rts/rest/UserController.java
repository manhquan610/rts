package com.cmc.rts.rest;

import com.cmc.rts.dto.response.GroupResponse;
import com.cmc.rts.entity.DepartmentGroup;
import com.cmc.rts.entity.UserAuthen;
import com.cmc.rts.entity.UserDTO;
import com.cmc.rts.repository.UserInfoRepository;
import com.cmc.rts.response.BaseResponse;
import com.cmc.rts.security.AuthenSession;
import com.cmc.rts.service.impl.ManagerGroupServiceImpl;
import com.cmc.rts.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Date;

@RestController
public class UserController {

    @Autowired
    private Environment env;

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private ManagerGroupServiceImpl managerGroupService;

    @PostMapping(value = "/authen/login")
    public ResponseEntity<?> authenLoginLdap(@RequestBody UserAuthen userAuthen) throws Exception {
        try {
            String uriLoginLdap = env.getProperty("authen.url.login.ldap");
            String lDapKey = env.getProperty("authen.ldap.key");
            String lDapSecret = env.getProperty("authen.ldap.secret");

            userAuthen.setKey(lDapKey);
            userAuthen.setSecret(lDapSecret);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
            HttpEntity<String> requestEntity = new HttpEntity<String>(userAuthen.toString(), headers);

            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<UserDTO> response_dpDTO = restTemplate.exchange(uriLoginLdap, HttpMethod.POST, requestEntity, UserDTO.class);

            UserDTO userDTO = response_dpDTO.getBody();
            GroupResponse groupResponse = managerGroupService.allManagerGroups();
            if (userDTO.getGroups() != null) {
                userDTO.getGroups().stream().forEach(group -> {
                    GroupResponse tempGroup = groupResponse.getListChild().stream()
                            .filter(g -> group.getGroupParentName().equals(g.getName()))
                            .findAny()
                            .orElse(null);
                    if (tempGroup != null) {
                        group.setGroupId(tempGroup.getId());
                        if (userDTO.getDepartment() != null && tempGroup.getListChild() != null) {
                            GroupResponse temp = tempGroup.getListChild().stream()
                                    .filter(g -> userDTO.getDepartment().equals(g.getName()))
                                    .findAny()
                                    .orElse(null);
                            if(temp != null) {
                                DepartmentGroup departmentGroup = new DepartmentGroup(temp.getId(), temp.getName());
                                userDTO.setDepartmentGroup(departmentGroup);
                            }
                        }
                    }
                });
            }

            if (userDTO.getToken() != null) {
                userDTO.setTime4Hout(new Date());
                userDTO.setTime30POut(new Date());
                AuthenSession.authenSessionToken.put(userDTO.getToken(), userDTO);
                System.out.println("Size session: " + AuthenSession.authenSessionToken.size());
            }
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            userDTO.getToken(),
                            userAuthen.getUsername()
                    ));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new ResponseEntity<UserDTO>(userDTO, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("The user and password is incorrect");
        }
    }

    @PostMapping(value = "/authen/logout")
    public ResponseEntity<?> authenLogoutLdap(@RequestHeader String token) {
        try {
            String uriLogoutLdap = env.getProperty("authen.url.logout.ldap");

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
            HttpEntity<String> requestEntity = new HttpEntity<String>(token, headers);

            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> response_dpDTO = restTemplate.exchange(uriLogoutLdap, HttpMethod.POST, requestEntity, String.class);

            AuthenSession.authenSessionToken.remove(token);

            return new ResponseEntity<String>("OK", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<BaseResponse>(
                    new BaseResponse(Constants.RESPONSE.INTERNAL_SERVER_ERROR, Constants.RESPONSE.EXITS_CODE,
                            Constants.RESPONSE.NOT_EXIST),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
