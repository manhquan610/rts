package com.cmc.rts.dto.request;

import lombok.Data;

@Data
public class ReassignedDTO {
    private Long id;
    private String name;
    private Integer target;
}
