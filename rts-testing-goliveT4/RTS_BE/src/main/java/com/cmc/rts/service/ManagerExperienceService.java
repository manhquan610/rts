package com.cmc.rts.service;

import com.cmc.rts.dto.request.ListNameRequest;
import com.cmc.rts.dto.request.NameRequest;
import com.cmc.rts.dto.response.ExperienceDTO;
import com.cmc.rts.dto.response.MajorDTO;
import com.cmc.rts.entity.ManagerExperience;
import org.springframework.web.bind.annotation.PathVariable;

public interface ManagerExperienceService {
    ManagerExperience addManagerExperience(ManagerExperience managerExperience);
    ExperienceDTO findAll(NameRequest nameRequest);
    ExperienceDTO findAll(ListNameRequest listNameRequest);
    ManagerExperience findOne(Long id);
    void delete(Long id);
}
