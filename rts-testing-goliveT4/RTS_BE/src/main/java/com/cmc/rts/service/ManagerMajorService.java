package com.cmc.rts.service;

import com.cmc.rts.dto.request.ListNameRequest;
import com.cmc.rts.dto.request.NameRequest;
import com.cmc.rts.dto.response.MajorDTO;
import com.cmc.rts.entity.ManagerMajor;

import java.util.List;

public interface ManagerMajorService {
    ManagerMajor addManagerMajor(ManagerMajor managerMajor) ;
    MajorDTO findAll(NameRequest nameRequest);
    MajorDTO findAll(ListNameRequest listNameRequest);
    ManagerMajor findOne(Long id);
    void delete(Long id);
}
