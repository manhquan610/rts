package com.cmc.rts.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommentRequest {
    private String text;
    private Long id;
    private Long requestId;
    private Long candidateId;

    public CommentRequest() {
    }

    public CommentRequest(String text, Long requestId, Long candidateId) {
        this.text = text;
        this.requestId = requestId;
        this.candidateId = candidateId;
    }
}