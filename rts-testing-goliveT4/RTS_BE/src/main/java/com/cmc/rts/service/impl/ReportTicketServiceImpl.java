package com.cmc.rts.service.impl;

import com.cmc.rts.dto.response.*;
import com.cmc.rts.dto.response.custom.BasePage;
import com.cmc.rts.entity.*;
import com.cmc.rts.entity.v2.*;
import com.cmc.rts.repository.ManagerAreaRepository;
import com.cmc.rts.repository.ManagerJobTypeRepository;
import com.cmc.rts.repository.RequestRepository;
import com.cmc.rts.repository.v2.*;
import com.cmc.rts.service.CandidateService;
import com.cmc.rts.service.ManagerGroupService;
import com.cmc.rts.service.ReportService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ReportTicketServiceImpl implements ReportService {

    @Autowired
    private RequestRepository requestRepository;

    @Autowired
    private ManagerGroupService managerGroupService;

    @Autowired
    private CandidateService candidateService;

    @Autowired
    private V2RequestRepository v2RequestRepository;

    @Autowired
    private ManagerJobTypeRepository managerJobTypeRepository;

    @Autowired
    private ManagerAreaRepository managerAreaRepository;

    @Autowired
    private V2ManagerJobLevelRepository v2ManagerJobLevelRepository;

    @Autowired
    private V2RequestManagerJobLevelRepository v2RequestManagerJobLevelRepository;

    @Autowired
    private V2RequestReportTicketRepository v2RequestReportTicketRepository;

    @Autowired
    private V2RequestCandidateLogRepository v2RequestCandidateLogRepository;

    @Override
    public Page<ReportResponse> pageReport(Date startTime, Date endTime, String projectIds, String skill, String groups, String departments, String locations, Pageable pageable) throws ParseException {
        return null;
    }

    @Override
    public Page<ReportResponse> pageReport(List<ReportResponse> reportResponseList, Pageable pageable) {
        List<ReportResponse> pageList = reportResponseList.stream()
                .skip((long) pageable.getPageSize() * pageable.getPageNumber())
                .limit(pageable.getPageSize())
                .collect(Collectors.toList());
        return new PageImpl<>(pageList, pageable, reportResponseList.size());
    }

    @Override
    public ReportResponseDTO pageReport(ReportResponseDTO responseDTO, Pageable pageable) {
        List<ReportResponse> reportResponses = responseDTO.getReportResponses()
                .stream()
                .skip((long) pageable.getPageSize() * pageable.getPageNumber())
                .limit(pageable.getPageSize())
                .collect(Collectors.toList());
        Page page = new PageImpl<>(reportResponses, pageable, responseDTO.getReportResponses().size());
        responseDTO.setBasePage(BasePage.build(page));
        responseDTO.setReportResponses(reportResponses);
        return responseDTO;
    }

    @Override
    public List<ReportResponse> reportRTS() throws ParseException {
        return null;
    }

    private String getDUName(GroupResponse response, Long idGroup, Long idDU) {
        if (response == null) {
            return null;
        }
        for (GroupResponse responseGroup : response.getListChild()) {
            if (Objects.equals(responseGroup.getId(), idGroup)) {
                if (CollectionUtils.isNotEmpty(responseGroup.getListChild())) {
                    for (GroupResponse responseDU : responseGroup.getListChild()) {
                        if (Objects.equals(responseDU.getId(), idDU)) {
                            return responseDU.getName();
                        }
                    }
                }
            }
        }
        return null;
    }

    private String getGroupName(GroupResponse response, Long idGroup) {
        if (ObjectUtils.isEmpty(response) || ObjectUtils.isEmpty(response.getListChild())) {
            return null;
        }
        return response.getListChild().stream().filter(v -> v.getId().equals(idGroup)).map(GroupResponse::getName).findAny().orElse(null);
    }

    private String getProjectName(List<ProjectResponse> projectResponses, Long idProject) {
        if (ObjectUtils.isEmpty(projectResponses) || ObjectUtils.isEmpty(idProject)) {
            return null;
        }
        return projectResponses.stream().filter(x -> x.getProjectId().equals(idProject)).map(ProjectResponse::getProjectName).findAny().orElse(null);
    }

    @Override
    public ReportResponseDTO v2ReportRTS(Date startTime, Date endTime, String projects, String skill, String groups, String departments, String locations, String statuses, String priorities, Pageable pageable) {
        GroupResponse managerGroups = managerGroupService.allManagerGroups();
        List<RequestStatusEnum> statusList = StringUtils.isEmpty(statuses) ? null : Arrays.stream(statuses.split(",")).map(RequestStatusEnum::valueOf).collect(Collectors.toList());
        List<ProjectResponse> allProject = managerGroupService.getAllProject();
        List<Long> projectIds = extractIdFromString(projects);
        List<Long> groupIds = extractIdFromString(groups);
        List<Long> departmentIds = extractIdFromString(departments);
        List<Long> locationIds = extractIdFromString(locations);
        List<Long> priorityIds = extractIdFromString(priorities);
        List<V2Request> requestList;
        List<ReportResponse> reportResponses = new ArrayList<>();
        ReportResponseDTO responseDTO = new ReportResponseDTO();
        if (startTime != null && endTime != null) {
            requestList = v2RequestRepository.findAllSortedByDate(startTime, endTime, statusList, !CollectionUtils.isEmpty(statusList));
        } else {
            requestList = v2RequestRepository.findAllSorted(statusList, !CollectionUtils.isEmpty(statusList));
        }
        requestList = requestList.stream().filter(x -> {
                    Long locationId = x.getAreaId() == null ? null : x.getAreaId();
                    return checkUnitExist(groupIds, x.getManagerGroupId())
                            && checkUnitExist(departmentIds, x.getDepartmentId())
                            && checkUnitExist(projectIds, x.getProjectId())
                            && checkUnitExist(priorityIds, x.getManagerPriorityId())
                            && checkUnitExist(locationIds, locationId);
                }
        ).collect(Collectors.toList());

        List<Long> requestIds = requestList.stream().map(V2Request::getId).collect(Collectors.toList());

        TotalCandidateStepResponse totalCandidateStepResponse = candidateService.getTotalCandidateInStepByIdRequest(requestIds.stream().map(String::valueOf).collect(Collectors.toList()));
//        Map<Long, List<V2RequestCandidateLog>> requestCandidateLogMap = getRequestCandidateLog(requestIds);

//        List<Long> managerJobTypeIds = requestList.stream().map(V2Request::getJobTypeId).collect(Collectors.toList());
//        List<ManagerJobType> managerJobTypes = managerJobTypeRepository.findManagerJobTypeByParams(managerJobTypeIds, !ObjectUtils.isEmpty(managerJobTypeIds));
//        Map<Long, ManagerJobType> managerJobTypeMap = managerJobTypes.stream().collect(Collectors.toMap(ManagerJobType::getId, v -> v));

        List<Long> areaIds = requestList.stream().map(V2Request::getAreaId).collect(Collectors.toList());
        List<ManagerArea> managerAreas = managerAreaRepository.findManagerAreaByParams(areaIds, !ObjectUtils.isEmpty(areaIds));
        Map<Long, ManagerArea> managerAreaMap = managerAreas.stream().collect(Collectors.toMap(ManagerArea::getId, v -> v));

        List<V2RequestManagerJobLevels> requestManagerJobLevels = v2RequestManagerJobLevelRepository.findV2RequestManagerJobLevelsByRequestIdIn(requestIds);
        List<Long> managerJobLevelIds = requestManagerJobLevels.stream().map(V2RequestManagerJobLevels::getManagerJobLevelsId).distinct().collect(Collectors.toList());
        List<V2ManagerJobLevel> managerJobLevels = v2ManagerJobLevelRepository.findV2ManagerJobLevelByIdIn(managerJobLevelIds);
        Map<Long, V2ManagerJobLevel> v2ManagerJobLevelMap = managerJobLevels.stream().collect(Collectors.toMap(V2ManagerJobLevel::getId, v -> v));
        Map<Long, List<V2RequestManagerJobLevels>> v2RequestManagerJobLevelsMap = requestManagerJobLevels.stream().collect(Collectors.groupingBy(V2RequestManagerJobLevels::getManagerJobLevelsId, Collectors.toList()));

        Map<Long, List<V2Request>> requestMapByGroupId = requestList.stream().collect(Collectors.groupingBy(V2Request::getManagerGroupId, Collectors.toList()));

        for (Map.Entry<Long, List<V2Request>> entry : requestMapByGroupId.entrySet()) {
            ReportResponse reportGroupResponse = new ReportResponse();
            reportGroupResponse.setUnit(entry.getKey());
            reportGroupResponse.setUnitName(getGroupName(managerGroups, entry.getKey()));
            List<ReportResponse> reportChildren = new ArrayList<>();
            for (V2Request v2Request : entry.getValue()) {
                CandidateListTotalStepResponse candidateListTotalStepResponse = totalCandidateStepResponse.getCandidateList().stream().filter(x -> x.getJobRts().equals(String.valueOf(v2Request.getId()))).findAny().orElse(null);

                ReportResponse reportByDepartment = new ReportResponse();
                reportByDepartment.setUnit(entry.getKey());
                reportByDepartment.setUnitName(getGroupName(managerGroups, entry.getKey()));
                reportByDepartment.setProject(v2Request.getProjectId());
                reportByDepartment.setProjectName(getProjectName(allProject, v2Request.getProjectId()));
                reportByDepartment.setDepartment(v2Request.getDepartmentId());
                reportByDepartment.setDepartmentName(getDUName(managerGroups, v2Request.getManagerGroupId(), v2Request.getDepartmentId()));
                reportByDepartment.setJobId(v2Request.getId());
                reportByDepartment.setDeadline(v2Request.getDeadline());
                reportByDepartment.setTotalTarget((long) v2Request.getNumber());

                if (!ObjectUtils.isEmpty(candidateListTotalStepResponse)) {
                    reportByDepartment.setCurrentOffering(candidateListTotalStepResponse.getOffering());
                    reportByDepartment.setTotalOffering(candidateListTotalStepResponse.getOffer());

                    reportByDepartment.setCurrentInterview(candidateListTotalStepResponse.getInterviewing());
                    reportByDepartment.setTotalInterview(candidateListTotalStepResponse.getInterview());

                    reportByDepartment.setCurrentQualifying(candidateListTotalStepResponse.getConfirming());
                    reportByDepartment.setTotalQualify(candidateListTotalStepResponse.getConfirm());

                    reportByDepartment.setCurrentApplication(candidateListTotalStepResponse.getQualifying());
                    reportByDepartment.setTotalApplication(candidateListTotalStepResponse.getQualify());

                    reportByDepartment.setCurrentOnBoarding(candidateListTotalStepResponse.getOnboarding());
                    reportByDepartment.setTotalOnBoarding(candidateListTotalStepResponse.getOnboard());

                    reportByDepartment.setTotalRejected(candidateListTotalStepResponse.getFailed());

                    reportByDepartment.setTotalActual(candidateListTotalStepResponse.getOnboarding());
                }

                reportByDepartment.setTotalGap(reportByDepartment.getTotalTarget() - reportByDepartment.getTotalActual());
                if (reportByDepartment.getTotalGap() < 0L) {
                    reportByDepartment.setTotalGap(0L);
                }
//                if (!ObjectUtils.isEmpty(v2Request.getJobTypeId())) {
//                    ManagerJobType managerJobType = managerJobTypeMap.get(v2Request.getJobTypeId());
//                    if (!ObjectUtils.isEmpty(managerJobType)) {
//                        reportByDepartment.setJobType(managerJobType.getName());
//                    }
//                }

                reportByDepartment.setJobType(v2Request.getName());
                if (!ObjectUtils.isEmpty(v2Request.getAreaId())) {
                    ManagerArea managerArea = managerAreaMap.get(v2Request.getAreaId());
                    if (!ObjectUtils.isEmpty(managerArea)) {
                        reportByDepartment.setLocation(managerArea.getName());
                    }
                }
                List<V2RequestManagerJobLevels> v2RequestManagerJobLevels = v2RequestManagerJobLevelsMap.get(v2Request.getId());
                if (!ObjectUtils.isEmpty(v2RequestManagerJobLevels)) {
                    V2ManagerJobLevel v2ManagerJobLevel = v2ManagerJobLevelMap.get(v2RequestManagerJobLevels.get(0).getManagerJobLevelsId());
                    if (!ObjectUtils.isEmpty(v2ManagerJobLevel)) {
                        reportByDepartment.setJobLevel(v2ManagerJobLevel.getName());
                    }
                }
                reportChildren.add(reportByDepartment);
            }
            reportGroupResponse.setChildren(reportChildren);


            Long totalDepartmentActual = 0L;
            Long totalDepartmentTarget = 0L;

            Long totalDepartmentApplication = 0L;
            Long totalDepartmentCurrentApplication = 0L;

            Long totalDepartmentQualified = 0L;
            Long totalDepartmentCurrentQualifying = 0L;

            Long totalDepartmentOffering = 0L;
            Long totalDepartmentCurrentOffering = 0L;

            Long totalDepartmentInterview = 0L;
            Long totalDepartmentCurrentInterview = 0L;

            Long totalDepartmentOnBoarding = 0L;
            Long totalDepartmentCurrentOnBoarding = 0L;

            Long totalDepartmentRejected = 0L;

            Long totalDepartmentGap = 0L;

            for (ReportResponse reportResponse : reportGroupResponse.getChildren()) {
                totalDepartmentActual += reportResponse.getTotalActual();
                totalDepartmentTarget += reportResponse.getTotalTarget();

                totalDepartmentApplication += reportResponse.getTotalApplication();
                totalDepartmentCurrentApplication += reportResponse.getCurrentApplication();

                totalDepartmentQualified += reportResponse.getTotalQualify();
                totalDepartmentCurrentQualifying += reportResponse.getCurrentQualifying();

                totalDepartmentOffering += reportResponse.getTotalOffering();
                totalDepartmentCurrentOffering += reportResponse.getCurrentOffering();

                totalDepartmentInterview += reportResponse.getTotalInterview();
                totalDepartmentCurrentInterview += reportResponse.getCurrentInterview();

                totalDepartmentOnBoarding += reportResponse.getTotalOnBoarding();
                totalDepartmentCurrentOnBoarding += reportResponse.getCurrentOnBoarding();

                totalDepartmentRejected += reportResponse.getTotalRejected();
                totalDepartmentGap += reportResponse.getTotalGap();
            }
//            Long totalDepartmentActual = reportGroupResponse.getChildren().stream().reduce(0L, (v, k) -> v + k.getTotalActual(), Long::sum);
//            Long totalDepartmentTarget = reportGroupResponse.getChildren().stream().reduce(0L, (v, k) -> v + k.getTotalTarget(), Long::sum);
//
//            Long totalDepartmentApplication = reportGroupResponse.getChildren().stream().reduce(0L, (v, k) -> v + k.getTotalApplication(), Long::sum);
//            Long totalDepartmentCurrentApplication = reportGroupResponse.getChildren().stream().reduce(0L, (v, k) -> v + k.getCurrentApplication(), Long::sum);
//
//            Long totalDepartmentQualified = reportGroupResponse.getChildren().stream().reduce(0L, (v, k) -> v + k.getTotalQualify(), Long::sum);
//            Long totalDepartmentCurrentQualifying = reportGroupResponse.getChildren().stream().reduce(0L, (v, k) -> v + k.getCurrentQualifying(), Long::sum);
//
//            Long totalDepartmentOffering = reportGroupResponse.getChildren().stream().reduce(0L, (v, k) -> v + k.getTotalOffering(), Long::sum);
//            Long totalDepartmentCurrentOffering = reportGroupResponse.getChildren().stream().reduce(0L, (v, k) -> v + k.getCurrentOffering(), Long::sum);
//
//            Long totalDepartmentInterview = reportGroupResponse.getChildren().stream().reduce(0L, (v, k) -> v + k.getTotalInterview(), Long::sum);
//            Long totalDepartmentCurrentInterview = reportGroupResponse.getChildren().stream().reduce(0L, (v, k) -> v + k.getCurrentInterview(), Long::sum);
//
//            Long totalDepartmentOnBoarding = reportGroupResponse.getChildren().stream().reduce(0L, (v, k) -> v + k.getTotalOnBoarding(), Long::sum);
//            Long totalDepartmentCurrentOnBoarding = reportGroupResponse.getChildren().stream().reduce(0L, (v, k) -> v + k.getCurrentOnBoarding(), Long::sum);
//
//            Long totalDepartmentRejected = reportGroupResponse.getChildren().stream().reduce(0L, (v, k) -> v + k.getTotalRejected(), Long::sum);
//
//            Long totalDepartmentGap = reportGroupResponse.getChildren().stream().reduce(0L, (v, k) -> v + k.getTotalGap(), Long::sum);


            reportGroupResponse.setTotalApplication(totalDepartmentApplication);
            reportGroupResponse.setCurrentApplication(totalDepartmentCurrentApplication);

            reportGroupResponse.setTotalQualify(totalDepartmentQualified);
            reportGroupResponse.setCurrentQualifying(totalDepartmentCurrentQualifying);

            reportGroupResponse.setTotalInterview(totalDepartmentInterview);
            reportGroupResponse.setCurrentInterview(totalDepartmentCurrentInterview);

            reportGroupResponse.setTotalOffering(totalDepartmentOffering);
            reportGroupResponse.setCurrentOffering(totalDepartmentCurrentOffering);

            reportGroupResponse.setTotalOnBoarding(totalDepartmentOnBoarding);
            reportGroupResponse.setCurrentOnBoarding(totalDepartmentCurrentOnBoarding);

            reportGroupResponse.setTotalRejected(totalDepartmentRejected);

            reportGroupResponse.setTotalActual(totalDepartmentActual);
            reportGroupResponse.setTotalTarget(totalDepartmentTarget);
            reportGroupResponse.setTotalGap(totalDepartmentGap);

            reportResponses.add(reportGroupResponse);
        }
        responseDTO.setReportResponses(reportResponses);
//        Long totalTargetResponse = reportResponses.stream().reduce(0L, (v, k) -> v + k.getTotalTarget(), Long::sum); //target
//        Long totalActualResponse = reportResponses.stream().reduce(0L, (v, k) -> v + k.getTotalActual(), Long::sum); //actual
//        Long totalGapResponse = reportResponses.stream().reduce(0L, (v, k) -> v + k.getTotalGap(), Long::sum); //gap
//
//        Long totalCurrentApplicationResponse = reportResponses.stream().reduce(0L, (v, k) -> v + k.getCurrentApplication(), Long::sum); //application
//        Long totalApplicationResponse = reportResponses.stream().reduce(0L, (v, k) -> v + k.getTotalApplication(), Long::sum); //application
//
//        Long totalCurrentQualifiedResponse = reportResponses.stream().reduce(0L, (v, k) -> v + k.getCurrentQualifying(), Long::sum); //qualify
//        Long totalQualifiedResponse = reportResponses.stream().reduce(0L, (v, k) -> v + k.getTotalQualify(), Long::sum); //qualify
//
//        Long totalCurrentOfferingResponse = reportResponses.stream().reduce(0L, (v, k) -> v + k.getCurrentOffering(), Long::sum); //offering
//        Long totalOfferingResponse = reportResponses.stream().reduce(0L, (v, k) -> v + k.getTotalOffering(), Long::sum); //offering
//
//        Long totalCurrentOnboardResponse = reportResponses.stream().reduce(0L, (v, k) -> v + k.getCurrentOnBoarding(), Long::sum); //onboard
//        Long totalOnboardResponse = reportResponses.stream().reduce(0L, (v, k) -> v + k.getTotalOnBoarding(), Long::sum); //onboard
//
//        Long totalCurrentInterviewResponse = reportResponses.stream().reduce(0L, (v, k) -> v + k.getCurrentInterview(), Long::sum); //interview
//        Long totalInterviewResponse = reportResponses.stream().reduce(0L, (v, k) -> v + k.getTotalInterview(), Long::sum); //interview
//
//        Long totalRejected = reportResponses.stream().reduce(0L, (v, k) -> v + k.getTotalRejected(), Long::sum); //interview

        Long totalTargetResponse = 0L; //target
        Long totalActualResponse = 0L; //actual
        Long totalGapResponse = 0L; //gap

        Long totalCurrentApplicationResponse = 0L; //application
        Long totalApplicationResponse = 0L; //application

        Long totalCurrentQualifiedResponse = 0L; //qualify
        Long totalQualifiedResponse = 0L; //qualify

        Long totalCurrentOfferingResponse = 0L; //offering
        Long totalOfferingResponse = 0L; //offering

        Long totalCurrentOnboardResponse = 0L; //onboard
        Long totalOnboardResponse = 0L; //onboard

        Long totalCurrentInterviewResponse = 0L; //interview
        Long totalInterviewResponse = 0L; //interview

        Long totalRejected = 0L; //interview

        for (ReportResponse reportResponse : responseDTO.getReportResponses()) {

            totalTargetResponse += reportResponse.getTotalTarget();
            totalActualResponse += reportResponse.getTotalActual();
            totalGapResponse += reportResponse.getTotalGap();

            totalCurrentApplicationResponse += reportResponse.getCurrentApplication();
            totalApplicationResponse += reportResponse.getTotalApplication();

            totalCurrentQualifiedResponse += reportResponse.getCurrentQualifying();
            totalQualifiedResponse += reportResponse.getTotalQualify();

            totalCurrentOfferingResponse += reportResponse.getCurrentOffering();
            totalOfferingResponse += reportResponse.getTotalOffering();

            totalCurrentInterviewResponse += reportResponse.getCurrentInterview();
            totalInterviewResponse += reportResponse.getTotalInterview();

            totalCurrentOnboardResponse += reportResponse.getCurrentOnBoarding();
            totalOnboardResponse += reportResponse.getTotalOnBoarding();

            totalRejected += reportResponse.getTotalRejected();
        }

        responseDTO.setTotalTarget(totalTargetResponse);
        responseDTO.setTotalActual(totalActualResponse);
        responseDTO.setTotalGap(totalGapResponse);

        responseDTO.setTotalCurrentApplicant(totalCurrentApplicationResponse);
        responseDTO.setTotalApplicant(totalApplicationResponse);

        responseDTO.setTotalCurrentQualified(totalCurrentQualifiedResponse);
        responseDTO.setTotalQualified(totalQualifiedResponse);

        responseDTO.setTotalCurrentOffering(totalCurrentOfferingResponse);
        responseDTO.setTotalOffering(totalOfferingResponse);

        responseDTO.setTotalCurrentInterview(totalCurrentInterviewResponse);
        responseDTO.setTotalInterview(totalInterviewResponse);

        responseDTO.setTotalCurrentOnBoarding(totalCurrentOnboardResponse);
        responseDTO.setTotalOnBoarding(totalOnboardResponse);

        responseDTO.setTotalRejected(totalRejected);

        return pageReport(responseDTO, pageable);
    }

    private Map<Long, List<V2RequestCandidateLog>> getRequestCandidateLog(List<Long> requestIds) {
        List<V2RequestCandidateLog> v2RequestCandidateLogs = v2RequestCandidateLogRepository.findV2RequestCandidateLogByRequestIdIn(requestIds);
        return v2RequestCandidateLogs.stream().collect(Collectors.groupingBy(V2RequestCandidateLog::getRequestId, Collectors.toList()));
    }


    @Override
    public List<ReportResponse> reportRTS(Date startTime, Date endTime, String projects, String skill, String groups, String departments, String locations) {

        long st = System.currentTimeMillis();
        GroupResponse groupResponse = managerGroupService.allManagerGroups();
        long et = System.currentTimeMillis();
        long timeElapsed = et - st;
        System.out.println("Execution time in milliseconds group api 1: " + timeElapsed);

        long st2 = System.currentTimeMillis();
        List<ProjectResponse> projectList = managerGroupService.getAllProject();
        long et2 = System.currentTimeMillis();
        long timeElapsed2 = et2 - st2;
        System.out.println("Execution time in milliseconds project api 1: " + timeElapsed2);

        List<Long> projectIds = extractIdFromString(projects);
        List<Long> groupIds = extractIdFromString(groups);
        List<Long> departmentIds = extractIdFromString(departments);
        List<Long> locationIds = extractIdFromString(locations);
        //Get Request List & Filter by projectIds, groupIds, departmentIds, locationIds
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        List<Request> requestList;
        if (startTime != null && endTime != null) {
            requestList = requestRepository.findAllSortedByDate(simpleDateFormat.format(startTime), simpleDateFormat.format(endTime));
        } else {
            requestList = requestRepository.findAllSorted();
        }
        requestList = requestList.stream().filter(x -> {
                    Long locationId = x.getArea() == null ? null : x.getArea().getId();
                    return checkUnitExist(groupIds, x.getManagerGroupId())
                            && checkUnitExist(departmentIds, x.getDepartmentId())
                            && checkUnitExist(projectIds, x.getProjectId())
                            && checkUnitExist(locationIds, locationId);
                }
        ).collect(Collectors.toList());

        List<String> requestIds = requestList.stream().map(x -> x.getId().toString()).collect(Collectors.toList());

        log.info("requestIds {}", requestIds);

        long st3 = System.currentTimeMillis();
        TotalCandidateStepResponse totalCandidateStepResponse = candidateService.getTotalCandidateInStepByIdRequest(requestIds);
        long et3 = System.currentTimeMillis();
        long timeElapsed3 = st3 - et3;
        System.out.println("Execution time in milliseconds total candidate step response api 1: " + timeElapsed3);


        List<ReportResponse> reportResponses = new ArrayList<>();
        Long groupId = null;
        int requestListSize = requestList.size();
        for (int i = 0; i < requestList.size(); i++) {
            if (requestList.get(i).getManagerGroupId().equals(groupId)) {
                continue;
            }
            ReportResponse reportResponseByGroup = new ReportResponse();
            reportResponseByGroup.setUnitName(getGroupName(groupResponse, requestList.get(i).getManagerGroupId()));
            reportResponseByGroup.setUnit(requestList.get(i).getManagerGroupId());
            groupId = requestList.get(i).getManagerGroupId();
            long totalActual = 0L;
            long totalTarget = 0L;
            long totalShort;
            long totalGroupApplication = 0L;
            long totalGroupQualified = 0L;
            long totalGroupInterview = 0L;
            long totalGroupOffering = 0L;
            long totalGroupOnBoarding = 0L;
            List<ReportResponse> reportResponseListByDepartment = new ArrayList<>();
            for (int j = i; j < requestListSize; j++) {
                if (!requestList.get(j).getManagerGroupId().equals(groupId)) {
                    break;
                }
                Request request = requestList.get(j);
                ReportResponse reportResponseByDepartment = new ReportResponse();
                reportResponseByDepartment.setDepartment(request.getManagerGroupId());
                if (request.getManagerGroupId() != null && request.getDepartmentId() != null) {
                    reportResponseByDepartment.setUnitName(getDUName(groupResponse, request.getManagerGroupId(), request.getDepartmentId()));
                } else {
                    reportResponseByDepartment.setUnitName("");
                }
                reportResponseByDepartment.setProjectName(getProjectName(projectList, request.getProjectId()));
                reportResponseByDepartment.setLocation(request.getArea() == null ? "" : request.getArea().getName());
                reportResponseByDepartment.setTotalTarget((long) request.getNumber());
                Long actual = totalCandidateStepResponse.getCandidateList().stream().filter(x -> x.getJobRts().equals(request.getId().toString())).map(CandidateListTotalStepResponse::getOnboard).findFirst().orElse(0L);

                reportResponseByDepartment.setTotalActual(actual);
                reportResponseByDepartment.setTotalGap(request.getNumber() - totalCandidateStepResponse.getCandidateList().stream().filter(x -> x.getJobRts().equals(request.getId().toString())).map(CandidateListTotalStepResponse::getOnboard).findFirst().orElse(0L));
                reportResponseByDepartment.setJobId(request.getId());
//                reportResponseByDepartment.setJobType(request.getJobType().getName());
                reportResponseByDepartment.setJobType(request.getName());
                if (!request.getManagerJobLevels().isEmpty()) {
                    reportResponseByDepartment.setJobLevel(request.getManagerJobLevels().stream().findFirst().orElse(new ManagerJobLevel()).getName());
                } else {
                    reportResponseByDepartment.setJobLevel("");
                }


                CandidateListTotalStepResponse candidateListTotalStepResponse = totalCandidateStepResponse.getCandidateList().stream().filter(x -> x.getJobRts().equals(request.getId().toString())).findFirst().orElse(null);
                if (candidateListTotalStepResponse != null) {
                    reportResponseByDepartment.setTotalApplication(candidateListTotalStepResponse.getQualify());
                    reportResponseByDepartment.setTotalQualify(candidateListTotalStepResponse.getConfirm());
                    reportResponseByDepartment.setTotalInterview(candidateListTotalStepResponse.getInterview());
                    reportResponseByDepartment.setTotalOffering(candidateListTotalStepResponse.getOffer());
                    reportResponseByDepartment.setTotalOnBoarding(candidateListTotalStepResponse.getOnboard());
                    reportResponseByDepartment.setDeadline(request.getDeadline());
                }

                reportResponseListByDepartment.add(reportResponseByDepartment);
            }
            reportResponseByGroup.setChildren(reportResponseListByDepartment);
            totalActual = reportResponseByGroup.getChildren().stream().reduce(0L, (v, k) -> v + k.getTotalActual(), Long::sum);
            totalTarget = reportResponseByGroup.getChildren().stream().reduce(0L, (v, k) -> v + k.getTotalTarget(), Long::sum);
            totalGroupApplication = reportResponseByGroup.getChildren().stream().reduce(0L, (v, k) -> v + k.getTotalApplication(), Long::sum);
            totalGroupQualified = reportResponseByGroup.getChildren().stream().reduce(0L, (v, k) -> v + k.getTotalQualify(), Long::sum);
            totalGroupOffering = reportResponseByGroup.getChildren().stream().reduce(0L, (v, k) -> v + k.getTotalOffering(), Long::sum);
            totalGroupOnBoarding = reportResponseByGroup.getChildren().stream().reduce(0L, (v, k) -> v + k.getTotalOnBoarding(), Long::sum);
            totalGroupInterview = reportResponseByGroup.getChildren().stream().reduce(0L, (v, k) -> v + k.getTotalInterview(), Long::sum);

            reportResponseByGroup.setTotalApplication(totalGroupApplication);
            reportResponseByGroup.setTotalQualify(totalGroupQualified);
            reportResponseByGroup.setTotalInterview(totalGroupInterview);
            reportResponseByGroup.setTotalOffering(totalGroupOffering);
            reportResponseByGroup.setTotalOnBoarding(totalGroupOnBoarding);

            totalShort = totalTarget - totalActual;
            reportResponseByGroup.setTotalActual(totalActual);
            reportResponseByGroup.setTotalTarget(totalTarget);
            reportResponseByGroup.setTotalGap(totalShort);

            reportResponses.add(reportResponseByGroup);
        }

        return reportResponses;
    }

    private List<Long> extractIdFromString(String idString) {
        return StringUtils.isEmpty(idString) ? null : Arrays.stream(idString.split(","))
                .map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
    }

    public boolean checkUnitExist(List<Long> target, Long id) {
        if (target == null) {
            return true;
        }
        if (id == null) {
            return false;
        }
        return target.stream().anyMatch(x -> x.equals(id));
    }

    @Override
    public ByteArrayInputStream exportExcel(List<ReportResponse> reportTicketList) {
        try (Workbook workbook = new XSSFWorkbook()) {
            Sheet sheet = workbook.createSheet("TicketReport");
            sheet.addMergedRegion(CellRangeAddress.valueOf("A1:A2"));
            sheet.addMergedRegion(CellRangeAddress.valueOf("B1:B2"));
            sheet.addMergedRegion(CellRangeAddress.valueOf("C1:C2"));
            sheet.addMergedRegion(CellRangeAddress.valueOf("D1:D2"));
            sheet.addMergedRegion(CellRangeAddress.valueOf("E1:E2"));
            sheet.addMergedRegion(CellRangeAddress.valueOf("F1:F2"));
            sheet.addMergedRegion(CellRangeAddress.valueOf("G1:G2"));
//            sheet.addMergedRegion(CellRangeAddress.valueOf("H1:H2"));

            sheet.addMergedRegion(CellRangeAddress.valueOf("H1:J1"));
            sheet.addMergedRegion(CellRangeAddress.valueOf("K1:P1"));

            Row row = sheet.createRow(0);
            Row row1 = sheet.createRow(1);

            // Define header cell style
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
            headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
            headerCellStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);
//            headerCellStyle.set
            headerCellStyle.setBorderBottom(BorderStyle.THIN);
            headerCellStyle.setBorderLeft(BorderStyle.THIN);
            headerCellStyle.setBorderRight(BorderStyle.THIN);
            headerCellStyle.setBorderTop(BorderStyle.THIN);

            // Creating header cells
            Cell cell = row.createCell(0);
            cell.setCellValue("Unit");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(0);
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(1);
            cell.setCellValue("Project");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(1);
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(2);
            cell.setCellValue("Location");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(2);
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(3);
            cell.setCellValue("Deadline");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(3);
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(4);
            cell.setCellValue("Target");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(4);
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(5);
            cell.setCellValue("Actual");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(5);
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(6);
            cell.setCellValue("Short");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(6);
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(7);
            cell.setCellValue("Status");
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(8);
            cell.setCellValue("Status");
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(9);
            cell.setCellValue("Status");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(7);
            cell.setCellValue("High");
            cell.setCellStyle(headerCellStyle);


            cell = row1.createCell(8);
            cell.setCellValue("Medium");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(9);
            cell.setCellValue("Low");
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(10);
            cell.setCellValue("Total");
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(11);
            cell.setCellValue("Total");
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(12);
            cell.setCellValue("Total");
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(13);
            cell.setCellValue("Total");
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(14);
            cell.setCellValue("Total");
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(15);
            cell.setCellValue("Total");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(10);
            cell.setCellValue("Application");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(11);
            cell.setCellValue("Qualified");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(12);
            cell.setCellValue("Interview");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(13);
            cell.setCellValue("Review Offering");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(14);
            cell.setCellValue("Offering");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(15);
            cell.setCellValue("Onboarding");
            cell.setCellStyle(headerCellStyle);

            // Creating data rows for each ticket
            CellStyle dataRowCellStyle = workbook.createCellStyle();
            dataRowCellStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
            dataRowCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            dataRowCellStyle.setAlignment(HorizontalAlignment.CENTER);
            dataRowCellStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            int rowSetData = 0;
            for (int i = 0; i < reportTicketList.size(); i++) {
                Row dataRow = sheet.createRow(i + 2 + rowSetData);
                Cell dataRowCell = dataRow.createCell(0);
                dataRowCell.setCellValue(reportTicketList.get(i).getUnitName());
                dataRowCell.setCellStyle(dataRowCellStyle);

//                dataRow.createCell(1).setCellValue(reportTicketList.get(i).getProject() == null ? 0 : reportTicketList.get(i).getProject());
                Cell dataRowCell1 = dataRow.createCell(1);
                dataRowCell1.setCellValue("");
                dataRowCell1.setCellStyle(dataRowCellStyle);

//                dataRow.createCell(2).setCellValue(reportTicketList.get(i).getLocation() == null ? "" : reportTicketList.get(i).getLocation());
                Cell dataRowCell2 = dataRow.createCell(2);
                dataRowCell2.setCellValue(reportTicketList.get(i).getLocation() == null ? "" : reportTicketList.get(i).getLocation());
                dataRowCell2.setCellStyle(dataRowCellStyle);

                Date date = reportTicketList.get(i).getDeadline();
                Cell dataRowCell3 = dataRow.createCell(3);
                try {
//                    dataRow.createCell(3).setCellValue(dateFormat.format(date));
                    dataRowCell3.setCellValue(dateFormat.format(date));
                    dataRowCell3.setCellStyle(dataRowCellStyle);
                } catch (Exception e) {
//                    dataRow.createCell(3).setCellValue("");
                    dataRowCell3.setCellValue("");
                    dataRowCell3.setCellStyle(dataRowCellStyle);
                }

//                dataRow.createCell(4).setCellValue(reportTicketList.get(i).getTarget());
                Cell dataRowCell4 = dataRow.createCell(4);
                dataRowCell4.setCellValue(reportTicketList.get(i).getTotalTarget());
                dataRowCell4.setCellStyle(dataRowCellStyle);

//                dataRow.createCell(5).setCellValue(reportTicketList.get(i).getActual());
                Cell dataRowCell5 = dataRow.createCell(5);
                dataRowCell5.setCellValue(reportTicketList.get(i).getTotalActual());
                dataRowCell5.setCellStyle(dataRowCellStyle);

//                dataRow.createCell(6).setCellValue(reportTicketList.get(i).getShortCount());
                Cell dataRowCell6 = dataRow.createCell(6);
                dataRowCell6.setCellValue(reportTicketList.get(i).getTotalGap());
                dataRowCell6.setCellStyle(dataRowCellStyle);

////                dataRow.createCell(7).setCellValue(reportTicketList.get(i).getStatusHigh());
//                Cell dataRowCell7 = dataRow.createCell(7);
//                dataRowCell7.setCellValue(reportTicketList.get(i).getStatusHigh());
//                dataRowCell7.setCellStyle(dataRowCellStyle);
//
////                dataRow.createCell(8).setCellValue(reportTicketList.get(i).getStatusMedium());
//                Cell dataRowCell8 = dataRow.createCell(8);
//                dataRowCell8.setCellValue(reportTicketList.get(i).getStatusMedium());
//                dataRowCell8.setCellStyle(dataRowCellStyle);
//
////                dataRow.createCell(9).setCellValue(reportTicketList.get(i).getStatusLow());
//                Cell dataRowCell9 = dataRow.createCell(9);
//                dataRowCell9.setCellValue(reportTicketList.get(i).getStatusLow());
//                dataRowCell9.setCellStyle(dataRowCellStyle);

//                dataRow.createCell(10).setCellValue(reportTicketList.get(i).getTotalApplication());
                Cell dataRowCell10 = dataRow.createCell(10);
                dataRowCell10.setCellValue(reportTicketList.get(i).getTotalApplication());
                dataRowCell10.setCellStyle(dataRowCellStyle);

//                dataRow.createCell(11).setCellValue(reportTicketList.get(i).getTotalQualify());
                Cell dataRowCell11 = dataRow.createCell(11);
                dataRowCell11.setCellValue(reportTicketList.get(i).getTotalQualify());
                dataRowCell11.setCellStyle(dataRowCellStyle);

//                dataRow.createCell(12).setCellValue(reportTicketList.get(i).getTotalInterview());
                Cell dataRowCell12 = dataRow.createCell(12);
                dataRowCell12.setCellValue(reportTicketList.get(i).getTotalInterview());
                dataRowCell12.setCellStyle(dataRowCellStyle);

//                dataRow.createCell(13).setCellValue(reportTicketList.get(i).getTotalReviewOffering());
//                Cell dataRowCell13 = dataRow.createCell(13);
//                dataRowCell13.setCellValue(reportTicketList.get(i).getTotalReviewOffering());
//                dataRowCell13.setCellStyle(dataRowCellStyle);

//                dataRow.createCell(14).setCellValue(reportTicketList.get(i).getTotalOffering());
                Cell dataRowCell14 = dataRow.createCell(14);
                dataRowCell14.setCellValue(reportTicketList.get(i).getTotalOffering());
                dataRowCell14.setCellStyle(dataRowCellStyle);

//                dataRow.createCell(15).setCellValue(reportTicketList.get(i).getTotalOnboarding());
                Cell dataRowCell15 = dataRow.createCell(15);
                dataRowCell15.setCellValue(reportTicketList.get(i).getTotalOnBoarding());
                dataRowCell15.setCellStyle(dataRowCellStyle);

                String checkDuName = "";
                for (int j = 0; j < reportTicketList.get(i).getChildren().size(); j++) {
                    Row dataRow1 = sheet.createRow(dataRow.getRowNum() + j + 1);
                    if (checkDuName.equals(reportTicketList.get(i).getChildren().get(j).getDepartmentName())) {
                        dataRow1.createCell(0).setCellValue("");
                    } else {
                        dataRow1.createCell(0).setCellValue(reportTicketList.get(i).getChildren().get(j).getDepartmentName());
                    }
                    checkDuName = reportTicketList.get(i).getChildren().get(j).getDepartmentName();
//                    dataRow1.createCell(0).setCellValue(reportTicketList.get(i).getChildren().get(j).getDepartmentName());
                    dataRow1.createCell(1).setCellValue(reportTicketList.get(i).getChildren().get(j).getProjectName());
                    dataRow1.createCell(2).setCellValue(reportTicketList.get(i).getChildren().get(j).getLocation());
                    Date date1 = reportTicketList.get(i).getChildren().get(j).getDeadline();
                    try {
                        dataRow1.createCell(3).setCellValue(dateFormat.format(date1));
                    } catch (Exception e) {
                        dataRow1.createCell(3).setCellValue("");
                    }
                    dataRow1.createCell(4).setCellValue(reportTicketList.get(i).getChildren().get(j).getTotalTarget());
                    dataRow1.createCell(5).setCellValue(reportTicketList.get(i).getChildren().get(j).getTotalActual());
                    dataRow1.createCell(6).setCellValue(reportTicketList.get(i).getChildren().get(j).getTotalGap());
//                    dataRow1.createCell(7).setCellValue(reportTicketList.get(i).getChildren().get(j).getStatusHigh());
//                    dataRow1.createCell(8).setCellValue(reportTicketList.get(i).getChildren().get(j).getStatusMedium());
//                    dataRow1.createCell(9).setCellValue(reportTicketList.get(i).getChildren().get(j).getStatusLow());
                    dataRow1.createCell(10).setCellValue(reportTicketList.get(i).getChildren().get(j).getTotalApplication());
                    dataRow1.createCell(11).setCellValue(reportTicketList.get(i).getChildren().get(j).getTotalQualify());
                    dataRow1.createCell(12).setCellValue(reportTicketList.get(i).getChildren().get(j).getTotalInterview());
//                    dataRow1.createCell(13).setCellValue(reportTicketList.get(i).getChildren().get(j).getTotalReviewOffering());
                    dataRow1.createCell(14).setCellValue(reportTicketList.get(i).getChildren().get(j).getTotalOffering());
                    dataRow1.createCell(15).setCellValue(reportTicketList.get(i).getChildren().get(j).getTotalOnBoarding());
                }
                rowSetData += reportTicketList.get(i).getChildren().size();
            }

            // Making size of column auto resize to fit with data
            sheet.setColumnWidth(0, 25 * 200);
            sheet.setColumnWidth(1, 25 * 150);
            sheet.setColumnWidth(2, 25 * 150);
            sheet.setColumnWidth(3, 25 * 150);
            sheet.setColumnWidth(4, 25 * 100);
            sheet.setColumnWidth(5, 25 * 100);
            sheet.setColumnWidth(6, 25 * 100);
            sheet.setColumnWidth(7, 25 * 100);
            sheet.setColumnWidth(8, 25 * 100);
            sheet.setColumnWidth(9, 25 * 100);
            sheet.setColumnWidth(10, 25 * 200);
            sheet.setColumnWidth(11, 25 * 200);
            sheet.setColumnWidth(12, 25 * 200);
            sheet.setColumnWidth(13, 25 * 200);
            sheet.setColumnWidth(14, 25 * 200);
            sheet.setColumnWidth(15, 25 * 200);


            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            return new ByteArrayInputStream(outputStream.toByteArray());
        } catch (IOException ex) {
            return null;
        }
    }

    public void changeCellBackgroundColorWithPattern(Cell cell) {
//        CellStyle cellStyle = cell.getCellStyle();
//        if(cellStyle == null) {
//            cellStyle = cell.getSheet().getWorkbook().createCellStyle();
//        }
//        cellStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
//        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
//        cell.setCellStyle(cellStyle);


        CellStyle dataRowCellStyle = cell.getCellStyle();
        dataRowCellStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
        dataRowCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        dataRowCellStyle.setAlignment(HorizontalAlignment.CENTER);
        dataRowCellStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    }

}
