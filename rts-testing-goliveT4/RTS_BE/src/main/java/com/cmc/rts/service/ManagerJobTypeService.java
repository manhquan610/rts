package com.cmc.rts.service;


import com.cmc.rts.dto.request.JobTypeRequest;
import com.cmc.rts.dto.response.JobCategoriesDTO;
import com.cmc.rts.dto.response.JobPostingDTO;
import com.cmc.rts.dto.response.JobPostingDetailDTO;
import com.cmc.rts.entity.ManagerArea;
import com.cmc.rts.entity.ManagerJobType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

public interface ManagerJobTypeService {
    Page<ManagerJobType> findByName(String searchValues, Pageable pageable);
    Page<ManagerJobType> findByListName(String name, Pageable pageable);
    Page<ManagerJobType> getAllJobType(Pageable pageable);
    ManagerJobType getOne(Long id);
    ManagerJobType createJobTpye(JobTypeRequest request) throws Exception;

    ManagerJobType updateJobType(JobTypeRequest request) throws Exception;

    void deleteJobTypes(Long id);

    Page<JobCategoriesDTO> getJobCategories(Pageable pageable);

    Page<JobPostingDTO> getJobPosting(Pageable pageable, String skills, Long area, Long jobType, String deadLine,
                                      Long experience, Integer startSalary, Integer endSalary) throws ParseException;

    /*
    * get information detail job by id
    * */
    JobPostingDetailDTO getJobPostingDetail(Long id);
}
