package com.cmc.rts.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
@Getter
@Setter
@NoArgsConstructor
public class LikeRequest {
    private Long candidateId;
    private String createdBy;
    private Date createdDate;
    private Boolean liked;
}
