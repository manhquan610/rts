package com.cmc.rts.dto.response;

import lombok.Data;

@Data
public class V2RequestAssignDTO {
    private Long assignId;
    private String assignName;
    private String assignLdap;
    private Long requestId;
    private int target = 0;
    // applied
    private int qualify = 0;
    private int qualifying = 0;

    //contact
    private int contact = 0;
    private int contacting = 0;

    //interview
    private int interview = 0;
    private int interviewing = 0;

    //other
    private int offer = 0;
    private int offering = 0;

    //onboard
    private int onBoarding = 0;
    private int onBoard = 0;

    private int rejected = 0;
}
