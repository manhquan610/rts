package com.cmc.rts.rest;

import com.cmc.rts.dto.DeleteDTO;
import com.cmc.rts.dto.request.AreaRequest;
import com.cmc.rts.dto.request.JobTypeRequest;
import com.cmc.rts.dto.response.JobCategoriesDTO;
import com.cmc.rts.dto.response.JobPostingDTO;
import com.cmc.rts.dto.response.JobPostingDetailDTO;
import com.cmc.rts.dto.response.ResponseAPI;
import com.cmc.rts.entity.ManagerArea;
import com.cmc.rts.entity.ManagerJobType;
import com.cmc.rts.exception.CustomException;
import com.cmc.rts.repository.ManagerAreaRepository;
import com.cmc.rts.repository.ManagerJobTypeRepository;
import com.cmc.rts.service.ManagerAreaService;
import com.cmc.rts.service.ManagerJobTypeService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

@RestController
public class ManagerJobTypeController {
    @Autowired
    ManagerJobTypeService managerJobTypeService;

    ManagerJobTypeRepository managerJobTypeRepository;

    @GetMapping(value = "/manager-jobType/getAll")
    public ResponseEntity<Page<ManagerJobType>> getAllJobType(@RequestParam(value = "limit") Integer limit,
                                                              @RequestParam(value = "page") Integer page) {
        Pageable pageable = new PageRequest(page - 1, limit);
        return new ResponseEntity<>(managerJobTypeService.getAllJobType(pageable), HttpStatus.OK);
    }

    @GetMapping(value = "/manager-jobType/same")
    public ResponseEntity<Page<ManagerJobType>> searchByName(@RequestParam(value = "nameSearch", required = false) String name,
                                                             @RequestParam(value = "limit") Integer limit,
                                                             @RequestParam(value = "page") Integer page) {
        Pageable pageable = new PageRequest(page - 1, limit);
        return new ResponseEntity<>(managerJobTypeService.findByName(name,pageable), HttpStatus.OK);
    }

    @GetMapping(value = "/manager-jobType/exact")
    public ResponseEntity<Page<ManagerJobType>> searchByListName(@RequestParam(value = "nameSearch", required = false) String listName,
                                                              @RequestParam(value = "limit") Integer limit,
                                                              @RequestParam(value = "page") Integer page) {
        Pageable pageable = new PageRequest(page - 1, limit);
        return new ResponseEntity<>(managerJobTypeService.findByListName(listName,pageable), HttpStatus.OK);
    }

    @GetMapping(value = "/manager-jobType/getByID")
    public ResponseEntity<ManagerJobType> searchByID(@RequestParam(value = "id", required = false) Long id) {
        return new ResponseEntity<>(managerJobTypeService.getOne(id), HttpStatus.OK);
    }

    @PostMapping(value = "/createJobType")
    public ResponseEntity<?> createArea(@RequestBody JobTypeRequest request) throws Exception {
        return new ResponseEntity<ManagerJobType>( managerJobTypeService.createJobTpye(request), HttpStatus.CREATED);
    }

    @PutMapping(value = "/updateJobType")
    public ResponseEntity<?> updateJobType(@RequestBody JobTypeRequest request) throws Exception {
        return new ResponseEntity<ManagerJobType>(managerJobTypeService.updateJobType(request), HttpStatus.OK);
    }

    @PostMapping(value = "/deleteJobTypes")
    public void delete(@RequestBody DeleteDTO deleteDTO) throws Exception {
        Long[] ids = deleteDTO.getIds();
        for(Long id: ids) {
            try {
                managerJobTypeService.deleteJobTypes(id);
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception("Can't Delete! Because this item already exists in other information");
            }
        }
    }

    @GetMapping(value ="/getJobCategories")
    public Page<JobCategoriesDTO> getCategories(){
        Pageable pageable= new PageRequest(0, 10);
        return managerJobTypeService.getJobCategories(pageable);
    }

    @GetMapping(value = "/findJobPosting")
    public Page<JobPostingDTO> findJobPosting(@RequestParam(value = "page",required = false ) Integer page, @RequestParam(value ="size",required = false ) Integer size,
                                              @RequestParam(value = "skills",required = false ) String skills, @RequestParam(value="area",required = false ) Long area,
                                              @RequestParam(value = "jobType",required = false ) Long jobType, @RequestParam(value = "deadLine",required = false )String deadLine,
                                              @RequestParam(value = "experience",required = false ) Long experience, @RequestParam( value = "startSalary" ,required = false ) Integer startSalary,
                                              @RequestParam(value = "endSalary",required = false )Integer endSalary) throws ParseException {
        Pageable pageable = new PageRequest(page -1, size);
        return managerJobTypeService.getJobPosting(pageable,skills,area,jobType, deadLine,experience,startSalary,endSalary);
    }

    @ApiOperation(value = "Detail job posting", response = JobPostingDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Thành công"),
            @ApiResponse(code = 401, message = "Chưa xác thực"),
            @ApiResponse(code = 403, message = "Truy cập bị cấm"),
            @ApiResponse(code = 404, message = "Không tìm thấy"),
            @ApiResponse(code = 500, message = "Lỗi server")
    })
    @GetMapping("/jobPostingDetail")
    public ResponseEntity<JobPostingDetailDTO> getJobDetail(@RequestParam Long id){
        try{
            return new ResponseEntity<>(managerJobTypeService.getJobPostingDetail(id),HttpStatus.OK);
        }catch (Exception e){
            throw new CustomException("Error get detail job posting by id "+e,HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }
}
