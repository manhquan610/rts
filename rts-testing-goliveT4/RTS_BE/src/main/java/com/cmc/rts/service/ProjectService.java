package com.cmc.rts.service;

import com.cmc.rts.entity.Project;

import java.util.List;

public interface ProjectService {
    List<Project> findByName(String name, int offset, int limit);

    void deleteUsersWithIds(List<Long> ids);

    Project setProject(Project project);

    Project findOne(Long id);

    Project findByProjectId(Long projectId);
}
