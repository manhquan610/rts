package com.cmc.rts.repository.v2;

import com.cmc.rts.entity.RequestAssign;
import com.cmc.rts.entity.v2.V2RequestAssign;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;

import javax.persistence.QueryHint;
import java.util.List;

/**
 * @author: nthieu10
 * 7/25/2022
 **/
public interface V2RequestAssignRepository extends JpaRepository<V2RequestAssign, Long> {

    @Query(value = "select ra " +
            "from V2RequestAssign ra " +
            "where (:requestNotEmpty = false or ra.requestId in (:requestIds)) " +
            "and (:hrNotEmpty =false or ra.assignLdap in (:hrLdaps))")
    List<V2RequestAssign> findV2RequestAssignByParams(@Param("requestIds") List<Long> requestIds,
                                                      @Param("hrLdaps") List<String> hrLdaps,
                                                      @Param("requestNotEmpty") Boolean requestNotEmpty,
                                                      @Param("hrNotEmpty") Boolean hrNotEmpty);


    @QueryHints(value = { @QueryHint(name = org.hibernate.jpa.QueryHints.HINT_CACHEABLE, value = "true")})
    @Query(value = "select ra " +
            "from V2RequestAssign ra " +
            "where (:requestNotEmpty = false or ra.requestId in (:requestIds))")
    List<V2RequestAssign> findV2RequestAssignByParams(@Param("requestIds") List<Long> requestIds,
                                                      @Param("requestNotEmpty") Boolean requestNotEmpty);


    @QueryHints(value = { @QueryHint(name = org.hibernate.jpa.QueryHints.HINT_CACHEABLE, value = "true")})
    @Query(value = "select ra " +
            "from V2RequestAssign ra " +
            "where (:requestNotEmpty = false or ra.requestId in (:requestIds)) " +
            "and (:hrNotEmpty =false or ra.assignLdap in (:hrLdaps))")
    List<V2RequestAssign> findV2RequestAssignByParamsPage(@Param("requestIds") List<Long> requestIds,
                                                          @Param("hrLdaps") List<String> hrLdaps,
                                                          @Param("requestNotEmpty") Boolean requestNotEmpty,
                                                          @Param("hrNotEmpty") Boolean hrNotEmpty);

    List<V2RequestAssign> findV2RequestAssignByRequestId(Long requestId);

}

