package com.cmc.rts.repository.custom.impl;

import com.cmc.rts.entity.ApplicationConfig;
import com.cmc.rts.repository.custom.ApplicationConfigCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class ApplicationConfigRepositoryImpl implements ApplicationConfigCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<ApplicationConfig> findByName(String name, int offset, int limit) {
        try {
            StringBuilder sql = new StringBuilder("select * from application_config m where 1=1 ");

            if (name != null && !name.isEmpty()) {
                sql.append(" and m.name = '" + name + "'");
            }
            if (!String.valueOf(limit).isEmpty() && !String.valueOf(offset).isEmpty()) {
                offset = offset > 0 ? --offset : offset;
                offset = offset * limit;
                sql.append(" limit  " + limit);
                sql.append(" offset  " + offset);
            }
            Query query = entityManager.createNativeQuery(sql.toString(), ApplicationConfig.class);
            return query.getResultList();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }
}
