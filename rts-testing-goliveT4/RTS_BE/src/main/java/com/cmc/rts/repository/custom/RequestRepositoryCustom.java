package com.cmc.rts.repository.custom;

import com.cmc.rts.entity.Request;
import org.json.JSONObject;

import java.util.List;

public interface RequestRepositoryCustom {
    List<Request> findByName(JSONObject jsonObject);

    Integer totalCount(JSONObject jsonObject);

    void close(String ids);

    void open(String ids);

    void updateStatus(Long id, int status);
}
