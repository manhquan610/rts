package com.cmc.rts.repository.custom.impl;


import com.cmc.rts.entity.Department;
import com.cmc.rts.repository.custom.DepartmentRepositoryCustom;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Repository
public class DepartmentRepositoryImpl implements DepartmentRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Department> findByName(String name, int offset, int limit) {
        try {
            StringBuilder sql = new StringBuilder("select * from department m where deleted = 0");

            if (name != null && !name.isEmpty()) {
                sql.append(" and m.name like '" + name + "'");
            }
            if (!String.valueOf(limit).isEmpty() && !String.valueOf(offset).isEmpty()) {
                offset = offset > 0 ? --offset : offset;
                offset = offset * limit;
                sql.append(" limit  " + limit);
                sql.append(" offset  " + offset);
            }
            Query query = entityManager.createNativeQuery(sql.toString(), Department.class);
            return query.getResultList();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    @Override
    public Department findByDepartmentId(Long id) {
        Department department = null;
        try {
            StringBuilder sql = new StringBuilder("select * from department m where department_id = ");
            sql.append(id);
            sql.append(" limit 1 ");
            Query query = entityManager.createNativeQuery(sql.toString(), Department.class);
            List<Department> result = query.getResultList();
            if (result.size() > 0) {
                department = result.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return department;
        }

    }
}
