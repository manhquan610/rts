package com.cmc.rts.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LocationDTO {
    private Long id;
    private String name;

    public LocationDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
