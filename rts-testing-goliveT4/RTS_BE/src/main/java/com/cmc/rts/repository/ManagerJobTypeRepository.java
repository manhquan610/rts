package com.cmc.rts.repository;

import com.cmc.rts.dto.response.JobCategoriesDTO;
import com.cmc.rts.dto.response.JobPostingDTO;
import com.cmc.rts.entity.ManagerArea;
import com.cmc.rts.entity.ManagerJobType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ManagerJobTypeRepository extends JpaRepository<ManagerJobType, Long> {
    @Query(value = "SELECT m " +
            "FROM ManagerJobType m " +
            "where m.deleted = false and m.name like %:searchValue%  order by m.id desc")
    Page<ManagerJobType> findByName(@Param(value = "searchValue") String searchValues,
                                    Pageable pageable);

    @Query(value = "SELECT m " +
            "FROM ManagerJobType m " +
            " where m.deleted = false and (:nameNotEmpty = false or m.name in :lstName) order by m.id desc ")
    Page<ManagerJobType> findByListName(@Param(value = "lstName") List<String> listName,
                                     @Param("nameNotEmpty") Boolean nameNotEmpty,
                                     Pageable pageable);

    @Query(value = "update " +
            "ManagerJobType m set m.deleted = true" +
            " where m.id = :id")
    void deleteJobTypes(@Param(value = "id") Long id);

    @Query(value = "SELECT m " +
            "FROM ManagerJobType m " +
            "where m.deleted = false  order by m.id desc")
    Page<ManagerJobType> findAllJobType(Pageable pageable);

    @Query(value = "Select new com.cmc.rts.dto.response.JobCategoriesDTO( j.name, count(r.id), j.id )  " +
            "from Request r " +
            "left join r.jobType j " +
            "where r.jobType is not null and r.shareJob = true and DATEDIFF(r.deadline,CURDATE()) >= 0 " +
            "group by j.name, j.id " )
    Page<JobCategoriesDTO> getJobCategories(Pageable pageable);


    @Query(value = "Select new com.cmc.rts.dto.response.JobPostingDTO(r.id, " +
            "r.name, j.name, r.area.name, r.managerExperience.name, r.salary , r.number, r.workingType, r.createdDate ) " +
            "from Request r " +
            "left join r.jobType j " +
            "left join r.area a " +
            "left join r.managerExperience e " +
            "where (:experience = 0L or e.id = :experience)" +
            "and (:skill = '' or r.skillNameId like %:skill%) " +
            "and  (:area = 0L or a.id = :area )" +
            "and (:jobType = 0L or j.id = :jobType) "
    )
    Page<JobPostingDTO> getJobPosting(Pageable pageable,
                                      @Param(value = "skill") String skill,
                                      @Param(value = "area") Long area,
                                      @Param(value = "jobType") Long jobType,
                                      @Param(value = "experience") Long experience);

    @Query(value = "SELECT m " +
            "FROM ManagerJobType m " +
            "where (:managerJobTypeIdsNotEmpty = false or m.id in :managerJobTypeIds ) ")
    List<ManagerJobType> findManagerJobTypeByParams(@Param("managerJobTypeIds") List<Long> managerJobTypeIds,
                                                   @Param("managerJobTypeIdsNotEmpty") Boolean managerJobTypeIdsNotEmpty);

    List<ManagerJobType> findManagerJobTypeByIdIn(List<Long> managerJobTypeIds);

}
