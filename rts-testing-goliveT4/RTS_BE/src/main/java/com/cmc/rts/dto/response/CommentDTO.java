package com.cmc.rts.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class CommentDTO implements Serializable {
    private Long id;
    private String text;
    private String createdBy;
    private Date createDate;

    public CommentDTO(Long id, String text, String createdBy, Date createDate ) {
        this.id = id;
        this.text = text;
        this.createdBy = createdBy;
        this.createDate = createDate;
    }
}