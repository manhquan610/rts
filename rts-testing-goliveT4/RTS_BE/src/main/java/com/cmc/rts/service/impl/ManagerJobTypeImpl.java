package com.cmc.rts.service.impl;

import com.cmc.rts.dto.request.JobTypeRequest;
import com.cmc.rts.dto.requestDto.RequestResponseDTO;
import com.cmc.rts.dto.response.JobCategoriesDTO;
import com.cmc.rts.dto.response.JobPostingDTO;
import com.cmc.rts.dto.response.JobPostingDetailDTO;
import com.cmc.rts.dto.response.JobTypeDTO;
import com.cmc.rts.entity.ManagerJobType;
import com.cmc.rts.entity.Request;
import com.cmc.rts.exception.CustomException;
import com.cmc.rts.repository.ManagerJobTypeRepository;
import com.cmc.rts.repository.RequestRepository;
import com.cmc.rts.service.ManagerJobTypeService;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.UnknownHttpStatusCodeException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ManagerJobTypeImpl implements ManagerJobTypeService {
    @Autowired
    private ManagerJobTypeRepository managerJobTypeRepository;
    @Autowired
    private RequestRepository requestRepository;

    @Override
    public Page<ManagerJobType> findByName(String searchValues, Pageable pageable) {
        if (searchValues == null || searchValues.isEmpty()) {
            searchValues = "";
        }
        searchValues = searchValues.trim();
        return managerJobTypeRepository.findByName(searchValues, pageable);
    }

    @Override
    public Page<ManagerJobType> findByListName(String name, Pageable pageable) {
        List<String> listName = null;
        if (name == null || name == "") {
            listName = null;
        } else {
            listName = Arrays.asList(name.split("\\s*,\\s*"));
        }
        List<ManagerJobType> managerJobTypes = new ArrayList<>();
        Page<ManagerJobType> jobTypeList = managerJobTypeRepository.findByListName(listName, !CollectionUtils.isEmpty(listName), pageable);
        if(jobTypeList.getContent() != null) {
            jobTypeList.getContent().stream().forEach(x -> {
                if (x.getDescription() == null) {
                    x.setDescription("");
                }
                ManagerJobType jobType = new ManagerJobType(x.getId(), x.getName(), x.getDescription(), x.getDeleted());
                managerJobTypes.add(jobType);
            });
        }
        return new PageImpl<>(managerJobTypes, pageable, jobTypeList.getTotalElements());
    }

    @Override
    public Page<ManagerJobType> getAllJobType(Pageable pageable) {
        return managerJobTypeRepository.findAllJobType(pageable);
    }

    @Override
    public ManagerJobType getOne(Long id) {
        return managerJobTypeRepository.findOne(id);
    }

    @Override
    public ManagerJobType createJobTpye(JobTypeRequest request) throws Exception {
        ManagerJobType managerJobType = new ManagerJobType();
        managerJobType.setName(request.getName());
        managerJobType.setDescription(request.getDescription());
        managerJobType.setDeleted(false);
        try {
            managerJobType = managerJobTypeRepository.save(managerJobType);
        } catch (Exception e) {
            throw new Exception("Job type name already exist");
        }
        return managerJobType;
    }

    @Override
    public ManagerJobType updateJobType(JobTypeRequest request) throws Exception {
        if (request.getId() == null) {
            throw new Exception("Id không được phép null");
        }
        ManagerJobType managerJobType = managerJobTypeRepository.findOne(request.getId());
        if (managerJobType == null) {
            throw new Exception("Không tìm thấy thông tin về Job type");
        }
        try {
            managerJobType.setName(request.getName());
        } catch (HttpServerErrorException | HttpClientErrorException | UnknownHttpStatusCodeException e) {
            throw new Exception("Tên không được trùng lặp");
        }
        managerJobType.setDescription(request.getDescription());
        managerJobType.setDeleted(false);
        ManagerJobType jobType;
        try{
            jobType = managerJobTypeRepository.save(managerJobType);
        }catch (Exception e){
            throw new Exception("Job type name already exist");
        }
        return jobType;
    }

    @Override
    public void deleteJobTypes(Long id) {
        managerJobTypeRepository.delete(id);
    }

    @Override
    public Page<JobCategoriesDTO> getJobCategories(Pageable pageable) {
        return managerJobTypeRepository.getJobCategories(pageable);
    }

    @Override
    public Page<JobPostingDTO> getJobPosting(Pageable pageable, String skills, Long area, Long jobType, String deadLine,
                                             Long experience, Integer startSalary, Integer endSalary) throws ParseException {
        Instant deadLineDate = deadLine != null ? Instant.parse(deadLine) : null;
        List<Request> requests = requestRepository.getAllByShareJobIs(true);
        List<JobPostingDTO> jobPostingDTOS = new ArrayList<>();
        SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        Date now = new Date();
        requests.forEach(r -> {
            try {
                if (r.getDeadline().before(dtf.parse(dtf.format(now)))){
                    return;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if ((endSalary != null && startSalary != null) && (r.getSalary() == null || r.getSalary().isEmpty())) {
                return;
            }
            if ((endSalary != null && startSalary != null)
                    && (Integer.parseInt(r.getSalary()) < startSalary || Integer.parseInt(r.getSalary()) > endSalary)) {
                return;
            }

            if (experience != null && (r.getManagerExperience() == null)) {
                return;
            }
            if (experience != null && (r.getManagerExperience().getId() != experience)) {
                return;
            }

            if (area != null && (r.getArea() == null)) {
                return;
            }
            if (area != null && (r.getArea().getId() != area)) {
                return;
            }

            if (jobType != null && (r.getJobType() == null)) {
                return;
            }
            if (jobType != null && (r.getJobType().getId() != jobType)) {
                return;
            }


            if (deadLineDate != null && r.getDeadline() == null) {
                return;
            }
            //Cover Edge Case: deadline vs query data have same date or query data after deadline
            if (deadLineDate != null && (r.getDeadline()).after(DateUtils.round(Date.from(deadLineDate), Calendar.MINUTE))) {
                return;
            }

            if (skills != null) {
                List<String> items_db = Arrays.asList(r.getSkillNameId().replaceAll("\\s+", "").split("\\s*,\\s*"));
                List<String> items_skills = Arrays.asList(skills.replaceAll("\\s+", "").split("\\s*,\\s*"));
                Collection<String> similar = new HashSet<>(items_skills);
                similar.retainAll(items_db);
                if (similar.size() == 0) {
                    return;
                }
            }
            jobPostingDTOS.add(new JobPostingDTO(r.getId(), r.getName(), r.getJobType().getName(), r.getArea().getName(),
                    r.getManagerExperience().getName(), r.getSalary(), r.getNumber(), r.getWorkingType(), r.getDeadline()));
        });
        Page<JobPostingDTO> result = new PageImpl<>(jobPostingDTOS.stream().skip(pageable.getOffset()).limit(pageable.getPageSize()).collect(Collectors.toList()), pageable, jobPostingDTOS.size());
        return result;
    }

    @Override
    public JobPostingDetailDTO getJobPostingDetail(Long id) {
        if (id == null) {
            return new JobPostingDetailDTO("404","job posting id not null",null);
        }
        Request r = requestRepository.findById(id);
        if (r == null){
            return new JobPostingDetailDTO("404","job posting id not found or incorrect",null);
        }
        return new JobPostingDetailDTO(r.getId(),r.getName(),r.getJobType().getName(), r.getArea().getName(),
                r.getManagerExperience().getName(), r.getSalary(), r.getNumber(), r.getWorkingType(), r.getDeadline(),
                r.getDescription(),r.getLanguageId(),r.getSkillNameId(),
                new JobTypeDTO(r.getJobType().getId(), r.getJobType().getName(),r.getJobType().getDescription(), r.getJobType().getDeleted()), r.getBenefit(), r.getRequirement());
    }

}
