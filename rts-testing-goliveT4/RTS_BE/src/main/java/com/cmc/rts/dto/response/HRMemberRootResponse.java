package com.cmc.rts.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class HRMemberRootResponse {
    @JsonProperty("item")
    private List<HRMemberResponse> item;
    @JsonProperty("messages")
    private List<String> messages;
    @JsonProperty("status")
    private boolean status;
    @JsonProperty("totalRecords")
    private int totalRecords;

    public HRMemberRootResponse() {
    }

    public HRMemberRootResponse(List<HRMemberResponse> item, List<String> messages, boolean status, int totalRecords) {
        this.item = item;
        this.messages = messages;
        this.status = status;
        this.totalRecords = totalRecords;
    }
}
