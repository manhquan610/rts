package com.cmc.rts.dto.v2;

import com.cmc.rts.entity.ManagerJobType;
import lombok.*;

/**
 * @author: nthieu10
 * 7/26/2022
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class V2ManagerJobLevelDTO {
    private Long id;
    private ManagerJobType jobType;
    private String name;
    private int salaryMin;
    private int salaryMax;
    private String description;
}
