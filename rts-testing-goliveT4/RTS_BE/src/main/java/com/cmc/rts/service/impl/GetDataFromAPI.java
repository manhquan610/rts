package com.cmc.rts.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONArray;

public class GetDataFromAPI {
    @SuppressWarnings("finally")
    public static JSONArray getData(String urlStr) throws IOException {
        // TODO Auto-generated method stub
//		System.out.println("start");
        URL url = new URL(urlStr);
        HttpURLConnection con = null;
        BufferedReader in = null;
        StringBuffer content = new StringBuffer();
        JSONArray jsonArray = null;
        try {
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            int status = con.getResponseCode();
            if (status != -1) {
                in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }
//				System.out.println(content.toString());
                jsonArray = new JSONArray(content.toString().trim());
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            if (in != null)
                in.close();
            if (con != null) {
                con.disconnect();
            }
            return jsonArray;
        }

    }
}
