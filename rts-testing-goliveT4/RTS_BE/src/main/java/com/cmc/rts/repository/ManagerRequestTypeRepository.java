package com.cmc.rts.repository;

import com.cmc.rts.entity.ManagerRequestType;
import com.cmc.rts.repository.custom.ManagerRequestTypeRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ManagerRequestTypeRepository extends JpaRepository<ManagerRequestType, Long>, ManagerRequestTypeRepositoryCustom {
}
