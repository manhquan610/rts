package com.cmc.rts.service.impl;

import com.cmc.rts.entity.Department;
import com.cmc.rts.repository.DepartmentRepository;
import com.cmc.rts.service.CommonService;
import com.cmc.rts.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class DepartmentServiceImpl implements DepartmentService {
    @Autowired
    DepartmentRepository departmentRepository;
    @Autowired
    CommonService commonService;


    @Override
    @Transactional
    public Department addDepartment(Department department) {
        return departmentRepository.save(department);
    }

    @Override
    public List<Department> findAll(String name, int offset, int limit) {
        return departmentRepository.findByName(name, offset, limit);
    }

    @Override
    public Department findOne(Long id) {
        return departmentRepository.findOne(id);
    }

    @Override
    public void delete(Long id) {
//        departmentRepository.delete(id);
        commonService.updateDeletedRowTable("department", id);
    }

    @Override
    public void delete(String ids) {
        commonService.updateDeletedRowTable("department", ids);
    }

    @Override
    public Department findByDepartmentId(Long id) {
        return departmentRepository.findByDepartmentId(id);
    }
}
