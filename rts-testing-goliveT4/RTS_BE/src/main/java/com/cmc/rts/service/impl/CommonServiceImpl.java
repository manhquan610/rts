package com.cmc.rts.service.impl;

import com.cmc.rts.service.CommonService;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Service
public class CommonServiceImpl implements CommonService {
    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    @Override
    public void deleteRowTable(String table_name, String column_name, Long id) {
        try {
            StringBuilder sql = new StringBuilder("DELETE FROM ");
            sql.append(table_name);
            sql.append(" WHERE ");
            sql.append(column_name);
            sql.append(" = ");
            sql.append(id);
            Query query = entityManager.createNativeQuery(sql.toString());
            query.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    @Override
    public void deleteRowTable(String table_name, String column_name, String ids) {
        try {
            StringBuilder sql = new StringBuilder("DELETE FROM ");
            sql.append(table_name);
            sql.append(" WHERE ");
            sql.append(column_name);
            sql.append(" in ( ");
            sql.append(ids);
            sql.append(")");
            Query query = entityManager.createNativeQuery(sql.toString());
            query.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    @Modifying
    public void updateDeletedRowTable(String table_name, Long id) {
        try {
            StringBuilder sql = new StringBuilder(" update ");
            sql.append(table_name);
            sql.append(" set deleted = 1 where ");
            sql.append( " id in ( ");
            sql.append(id);
            sql.append(")");
            Query query = entityManager.createNativeQuery(sql.toString());
            query.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    public void updateDeletedRowTable(String table_name, String ids) {
        try {
            StringBuilder sql = new StringBuilder(" update ");
            sql.append(table_name);
            sql.append(" set deleted = 1 where id");
            sql.append(" in ( ");
            sql.append(ids);
            sql.append(")");
            Query query = entityManager.createNativeQuery(sql.toString());
            query.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
