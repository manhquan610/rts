package com.cmc.rts.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.sql.Timestamp;

@Data
@Builder
public class GLeadResponse {
    @JsonProperty("user_name")
    private String user_name;
    @JsonProperty("department")
    private String department;
    @JsonProperty("division")
    private String division;
    @JsonProperty("location")
    private String location;
    @JsonProperty("date_of_birth")
    private Timestamp date_of_birth;
    @JsonProperty("email")
    private String email;
    @JsonProperty("avatar_url")
    private String avatar_url;

    public GLeadResponse() {
    }

    public GLeadResponse(String user_name, String department, String division, String location, Timestamp date_of_birth, String email, String avatar_url) {
        this.user_name = user_name;
        this.department = department;
        this.division = division;
        this.location = location;
        this.date_of_birth = date_of_birth;
        this.email = email;
        this.avatar_url = avatar_url;
    }
}
