package com.cmc.rts.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TaInfoDto {
	@JsonProperty(value = "username")
    private String assignLdap;
	@JsonProperty(value = "fullName")
    private String assignName;
}