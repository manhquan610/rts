package com.cmc.rts.repository.custom.impl;

import com.cmc.rts.entity.Project;
import com.cmc.rts.repository.custom.ProjectRepositoryCustom;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ProjectRepositoryImpl implements ProjectRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Project> findByName(String name, int offset, int limit) {
        try {
            StringBuilder sql = new StringBuilder("select * from project where 1=1 ");
            if (name != null && !name.isEmpty()) {
                sql.append("AND name like '" + name + "'");
            }
            if (!String.valueOf(limit).isEmpty() && !String.valueOf(offset).isEmpty()) {
                offset = offset > 0 ? --offset : offset;
                offset = offset * limit;
                sql.append(" limit  " + limit);
                sql.append(" offset  " + offset);
            }
            Query query = entityManager.createNativeQuery(sql.toString(), Project.class);
            return query.getResultList();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    @Override
    public Project findByProjectId(Long id) {
        Project project = null;
        try {
            StringBuilder sql = new StringBuilder("select * from project where project_id =  " + id + " limit 1");
            Query query = entityManager.createNativeQuery(sql.toString(), Project.class);
            List<Object> object = query.getResultList();
            if (object.size() > 0) {
                project = (Project) object.get(0);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return project;
        }
    }
}
