package com.cmc.rts.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "log")
public class Log implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2710841404788747440L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "action")
    private String action;

    @Column(name = "log_time")
    private Date log_time;

    @Column(name = "table_name")
    private String tableName;

    @Column(name = "content")
    private String content;

    @Column(name = "assignee_id")
    private Long assigneeId;

    @Column(name = "interviewer_id")
    private Long interviewerId;

    @Column(name = "request_id")
    private Long requestId;

    @Column(name = "candidate_id")
    private Long candidateId;

    @Column(name = "interview_id")
    private Long interviewId;

    @Column(name = "cv_id")
    private Long cvId;

    public Log(Long id) {
        this.id = id;
    }

    public Log(Long id, String action, Date log_time, String tableName, String content, Long assigneeId,
               Long requestId, Long candidateId, Long interviewId, Long cvId) {

        super();
        this.id = id;
        this.action = action;
        this.log_time = log_time;
        this.tableName = tableName;
        this.content = content;
        this.assigneeId = assigneeId;
        this.requestId = requestId;
        this.candidateId = candidateId;
        this.cvId = cvId;
    }

    public Long getInterviewId() {
        return interviewId;
    }

    public void setInterviewId(Long interviewId) {
        this.interviewId = interviewId;
    }

    public Log(String action, Date log_time, String tableName, String content, Long assigneeId,
               Long requestId, Long candidateId, Long interviewId, Long cvId) {
        super();
        this.action = action;
        this.log_time = log_time;
        this.tableName = tableName;
        this.content = content;
        this.assigneeId = assigneeId;
        this.requestId = requestId;
        this.candidateId = candidateId;
        this.interviewId = interviewId;
        this.cvId = cvId;
    }


    public Log(String action, Date log_time, String tableName, String content, Long assigneeId,
               Long interviewerId, Long requestId, Long candidateId, Long interviewId, Long cvId) {
        super();
        this.action = action;
        this.log_time = log_time;
        this.tableName = tableName;
        this.content = content;
        this.assigneeId = assigneeId;
        this.interviewerId = interviewerId;
        this.requestId = requestId;
        this.candidateId = candidateId;
        this.interviewId = interviewId;
        this.cvId = cvId;
    }

}
