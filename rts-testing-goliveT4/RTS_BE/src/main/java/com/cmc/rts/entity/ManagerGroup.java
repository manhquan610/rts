package com.cmc.rts.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "manager_group")
@JsonIgnoreProperties({"userManager"})
public class ManagerGroup extends CmcBaseDomain {
    @Column
    private Long groupId; // id from api nghiep vu
    @Column
    private Boolean groupDesc;

    public ManagerGroup(Boolean groupDesc) {
        this.groupDesc = groupDesc;
    }
}
