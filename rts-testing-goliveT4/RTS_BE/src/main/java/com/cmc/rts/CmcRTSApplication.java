package com.cmc.rts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.rest.RepositoryRestMvcAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication(exclude = RepositoryRestMvcAutoConfiguration.class)
@EnableCaching
//@SpringBootApplication(scanBasePackages={"com.cmc.rts"})
public class CmcRTSApplication {
    public static void main(String[] args) {
        SpringApplication.run(CmcRTSApplication.class, args);
    }
}
