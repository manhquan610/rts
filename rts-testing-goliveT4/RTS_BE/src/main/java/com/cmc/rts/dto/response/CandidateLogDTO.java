package com.cmc.rts.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class CandidateLogDTO implements Serializable {

    private Long id;
    private Long requestId;
    private Long candidateId;
    private String text;
    private String createdBy;
    private Date createDate;

    public CandidateLogDTO(Long id, Long requestId, Long candidateId, String text, String createdBy, Date createDate) {
        this.id = id;
        this.requestId = requestId;
        this.candidateId = candidateId;
        this.text = text;
        this.createdBy = createdBy;
        this.createDate = createDate;
    }
}
