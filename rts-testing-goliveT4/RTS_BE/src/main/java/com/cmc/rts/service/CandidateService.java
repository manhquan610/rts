package com.cmc.rts.service;

import com.cmc.rts.dto.response.*;
import com.cmc.rts.entity.CandidateStateEnum;
import com.cmc.rts.response.SingleResponseDTO;


import java.io.IOException;
import java.util.Date;
import java.util.List;

public interface CandidateService {

    List<CandidateRootResponse> searchCandidates(List<Long> requestId, CandidateStateEnum candidateState, String searchValues,
                                           Integer page, Integer size, String assignees, String skill, String experience, String ta);
    List<TotalCandidateOfRequestResponse> searchCandidateList(List<Long> finalRequestIds, CandidateStateEnum none);

    CandidateRootResponse searchSource(Long requestId, CandidateStateEnum candidateState, String searchValues,
                                       Integer page, Integer size, String hasCV, Date startDate, Date endDate, String lastCompany);

    CandidateResponse findCandidateById(Long id) throws IOException;

//    List<Candidate> addCandidate(Long requestId, Integer page, Integer size) throws ParseException, IOException;

    CandidateResponse[] updateCandidateState(String candidateIds,String candidateStateCurrent, CandidateStateEnum candidateStateEnum, Long requestId, String assignees, String ta) throws Exception;

    CandidateResponse[] deleteCandidate(String candidateIds, Long requestId) throws IOException;

    TotalCandidateStepResponse getTotalCandidateInStepByIdRequest(List<String> idRequestIds);

    List<TotalCandidateTAResponse> getTotalCandidateInStepInTAByIdRequest(List<String> listTAs);

    List<TotalCandidateTAResponseV2> getTotalCandidateInStepInTAByIdRequestV2(List<String> listTAs, List<String> requestIds);

    SingleResponseDTO<CandidateCheckDTO> candidateCheck(String candidateMail);
}
