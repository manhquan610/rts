package com.cmc.rts.rest;

import com.cmc.rts.entity.Department;
import com.cmc.rts.entity.UserDTO;
import com.cmc.rts.security.AuthenUser;
import com.cmc.rts.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
public class DepartmentController {
    @Autowired
    private AuthenUser authenUser;

    @Autowired
    DepartmentService departmentService;

    @PostMapping(value = "/department")
    public ResponseEntity<?> createDepartment(HttpServletRequest request, @RequestBody Department department) {

        UserDTO userLogin = authenUser.getUser(request);

        departmentService.addDepartment(department);

        return new ResponseEntity<Department>(department, HttpStatus.OK);
    }

    @PutMapping(value = "/department")
    public ResponseEntity<?> updateDepartment(HttpServletRequest request, @RequestBody Department department) {

        UserDTO userLogin = authenUser.getUser(request);

        departmentService.addDepartment(department);

        return new ResponseEntity<Department>(department, HttpStatus.OK);
    }

    @GetMapping(value = "/department/{id}")
    public ResponseEntity<?> findOneDepartment(@PathVariable Long id) {
        Department department = departmentService.findOne(id);
        return new ResponseEntity<Department>(department, HttpStatus.OK);
    }

    @GetMapping(value = "/department")
    public ResponseEntity<List<Department>> allDepartment(@RequestParam(value = "name", required = false) String name,
                                                          @RequestParam(value = "offset", required = false) int offset,
                                                          @RequestParam(value = "limit", required = false) int limit) {
        List<Department> list = departmentService.findAll(name, offset, limit);
        return new ResponseEntity<List<Department>>(list, HttpStatus.OK);
    }


    @DeleteMapping(value = "/department")
    public void delete(@RequestBody long[] ids) {
        for (long item : ids) {
            departmentService.delete(item);
        }
    }
}
