package com.cmc.rts.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobTypeRequest {

    private Long id;

    private String name;

    private String description ;

    //private Boolean deleted;

    public JobTypeRequest(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
