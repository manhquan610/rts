package com.cmc.rts.service.impl;

import com.cmc.rts.dto.response.HRMemberResponse;
import com.cmc.rts.dto.response.HRMemberRootResponse;
import com.cmc.rts.dto.response.ManageKpiDTO;
import com.cmc.rts.entity.ManageKpi;
import com.cmc.rts.repository.ManageKPIRepository;
import com.cmc.rts.service.ManageKPIService;
import com.cmc.rts.service.RequestService;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ManageKPIServiceImpl implements ManageKPIService {
    private final ManageKPIRepository manageKPIRepository;
    private final RequestService requestService;

    @Autowired
    public ManageKPIServiceImpl(ManageKPIRepository manageKPIRepository,
                                RequestService requestService) {
        this.manageKPIRepository = manageKPIRepository;
        this.requestService = requestService;
    }


    @Override
    public Page<ManageKpiDTO> getAllByNameAndYear(String name, String year, Pageable pageable) {
        this.createManageKpi(year);
        return manageKPIRepository.searchManageKpiByNameAndYear(name, year, pageable)
                .map(e -> {
                    ManageKpiDTO manageKpiDTO = new ManageKpiDTO();
                    BeanUtils.copyProperties(e, manageKpiDTO);
                    return manageKpiDTO;
                });
    }

    @Override
    public List<ManageKpiDTO> getAllByNameAndYear(String name, String year) {
        return manageKPIRepository.searchManageKpiByNameAndYear(name, year).stream()
                .map(e -> {
                    ManageKpiDTO manageKpiDTO = new ManageKpiDTO();
                    BeanUtils.copyProperties(e, manageKpiDTO);
                    return manageKpiDTO;
                }).collect(Collectors.toList());
    }

    @Override
    public List<ManageKpiDTO> createManageKpi(String year) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        HRMemberRootResponse hrMemberRootResponse = requestService.getAllHRMember();
        List<ManageKpi> manageKpis = manageKPIRepository.getAllManageKpiByYear(year);
        List<ManageKpiDTO> manageKpiDTOChangesList = new ArrayList<>();

        for (HRMemberResponse h : hrMemberRootResponse.getItem()) {
            boolean isExist = false;
            for (ManageKpi m : manageKpis) {
                if (h.getUsername().equals(m.getUsername())) {
                    isExist = true;
                    break;
                }
            }
            if (!isExist) {
                ManageKpi newManageKpi = new ManageKpi();
                newManageKpi.setFullName(h.getFullName());
                newManageKpi.setUsername(h.getUsername());
                newManageKpi.setCreatedBy(authentication.getPrincipal().toString());
                newManageKpi.setDeleted(!h.isActive());
                newManageKpi.setDescription("");
                newManageKpi.setStatus(h.isActive());
                newManageKpi.setLastUpdatedBy(authentication.getPrincipal().toString());
                manageKPIRepository.save(newManageKpi);
                ManageKpiDTO newManageKpiDTO = new ManageKpiDTO();
                BeanUtils.copyProperties(newManageKpi, newManageKpiDTO);
                if (h.isActive()) {
                    newManageKpiDTO.setChangeStatus("created");
                } else {
                    newManageKpiDTO.setChangeStatus("created & deleted");
                }

                manageKpiDTOChangesList.add(newManageKpiDTO);
            }
        }

        manageKpis.forEach(m -> hrMemberRootResponse.getItem().stream().filter(h -> !h.isActive()).forEach(p -> {
            if (p.getUsername().equals(m.getUsername()) && !m.getDeleted()) {
                ManageKpi deletedManageKpi = manageKPIRepository.findByUsername(p.getUsername());
                deletedManageKpi.setDeleted(true);
                deletedManageKpi.setStatus(false);
                ManageKpiDTO deletedManageKpiDTO = new ManageKpiDTO();
                BeanUtils.copyProperties(deletedManageKpi, deletedManageKpiDTO);
                deletedManageKpiDTO.setChangeStatus("deleted");
                manageKpiDTOChangesList.add(deletedManageKpiDTO);
                manageKPIRepository.save(deletedManageKpi);
            }
        }));

        return manageKpiDTOChangesList;
    }

    @Override
    public ManageKpiDTO updateManageKpi(String username, String year, ManageKpiDTO manageKpiDTO) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        ManageKpi updatedManageKpi = new ManageKpi();
        BeanUtils.copyProperties(manageKPIRepository.findByUsername(username), updatedManageKpi);
        BeanUtils.copyProperties(manageKpiDTO, updatedManageKpi, "fullName", "username", "changeStatus");
        updatedManageKpi.setLastUpdatedBy(authentication.getPrincipal().toString());

        manageKPIRepository.save(updatedManageKpi);
        return manageKpiDTO;
    }

    public ByteArrayInputStream exportExcel(List<ManageKpiDTO> manageKpiDtoList) {
        try (Workbook workbook = new XSSFWorkbook()) {
            Sheet sheet = workbook.createSheet("KPI");

            Row row = sheet.createRow(0);

            // Define header cell style
            CellStyle headerCellStyle = createCellStyle(workbook, true, true);
            workbook.createCellStyle();

            // Creating header cells
            createHeaderCell(row, headerCellStyle);

            // Define data row style
            CellStyle dataRowCellStyle = createCellStyle(workbook, false, true);

            // Define data row total style
            CellStyle dataRowCellTotalStyle = createCellStyle(workbook, true, true);

            // Define data row name style
            CellStyle dataRowCellNameStyle = createCellStyle(workbook, false, false);

            int rowSetData = 0;
            for (int i = 0; i < manageKpiDtoList.size(); i++) {
                ManageKpiDTO manageKpiDto = manageKpiDtoList.get(i);

                Row dataRow = sheet.createRow(i + 1 + rowSetData);
                Cell dataRowCell = dataRow.createCell(0);
                dataRowCell.setCellValue(manageKpiDto.getFullName());
                dataRowCell.setCellStyle(dataRowCellNameStyle);

                Cell dataRowCell1 = dataRow.createCell(1);

                long KpiTotal = manageKpiDto.getJanQuantity() + manageKpiDto.getFebQuantity() + manageKpiDto.getMarQuantity()
                        + manageKpiDto.getAprQuantity() + manageKpiDto.getMayQuantity() + manageKpiDto.getJunQuantity()
                        + manageKpiDto.getJulQuantity() + manageKpiDto.getAugQuantity() + manageKpiDto.getSepQuantity()
                        + manageKpiDto.getOctQuantity() + manageKpiDto.getNovQuantity() + manageKpiDto.getDecQuantity();

                dataRowCell1.setCellValue(KpiTotal);
                dataRowCell1.setCellStyle(dataRowCellTotalStyle);

                Cell dataRowCell2 = dataRow.createCell(2);
                dataRowCell2.setCellValue(manageKpiDto.getJanQuantity());
                dataRowCell2.setCellStyle(dataRowCellStyle);

                Cell dataRowCell3 = dataRow.createCell(3);
                dataRowCell3.setCellValue(manageKpiDto.getFebQuantity());
                dataRowCell3.setCellStyle(dataRowCellStyle);

                Cell dataRowCell4 = dataRow.createCell(4);
                dataRowCell4.setCellValue(manageKpiDto.getMarQuantity());
                dataRowCell4.setCellStyle(dataRowCellStyle);

                Cell dataRowCell5 = dataRow.createCell(5);
                dataRowCell5.setCellValue(manageKpiDto.getAprQuantity());
                dataRowCell5.setCellStyle(dataRowCellStyle);

                Cell dataRowCell6 = dataRow.createCell(6);
                dataRowCell6.setCellValue(manageKpiDto.getMayQuantity());
                dataRowCell6.setCellStyle(dataRowCellStyle);

                Cell dataRowCell7 = dataRow.createCell(7);
                dataRowCell7.setCellValue(manageKpiDto.getJunQuantity());
                dataRowCell7.setCellStyle(dataRowCellStyle);

                Cell dataRowCell8 = dataRow.createCell(8);
                dataRowCell8.setCellValue(manageKpiDto.getJulQuantity());
                dataRowCell8.setCellStyle(dataRowCellStyle);

                Cell dataRowCell9 = dataRow.createCell(9);
                dataRowCell9.setCellValue(manageKpiDto.getAugQuantity());
                dataRowCell9.setCellStyle(dataRowCellStyle);

                Cell dataRowCell10 = dataRow.createCell(10);
                dataRowCell10.setCellValue(manageKpiDto.getSepQuantity());
                dataRowCell10.setCellStyle(dataRowCellStyle);

                Cell dataRowCell11 = dataRow.createCell(11);
                dataRowCell11.setCellValue(manageKpiDto.getOctQuantity());
                dataRowCell11.setCellStyle(dataRowCellStyle);

                Cell dataRowCell12 = dataRow.createCell(12);
                dataRowCell12.setCellValue(manageKpiDto.getNovQuantity());
                dataRowCell12.setCellStyle(dataRowCellStyle);

                Cell dataRowCell13 = dataRow.createCell(13);
                dataRowCell13.setCellValue(manageKpiDto.getDecQuantity());
                dataRowCell13.setCellStyle(dataRowCellStyle);

                // Making size of column auto resize to fit with data
                sheet.setColumnWidth(0, 25 * 450);
                sheet.setColumnWidth(1, 25 * 150);
                sheet.setColumnWidth(2, 25 * 150);
                sheet.setColumnWidth(3, 25 * 150);
                sheet.setColumnWidth(4, 25 * 150);
                sheet.setColumnWidth(5, 25 * 150);
                sheet.setColumnWidth(6, 25 * 150);
                sheet.setColumnWidth(7, 25 * 150);
                sheet.setColumnWidth(8, 25 * 150);
                sheet.setColumnWidth(9, 25 * 150);
                sheet.setColumnWidth(10, 25 * 150);
                sheet.setColumnWidth(11, 25 * 150);
                sheet.setColumnWidth(12, 25 * 150);
                sheet.setColumnWidth(13, 25 * 150);
                sheet.setColumnWidth(14, 25 * 150);
            }
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            return new ByteArrayInputStream(outputStream.toByteArray());
        } catch (IOException ex) {
            return null;
        }
    }

    private CellStyle createCellStyle(Workbook workbook, boolean isFillColor, boolean isAlignmentCenter) {

        CellStyle cellStyle = workbook.createCellStyle();
        if (isFillColor) {
            cellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
            cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        }

        if (isAlignmentCenter) {
            cellStyle.setAlignment(HorizontalAlignment.CENTER);
        } else {
            cellStyle.setAlignment(HorizontalAlignment.LEFT);
        }
        cellStyle.setBorderBottom(BorderStyle.THIN);
        cellStyle.setBorderLeft(BorderStyle.THIN);
        cellStyle.setBorderRight(BorderStyle.THIN);
        cellStyle.setBorderTop(BorderStyle.THIN);

        return cellStyle;
    }

    private void createHeaderCell(Row row, CellStyle headerCellStyle) {
        Cell cell = row.createCell(0);
        cell.setCellValue("Name");
        cell.setCellStyle(headerCellStyle);

        cell = row.createCell(1);
        cell.setCellValue("Total");
        cell.setCellStyle(headerCellStyle);

        cell = row.createCell(2);
        cell.setCellValue("January");
        cell.setCellStyle(headerCellStyle);

        cell = row.createCell(3);
        cell.setCellValue("February");
        cell.setCellStyle(headerCellStyle);

        cell = row.createCell(4);
        cell.setCellValue("March");
        cell.setCellStyle(headerCellStyle);

        cell = row.createCell(5);
        cell.setCellValue("April");
        cell.setCellStyle(headerCellStyle);

        cell = row.createCell(6);
        cell.setCellValue("May");
        cell.setCellStyle(headerCellStyle);

        cell = row.createCell(7);
        cell.setCellValue("June");
        cell.setCellStyle(headerCellStyle);

        cell = row.createCell(8);
        cell.setCellValue("July");
        cell.setCellStyle(headerCellStyle);

        cell = row.createCell(9);
        cell.setCellValue("August");
        cell.setCellStyle(headerCellStyle);

        cell = row.createCell(10);
        cell.setCellValue("September");
        cell.setCellStyle(headerCellStyle);

        cell = row.createCell(11);
        cell.setCellValue("October");
        cell.setCellStyle(headerCellStyle);

        cell = row.createCell(12);
        cell.setCellValue("November");
        cell.setCellStyle(headerCellStyle);

        cell = row.createCell(13);
        cell.setCellValue("December");
        cell.setCellStyle(headerCellStyle);
    }
}
