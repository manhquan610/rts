package com.cmc.rts.dto.response;


import com.cmc.rts.entity.RequestStatusEnum;

import java.util.Date;

public interface TaReportGetDuDTO {

    Long  getRequestAssignId();
    String getAssignLdap();
    Long getRequestId();
    Long getDepartmentId();
    Long getManagerGroupId();
    String getRequestName();
    RequestStatusEnum getRequestStatusName();
    Date getDeadline();
    Long getNumber();
    String getSkillNameId();
    Long getAreaId();
    String getSkillId();


}
