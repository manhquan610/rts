package com.cmc.rts.rest;

import com.cmc.rts.constant.Constant;
import com.cmc.rts.dto.response.LanguageDTO;
import com.cmc.rts.security.AuthenUser;
import com.cmc.rts.service.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class LanguageController {
    @Autowired
    private AuthenUser authenUser;

    @Autowired
    LanguageService languageService;

    @GetMapping(value = "/language/getFromApi")
    public void getFromApi() throws IOException {
        languageService.getFromApi();
    }

    @GetMapping(value = "/language")
    public ResponseEntity<List<LanguageDTO>> allLanguage(@RequestParam(value = "name", required = false) String name,
                                                         @RequestParam(value = "offset", required = false) int offset,
                                                         @RequestParam(value = "limit", required = false) int limit) {
        List<LanguageDTO> list = new ArrayList<>();
        Map<Integer, String> languages = Constant.languagueMap;
        for(Map.Entry<Integer, String> entry : languages.entrySet()) {
            list.add(new LanguageDTO(entry.getKey(),entry.getValue()));
        }
        return new ResponseEntity<>(list, HttpStatus.OK);
    }
}
