package com.cmc.rts.rest;

import com.cmc.rts.entity.Project;
import com.cmc.rts.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ProjectController {
    @Autowired
    ProjectService projectService;

    @GetMapping(value = "/project")
    public ResponseEntity<List<Project>> getAllProject(@RequestParam(value = "name", required = false) String name,
                                                       @RequestParam(value = "offset", required = false) int offset,
                                                       @RequestParam(value = "limit", required = false) int limit){
        List<Project> listResult = projectService.findByName(name, offset, limit);
        return new ResponseEntity<List<Project>>(listResult, HttpStatus.OK);
    }

    @PostMapping(value = "/project/delete")
    public void deleteIds( @RequestBody Object listID){
        LinkedHashMap<String, List<Integer>> mapID= (LinkedHashMap)listID;
        List<Integer> ids1 = mapID.get("listID");
        List<Long> ids = new ArrayList<>();
        if(ids1 != null && ids1.size() > 0) {
            ids = ids1.stream().mapToLong(Integer::longValue).boxed().collect(Collectors.toList());
        }
        projectService.deleteUsersWithIds(ids);
    }

    @PostMapping(value = "/project/set")
    public ResponseEntity<Project> setProject( @RequestBody Project project){
        projectService.setProject(project);
        return new ResponseEntity<Project>(project, HttpStatus.OK);
    }
}
