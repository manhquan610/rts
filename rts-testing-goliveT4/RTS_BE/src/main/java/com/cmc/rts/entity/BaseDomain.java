package com.cmc.rts.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@MappedSuperclass
@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"version", "last_updated", "last_updated_by", "deleted", "last_updated"})
public class BaseDomain implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @Column(name = "version", columnDefinition = "int default 1")
    private int version;

    @Column(name = "description", length = 3000)
    private String description;

    @Column(name = "created_date", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", nullable = false, updatable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Timestamp createdDate = new Timestamp(java.lang.System.currentTimeMillis());

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "last_updated", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Timestamp lastUpdated = new Timestamp(java.lang.System.currentTimeMillis());

    @Column(name = "last_updated_by")
    private String lastUpdatedBy;

    @Column(name = "deleted", columnDefinition = "boolean default false")
    private Boolean deleted;

    @Column(name = "status", columnDefinition = "boolean default true")
    private Boolean status;
}
