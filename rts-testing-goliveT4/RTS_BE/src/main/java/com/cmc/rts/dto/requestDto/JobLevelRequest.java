package com.cmc.rts.dto.requestDto;

import lombok.Data;

@Data
public class JobLevelRequest {

    private Long id;
    private String nameJobLevel;
    private Long idJobType;
    private int salaryMin;
    private int salaryMax;
    private String description;
}
