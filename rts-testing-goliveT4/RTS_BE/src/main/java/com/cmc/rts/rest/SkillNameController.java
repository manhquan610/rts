package com.cmc.rts.rest;

import com.cmc.rts.dto.response.SkillNameDTO;
import com.cmc.rts.security.AuthenUser;
import com.cmc.rts.service.SkillNameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
public class SkillNameController {
    @Autowired
    private AuthenUser authenUser;

    @Autowired
    SkillNameService skillNameService;

    @GetMapping(value = "/skill-name")
    public ResponseEntity<List<SkillNameDTO>> allSkillName(@RequestParam(value = "name", required = false) String name,
                                                           @RequestParam(value = "offset", required = false) int offset,
                                                           @RequestParam(value = "limit", required = false) int limit) {
        RestTemplate restTemplate = new RestTemplate();
        String[] response = restTemplate.getForObject("https://skills.cmcglobal.com.vn/api/setting/getSkillNameList", String[].class);
        Set<String> responseSet = new LinkedHashSet<>();
        Collections.addAll(responseSet, response);
        responseSet.add("Negotiation");
        responseSet.add("Communication");
        responseSet.add("Teamwork");
        responseSet.add("Time management");
        responseSet.add("Planning");
        responseSet.add("Listening");
        responseSet.add("Leadership");
        responseSet.add("Problem solving");
        responseSet.add("Time management");
        responseSet.add("Management");
        responseSet.add("Supervision");
        List<SkillNameDTO> skillNameDTOS = new ArrayList<>();
        int index = 0;
        for (String skillName : responseSet) {
        	skillNameDTOS.add(new SkillNameDTO(index, skillName));
        	index++;
		}

        if(!StringUtils.isEmpty(name)){
            skillNameDTOS = skillNameDTOS.stream().filter(x -> x.getName().toLowerCase(Locale.ROOT).contains(name.toLowerCase(Locale.ROOT))).collect(Collectors.toList());
        }
        return new ResponseEntity<>(skillNameDTOS, HttpStatus.OK);
    }

    @GetMapping(value = "/skill-name/getFromApi")
    public void getFromApi() throws IOException {
        skillNameService.getFromApi();
    }
}
