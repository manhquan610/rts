package com.cmc.rts.dto.sf4c;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MetaDataDTO {
    @SerializedName("uri")
    private String uri;
    @SerializedName("type")
    private String type;
}
