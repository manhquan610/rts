package com.cmc.rts.repository.v2;

import com.cmc.rts.entity.v2.V2RequestManagerJobLevelId;
import com.cmc.rts.entity.v2.V2RequestManagerJobLevels;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author: nthieu10
 * 7/26/2022
 **/
public interface V2RequestManagerJobLevelRepository extends JpaRepository<V2RequestManagerJobLevels, V2RequestManagerJobLevelId> {

    List<V2RequestManagerJobLevels> findV2RequestManagerJobLevelsByRequestIdIn(List<Long> requestIds);


}
