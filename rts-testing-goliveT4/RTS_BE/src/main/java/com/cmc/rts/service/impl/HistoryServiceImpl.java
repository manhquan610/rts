package com.cmc.rts.service.impl;

import com.cmc.rts.entity.HistoryMoveLog;
import com.cmc.rts.exception.CustomException;
import com.cmc.rts.repository.HistoryRepository;
import com.cmc.rts.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HistoryServiceImpl implements HistoryService {

    @Autowired
    private HistoryRepository historyRepository;

    @Override
    public void createHistory(List<HistoryMoveLog> listRequest) {
        for(HistoryMoveLog request : listRequest){
            HistoryMoveLog historyMoveLog = new HistoryMoveLog();
            historyMoveLog.setCreatedBy(request.getCreatedBy());
            historyMoveLog.setCreatedDate(request.getCreatedDate());
            historyMoveLog.setCandidateId(request.getCandidateId());
            historyMoveLog.setRequestId(request.getRequestId());
            historyMoveLog.setCandidateBefore(request.getCandidateBefore());
            historyMoveLog.setCandidateBefore(request.getCandidateBefore());
            historyMoveLog.setCandidateAfter(request.getCandidateAfter());
            historyRepository.save(historyMoveLog);
        }

    }

    @Override
    public void updateHistory(HistoryMoveLog request) {
        if(request.getRequestId() == null || request.getId() == null || request.getCandidateId() == null){
            throw new CustomException("Request Id, Candidate Id và Id không được phép null", HttpStatus.FORBIDDEN.value());
        }
        HistoryMoveLog historyMoveLog = historyRepository.findOne(request.getId());
        if(historyMoveLog == null){
            throw new CustomException("Không tìm thấy thông tin của history", HttpStatus.NOT_FOUND.value());
        }
        historyMoveLog.setCreatedBy(request.getCreatedBy());
        historyMoveLog.setCreatedDate(request.getCreatedDate());
        historyMoveLog.setCandidateBefore(request.getCandidateBefore());
        historyMoveLog.setCandidateBefore(request.getCandidateBefore());
        historyMoveLog.setCandidateAfter(request.getCandidateAfter());
        historyRepository.save(historyMoveLog);
    }

    @Override
    public  List<HistoryMoveLog> getHistoryByCandidateId(Long candidateId) {
        return historyRepository.findByCandidateId(candidateId);
    }
}
