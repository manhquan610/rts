package com.cmc.rts.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cmc.rts.entity.RejectReason;
import com.cmc.rts.repository.custom.RejectReasonRepositoryCustom;

public interface RejectReasonRepository extends JpaRepository<RejectReason, Long>, RejectReasonRepositoryCustom {
	RejectReason findByReason(String reason);
}
