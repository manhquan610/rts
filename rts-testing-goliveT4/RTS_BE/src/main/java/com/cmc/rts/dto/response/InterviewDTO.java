package com.cmc.rts.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Getter
@Setter
public class InterviewDTO {
    private Long id;
    private String requestName;
    private String title;
    private String startTime;
    private String endTime;
    private String interviewerName;
    private String comments;
    private Boolean status;
    private Date createdDate;
    private String createdBy;


    public InterviewDTO(Long id, String requestName, String title, String interviewerName, String comments, Date startTime, Date endTime, Boolean status,Date createdDate, String createdBy){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.id = id;
        this.requestName = requestName;
        this.title = title;
        if (startTime == null){
            this.startTime = null;
        } else {
            this.startTime = dateFormat.format(startTime);
        }
        if (endTime == null){
            this.endTime = null;
        } else {
            this.endTime = dateFormat.format(endTime);
        }
        this.interviewerName = interviewerName;
        this.comments = comments;
        this.status = status;
        this.createdDate = createdDate;
        this.createdBy = createdBy;

    }

}
