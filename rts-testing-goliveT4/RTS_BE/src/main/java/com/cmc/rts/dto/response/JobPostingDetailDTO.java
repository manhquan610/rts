package com.cmc.rts.dto.response;


import com.cmc.rts.entity.WorkingTypeEnum;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Create by trungtx
 * Date: 11/10/2021
 * Time: 10:07 PM
 * Project: CMC_RTS
 */

@Getter @Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JobPostingDetailDTO {

    private Long id;
    private String avatar = "https://www.cmc.com.vn/main/imgs/logo.svg";
    private String name;
    private String position;
    private String location;
    private String experience;
    private String salary;
    private Integer totalUser;
    private WorkingTypeEnum workingType;
    private String date;
    private String description;
    private String languageId;
    private String skillNameId;
    private JobTypeDTO jobType;
    private String benefit;
    private String code;
    private String message;
    private String requirement;


    public JobPostingDetailDTO(String code, String message, String avatar) {
        this.code = code;
        this.message = message;
        this.avatar = avatar;
    }

    public JobPostingDetailDTO(Long id, String name, String position, String location, String experience,
                               String salary, Integer totalUser, WorkingTypeEnum workingType, Date date,
                               String description, String languageId, String skillNameId, JobTypeDTO jobType, String benefit, String requirement) {
        this.id = id;
        this.name = name;
        this.position = position;
        this.location = location;
        this.experience = experience;
        this.salary = salary;
        this.totalUser = totalUser;
        this.workingType = workingType;
        this.date = date.toInstant().toString();
        this.description = description;
        this.languageId = languageId;
        this.skillNameId = skillNameId;
        this.jobType = jobType;
        this.benefit = benefit;
        this.requirement = requirement;
    }
}
