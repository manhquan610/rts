package com.cmc.rts.service.impl;

import com.cmc.rts.entity.UserInfo;
import com.cmc.rts.repository.UserInfoRepository;
import com.cmc.rts.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserInfoRepository userInfoRepository;

    @Override
    public UserInfo findOne(Long id) {
        return userInfoRepository.findOne(id);
    }

    @Override
    public UserInfo findByUsername(String username) {
        return userInfoRepository.findByUsername(username);
    }
}
