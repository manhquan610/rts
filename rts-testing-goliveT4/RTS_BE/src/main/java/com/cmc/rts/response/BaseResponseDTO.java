package com.cmc.rts.response;

/**
 * @author: nthieu10
 * 8/10/2022
 **/
import com.cmc.rts.constant.ErrorCode;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class BaseResponseDTO {
    private String code;
    private String message;
    private String description;

    public BaseResponseDTO() {
        this.code = ErrorCode.SUCCESS;
        this.message = "";
    }

    public BaseResponseDTO(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public BaseResponseDTO(String code, String message, String description) {
        this.code = code;
        this.message = message;
        this.description = description;
    }
}