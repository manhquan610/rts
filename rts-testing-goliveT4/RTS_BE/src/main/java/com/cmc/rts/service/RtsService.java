package com.cmc.rts.service;

import com.cmc.rts.entity.EmailQueue;
import org.springframework.messaging.MessagingException;

import java.io.IOException;

public interface RtsService {
    void initRequestStatus();

    EmailQueue sendHtmlMessage(String emailTo, String emailCc, String subject, String htmlBody) throws MessagingException, javax.mail.MessagingException;

    void saveLogException(String object, String error);

    void getUserAll(String url) throws IOException;

}
