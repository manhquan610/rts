package com.cmc.rts.dto.user;

import lombok.Getter;

import java.util.Set;

@Getter
public class User {
    private String id;
    private String fullName;
    private String departmentName;
    private Boolean status;
    private String createdDate;
    private String createdBy;
    private String updatedDate;
    private String updatedBy;
    private String email;
    private String userName;
    private String gender;
    private String employeeId;
    private String startDate;
    private String endDate;
    private String startWorkingDay;
    private Set<UserGroup> listHistoryUserGroup;
}
