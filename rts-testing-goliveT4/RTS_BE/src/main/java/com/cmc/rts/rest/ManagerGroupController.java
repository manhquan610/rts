package com.cmc.rts.rest;

import com.cmc.rts.dto.response.GroupResponse;
import com.cmc.rts.entity.ManagerGroup;
import com.cmc.rts.entity.UserDTO;
import com.cmc.rts.security.AuthenUser;
import com.cmc.rts.service.ManagerGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
public class ManagerGroupController {
    @Autowired
    private AuthenUser authenUser;

    @Autowired
    ManagerGroupService managerGroupService;

    @PostMapping(value = "/manager-group")
    public ResponseEntity<?> createManagerGroup(HttpServletRequest request, @RequestBody ManagerGroup managerGroup) {

        UserDTO userLogin = authenUser.getUser(request);

        managerGroupService.addManagerGroup(managerGroup);

        return new ResponseEntity<ManagerGroup>(managerGroup, HttpStatus.OK);
    }

    @PutMapping(value = "/manager-group/{id}")
    public ResponseEntity<?> updateManagerGroup(HttpServletRequest request, @RequestBody ManagerGroup managerGroup) {

        UserDTO userLogin = authenUser.getUser(request);

        managerGroupService.addManagerGroup(managerGroup);

        return new ResponseEntity<ManagerGroup>(managerGroup, HttpStatus.OK);
    }

    @GetMapping(value = "/manager-group/{id}")
    public ResponseEntity<?> findOneManagerGroup(@PathVariable Long id) {
        ManagerGroup managerGroup = managerGroupService.findOne(id);
        return new ResponseEntity<ManagerGroup>(managerGroup, HttpStatus.OK);
    }

    @GetMapping(value = "/manager-group")
    public ResponseEntity<GroupResponse> allManagerGroup(@RequestParam(value = "name", required = false) String name,
                                                         @RequestParam(value = "offset") int offset,
                                                         @RequestParam(value = "limit") int limit) {

        return new ResponseEntity<GroupResponse>(managerGroupService.allManagerGroups(), HttpStatus.OK);
    }


    @DeleteMapping(value = "/manager-group")
    public void delete(@RequestBody long[] ids) {
        for (long item : ids) {
            managerGroupService.delete(item);
        }
    }
}
