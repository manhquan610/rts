package com.cmc.rts.service.impl;

import com.cmc.rts.dto.response.*;
import com.cmc.rts.entity.ManagerArea;
import com.cmc.rts.entity.ManagerPriority;
import com.cmc.rts.entity.Request;
import com.cmc.rts.entity.RequestAssign;
import com.cmc.rts.repository.RequestAssignRepository;
import com.cmc.rts.repository.RequestRepository;
import com.cmc.rts.service.CandidateService;
import com.cmc.rts.service.ManagerGroupService;
import com.cmc.rts.service.ReportService;
import com.cmc.rts.service.RequestService;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ReportTAServiceImpl implements ReportService {

    @Autowired
    RequestService requestService;

    @Autowired
    private ManagerGroupService managerGroupService;

    @Autowired
    private CandidateService candidateService;

    @Autowired
    RequestAssignRepository requestAssignRepository;

    @Autowired
    RequestRepository requestRepository;


    @Override
    public Page<ReportResponse> pageReport(Date startTime, Date endTime, String projects, String skills, String groups, String departments, String locations, Pageable pageable) throws ParseException {

        // Get Data
//        List<ReportResponse> reportTAList = reportRTS();
        List<ReportResponse> reportTAList = reportRTS(startTime, endTime, projects, skills, groups, departments, locations);


        List<ReportResponse> pageList = reportTAList.stream()
                .skip((long) pageable.getPageSize() * pageable.getPageNumber())
                .limit(pageable.getPageSize())
                .collect(Collectors.toList());

        return new PageImpl<>(pageList, pageable, reportTAList.size());
    }

    @Override
    public Page<ReportResponse> pageReport(List<ReportResponse> reportResponseList, Pageable pageable) throws ParseException {
        return null;
    }

    @Override
    public ReportResponseDTO pageReport(ReportResponseDTO responseDTO, Pageable pageable) {
        return null;
    }

    @Override
    public List<ReportResponse> reportRTS() throws ParseException {
        return null;
    }

    @Override
    public List<ReportResponse> reportRTS(Date startTime, Date endTime, String projects, String skills, String groups, String departments, String locations) {
        //lấy list skill
        List<SkillNameDTO> skillNameDTOS = getListOfSkill();
        //lấy list HR(TA)
        HRMemberRootResponse hrMemberResponse = requestService.getAllHRMember();
        //lấy list distinct request assign
        List<RequestAssign> listAssignDistinctTA = requestAssignRepository.getDistinctByTAndAssignLdap();
        // lấy set TA từ request assign
        Set<String> talist = listAssignDistinctTA.stream().map(RequestAssign::getAssignLdap).collect(Collectors.toSet());
        // Lấy DUlist từ API
        GroupResponse duResponse = managerGroupService.allManagerGroups();

//        RestTemplate restTemplate = new RestTemplate();
//        ResponseEntity<GroupResponse[]> responseEntity = restTemplate.getForEntity("https://group-dashboard.cmcglobal.com.vn/api/departmentCMC?year=2021&month=10", GroupResponse[].class);
//        GroupResponse duResponse = responseEntity.getBody()[0];

//       all Request
        List<Request> allRequest = requestRepository.findAll();
//        managerPriority
        List<ManagerPriority> managerPriority = requestRepository.findManagerPriority();
//        managerArea
        List<ManagerArea> managerArea = requestRepository.findAllArea();

        // lấy list total request theo TA
        List<TotalCandidateTAResponse> totalCandidateTAResponseList = candidateService.getTotalCandidateInStepInTAByIdRequest(talist.stream().collect(Collectors.toList()));

        //lấy Request List theo TA & Filter theo deadline, skill, groupIds, departmentIds, locationIds
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        List<Long> skillsIds = extractIdFromString(skills);
        List<Long> groupIds = extractIdFromString(groups);
        List<Long> departmentIds = extractIdFromString(departments);
        List<Long> locationIds = extractIdFromString(locations);

        List<TaReportGetDuDTO> assignByTAList;
        if (startTime != null && endTime != null) {
            assignByTAList = requestAssignRepository.findAllTaReportSortedByDate(talist.stream().collect(Collectors.toList()), simpleDateFormat.format(startTime), simpleDateFormat.format(endTime));
        } else {
            // lấy list request assign DTO theo TA
            assignByTAList = requestAssignRepository.findAllTaReportSorted(talist.stream().collect(Collectors.toList()));
        }
        assignByTAList = assignByTAList.stream().filter(x -> {
                    Long locationId = x.getAreaId() == null ? null : x.getAreaId();

                    return checkFilterExist(groupIds, x.getManagerGroupId())
                            && checkFilterExist(departmentIds, x.getDepartmentId())
                            && checkSkillExist(skillsIds,x.getSkillId())
                            && checkFilterExist(locationIds, locationId);
                }
        ).collect(Collectors.toList());



        long totalTargetRequest = 0;
        long totalActualRequest = 0;

        long totalTargetGroupDU = 0;
        long totalActualGroupDU = 0;

        long totalStatusHighRequest = 0;
        long totalStatusMediumRequest = 0;
        long totalStatusLowRequest = 0;

        long totalStatusHighGroupDU = 0;
        long totalStatusMediumGroupDU = 0;
        long totalStatusLowGroupDU = 0;

        long totalApplicationRequest = 0;
        long totalQualifyRequest = 0;
        long totalInterviewRequest = 0;
        long totalOfferingRequest = 0;
        long totalReviewOfferingRequest = 0;
        long totalOnboardingRequest = 0;

        long totalApplicationGroupDU = 0;
        long totalQualifyGroupDU = 0;
        long totalInterviewGroupDU = 0;
        long totalOfferingGroupDU = 0;
        long totalReviewOfferingGroupDU = 0;
        long totalOnboardingGroupDU = 0;

        //lay TA
        String assignLdap = "";

        List<ReportResponse> reportResponseTaList = new ArrayList<>();
        for (int i = 0; i < assignByTAList.size(); i++) {

            TaReportGetDuDTO taReportGetDuDTO = assignByTAList.get(i);
            if (taReportGetDuDTO.getAssignLdap().equals(assignLdap)) {
                continue;
            }

            assignLdap = taReportGetDuDTO.getAssignLdap();
            ReportResponse reportResponseTa = new ReportResponse();

            reportResponseTa.setUnitName(getNameHR(hrMemberResponse.getItem(), taReportGetDuDTO.getAssignLdap()));

            //lay Du
            List<ReportResponse> reportResponseDuList = new ArrayList<>();
            long Du = 0L;
            for (int j = i; j < assignByTAList.size(); j++) {
                TaReportGetDuDTO taReportGetDuDTO2 = assignByTAList.get(j);

                if (taReportGetDuDTO2.getDepartmentId().equals(Du)) {
                    continue;
                }
                Du = taReportGetDuDTO2.getDepartmentId();
                if (!taReportGetDuDTO2.getAssignLdap().equals(assignLdap)) {
                    break;
                }

                ReportResponse reportResponseDu = new ReportResponse();
                reportResponseDu.setUnitName(this.getDuName(duResponse,taReportGetDuDTO2.getManagerGroupId(), taReportGetDuDTO2.getDepartmentId()));

                //lay request
                List<ReportResponse> reportResponseRequestList = new ArrayList<>();
                List<TaReportGetDuDTO> taReportGetDuDTOList = assignByTAList.stream().filter(a -> a.getDepartmentId().equals(taReportGetDuDTO2.getDepartmentId()) && a.getAssignLdap().equals(taReportGetDuDTO2.getAssignLdap())).collect(Collectors.toList());
                List<CandidateListTotalStepResponse> listTotalRequestByTa = totalCandidateTAResponseList.stream().filter(t -> t.getTa().equals(taReportGetDuDTO2.getAssignLdap())).map(TotalCandidateTAResponse::getCandidateByJobRtsList).findFirst().orElse(null);
                for (TaReportGetDuDTO taReportGetDu : taReportGetDuDTOList) {
                    ReportResponse reportResponseRequest = new ReportResponse();

                    if (listTotalRequestByTa != null ) {

                        CandidateListTotalStepResponse requestByTa = listTotalRequestByTa.stream().filter(r -> {
                            if (r.getJobRts() != null && !r.getJobRts().equals("")){
                                r.getJobRts().equals(taReportGetDu.getRequestId().toString());
                                return true;
                            }
                            return false;
                        }).findFirst().orElse(null);


                        if (requestByTa != null && requestByTa.getJobRts() != null) {

                            reportResponseRequest.setUnitName(taReportGetDu.getRequestName());
                            reportResponseRequest.setSkill(getNameSkill(Objects.requireNonNull(extractIdFromString(taReportGetDu.getSkillNameId())), skillNameDTOS));
                            reportResponseRequest.setStatus(taReportGetDu.getRequestStatusName());

                            reportResponseRequest.setLocation(this.getRequestArea(allRequest,managerArea,taReportGetDu.getRequestId()));

                            reportResponseRequest.setDeadline(taReportGetDu.getDeadline());

                            reportResponseRequest.setTotalTarget(taReportGetDu.getNumber());
                            totalTargetRequest += taReportGetDu.getNumber();

                            reportResponseRequest.setTotalActual(requestByTa.getOnboard());
                            totalActualRequest += requestByTa.getOnboard();
                            if(taReportGetDu.getNumber()!=0){
                                float shortRequest = (float)requestByTa.getOnboard()/(float)taReportGetDu.getNumber() *100;
                                reportResponseRequest.setTotalGap((long)shortRequest);
                            }else{
                                reportResponseRequest.setTotalGap(0L);
                            }

                            //Status request

                            String priority = this.getManagerPriority(allRequest,managerPriority,taReportGetDu.getRequestId());

//                             if (priority.equalsIgnoreCase("HIGH")) {
//                                totalStatusHighRequest += taReportGetDu.getNumber();
//                                reportResponseRequest.setStatusHigh(taReportGetDu.getNumber());
//                                reportResponseRequest.setStatusMedium(0L);
//                                reportResponseRequest.setStatusLow(0L);
//                            }
//                            if (priority.equalsIgnoreCase("MEDIUM")) {
//                                totalStatusMediumRequest += taReportGetDu.getNumber();
//                                reportResponseRequest.setStatusMedium(taReportGetDu.getNumber());
//                                reportResponseRequest.setStatusHigh(0L);
//                                reportResponseRequest.setStatusLow(0L);
//                            }
//                            if (priority.equalsIgnoreCase("LOW")) {
//                                totalStatusLowRequest += taReportGetDu.getNumber();
//                                reportResponseRequest.setStatusLow(taReportGetDu.getNumber());
//                                reportResponseRequest.setStatusHigh(0L);
//                                reportResponseRequest.setStatusMedium(0L);
//                            }

                            //Total request
                            reportResponseRequest.setTotalApplication(requestByTa.getQualify());
                            reportResponseRequest.setTotalQualify(requestByTa.getConfirm());
                            reportResponseRequest.setTotalInterview(requestByTa.getInterview());
                            reportResponseRequest.setTotalOffering(requestByTa.getOffer());
//                            reportResponseRequest.setTotalReviewOffering(0L);
                            reportResponseRequest.setTotalOnBoarding(requestByTa.getOnboard());
                            totalApplicationRequest += requestByTa.getQualify();
                            totalQualifyRequest += requestByTa.getConfirm();
                            totalOfferingRequest += requestByTa.getOffer();
                            totalInterviewRequest += requestByTa.getInterview();
                            totalReviewOfferingRequest += 0L;
                            totalOnboardingRequest += requestByTa.getOnboard();

                            reportResponseRequestList.add(reportResponseRequest);
                        }
                    }
                }

                reportResponseDu.setTotalTarget(totalTargetRequest);
                reportResponseDu.setTotalActual(totalActualRequest);
                if (totalTargetRequest!=0) {
                    float shortDu = (float)totalActualRequest/(float)totalTargetRequest *100;
                    reportResponseDu.setTotalGap((long)shortDu);
                }else{
                    reportResponseDu.setTotalGap(0L);
                }
                totalTargetGroupDU += totalTargetRequest;
                totalActualGroupDU += totalOnboardingRequest;

                //status DU
//                reportResponseDu.setStatusHigh(totalStatusHighRequest);
//                reportResponseDu.setStatusMedium(totalStatusMediumRequest);
//                reportResponseDu.setStatusLow(totalStatusLowRequest);
                totalStatusHighGroupDU += totalStatusHighRequest;
                totalStatusMediumGroupDU += totalStatusMediumRequest;
                totalStatusLowGroupDU += totalStatusLowRequest;

                //Total DU
                reportResponseDu.setTotalApplication(totalApplicationRequest);
                reportResponseDu.setTotalQualify(totalQualifyRequest);
                reportResponseDu.setTotalInterview(totalInterviewRequest);
                reportResponseDu.setTotalOffering(totalOfferingRequest);
//                reportResponseDu.setTotalReviewOffering(totalReviewOfferingRequest);
                reportResponseDu.setTotalOnBoarding(totalOnboardingRequest);
                totalApplicationGroupDU += totalApplicationRequest;
                totalQualifyGroupDU += totalQualifyRequest;
                totalOfferingGroupDU += totalOfferingRequest;
                totalInterviewGroupDU += totalInterviewRequest;
                totalReviewOfferingGroupDU += totalReviewOfferingRequest;
                totalOnboardingGroupDU += totalOnboardingRequest;

                reportResponseDu.setChildren(reportResponseRequestList);

                reportResponseDuList.add(reportResponseDu);

                totalTargetRequest = 0;
                totalActualRequest = 0;

                totalStatusHighRequest = 0;
                totalStatusMediumRequest = 0;
                totalStatusLowRequest = 0;

                totalApplicationRequest = 0;
                totalQualifyRequest = 0;
                totalInterviewRequest = 0;
                totalOfferingRequest = 0;
                totalReviewOfferingRequest = 0;
                totalOnboardingRequest = 0;

            }

            reportResponseTa.setTotalTarget(totalTargetGroupDU);
            reportResponseTa.setTotalActual(totalActualGroupDU);
            if (totalTargetGroupDU!=0) {
                float shortTa = (float)totalActualGroupDU/(float)totalTargetGroupDU *100;
                reportResponseTa.setTotalGap((long)shortTa);
            }else{
                reportResponseTa.setTotalGap(0L);
            }
            //status Ta
//            reportResponseTa.setStatusHigh(totalStatusHighGroupDU);
//            reportResponseTa.setStatusLow(totalStatusLowGroupDU);
//            reportResponseTa.setStatusMedium(totalStatusMediumGroupDU);
            //total TA
            reportResponseTa.setTotalApplication(totalApplicationGroupDU);
            reportResponseTa.setTotalQualify(totalQualifyGroupDU);
            reportResponseTa.setTotalInterview(totalInterviewGroupDU);
            reportResponseTa.setTotalOffering(totalOfferingGroupDU);
//            reportResponseTa.setTotalReviewOffering(0L);
            reportResponseTa.setTotalOnBoarding(totalOnboardingGroupDU);

            reportResponseTa.setChildren(reportResponseDuList);

            reportResponseTaList.add(reportResponseTa);

            totalTargetGroupDU = 0;
            totalActualGroupDU = 0;

            totalStatusHighGroupDU = 0;
            totalStatusMediumGroupDU = 0;
            totalStatusLowGroupDU = 0;

            totalApplicationGroupDU = 0;
            totalQualifyGroupDU = 0;
            totalInterviewGroupDU = 0;
            totalOfferingGroupDU = 0;
            totalReviewOfferingGroupDU = 0;
            totalOnboardingGroupDU = 0;
        }

        return reportResponseTaList;
    }

    @Override
    public ByteArrayInputStream exportExcel(List<ReportResponse> contacts) {
        try (Workbook workbook = new XSSFWorkbook()) {
            Sheet sheet = workbook.createSheet("Contacts");
            sheet.addMergedRegion(CellRangeAddress.valueOf("A1:A2"));
            sheet.addMergedRegion(CellRangeAddress.valueOf("B1:B2"));
            sheet.addMergedRegion(CellRangeAddress.valueOf("C1:C2"));
            sheet.addMergedRegion(CellRangeAddress.valueOf("D1:D2"));
            sheet.addMergedRegion(CellRangeAddress.valueOf("E1:E2"));
            sheet.addMergedRegion(CellRangeAddress.valueOf("F1:F2"));
            sheet.addMergedRegion(CellRangeAddress.valueOf("G1:G2"));
            sheet.addMergedRegion(CellRangeAddress.valueOf("H1:H2"));

            sheet.addMergedRegion(CellRangeAddress.valueOf("I1:K1"));
            sheet.addMergedRegion(CellRangeAddress.valueOf("L1:Q1"));

            Row row = sheet.createRow(0);
            Row row1 = sheet.createRow(1);

            // Define header cell style
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
            headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
            headerCellStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);
//            headerCellStyle.set
            headerCellStyle.setBorderBottom(BorderStyle.THIN);
            headerCellStyle.setBorderLeft(BorderStyle.THIN);
            headerCellStyle.setBorderRight(BorderStyle.THIN);
            headerCellStyle.setBorderTop(BorderStyle.THIN);

            // Creating header cells
            Cell cell = row.createCell(0);
            cell.setCellValue("Unit");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(0);
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(1);
            cell.setCellValue("Skill");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(1);
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(2);
            cell.setCellValue("Status");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(2);
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(3);
            cell.setCellValue("Location");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(3);
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(4);
            cell.setCellValue("Deadline");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(4);
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(5);
            cell.setCellValue("Target");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(5);
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(6);
            cell.setCellValue("Actual");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(6);
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(7);
            cell.setCellValue("Short");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(7);
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(8);
            cell.setCellValue("Status");
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(9);
            cell.setCellValue("Status");
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(10);
            cell.setCellValue("Status");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(8);
            cell.setCellValue("High");
            cell.setCellStyle(headerCellStyle);


            cell = row1.createCell(9);
            cell.setCellValue("Medium");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(10);
            cell.setCellValue("Low");
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(11);
            cell.setCellValue("Total");
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(12);
            cell.setCellValue("Total");
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(13);
            cell.setCellValue("Total");
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(14);
            cell.setCellValue("Total");
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(15);
            cell.setCellValue("Total");
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(16);
            cell.setCellValue("Total");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(11);
            cell.setCellValue("Application");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(12);
            cell.setCellValue("Qualified");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(13);
            cell.setCellValue("Interview");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(14);
            cell.setCellValue("Review Offering");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(15);
            cell.setCellValue("Offering");
            cell.setCellStyle(headerCellStyle);

            cell = row1.createCell(16);
            cell.setCellValue("Onboarding");
            cell.setCellStyle(headerCellStyle);

            // Creating data rows for each contact
            CellStyle dataRowCellStyle = workbook.createCellStyle();
            dataRowCellStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
            dataRowCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            dataRowCellStyle.setAlignment(HorizontalAlignment.CENTER);
            dataRowCellStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            int rowSetData = 0;
            for (int i = 0; i < contacts.size(); i++) {
                Row dataRow = sheet.createRow(i + 2 + rowSetData);
                Cell dataRowCell = dataRow.createCell(0);
                dataRowCell.setCellValue(contacts.get(i).getUnitName());
                dataRowCell.setCellStyle(dataRowCellStyle);

                Cell dataRowCell1 = dataRow.createCell(1);
                dataRowCell1.setCellValue("");
                dataRowCell1.setCellStyle(dataRowCellStyle);

                Cell dataRowCell2 = dataRow.createCell(2);
                dataRowCell2.setCellValue("");
                dataRowCell2.setCellStyle(dataRowCellStyle);

                Cell dataRowCell3 = dataRow.createCell(3);
                dataRowCell3.setCellValue("");
                dataRowCell3.setCellStyle(dataRowCellStyle);

                Date date = contacts.get(i).getDeadline();
                Cell dataRowCell4 = dataRow.createCell(4);
                dataRowCell4.setCellValue("");
                dataRowCell4.setCellStyle(dataRowCellStyle);

                Cell dataRowCell5 = dataRow.createCell(5);
                dataRowCell5.setCellValue(contacts.get(i).getTotalTarget());
                dataRowCell5.setCellStyle(dataRowCellStyle);

                Cell dataRowCell6 = dataRow.createCell(6);
                dataRowCell6.setCellValue(contacts.get(i).getTotalActual());
                dataRowCell6.setCellStyle(dataRowCellStyle);

                Cell dataRowCell7 = dataRow.createCell(7);
                dataRowCell7.setCellValue(contacts.get(i).getTotalGap());
                dataRowCell7.setCellStyle(dataRowCellStyle);

//                Cell dataRowCell8 = dataRow.createCell(8);
//                dataRowCell8.setCellValue(contacts.get(i).getStatusHigh());
//                dataRowCell8.setCellStyle(dataRowCellStyle);

//                Cell dataRowCell9 = dataRow.createCell(9);
//                dataRowCell9.setCellValue(contacts.get(i).getStatusMedium());
//                dataRowCell9.setCellStyle(dataRowCellStyle);

//                Cell dataRowCell10 = dataRow.createCell(10);
//                dataRowCell10.setCellValue(contacts.get(i).getStatusLow());
//                dataRowCell10.setCellStyle(dataRowCellStyle);

                Cell dataRowCell11 = dataRow.createCell(11);
                dataRowCell11.setCellValue(contacts.get(i).getTotalApplication());
                dataRowCell11.setCellStyle(dataRowCellStyle);

                Cell dataRowCell12 = dataRow.createCell(12);
                dataRowCell12.setCellValue(contacts.get(i).getTotalQualify());
                dataRowCell12.setCellStyle(dataRowCellStyle);

                Cell dataRowCell13 = dataRow.createCell(13);
                dataRowCell13.setCellValue(contacts.get(i).getTotalInterview());
                dataRowCell13.setCellStyle(dataRowCellStyle);

//                dataRow.createCell(13).setCellValue(contacts.get(i).getTotalReviewOffering());
//                Cell dataRowCell14 = dataRow.createCell(14);
//                dataRowCell14.setCellValue(contacts.get(i).getTotalReviewOffering());
//                dataRowCell14.setCellStyle(dataRowCellStyle);

//                dataRow.createCell(14).setCellValue(contacts.get(i).getTotalOffering());
                Cell dataRowCell15 = dataRow.createCell(15);
                dataRowCell15.setCellValue(contacts.get(i).getTotalOffering());
                dataRowCell15.setCellStyle(dataRowCellStyle);

//                dataRow.createCell(15).setCellValue(contacts.get(i).getTotalOnboarding());
                Cell dataRowCell16 = dataRow.createCell(16);
                dataRowCell16.setCellValue(contacts.get(i).getTotalOnBoarding());
                dataRowCell16.setCellStyle(dataRowCellStyle);
                int rowSetData1 = 0;
                for (int j = 0; j < contacts.get(i).getChildren().size(); j++) {
                    Row dataRow1 = sheet.createRow(dataRow.getRowNum() + j + 1 + rowSetData1);
                    dataRow1.createCell(0).setCellValue(contacts.get(i).getChildren().get(j).getUnitName());
                    dataRow1.createCell(1).setCellValue("");
                    dataRow1.createCell(2).setCellValue("");
                    dataRow1.createCell(3).setCellValue("");
                    dataRow1.createCell(4).setCellValue("");
                    dataRow1.createCell(5).setCellValue(contacts.get(i).getChildren().get(j).getTotalTarget());
                    dataRow1.createCell(6).setCellValue(contacts.get(i).getChildren().get(j).getTotalActual());
                    dataRow1.createCell(7).setCellValue(contacts.get(i).getChildren().get(j).getTotalGap());
                    dataRow1.createCell(11).setCellValue(contacts.get(i).getChildren().get(j).getTotalApplication());
                    dataRow1.createCell(12).setCellValue(contacts.get(i).getChildren().get(j).getTotalQualify());
                    dataRow1.createCell(13).setCellValue(contacts.get(i).getChildren().get(j).getTotalInterview());
                    dataRow1.createCell(15).setCellValue(contacts.get(i).getChildren().get(j).getTotalOffering());
                    dataRow1.createCell(16).setCellValue(contacts.get(i).getChildren().get(j).getTotalOnBoarding());
                    if (contacts.get(i).getChildren().get(j).getChildren().size() > 0) {
                        for (int k = 0; k < contacts.get(i).getChildren().get(j).getChildren().size(); k++) {
                            Row dataRow2 = sheet.createRow(dataRow1.getRowNum() + k + 1);
                            dataRow2.createCell(0).setCellValue(contacts.get(i).getChildren().get(j).getChildren().get(k).getUnitName());
                            dataRow2.createCell(1).setCellValue(contacts.get(i).getChildren().get(j).getChildren().get(k).getSkill());
                            dataRow2.createCell(2).setCellValue(contacts.get(i).getChildren().get(j).getChildren().get(k).getStatus().toString());
                            dataRow2.createCell(3).setCellValue(contacts.get(i).getChildren().get(j).getChildren().get(k).getLocation());
                            try {
                                dataRow2.createCell(4).setCellValue(dateFormat.format(contacts.get(i).getChildren().get(j).getChildren().get(k).getDeadline()));
                            } catch (Exception e) {
                                dataRow2.createCell(4).setCellValue("");
                            }
                            dataRow2.createCell(5).setCellValue(contacts.get(i).getChildren().get(j).getChildren().get(k).getTotalTarget());
                            dataRow2.createCell(6).setCellValue(contacts.get(i).getChildren().get(j).getChildren().get(k).getTotalActual());
                            dataRow2.createCell(7).setCellValue(contacts.get(i).getChildren().get(j).getChildren().get(k).getTotalGap());
                            dataRow2.createCell(11).setCellValue(contacts.get(i).getChildren().get(j).getChildren().get(k).getTotalApplication());
                            dataRow2.createCell(12).setCellValue(contacts.get(i).getChildren().get(j).getChildren().get(k).getTotalQualify());
                            dataRow2.createCell(13).setCellValue(contacts.get(i).getChildren().get(j).getChildren().get(k).getTotalInterview());
                            dataRow2.createCell(15).setCellValue(contacts.get(i).getChildren().get(j).getChildren().get(k).getTotalOffering());
                            dataRow2.createCell(16).setCellValue(contacts.get(i).getChildren().get(j).getChildren().get(k).getTotalOnBoarding());
                        }
                        rowSetData1 += contacts.get(i).getChildren().get(j).getChildren().size();
                    }
                }
                rowSetData += contacts.get(i).getChildren().size() + rowSetData1;
            }

            // Making size of column auto resize to fit with data
            sheet.setColumnWidth(0, 25 * 200);
            sheet.setColumnWidth(1, 25 * 150);
            sheet.setColumnWidth(2, 25 * 150);
            sheet.setColumnWidth(3, 25 * 150);
            sheet.setColumnWidth(4, 25 * 100);
            sheet.setColumnWidth(5, 25 * 100);
            sheet.setColumnWidth(6, 25 * 100);
            sheet.setColumnWidth(7, 25 * 100);
            sheet.setColumnWidth(8, 25 * 100);
            sheet.setColumnWidth(9, 25 * 100);
            sheet.setColumnWidth(10, 25 * 200);
            sheet.setColumnWidth(11, 25 * 200);
            sheet.setColumnWidth(12, 25 * 200);
            sheet.setColumnWidth(13, 25 * 200);
            sheet.setColumnWidth(14, 25 * 200);
            sheet.setColumnWidth(15, 25 * 200);


            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            return new ByteArrayInputStream(outputStream.toByteArray());
        } catch (IOException ex) {
            return null;
        }
    }

    @Override
    public ReportResponseDTO v2ReportRTS(Date startTime, Date endTime, String projects, String skill, String groups, String departments, String locations, String statuses, String priorityIds, Pageable pageable) {
        return null;
    }

    private String getManagerPriority(List<Request> allRequest, List<ManagerPriority> managerPriorities, Long id){

        for (int i = 0; i < allRequest.size(); i++) {
            if(allRequest.get(i).getId().equals(id)){
                for (int j = 0; j < managerPriorities.size(); j++) {
                    if (allRequest.get(i).getManagerPriority().getId().equals(managerPriorities.get(j).getId())){
                        return managerPriorities.get(j).getName();
                    }
                }
            }
        }
        return "";
    }

    private String getRequestArea(List<Request> allRequest, List<ManagerArea> allArea, Long id){

        for (int i = 0; i < allRequest.size(); i++) {
            if(allRequest.get(i).getId().equals(id)){
                for (int j = 0; j < allArea.size(); j++) {
                    if (allRequest.get(i).getArea().getId().equals(allArea.get(j).getId())){
                        return allArea.get(j).getName();
                    }
                }
            }
        }
        return "";
    }
    
    private String getDuName(GroupResponse duResponse, Long idGroup, Long idDU){
        String name= "";
        for (GroupResponse responseGroup : duResponse.getListChild()) {
            if (Objects.equals(responseGroup.getId(), idGroup)) {
                name += responseGroup.getName();
                if (responseGroup.getListChild() != null) {
                    for (GroupResponse responseDU : responseGroup.getListChild()) {
                        if (Objects.equals(responseDU.getId(), idDU)) {
                            name += " - "+responseDU.getName();
                            return name;
                        }
                    }
                }
                return name;
            }
        }
        return null;
    }

    private List<String> extractListStringFromString(String idString) {
        return StringUtils.isEmpty(idString) ? null : Arrays.stream(idString.split(","))
                .map(String::trim).collect(Collectors.toList());
    }

    private List<Long> extractIdFromString(String idString) {
        return StringUtils.isEmpty(idString) ? null : Arrays.stream(idString.split(","))
                .map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
    }

    private String getNameHR(List<HRMemberResponse> item, String taLdap) {
        if (item.size() > 0) {
            for (HRMemberResponse response : item) {
                if (Objects.equals(response.getUsername(), taLdap)) {
                    return response.getFullName();
                }
            }
        }
        return "";
    }

    private List<SkillNameDTO> getListOfSkill() {
        RestTemplate restTemplate = new RestTemplate();
        String[] response = restTemplate.getForObject("https://skills.cmcglobal.com.vn/api/setting/getSkillNameList", String[].class);
        List<SkillNameDTO> skillNameDTOS = new ArrayList<>();
        for (int i = 0; i < response.length; i++) {
            skillNameDTOS.add(new SkillNameDTO(i, response[i]));
        }
        return skillNameDTOS;
    }

    private String getNameSkill(List<Long> idSkills, List<SkillNameDTO> skillNameDTOS) {
        StringBuilder nameList = new StringBuilder();
        if (idSkills.size() == 1) {
            for (Long idSkill : idSkills) {
                nameList.append(skillNameDTOS.stream().filter(x -> x.getId().toString().equals(idSkill.toString()))
                        .collect(Collectors.toList()).stream()
                        .map(SkillNameDTO::getName).findFirst().orElse(""));
            }
        } else {
            nameList.append(", ");
            for (Long idSkill : idSkills) {
                nameList.append(skillNameDTOS.stream().filter(x -> x.getId().toString().equals(idSkill.toString()))
                        .collect(Collectors.toList()).stream()
                        .map(SkillNameDTO::getName).findFirst().orElse(""));
            }
        }
        return nameList.toString();
    }

    public boolean checkFilterExist(List<Long> target, Long id) {
        if (target == null) {
            return true;
        }
        if (id == null) {
            return false;
        }
        return target.stream().anyMatch(x -> x.equals(id));
    }

    public boolean checkSkillExist(List<Long> target, String skills ){
        if (target == null){
            return true;
        }
        if (skills == null){
            return false;
        }
        List<Long> skillsIds = extractIdFromString(skills);

        for (int i = 0; i < target.size(); i++) {
            for (int j = 0; j < skillsIds.size(); j++) {
                if (target.get(i).equals(skillsIds.get(j))){
                    return true;
                }
            }
        }

        return false;
    }

}
