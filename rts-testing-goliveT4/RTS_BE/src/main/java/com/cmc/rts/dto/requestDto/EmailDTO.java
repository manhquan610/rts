package com.cmc.rts.dto.requestDto;

import lombok.Getter;

@Getter
public class EmailDTO {
    private String emailTo;
    private String emailCc;
    private String subject;
    private String body;
}
