package com.cmc.rts.service.impl;

import com.cmc.rts.entity.UserDTO;
import com.cmc.rts.repository.UserInfoRepository;
import com.cmc.rts.security.AuthenSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        System.out.println(s);
        List<GrantedAuthority> roles = new ArrayList<>();
        UserDTO userDTO = AuthenSession.authenSessionToken.get(s);
        userDTO.getUserRoles().forEach(x -> {
            SimpleGrantedAuthority authority = new SimpleGrantedAuthority(x);
            roles.add(authority);
        });
        return new User(userDTO.getToken(), userDTO.getUserName(), roles);
    }
}