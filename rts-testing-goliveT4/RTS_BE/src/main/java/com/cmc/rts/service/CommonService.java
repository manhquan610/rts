package com.cmc.rts.service;

public interface CommonService {
    void deleteRowTable(String table_name, String column_name, Long id);

    void deleteRowTable(String table_name, String column_name, String ids);

    void updateDeletedRowTable(String table_name, Long id);

    void updateDeletedRowTable(String table_name, String ids);
}
