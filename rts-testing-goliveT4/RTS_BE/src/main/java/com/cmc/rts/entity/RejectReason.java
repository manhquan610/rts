package com.cmc.rts.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table
public class RejectReason extends CategoryBaseDomain {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	@Column(name = "reason", length = 100, nullable = false)
    private String reason;
	
}
