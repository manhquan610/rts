package com.cmc.rts.service.impl;

import com.cmc.rts.dto.sf4c.ResponseDTO;
import com.cmc.rts.dto.sf4c.Sf4cResponseDTO;
import com.cmc.rts.dto.sf4c.dto.PositionListResponse;
import com.cmc.rts.service.SF4CService;
import com.cmc.rts.utils.DIConfig.SF4CInject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SF4CServiceImpl implements SF4CService {

    @Value("${sf4c.host}")
    private String host;

    @Value("${sf4c.user.id}")
    private String userIdURL;

    @Value("${sf4c.code.position}")
    private String codePositionURL;

    @Value("${sf4c.child.code.position}")
    private String childCodePositionURL;

    @Value("${sf4c.detail.FTE_Code_Position}")
    private String detailFTECodePositionURL;

    @Value("${sf4c.detail.JobRequisition}")
    private String detailJobRequisitionURL;

    private final SF4CInject sf4cInject;

    @Autowired
    public SF4CServiceImpl(SF4CInject sf4cInject) {
        this.sf4cInject = sf4cInject;
    }

    @Override
    public ResponseDTO getUserId(String userName) {
        String url = host + userIdURL +"?$filter=username eq '"+userName+"'&$select=userId&$format=JSON";
        return sf4cInject.common.callAPIRestTemplate(null,url, MediaType.APPLICATION_JSON, HttpMethod.GET, ResponseDTO.class);
    }

    @Override
    public ResponseDTO getCodePosition(String userId) {
        String url = host + codePositionURL +"?$filter=company eq 'CMCGLOBAL' and userId eq '"+userId+"'&$select=position/Code&$format=JSON";
        return sf4cInject.common.callAPIRestTemplate(null,url, MediaType.APPLICATION_JSON, HttpMethod.GET, ResponseDTO.class);
    }

    @Override
    public ResponseDTO getChildCodePosition(String parentPosition) {
        String url = host + childCodePositionURL +"?$filter=parentPosition/code eq '"+parentPosition+"' and effectiveStatus eq 'A'&$select=code, targetFTE, externalName_en_US&$format=JSON";
        return sf4cInject.common.callAPIRestTemplate(null,url, MediaType.APPLICATION_JSON, HttpMethod.GET, ResponseDTO.class);
    }

    @Override
    public ResponseDTO getDetailFTECodePosition(String positionNav) {
        String url = host + detailFTECodePositionURL +"?$filter=positionNav/code eq '"+positionNav+"' and emplStatus eq '25640'&$select=fte, emplStatus&$format=JSON";
        return sf4cInject.common.callAPIRestTemplate(null,url, MediaType.APPLICATION_JSON, HttpMethod.GET, ResponseDTO.class);
    }

    @Override
    public ResponseDTO getDetailJobRequisition() {
        String url = host + detailJobRequisitionURL +"?$filter=internalStatus eq 'Approved' and positionNumber ne 'null'&$select=positionNumber, jobReqId&$format=JSON";
        return sf4cInject.common.callAPIRestTemplate(null,url, MediaType.APPLICATION_JSON, HttpMethod.GET, ResponseDTO.class);
    }

    @Override
    public ResponseDTO getPositionByCode(String code) {
        String url = host + childCodePositionURL +"?$filter=code eq '"+code+"' and effectiveStatus eq 'A'&$select=code, targetFTE, externalName_en_US&$format=JSON";
        return sf4cInject.common.callAPIRestTemplate(null,url, MediaType.APPLICATION_JSON, HttpMethod.GET, ResponseDTO.class);
    }

    @Override
    public List<PositionListResponse> getListPosition() {
        //Get list Position
        return getChildCodePosition(getCodePosition(getUserId("nthieu5").getD().getResults().get(0).getUserId())
                .getD().getResults().get(0).getPosition())
                .getD().getResults().stream().map(sf4cResponseDTO -> {
            PositionListResponse listResponse = new PositionListResponse();
            listResponse.setCode(sf4cResponseDTO.getCode());
            listResponse.setName(sf4cResponseDTO.getExternalName_en_US());
            listResponse.setCodeName(sf4cResponseDTO.getExternalName_en_US()+" - "+sf4cResponseDTO.getCode());
            return listResponse;
        }).collect(Collectors.toList());
    }

    @Override
    public List<PositionListResponse> getListIDRequest() {
        List<Sf4cResponseDTO> detaiRequestId = getDetailJobRequisition().getD().getResults();
        List<PositionListResponse> getListPosition = new ArrayList<>();
        for (Sf4cResponseDTO dto: detaiRequestId) {
            List<Sf4cResponseDTO> dtos = getPositionByCode(dto.getPositionNumber()).getD().getResults();
            if (dtos.size() > 0) {
                if (dtos.get(0) != null) {
                    PositionListResponse listResponse = new PositionListResponse();
                    listResponse.setCode(dto.getJobReqId());
                    listResponse.setName(dtos.get(0).getExternalName_en_US());
                    listResponse.setCodeName(dtos.get(0).getExternalName_en_US() + " - " + dto.getJobReqId());
                    getListPosition.add(listResponse);
                }
            }
        }
            if (getListPosition.size() > 0){
            return getListPosition;
        }
        return null;
    }
}
