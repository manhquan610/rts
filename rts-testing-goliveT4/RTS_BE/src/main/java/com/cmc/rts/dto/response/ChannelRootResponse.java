package com.cmc.rts.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ChannelRootResponse {
    @JsonProperty("selectedChannel")
    private Object selectedChannel;
    @JsonProperty("channellList")
    private List<ChannelResponse> channelResponseList;
    @JsonProperty("selectedChannelId")
    private Object selectedChannelId;
    @JsonProperty("totalElements")
    private int totalElements;
}
