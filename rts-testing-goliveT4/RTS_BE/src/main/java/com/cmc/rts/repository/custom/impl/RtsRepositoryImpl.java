package com.cmc.rts.repository.custom.impl;

import com.cmc.rts.repository.custom.RtsRepositoryCustom;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class RtsRepositoryImpl implements RtsRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;
}
