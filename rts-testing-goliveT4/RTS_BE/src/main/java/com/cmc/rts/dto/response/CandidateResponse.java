package com.cmc.rts.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CandidateResponse {

    @JsonProperty(value = "errorMessage")
    private String errorMessage;
    private String channelName;
    @JsonProperty("candidateId")
    private Long candidateId;
    @JsonProperty("fullName")
    private String fullName;
    @JsonProperty("email")
    private String email;
    @JsonProperty("roleName")
    private String roleName;
    @JsonProperty("status")
    private String status;
    @JsonProperty("createBy")
    private String createBy;
    @JsonProperty("sendDate")
    private Timestamp sendDate;
    @JsonProperty("createdDate")
    private Timestamp createdDate;
    @JsonProperty("updatedDate")
    private Timestamp updatedDate;
    @JsonProperty("linkRequest")
    private String linkRequest;
    @JsonProperty("accessToken")
    private String accessToken;
    @JsonProperty("accessCode")
    private String accessCode;
    @JsonProperty("phone")
    private String phone;
    @JsonProperty("step")
    private Long step;
    @JsonProperty("address")
    private String address;
    @JsonProperty("linkCv")
    private String linkCv;
    @JsonProperty("channel")
    private Long channel;
    @JsonProperty("applySince")
    private String applySince;
    @JsonProperty("introducer")
    private String introducer;
    @JsonProperty("location")
    private String location;
    @JsonProperty("school")
    private String school;
    @JsonProperty("degree")
    private String degree;
    @JsonProperty("skill")
    private String skill;
    @JsonProperty("language")
    private String language;
    @JsonProperty("job")
    private String job;
    @JsonProperty("level")
    private String level;
    @JsonProperty("experience")
    private String experience;
    @JsonProperty("sex")
    private String sex;
    @JsonProperty("birthDay")
    private String birthDay;
    @JsonProperty("stepRts")
    private String stepRts;
    @JsonProperty("jobRtsId")
    private Long jobRtsId;
    @JsonProperty("assignees")
    private String assignees;
    @JsonProperty("removeJobRtsIds")
    private String removeJobRtsIds;
    @JsonProperty("password")
    private String password;
    @JsonProperty("source")
    private String source;
    @JsonProperty("lastCompany")
    private String lastCompany;
    @JsonProperty("candidateType")
    private String candidateType;
    @JsonProperty("skypeID")
    private String skypeID;
    @JsonProperty("facebook")
    private String facebook;
    @JsonProperty("refererUser")
    private String refererUser;
    @JsonProperty("headhunterName")
    private String headhunterName;
    @JsonProperty("freelancerName")
    private String freelancerName;
    @JsonProperty("freelancerEmail")
    private String freelancerEmail;
    @JsonProperty("freelancerMobile")
    private String freelancerMobile;
    @JsonProperty("ta")
    private String ta;
//    public CandidateResponse() {
//    }
//
//    public CandidateResponse(String errorMessage, Long candidateId, String fullName, String email, String roleName, String status, String createBy, Timestamp sendDate, Timestamp createdDate, Timestamp updatedDate, String linkRequest, String accessToken, String accessCode, String phone, Long step, String address, String linkCv, Long channel, String applySince, String introducer, String location, String school, String degree, String skill, String language, String job, String level, String experience, String sex, String birthDay, String stepRts, Long jobRtsId, String assignees, String removeJobRtsIds, String password, String source, String lastCompany, String candidateType, String skypeID, String facebook, String refererUser, String headhunterName, String freelancerName, String freelancerEmail, String freelancerMobile, String ta) {
//        this.errorMessage = errorMessage;
//        this.candidateId = candidateId;
//        this.fullName = fullName;
//        this.email = email;
//        this.roleName = roleName;
//        this.status = status;
//        this.createBy = createBy;
//        this.sendDate = sendDate;
//        this.createdDate = createdDate;
//        this.updatedDate = updatedDate;
//        this.linkRequest = linkRequest;
//        this.accessToken = accessToken;
//        this.accessCode = accessCode;
//        this.phone = phone;
//        this.step = step;
//        this.address = address;
//        this.linkCv = linkCv;
//        this.channel = channel;
//        this.applySince = applySince;
//        this.introducer = introducer;
//        this.location = location;
//        this.school = school;
//        this.degree = degree;
//        this.skill = skill;
//        this.language = language;
//        this.job = job;
//        this.level = level;
//        this.experience = experience;
//        this.sex = sex;
//        this.birthDay = birthDay;
//        this.stepRts = stepRts;
//        this.jobRtsId = jobRtsId;
//        this.assignees = assignees;
//        this.removeJobRtsIds = removeJobRtsIds;
//        this.password = password;
//        this.source = source;
//        this.lastCompany = lastCompany;
//        this.candidateType = candidateType;
//        this.skypeID = skypeID;
//        this.facebook = facebook;
//        this.refererUser = refererUser;
//        this.headhunterName = headhunterName;
//        this.freelancerName = freelancerName;
//        this.freelancerEmail = freelancerEmail;
//        this.freelancerMobile = freelancerMobile;
//        this.ta = ta;
//    }
}
