package com.cmc.rts.repository;

import com.cmc.rts.entity.LanguageTmp;
import com.cmc.rts.repository.custom.LanguageTmpRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LanguageTmpRepository extends JpaRepository<LanguageTmp, Long>, LanguageTmpRepositoryCustom {
}
