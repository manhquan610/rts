package com.cmc.rts.repository.custom.impl;

import com.cmc.rts.entity.ManagerMajor;
import com.cmc.rts.entity.ManagerRequestType;
import com.cmc.rts.repository.custom.ManagerRequestTypeRepositoryCustom;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ManagerRequestTypeRepositoryImpl implements ManagerRequestTypeRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<ManagerRequestType> findByNameSame(Integer first, Integer limit,  String name) {
        try {
            StringBuilder sql = new StringBuilder("select * from manager_request_type m where 1=1 ");

            if (name != null && !name.isEmpty()) {
                sql.append(" and m.name like '%"  + name +"%'") ;
            }
            sql.append(" ORDER BY id DESC " ) ;
            if (first != null && limit != null) {
                sql.append(" limit " + first + "," + limit);
            }
            Query query = entityManager.createNativeQuery(sql.toString(), ManagerRequestType.class);
            return query.getResultList();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    @Override
    public List<ManagerRequestType> findByName(Integer first, Integer limit, List<String> lstName) {
        try {
            StringBuilder sql = new StringBuilder("select * from manager_request_type m where 1=1 ");

            if (lstName != null && !lstName.isEmpty()) {
                sql.append(" and m.name IN (NULL ") ;
                for(String  item : lstName){
                    sql.append(" , '" + item + "'") ;
                }
                sql.append(")" ) ;
            }
            sql.append(" ORDER BY id DESC " ) ;
            if (first != null && limit != null) {
                sql.append(" limit " + first + "," + limit);
            }
            Query query = entityManager.createNativeQuery(sql.toString(), ManagerRequestType.class);
            return query.getResultList();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }
}
