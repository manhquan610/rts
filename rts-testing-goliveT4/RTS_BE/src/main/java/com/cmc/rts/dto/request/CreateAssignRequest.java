package com.cmc.rts.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CreateAssignRequest {
    @JsonProperty("target")
    private int target;
    @JsonProperty("assign_ldap")
    private String assignLdap;
}
