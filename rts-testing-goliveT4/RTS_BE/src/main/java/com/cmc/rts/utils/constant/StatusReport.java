package com.cmc.rts.utils.constant;

public enum StatusReport {
    PENDING,
    IN_PROGRESS,
    COMPLETED,
    OVERDUE,
}
