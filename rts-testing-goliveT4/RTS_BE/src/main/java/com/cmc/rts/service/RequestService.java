package com.cmc.rts.service;

import com.cmc.rts.dto.request.CreateAssignRequest;
import com.cmc.rts.dto.requestDto.RequestDTO;
import com.cmc.rts.dto.requestDto.RequestProcessDTO;
import com.cmc.rts.dto.requestDto.RequestResponseDTO;
import com.cmc.rts.dto.response.GLeadResponse;
import com.cmc.rts.dto.response.HRMemberRootResponse;
import com.cmc.rts.dto.response.ResponseAPI;
import com.cmc.rts.dto.response.TaInfoDto;
import com.cmc.rts.entity.Request;
import com.cmc.rts.entity.RequestAssign;
import com.cmc.rts.entity.UserDTO;
import com.cmc.rts.response.SingleResponseDTO;
import org.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.mail.MessagingException;
import java.util.Date;
import java.util.List;

public interface RequestService {

    Request addRequest(RequestDTO requestDTO) throws Exception;

    Request addAllRequest(RequestDTO requestDTO) throws Exception;

    Request updateRequest(RequestDTO requestDTO) throws Exception;

    Request saveRequest(RequestDTO requestDTO) throws Exception;

    Request saveAllRequest(RequestDTO requestDTO) throws Exception;

    Request updateRequest(Long id, RequestDTO requestDTO) throws Exception;
    
    List<Request> findAll(JSONObject jsonObject);

    Page<RequestResponseDTO> searchRequest(UserDTO userLogin, String requestIds, String name, String groupIds, String department, String statuses, String areaIds, String hrIds, String priority, Date startDate, Date endDate, String languageIds, Date startCreate, Date endCreate, Pageable pageable);

    Page<RequestResponseDTO> v2SearchRequest(UserDTO userLogin, String requestIds, String name, String groupIds, String department, String statuses, String areaIds, String hrIds, String priority, Date startDate, Date endDate, String languageIds, Date startCreate, Date endCreate, Pageable pageable);

    Integer totalRequest(JSONObject jsonObject);

    String throwException(String errorMessage);

    String throwException(RequestDTO requestDTO);

    String validateUpdateRequest(Long requestId, RequestDTO requestDTO);

    Request findOne(Long id);

    void delete(Long id);

    void delete(String ids);

    List<Request> close(String ids, String username) throws Exception;

    void open(String ids);

    void approveRequest(String ids, String username) throws Exception;

    void approve2Request(String ids, String username) throws Exception;

    void rejectRequest(String ids, String reason, String username) throws Exception;

    List<Request> submitRequest(String ids, String receiver, UserDTO userSentDTO) throws Exception;

    List<RequestAssign> createRequestAssign(Long id, List<CreateAssignRequest> request) throws MessagingException;

    List<RequestAssign> createAllRequestAssign(Long id, List<CreateAssignRequest> request);

    ResponseAPI reassignRequest(Long id, String name, Integer target);

    ResponseAPI updateRequestCreatedBy(Long requestId, String newCreatedBy);

    RequestProcessDTO getRequestHistory(Long id);

    HRMemberRootResponse getAllHRMember();

    HRMemberRootResponse getAllHRMemberActive();

    HRMemberRootResponse getAllHRMemberHRLeadActive();

    HRMemberRootResponse getAllHRLeadActive();

    GLeadResponse getGleadByDulead(String username);

    Request updateShareJob(Long id);

    List<Long> getAllIdRequests(String ids);
    
    List<TaInfoDto> getTaInfoByRequestId(Long id);
}
