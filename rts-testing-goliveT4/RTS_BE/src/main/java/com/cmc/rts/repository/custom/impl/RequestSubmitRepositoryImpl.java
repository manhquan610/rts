package com.cmc.rts.repository.custom.impl;

import com.cmc.rts.entity.RequestSubmit;
import com.cmc.rts.repository.custom.RequestSubmitRepositoryCustom;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class RequestSubmitRepositoryImpl implements RequestSubmitRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<RequestSubmit> findByName(String name, int offset, int limit) {
        return null;
    }

    @Override
    public List<RequestSubmit> findByCode(String code, int offset, int limit) {
        return null;
    }
}
