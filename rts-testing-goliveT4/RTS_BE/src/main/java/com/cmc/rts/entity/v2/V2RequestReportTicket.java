package com.cmc.rts.entity.v2;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "request_report_ticket")
@TypeDefs({
        @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
})
public class V2RequestReportTicket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "request_id")
    private Long requestId;

    @Column(name = "target")
    private Long target = 0L;

    @Column(name = "actual")
    private Long actual = 0L;

    @Column(name = "gap")
    private Long gap = 0L;

    @Column(name = "qualify", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private Qualify qualify;

    @Column(name = "confirm", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private Confirm confirm;

    @Column(name = "interview", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private Interview interview;

    @Column(name = "offer", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private Offer offer;

    @Column(name = "onboarding")
    private Long onBoarding = 0L;

    @Column(name = "ta_lead")
    private String taLead;

    @Column(name = "failed")
    private Long failed = 0L;

    @Column(name = "created_date", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", nullable = false, updatable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private Timestamp createdDate = new Timestamp(java.lang.System.currentTimeMillis());

    @Column(name = "last_updated_date", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private Timestamp lastUpdatedDate = new Timestamp(java.lang.System.currentTimeMillis());

    @Data
    public static class Qualify {
        private Long qualified = 0L;
        private Long qualifying = 0L;
        private Long failed = 0L;
    }

    @Data
    public static class Confirm {
        private Long confirmed = 0L;
        private Long confirming = 0L;
        private Long failed = 0L;
    }

    @Data
    public static class Interview {
        private Long interviewed = 0L;
        private Long interviewing = 0L;
        private Long failed = 0L;
    }

    @Data
    public static class Offer {
        private Long offered = 0L;
        private Long offering = 0L;
        private Long failed = 0L;
    }
}
