package com.cmc.rts.entity.v2;

import com.cmc.rts.entity.ManagerJobType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

/**
 * @author: nthieu10
 * 7/25/2022
 **/
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name="manager_job_level")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "manager_job_level")
public class V2ManagerJobLevel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "job_type_id")
    private Long jobTypeId;

    @Column(name = "name", length = 50)
    private String name;

    @Column(name = "salaryMin")
    private int salaryMin;

    @Column(name = "salaryMax")
    private int salaryMax;

    @Column(name = "description" , length = 250)
    private String description ;
}
