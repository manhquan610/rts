package com.cmc.rts.rest;

import com.cmc.rts.dto.DeleteDTO;
import com.cmc.rts.dto.request.ListNameRequest;
import com.cmc.rts.dto.request.NameRequest;
import com.cmc.rts.dto.response.ExperienceDTO;
import com.cmc.rts.entity.ManagerExperience;
import com.cmc.rts.entity.UserDTO;
import com.cmc.rts.security.AuthenUser;
import com.cmc.rts.service.ManagerExperienceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
public class ManagerExperienceController {
    @Autowired
    private AuthenUser authenUser;

    @Autowired
    ManagerExperienceService managerExperienceService;

    @PostMapping(value = "/manager-experience")
    public ResponseEntity<?> createManagerExperience(HttpServletRequest request, @RequestBody ManagerExperience managerExperience) throws Exception{

        UserDTO userLogin = authenUser.getUser(request);
        if(managerExperience !=null){
            if(managerExperience.getName() != null && !managerExperience.getName().isEmpty() && managerExperience.getName().length() > 50){
                throw new Exception("You've exceeded the limit by 50 characters");
            }
            if(managerExperience.getDescription() != null && !managerExperience.getDescription().isEmpty() && managerExperience.getDescription().length() > 250){
                throw new Exception("You've exceeded the limit by 250 characters");
            }
        }

        try {
            managerExperienceService.addManagerExperience(managerExperience);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Experience Name already exist");
        }

        return new ResponseEntity<ManagerExperience>(managerExperience, HttpStatus.OK);
    }

    @PutMapping(value = "/manager-experience")
    public ResponseEntity<?> updateManagerExperience(HttpServletRequest request, @RequestBody ManagerExperience managerExperience) throws Exception{

        UserDTO userLogin = authenUser.getUser(request);

        if(managerExperience !=null){
            if(managerExperience.getName() != null && !managerExperience.getName().isEmpty() && managerExperience.getName().length() > 50){
                throw new Exception("You've exceeded the limit by 50 characters");
            }
            if(managerExperience.getDescription() != null && !managerExperience.getDescription().isEmpty() && managerExperience.getDescription().length() > 250){
                throw new Exception("You've exceeded the limit by 250 characters");
            }
        }

        try {
            managerExperienceService.addManagerExperience(managerExperience);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Experience Name already exist");
        }

        return new ResponseEntity<ManagerExperience>(managerExperience, HttpStatus.OK);
    }

    @GetMapping(value = "/manager-experience/{id}")
    public ResponseEntity<?> findOneManagerExperience(@PathVariable Long id) {
        ManagerExperience managerExperience = managerExperienceService.findOne(id);
        return new ResponseEntity<ManagerExperience>(managerExperience, HttpStatus.OK);
    }

    @PostMapping(value = "/manager-experience/same")
    public ResponseEntity<ExperienceDTO> all(@RequestBody NameRequest nameRequest) {
        ExperienceDTO experienceDTO = managerExperienceService.findAll(nameRequest);
        return new ResponseEntity<ExperienceDTO>(experienceDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/manager-experience/exact")
    public ResponseEntity<ExperienceDTO> all(@RequestBody ListNameRequest listNameRequest) {
        ExperienceDTO experienceDTO = managerExperienceService.findAll(listNameRequest);
        return new ResponseEntity<ExperienceDTO>(experienceDTO, HttpStatus.OK);
    }

    @PostMapping (value = "/manager-experience/delete")
    public void delete(@RequestBody DeleteDTO deleteDTO) throws Exception{
        Long[] ids = deleteDTO.getIds();
        for(Long item: ids) {
            try {
            managerExperienceService.delete(item);
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception("Can't Delete! Because this item already exists in other information");
            }
        }
    }
}
