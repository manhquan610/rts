package com.cmc.rts.service.impl;

import com.cmc.rts.dto.response.*;
import com.cmc.rts.entity.ManagerGroup;
import com.cmc.rts.repository.ManagerGroupRepository;
import com.cmc.rts.service.CommonService;
import com.cmc.rts.service.ManagerGroupService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class ManagerGroupServiceImpl implements ManagerGroupService {
    @Resource
    ManagerGroupRepository managerGroupRepository;
    @Autowired
    CommonService commonService;

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final String GROUP_TOKEN = "Bearer eyJTeXN0ZW0iOiJSVFMiLCJLZXkiOiJHeGFUYTVTK0lSMGx0OTBJYmZnQVJRPT06Q29CMG91OWZmOHptMCswQ2Z3S0Q5dz09In0=";

    @Override
    @Transactional
    public ManagerGroup addManagerGroup(ManagerGroup managerGroup) {
        return managerGroupRepository.save(managerGroup);
    }

    @Override
    public List<ManagerGroup> findAll(String name, int offset, int limit) {
        return managerGroupRepository.findByName(name, offset, limit);
    }

    @Override
    public ManagerGroup findOne(Long id) {
        return managerGroupRepository.findOne(id);
    }

    @Override
    public void delete(Long id) {
        commonService.updateDeletedRowTable("manager_group", id);
    }

    @Override
    public void delete(String ids) {
        commonService.updateDeletedRowTable("manager_group", ids);
    }

    @Override
    public ManagerGroup findByGroupId(Long id) {
        return managerGroupRepository.findByGroupId(id);
    }

//    @Override
//    public GroupResponse allManagerGroups() {
//        RestTemplate restTemplate = new RestTemplate();
//        ResponseEntity<GroupResponse[]> responseEntity = restTemplate.getForEntity("https://group-dashboard.cmcglobal.com.vn/api/departmentCMC?year=2021&month=10", GroupResponse[].class);
//        return responseEntity.getBody()[0];
//    }

    @Override
    public GroupResponse allManagerGroups() {
        String url = "https://auth-api.cmcglobal.com.vn/api/group/GetGroupByAdministrative?isAdministrative=true";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", GROUP_TOKEN);
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity entity = new HttpEntity(headers);
        RestTemplate restTemplate = new RestTemplate();
//        ResponseEntity<GroupResponseDTO> responseEntity = restTemplate.getForEntity(url, GroupResponseDTO.class);
        ResponseEntity<GroupResponseDTO> response = restTemplate.exchange(url, HttpMethod.GET,entity, GroupResponseDTO.class);
        GroupResponseDTO groupResponseDTO = response.getBody();
        //Lọc ra list các Group
        List<GroupResponseItemDTO> groupResponseList = groupResponseDTO.getGroupResponseDTO().stream().filter(x -> x.getParentId() == null).collect(Collectors.toList());
        //Khởi tạo CMC Global Group (CMC Global)
        GroupResponse groupResponseCMC = new GroupResponse();
        groupResponseCMC.setId(63L);
        groupResponseCMC.setName("CMC Global");
        //Khởi tạo list Parent Group
        List<GroupResponse> groupResponseParent = new ArrayList<>();
        for (GroupResponseItemDTO g : groupResponseList){
            //Khởi tạo Group (TECH, G3, MA,...)
            GroupResponse groupResponse2 = new GroupResponse();
            groupResponse2.setId(g.getId());
            groupResponse2.setName(g.getName());
            groupResponse2.setParentId(63L);
            //Mapping các All Department của Group (G3 -> DU3.1, DU3.9, DU3.11,...)
            List<GroupResponse> groupResponses = groupResponseDTO.getGroupResponseDTO().stream().filter(x -> Objects.equals(g.getId(), x.getParentId())).map(y -> {
                //Khởi tạo Department
                GroupResponse groupResponse = new GroupResponse();
                BeanUtils.copyProperties(y, groupResponse);
                return groupResponse;
            }).collect(Collectors.toList());
            groupResponse2.setListChild(groupResponses);
            groupResponseParent.add(groupResponse2);
        }
        groupResponseCMC.setListChild(groupResponseParent);
        return groupResponseCMC;
    }

    @Override
    public String getDUName(Long idGroup, Long idDU){
        try {
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<GroupResponse[]> responseEntity = restTemplate.getForEntity("https://group-dashboard.cmcglobal.com.vn/api/departmentCMC?year=2021&month=10", GroupResponse[].class);
            GroupResponse response = responseEntity.getBody()[0];
            for (GroupResponse responseGroup : response.getListChild()) {
                if (Objects.equals(responseGroup.getId(), idGroup)) {
                    if (responseGroup.getListChild().size() > 0) {
                        for (GroupResponse responseDU : responseGroup.getListChild()) {
                            if (Objects.equals(responseDU.getId(), idDU)) {
                                return responseDU.getName();
                            }
                        }
                    }
                }
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public String getGroupName(Long idGroup){
        try {
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<GroupResponse[]> responseEntity = restTemplate.getForEntity("https://group-dashboard.cmcglobal.com.vn/api/departmentCMC?year=2021&month=10", GroupResponse[].class);
            GroupResponse response = responseEntity.getBody()[0];
            for (GroupResponse responseGroup : response.getListChild()) {
                if (Objects.equals(responseGroup.getId(), idGroup)) {
                    return responseGroup.getName();
                }
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public String getGroupDUName(Long idGroup, Long idDU) {
        try {
            String name = "";
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<GroupResponse[]> responseEntity = restTemplate.getForEntity("https://group-dashboard.cmcglobal.com.vn/api/departmentCMC?year=2021&month=10", GroupResponse[].class);
            GroupResponse response = responseEntity.getBody()[0];
            for (GroupResponse responseGroup : response.getListChild()) {
                if (Objects.equals(responseGroup.getId(), idGroup)) {
                    name += responseGroup.getName();
                    if (responseGroup.getListChild() != null) {
                        for (GroupResponse responseDU : responseGroup.getListChild()) {
                            if (Objects.equals(responseDU.getId(), idDU)) {
                                name += " - "+responseDU.getName();
                                return name;
                            }
                        }
                    }
                    return name;
                }
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<ProjectResponse> getAllProject() {
        try {
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> responseEntity = restTemplate.getForEntity("https://dashboard.cmcglobal.com.vn/api/project/all", String.class);
            return objectMapper.readValue(responseEntity.getBody(), new TypeReference<List<ProjectResponse>>(){});
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public String getNameProject(Long idProject) {
        List<ProjectResponse> getAllProject = getAllProject();
        if (getAllProject.size() > 0){
            if (idProject != null){
                return getAllProject.stream().filter(x -> x.getProjectId().equals(idProject)).collect(Collectors.toList()).get(0).getProjectName();
            }
        }
        return null;
    }
}
