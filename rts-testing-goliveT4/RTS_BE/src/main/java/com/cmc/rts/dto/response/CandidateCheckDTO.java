package com.cmc.rts.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author: nthieu10
 * 8/10/2022
 **/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CandidateCheckDTO {
    String ldapTA;
    String key;
}
