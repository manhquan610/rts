package com.cmc.rts.rest;

import com.cmc.rts.dto.LocationDTO;
import com.cmc.rts.dto.request.InterviewRequest;
import com.cmc.rts.dto.response.InterviewDTO;
import com.cmc.rts.dto.response.ResponseAPI;
import com.cmc.rts.dto.response.UserRootResponse;
import com.cmc.rts.exception.CustomException;
import com.cmc.rts.service.InterviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
public class InterviewController {
    @Autowired
    InterviewService interviewService;

    @GetMapping(value = "/interview")
    public ResponseEntity<List<InterviewDTO>> getAll(@RequestParam(value = "request_id") Long request_id) {
        return new ResponseEntity<>(interviewService.getAllInterview(request_id), HttpStatus.OK);
    }

    @GetMapping(value = "/interview-search")
    public ResponseEntity<Page<InterviewDTO>> searchInterview(@RequestParam(value = "searchValue", required = false) String searchValue,
                                                              @RequestParam(value = "location", required = false) String location,
                                                              @RequestParam(value = "interviewer", required = false) String interviewer,
                                                              @RequestParam(value = "status", required = false) Boolean status,
                                                              @RequestParam(value = "request_id") Long request_id,
                                                              @RequestParam(value = "ids", required = false) String ids,
                                                              @RequestParam(value = "limit") Integer limit,
                                                              @RequestParam(value = "page") Integer page) {
        Pageable pageable = new PageRequest(page - 1, limit);
        return new ResponseEntity<>(interviewService.searchInterview(searchValue, location, interviewer, status, request_id, ids,  pageable), HttpStatus.OK);
    }

    @GetMapping(value = "/location-interview")
    public ResponseEntity<List<LocationDTO>> getLocationForInterview() {
        List<LocationDTO> locationDTOs = interviewService.getAllLocationForInterview();
        return new ResponseEntity<>(locationDTOs, HttpStatus.OK);
    }

    @GetMapping(value = "/interview/get")
    public ResponseEntity<List<InterviewDTO>> getInterviewByCandidateIdAndRequestId(@RequestParam(value = "candidate_id") Long candidate_id,
                                                                           @RequestParam(value = "request_id") Long request_id) {
        return new ResponseEntity<>(interviewService.getInterviewByCandidateIdAndRequestId(request_id, candidate_id), HttpStatus.OK);
    }

    @PostMapping(value = "/createInterview")
    public ResponseEntity<?> createInterview(@RequestBody InterviewRequest request) throws Exception {
        try {
            return new ResponseEntity<>(new ResponseAPI(interviewService.createInterview(request), "Create success!"),HttpStatus.OK);
        } catch (CustomException e){
            return new ResponseEntity<>(new ResponseAPI(e.getHttpCode(),"Error create interview: "+e), HttpStatus.OK);
        }
    }

    @PutMapping(value = "/updateInterview")
    public ResponseEntity<Void> updateInterview(@RequestBody InterviewRequest request) throws Exception {
        interviewService.updateInterview(request);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    @GetMapping(value = "/interviewer")
    public ResponseEntity<InterviewDTO> getAllInterviewer(@RequestParam String name){
        UserRootResponse userRootResponse = interviewService.searchUser(name);
        return new ResponseEntity(userRootResponse, HttpStatus.OK);
    }

    @GetMapping("/interviewer/getInterview/timeline")
    public ResponseEntity<?> getAllInterviewOneWeek(@RequestParam(value = "startDate", required = false) String startDate,
                                                    @RequestParam(value = "endDate", required = false) String endDate,
                                                    @RequestParam(value = "text_search", required = false) String textSearch
    ){
        try{
            return new ResponseEntity<>(interviewService.getAllInterviewOneWeek(startDate,endDate,textSearch), HttpStatus.OK);
        }catch (CustomException e){
            return new ResponseEntity<>(new ResponseAPI(e.getHttpCode(),"Error server getAllInterviewOneWeek: "+e), HttpStatus.OK);
        }
    }

}
