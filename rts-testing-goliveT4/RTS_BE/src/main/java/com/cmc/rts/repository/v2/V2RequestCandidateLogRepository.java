package com.cmc.rts.repository.v2;

import com.cmc.rts.entity.CandidateStateEnum;
import com.cmc.rts.entity.v2.V2RequestCandidateLog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface V2RequestCandidateLogRepository extends JpaRepository<V2RequestCandidateLog, Long> {
    List<V2RequestCandidateLog> findV2RequestCandidateLogByRequestIdIn(List<Long> requestIds);
    V2RequestCandidateLog findV2RequestCandidateLogByRequestIdAndCandidateIdAndCurrentStepAndPreviousStep(Long requestId, Long candidateId, String currentStep, String previousStep);
}
