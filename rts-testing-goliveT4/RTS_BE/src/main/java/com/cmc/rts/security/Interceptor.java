package com.cmc.rts.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cmc.rts.entity.UserDTO;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import java.util.Enumeration;

@Component
public class Interceptor implements HandlerInterceptor {

    @Autowired
    AuthenUser authenUser;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {
        boolean flag = true;
        String urlRequest = request.getRequestURL().toString();
        String bearerHeader = request.getHeader("Authorization");
        long startTime = System.currentTimeMillis();

        Enumeration<String> headerNames = request.getHeaderNames();

        if (headerNames != null) {
            while (headerNames.hasMoreElements()) {
                System.out.println("Header: " + request.getHeader(headerNames.nextElement()));
            }
        }

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        System.out.println("=============================================================================== ");
        System.out.println("------------- Interception.preHandle start ---> ");
        System.out.println("------------- Request URL: " + request.getRequestURL());
        System.out.println("------------- Start Time: " + System.currentTimeMillis());
        System.out.println("------------- Authorization: " + bearerHeader);

        if (bearerHeader == null || !bearerHeader.startsWith("Bearer")) {
            if (!urlRequest.endsWith("/api/authen/login")) {
                JsonObject dataRes = new JsonObject();
                dataRes.addProperty("code", "ITCT_999");
                dataRes.addProperty("errorMessage", "Not logged in!");
                response.getWriter().write(dataRes.toString());
                System.out.println("------------- Status: Not logged in!");
                flag = false;
            }
        } else {
            UserDTO userLogin = authenUser.getUser(request);
            if (userLogin == null) {
                JsonObject dataRes = new JsonObject();
                dataRes.addProperty("code", "ITCT_666");
                dataRes.addProperty("errorMessage", "Token expired!");
                response.getWriter().write(dataRes.toString());
                System.out.println("------------- Status: Token expired!");
                flag = false;
            } else {
                if (urlRequest.endsWith("/api/authen/login")) {
                    JsonObject dataRes = new JsonObject();
                    dataRes.addProperty("code", "ITCT_333");
                    dataRes.addProperty("errorMessage", "Already logged in!");
                    response.getWriter().write(dataRes.toString());
                    System.out.println("------------- Status: Already logged in!");
                    flag = false;
                }
            }
        }

        System.out.println("=============================================================================== ");

        // todo role user api

        return flag;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object o, ModelAndView modelAndView) {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object o, Exception e) {

    }
}

