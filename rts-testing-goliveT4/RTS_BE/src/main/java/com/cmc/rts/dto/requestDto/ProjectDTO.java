package com.cmc.rts.dto.requestDto;

import lombok.Getter;

@Getter
public class ProjectDTO {
    private Long projectId;
    private String projectName;
    private String startDate;
    private String endDate;
    private String projectCode;
}
