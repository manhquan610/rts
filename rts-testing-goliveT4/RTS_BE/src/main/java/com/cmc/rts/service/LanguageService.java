package com.cmc.rts.service;

import java.io.IOException;
import java.util.List;

public interface LanguageService {

    void getFromApi() throws IOException;
}
