package com.cmc.rts.dto.response;

import com.cmc.rts.entity.CandidateStateEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class HistoryMoveLogDTO {

    private String candidateBefore;

    private String candidateAfter;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private Timestamp createdDate;

    private String createdBy;

    private Long candidateId;

    private Long requestId;


}
