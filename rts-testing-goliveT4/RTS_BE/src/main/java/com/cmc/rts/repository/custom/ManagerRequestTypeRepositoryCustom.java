package com.cmc.rts.repository.custom;

import com.cmc.rts.entity.ManagerMajor;
import com.cmc.rts.entity.ManagerPriority;
import com.cmc.rts.entity.ManagerRequestType;

import java.util.List;

public interface ManagerRequestTypeRepositoryCustom {
    List<ManagerRequestType> findByNameSame(Integer first, Integer limit, String name);
    List<ManagerRequestType> findByName(Integer first, Integer limit, List<String> name);
}
