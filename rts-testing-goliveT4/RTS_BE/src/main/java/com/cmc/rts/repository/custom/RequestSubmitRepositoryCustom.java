package com.cmc.rts.repository.custom;

import com.cmc.rts.entity.RequestSubmit;

import java.util.List;

public interface RequestSubmitRepositoryCustom {
    List<RequestSubmit> findByName(String name, int offset, int limit);

    List<RequestSubmit> findByCode(String code, int offset, int limit);
}
