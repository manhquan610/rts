package com.cmc.rts.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class GroupDuIdName {
    private Long id;
    private String name;
    private List<GroupDuIdName> duIdName;
}
