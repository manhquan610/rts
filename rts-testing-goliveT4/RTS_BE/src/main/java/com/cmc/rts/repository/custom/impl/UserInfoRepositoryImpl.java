package com.cmc.rts.repository.custom.impl;

import com.cmc.rts.entity.UserInfo;
import com.cmc.rts.repository.custom.UserInfoRepositoryCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class UserInfoRepositoryImpl implements UserInfoRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<UserInfo> findByName(String name, int offset, int limit) {
        try {
            StringBuilder sql = new StringBuilder("select * from user_info m where 1=1");

            if (name != null && !name.isEmpty()) {
                sql.append(" and m.name = '" + name + "' ");
            }
            if (!String.valueOf(limit).isEmpty() && !String.valueOf(offset).isEmpty()) {
                offset = offset > 0 ? --offset : offset;
                offset = offset * limit;
                sql.append(" limit  " + limit);
                sql.append(" offset  " + offset);
            }
            Query query = entityManager.createNativeQuery(sql.toString(), UserInfo.class);
            return query.getResultList();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    @Override
    public UserInfo findByUsername(String username) {
        UserInfo userInfo = null;
        try {
            StringBuilder sql = new StringBuilder("select * from user_info m where user_name = '");
            sql.append(username);
            sql.append("' limit 1");
            Query query = entityManager.createNativeQuery(sql.toString(), UserInfo.class);
            Object object = query.getSingleResult();
            if (object != null) {
                userInfo = (UserInfo) object;
            }
        } catch (Exception e) {
//            e.printStackTrace();
        } finally {
            return userInfo;
        }
    }

    @Override
    public UserInfo getLastGroupLeaderSubmit(Long requestId) {
        StringBuilder stringBuilder = new StringBuilder("SELECT\n" +
                "  b.*\n" +
                "FROM request_submit a,\n" +
                "  user_info b\n" +
                "WHERE a.id = (SELECT\n" +
                "                MAX(c.id) AS request_submit_id\n" +
                "              FROM request a,\n" +
                "                request_request_submits b,\n" +
                "                request_submit c,\n" +
                "                request_status d\n" +
                "              WHERE a.id = b.request_id\n" +
                "                  AND c.id = b.request_submits_id\n" +
                "                  AND c.request_status_id = d.id\n" +
                "                  AND d.name = 'Pending'\n" +
                "                  AND a.id =  ");
        stringBuilder.append(requestId);
        stringBuilder.append(" )\n" +
                "    AND a.user_receiver_id = b.id");
        String sql = stringBuilder.toString();
        Query query = entityManager.createNativeQuery(sql.toString(), UserInfo.class);
        List<UserInfo> userInfos = query.getResultList();
        if (userInfos.size() > 0) {
            return userInfos.get(0);
        } else {
            return null;
        }
    }
}
