package com.cmc.rts.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class CandidateRootResponse {

    @JsonProperty("jobRtsId")
    private String jobRtsId;
    @JsonProperty("selectedCandidate")
    private Object selectedCandidate;
    @JsonProperty("candidateList")
    private List<CandidateResponse> candidateList;
    @JsonProperty("selectedCandidateId")
    private Object selectedCandidateId;
    @JsonProperty("projectroleList")
    private Object projectroleList;
    @JsonProperty("totalElements")
    private int totalElements;
    @JsonProperty("page")
    private int page;
    @JsonProperty("size")
    private int size;
    @JsonProperty("totalPage")
    private int totalPage;
    @JsonProperty("totalSource")
    private int totalSource;
    @JsonProperty("totalQualify")
    private int totalQualify;
    @JsonProperty("allQualify")
    private int allQualify;
    @JsonProperty("totalConfirm")
    private int totalConfirm;
    @JsonProperty("totalInterview")
    private int totalInterview;
    @JsonProperty("totalOffer")
    private int totalOffer;
    @JsonProperty("totalOnboard")
    private int totalOnboard;
    @JsonProperty("totalFailed")
    private int totalFailed;

    private int allConfirm;
    private int allInterview;
    private int allOffer;
    private int allOnboard;
    private int allFailed;


    public CandidateRootResponse() {
    }

    public CandidateRootResponse(Object selectedCandidate, List<CandidateResponse> candidateList, Object selectedCandidateId, Object projectroleList, int totalElements, int page, int size, int totalPage) {
        this.selectedCandidate = selectedCandidate;
        this.candidateList = candidateList;
        this.selectedCandidateId = selectedCandidateId;
        this.projectroleList = projectroleList;
        this.totalElements = totalElements;
        this.page = page;
        this.size = size;
        this.totalPage = totalPage;
    }

    public Object getSelectedCandidate() {
        return selectedCandidate;
    }

    public void setSelectedCandidate(Object selectedCandidate) {
        this.selectedCandidate = selectedCandidate;
    }

    public List<CandidateResponse> getCandidateList() {
        return candidateList;
    }

    public void setCandidateList(List<CandidateResponse> candidateList) {
        this.candidateList = candidateList;
    }

    public Object getSelectedCandidateId() {
        return selectedCandidateId;
    }

    public void setSelectedCandidateId(Object selectedCandidateId) {
        this.selectedCandidateId = selectedCandidateId;
    }

    public Object getProjectroleList() {
        return projectroleList;
    }

    public void setProjectroleList(Object projectroleList) {
        this.projectroleList = projectroleList;
    }

    public int getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(int totalElements) {
        this.totalElements = totalElements;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getTotalFailed() {
        return totalFailed;
    }

    public void setTotalFailed(int totalFailed) {
        this.totalFailed = totalFailed;
    }

    public int getTotalSource() {
        return totalSource;
    }

    public int getTotalQualify() {
        return totalQualify;
    }

    public int getTotalConfirm() {
        return totalConfirm;
    }

    public int getTotalInterview() {
        return totalInterview;
    }

    public int getTotalOffer() {
        return totalOffer;
    }

    public int getTotalOnboard() {
        return totalOnboard;
    }
}
