package com.cmc.rts.rest;

import com.cmc.rts.dto.response.LikeDTO;
import com.cmc.rts.service.LikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LikeController {
    @Autowired
    LikeService likeService;

    @PostMapping(value = "/liked")
    public ResponseEntity<?> createLike(@RequestParam(value = "candidateID") Long candidateId, @RequestParam(value = "liked") Boolean liked) {

        return new ResponseEntity<>(likeService.createLike(candidateId, liked), HttpStatus.OK);
    }

    @GetMapping(value = "/totalReact")
    public ResponseEntity<LikeDTO> getTotalReact(@RequestParam(value = "candidateID") Long candidateId) {
        return new ResponseEntity<LikeDTO>(likeService.totalReact(candidateId), HttpStatus.OK);
    }
}
