package com.cmc.rts.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter

public class UserRootResponse {

    @JsonProperty ("totalRecords")
    private int totalRecords;
    @JsonProperty ("items")
    private List<UserResponse> userResponses;
    @JsonProperty ("status")
    private Boolean status;

    public UserRootResponse() {
    }

    public UserRootResponse(int totalRecords, List<UserResponse> userResponses, Boolean status) {
        this.totalRecords = totalRecords;
        this.userResponses = userResponses;
        this.status = status;
    }
}
