package com.cmc.rts.rest;

import com.cmc.rts.dto.request.CreateAssignRequest;
import com.cmc.rts.dto.response.CandidateCheckDTO;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.http.HttpStatus;
import com.cmc.rts.dto.request.ReassignedDTO;
import com.cmc.rts.dto.requestDto.RequestDTO;
import com.cmc.rts.dto.requestDto.RequestProcessDTO;
import com.cmc.rts.dto.requestDto.RequestResponseDTO;
import com.cmc.rts.dto.response.GLeadResponse;
import com.cmc.rts.dto.response.HRMemberRootResponse;
import com.cmc.rts.dto.response.TaInfoDto;
import com.cmc.rts.entity.Request;
import com.cmc.rts.entity.RequestAssign;
import com.cmc.rts.entity.UserDTO;
import com.cmc.rts.repository.LogExceptionRepository;
import com.cmc.rts.security.AuthenUser;
import com.cmc.rts.service.CandidateService;
import com.cmc.rts.service.RequestService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@RestController
@Slf4j
public class RequestController {
    private static final Logger LOGGER = Logger.getLogger(RequestController.class.getName());
    @Autowired
    private AuthenUser authenUser;
    @Autowired
    RequestService requestService;
    @Autowired
    CandidateService candidateService;
    @Autowired
    LogExceptionRepository logExceptionRepository;

    @PostMapping(value = "/request")
    @Caching(
            evict = {@CacheEvict(value = "requestCache", allEntries = true)}
    )
    public ResponseEntity<?> createRequest(HttpServletRequest httpServletRequest, @RequestBody(required = false) RequestDTO requestDTO) throws Exception {
        Request request = null;
        String exception = requestService.throwException(requestDTO);
        if (exception != null && !exception.isEmpty()) {
            throw new Exception(exception);
        }
        try {
            request = requestService.saveRequest(requestDTO);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        } finally {
            return new ResponseEntity<Request>(request, HttpStatus.OK);
        }

    }

    @PatchMapping(value = "/request/{id}")
    public ResponseEntity<?> updateRequestPatch(HttpServletRequest httpServletRequest, @RequestBody RequestDTO requestDTO, @PathVariable Long id) throws Exception {
        return getResponseEntity(httpServletRequest, requestDTO, id);

    }

    @Caching(
            evict = {@CacheEvict(value = {"requestCache", "requestId"}, allEntries = true)}
    )
    protected ResponseEntity<?> getResponseEntity(HttpServletRequest httpServletRequest, @RequestBody RequestDTO requestDTO, @PathVariable Long id) throws Exception {
        UserDTO userLogin = authenUser.getUser(httpServletRequest);
        String username = userLogin.getUserName();
        requestDTO.setUsername(username);
        String exception = requestService.throwException(requestDTO);
        if (exception != null && !exception.isEmpty()) {
            throw new Exception(exception);
        }

        String validateUpdateRequest = requestService.validateUpdateRequest(id, requestDTO);
        if (!StringUtils.isEmpty(validateUpdateRequest)) {
            //return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
            throw new Exception(validateUpdateRequest);
        }

        Request request = null;
        try {
            request = requestService.updateRequest(id, requestDTO);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        } finally {
            return new ResponseEntity<Request>(request, HttpStatus.OK);
        }
    }

    @PostMapping(value = "/request/{id}")
    @Caching(
            evict = {@CacheEvict(value = {"requestCache", "requestId"}, allEntries = true)}
    )
    public ResponseEntity<?> updateRequestPost(HttpServletRequest httpServletRequest, @RequestBody RequestDTO requestDTO, @PathVariable Long id) throws Exception {
        return getResponseEntity(httpServletRequest, requestDTO, id);
    }

    @GetMapping(value = "/request/{id}")
    @Cacheable(value = "requestId", key = "#id")
    public ResponseEntity<?> findOneRequest(HttpServletRequest httpServletRequest, @PathVariable Long id) {
        UserDTO userLogin = authenUser.getUser(httpServletRequest);
        Request request = requestService.findOne(id);
        return new ResponseEntity<Request>(request, HttpStatus.OK);
    }

    @GetMapping(value = "/request")
    @Cacheable(value = "requestCache", key = "{#offset, #limit, #like, #requestIds, #group, #department, #project, #statusIds, #orderBy, #orderType, #hrIds, #areaIds, #priorityIds, #startDate, #endDate, #languageIds, #startCreate, #endCreate, #name}")
    public ResponseEntity<Page<RequestResponseDTO>> allRequest(HttpServletRequest httpServletRequest, @RequestParam(value = "name", required = false) String name,
                                                               @RequestParam(value = "offset", required = false) Integer offset,
                                                               @RequestParam(value = "limit", required = false) Integer limit,
                                                               @RequestParam(value = "like", required = false) Integer like,
                                                               @RequestParam(value = "requestIds", required = false) String requestIds,
                                                               @RequestParam(value = "group", required = false) String group,
                                                               @RequestParam(value = "department", required = false) String department,
                                                               @RequestParam(value = "project", required = false) String project,
                                                               @RequestParam(value = "statusIds", required = false) String statusIds,
                                                               @RequestParam(value = "orderBy", required = false, defaultValue = "DESC") String orderBy,
                                                               @RequestParam(value = "orderType", required = false, defaultValue = "id") String orderType,
                                                               @RequestParam(value = "hrIds", required = false) String hrIds,
                                                               @RequestParam(value = "areaIds", required = false) String areaIds,
                                                               @RequestParam(value = "priorityIds", required = false) String priorityIds,
                                                               @RequestParam(value = "startDate", required = false) Date startDate,
                                                               @RequestParam(value = "endDate", required = false) Date endDate,
                                                               @RequestParam(value = "languageIds", required = false) String languageIds,
                                                               @RequestParam(value = "startCreate", required = false) Date startCreate,
                                                               @RequestParam(value = "endCreate", required = false) Date endCreate
    ) {
        UserDTO userLogin = authenUser.getUser(httpServletRequest);
        Sort sort = null;
        if (orderBy.equals("ASC")) {
            sort = new Sort(Sort.Direction.ASC, orderType);
        }
        if (orderBy.equals("DESC")) {
            sort = new Sort(Sort.Direction.DESC, orderType);
        }
        Pageable pageable = new PageRequest(offset - 1, limit, sort);
        Page<RequestResponseDTO> requests = requestService.v2SearchRequest(userLogin, requestIds, name, group, department, statusIds,
                areaIds, hrIds, priorityIds, startDate, endDate, languageIds, startCreate, endCreate, pageable);
        return new ResponseEntity<>(requests, HttpStatus.OK);
    }

    @GetMapping(value = "/v2-request")
    public Page<RequestResponseDTO> v2Request(HttpServletRequest httpServletRequest, @RequestParam(value = "name", required = false) String name,
                                                               @RequestParam(value = "offset", required = false) Integer offset,
                                                               @RequestParam(value = "limit", required = false) Integer limit,
                                                               @RequestParam(value = "like", required = false) Integer like,
                                                               @RequestParam(value = "requestIds", required = false) String requestIds,
                                                               @RequestParam(value = "group", required = false) String group,
                                                               @RequestParam(value = "department", required = false) String department,
                                                               @RequestParam(value = "project", required = false) String project,
                                                               @RequestParam(value = "statusIds", required = false) String statusIds,
                                                               @RequestParam(value = "orderBy", required = false, defaultValue = "DESC") String orderBy,
                                                               @RequestParam(value = "orderType", required = false, defaultValue = "id") String orderType,
                                                               @RequestParam(value = "hrIds", required = false) String hrIds,
                                                               @RequestParam(value = "areaIds", required = false) String areaIds,
                                                               @RequestParam(value = "priorityIds", required = false) String priorityIds,
                                                               @RequestParam(value = "startDate", required = false) Date startDate,
                                                               @RequestParam(value = "endDate", required = false) Date endDate,
                                                               @RequestParam(value = "languageIds", required = false) String languageIds,
                                                               @RequestParam(value = "startCreate", required = false) Date startCreate,
                                                               @RequestParam(value = "endCreate", required = false) Date endCreate
    ) {
        UserDTO userLogin = authenUser.getUser(httpServletRequest);
        Sort sort = null;
        if (orderBy.equals("ASC")) {
            sort = new Sort(Sort.Direction.ASC, orderType);
        }
        if (orderBy.equals("DESC")) {
            sort = new Sort(Sort.Direction.DESC, orderType);
        }
        Pageable pageable = new PageRequest(offset - 1, limit, sort);

        return requestService.v2SearchRequest(userLogin, requestIds, name, group, department, statusIds,
                areaIds, hrIds, priorityIds, startDate, endDate, languageIds, startCreate, endCreate, pageable);
    }


    @DeleteMapping(value = "/request")
    @CacheEvict(value = { "requestCache", "requestId"}, allEntries = true)
    public void delete(@RequestBody long[] ids) {
        for (long item : ids) {
            requestService.delete(item);
        }
    }

    @DeleteMapping(value = "/request/{ids}")
    @CacheEvict(value = { "requestCache", "requestId"}, allEntries = true)
    public void delete(@PathVariable String ids) {
        requestService.delete(ids);
    }


    @Caching(
            evict = {@CacheEvict(value = {"requestCache", "requestId"}, allEntries = true)}
    )
    //glead - dulead
    @GetMapping(value = "/request-close/{ids}")
    public ResponseEntity<?> close(HttpServletRequest httpServletRequest, @PathVariable String ids, @RequestParam(value = "userFake", required = false) String userFake) throws Exception {
        String username;
        if (userFake != null && !userFake.isEmpty()) {
            username = userFake;
        } else {
            UserDTO userLogin = authenUser.getUser(httpServletRequest);
            username = userLogin.getUserName();
        }

        List<Request> lstResult = requestService.close(ids, username);
        return new ResponseEntity<List<Request>>(lstResult, HttpStatus.OK);
    }

    @Caching(
            evict = {@CacheEvict(value = {"requestCache", "requestId"}, allEntries = true)}
    )
    @GetMapping(value = "/request-open/{ids}")
    public void open(@PathVariable String ids) {
        requestService.open(ids);
    }

    //glead
    @Caching(
            evict = {@CacheEvict(value = {"requestCache", "requestId"}, allEntries = true)}
    )
    @GetMapping(value = "/request-approve/{ids}")
    public void approveRequest(HttpServletRequest httpServletRequest, @PathVariable String ids) throws Exception {
        UserDTO userLogin = authenUser.getUser(httpServletRequest);
        String username = userLogin.getUserName();
        requestService.approveRequest(ids, username);
    }

    //TA-Manager
    @Caching(
            evict = {@CacheEvict(value = {"requestCache", "requestId"}, allEntries = true)}
    )
    @GetMapping(value = "/request-approve2/{ids}")
    public void approve2Request(HttpServletRequest httpServletRequest, @PathVariable String ids) throws Exception {
        UserDTO userLogin = authenUser.getUser(httpServletRequest);
        String username = userLogin.getUserName();
        requestService.approve2Request(ids, username);
    }

    //glead
    @Caching(
            evict = {@CacheEvict(value = {"requestCache", "requestId"}, allEntries = true)}
    )
    @GetMapping(value = "/request-reject/{ids}")
    public void rejectRequest(HttpServletRequest httpServletRequest, @PathVariable String ids, @RequestParam(value = "reason", required = true) String reason) throws Exception {
        UserDTO userLogin = authenUser.getUser(httpServletRequest);
        String username = userLogin.getUserName();
        requestService.rejectRequest(ids, reason, username);
    }

    //du
    @Caching(
            evict = {@CacheEvict(value = {"requestCache", "requestId"}, allEntries = true)}
    )
    @GetMapping(value = "/request-submit/{ids}")
    public ResponseEntity<List<Request>> submitRequest(HttpServletRequest httpServletRequest,
                                                       @PathVariable String ids,
                                                       @RequestParam(value = "receiver", required = false) String receiver) throws Exception {
        UserDTO userSentDTO = authenUser.getUser(httpServletRequest);
        List<Request> lstresult = requestService.submitRequest(ids, receiver, userSentDTO);
        return new ResponseEntity<>(lstresult, HttpStatus.OK);
    }

    //hr
    @PostMapping(value = "/request/{id}/assign")
    @Caching(
            evict = {@CacheEvict(value = {"requestCache", "requestId"}, allEntries = true)}
    )
    public ResponseEntity<List<RequestAssign>> createRequestAssign(@RequestBody List<CreateAssignRequest> request,
                                                                   @PathVariable Long id) throws MessagingException {
        List<RequestAssign> data = requestService.createRequestAssign(id, request);
        return new ResponseEntity(data, HttpStatus.OK);
    }

    @PostMapping(value = "/request-change-created-by/{id}")
    @PreAuthorize("hasAnyAuthority(T(com.cmc.rts.utils.Constants.ROLE).ROLE_TA_MANAGER)")
    @Caching(
            evict = {@CacheEvict(value = {"requestCache", "requestId"}, allEntries = true)}
    )
    public ResponseEntity<List<RequestAssign>> updateRequestCreatedBy(@RequestBody Map<String, String> payload,
                                                                      @PathVariable Long id) throws MessagingException {
        String newCreatedBy = payload.get("createdBy");
        return new ResponseEntity(requestService.updateRequestCreatedBy(id, newCreatedBy), HttpStatus.OK);
    }

    @GetMapping(value = "/request-history/{id}")
    public ResponseEntity<RequestProcessDTO> getRequestHistory(@RequestParam(value = "id", required = true) Long id) {
        RequestProcessDTO result = requestService.getRequestHistory(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(value = "/hr-member")
    public ResponseEntity<HRMemberRootResponse> getAllHRMember() {
        HRMemberRootResponse hrMemberRootResponse = requestService.getAllHRMemberActive();
        return new ResponseEntity<>(hrMemberRootResponse, HttpStatus.OK);
    }

    @GetMapping(value = "/hr-member-lead")
    public ResponseEntity<HRMemberRootResponse> getAllHRMemberHRLead() {
        HRMemberRootResponse hrMemberRootResponse = requestService.getAllHRMemberHRLeadActive();
        return new ResponseEntity<>(hrMemberRootResponse, HttpStatus.OK);
    }

    @GetMapping(value = "/hr-lead")
    public ResponseEntity<HRMemberRootResponse> getAllHRLeadMember() {
        HRMemberRootResponse hrMemberRootResponse = requestService.getAllHRLeadActive();
        return new ResponseEntity<>(hrMemberRootResponse, HttpStatus.OK);
    }

    @GetMapping(value = "/get-glead")
    public ResponseEntity<GLeadResponse> getGlead(@RequestParam(value = "username", required = true) String username) {
        GLeadResponse gLeadResponse = requestService.getGleadByDulead(username);
        return new ResponseEntity<>(gLeadResponse, HttpStatus.OK);
    }

    @PutMapping(value = "/request/{id}")
    @Caching(
            evict = {@CacheEvict(value = {"requestCache", "requestId"}, allEntries = true)}
    )
    public ResponseEntity<Request> updateShareJob(@PathVariable("id") Long id) {
        Request request = requestService.updateShareJob(id);
        return new ResponseEntity<>(request, HttpStatus.OK);
    }

    @PostMapping(value = "/reassigned")
    @Caching(
            evict = {@CacheEvict(value = {"requestCache", "requestId"}, allEntries = true)}
    )
    public ResponseEntity<?> reassigned(@RequestBody ReassignedDTO reAssignDTO) {
        return new ResponseEntity<>(requestService.reassignRequest(reAssignDTO.getId(),
                reAssignDTO.getName(), reAssignDTO.getTarget()), HttpStatus.OK);
    }

    //Get all Id request
    @GetMapping(value = "/request/getId")
    public ResponseEntity<?> getAllIdRequests(@RequestParam(value = "id") String id) {
        return new ResponseEntity<>(requestService.getAllIdRequests(id), HttpStatus.OK);
    }

    @GetMapping(value = "/get-ta/{id}")
    public ResponseEntity<List<TaInfoDto>> getTaInfo(@RequestParam(value = "id", required = true) Long requestId) {
        List<TaInfoDto> taInfoDtoList = requestService.getTaInfoByRequestId(requestId);
        return new ResponseEntity<>(taInfoDtoList, HttpStatus.OK);
    }
}
