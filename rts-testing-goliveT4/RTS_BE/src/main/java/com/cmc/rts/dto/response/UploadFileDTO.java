package com.cmc.rts.dto.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Create by trungtx
 * Date: 11/10/2021
 * Time: 12:59 AM
 * Project: CMC_RTS
 */

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UploadFileDTO {

    private String message;
    private String totalFiles;
    private List<FileDTO> files;

    public UploadFileDTO() {
    }

    public UploadFileDTO(String message) {
        this.message = message;
    }
}
