package com.cmc.rts.rest;

import com.cmc.rts.dto.requestDto.RequestUpdateDto;
import com.cmc.rts.dto.response.CandidateResponse;
import com.cmc.rts.dto.response.CandidateRootResponse;
import com.cmc.rts.entity.CandidateStateEnum;
import com.cmc.rts.entity.HistoryMoveLog;
import com.cmc.rts.service.CandidateService;
import com.cmc.rts.service.HistoryService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Caching;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.*;

@RestController
@PropertySource("classpath:application-dev.properties")
@Slf4j
public class CandidateController {
    private final ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private CandidateService candidateService;

    @Autowired
    private HistoryService historyService;

    @PostMapping(value = "/candidate/search")
    public ResponseEntity<CandidateRootResponse> allCandidate(@RequestParam Long requestId,
                                                              @RequestParam CandidateStateEnum candidateState,
                                                              @RequestParam(required = false) String searchString,
                                                              @RequestParam Integer limit,
                                                              @RequestParam Integer offset,
                                                              @RequestParam(required = false) String hasCV,
                                                              @RequestParam(required = false) Date startImportDate,
                                                              @RequestParam(required = false) Date endImportDate,
                                                              @RequestParam(required = false) String lastCompany
                                                              ){
        if (searchString == null || searchString.isEmpty()) {
            searchString = "";
        }
        searchString = searchString.trim();
        CandidateRootResponse candidateDTOS = candidateService.searchSource(requestId, candidateState, searchString,
                offset, limit, hasCV, startImportDate, endImportDate, lastCompany);
        return new ResponseEntity<>(candidateDTOS, HttpStatus.OK);
    }

    @GetMapping(value = "/candidate/{id}")
    public ResponseEntity<CandidateResponse> getCandidateByID(@PathVariable("id") Long id) throws IOException {
        CandidateResponse candidate = candidateService.findCandidateById(id);
        return new ResponseEntity<>(candidate, HttpStatus.OK);
    }

    @GetMapping(value = "/history/{candidateId}")
    public  ResponseEntity<?> getHistoryCandidateByID(@PathVariable("candidateId") Long candidateId) throws IOException{
        List<HistoryMoveLog> historyMoveLog = historyService.getHistoryByCandidateId(candidateId);
        return new ResponseEntity(historyMoveLog, HttpStatus.OK);
    }

    @PutMapping("/candidate/{ids}")
    @Caching(
            evict = {@CacheEvict(value = {"requestCache", "requestId"}, allEntries = true)}
    )
    public ResponseEntity<CandidateResponse[]> updateCandidateState(@PathVariable("ids") String ids,@RequestParam("candidateStateCurrent") String stateCurrent,
                                                     @RequestBody RequestUpdateDto request) throws Exception {
        CandidateResponse[] candidates = candidateService.updateCandidateState(ids,stateCurrent, request.getCandidateStateEnum(), request.getRequestId(), request.getAssignees(), request.getTa());
        return new ResponseEntity<>(candidates, HttpStatus.OK);
    }

    @DeleteMapping(value = "/candidate/{ids}")
    @CacheEvict(value = {"requestCache", "requestId"}, allEntries = true)
    public ResponseEntity<CandidateResponse[]> deleteCandidate(@PathVariable("ids") String ids,
                                                             @RequestParam Long requestId) throws IOException {
        CandidateResponse[] candidates = candidateService.deleteCandidate(ids, requestId);
        return new ResponseEntity<>(candidates, HttpStatus.OK);
    }

    @PostMapping("/getTotalCandidateInStepByIdRequest")
    public ResponseEntity<?> getTotalCandidateInStepByIdRequest(@RequestBody List<String> request){
        return new ResponseEntity<>(candidateService.getTotalCandidateInStepByIdRequest(request), HttpStatus.OK);
    }

    @PostMapping("/getTotalCandidateInStepInTAByIdRequest")
    public ResponseEntity<?> getTotalCandidateInStepInTAByIdRequest(@RequestBody List<String> ta){
        return new ResponseEntity<>(candidateService.getTotalCandidateInStepInTAByIdRequest(ta), HttpStatus.OK);
    }


}
