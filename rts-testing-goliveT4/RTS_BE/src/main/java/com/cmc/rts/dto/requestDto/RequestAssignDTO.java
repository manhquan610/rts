package com.cmc.rts.dto.requestDto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class RequestAssignDTO {
    private Long id;
    private Integer quantity;
    private String Assignee;
    private String ldap;

//    public RequestAssignDTO(Long id, Integer quantity, String assignee) {
//        this.id = id;
//        this.quantity = quantity;
//        Assignee = assignee;
//    }

    public RequestAssignDTO(Long id, Integer quantity, String ldap) {
        this.id = id;
        this.quantity = quantity;
        this.ldap = ldap;
    }
}
