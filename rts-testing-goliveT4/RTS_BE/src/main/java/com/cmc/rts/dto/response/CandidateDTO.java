package com.cmc.rts.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.Date;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CandidateDTO {

    private Long id;

    private String fullName;

    private String createdDate;

    private String createBy;

    public CandidateDTO(Long id, String fullName, Date createdDate, String createBy) {
        this.id = id;
        this.fullName = fullName;
        this.createdDate = createdDate.toInstant().toString();
        this.createBy = createBy;
    }
}
