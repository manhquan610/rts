package com.cmc.rts.repository.v2;

import com.cmc.rts.entity.RequestStatusEnum;
import com.cmc.rts.entity.v2.V2Request;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;

import javax.persistence.QueryHint;
import java.sql.Date;
import java.util.List;

/**
 * @author: nthieu10
 * 7/25/2022
 **/
public interface V2RequestRepository extends JpaRepository<V2Request, Long> {


    @QueryHints(value = { @QueryHint(name = org.hibernate.jpa.QueryHints.HINT_CACHEABLE, value = "true")})
    @Query(value = "select distinct r " +
            "from V2Request r " +
            "where (:requestNotEmpty = false or r.id in :requestIds ) " +
            "and (:nameNotEmpty = false or  r.name like %:name% ) " +
            "and (:groupNotEmpty = false or  r.managerGroupId in :groupIds ) " +
            "and (:departmentNotEmpty = false  or r.departmentId in :departmentIds ) " +
            "and (:statusNotEmpty = false or r.requestStatusName in :statuses ) " +
            "and (:areaNotEmpty = false  or r.areaId in :areaIds) " +
            "and (:priorityNotEmpty = false or r.managerPriorityId in :priority ) " +
            "and ( r.deadlineStr between :startDate and :endDate ) " +
            "and (:languageNotEmpty = false or r.languageId in :language ) " +
            "and ( r.createdDate between :startCreate and :endCreate) ")
    Page<V2Request> findAllBySearchParams(@Param("requestIds") List<Long> requestIds,
                                          @Param("name") String name,
                                          @Param("groupIds") List<Long> groupIds,
                                          @Param("departmentIds") List<Long> departmentIds,
                                          @Param("statuses") List<RequestStatusEnum> statuses,
                                          @Param("areaIds") List<Long> areaIds,
                                          @Param("priority") List<Long> priority,
                                          @Param("startDate") String startDate,
                                          @Param("endDate") String endDate,
                                          @Param("language") List<String> language,
                                          @Param("startCreate") Date startCreate,
                                          @Param("endCreate") Date endCreate,
                                          @Param("requestNotEmpty") Boolean requestNotEmpty,
                                          @Param("nameNotEmpty") Boolean nameNotEmpty,
                                          @Param("groupNotEmpty") Boolean groupNotEmpty,
                                          @Param("departmentNotEmpty") Boolean departmentNotEmpty,
                                          @Param("statusNotEmpty") Boolean statusNotEmpty,
                                          @Param("areaNotEmpty") Boolean areaNotEmpty,
                                          @Param("priorityNotEmpty") Boolean priorityNotEmpty,
                                          @Param("languageNotEmpty") Boolean languageNotEmpty,
                                          Pageable pageable);

    @QueryHints(value = { @QueryHint(name = org.hibernate.jpa.QueryHints.HINT_CACHEABLE, value = "true")})
    @Query(value = "select distinct r " +
            "from V2Request r " +
            "where (:requestNotEmpty = false or r.id in :requestIds ) " +
            "and (:nameNotEmpty = false or  r.name like %:name% ) " +
            "and (:groupNotEmpty = false or  r.managerGroupId in :groupIds ) " +
            "and (:departmentNotEmpty = false  or r.departmentId in :departmentIds ) " +
            "and (:statusNotEmpty = false or r.requestStatusName in :statuses ) " +
            "and (:areaNotEmpty = false  or r.areaId in :areaIds) " +
            "and (:hrNotEmpty = false  or r.createdBy in :hrLdaps)" +
            "and (:priorityNotEmpty = false or r.managerPriorityId in :priority ) " +
            "and ( r.deadlineStr between :startDate and :endDate ) " +
            "and (:languageNotEmpty = false or r.languageId in :language ) " +
            "and ( r.createdDate between :startCreate and :endCreate) ")
        // query for request screen as TA Manager and TA Lead
    Page<V2Request> findAllBySearchParamsRoleTa(@Param("requestIds") List<Long> requestIds,
                                              @Param("name") String name,
                                              @Param("groupIds") List<Long> groupIds,
                                              @Param("departmentIds") List<Long> departmentIds,
                                              @Param("statuses") List<RequestStatusEnum> statuses,
                                              @Param("areaIds") List<Long> areaIds,
                                              @Param("hrLdaps") List<String> hrLdaps,
                                              @Param("priority") List<Long> priority,
                                              @Param("startDate") String startDate,
                                              @Param("endDate") String endDate,
                                              @Param("language") List<String> language,
                                              @Param("startCreate") Date startCreate,
                                              @Param("endCreate") Date endCreate,
                                              @Param("requestNotEmpty") Boolean requestNotEmpty,
                                              @Param("nameNotEmpty") Boolean nameNotEmpty,
                                              @Param("groupNotEmpty") Boolean groupNotEmpty,
                                              @Param("departmentNotEmpty") Boolean departmentNotEmpty,
                                              @Param("statusNotEmpty") Boolean statusNotEmpty,
                                              @Param("areaNotEmpty") Boolean areaNotEmpty,
                                              @Param("hrNotEmpty") Boolean hrNotEmpty,
                                              @Param("priorityNotEmpty") Boolean priorityNotEmpty,
                                              @Param("languageNotEmpty") Boolean languageNotEmpty,
                                              Pageable pageable);

    @Query(value = "SELECT r FROM V2Request r WHERE( r.deadline BETWEEN :startTime AND :endTime)" +
            "and (:statusNotEmpty = false or r.requestStatusName in :statuses ) " +
            " ORDER BY r.managerGroupId, r.departmentId ASC")
    Page<V2Request> findAllSortedByDate(@Param("startTime") String startTime, @Param("endTime") String endTime, @Param("statuses") List<RequestStatusEnum> statuses, @Param("statusNotEmpty") Boolean statusNotEmpty, Pageable pageable);

    @Query(value = "SELECT r FROM V2Request r where (:statusNotEmpty = false or r.requestStatusName in :statuses ) " +
            " ORDER BY r.managerGroupId, r.departmentId ASC")
    Page<V2Request> findAllSorted(@Param("statuses") List<RequestStatusEnum> statuses, @Param("statusNotEmpty") Boolean statusNotEmpty, Pageable pageable);

    @Query(value = "SELECT r FROM V2Request r WHERE( r.deadline BETWEEN :startTime AND :endTime)" +
            "and (:statusNotEmpty = false or r.requestStatusName in :statuses ) " +
            " ORDER BY r.managerGroupId, r.departmentId ASC")
    List<V2Request> findAllSortedByDate(@Param("startTime") java.util.Date startTime, @Param("endTime") java.util.Date endTime, @Param("statuses") List<RequestStatusEnum> statuses, @Param("statusNotEmpty") Boolean statusNotEmpty);

    @Query(value = "SELECT r FROM V2Request r where (:statusNotEmpty = false or r.requestStatusName in :statuses ) " +
            " ORDER BY r.managerGroupId, r.departmentId ASC")
    List<V2Request> findAllSorted(@Param("statuses") List<RequestStatusEnum> statuses, @Param("statusNotEmpty") Boolean statusNotEmpty);

    V2Request findV2RequestById(Long id);
    List<V2Request> findV2RequestByIdIn(List<Long> ids);
}
