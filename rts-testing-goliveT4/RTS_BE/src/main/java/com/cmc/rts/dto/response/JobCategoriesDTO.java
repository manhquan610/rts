package com.cmc.rts.dto.response;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JobCategoriesDTO {
    private String avatar;
    private String name;
    private Long openPosition;
    private Long id;

    public JobCategoriesDTO(String name, Long openPosition, Long id) {
        this.avatar = "https://www.cmc.com.vn/main/imgs/logo.svg";
        this.name = name;
        this.openPosition = openPosition;
        this.id = id;
    }
}
