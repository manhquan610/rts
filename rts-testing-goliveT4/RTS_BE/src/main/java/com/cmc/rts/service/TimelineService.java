package com.cmc.rts.service;

import com.cmc.rts.dto.response.HistoryMoveLogDTO;
import com.cmc.rts.dto.response.TimelineDTO;

import java.util.List;

public interface TimelineService {
    TimelineDTO geTimeline(Long requestId, Long candidateId);
    List<HistoryMoveLogDTO> getHistoryMoveLogCandidate(long candidateId);
}
