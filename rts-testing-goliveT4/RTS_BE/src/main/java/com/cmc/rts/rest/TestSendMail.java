package com.cmc.rts.rest;


import com.cmc.rts.service.RtsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;

@RestController(value = "/mail")
public class TestSendMail {

    @Autowired
    private RtsService rtsService;

    @GetMapping(value = "/send")
    public ResponseEntity<?> send() throws MessagingException {
        return new ResponseEntity<>(rtsService.sendHtmlMessage("txtrung@cmcglobal.vn",null,
                "Sent Test RTS", "Hello Trung"), HttpStatus.OK);
    }
}
