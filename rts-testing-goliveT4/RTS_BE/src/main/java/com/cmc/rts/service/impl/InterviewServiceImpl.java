package com.cmc.rts.service.impl;

import com.cmc.rts.CmcRTSApplication;
import com.cmc.rts.dto.LocationDTO;
import com.cmc.rts.dto.request.InterviewRequest;
import com.cmc.rts.dto.response.InterviewDTO;
import com.cmc.rts.dto.response.ResponseAPI;
import com.cmc.rts.dto.response.TimelineInterviewDTO;
import com.cmc.rts.dto.response.UserRootResponse;
import com.cmc.rts.entity.HistoryMoveLog;
import com.cmc.rts.entity.Interview;
import com.cmc.rts.entity.Request;
import com.cmc.rts.exception.CustomException;
import com.cmc.rts.repository.InterviewRepository;
import com.cmc.rts.repository.LocationRepository;
import com.cmc.rts.repository.RequestRepository;
import com.cmc.rts.service.InterviewService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.time.DateUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class InterviewServiceImpl implements InterviewService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    InterviewRepository interviewRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private RequestRepository requestRepository;

    private final String interviewerToken = "Bearer eyJTeXN0ZW0iOiJSVFMiLCJLZXkiOiJHeGFUYTVTK0lSMGx0OTBJYmZnQVJRPT06Q29CMG91OWZmOHptMCswQ2Z3S0Q5dz09In0=";
    @Value("${cmc.global.searchUser}")
    private String interviewerApiUrl;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public List<InterviewDTO> getAllInterview(Long request_id) {
        List<InterviewDTO> interviewDTOS = interviewRepository.getAllInterview(request_id);
        return interviewDTOS;
    }

    @Override
    public Page<InterviewDTO> searchInterview(String searchValue, String location, String interviewer, Boolean status,
                                              Long request_id, String ids, Pageable pageable) {
        if(searchValue == null) {
            searchValue = "";
        }
        if(interviewer == null){
            interviewer = "";
        }
        if(location == null){
            location = "";
        }
        return interviewRepository.searchInterview(searchValue, location, interviewer, status, request_id, pageable);
    }

    @Override
    public List<LocationDTO> getAllLocationForInterview() {
        return locationRepository.getAllLocation();
    }

    @Override
    public List<InterviewDTO> getInterviewByCandidateIdAndRequestId(Long candidate_id, Long request_id) {
        List<InterviewDTO> interviewDTO = interviewRepository.getInterviewByCandidateIdAndRequestId(candidate_id, request_id);
        return interviewDTO;
    }

    @Override
    public UserRootResponse searchUser(String name) {
        String interviewerUrl = interviewerApiUrl + "?name=" + name
                +"&pageSize=10000&pageIndex=0";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", interviewerToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity httpEntity = new HttpEntity(headers);
        ResponseEntity<UserRootResponse> response = restTemplate.exchange(interviewerUrl, HttpMethod.GET, httpEntity,  UserRootResponse.class);
        return response.getBody();
    }

    @Override
    public int createInterview(InterviewRequest requestInterview) throws Exception {
        if (!validateJavaDate(requestInterview.getStartTime())) {
            throw new CustomException("startDate is Invalid Date format: yyyy-MM-dd HH:mm:ss", HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
        List<Interview> interviewList = null;
        try {
            interviewList = interviewRepository.checkDuplicateCandidate(requestInterview.getCandidateId(), convertStringToDate(requestInterview.getStartTime()));
        } catch (Exception e){
            interviewList = interviewRepository.checkDuplicateCandidateH2(requestInterview.getCandidateId(), convertStringToDate(requestInterview.getStartTime()));
        }
        if (!interviewList.isEmpty()){
            throw new CustomException("duplicate the candidate's interview schedule", HttpStatus.CONFLICT.value());
        }
        try {
            Interview interview = new Interview();
            Request request = requestRepository.findById(requestInterview.getRequestId());
            interview.setCandidateId(requestInterview.getCandidateId());
            interview.setRequest(request);
            interview.setTitle(requestInterview.getTitle());
            interview.setStartTime(convertStringToDate(requestInterview.getStartTime()));
            interview.setInterviewer(requestInterview.getInterviewer());
            interview.setComment(requestInterview.getComment());
            interview.setCreatedBy(requestInterview.getCreatedBy());
            interviewRepository.save(interview);
            return 200;
        } catch (Exception e){
            throw new CustomException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }

    @Override
    public void updateInterview(InterviewRequest request) throws Exception {
        if(request.getRequestId() == null || request.getId() == null || request.getCandidateId() == null){
            throw new CustomException("Request Id, Candidate Id và Id không được phép null", HttpStatus.FORBIDDEN.value());
        }
        Interview interview = interviewRepository.findOne(request.getId());
        if(interview == null){
            throw new CustomException("Không tìm thấy thông tin của inteview", HttpStatus.NOT_FOUND.value());
        }
        if (!validateJavaDate(request.getStartTime())) {
            throw new CustomException("startDate is Invalid Date format: yyyy-MM-dd HH:mm:ss", HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
        interview.setTitle(request.getTitle());
        interview.setStartTime(convertStringToDate(request.getStartTime()));
        interview.setInterviewer(request.getInterviewer());
        interview.setComment(request.getComment());
        interviewRepository.save(interview);
    }

    @Override
    public List<TimelineInterviewDTO> getAllInterviewOneWeek(String startDate, String endDate, String textSearch) {
        List<TimelineInterviewDTO> interviewDTOS = new ArrayList<>();
        List<Interview> interviewList ;
        if (startDate == null){
            throw new CustomException("start date is not null", HttpStatus.NOT_FOUND.value());
        }
        if (endDate == null){
            throw new CustomException("end date is not null", HttpStatus.NOT_FOUND.value());
        }
        startDate = startDate+" 00:00:00";
        endDate = endDate+" 23:59:59";
        if (!validateJavaDate(startDate) || !validateJavaDate(endDate)) {
            throw new CustomException("startDate or endDate is Invalid Date format: yyyy-MM-dd HH:mm:ss", HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
        if (textSearch != null) {
            textSearch = textSearch.toLowerCase();
        }
        try {
            interviewList = interviewRepository.getAllInterviewOneWeek(
                    convertStringToDate(startDate),
                    convertStringToDate(endDate),
                    textSearch);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
        if (interviewList.isEmpty()) {
            throw new CustomException("list interview is null", HttpStatus.NOT_FOUND.value());
        }

        for (Interview interview: interviewList) {
            interviewDTOS.add(new TimelineInterviewDTO(interview.getTitle(), interview.getStartTime(), interview.getEndTime(),
                    interview.getRequest().getId(),interview.getCandidateId(),interview.getInterviewer(),interview.getCreatedBy()));
        }
        return interviewDTOS;
    }

    public Date convertStringToDate(String dateStr) throws Exception {
        SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return formatter.parse(dateStr);
    }

    public boolean validateJavaDate(String strDate) {
        if (!strDate.trim().equals("")) {
            SimpleDateFormat sdfrmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdfrmt.setLenient(false);
            try {
                sdfrmt.parse(strDate);
            } catch (ParseException e) {
                return false;
            }
        }
        return true;
    }

}
