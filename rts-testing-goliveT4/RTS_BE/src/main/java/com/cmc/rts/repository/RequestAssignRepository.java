package com.cmc.rts.repository;

import com.cmc.rts.dto.requestDto.RequestAssignDTO;
import com.cmc.rts.dto.response.TaReportGetDuDTO;
import com.cmc.rts.entity.RequestAssign;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface RequestAssignRepository extends JpaRepository<RequestAssign, Long> {
    @Query(value = "select new com.cmc.rts.dto.requestDto.RequestAssignDTO(r.id, r.target, r.assignLdap) " +
            "from RequestAssign r where r.request.id = :id ")
    List<RequestAssignDTO> findAssignByRequestId(@Param(value = "id") Long id);

    RequestAssign findRequestAssignByAssignLdapAndRequestId(String ldap, Long requestId);

    @Query(value = "select r from RequestAssign r where r.assignLdap in :ta ")
    List<RequestAssign> findAssignByTA(@Param(value = "ta") List<String> ta);

    @Query(value = "select distinct r from RequestAssign r ")
    List<RequestAssign> getDistinctByTAndAssignLdap();

    List<RequestAssign> findByRequestId(Long requestId);


//    @Query(value = "SELECT ra.assign_ldap as assignLdap , ra.id as requestAssignId , r.id as requestId , r.department_id as departmentId , r.manager_group_id as managerGroupId \n" +
//            "FROM request_assign as ra \n" +
//            "join request as r on r.id = ra.request_id \n" +
//            "where ra.assign_ldap in :ta \n" +
//            "ORDER BY r.department_id ASC " , nativeQuery = true)

    @Query(value = "SELECT ra.assignLdap as assignLdap , ra.id as requestAssignId , r.id as requestId , r.departmentId as departmentId , r.managerGroupId as managerGroupId, \n" +
            "r.name as requestName, r.requestStatusName as requestStatusName, r.deadline as deadline, r.number as number, r.skillNameId as skillNameId, r.area.id as areaId, r.skillNameId as skillId \n" +
            "FROM RequestAssign as ra \n" +
            "join ra.request as r \n" +
            "where ra.assignLdap in :ta \n" +
            "ORDER BY ra.assignLdap, r.departmentId, r.id ASC ")
    List<TaReportGetDuDTO> findAllTaReportSorted(@Param("ta") List<String> ta);

    @Query(value = "SELECT ra.assignLdap as assignLdap , ra.id as requestAssignId , r.id as requestId , r.departmentId as departmentId , r.managerGroupId as managerGroupId, \n" +
            "r.name as requestName, r.requestStatusName as requestStatusName, r.deadline as deadline, r.number as number, r.skillNameId as skillNameId, r.area.id as areaId, r.skillNameId as skillId \n" +
            "FROM RequestAssign as ra \n" +
            "JOIN ra.request as r \n" +
            "WHERE ra.assignLdap in :ta AND r.deadlineStr BETWEEN :start AND :end \n" +
            "ORDER BY ra.assignLdap, r.departmentId, r.id ASC ")
    List<TaReportGetDuDTO> findAllTaReportSortedByDate(@Param("ta") List<String> ta, @Param("start") String start, @Param("end") String end);

    @Query(value = "SELECT m " +
            "FROM V2RequestAssign m " +
            "where (:managerPriorityIdNotEmpty = false or m.id in :managerPriorityIds ) ")
    List<RequestAssign> findRequestAssignsByParam(List<Long> finalRequestIds, boolean b);
}
