package com.cmc.rts.rest;

import com.cmc.rts.service.SF4CService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sf4c/")
public class SF4CController {

    public SF4CService sf4c;

    @Autowired
    public SF4CController(SF4CService sf4c) {
        this.sf4c = sf4c;
    }

    @GetMapping("userID")
    public ResponseEntity<?> getUserId(@RequestParam(value = "username") String username) {
        return new ResponseEntity<>(sf4c.getUserId(username), HttpStatus.OK);
    }

    @GetMapping("position/code")
    public ResponseEntity<?> positionCode(@RequestParam(value = "userID") String userID) {
        return new ResponseEntity<>(sf4c.getCodePosition(userID), HttpStatus.OK);
    }

    @GetMapping("position/child")
    public ResponseEntity<?> positionChild(@RequestParam(value = "parentPosition") String parentPosition) {
        return new ResponseEntity<>(sf4c.getChildCodePosition(parentPosition), HttpStatus.OK);
    }

    @GetMapping("position/detail")
    public ResponseEntity<?> positionDetail(@RequestParam(value = "parentPosition") String positionNav) {
        return new ResponseEntity<>(sf4c.getDetailFTECodePosition(positionNav), HttpStatus.OK);
    }

    @GetMapping("position/list")
    public ResponseEntity<?> positionList() {
        return new ResponseEntity<>(sf4c.getListPosition(), HttpStatus.OK);
    }
}
