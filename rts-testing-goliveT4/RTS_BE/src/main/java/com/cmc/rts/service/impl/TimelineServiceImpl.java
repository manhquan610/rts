package com.cmc.rts.service.impl;

import com.cmc.rts.dto.response.*;
import com.cmc.rts.entity.CandidateStateEnum;
import com.cmc.rts.entity.HistoryMoveLog;
import com.cmc.rts.repository.CandidateLogRepository;
import com.cmc.rts.repository.CommentRepository;
import com.cmc.rts.repository.HistoryRepository;
import com.cmc.rts.repository.InterviewRepository;
import com.cmc.rts.service.TimelineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.*;
import java.util.*;

@Service
public class TimelineServiceImpl implements TimelineService {
    @Autowired
    private InterviewRepository interviewRepository;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private HistoryRepository historyRepository;

    @Autowired
    private CandidateLogRepository candidateLogRepository;

//    @Autowired
//    private CandidateTimeLineMapper mapper;


    @Override
    public TimelineDTO geTimeline(Long requestId, Long candidateId) {
        List<InterviewDTO> interviewDTOs = interviewRepository.getInterviewByCandidateIdAndRequestId(requestId,candidateId);
        List<HistoryMoveLog> historyMoveLogs = historyRepository.getAllHistory(requestId,candidateId);
        List<CommentDTO> commentDTOs = commentRepository.getAllCommentsForTimeline(requestId,candidateId);
//        List<CandidateLogDTO> candidateLogDTOS = candidateLogRepository.getAllCandidate(requestId,candidateId);
        List<CandidateLogDTO> candidateLogDTOS = new ArrayList<>();

        List<HistoryDeleteDTO> historyDeleteds  = new ArrayList<>();
        for(HistoryMoveLog index : historyMoveLogs){
            if(index.getCandidateAfter() == CandidateStateEnum.none){
                HistoryDeleteDTO historyDeleteDTO = new HistoryDeleteDTO();
                historyDeleteDTO.setId(index.getId());
                historyDeleteDTO.setCreatedBy(index.getCreatedBy());
                historyDeleteDTO.setCreatedDate(index.getCreatedDate());
                historyDeleteds.add(historyDeleteDTO);
            }
        }
        return new TimelineDTO(interviewDTOs,commentDTOs,historyMoveLogs,historyDeleteds,candidateLogDTOS);
    }

    @Override
    public List<HistoryMoveLogDTO> getHistoryMoveLogCandidate(long candidateId) {
        List<HistoryMoveLogDTO> responses = new ArrayList<>();
        List<HistoryMoveLog> historyMoveLogs = historyRepository.findByCandidateId(candidateId);
        for (HistoryMoveLog historyMoveLog : historyMoveLogs) {
            HistoryMoveLogDTO dto = new HistoryMoveLogDTO();
            dto.setCandidateId(historyMoveLog.getCandidateId());
            dto.setRequestId(historyMoveLog.getRequestId());
            dto.setCreatedBy(historyMoveLog.getCreatedBy());
            dto.setCandidateBefore(stepRts(historyMoveLog.getCandidateBefore()));
            dto.setCandidateAfter(stepRts(historyMoveLog.getCandidateAfter()));
            dto.setCreatedDate(timestampByTimezone(historyMoveLog.getCreatedDate()));
            responses.add(dto);
        }
        return responses;
    }

    private String stepRts(CandidateStateEnum step) {
        String stepRts = null;
        switch (step){
            case none:
                stepRts = "Source";
                break;
            case qualify:
                stepRts = "Applicant";
                break;
            case confirm:
                stepRts = "Qualify";
                break;
            case interview:
                stepRts = "Interview";
                break;
            case offer:
                stepRts = "Offer";
                break;
            case onboard:
                stepRts = "Onboard";
                break;
            case failed:
                stepRts = "Failed";
                break;
        }
        return stepRts;
    }

    private Timestamp timestampByTimezone(Timestamp timestamp) {
        ZoneId zone = ZoneId.of("Asia/Ho_Chi_Minh");
        OffsetDateTime offsetDateTime = ZonedDateTime
                .ofInstant(Instant.ofEpochMilli(timestamp.getTime()), zone)
                .toOffsetDateTime();
        return Timestamp.from(offsetDateTime.toInstant().plusSeconds(25200));
    }
}
