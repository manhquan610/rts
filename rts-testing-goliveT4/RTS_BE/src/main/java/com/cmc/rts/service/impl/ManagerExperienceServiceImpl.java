package com.cmc.rts.service.impl;

import com.cmc.rts.dto.request.ListNameRequest;
import com.cmc.rts.dto.request.NameRequest;
import com.cmc.rts.dto.response.ExperienceDTO;
import com.cmc.rts.entity.ManagerExperience;
import com.cmc.rts.repository.ManagerExperienceRepository;
import com.cmc.rts.service.ManagerExperienceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class ManagerExperienceServiceImpl  implements ManagerExperienceService {
    @Autowired
    ManagerExperienceRepository managerExperienceRepository;

    @Override
    @Transactional
    public ManagerExperience addManagerExperience(ManagerExperience managerExperience) {
        return managerExperienceRepository.save(managerExperience);
    }

    @Override
    public ExperienceDTO findAll(NameRequest nameRequest) {
        ExperienceDTO experienceDTO = new ExperienceDTO();
        String lstName = "";
        if(nameRequest !=null && nameRequest.getLstName() !=null){
            lstName = nameRequest.getLstName();
        }
        Integer offset = null;
        if(nameRequest !=null && nameRequest.getOffset() !=null){
            offset = nameRequest.getOffset();
        }
        Integer limit = null;
        if(nameRequest !=null && nameRequest.getLimit()!=null){
            limit = nameRequest.getLimit();
        }

        Integer first = 0;
        if(offset !=null && limit !=null){
            first = (offset-1)*limit;
        }else {
            limit = 10;
        }
        experienceDTO.setManager(managerExperienceRepository.findByNameSame(first,limit,lstName));
        Integer total_item = (managerExperienceRepository.findByNameSame(null,null,lstName)).size();
        Integer total_page = 0;
        if(limit !=0){
            total_page = total_item / limit ;
            if((total_item % limit) != 0){
                total_page = total_page + 1;
            }
        }
        experienceDTO.setTotal_item(total_item);
        experienceDTO.setTotal_page(total_page);
        return experienceDTO;
    }

    @Override
    public ExperienceDTO findAll(ListNameRequest listNameRequest) {
        ExperienceDTO experienceDTO = new ExperienceDTO();
        List<String> lstName = new ArrayList<>();
        if(listNameRequest !=null && listNameRequest.getLstName() !=null){
            lstName = listNameRequest.getLstName();
        }
        Integer offset = null;
        if(listNameRequest !=null && listNameRequest.getOffset() !=null){
            offset = listNameRequest.getOffset();
        }
        Integer limit = null;
        if(listNameRequest !=null && listNameRequest.getLimit()!=null){
            limit = listNameRequest.getLimit();
        }

        Integer first = 0;
        if(offset !=null && limit !=null){
            first = (offset-1)*limit;
        }else {
            limit = 10;
        }
        experienceDTO.setManager(managerExperienceRepository.findByName(first,limit,lstName));
        Integer total_item = (managerExperienceRepository.findByName(null,null,lstName)).size();
        Integer total_page = 0;
        if(limit !=0){
            total_page = total_item / limit ;
            if((total_item % limit) != 0){
                total_page = total_page + 1;
            }
        }
        experienceDTO.setTotal_item(total_item);
        experienceDTO.setTotal_page(total_page);
        return experienceDTO;
    }

    @Override
    public ManagerExperience findOne(Long id) {
        return managerExperienceRepository.findOne(id);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        managerExperienceRepository.delete(id);
    }
}
