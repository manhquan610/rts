package com.cmc.rts.dto.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

/**
 * Create by trungtx
 * Date: 11/10/2021
 * Time: 12:59 AM
 * Project: CMC_RTS
 */

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FileDTO {

    private String fileName;
    private String previewUrl;
    private String message;

    public FileDTO(String message) {
        this.message = message;
    }

    public FileDTO(String fileName, String previewUrl) {
        this.fileName = fileName;
        this.previewUrl = previewUrl;
    }

    public FileDTO() {
    }
}
