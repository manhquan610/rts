package com.cmc.rts.repository;

import com.cmc.rts.entity.Department;
import com.cmc.rts.repository.custom.DepartmentRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long>, DepartmentRepositoryCustom {
}
