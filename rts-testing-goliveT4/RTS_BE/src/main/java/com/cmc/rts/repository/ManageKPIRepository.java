package com.cmc.rts.repository;

import com.cmc.rts.entity.ManageKpi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ManageKPIRepository extends JpaRepository<ManageKpi, Long> {
    @Query(value = "SELECT m FROM ManageKpi m WHERE UPPER(m.fullName) LIKE UPPER(CONCAT('%',:name,'%')) AND SUBSTRING(m.createdDate,1,4) = :year ORDER BY m.fullName ASC")
    Page<ManageKpi> searchManageKpiByNameAndYear(@Param(value = "name") String name, @Param(value = "year") String year, Pageable pageable);

    @Query(value = "SELECT m FROM ManageKpi m WHERE UPPER(m.fullName) LIKE UPPER(CONCAT('%',:name,'%')) AND SUBSTRING(m.createdDate,1,4) = :year ORDER BY m.fullName ASC")
    List<ManageKpi> searchManageKpiByNameAndYear(@Param(value = "name") String name, @Param(value = "year") String year);

    @Query(value = "SELECT m FROM ManageKpi m WHERE SUBSTRING(m.createdDate,1,4) = :year ORDER BY m.fullName ASC")
    List<ManageKpi> getAllManageKpiByYear(@Param(value = "year") String year);

    ManageKpi findByUsername(String username);
}
