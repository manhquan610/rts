package com.cmc.rts.repository.custom.impl;

import com.cmc.rts.entity.LogException;
import com.cmc.rts.repository.custom.LogExceptionRepositoryCustom;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Repository
public class LogExceptionRepositoryImpl implements LogExceptionRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<LogException> findByName(String name) {
        try {
            StringBuilder sql = new StringBuilder("select * from log_exception m where 1=1");

            if (name != null && !name.isEmpty()) {
                sql.append(" and m.name like '%" + name + "%'");
            }
            Query query = entityManager.createNativeQuery(sql.toString(), LogException.class);
            return query.getResultList();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }
}
