package com.cmc.rts.service.impl;

import com.cmc.rts.dto.response.ChannelRootResponse;
import com.cmc.rts.dto.response.ChannelSourceResponse;
import com.cmc.rts.dto.response.ProjectResponse;
import com.cmc.rts.service.ChannelService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

@Service
public class ChannelServiceImpl implements ChannelService {
    @Value("${candidate.list.api.url}")
    private String candidateApiUrl;

    @Value("${candidate.getChnnelList.path}")
    private String candidateList;

    @Value("${channel.source}")
    private String channelBySource;

    @Value("${channel.by.source}")
    private String allSource;

    private final ObjectMapper objectMapper = new ObjectMapper();
    @Override
    public ChannelRootResponse getChannel() {
        String url = candidateApiUrl + candidateList;
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter(Charset.forName("UTF-8")));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        String response = restTemplate.getForObject(url,String.class,headers);
        ChannelRootResponse channelRootResponse = new ChannelRootResponse();
        try{
            channelRootResponse = objectMapper.readValue(response,ChannelRootResponse.class);
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return channelRootResponse;
    }

    @Override
    public ChannelSourceResponse getChannelBySource(String source) {
        String url = candidateApiUrl + channelBySource + "?source="+source;
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter(Charset.forName("UTF-8")));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        String response = restTemplate.getForObject(url,String.class,headers);
        ChannelSourceResponse channelRootResponse = new ChannelSourceResponse();
        try{
            channelRootResponse = objectMapper.readValue(response,ChannelSourceResponse.class);
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return channelRootResponse;
    }

    @Override
    public List<ChannelSourceResponse> getAllSources() {
        String url = candidateApiUrl + allSource;
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter(Charset.forName("UTF-8")));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        String response = restTemplate.getForObject(url,String.class,headers);
        List<ChannelSourceResponse> responses = new ArrayList<>();
        try{
            responses = objectMapper.readValue(response, new TypeReference<List<ChannelSourceResponse>>(){});
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return responses;
    }
}
