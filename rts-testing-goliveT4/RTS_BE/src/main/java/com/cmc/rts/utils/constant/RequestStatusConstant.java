package com.cmc.rts.utils.constant;

import lombok.Getter;

@Getter
public enum RequestStatusConstant {
    NEW(1),
    PENDING(2),
    APPROVED(3),
    IN_PROGRESS(4),
    PUBLISHED(5),
    CLOSED(6),
    REJECTED(7),
    ASSIGNED(8);

    private int code;

    RequestStatusConstant(int code) {
        this.code = code;
    }
}
