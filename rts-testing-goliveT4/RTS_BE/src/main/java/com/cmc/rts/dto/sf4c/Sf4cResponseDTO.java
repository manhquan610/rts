package com.cmc.rts.dto.sf4c;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Sf4cResponseDTO {
    @SerializedName("__metadata")
    private MetaDataDTO __metadata;
    //Get_UserId
    @SerializedName("userId")
    private String userId;

    //Get_Code_Position
    @SerializedName("position")
    private String position;

    //Get_Child_Code_Position
    @SerializedName("code")
    private String code;
    @SerializedName("targetFTE")
    private String targetFTE;
    @SerializedName("externalName_en_US")
    private String externalName_en_US;

    //Get_Detail_FTE_Code_Position
    @SerializedName("emplStatus")
    private String emplStatus;
    @SerializedName("fte")
    private String fte;

    //JobRequisition
    @SerializedName("jobReqId")
    private String jobReqId;
    @SerializedName("positionNumber")
    private String positionNumber;

}
