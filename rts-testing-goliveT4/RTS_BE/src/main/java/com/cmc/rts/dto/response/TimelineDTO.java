package com.cmc.rts.dto.response;


import com.cmc.rts.entity.HistoryMoveLog;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TimelineDTO {
    private List<InterviewDTO> interviewDTO;
    private List<CommentDTO> commentDTO;
    private List<HistoryMoveLog> historyMoveLog;
    private List<HistoryDeleteDTO> historyDelete;
    private List<CandidateLogDTO> candidateLog;
}
