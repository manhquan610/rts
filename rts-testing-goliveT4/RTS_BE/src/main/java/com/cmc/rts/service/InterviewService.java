package com.cmc.rts.service;

import com.cmc.rts.dto.LocationDTO;
import com.cmc.rts.dto.request.InterviewRequest;
import com.cmc.rts.dto.response.InterviewDTO;
import com.cmc.rts.dto.response.TimelineInterviewDTO;
import com.cmc.rts.dto.response.UserRootResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface InterviewService {
    List<InterviewDTO> getAllInterview(Long request_id);

    Page<InterviewDTO> searchInterview(String searchValue, String location, String interviewer, Boolean status, Long request_id, String ids , Pageable pageable);

    List<LocationDTO> getAllLocationForInterview();

    List<InterviewDTO> getInterviewByCandidateIdAndRequestId(Long candidate_id, Long request_id);

    int createInterview(InterviewRequest request) throws Exception;

    void updateInterview(InterviewRequest request) throws Exception;

    UserRootResponse searchUser(String name);

    List<TimelineInterviewDTO> getAllInterviewOneWeek(String startDate, String endDate, String textSearch);
}

