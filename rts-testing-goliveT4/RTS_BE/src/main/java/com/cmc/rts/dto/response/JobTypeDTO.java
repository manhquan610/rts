package com.cmc.rts.dto.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;

/**
 * Create by trungtx
 * Date: 11/11/2021
 * Time: 1:11 PM
 * Project: CMC_RTS
 */
@Getter
@Setter
public class JobTypeDTO {

    private Long id;
    private String name;
    private String description ;
    private Boolean deleted;

    public JobTypeDTO(Long id, String name, String description, Boolean deleted) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.deleted = deleted;
    }
}
