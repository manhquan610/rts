package com.cmc.rts.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.BooleanSupplier;
import java.util.stream.Collectors;

public class SimpleQueryBuilderUtil {
    private List<String> columns = new ArrayList<>();
    private List<String> tables = new ArrayList<>();
    private List<String> joins = new ArrayList<>();
    private List<String> wheres = new ArrayList<>();
    private List<String> orderBys = new ArrayList<>();
    private List<String> groupBys = new ArrayList<>();
    private String selectStatement;
    private int limit = -1;
    private int offset = -1;

    public SimpleQueryBuilderUtil() {
    }

    public SimpleQueryBuilderUtil(String selectStatement) {
        this.selectStatement = selectStatement;
    }

    public SimpleQueryBuilderUtil addColumn(String name) {
        columns.add(name);
        return this;
    }

    public SimpleQueryBuilderUtil addGroupByColumn(String columnName) {
        columns.add(columnName);
        groupBys.add(columnName);
        return this;
    }

    public SimpleQueryBuilderUtil from(String table) {
        tables.add(table);
        return this;
    }

    public SimpleQueryBuilderUtil groupBy(String expr) {
        groupBys.add(expr);
        return this;
    }

    public SimpleQueryBuilderUtil joinExp(JoinType joinType, String table, List<Condition> conditions) {
        String joinCondition = conditions.stream().map(condition -> condition.toString()).collect(Collectors.joining(" AND "));
        joins.add(joinType.getType() + StringUtils.SPACE + table + StringUtils.SPACE + " ON " + joinCondition);
        return this;
    }

    public SimpleQueryBuilderUtil joinExp(JoinType joinType, String table, String joinStatement) {
        joins.add(joinType.getType() + StringUtils.SPACE + table + StringUtils.SPACE + "ON " + joinStatement);
        return this;
    }

    public SimpleQueryBuilderUtil orderBy(String name, boolean isASC) {
        orderBys.add(name + StringUtils.SPACE + (isASC ? "ASC" : "DESC"));
        return this;
    }

    public SimpleQueryBuilderUtil orderBy(String name, boolean isASC, boolean isNullFirst) {
        orderBys.add(name + StringUtils.SPACE + (isASC ? "ASC" : "DESC") + (isNullFirst ? " NULLS FIRST " : " NULLS LAST "));
        return this;
    }

    public SimpleQueryBuilderUtil limit(int max) {
        limit = max;
        return this;
    }

    public SimpleQueryBuilderUtil offset(int max) {
        offset = max;
        return this;
    }

    public SimpleQueryBuilderUtil where(BooleanSupplier booleanSupplier, Condition condition) {
        if (booleanSupplier.getAsBoolean()) {
            wheres.add(condition.toString());
        }
        return this;
    }

    public SimpleQueryBuilderUtil where(Condition condition) {
        wheres.add(condition.toString());
        return this;
    }

    public String build() {
        StringBuilder sql = new StringBuilder();
        if (Objects.nonNull(this.selectStatement)) {
            sql.append(this.selectStatement);
        } else {
            sql = new StringBuilder("SELECT ");
            if (columns.isEmpty()) {
                sql.append("*");
            } else {
                appendQuery(sql, columns, StringUtils.EMPTY, ", ");
            }
            appendQuery(sql, tables, " FROM ", ", ");
        }
        appendQuery(sql, joins, " ", " ");
        appendQuery(sql, wheres, " WHERE ", " AND ");
        appendQuery(sql, groupBys, " GROUP BY ", ", ");
        appendQuery(sql, orderBys, " ORDER BY ", ", ");
        String response = sql.toString();
        if (limit > 0) {
            response += " LIMIT " + limit;
        }
        if (offset > 0) {
            response += " OFFSET " + offset;
        }
        return response;
    }

    private void appendQuery(StringBuilder sql, List<String> listElement, String statement, String separator) {
        if (listElement.isEmpty()) {
            return;
        }
        sql.append(statement);
        sql.append(listElement.stream().collect(Collectors.joining(separator)));
    }

    public static class Condition {
        private final String fieldName;
        private final Expression expression;
        private final String parameterName;

        public Condition(String fieldName, Expression expression, String parameterName) {
            super();
            this.fieldName = fieldName;
            this.expression = expression;
            this.parameterName = parameterName;
        }

        @Override
        public String toString() {
            return fieldName + StringUtils.SPACE + expression.getNativeValue() + StringUtils.SPACE + ":" + parameterName;
        }
    }

    public enum Expression {
        EQUAL("="), GREATER_THAN(">"), LESS_THAN("<"), IN("IN"), LESS_THAN_OR_EQUAL("<="), GREATER_THAN_OR_EQUAL(">="), LIKE("LIKE");
        private String nativeValue;

        private Expression(final String nativeValue) {
            this.nativeValue = nativeValue;
        }

        public String getNativeValue() {
            return nativeValue;
        }

        public static Expression fromValue(String value) {
            for (Expression v : values()) {
                if (v.getNativeValue().equals(value)) {
                    return v;
                }
            }
            return null;
        }
    }

    public enum JoinType {
        INNER("INNER JOIN"), LEFT("LEFT JOIN"), RIGHT("RIGHT JOIN ");
        private String type;

        private JoinType(final String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }

        public static JoinType fromValue(String value) {
            for (JoinType v : values()) {
                if (v.getType().equals(value)) {
                    return v;
                }
            }
            return null;
        }
    }
}
