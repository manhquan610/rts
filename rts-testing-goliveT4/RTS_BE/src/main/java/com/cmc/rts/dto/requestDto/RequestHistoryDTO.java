package com.cmc.rts.dto.requestDto;

import com.cmc.rts.entity.RequestStatusEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public class RequestHistoryDTO {
    private Long id;
    private String createdBy;
    private String reason;
    private String createdDate;
    private RequestStatusEnum statusRequestType;
    private Integer priority;

    public RequestHistoryDTO(Long id, String createdBy, String reason, RequestStatusEnum statusRequestType, Date createdDate) {
        this.id = id;
        this.createdBy = createdBy;
        this.reason = reason;
        this.statusRequestType = statusRequestType;
        this.createdDate = createdDate.toInstant().toString();
        switch (statusRequestType) {
            case New:
                this.priority = 1;
                break;
            case Edited:
                this.priority = 2;
                break;
            case Pending:
                this.priority = 3;
                break;
            case Approved:
                this.priority = 4;
                break;
            case Approved2:
                this.priority = 5;
                break;
            case Rejected:
                this.priority = 6;
                break;
            case Assigned:
                this.priority = 7;
                break;
            case Closed:
                this.priority = 8;
                break;
            case Shared:
                this.priority = 9;
                break;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RequestHistoryDTO)) return false;
        RequestHistoryDTO that = (RequestHistoryDTO) o;
        return getPriority().equals(that.getPriority());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPriority());
    }
}
