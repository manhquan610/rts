package com.cmc.rts.dto.public_dto;

import com.cmc.rts.entity.CandidateStateEnum;
import lombok.Data;

@Data
public class RequestCandidateDTO {

    private String jobRtsId;

    private CandidateStateEnum previousStep;

    private CandidateStateEnum currentStep;

    private String candidateId;

    private String taLead;

    private String taMember;

    private String createdBy;
}