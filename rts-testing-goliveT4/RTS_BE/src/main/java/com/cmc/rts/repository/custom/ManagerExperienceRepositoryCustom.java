package com.cmc.rts.repository.custom;

import com.cmc.rts.entity.ManagerExperience;
import com.cmc.rts.entity.ManagerMajor;

import java.util.List;

public interface ManagerExperienceRepositoryCustom {
    List<ManagerExperience> findByNameSame(Integer first, Integer limit, String name);
    List<ManagerExperience> findByName(Integer first, Integer limit, List<String> name);
}
