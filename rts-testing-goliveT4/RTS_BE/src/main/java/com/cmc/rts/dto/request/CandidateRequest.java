package com.cmc.rts.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CandidateRequest {
    private String name;
    private String position;
    private String phone;

}
