package com.cmc.rts.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CVRequest {
    private Long id;
    private String fullName;
    private String email;
    private String phone;
    private String linkCv;
    private Long channel;
    private String applySince;
    private String job;
    private String requestId;
    private String jobLevel;
    private Long jobRtsId;
    private String source;
    private String lastCompany;
    private String candidateType;
    private String stepRts;
    private String ta;
    //
    public String skypeID;
    public String facebook;
    public String sex;
    public String address;
    public String birthDay;
    public String experience;
    public String introducer;
    public String school;
    public String degree;
    public String freelancerName;
    public String freelancerMobile;
    public String freelancerEmail;
    public String headhunterName;
    public String refererUser;
    public String roleName;
    public String language;
    public String location;
    public String level;
    public String skill;
    public String assignees;

    public CVRequest(String fullName, String email, String phone, String linkCv, Long channel, String applySince, String job, String requestId, String ta) {
        this.fullName = fullName;
        this.email = email;
        this.phone = phone;
        this.linkCv = linkCv;
        this.channel = channel;
        this.applySince = applySince;
        this.job = job;
        this.requestId = requestId;
        this.ta = ta;
    }

        @Override
    public String toString() {
        return "{\n" +
                "        \"fullName\": \"" + fullName + "\",\n" +
                "        \"email\": \"" + email + "\",\n" +
                "        \"phone\": \"" + phone + "\",\n" +
                "        \"linkCv\": \"" + linkCv + "\",\n" +
                "        \"channel\": \"" + channel + "\",\n" +
                "        \"applySince\": \"" + applySince+ "\",\n" +
                "        \"job\": \"" + job + "\",\n" +
                "        \"jobRtsId\": \"" + requestId + "\",\n" +
                "        \"source\": \"" + source+ "\",\n" +
                "        \"lastCompany\": \"" + lastCompany + "\",\n" +
                "        \"stepRts\": \"" + stepRts + "\",\n" +
                "        \"ta\": \"" + ta + "\",\n" +
                //
                "        \"skypeID\": \"" + skypeID + "\",\n" +
                "        \"facebook\": \"" + facebook + "\",\n" +
                "        \"sex\": \"" + sex + "\",\n" +
                "        \"address\": \"" + address + "\",\n" +
                "        \"birthDay\": \"" + birthDay + "\",\n" +
                "        \"experience\": \"" + experience + "\",\n" +
                "        \"introducer\": \"" + introducer + "\",\n" +
                "        \"school\": \"" + school + "\",\n" +
                "        \"degree\": \"" + degree + "\",\n" +
                "        \"freelancerName\": \"" + freelancerName + "\",\n" +
                "        \"freelancerMobile\": \"" + freelancerMobile + "\",\n" +
                "        \"freelancerEmail\": \"" + freelancerEmail + "\",\n" +
                "        \"headhunterName\": \"" + headhunterName + "\",\n" +
                "        \"refererUser\": \"" + refererUser + "\",\n" +
                "        \"roleName\": \"" + roleName + "\",\n" +
                "        \"language\": \"" + language + "\",\n" +
                "        \"location\": \"" + location + "\",\n" +
                "        \"level\": \"" + level + "\",\n" +
                "        \"skill\": \"" + skill + "\",\n" +
                "        \"assignees\": \"" + assignees + "\",\n" +
                //
                "        \"candidateType\": \"" + candidateType + "\"\n" +
                "}";
    }

}
