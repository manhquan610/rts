package com.cmc.rts.dto.response;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.Objects;

@Getter
@Setter
public class TotalCandidateTAResponse {

    private String ta;
    private Long totalNone;
    private Long totalQualify;
    private Long totalConfirm;
    private Long totalInterview;
    private Long totalOffer;
    private Long totalOnboard;
    private Long totalFailed;
    private List<CandidateListTotalStepResponse> candidateByJobRtsList;
}
