package com.cmc.rts.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CandidateDetailDTO {
    private Long id;

    private String fullName;

    private Timestamp createdDate;

    private String location;

    private String phone;

    private String position;

    private String cvAttach;

    private String note;

    private Boolean status;
}
