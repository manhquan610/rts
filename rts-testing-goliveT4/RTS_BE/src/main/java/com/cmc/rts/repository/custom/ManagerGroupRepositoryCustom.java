package com.cmc.rts.repository.custom;

import com.cmc.rts.entity.ManagerGroup;

import java.util.List;

public interface ManagerGroupRepositoryCustom {
    List<ManagerGroup> findByName(String name, int offset, int limit);

    ManagerGroup findByGroupId(Long id);
}
