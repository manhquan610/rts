package com.cmc.rts.repository.custom;

import com.cmc.rts.entity.Project;

import java.util.List;

public interface ProjectRepositoryCustom {
    List<Project> findByName(String name, int offset, int limit);

    Project findByProjectId(Long id);
}
