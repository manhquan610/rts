package com.cmc.rts.service.impl;

import com.cmc.rts.dto.response.LikeDTO;
import com.cmc.rts.entity.Like;
import com.cmc.rts.repository.LikeRepository;
import com.cmc.rts.service.LikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import java.sql.Timestamp;

@Service
public class LikeServiceImpl implements LikeService {

    @Autowired
    private LikeRepository likeRepository;
    @Override
    public Like createLike(Long candidateId, Boolean liked) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Like like = likeRepository.getLiked(authentication.getPrincipal().toString(), candidateId);
        if(like == null){
            Like temp = new Like();
            temp.setCandidateId(candidateId);
            temp.setCreatedBy(authentication.getPrincipal().toString());
            temp.setCreatedDate(new Timestamp(System.currentTimeMillis()));
            temp.setLiked(liked);
           return likeRepository.save(temp);
        }else if(liked == true){
            like.setCandidateId(candidateId);
            like.setCreatedBy(authentication.getPrincipal().toString());
            like.setCreatedDate(new Timestamp(System.currentTimeMillis()));
            like.setLiked(true);
            return likeRepository.save(like);

        }else if(liked == false){
            like.setCandidateId(candidateId);
            like.setCreatedBy(authentication.getPrincipal().toString());
            like.setCreatedDate(new Timestamp(System.currentTimeMillis()));
            like.setLiked(false);
            return likeRepository.save(like);
        }

        return  null;
    }


    @Override
    public LikeDTO totalReact(Long candidateId) {
        LikeDTO dto = new LikeDTO();
        dto.setLike(likeRepository.count(candidateId,true));
        dto.setDislike(likeRepository.count(candidateId,false));
        dto.setLikedUser(likeRepository.getReactUser(true,candidateId));
        dto.setDislikedUser(likeRepository.getReactUser(false,candidateId));
        return dto;
    }
}
