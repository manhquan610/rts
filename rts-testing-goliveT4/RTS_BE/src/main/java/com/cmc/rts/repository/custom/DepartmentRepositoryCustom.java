package com.cmc.rts.repository.custom;


import com.cmc.rts.entity.Department;

import java.util.List;

public interface DepartmentRepositoryCustom {
    List<Department> findByName(String name, int offset, int limit);

    Department findByDepartmentId(Long id);
}
