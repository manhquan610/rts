package com.cmc.rts.dto.response;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LanguageDTO {
    private Integer id;
    private String name;

    public LanguageDTO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
}