package com.cmc.rts.repository.v2;

import com.cmc.rts.entity.v2.V2RequestReportTicket;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface V2RequestReportTicketRepository extends JpaRepository<V2RequestReportTicket, Long> {

    List<V2RequestReportTicket> findV2RequestReportTicketByIdIn(List<Long> requestIds);

    V2RequestReportTicket findV2RequestReportTicketByRequestId(Long requestId);

}
