package com.cmc.rts.entity.v2;

import com.cmc.rts.dto.response.UserResponse;
import com.cmc.rts.entity.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Date;
import java.util.Set;

/**
 * @author: nthieu10
 * 7/25/2022
 **/
@Entity
@Getter
@Setter
@Table(name = "request")
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"code", "rejectReason", "deleted", "version", "deadlineUnused"})
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "request")
public class V2Request extends CategoryBaseDomain {

    @Column(columnDefinition = "boolean default false")
    private Boolean published;

    @Column(name = "deadline", nullable = false)
    @JsonProperty("deadlineUnused")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date deadline;

    @Column
    @JsonProperty("deadline")
    private String deadlineStr;

    @Min(0)
    @Max(1000)
    @Column(name = "number", nullable = false)
    private int number;

    @Column(name = "manager_major_id")
    private Long managerMajorId;

    @Enumerated(EnumType.STRING)
    @Column
    private RequestStatusEnum requestStatusName;

    @Column(name = "manager_priority_id", nullable = false)
    private Long managerPriorityId;

    @Column(name = "manager_experience_id", nullable = false)
    private Long managerExperienceId;

    @Column(name = "manager_request_type_id", nullable = false)
    private Long managerRequestTypeId;

    private Long projectId;

    @Column(name = "reject_reason_id")
    private Long rejectReasonId;

    private Long departmentId;

    private Long managerGroupId;

    @Column
    private String managerName;

    @Column(name = "certificate")
    private String certificate;

    @Column(name = "others", length = 3000)
    private String others;

    @Column(name = "salary")
    private String salary;

    @Column(name = "benefit", length = 3000)
    private String benefit;

    private String languageId;

    private String skillNameId;

    @Column(name = "area_id")
    private Long areaId;

    @Column(name = "job_type_id")
    private Long jobTypeId;

    @Enumerated(EnumType.STRING)
    private WorkingTypeEnum workingType;

    private Boolean shareJob;

    private transient UserResponse lastGroupSubmit;

    private String lastGroupSubmitLdap;

    private String codePosition;

    @Column(length = 3000)
    private String requirement;
}
