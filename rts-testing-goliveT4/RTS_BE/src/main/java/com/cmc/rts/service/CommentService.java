package com.cmc.rts.service;

import com.cmc.rts.dto.request.CommentRequest;
import com.cmc.rts.dto.response.CommentDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CommentService {
    Page<CommentDTO> getAllCommentsForRequest(Long requestId, Pageable pageable);
    Page<CommentDTO> getAllCommentsForCandidate(Long requestId,Long candidateId, Pageable pageable);
    void delete(Long id);
    void createComment(CommentRequest request);
    void updateComment(CommentRequest request);
}