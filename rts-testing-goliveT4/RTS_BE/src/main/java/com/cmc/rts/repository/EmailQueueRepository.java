package com.cmc.rts.repository;

import com.cmc.rts.entity.EmailQueue;
import com.cmc.rts.repository.custom.EmailQueueRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailQueueRepository extends JpaRepository<EmailQueue, Long>, EmailQueueRepositoryCustom {
}
