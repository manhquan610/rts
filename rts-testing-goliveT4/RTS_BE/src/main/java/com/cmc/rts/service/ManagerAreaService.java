package com.cmc.rts.service;

import com.cmc.rts.dto.request.AreaRequest;
import com.cmc.rts.dto.request.InterviewRequest;
import com.cmc.rts.entity.ManagerArea;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ManagerAreaService {
    Page<ManagerArea> findByName( String searchValues, Pageable pageable);
    Page<ManagerArea> findByListName(String name, Pageable pageable);
    Page<ManagerArea> getAllArea(Pageable pageable);
    ManagerArea getOne(Long id);
    ManagerArea createArea(AreaRequest request) throws Exception;

    ManagerArea updateArea(AreaRequest request) throws Exception;

    void deleteAreas(Long id);


}
