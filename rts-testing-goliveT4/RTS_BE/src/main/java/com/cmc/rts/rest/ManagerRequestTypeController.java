package com.cmc.rts.rest;

import com.cmc.rts.dto.DeleteDTO;
import com.cmc.rts.dto.request.ListNameRequest;
import com.cmc.rts.dto.request.NameRequest;
import com.cmc.rts.dto.response.RequestTypeDTO;
import com.cmc.rts.entity.ManagerRequestType;
import com.cmc.rts.entity.UserDTO;
import com.cmc.rts.security.AuthenUser;
import com.cmc.rts.service.ManagerRequestTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
public class ManagerRequestTypeController {
    @Autowired
    private AuthenUser authenUser;

    @Autowired
    ManagerRequestTypeService managerRequestTypeService;

    @PostMapping(value = "/manager-request-type")
    public ResponseEntity<?> createManagerMajor(HttpServletRequest request, @RequestBody ManagerRequestType managerRequestType) throws Exception{

        UserDTO userLogin = authenUser.getUser(request);
        if(managerRequestType !=null){
            if(managerRequestType.getName() != null && !managerRequestType.getName().isEmpty() && managerRequestType.getName().length() > 50){
                throw new Exception("You've exceeded the limit by 50 characters");
            }
            if(managerRequestType.getDescription() != null && !managerRequestType.getDescription().isEmpty() && managerRequestType.getDescription().length() > 250){
                throw new Exception("You've exceeded the limit by 250 characters");
            }
        }

        try {
            managerRequestTypeService.addManagerRequestType(managerRequestType);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("RequestType Name already exist");
        }


        return new ResponseEntity<ManagerRequestType>(managerRequestType, HttpStatus.OK);
    }

    @PutMapping(value = "/manager-request-type")
    public ResponseEntity<?> updateManagerMajor(HttpServletRequest request, @RequestBody ManagerRequestType managerRequestType) throws Exception{

        UserDTO userLogin = authenUser.getUser(request);

        if(managerRequestType !=null){
            if(managerRequestType.getName() != null && !managerRequestType.getName().isEmpty() && managerRequestType.getName().length() > 50){
                throw new Exception("You've exceeded the limit by 50 characters");
            }
            if(managerRequestType.getDescription() != null && !managerRequestType.getDescription().isEmpty() && managerRequestType.getDescription().length() > 250){
                throw new Exception("You've exceeded the limit by 250 characters");
            }
        }

        try {
            managerRequestTypeService.addManagerRequestType(managerRequestType);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("RequestType Name already exist");
        }

        return new ResponseEntity<ManagerRequestType>(managerRequestType, HttpStatus.OK);
    }

    @GetMapping(value = "/manager-request-type/{id}")
    public ResponseEntity<?> findOneManagerMajor(@PathVariable Long id) {
        ManagerRequestType managerRequestType = managerRequestTypeService.findOne(id);
        return new ResponseEntity<ManagerRequestType>(managerRequestType, HttpStatus.OK);
    }

    @PostMapping(value = "/manager-request-type/same")
    public ResponseEntity<RequestTypeDTO> all(@RequestBody NameRequest nameRequest) {
        RequestTypeDTO requestTypeDTO = managerRequestTypeService.findAll(nameRequest);
        return new ResponseEntity<RequestTypeDTO>(requestTypeDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/manager-request-type/exact")
    public ResponseEntity<RequestTypeDTO> all(@RequestBody ListNameRequest listNameRequest) {
        RequestTypeDTO requestTypeDTO = managerRequestTypeService.findAll(listNameRequest);
        return new ResponseEntity<RequestTypeDTO>(requestTypeDTO, HttpStatus.OK);
    }


    @PostMapping (value = "/manager-request-type/delete")
    public void delete(@RequestBody DeleteDTO deleteDTO) throws Exception{
        Long[] ids = deleteDTO.getIds();
        for(Long item: ids) {
            try {
            managerRequestTypeService.delete(item);
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception("Can't Delete! Because this item already exists in other information");
            }
        }
    }
}
