package com.cmc.rts.dto.user;

import lombok.Getter;

@Getter
public class UserGroup {
    private Long idHistory;
    private Long duId;
    private String duName;
    private String startDate;
    private String endDate;
    private String updateDate;
}
