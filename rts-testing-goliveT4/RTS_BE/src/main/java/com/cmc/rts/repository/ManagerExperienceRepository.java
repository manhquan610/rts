package com.cmc.rts.repository;

import com.cmc.rts.entity.ManagerExperience;
import com.cmc.rts.repository.custom.ManagerExperienceRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ManagerExperienceRepository  extends JpaRepository<ManagerExperience, Long>, ManagerExperienceRepositoryCustom {
}
