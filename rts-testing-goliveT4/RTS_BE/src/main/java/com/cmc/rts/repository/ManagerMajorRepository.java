package com.cmc.rts.repository;

import com.cmc.rts.entity.ManagerMajor;
import com.cmc.rts.repository.custom.ManagerMajorRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ManagerMajorRepository  extends JpaRepository<ManagerMajor, Long>, ManagerMajorRepositoryCustom {
}
