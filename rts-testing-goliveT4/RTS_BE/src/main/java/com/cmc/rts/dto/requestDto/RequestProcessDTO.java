package com.cmc.rts.dto.requestDto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RequestProcessDTO {
    private List<RequestAssignDTO> assignees;
    private List<RequestHistoryDTO> process;

    public RequestProcessDTO(List<RequestAssignDTO> assignees, List<RequestHistoryDTO> process) {
        this.assignees = assignees;
        this.process = process;
    }
}
