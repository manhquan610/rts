package com.cmc.rts.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GroupResponseDTO {
    @JsonProperty("items")
    List<GroupResponseItemDTO> groupResponseDTO;
    public Integer totalRecords;
    public List<String> messages;
    public Boolean status;
}