package com.cmc.rts.repository;

import com.cmc.rts.entity.CategoryBaseDomain;
import com.cmc.rts.repository.custom.RtsRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RtsRepository  extends JpaRepository<CategoryBaseDomain,Long>, RtsRepositoryCustom {
}
