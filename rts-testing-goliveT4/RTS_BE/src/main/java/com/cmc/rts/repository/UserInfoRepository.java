package com.cmc.rts.repository;

import com.cmc.rts.entity.UserInfo;
import com.cmc.rts.repository.custom.UserInfoRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserInfoRepository extends JpaRepository<UserInfo, Long>, UserInfoRepositoryCustom {
}
