package com.cmc.rts.repository;

import com.cmc.rts.dto.response.CandidateDTO;
import com.cmc.rts.dto.response.HistoryStatusDTO;
import com.cmc.rts.entity.Candidate;
import com.cmc.rts.entity.CandidateStateEnum;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CandidateRepository extends JpaRepository<Candidate, Long> {

    List<Candidate> findAllByIdIn(List<Long> ids);

    @Query(value = "SELECT new com.cmc.rts.dto.response.CandidateDTO(c.id, c.fullName, c.createdDate, c.createdBy) " +
            "FROM RequestCandidate rc " +
            "inner join rc.candidate c " +
            "where rc.request.id = :requestId and rc.candidateState = :candidateState and (c.fullName like %:searchValue% or c.email like %:searchValue% or c.phone like %:searchValue% ) order by c.sendDate desc")
    Page<CandidateDTO>  searchCandidates(@Param(value = "requestId") Long requestId,
                                          @Param(value = "candidateState") CandidateStateEnum candidateState,
                                          @Param(value = "searchValue") String searchValues,
                                          Pageable pageable);

    @Query(value = "select c from RequestCandidate rc " +
            "inner join rc.candidate c " +
            " where rc.request.id = :requestId  and c.candidateState  = :candidateState and c.id in :ids order by c.sendDate desc ")
    List<Candidate> findAllCandidatesByIds(@Param(value = "ids") List<Long> id,
                                           @Param(value = "requestId") Long requestId,
                                           @Param(value = "candidateState") CandidateStateEnum candidateState,
                                           Pageable pageable);

    @Query(value = "select c from RequestCandidate rc " +
            "inner join rc.candidate c " +
            "where rc.candidate.candidateId = :id ")
    Candidate findByCandidateId(@Param("id") Long id);

//    @Query(value = "select new com.cmc.rts.dto.response.HistoryStatusDTO(c.id, rc.candidateState) " +
//            "from RequestCandidate rc " +
//            "inner join rc.candidate c " +
//            " where rc.request.id = :requestId  and c.id in :ids order by c.sendDate desc ")
//    List<HistoryStatusDTO> findAllStatusByIds(@Param(value = "ids") List<Long> id,
//                                                  @Param(value = "requestId") Long requestId);

}
