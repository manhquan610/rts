package com.cmc.rts.repository;

import com.cmc.rts.dto.response.CandidateLogDTO;
import com.cmc.rts.dto.response.CommentDTO;
import com.cmc.rts.entity.CandidateLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CandidateLogRepository extends JpaRepository<CandidateLog, Long> {

    @Query(value = "select new com.cmc.rts.dto.response.CandidateLogDTO(c.id, c.requestId, c.candidateId, c.log ,c.createdBy, c.createdDate) " +
            "from CandidateLog as c " +
            "where c.requestId = :requestId " +
            "and c.candidateId = :candidateId  order by c.createdDate desc  ")
    List<CandidateLogDTO> getAllCandidate(@Param(value ="requestId") Long requestId, @Param(value = "candidateId") Long candidateId);
}
