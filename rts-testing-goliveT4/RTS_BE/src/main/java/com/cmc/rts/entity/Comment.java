package com.cmc.rts.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Comment extends BaseDomain{

    @Column(name = "text", length = 1000)
    private String text;
    @Column(name = "request_id")
    private Long requestId;
    @Column(name = "candidate_id")
    private Long candidateId;
}