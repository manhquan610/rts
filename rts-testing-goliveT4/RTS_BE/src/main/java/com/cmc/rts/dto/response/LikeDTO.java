package com.cmc.rts.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LikeDTO {
    private Integer like;
    private Integer dislike;
    private List<String> likedUser;
    private List<String> dislikedUser;
}
