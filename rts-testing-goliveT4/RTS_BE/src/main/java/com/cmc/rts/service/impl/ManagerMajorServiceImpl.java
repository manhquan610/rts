package com.cmc.rts.service.impl;

import com.cmc.rts.dto.request.ListNameRequest;
import com.cmc.rts.dto.request.NameRequest;
import com.cmc.rts.dto.response.MajorDTO;
import com.cmc.rts.entity.ManagerMajor;
import com.cmc.rts.repository.ManagerMajorRepository;
import com.cmc.rts.service.ManagerMajorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class ManagerMajorServiceImpl implements ManagerMajorService {

    @Autowired
    ManagerMajorRepository  managerMajorRepository;

    @Override
    @Transactional
    public ManagerMajor addManagerMajor(ManagerMajor managerMajor) {
        return managerMajorRepository.save(managerMajor);
    }

    @Override
    public MajorDTO findAll(NameRequest nameRequest) {
        MajorDTO majorDTO = new MajorDTO();
        String lstName = "";
        if(nameRequest !=null && nameRequest.getLstName() !=null){
            lstName = nameRequest.getLstName();
        }
        Integer offset = null;
        if(nameRequest !=null && nameRequest.getOffset() !=null){
            offset = nameRequest.getOffset();
        }
        Integer limit = null;
        if(nameRequest !=null && nameRequest.getLimit()!=null){
            limit = nameRequest.getLimit();
        }

        Integer first = 0;
        if(offset !=null && limit !=null){
            first = (offset-1)*limit;
        }else {
            limit = 10;
        }
        majorDTO.setManager(managerMajorRepository.findByNameSame(first,limit,lstName));
        Integer total_item = (managerMajorRepository.findByNameSame(null,null,lstName)).size();
        Integer total_page = 0;
        if(limit !=0){
            total_page = total_item / limit ;
            if((total_item % limit) != 0){
                total_page = total_page + 1;
            }
        }
        majorDTO.setTotal_item(total_item);
        majorDTO.setTotal_page(total_page);
        return majorDTO;
    }

    @Override
    public MajorDTO findAll(ListNameRequest listNameRequest) {
        MajorDTO majorDTO = new MajorDTO();
        List<String> lstName = new ArrayList<>();
        if(listNameRequest !=null && listNameRequest.getLstName() !=null){
            lstName = listNameRequest.getLstName();
        }
        Integer offset = null;
        if(listNameRequest !=null && listNameRequest.getOffset() !=null){
            offset = listNameRequest.getOffset();
        }
        Integer limit = null;
        if(listNameRequest !=null && listNameRequest.getLimit()!=null){
            limit = listNameRequest.getLimit();
        }

        Integer first = 0;
        if(offset !=null && limit !=null){
            first = (offset-1)*limit;
        }else {
            limit = 10;
        }
        majorDTO.setManager(managerMajorRepository.findByName(first,limit,lstName));
        Integer total_item = (managerMajorRepository.findByName(null,null,lstName)).size();
        Integer total_page = 0;
        if(limit !=0){
            total_page = total_item / limit ;
            if((total_item % limit) != 0){
                total_page = total_page + 1;
            }
        }
        majorDTO.setTotal_item(total_item);
        majorDTO.setTotal_page(total_page);
        return majorDTO;
    }

    @Override
    public ManagerMajor findOne(Long id) {
        return managerMajorRepository.findOne(id);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        managerMajorRepository.delete(id);
    }
}
