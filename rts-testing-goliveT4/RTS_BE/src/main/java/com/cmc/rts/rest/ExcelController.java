package com.cmc.rts.rest;


import com.cmc.rts.response.ResponseMessage;
import com.cmc.rts.service.ExcelService;
import com.cmc.rts.utils.ExcelHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class ExcelController {

    @Autowired
    private ExcelService excelService;


    @PostMapping(value = "/excel/read/upload")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) {
        String message = "";

        if (ExcelHelper.hasExcelFormat(file)) {
            try {
                String result = excelService.saveExcelData(file);
                if (result.equals("Done!")) {
                    return ResponseEntity.status(HttpStatus.OK).body(result);
                } else {
                    return ResponseEntity.status(HttpStatus.OK).body("Error");
                }
            } catch (Exception e) {
                message = "Could not upload the file: " + file.getOriginalFilename() + "!";
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(e.getMessage()));
            }
        }
        message = "Please upload an excel file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
    }

    @PostMapping(value = "/excel/read/assigned")
    public ResponseEntity<?> assignedHRExcel(@RequestParam("file") MultipartFile file) {
        String message = "";

        if (ExcelHelper.hasExcelFormat(file)) {
            try {
                String result = excelService.assignedHRRequest(file);
                if (result.equals("Done!")) {
                    return ResponseEntity.status(HttpStatus.OK).body(result);
                } else {
                    return ResponseEntity.status(HttpStatus.OK).body("Error");
                }
            } catch (Exception e) {
                message = "Could not upload the file: " + file.getOriginalFilename() + "!";
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(e.getMessage()));
            }
        }
        message = "Please upload an excel file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
    }
}
