package com.cmc.rts.service.impl;

import com.cmc.rts.dto.request.CommentRequest;
import com.cmc.rts.dto.response.CommentDTO;
import com.cmc.rts.entity.Comment;
import com.cmc.rts.exception.CustomException;
import com.cmc.rts.repository.CommentRepository;
import com.cmc.rts.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    CommentRepository commentRepository;

    @Override
    public Page<CommentDTO> getAllCommentsForRequest(Long requestId, Pageable pageable) {
        Page<CommentDTO> commentDTOS = commentRepository.getAllCommentsForRequestDetail(requestId, pageable);
        return commentDTOS;
    }

    @Override
    public Page<CommentDTO> getAllCommentsForCandidate(Long requestId, Long candidateId, Pageable pageable) {
        return commentRepository.getAllCommentsForCandidateInRequest(requestId, candidateId, pageable);
    }

    @Override
    public void delete(Long parentId) {
        boolean exists = commentRepository.exists(parentId);
        if (!exists) {
            throw new IllegalStateException("comment with ID" + parentId + "does not exist");
        }
        commentRepository.delete(parentId);
    }

    @Override
    public void createComment(CommentRequest request) {
        Comment comment = new Comment();
        comment.setCandidateId(request.getCandidateId());
        comment.setText(request.getText());
        comment.setRequestId(request.getRequestId());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        comment.setCreatedBy(authentication.getPrincipal().toString());
        commentRepository.save(comment);
    }

    @Override
    public void updateComment(CommentRequest request) {
        if(request.getRequestId() == null || request.getId() == null){
            throw new CustomException("Request Id và Id không được phép null", HttpStatus.FORBIDDEN.value());
        }
        Comment comment = commentRepository.findOne(request.getId());
        if(comment == null){
            throw new CustomException("Không tìm thấy comment", HttpStatus.NOT_FOUND.value());
        }
        comment.setText(request.getText());
        commentRepository.save(comment);
    }
}