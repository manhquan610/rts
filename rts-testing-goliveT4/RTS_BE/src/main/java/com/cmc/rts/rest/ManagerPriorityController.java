package com.cmc.rts.rest;

import com.cmc.rts.dto.DeleteDTO;
import com.cmc.rts.dto.request.ListNameRequest;
import com.cmc.rts.dto.request.NameRequest;
import com.cmc.rts.dto.response.PriorityDTO;
import com.cmc.rts.entity.ManagerPriority;
import com.cmc.rts.entity.UserDTO;
import com.cmc.rts.security.AuthenUser;
import com.cmc.rts.service.ManagerPriorityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
public class ManagerPriorityController {

    @Autowired
    private AuthenUser authenUser;

    @Autowired
    ManagerPriorityService managerPriorityService;

    @PostMapping(value = "/manager-priority")
    public ResponseEntity<?> createManagerPriority(HttpServletRequest request, @RequestBody ManagerPriority managerPriority) throws Exception {

        UserDTO userLogin = authenUser.getUser(request);
        if (managerPriority != null) {
            if (managerPriority.getName() != null && !managerPriority.getName().isEmpty() && managerPriority.getName().length() > 50) {
                throw new Exception("You've exceeded the limit by 50 characters");
            }
            if (managerPriority.getDescription() != null && !managerPriority.getDescription().isEmpty() && managerPriority.getDescription().length() > 250) {
                throw new Exception("You've exceeded the limit by 250 characters");
            }
        }

        try {
            managerPriorityService.addManagerPriority(managerPriority);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Priority Name already exist");
        }

        return new ResponseEntity<ManagerPriority>(managerPriority, HttpStatus.OK);
    }

    @PutMapping(value = "/manager-priority")
    public ResponseEntity<?> updateManagerPriority(HttpServletRequest request, @RequestBody ManagerPriority managerPriority) throws Exception {

        UserDTO userLogin = authenUser.getUser(request);

        if (managerPriority != null) {
            if (managerPriority.getName() != null && !managerPriority.getName().isEmpty() && managerPriority.getName().length() > 50) {
                throw new Exception("You've exceeded the limit by 50 characters");
            }
            if (managerPriority.getDescription() != null && !managerPriority.getDescription().isEmpty() && managerPriority.getDescription().length() > 250) {
                throw new Exception("You've exceeded the limit by 250 characters");
            }
        }

        try {
            managerPriorityService.addManagerPriority(managerPriority);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Priority Name already exist");
        }

        return new ResponseEntity<ManagerPriority>(managerPriority, HttpStatus.OK);
    }

    @GetMapping(value = "/manager-priority/{id}")
    public ResponseEntity<?> findOneManagerPriority(@PathVariable Long id) {
        ManagerPriority managerPriority = managerPriorityService.findOne(id);
        return new ResponseEntity<ManagerPriority>(managerPriority, HttpStatus.OK);
    }

    @PostMapping(value = "/manager-priority/same")
    public ResponseEntity<PriorityDTO> all(@RequestBody NameRequest nameRequest) {
        PriorityDTO priorityDTO = managerPriorityService.findAll(nameRequest);
        return new ResponseEntity<PriorityDTO>(priorityDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/manager-priority/exact")
    public ResponseEntity<PriorityDTO> allAxact(@RequestBody ListNameRequest listNameRequest) {
        PriorityDTO priorityDTO = managerPriorityService.findAll(listNameRequest);
        return new ResponseEntity<PriorityDTO>(priorityDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/manager-priority/delete")
    public void delete(@RequestBody DeleteDTO deleteDTO) throws Exception{
        Long[] ids = deleteDTO.getIds();
        for (Long item : ids) {
            try {
                managerPriorityService.delete(item);
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception("Can't Delete! Because this item already exists in other information");
            }
        }
    }
}
