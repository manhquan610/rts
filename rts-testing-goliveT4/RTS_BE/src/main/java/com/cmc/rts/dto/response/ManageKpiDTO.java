package com.cmc.rts.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ManageKpiDTO implements Validator {
    @NotNull
    private String fullName;
    @NotNull
    private String username;
    @NotNull
    private Long janQuantity;
    @NotNull
    private Long febQuantity;
    @NotNull
    private Long marQuantity;
    @NotNull
    private Long aprQuantity;
    @NotNull
    private Long mayQuantity;
    @NotNull
    private Long junQuantity;
    @NotNull
    private Long julQuantity;
    @NotNull
    private Long augQuantity;
    @NotNull
    private Long sepQuantity;
    @NotNull
    private Long octQuantity;
    @NotNull
    private Long novQuantity;
    @NotNull
    private Long decQuantity;
    
    private String changeStatus;

    @Override
    public boolean supports(Class<?> aClass) {
        return false;
    }

    @Override
    public void validate(Object o, Errors errors) {

    }
}
