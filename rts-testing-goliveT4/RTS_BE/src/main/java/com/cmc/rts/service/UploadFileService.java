package com.cmc.rts.service;


import com.cmc.rts.dto.response.FileDTO;
import com.cmc.rts.dto.response.UploadExcelRes;
import com.cmc.rts.dto.response.UploadFileDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * Create by trungtx
 * Date: 11/9/2021
 * Time: 11:51 PM
 * Project: CMC_RTS
 */
public interface UploadFileService {

    /*upload file*/
    FileDTO uploadFile(String fileType, MultipartFile file) throws IOException;

    /*upload multi file*/
    UploadFileDTO uploadMultiFile(String fileType, MultipartFile[] file) throws IOException;

    UploadExcelRes uploadExcel(String fileInfo, MultipartFile file) throws IOException;

    UploadExcelRes uploadExcelStepRTS(String fileInfo, MultipartFile file, String requestId, String stepRTS) throws IOException;

}
