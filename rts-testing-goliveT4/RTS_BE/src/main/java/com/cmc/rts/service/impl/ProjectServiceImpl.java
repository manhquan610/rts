package com.cmc.rts.service.impl;

import com.cmc.rts.entity.Project;
import com.cmc.rts.repository.ProjectRepository;
import com.cmc.rts.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    ProjectRepository projectRepository;

    @Override
    public List<Project> findByName(String name, int offset, int limit) {
        return projectRepository.findByName(name, offset, limit);
    }

    @Override
    @Transactional
    public void deleteUsersWithIds(List<Long> ids) {
        projectRepository.deleteUsersWithIds(ids);
    }

    @Override
    @Transactional
    public Project setProject(Project project) {
        return projectRepository.save(project);
    }

    @Override
    public Project findOne(Long id) {
        return projectRepository.findOne(id);
    }

    @Override
    public Project findByProjectId(Long projectId) {
        return projectRepository.findByProjectId(projectId);
    }
}
