package com.cmc.rts.repository;

import com.cmc.rts.entity.ManagerPriority;
import com.cmc.rts.repository.custom.ManagerMajorRepositoryCustom;
import com.cmc.rts.repository.custom.ManagerPriorityRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ManagerPriorityRepository  extends JpaRepository<ManagerPriority, Long>, ManagerPriorityRepositoryCustom {
    @Query(value = "SELECT m " +
            "FROM ManagerPriority m " +
            "where (:managerPriorityIdNotEmpty = false or m.id in :managerPriorityIds ) ")
    List<ManagerPriority> findManagerPrioritiesByParam(@Param("managerPriorityIds") List<Long> managerPriorityIds,
                                                       @Param("managerPriorityIdNotEmpty") Boolean managerPriorityIdNotEmpty);

}
