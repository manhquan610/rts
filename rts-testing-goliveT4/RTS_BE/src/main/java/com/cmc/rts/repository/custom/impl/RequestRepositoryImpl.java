package com.cmc.rts.repository.custom.impl;

import com.cmc.rts.entity.Request;
import com.cmc.rts.repository.custom.RequestRepositoryCustom;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Repository
public class RequestRepositoryImpl implements RequestRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Request> findByName(JSONObject jsonObject) {
        String name = null;
        if (jsonObject.has("name")) {
            name = jsonObject.getString("name");
        }
        int like = 1;
        if (jsonObject.has("name")) {
            like = jsonObject.getInt("like");
        }
        String requestIds = null;
        if (jsonObject.has("requestIds")) {
            requestIds = jsonObject.getString("requestIds");
        }
        String groupIds = null;
        if (jsonObject.has("groupIds")) {
            groupIds = jsonObject.getString("groupIds");
        }
        String managerGroupId = null;
        if (jsonObject.has("managerGroupId")) {
            managerGroupId = jsonObject.getString("managerGroupId");
        }
        String departmentIds = null;
        if (jsonObject.has("departmentIds")) {
            departmentIds = jsonObject.getString("departmentIds");
        }
        String departmentId = null;
        if (jsonObject.has("departmentId")) {
            departmentId = jsonObject.getString("departmentId");
        }
        String projectIds = null;
        if (jsonObject.has("projectIds")) {
            projectIds = jsonObject.getString("projectIds");
        }
        String statusIds = null;
        if (jsonObject.has("statusIds")) {
            statusIds = jsonObject.getString("statusIds");
        }
        String hrIds = null;
        if (jsonObject.has("hrIds")) {
            hrIds = jsonObject.getString("hrIds");
        }
        int limit = 25;
        if (jsonObject.has("limit")) {
            limit = jsonObject.getInt("limit");
        }
        int offset = 0;
        if (jsonObject.has("offset")) {
            offset = jsonObject.getInt("offset");
        }
        String orderBy = null;
        if (jsonObject.has("orderBy")) {
            orderBy = jsonObject.getString("orderBy");
        }
        String orderType = null;
        if (jsonObject.has("orderType")) {
            if (jsonObject.getString("orderType").equalsIgnoreCase("desc") || jsonObject.getString("orderType").equalsIgnoreCase("asc")) {
                orderType = jsonObject.getString("orderType");
            }

        }
        String type = null;
        if (jsonObject.has("type")) {
            if (jsonObject.getString("type").equalsIgnoreCase("list")) {
                type = jsonObject.getString("type");
            }

        }
        List<Request> requests = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder("select * from request m ");
            if (hrIds != null && !hrIds.isEmpty()) {
                sql.append("inner join request_assign ra on m.id = ra.request_id and ra.user_info_id in (" + hrIds + ")" );
            }
            sql.append("where m.deleted = 0 ");
            if (name != null && !name.isEmpty()) {
                String[] strings = name.split(",");
                String orCondition = " or ";
                if (strings.length > 0) {
                    sql.append(" and (");
                    for (String tmp : strings) {
                        if (like == 1) {
                            sql.append(" m.name like '%" + tmp + "%'");
                            sql.append(orCondition);
                        } else {
                            sql.append(" m.name like '" + tmp + "'");
                            sql.append(orCondition);
                        }
                    }
                    sql.setLength(sql.length() - orCondition.length());
                    sql.append(" )");
                }

            }
            if (requestIds != null && !requestIds.isEmpty()) {
                sql.append(" and m.id in (" + requestIds + ") ");
            }
            if (departmentIds != null && !departmentIds.isEmpty()) {
                sql.append(" and m.department_id in (" + departmentIds + ") ");
            }
            if (departmentId != null && !departmentId.isEmpty()) {
                sql.append(" and m.department_id in (" + departmentId + ") ");
            }
            if (projectIds != null && !projectIds.isEmpty()) {
                sql.append(" and m.project_id in (" + projectIds + ") ");
            }
            if (groupIds != null && !groupIds.isEmpty()) {
                sql.append(" and m.manager_group_id in (" + groupIds + ") ");
            }
            if (managerGroupId != null && !managerGroupId.isEmpty()) {
                sql.append(" and m.manager_group_id in (" + managerGroupId + ") ");

            }
//            if (lastGroupSubmitId != null && !lastGroupSubmitId.isEmpty()) {
//                sql.append(" and m.last_Group_Submit_id in (" + lastGroupSubmitId + ") ");
//            }

            if (statusIds != null && !statusIds.isEmpty()) {
                sql.append(" and m.request_status in (" + statusIds + ") ");
            }

            if (type != null && !type.isEmpty() && type.equalsIgnoreCase("list")) {

                if (orderBy != null && !orderBy.isEmpty()) {
                    sql.append(" order by " + orderBy + " ");
                } else {
                    sql.append(" order by m.id  ");
                }

                if (orderType != null && !orderType.isEmpty()) {
                    sql.append(" " + orderType + " ");
                } else {
                    sql.append(" desc  ");
                }
                if (!String.valueOf(limit).isEmpty() && !String.valueOf(offset).isEmpty()) {
                    offset = offset > 0 ? --offset : offset;
                    offset = offset * limit;
                    sql.append(" limit  " + limit);
                    sql.append(" offset  " + offset);
                }
            }


            Query query = entityManager.createNativeQuery(sql.toString(), Request.class);
            requests = query.getResultList().size() > 0 ? query.getResultList() : null;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return requests;
        }
    }

    @Override
    public Integer totalCount(JSONObject jsonObject) {
        Integer total = 0;
        List<Request> requests;
        try {
            requests = findByName(jsonObject);
            total = Integer.valueOf(requests.size());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return total;
        }
    }

    @Override
    public void close(String ids) {
        try {
            if (ids != null && !ids.isEmpty()) {
                String sql = "update request a set a.status = 0, request_status_name = 'Closed', request_status_id = (select id from request_status where name = 'Closed' limit 1) where id in (" + ids + ")";
                Query query = entityManager.createNativeQuery(sql.toString());
                query.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void open(String ids) {
        try {
            if (ids != null && !ids.isEmpty()) {
                String sql = "update request a set a.status = 1 where id in (" + ids + ")";
                Query query = entityManager.createNativeQuery(sql.toString());
                query.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateStatus(Long id, int status) {
        try {
            if (id != null) {
                String sql = "UPDATE request a set a.status = " +status + " WHERE a.id = " + id;
                Query query = entityManager.createNativeQuery(sql);
                query.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
