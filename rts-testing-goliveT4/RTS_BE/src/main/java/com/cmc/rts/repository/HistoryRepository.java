package com.cmc.rts.repository;

import com.cmc.rts.dto.response.CandidateDTO;
import com.cmc.rts.dto.response.HistoryStatusDTO;
import com.cmc.rts.dto.response.InterviewDTO;
import com.cmc.rts.entity.CandidateStateEnum;
import com.cmc.rts.entity.HistoryMoveLog;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HistoryRepository extends JpaRepository<HistoryMoveLog, Long> {

    @Query(value = "SELECT c from HistoryMoveLog c " +
            "WHERE c.requestId = :request_id AND c.candidateId = :candidate_id" +
            " order by c.createdDate")
    List<HistoryMoveLog> getAllHistory(@Param(value = "request_id") Long request_id,
                                               @Param(value = "candidate_id") Long candidate_id);

    @Query(value = "SELECT distinct c from HistoryMoveLog c " +
            "WHERE c.candidateId = :candidateId" +
            " order by c.createdDate desc")
    List<HistoryMoveLog> findByCandidateId(@Param(value = "candidateId") Long candidateId);
}
