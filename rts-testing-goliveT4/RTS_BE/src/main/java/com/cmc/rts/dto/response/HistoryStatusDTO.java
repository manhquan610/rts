package com.cmc.rts.dto.response;

import com.cmc.rts.entity.CandidateStateEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HistoryStatusDTO {
    private Long id;

    private CandidateStateEnum candidateState;
}
