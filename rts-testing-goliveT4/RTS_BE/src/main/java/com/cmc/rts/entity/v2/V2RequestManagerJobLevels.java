package com.cmc.rts.entity.v2;

import com.cmc.rts.entity.Request;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author: nthieu10
 * 7/25/2022
 **/
@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name="request_manager_job_levels")
@IdClass(V2RequestManagerJobLevelId.class)
public class V2RequestManagerJobLevels implements Serializable {

    @Id
    @Column(name = "request_id")
    private Long requestId;

    @Id
    @Column(name = "manager_job_levels_id")
    private Long managerJobLevelsId;
}