package com.cmc.rts.repository.custom.impl;

import com.cmc.rts.entity.ManagerMajor;
import com.cmc.rts.repository.custom.ManagerMajorRepositoryCustom;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;


@Repository
public class ManagerMajorRepositoryImpl implements ManagerMajorRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<ManagerMajor> findByNameSame(Integer first, Integer limit,  String name) {
        try {
            StringBuilder sql = new StringBuilder("select * from manager_major m where 1=1 ");

            if (name != null && !name.isEmpty()) {
                sql.append(" and m.name like '%"  + name +"%'") ;
            }

            sql.append(" ORDER BY id DESC " ) ;

            if (first != null && limit != null) {
                sql.append(" limit " + first + "," + limit);
            }
            Query query = entityManager.createNativeQuery(sql.toString(), ManagerMajor.class);
            return query.getResultList();
        } catch (Exception e) {
            return new ArrayList<>();
        }

    }

    @Override
    public List<ManagerMajor> findByName(Integer first, Integer limit,  List<String> lstName) {
        try {
            StringBuilder sql = new StringBuilder("select * from manager_major m where 1=1 ");

            if (lstName != null && !lstName.isEmpty()) {
                sql.append(" and m.name IN (NULL ") ;
                 for(String  item : lstName){
                     sql.append(" , '" + item + "'") ;
                }
                sql.append(")" ) ;
            }

            sql.append(" ORDER BY id DESC " ) ;

            if (first != null && limit != null) {
                sql.append(" limit " + first + "," + limit);
            }
            Query query = entityManager.createNativeQuery(sql.toString(), ManagerMajor.class);
            return query.getResultList();
        } catch (Exception e) {
            return new ArrayList<>();
        }

    }
}
