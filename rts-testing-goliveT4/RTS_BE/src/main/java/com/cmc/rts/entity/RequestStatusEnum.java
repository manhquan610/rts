package com.cmc.rts.entity;

public enum RequestStatusEnum {
    New,
    Pending,
    Approved,
    Approved2,
    Closed,
    Rejected,
    Assigned,
    Edited,
    Shared,
}
