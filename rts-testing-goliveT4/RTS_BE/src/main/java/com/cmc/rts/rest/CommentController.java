package com.cmc.rts.rest;

import com.cmc.rts.dto.request.CommentRequest;
import com.cmc.rts.dto.response.CommentDTO;
import com.cmc.rts.exception.CustomException;
import com.cmc.rts.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CommentController {
    @Autowired
    CommentService commentService;

    @GetMapping(value = "/comment")
    public ResponseEntity<Page<CommentDTO>> getAllComment(@RequestParam(value = "requestId") Long requestId,
                                                          Integer page,
                                                          Integer limit) {
        if(page < 1) {
            throw new CustomException("Page phải lớn hơn 1", HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
        Pageable pageable = new PageRequest(page-1, limit);
        return new ResponseEntity<>(commentService.getAllCommentsForRequest(requestId, pageable), HttpStatus.OK);
    }

    @GetMapping(value = "/comment/candidate")
    public ResponseEntity<Page<CommentDTO>> getAllCommentForCandidate(@RequestParam(value = "requestId") Long requestId,
                                                                      @RequestParam(value = "candidateId") Long candidateId,
                                                                      Integer page,
                                                                      Integer limit)
    {
        if(page < 1){
            throw new CustomException("Page phải lớn hơn 1", HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
        Pageable pageable = new PageRequest(page -1, limit);
        return new ResponseEntity<>(commentService.getAllCommentsForCandidate(requestId, candidateId, pageable), HttpStatus.OK);
    }


    @DeleteMapping(value = "/comment")
    public ResponseEntity<Void> delete(@PathVariable("parentId") Long parentId) {
        commentService.delete(parentId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/comment")
    public ResponseEntity<Void> createComment(@RequestBody CommentRequest request) {
        commentService.createComment(request);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping(value = "/comment")
    public ResponseEntity<Void> updateComment(@RequestBody CommentRequest request) {
        commentService.updateComment(request);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
