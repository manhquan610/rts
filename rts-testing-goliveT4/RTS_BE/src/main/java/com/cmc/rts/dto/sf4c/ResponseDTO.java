package com.cmc.rts.dto.sf4c;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseDTO {
    @SerializedName("d")
    private ResultsDTO d;
}
