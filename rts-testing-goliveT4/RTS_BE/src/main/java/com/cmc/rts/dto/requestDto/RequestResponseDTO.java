package com.cmc.rts.dto.requestDto;

import com.cmc.rts.dto.response.ManagerJobLevelDTO;
import com.cmc.rts.dto.response.V2RequestAssignDTO;
import com.cmc.rts.entity.*;
import com.cmc.rts.entity.v2.V2ManagerJobLevel;
import com.cmc.rts.entity.v2.V2RequestAssign;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class RequestResponseDTO {
    private Long id;
    private String name;
    private String createdBy;
    private RequestStatusEnum requestStatusName;
    private Date deadline;
    private Long projectId;
    private Integer number;
    private Long departmentId;
    private Long managerGroupId;
    private String division;
    private String department;

//    private Set<V2RequestAssign> requestAssigns;
    private Set<V2RequestAssignDTO> requestAssigns;
    private ManagerArea area;
    private ManagerJobType jobType;
    private String codePosition;

    private Set<ManagerJobLevelDTO> managerJobLevel;
    private Long totalApplicant;
    private Long totalCurrentApplicant;
    private Long totalOnboarding;
    private Long totalCurrentOnboarding;
    private ManagerPriority priority;
    private String language;
    private Date createDate;

//


    public RequestResponseDTO(Long id, String name, String createdBy, RequestStatusEnum requestStatusName, Date deadline, Long projectId, Integer number, Long departmentId, Long managerGroupId,
                              Set<RequestAssign> requestAssigns, ManagerArea area, ManagerJobType jobType, String codePosition, Set<ManagerJobLevel> managerJobLevel) {
        this.id = id;
        this.name = name;
        this.createdBy = createdBy;
        this.requestStatusName = requestStatusName;
        this.deadline = deadline;
        this.projectId = projectId;
        this.number = number;
        this.departmentId = departmentId;
        this.managerGroupId = managerGroupId;
        this.area = area;
        this.jobType = jobType;
        this.codePosition = codePosition;
    }

    public RequestResponseDTO(Long id, String name, String createdBy, RequestStatusEnum requestStatusName, Date deadline,
                              Long projectId, Integer number, Long departmentId, Long managerGroupId, Set<RequestAssign> requestAssigns,
                              ManagerArea area, ManagerJobType jobType, String codePosition, Set<ManagerJobLevel> managerJobLevel,
                              Long totalApplicant, ManagerPriority priority, String language, Date createDate, Long totalOnboarding, String division, String department
    ) {
        this.id = id;
        this.name = name;
        this.createdBy = createdBy;
        this.requestStatusName = requestStatusName;
        this.deadline = deadline;
        this.projectId = projectId;
        this.number = number;
        this.departmentId = departmentId;
        this.managerGroupId = managerGroupId;
        this.area = area;
        this.jobType = jobType;
        this.codePosition = codePosition;
        this.totalApplicant = totalApplicant;
        this.priority = priority;
        this.language = language;
        this.createDate = createDate;
        this.totalOnboarding = totalOnboarding;
        this.division = division;
        this.department = department;
    }
}