package com.cmc.rts.utils;

import com.cmc.rts.exception.CustomException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;

@Component
public class Common {

    @Value("${sf4c.username}")
    private String username;

    @Value("${sf4c.password}")
    private String password;

    public <T> T callAPIRestTemplate(String body, String url, MediaType type, HttpMethod httpMethod, Class<T> obj) {
        T javaObject = null;

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
        restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(username, password));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.ALL_VALUE);
        headers.setContentType(type);
        HttpEntity<String> entity = new HttpEntity<>(body, headers);
        ResponseEntity<String> response = restTemplate.exchange(url, httpMethod, entity, String.class);
        if (response.getStatusCode().value() != 200) {
            throw new CustomException("Error call API upload file: " + response.getBody(), 500);
        }
        try {
            Gson gson = new GsonBuilder().create();
            String a = response.getBody();
            javaObject = gson.fromJson(response.getBody(), obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return javaObject;
    }

}
