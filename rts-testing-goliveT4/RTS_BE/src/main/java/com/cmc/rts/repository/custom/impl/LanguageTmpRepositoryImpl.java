package com.cmc.rts.repository.custom.impl;

import com.cmc.rts.entity.LanguageTmp;
import com.cmc.rts.repository.custom.LanguageTmpRepositoryCustom;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Repository
public class LanguageTmpRepositoryImpl implements LanguageTmpRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<LanguageTmp> findByName(String name) {
        try {
            StringBuilder sql = new StringBuilder("select * from language_tmp m where 1=1");

            if (name != null && !name.isEmpty()) {
                sql.append(" and m.name like '%" + name + "%'");
            }
            Query query = entityManager.createNativeQuery(sql.toString(), LanguageTmp.class);
            return query.getResultList();
        } catch (Exception e) {
            return new ArrayList<>();
        }

    }
}
