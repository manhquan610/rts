package com.cmc.rts.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table
public class Project extends CategoryBaseDomain {
    @Column
    private Date staDate;
    @Column
    private Date endDate;
    @Column(unique = true)
    private Long projectId; // id from api nghiep vu
}
