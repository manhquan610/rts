package com.cmc.rts.dto.requestDto;

import lombok.Getter;

@Getter
public class  CmcDTO {
    private Long id;
    private String name;
    private String description;
    private String manager;
    private Boolean developmentUnit;
    private Boolean groupSale;
    private String depth;
    private Long parentId;
}
