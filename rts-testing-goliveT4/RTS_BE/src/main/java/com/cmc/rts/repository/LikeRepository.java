package com.cmc.rts.repository;
import com.cmc.rts.entity.Like;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.Date;
import java.util.List;


@Repository
public interface LikeRepository extends JpaRepository<Like,Long> {

    @Query(value = "SELECT l " +
            "FROM Like l " +
            "WHERE l.createdBy = :ldap AND l.candidateId = :candidate_id ")
    Like getLiked(@Param(value = "ldap") String ldap,
                  @Param(value = "candidate_id") Long candidate_id);

    @Query(value = "SELECT l.createdBy " +
            "FROM Like l " +
            "WHERE l.liked = :liked  AND l.candidateId = :candidate_id ")
    List<String> getReactUser(@Param(value = "liked") Boolean liked,
                              @Param(value = "candidate_id") Long candidate_id);


    @Query(value = "SELECT count(l)" +
            "from Like l " +
            "where l.candidateId = :candidateID and l.liked = :liked")
    Integer count(@Param(value = "candidateID") Long candidateId,@Param(value = "liked") Boolean liked);

}
