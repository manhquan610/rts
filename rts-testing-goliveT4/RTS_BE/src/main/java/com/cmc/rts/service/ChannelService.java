package com.cmc.rts.service;

import com.cmc.rts.dto.response.ChannelRootResponse;
import com.cmc.rts.dto.response.ChannelSourceResponse;

import java.util.List;

public interface ChannelService {

    ChannelRootResponse getChannel();

    ChannelSourceResponse getChannelBySource(String source);

    List<ChannelSourceResponse> getAllSources();

}
