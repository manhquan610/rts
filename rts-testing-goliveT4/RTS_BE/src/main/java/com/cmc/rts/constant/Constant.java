package com.cmc.rts.constant;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @author: nthieu10
 * 7/22/2022
 **/
public class Constant {
    public static final List<String> LANGUAGES = Arrays.asList("Mandarin Chinese", "Spanish", "English", "Hindi", "Bengali", "Portuguese",
            "Russian", "Japanese", "Western Punjabi", "Marathi", "Telugu", "Wu Chinese",
            "Turkish", "Korean", "French", "German", "Vietnamese", "Tamil", "Yue Chinese",
            "Urdu", "Javanese", "Italian", "Egyptian Arabic", "Gujarati", "Iranian Persian",
            "Bhojpuri", "Min Nan Chinese", "Hakka Chinese", "Jin Chinese", "Hausa",
            "Kannada", "Indonesian", "Polish", "Yoruba", "Xiang Chinese", "Malayalam",
            "Odia", "Maithili", "Burmese", "Eastern Punjabi", "Sunda", "Sudanese Arabic",
            "Algerian Arabic", "Moroccan Arabic", "Ukrainian", "Igbo", "Northern Uzbek", "Sindhi",
            "North Levantine Arabic", "Romanian", "Tagalog", "Dutch", "Saʽidi Arabic", "Gan Chinese",
            "Amharic", "Northern Pashto" , "Magahi", "Thai", "Saraiki", "Khmer", "Chhattisgarhi",
            "Somali", "Malay (Malaysian Malay)", "Cebuano", "Nepali", "Mesopotamian Arabic",
            "Assamese", "Sinhalese", "Northern Kurdish", "Hejazi Arabic", "Nigerian Fulfulde",
            "Bavarian", "South Azerbaijani", "Greek", "Chittagonian", "Kazakh", "Deccan",
            "Hungarian", "Kinyarwanda", "Zulu", "South Levantine Arabic", "Tunisian Arabic",
            "Sanaani Spoken Arabic", "Min Bei Chinese", "Southern Pashto", "Rundi", "Czech",
            "Taʽizzi-Adeni Arabic", "Uyghur", "Min Dong Chinese", "Sylheti");


    public static AtomicInteger i = new AtomicInteger(-1);
    public static final Map<Integer, String> languagueMap = LANGUAGES.stream().collect(Collectors.toMap(v -> i.incrementAndGet(), v -> v));
}
