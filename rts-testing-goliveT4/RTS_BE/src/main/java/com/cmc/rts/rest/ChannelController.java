package com.cmc.rts.rest;

import com.cmc.rts.dto.response.CandidateRootResponse;
import com.cmc.rts.dto.response.ChannelRootResponse;
import com.cmc.rts.dto.response.ChannelSourceResponse;
import com.cmc.rts.service.ChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@PropertySource("classpath:application-dev.properties")
public class ChannelController {
    @Autowired
    private ChannelService channelService;

    @GetMapping(value = "/getChannel")
    public ResponseEntity<ChannelRootResponse> getChannel() throws IOException{
        ChannelRootResponse channelRootResponse = channelService.getChannel();
        return new ResponseEntity<>(channelRootResponse, HttpStatus.OK);
    }

    @GetMapping(value = "/getChannelBySource")
    public ResponseEntity<ChannelSourceResponse> getChannelBySource(@RequestParam(value = "source") String source) {
        ChannelSourceResponse channelRootResponse = channelService.getChannelBySource(source);
        return new ResponseEntity<>(channelRootResponse, HttpStatus.OK);
    }

    @GetMapping(value = "/getAllSource")
    public ResponseEntity<?> getAllSource() {
        return new ResponseEntity<>(channelService.getAllSources(), HttpStatus.OK);
    }


}
