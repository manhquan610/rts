package com.cmc.rts.service;

import com.cmc.rts.dto.sf4c.ResponseDTO;
import com.cmc.rts.dto.sf4c.dto.PositionListResponse;

import java.util.List;

public interface SF4CService {

    /*sf4c - api*/
    //Get_UserId
    ResponseDTO getUserId(String userName);

    //Get_Code_Position
    ResponseDTO getCodePosition(String userId);

    //Get_Child_Code_Position
    ResponseDTO getChildCodePosition(String parentPosition);

    //Get_Detail_FTE_Code_Position
    ResponseDTO getDetailFTECodePosition(String positionNav);

    //JobRequisition
    ResponseDTO getDetailJobRequisition();

    //Get_Position_By_Code
    ResponseDTO getPositionByCode(String code);

    List<PositionListResponse> getListPosition();

    List<PositionListResponse> getListIDRequest();

}
