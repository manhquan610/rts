package com.cmc.rts.rest;


import com.cmc.rts.dto.requestDto.JobLevelRequest;
import com.cmc.rts.dto.response.ResponseAPI;
import com.cmc.rts.entity.ManagerJobLevel;
import com.cmc.rts.exception.CustomException;
import com.cmc.rts.service.ManagerJobLevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
@RequestMapping("/jobLevel")
public class ManagerJobLevelController {

    @Autowired
    private ManagerJobLevelService jobLevelService;

    @GetMapping(value = "/getAll")
    public ResponseEntity<Page<ManagerJobLevel>> searchByName(@RequestParam(value = "limit") Integer limit,
                                                              @RequestParam(value = "page") Integer page) {
        Pageable pageable = new PageRequest(page - 1, limit);
        return new ResponseEntity<>(jobLevelService.getAllJobLevels(pageable), HttpStatus.OK);
    }

    @GetMapping(value = "/searchName")
    public ResponseEntity<Page<ManagerJobLevel>> searchByName(@RequestParam(value = "nameSearch", required = false) String name,
                                                              @RequestParam(value = "limit") Integer limit,
                                                              @RequestParam(value = "page") Integer page) {
        Pageable pageable = new PageRequest(page - 1, limit);
        return new ResponseEntity<>(jobLevelService.searchByNameLevel(name, pageable), HttpStatus.OK);
    }

    @GetMapping(value = "/searchByJobType")
    public ResponseEntity<?> searchByJobType(@RequestParam(value = "idJobType", required = false) Long idJobType,
                                                              @RequestParam(value = "limit") Integer limit,
                                                              @RequestParam(value = "page") Integer page) {
        Pageable pageable = new PageRequest(page - 1, limit);
        Page<ManagerJobLevel> managerJobLevels = jobLevelService.findAllJobLevelsByPosition(idJobType, pageable);
        if (managerJobLevels == null){
            return new ResponseEntity<>(new ResponseAPI(400, "Error search By Job Type"), HttpStatus.OK);
        }
        return new ResponseEntity<>(managerJobLevels, HttpStatus.OK);
    }

    @PostMapping(value = "/insert")
    public ResponseEntity<?> insertJobLevel(@RequestBody JobLevelRequest jobLevelRequest){
        return new ResponseEntity<>(jobLevelService.insertJobLevel(jobLevelRequest), HttpStatus.OK);
    }

    @PostMapping(value = "/update")
    public ResponseEntity<?> updateJobLevel(@RequestBody JobLevelRequest jobLevelRequest){
        return new ResponseEntity<>(jobLevelService.updateJobType(jobLevelRequest), HttpStatus.OK);
    }

    @GetMapping(value = "/delete")
    public ResponseEntity<?> deleteJobLevel(@RequestParam Long idJobLevel) throws CustomException {
        return new ResponseEntity<>(jobLevelService.deleteJobType(idJobLevel), HttpStatus.OK);
    }




}
