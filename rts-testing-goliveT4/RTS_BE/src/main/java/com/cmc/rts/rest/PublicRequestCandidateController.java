package com.cmc.rts.rest;

import com.cmc.rts.dto.public_dto.RequestCandidateDTO;
import com.cmc.rts.public_service.PublicRequestCandidateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("public")
@Slf4j
public class PublicRequestCandidateController {

    @Autowired
    private PublicRequestCandidateService publicRequestCandidateService;

    @PostMapping("request_candidate_log/create")
    public void requestCandidateLogCreate(@RequestBody RequestCandidateDTO dto) {
        publicRequestCandidateService.requestCandidateLog(dto);
    }
}
