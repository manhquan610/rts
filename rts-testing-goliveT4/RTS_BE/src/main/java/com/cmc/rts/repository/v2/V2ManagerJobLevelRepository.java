package com.cmc.rts.repository.v2;

import com.cmc.rts.entity.ManagerJobLevel;
import com.cmc.rts.entity.v2.V2ManagerJobLevel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.QueryHints;

import javax.persistence.QueryHint;
import java.util.List;

/**
 * @author: nthieu10
 * 7/26/2022
 **/
public interface V2ManagerJobLevelRepository extends JpaRepository<V2ManagerJobLevel, Long> {

    @QueryHints(value = { @QueryHint(name = org.hibernate.jpa.QueryHints.HINT_CACHEABLE, value = "true")})
    List<V2ManagerJobLevel> findV2ManagerJobLevelByIdIn(List<Long> Ids);
}
