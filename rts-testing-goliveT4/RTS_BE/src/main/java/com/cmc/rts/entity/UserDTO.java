package com.cmc.rts.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"menuTabs", "userInfo"})
public class UserDTO {
    private String token;
    private String refreshToken;
    private String code;
    private boolean status;
    private String errorMessage;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date time4Hout;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date time30POut;
    public User user;
    private List<MenuTabs> menuTabs;
    private DepartmentGroup departmentGroup;
    private String department;
    private ManagerGroup managerGroup;
    private List<Groups> groups;

    public String getUserName() {
        return user.getUserName();
    }

    public Set<String> getUserRoles() {
        return user.getRoles().stream().map(x -> x.getName()).collect(Collectors.toSet());
    }

    public String getGroupIds() {
        StringBuilder groupIds = new StringBuilder();
        groups.stream().forEach(x -> {
            groupIds.append(x.getGroupId()).append(",");
        });
        return groupIds.toString();
    }

    public String getGroupName(){
        return !CollectionUtils.isEmpty(groups) ? groups.stream().map(x -> x.getGroupName()).findFirst().get() : "";
    }

    public UserDTO(String token, String refreshToken, String code, boolean status, String errorMessage, Date time4Hout, Date time30POut, User user, List<MenuTabs> menuTabs) {
        this.token = token;
        this.refreshToken = refreshToken;
        this.code = code;
        this.status = status;
        this.errorMessage = errorMessage;
        this.time4Hout = time4Hout;
        this.time30POut = time30POut;
        this.user = user;
        this.menuTabs = menuTabs;
    }
}

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"system", "image"})
class User {
    public String id;
    public String fullName;
    public String firstName;
    public String lastName;
    public String userName;
    public String email;
    public String phone;
    public String dateOfBirth;
    public String externalInfo;
    public String errorMessage;
    public boolean status;
    public String createdDate;
    public String updatedDate;
    public String createdBy;
    public String updatedBy;
    public Image image;
    public System system;
    public List<Permissions> permissions;
    public List<UserRoles> roles;

    public User(String id, String fullName, String firstName, String lastName, String userName, String email, String phone, String dateOfBirth, String externalInfo, String errorMessage, boolean status, String createdDate, String updatedDate, String createdBy, String updatedBy, Image image, System system, List<Permissions> permissions) {
        this.id = id;
        this.fullName = fullName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
        this.email = email;
        this.phone = phone;
        this.dateOfBirth = dateOfBirth;
        this.externalInfo = externalInfo;
        this.errorMessage = errorMessage;
        this.status = status;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
        this.createdBy = createdBy;
        this.updatedBy = updatedBy;
        this.image = image;
        this.system = system;
        this.permissions = permissions;
    }
}

@Getter
@Setter
@NoArgsConstructor
class Image {
    private String src;

    public Image(String src) {
        this.src = src;
    }
}

@Getter
@Setter
@NoArgsConstructor
class System {
    private String name;
    private String key;
    private boolean enabled;

    public System(String name, String key, boolean enabled) {
        this.name = name;
        this.key = key;
        this.enabled = enabled;
    }
}

@Getter
@Setter
@NoArgsConstructor
class Permissions {
    private String name;
    private String key;
    private boolean enabled;
    private Page page;
    private List<Activities> activities;

    public Permissions(String name, String key, boolean enabled, Page page, List<Activities> activities) {
        this.name = name;
        this.key = key;
        this.enabled = enabled;
        this.page = page;
        this.activities = activities;
    }
}

@Getter
@Setter
@NoArgsConstructor
class Page {
    private String name;
    private String key;
    private String state;
    private boolean enabled;
    private String parent;

    public Page(String name, String key, String state, boolean enabled, String parent) {
        this.name = name;
        this.key = key;
        this.state = state;
        this.enabled = enabled;
        this.parent = parent;
    }
}

@Getter
@Setter
@NoArgsConstructor
class Activities {
    private String name;
    private String key;
    private boolean enabled;

    public Activities(String name, String key, boolean enabled) {
        this.name = name;
        this.key = key;
        this.enabled = enabled;
    }
}

@Getter
@Setter
@NoArgsConstructor
class UserRoles {
    private String name;
    private String key;
    private boolean enabled;
    private List<Permissions> permissions;

    public UserRoles(String name, String key, boolean enabled, List<Permissions> permissions) {
        this.name = name;
        this.key = key;
        this.enabled = enabled;
        this.permissions = permissions;
    }
}

@Getter
@Setter
@NoArgsConstructor
class MenuTabs {
    private String role;
    private List<Items> items;

    public MenuTabs(String role, List<Items> items) {
        this.role = role;
        this.items = items;
    }
}

@Getter
@Setter
@NoArgsConstructor
class Items {
    private String mainState;
    private String state;
    private String type;
    private String name;
    private String label;
    private String icon;
    private List<Children> children;

    public Items(String mainState, String state, String type, String name, String label, String icon, List<Children> children) {
        this.mainState = mainState;
        this.state = state;
        this.type = type;
        this.name = name;
        this.label = label;
        this.icon = icon;
        this.children = children;
    }
}

@Getter
@Setter
@NoArgsConstructor
class Children {
    private String mainState;
    private String state;
    private String type;
    private String name;
    private String label;
    private String icon;
    private String children;

    public Children(String mainState, String state, String type, String name, String label, String icon, String children) {
        this.mainState = mainState;
        this.state = state;
        this.type = type;
        this.name = name;
        this.label = label;
        this.icon = icon;
        this.children = children;
    }
}

