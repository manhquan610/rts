package com.cmc.rts.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "request_assign")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"code", "request", "version", "userInfo"})
public class RequestAssign extends BaseDomain {
    private Long assignId;
    private String assignName;
    private String assignLdap;

    @ManyToOne
    @JoinColumn(name = "request_id", referencedColumnName = "id")
    private Request request;

    @Column(name = "target")
    private int target;
    @Column(name = "applied", columnDefinition = "integer default 0")
    private int applied;
    @Column(name = "contacting", columnDefinition = "integer default 0")
    private int contacting;
    @Column(name = "interview", columnDefinition = "integer default 0")
    private int interview;
    @Column(name = "other", columnDefinition = "integer default 0")
    private int other;
    @Column(name = "onboard", columnDefinition = "integer default 0")
    private int onboard;

}
