package com.cmc.rts.public_service;

import com.cmc.rts.dto.public_dto.RequestCandidateDTO;

public interface PublicRequestCandidateService {
    void requestCandidateLog(RequestCandidateDTO dto);
}
