package com.cmc.rts.repository.custom;

import com.cmc.rts.entity.LogException;

import java.util.List;

public interface LogExceptionRepositoryCustom {
    List<LogException> findByName(String name);
}
