package com.cmc.rts.repository;

import com.cmc.rts.entity.ManagerArea;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ManagerAreaRepository extends JpaRepository<ManagerArea, Long> {
    @Query(value = "SELECT m " +
            "FROM ManagerArea m " +
            "where m.deleted = false and m.name like %:searchValue%  order by m.id desc")
    Page<ManagerArea> findByName(@Param(value = "searchValue") String searchValues,
                                        Pageable pageable);

    @Query(value = "SELECT m " +
            "FROM ManagerArea m " +
            " where m.deleted = false and (:nameNotEmpty = false or m.name in :lstName) order by m.id desc ")
    Page<ManagerArea> findByListName(@Param(value = "lstName") List<String> listName,
                                     @Param("nameNotEmpty") Boolean nameNotEmpty,
                                     Pageable pageable);

    @Query(value = "update " +
            "ManagerArea m set m.deleted = true" +
            " where m.id = :id")
    void deleteAreas(@Param(value = "id") Long id);

    @Query(value = "SELECT m " +
            "FROM ManagerArea m " +
            "where m.deleted = false  order by m.id desc")
    Page<ManagerArea> findAllArea(Pageable pageable);

    @Query(value = "SELECT m " +
            "FROM ManagerArea m " +
            "where (:areaNotEmpty = false or m.id in :areaIds ) ")
    List<ManagerArea> findManagerAreaByParams(
            @Param("areaIds") List<Long> areaIds,
            @Param("areaNotEmpty") Boolean areaNotEmpty);

}
