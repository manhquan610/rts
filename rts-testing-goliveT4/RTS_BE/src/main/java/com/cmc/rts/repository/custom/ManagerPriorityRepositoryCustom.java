package com.cmc.rts.repository.custom;

import com.cmc.rts.entity.ManagerPriority;

import java.util.List;

public interface ManagerPriorityRepositoryCustom {
    List<ManagerPriority> findByNameSame(Integer first, Integer limit, String name);
    List<ManagerPriority> findByName(Integer first, Integer limit, List<String> name);
}
