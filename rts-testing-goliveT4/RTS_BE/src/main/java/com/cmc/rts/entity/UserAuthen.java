package com.cmc.rts.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserAuthen {
    private String username;
    private String password;
    private String key;
    private String secret;

    public UserAuthen(String username, String password, String key, String secret) {
        this.username = username;
        this.password = password;
        this.key = key;
        this.secret = secret;
    }

    @Override
    public String toString() {
        return "{\n" +
                "    \"payload\": {\n" +
                "        \"username\": \"" + username + "\",\n" +
                "        \"password\": \"" + password + "\",\n" +
                "        \"key\": \"" + key + "\",\n" +
                "        \"secret\": \"" + secret + "\"\n" +
                "    }\n" +
                "}";
    }
}
