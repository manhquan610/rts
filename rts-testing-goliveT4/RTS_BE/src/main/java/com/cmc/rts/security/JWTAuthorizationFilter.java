package com.cmc.rts.security;

import com.cmc.rts.entity.UserDTO;
import com.google.gson.JsonObject;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.cmc.rts.utils.Constants.Security.AUTHORIZATION;
import static com.cmc.rts.utils.Constants.Security.BEARER;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {
    public JWTAuthorizationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }
    @Transactional
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        String header = request.getHeader(AUTHORIZATION);
        String urlRequest = request.getRequestURL().toString();
        if (header == null || !header.startsWith(BEARER)) {
            chain.doFilter(request, response);
            return;
        }
        UsernamePasswordAuthenticationToken authentication = null;
        header = header.replace(BEARER, "");

        UserDTO userLogin = AuthenSession.authenSessionToken.get(header);
        boolean flag = true;
        if (userLogin == null) {
            JsonObject dataRes = new JsonObject();
            dataRes.addProperty("code", "ITCT_666");
            dataRes.addProperty("errorMessage", "Token expired!");
            response.getWriter().write(dataRes.toString());
            System.out.println("------------- Status: Token expired!");
            flag = false;
            return;
        }
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        userLogin.getUserRoles().stream().forEach(x -> {
            SimpleGrantedAuthority authority = new SimpleGrantedAuthority(x);
            grantedAuthorities.add(authority);
        });
       authentication = new UsernamePasswordAuthenticationToken(userLogin.getUserName(),
               null, grantedAuthorities);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request, response);
    }
}