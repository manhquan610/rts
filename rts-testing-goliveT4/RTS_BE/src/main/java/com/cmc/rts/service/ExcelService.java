package com.cmc.rts.service;

import com.cmc.rts.dto.request.CreateAssignRequest;
import com.cmc.rts.dto.request.DataAssignRequest;
import com.cmc.rts.dto.request.TargetAssigned;
import com.cmc.rts.dto.requestDto.RequestDTO;
import com.cmc.rts.entity.Request;
import com.cmc.rts.repository.RequestRepository;
import com.cmc.rts.utils.ExcelHelper;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

@Service
public class ExcelService {

    @Autowired
    ExcelHelper excelHelper;

    @Autowired
    RequestService requestService;

    @Autowired
    RequestRepository requestRepository;

    public String saveExcelData(MultipartFile file) {
        try {
            List<RequestDTO> requestDTOS = excelHelper.excelToRequests(file.getInputStream());
            for (RequestDTO requestDTO : requestDTOS) {
                requestService.saveAllRequest(requestDTO);
            }
            return "Done!";
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String assignedHRRequest(MultipartFile file) {
        try {
            //get data to file excel, rm request not assigned
            List<RequestDTO> requestDTOS = excelHelper.getAllDataExcelRequests(file.getInputStream());

            //handle data
            Map<String, List<DataAssignRequest>> checkAssignee = new HashMap<>();
            for (RequestDTO r : requestDTOS) {
                DataAssignRequest dto = new DataAssignRequest();
                dto.setAssignLdap(r.getAssigned());
                dto.setTarget(1);
                dto.setHeadCount(r.getNumber());
                if (checkAssignee.isEmpty()) {
                    List<DataAssignRequest> requestList = new ArrayList<>();
                    requestList.add(dto);
                    checkAssignee.put(r.getCode(), requestList);
                } else {
                    if (checkAssignee.get(r.getCode()) == null) {
                        List<DataAssignRequest> requestList = new ArrayList<>();
                        requestList.add(dto);
                        checkAssignee.put(r.getCode(), requestList);
                    } else {
                        List<DataAssignRequest> requestList = checkAssignee.get(r.getCode());
                        requestList.add(dto);
                        checkAssignee.put(r.getCode(), requestList);
                    }
                }
            }
            Map<String, List<DataAssignRequest>> response = new HashMap<>();
            checkAssignee.forEach((key, value) -> {
                Map<String, TargetAssigned> newSet = new HashMap<>();
                value.forEach(d -> {
                    TargetAssigned assigned = new TargetAssigned();
                    assigned.setTarget(d.getTarget());
                    assigned.setHeadCount(d.getHeadCount());
                    if (newSet.containsKey(d.getAssignLdap())) {
                        if (newSet.get(d.getAssignLdap()).getHeadCount() == d.getHeadCount()) {
                            int value111 = newSet.get(d.getAssignLdap()).getTarget() + 1;
                            assigned.setTarget(value111);
                            newSet.put(d.getAssignLdap(), assigned);
                        }
                        newSet.put(d.getAssignLdap(), assigned);
                    } else {
                        newSet.put(d.getAssignLdap(), assigned);
                    }

                });
                List<DataAssignRequest> newList = new ArrayList<>();
                newSet.forEach((key2, value2) -> {
                    DataAssignRequest dataAssignRequest = new DataAssignRequest();
                    dataAssignRequest.setTarget(value2.getTarget());
                    dataAssignRequest.setAssignLdap(key2);
                    dataAssignRequest.setHeadCount(value2.getHeadCount());
                    newList.add(dataAssignRequest);
                });
                response.put(key, newList);
            });

            //submit assigned
            Map<Long, List<CreateAssignRequest>> submit = new HashMap<>();
            response.forEach((key, value) -> {
                List<CreateAssignRequest> assignRequestList = new ArrayList<>();
                List<Request> request = requestRepository.getAllByCode(key);
                if (!request.isEmpty()) {
                    for (Request r : request) {
                        value.forEach(d -> {
                            CreateAssignRequest dto = new CreateAssignRequest();
                            if (d.getHeadCount() == r.getNumber()) {
                                dto.setAssignLdap(d.getAssignLdap());
                                dto.setTarget(d.getTarget());
                                assignRequestList.add(dto);
                            }
                        });
                        submit.put(r.getId(), assignRequestList);
                    }
                }
            });
            submit.forEach((key, value) -> {
                requestService.createAllRequestAssign(key, value);
            });
            return "Done!";
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
