package com.cmc.rts.repository.custom;

import com.cmc.rts.entity.EmailQueue;

import java.util.List;

public interface EmailQueueRepositoryCustom {
    List<EmailQueue> findByName(String name, int offset, int limit);
}
