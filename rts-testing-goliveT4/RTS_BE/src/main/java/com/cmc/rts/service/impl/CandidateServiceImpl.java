package com.cmc.rts.service.impl;

import com.cmc.rts.dto.requestDto.UpdateCandidateOutputRequest;
import com.cmc.rts.dto.response.*;
import com.cmc.rts.entity.*;
import com.cmc.rts.entity.v2.V2Request;
import com.cmc.rts.exception.CustomException;
import com.cmc.rts.repository.*;
import com.cmc.rts.repository.v2.V2RequestRepository;
import com.cmc.rts.response.SingleResponseDTO;
import com.cmc.rts.service.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.*;

import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;

@Service
@Slf4j
public class CandidateServiceImpl implements CandidateService {
    @Autowired
    CommonService commonService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private InterviewService interviewService;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private CandidateService candidateService;

    @Autowired
    private V2RequestRepository v2RequestRepository;

    @Autowired
    private ChannelService channelService;

    @Autowired
    RequestService requestService;

    private final String CANDIDATE_TOKEN = "Bearer Oo2V-DGze-";

    @Value("${candidate.list.api.url}")
    private String candidateApiUrl;

    @Value("${candidate.list.path}")
    private String candidateListPath;

    @Value("${candidate.update.path}")
    private String candidateUpdatePath;

    @Value("${candidate.getList.candidate}")
    private String candidateListPath2;

    @Value("${candidate.detail.path}")
    private String candidateFindByIdPath;

    @Value("${candidate.findbyjob.path}")
    private String candidateFindByJobId;

    @Value("${candidate.delete.path}")
    private String candidateDeletePath;

    @Value("${candidate.getTotal.step}")
    private String totalCandidateStep;

    @Value("${candidate.getTotal.ta}")
    private String totalCandidateTA;

    @Value("${candidate.getTotal.tav2}")
    private String totalCandidateTAv2;

    @Value("${candidate.check}")
    private String candidateCheck;


    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private RequestCandidatesRepository requestCandidatesRepository;

    @Autowired
    private RequestRepository requestRepository;

    @Autowired
    private CandidateLogRepository candidateLogRepository;

    @Autowired
    private CandidateRepository candidateRepository;

    @Autowired
    private HistoryRepository historyRepository;

    @Override
    public List<CandidateRootResponse> searchCandidates(List<Long> requestId, CandidateStateEnum candidateState, String searchValues,
                                                        Integer page, Integer size, String assignees, String skill, String experience, String ta) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        List roleNames = (List) authentication.getAuthorities();
        String url = candidateApiUrl + candidateFindByJobId + "?jobRtsId=" + requestId
                + "&stepRts=" + candidateState
                + "&role_name=" + roleNames.get(1)
                + "&findValue=" + searchValues + "&page=" + page
                + "&size=" + size + "&orderBy=apply_since&sort=DESC" + "&ta=" + authentication.getName();
        if (assignees != null && !assignees.isEmpty() && assignees != "") {
            url += "&assignees=" + assignees;
        }
        if (skill != null && !skill.isEmpty() && skill != "") {
            url += "&skill=" + skill;
        }
        if (experience != null && !experience.isEmpty() && experience != "") {
            url += "&experience=" + experience;
        }
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", CANDIDATE_TOKEN);
        headers.setContentType(MediaType.APPLICATION_JSON);
        CandidateRootResponse[] response = restTemplate.getForObject(url, CandidateRootResponse[].class, headers);
        List<CandidateRootResponse> candidateRootResponse = new ArrayList<>();
        try {
            candidateRootResponse = Arrays.asList(response);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return candidateRootResponse;
    }

    public List<TotalCandidateOfRequestResponse> searchCandidateList(List<Long> requestId, CandidateStateEnum candidateState) {
        String jobRtsId = requestId.stream().map(v -> String.valueOf(v)).collect(Collectors.joining(","));
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        List roleNames = (List) authentication.getAuthorities();
        String url = candidateApiUrl + candidateListPath2 + "?jobRtsId=" + jobRtsId
                + "&stepRts=" + candidateState
                + "&role_name=" + roleNames.get(1) + "&orderBy=apply_since&sort=DESC" + "&ta=" + authentication.getName()
                + "&orderBy=apply_since&sort=DESC" + "&page=1&&size=1000";

        log.info("url {}", url);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", CANDIDATE_TOKEN);
        headers.setContentType(MediaType.APPLICATION_JSON);
        TotalCandidateOfRequestResponse[] response = restTemplate.getForObject(url, TotalCandidateOfRequestResponse[].class, headers);
        List<TotalCandidateOfRequestResponse> candidateRootResponse = new ArrayList<>();
        try {
            candidateRootResponse = Arrays.asList(response);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return candidateRootResponse;
    }

    @Override
    public CandidateRootResponse searchSource(Long requestId, CandidateStateEnum candidateState, String searchValues,
                                              Integer page, Integer size, String hasCV, Date startDate, Date endDate, String lastCompany) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        List roleNames = (List) authentication.getAuthorities();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String url = candidateApiUrl + candidateFindByJobId + "?jobRtsId=" + requestId
                + "&stepRts=" + candidateState
                + "&role_name=" + roleNames.get(1)
                + "&findValue=" + searchValues + "&page=" + page
                + "&size=" + size + "&ta=" + authentication.getName();
        if (hasCV != null && !hasCV.isEmpty() && hasCV != "") {
            url += "&hasCV=" + hasCV;
        }
        if (startDate != null && endDate != null) {
            url += "&startDate=" + simpleDateFormat.format(startDate) + "&endDate=" + simpleDateFormat.format(endDate);
        }
        if (lastCompany != null && !lastCompany.isEmpty() && lastCompany != "") {
            url += "&lastCompany=" + lastCompany;
        }
        log.info("url {}", url);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", CANDIDATE_TOKEN);
        headers.setContentType(MediaType.APPLICATION_JSON);
        String response = restTemplate.getForObject(url, String.class, headers);
        CandidateRootResponse candidateRootResponse = new CandidateRootResponse();
        try {
            candidateRootResponse = objectMapper.readValue(response, CandidateRootResponse.class);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return candidateRootResponse;
    }


    @Override
    public CandidateResponse findCandidateById(Long id) throws IOException {
        String url = candidateApiUrl + candidateFindByIdPath + "?candidateId=" + id;
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", CANDIDATE_TOKEN);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity requestHeader = new HttpEntity(headers);

        ChannelRootResponse channelRootResponse = null;
        Map<Long, ChannelResponse> channelResponseMap = null;
        try {
            channelRootResponse = channelService.getChannel();
            channelResponseMap = channelRootResponse.getChannelResponseList().stream().collect(Collectors.toMap(ChannelResponse::getId, v -> v));
        } catch (Exception e) {
            log.info(e.getMessage());
        }

        try {
            String response = restTemplate.getForObject(url, String.class, requestHeader);
            CandidateResponse candidateResponse = objectMapper.readValue(response, CandidateResponse.class);
            if (!ObjectUtils.isEmpty(channelResponseMap) && !ObjectUtils.isEmpty(candidateResponse.getChannel())) {
                ChannelResponse channelResponse = channelResponseMap.get(candidateResponse.getChannel());
                if (!ObjectUtils.isEmpty(channelResponse)) {
                    candidateResponse.setChannelName(channelResponse.getName());
                }
            }
            return candidateResponse;
        } catch (CustomException e) {
            throw new CustomException("Không tìm thấy user có id là " + id);
        }
    }

    @Override
    @Transactional(dontRollbackOn = CustomException.class)
    public CandidateResponse[] updateCandidateState(String ids, String candidateStateCurrent, CandidateStateEnum candidateState, Long requestId, String assignees, String ta) throws Exception {
        List<Long> listIds = Arrays.asList(ids.split(",")).stream().map(x -> Long.parseLong(x)).collect(Collectors.toList());
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        HttpHeaders headers = new HttpHeaders();
        List<HistoryMoveLog> historyMoveLogList = new ArrayList<HistoryMoveLog>();
        headers.set("Authorization", CANDIDATE_TOKEN);
        headers.setContentType(MediaType.APPLICATION_JSON);
        String url = candidateApiUrl + candidateUpdatePath;
        V2Request v2Request = v2RequestRepository.findV2RequestById(requestId);
        UpdateCandidateOutputRequest candidateOutputRequest = new UpdateCandidateOutputRequest(listIds, candidateState.toString(), requestId.toString(), v2Request.getCreatedBy(), ta);
        HttpEntity requestEntity = new HttpEntity(candidateOutputRequest, headers);
        try {
            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, requestEntity, String.class);

            CandidateResponse[] candidateResponses = objectMapper.readValue(response.getBody(), CandidateResponse[].class);
            for (CandidateResponse candidateResponse :  candidateResponses) {
                if (candidateResponse.getErrorMessage().equals("success")) {
                    HistoryMoveLog historyMoveLog = new HistoryMoveLog();
                    historyMoveLog.setCandidateId(candidateResponse.getCandidateId());
                    historyMoveLog.setRequestId(requestId);
                    historyMoveLog.setCandidateBefore(CandidateStateEnum.valueOf(candidateStateCurrent));
                    historyMoveLog.setCandidateAfter(candidateState);
                    historyMoveLog.setCreatedDate(new Timestamp(System.currentTimeMillis()));
                    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                    historyMoveLog.setCreatedBy(authentication.getPrincipal().toString());
                    historyMoveLogList.add(historyMoveLog);
                }
            }
            historyService.createHistory(historyMoveLogList);
            //Auto Report
//            if (StringUtils.equals(candidateStateCurrent, "offer") && candidateState.equals(CandidateStateEnum.onboard)) {
//                Request request = requestRepository.getOne(requestId);
//                int number = request.getNumber();
//                CandidateRootResponse candidateDTOS = candidateService.searchSource(requestId, CandidateStateEnum.onboard, "",
//                        1, 5, null, null, null, null);
//                int totalOnboard = candidateDTOS.getTotalOnboard();
//
//                if (number <= totalOnboard) {
//                    request.setRequestStatusName(RequestStatusEnum.Closed);
//                    requestRepository.save(request);
//                }
//            }
            return candidateResponses;
        } catch (HttpServerErrorException | HttpClientErrorException | UnknownHttpStatusCodeException e) {
            for (Long id : listIds) {
                CandidateLog candidateLog = new CandidateLog();
                candidateLog.setRequestId(requestId);
                candidateLog.setCandidateId(id);
                candidateLog.setLog(e.toString());
                candidateLogRepository.save(candidateLog);
            }
            throw new CustomException("Api update bị lỗi");
        }
    }

    @Override
    public CandidateResponse[] deleteCandidate(String candidateIds, Long requestId) throws IOException {
        List<Long> listIds = Arrays.asList(candidateIds.split(",")).stream().map(x -> Long.parseLong(x)).collect(Collectors.toList());
        List<HistoryMoveLog> historyMoveLogList = new ArrayList<HistoryMoveLog>();
        String url = candidateApiUrl + candidateDeletePath + "?candidateIds=" + candidateIds + "&jobRtsId=" + requestId;
        for (Long id : listIds) {
            HistoryMoveLog historyMoveLog = new HistoryMoveLog();
            historyMoveLog.setCandidateId(id);
            historyMoveLog.setRequestId(requestId);
            historyMoveLog.setCandidateBefore(CandidateStateEnum.valueOf(findCandidateById(id).getStepRts()));
            historyMoveLog.setCandidateAfter(CandidateStateEnum.none);
            historyMoveLog.setCreatedDate(new Timestamp(System.currentTimeMillis()));
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            historyMoveLog.setCreatedBy(authentication.getPrincipal().toString());
            historyMoveLogList.add(historyMoveLog);
        }
        historyService.createHistory(historyMoveLogList);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", CANDIDATE_TOKEN);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity requestHeader = new HttpEntity(headers);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.DELETE, requestHeader, String.class);
        CandidateResponse[] candidateResponses = objectMapper.readValue(response.getBody(), CandidateResponse[].class);
        return candidateResponses;
    }

    @Override
    public TotalCandidateStepResponse getTotalCandidateInStepByIdRequest(List<String> idRequestIds) {
        log.info("idrequest {}", idRequestIds);
        String url = candidateApiUrl + totalCandidateStep;
        log.info("url {}", url);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", CANDIDATE_TOKEN);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity requestEntity = new HttpEntity(idRequestIds, headers);
        TotalCandidateStepResponse totalCandidateStepResponse = new TotalCandidateStepResponse();
        try {
            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
            totalCandidateStepResponse = objectMapper.readValue(response.getBody(), TotalCandidateStepResponse.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return totalCandidateStepResponse;
    }

    @Override
    public List<TotalCandidateTAResponse> getTotalCandidateInStepInTAByIdRequest(List<String> listTAs) {
        String url = candidateApiUrl + totalCandidateTA;
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", CANDIDATE_TOKEN);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity requestEntity = new HttpEntity(listTAs, headers);
        try {
            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
            return objectMapper.readValue(response.getBody(), new TypeReference<List<TotalCandidateTAResponse>>() {
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<TotalCandidateTAResponseV2> getTotalCandidateInStepInTAByIdRequestV2(List<String> listTA, List<String> requestIds) {
        String url = candidateApiUrl + totalCandidateTAv2;
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", CANDIDATE_TOKEN);
        headers.setContentType(MediaType.APPLICATION_JSON);
        Map<String, Object> body = new HashMap<>();
        body.put("requestIds", requestIds);
        body.put("listTA", listTA);
        log.info("list {}, {}", requestIds, listTA);

        HttpEntity requestEntity = new HttpEntity(body, headers);
        try {
            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
            return objectMapper.readValue(response.getBody(), new TypeReference<List<TotalCandidateTAResponseV2>>() {
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    @Override
    public SingleResponseDTO<CandidateCheckDTO> candidateCheck(String candidateMail) {
//        SingleResponseDTO responseDTO = new SingleResponseDTO();
//        String url = candidateApiUrl + candidateCheck;
//        RestTemplate restTemplate = new RestTemplate();
//        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
//        HttpHeaders headers = new HttpHeaders();
//        headers.set("Authorization", CANDIDATE_TOKEN);
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        Map<String, Object> body = new HashMap<>();
//        body.put("key", candidateMail);
//        HttpEntity requestEntity = new HttpEntity(headers);
//        try {
//            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class, candidateMail);
//            responseDTO.setCode(String.valueOf(HttpStatus.OK));
//            responseDTO.setData(response.getBody());
//            return responseDTO;
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }

        SingleResponseDTO responseDTO = new SingleResponseDTO();
        String url = candidateApiUrl + candidateCheck + candidateMail;

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders requestHeaders = new HttpHeaders();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", CANDIDATE_TOKEN);
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity requestEntity = new HttpEntity<>(requestHeaders);

        ResponseEntity<String> responseEntity = restTemplate.exchange(
                url,
                HttpMethod.GET,
                requestEntity,
                String.class
        );
        try {
            responseDTO.setData(objectMapper.readValue(responseEntity.getBody(), CandidateCheckDTO.class));
            responseDTO.setCode(String.valueOf(HttpStatus.OK));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return responseDTO;
    }
}
