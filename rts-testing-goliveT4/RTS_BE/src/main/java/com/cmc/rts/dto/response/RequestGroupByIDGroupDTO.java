package com.cmc.rts.dto.response;

import lombok.Data;

@Data
public class RequestGroupByIDGroupDTO {

    private Long managerGroupId;
    private Long id;
    private Long number;

    public RequestGroupByIDGroupDTO() {
    }

    public RequestGroupByIDGroupDTO(Long managerGroupId, Long id, Long number) {
        this.managerGroupId = managerGroupId;
        this.id = id;
        this.number = number;
    }
}
