package com.cmc.rts.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SkillNameDTO {
    private Integer id;
    private String name;

    public SkillNameDTO() {
    }

    public SkillNameDTO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
}
