package com.cmc.rts.rest;


import com.cmc.rts.service.UploadFileService;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.jdbc.Work;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * Create by trungtx
 * Date: 11/10/2021
 * Time: 12:09 AM
 * Project: CMC_RTS
 */

@Slf4j
@RestController
public class UploadFile {

    @Autowired
    private UploadFileService uploadFileService;

    @PostMapping(value = "/upload",consumes = { MediaType.APPLICATION_JSON_VALUE,MediaType.MULTIPART_FORM_DATA_VALUE })
    public ResponseEntity<?> uploadFile(@RequestPart MultipartFile file, @RequestParam String fileType ) {
        try {
            return new ResponseEntity<>(uploadFileService.uploadFile(fileType, file), HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/uploadMultiFile",consumes = { MediaType.APPLICATION_JSON_VALUE,MediaType.MULTIPART_FORM_DATA_VALUE })
    public ResponseEntity<?> uploadMultiFile(@RequestPart MultipartFile[] file, @RequestParam String fileType ) {
        try {
            return new ResponseEntity<>(uploadFileService.uploadMultiFile(fileType, file), HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/uploadExcel",consumes = { MediaType.APPLICATION_JSON_VALUE,MediaType.MULTIPART_FORM_DATA_VALUE })
    public ResponseEntity<?> uploadExcel(@RequestPart MultipartFile file, @RequestPart String fileInfo) {
        try {
            return new ResponseEntity<>(uploadFileService.uploadExcel(fileInfo, file), HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/uploadExcelStepRTS",consumes = { MediaType.APPLICATION_JSON_VALUE,MediaType.MULTIPART_FORM_DATA_VALUE })
    public ResponseEntity<?> uploadExcelStepRTS(@RequestPart MultipartFile file, @RequestPart String fileInfo,  @RequestPart String requestID,  @RequestPart String stepRTS) {
        try {
            return new ResponseEntity<>(uploadFileService.uploadExcelStepRTS(fileInfo, file, requestID, stepRTS), HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
