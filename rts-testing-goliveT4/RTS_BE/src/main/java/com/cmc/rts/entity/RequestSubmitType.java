package com.cmc.rts.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table
public class RequestSubmitType extends CategoryBaseDomain{
    // define submit from DU_Lead-> Group lead, HR_lead -> Group lead, Group lead -> Hr lead
}
