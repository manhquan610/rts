//package com.cmc.rts.dto.response.custom;
//
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//import java.util.List;
//import java.util.Map;
//
///**
// * @author: nthieu10
// * 7/20/2022
// **/
//@Data
//@NoArgsConstructor
//@AllArgsConstructor
//public class MultipleResponseDTO<T> {
//    private String statusCode;
//    private Map<String, String> message;
//    private String description;
//    private List<T> data;
//
//}
