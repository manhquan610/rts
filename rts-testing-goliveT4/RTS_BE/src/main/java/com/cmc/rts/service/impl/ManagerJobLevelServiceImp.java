package com.cmc.rts.service.impl;

import com.cmc.rts.dto.requestDto.JobLevelRequest;
import com.cmc.rts.dto.response.ResponseAPI;
import com.cmc.rts.entity.ManagerJobLevel;
import com.cmc.rts.exception.CustomException;
import com.cmc.rts.repository.ManagerJobLevelRepository;
import com.cmc.rts.repository.ManagerJobTypeRepository;
import com.cmc.rts.service.ManagerJobLevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class ManagerJobLevelServiceImp implements ManagerJobLevelService {

    @Autowired
    private ManagerJobLevelRepository jobLevelRepository;

    @Autowired
    private ManagerJobTypeRepository managerJobTypeRepository;

    @Override
    public Page<ManagerJobLevel> getAllJobLevels(Pageable pageable) {
        return jobLevelRepository.findAllJobLevels(pageable);
    }

    @Override
    public ResponseAPI insertJobLevel(JobLevelRequest jobLevel) {
        try{
            if (checkDuplicates(jobLevel.getNameJobLevel(), jobLevel.getIdJobType()) == 1){
                return new ResponseAPI(400, "Name job level duplicates");
            }
            if (jobLevel.getSalaryMin() > jobLevel.getSalaryMax()){
                return new ResponseAPI(400, "SalaryMax must be greater than SalaryMin");
            }
            jobLevelRepository.save(mapper(jobLevel));
            return new ResponseAPI(200, "Insert job level successfully");
        }catch (Exception e){
            return new ResponseAPI(500, e.getMessage());
        }
    }

    @Override
    public ResponseAPI updateJobType(JobLevelRequest jobLevel) {
        try{
            if (jobLevel.getSalaryMin() > jobLevel.getSalaryMax()){
                return new ResponseAPI(400, "SalaryMax must be greater than SalaryMin");
            }
            ManagerJobLevel managerJobLevel = jobLevelRepository.findOne(jobLevel.getId());
            if (managerJobLevel == null) {
                return new ResponseAPI(404, "Job level not found");
            }
            List<ManagerJobLevel> existsAllByName = jobLevelRepository.checkDuplicateJobLevels(jobLevel.getNameJobLevel(), jobLevel.getIdJobType());
            if (existsAllByName != null) {
                for (ManagerJobLevel level : existsAllByName){
                    if (!Objects.equals(level.getId(), jobLevel.getId())){
                        return new ResponseAPI(400, "Name job level duplicates");
                    }
                }
            }
            managerJobLevel.setName(jobLevel.getNameJobLevel());
            managerJobLevel.setJobType(managerJobTypeRepository.getOne(jobLevel.getIdJobType()));
            managerJobLevel.setSalaryMin(jobLevel.getSalaryMin());
            managerJobLevel.setSalaryMax(jobLevel.getSalaryMax());
            managerJobLevel.setDescription(jobLevel.getDescription());
            jobLevelRepository.save(managerJobLevel);
            return new ResponseAPI(200, "Update job level successfully");
        }catch (Exception e){
            return new ResponseAPI(500, e.getMessage());
        }
    }

    @Override
    public ResponseAPI deleteJobType(Long idJobLevel) throws CustomException {
        try {
            ManagerJobLevel managerJobLevel = jobLevelRepository.findOne(idJobLevel);
            if (managerJobLevel == null) {
                return new ResponseAPI(404, "Job level not found");
            }
            jobLevelRepository.delete(idJobLevel);
            return new ResponseAPI(200, "Job level delete successfully");
        }catch (Exception e) {
            throw new CustomException("Can't Delete! Because this item already exists in other information", 500);
        }
    }

    @Override
    public Page<ManagerJobLevel> findAllJobLevelsByPosition(Long id, Pageable pageable) {
        try{
         return jobLevelRepository.findAllBySearchParams(id, pageable);
        }catch (Exception e) {
            return null;
        }
    }

    @Override
    public Page<ManagerJobLevel> searchByNameLevel(String searchValues, Pageable pageable) {
        if (searchValues == null || searchValues.isEmpty()) {
            searchValues = "";
        }
        searchValues = searchValues.trim();
        return jobLevelRepository.findAllBySearchParams(searchValues, pageable);
    }

    @Override
    public ManagerJobLevel searchById(Long id) {
        return jobLevelRepository.findOne(id);
    }


    public int checkDuplicates(String checkValue, Long idJobType){
        List<ManagerJobLevel> existsAllByName = jobLevelRepository.checkDuplicateJobLevels(checkValue, idJobType);
        if (existsAllByName.size() > 0){
            return 1;
        } else {
            return 0;
        }
    }

    ManagerJobLevel mapper(JobLevelRequest jobLevel){
        ManagerJobLevel managerJobLevel = new ManagerJobLevel();
        managerJobLevel.setName(jobLevel.getNameJobLevel());
        managerJobLevel.setJobType(managerJobTypeRepository.getOne(jobLevel.getIdJobType()));
        managerJobLevel.setSalaryMin(jobLevel.getSalaryMin());
        managerJobLevel.setSalaryMax(jobLevel.getSalaryMax());
        managerJobLevel.setDescription(jobLevel.getDescription());
        return managerJobLevel;
    }



}
