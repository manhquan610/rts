package com.cmc.rts.dto.response;

import com.cmc.rts.dto.response.custom.BasePage;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReportResponseDTO {
    Long totalTarget = 0L;
    Long totalActual = 0L;
    Long totalGap = 0L;

    Long totalApplicant = 0L;
    Long totalCurrentApplicant = 0L;

    Long totalQualified = 0L;
    Long totalCurrentQualified = 0L;

    Long totalInterview = 0L;
    Long totalCurrentInterview = 0L;

    Long totalOffering = 0L;
    Long totalCurrentOffering = 0L;

    Long totalOnBoarding = 0L;
    Long totalCurrentOnBoarding = 0L;

    Long totalRejected = 0L;

    List<ReportResponse> reportResponses;
    BasePage basePage;
}
