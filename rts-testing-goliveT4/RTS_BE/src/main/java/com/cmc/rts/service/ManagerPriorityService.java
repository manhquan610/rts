package com.cmc.rts.service;

import com.cmc.rts.dto.request.ListNameRequest;
import com.cmc.rts.dto.request.NameRequest;
import com.cmc.rts.dto.response.PriorityDTO;
import com.cmc.rts.entity.ManagerPriority;
import org.springframework.web.bind.annotation.PathVariable;

public interface ManagerPriorityService {
    ManagerPriority addManagerPriority(ManagerPriority managerPriority);
    PriorityDTO findAll(NameRequest nameRequest);
    PriorityDTO findAll(ListNameRequest listNameRequest);
    ManagerPriority findOne(Long id);
    void delete(Long id);
}
