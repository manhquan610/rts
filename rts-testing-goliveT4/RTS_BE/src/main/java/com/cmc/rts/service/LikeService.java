package com.cmc.rts.service;
import com.cmc.rts.dto.response.LikeDTO;
import com.cmc.rts.entity.Like;

public interface LikeService {
    Like createLike(Long candidateId, Boolean liked);

    LikeDTO totalReact(Long candidateID);
}
