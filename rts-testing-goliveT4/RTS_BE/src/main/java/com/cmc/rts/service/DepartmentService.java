package com.cmc.rts.service;

import com.cmc.rts.entity.Department;

import java.util.List;

public interface DepartmentService {
    Department addDepartment(Department department);

    List<Department> findAll(String name, int offset, int limit);

    Department findOne(Long id);

    void delete(Long id);

    void delete(String ids);

    Department findByDepartmentId(Long id);
}
