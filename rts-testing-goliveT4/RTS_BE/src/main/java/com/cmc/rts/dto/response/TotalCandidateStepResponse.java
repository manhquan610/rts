package com.cmc.rts.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class TotalCandidateStepResponse {

    private List<CandidateListTotalStepResponse> candidateList;
    private Long totalNone;
    private Long totalQualify;
    private Long totalConfirm;
    private Long totalInterview;
    private Long totalOffer;
    private Long totalOnboard;
    private Long totalFailed;
    private Long totalQualifying;
    private Long totalConfirming;
    private Long totalInterviewing;
    private Long totalOffering;
    private Long totalOnboarding;

    public TotalCandidateStepResponse() {
    }

    public TotalCandidateStepResponse(List<CandidateListTotalStepResponse> candidateList, Long totalNone,
                                      Long totalQualify, Long totalConfirm, Long totalInterview, Long totalOffer,
                                      Long totalOnboard, Long totalFailed, Long totalQualifying, Long totalConfirming, Long totalInterviewing, Long totalOffering, Long totalOnboarding) {
        this.candidateList = candidateList;
        this.totalNone = totalNone;
        this.totalQualify = totalQualify;
        this.totalConfirm = totalConfirm;
        this.totalInterview = totalInterview;
        this.totalOffer = totalOffer;
        this.totalOnboard = totalOnboard;
        this.totalFailed = totalFailed;
        this.totalInterviewing = totalInterviewing;
        this.totalQualifying = totalQualifying;
        this.totalConfirming = totalConfirming;
        this.totalOffering = totalOffering;
        this.totalOnboarding = totalOnboarding;
    }
}
