package com.cmc.rts.service;

import com.cmc.rts.entity.UserInfo;

public interface UserService {
    UserInfo findOne(Long id);

    UserInfo findByUsername(String username);
}
