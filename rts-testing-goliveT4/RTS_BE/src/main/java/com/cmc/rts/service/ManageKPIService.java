package com.cmc.rts.service;

import com.cmc.rts.dto.response.ManageKpiDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.io.ByteArrayInputStream;

import java.util.List;

public interface ManageKPIService {

    Page<ManageKpiDTO> getAllByNameAndYear(String name, String year, Pageable pageable);

    List<ManageKpiDTO> getAllByNameAndYear(String name, String year);

    ByteArrayInputStream exportExcel(List<ManageKpiDTO> manageKpiList);
    
    List<ManageKpiDTO> createManageKpi(String year);

    ManageKpiDTO updateManageKpi(String username, String year, ManageKpiDTO manageKpiDTO);

}
