package com.cmc.rts.dto.requestDto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class UpdateCandidateOutputRequest {
     private List<Long> candidateId;
     private String stepRts;
     private String assignees;
     private String jobRtsId;
     private String ta;

    public UpdateCandidateOutputRequest(List<Long> candidateId, String stepRts, String jobRtsId, String assignees, String ta) {
        this.candidateId = candidateId;
        this.stepRts = stepRts;
        this.jobRtsId = jobRtsId;
        this.assignees = assignees;
        this.ta = ta;
    }
}