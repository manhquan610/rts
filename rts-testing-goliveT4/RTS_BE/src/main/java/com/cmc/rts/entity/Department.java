package com.cmc.rts.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table
@JsonIgnoreProperties({"userManager"})
public class Department extends CmcBaseDomain {
    @Column
    private Long departmentId;
    @ManyToOne
    @JoinColumn
    private ManagerGroup managerGroup;
}
