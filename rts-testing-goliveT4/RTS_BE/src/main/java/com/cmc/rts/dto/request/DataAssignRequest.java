package com.cmc.rts.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataAssignRequest {
    @JsonProperty("target")
    private int target;
    @JsonProperty("assign_ldap")
    private String assignLdap;
    @JsonProperty("head_Count")
    private int headCount;
}
