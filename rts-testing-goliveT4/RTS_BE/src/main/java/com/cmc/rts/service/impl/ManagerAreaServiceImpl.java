package com.cmc.rts.service.impl;


import com.cmc.rts.dto.request.AreaRequest;
import com.cmc.rts.entity.ManagerArea;
import com.cmc.rts.exception.CustomException;
import com.cmc.rts.repository.ManagerAreaRepository;
import com.cmc.rts.service.ManagerAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.UnknownHttpStatusCodeException;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
@Service
public class ManagerAreaServiceImpl implements ManagerAreaService {

    @Autowired
    private ManagerAreaRepository managerAreaRepository;
    @Override
    public Page<ManagerArea> findByName(String searchValues, Pageable pageable) {
        if (searchValues == null || searchValues.isEmpty()) {
            searchValues = "";
        }
        searchValues = searchValues.trim();
        return managerAreaRepository.findByName(searchValues,pageable);
    }

    @Override
    public Page<ManagerArea> findByListName(String name, Pageable pageable) {
        List<String> listName =null;
        boolean existName = true;
        if(name == null || name == "") {
            listName = null;

        }else {
            listName = Arrays.asList(name.split("\\s*,\\s*"));
        }
        return managerAreaRepository.findByListName(listName, !CollectionUtils.isEmpty(listName), pageable);
    }

    @Override
    public Page<ManagerArea> getAllArea(Pageable pageable) {
        return managerAreaRepository.findAllArea(pageable);
    }

    @Override
    public ManagerArea getOne(Long id) {
        return managerAreaRepository.findOne(id);
    }

    @Override
    public ManagerArea createArea(AreaRequest request) throws Exception {
        ManagerArea managerArea = new ManagerArea();
        managerArea.setName(request.getName());
        managerArea.setDescription(request.getDescription());
        managerArea.setDeleted(false);
        try {
            managerArea = managerAreaRepository.save(managerArea);
        }catch (Exception e){
            throw new Exception("Area name already exist");
        }
       return managerArea;
    }

    @Override
    public ManagerArea updateArea(AreaRequest request) throws Exception {
        if(request.getId() == null ){
            throw new CustomException("Id không được phép null", HttpStatus.FORBIDDEN.value());
        }
        ManagerArea managerArea = managerAreaRepository.findOne(request.getId());
        if(managerArea == null){
            throw new CustomException("Không tìm thấy thông tin về Area", HttpStatus.NOT_FOUND.value());
        }
        try {
            managerArea.setName(request.getName());
        }catch (HttpServerErrorException | HttpClientErrorException | UnknownHttpStatusCodeException e){
            throw new CustomException("Tên không được trùng lặp", HttpStatus.FORBIDDEN.value());
        }
        managerArea.setDescription(request.getDescription());
        managerArea.setDeleted(false);
        ManagerArea area;
        try{
            area = managerAreaRepository.save(managerArea);
        }catch (Exception e){
            throw new Exception("Area name already exist");
        }
        return area;

    }

    @Override
    public void deleteAreas(Long id) {
            managerAreaRepository.delete(id);
    }
}
