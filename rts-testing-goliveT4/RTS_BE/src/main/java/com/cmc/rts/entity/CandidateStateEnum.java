package com.cmc.rts.entity;

public enum CandidateStateEnum {
    none,
    qualify,
    confirm,
    interview,
    offer,
    onboard,
    failed,
}
