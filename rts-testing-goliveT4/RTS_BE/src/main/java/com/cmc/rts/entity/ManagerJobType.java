package com.cmc.rts.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name="manager_job_type")
public class ManagerJobType {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", length = 50 , unique=true)
    private String name;

    @Column(name = "description" , length = 250)
    private String description ;

    @Column(name = "deleted", columnDefinition = "boolean default false")
    private Boolean deleted;

    public ManagerJobType(Long id, String name, String description, Boolean deleted) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.deleted = deleted;
    }
}
