package com.cmc.rts.security;

import com.cmc.rts.entity.UserDTO;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class AuthenUser {
    public UserDTO getUser(HttpServletRequest request) {
        String bearerHeader = request.getHeader("Authorization");
        if (bearerHeader == null || !bearerHeader.startsWith("Bearer")) {
            return null;
        }
        bearerHeader = bearerHeader.replace("Bearer ", "");
        return AuthenSession.authenSessionToken.get(bearerHeader);
    }
}
