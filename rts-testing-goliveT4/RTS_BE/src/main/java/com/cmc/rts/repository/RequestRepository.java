package com.cmc.rts.repository;

import com.cmc.rts.dto.requestDto.RequestAssignDTO;
import com.cmc.rts.dto.response.RequestGroupByIDGroupDTO;
import com.cmc.rts.entity.ManagerArea;
import com.cmc.rts.entity.ManagerPriority;
import com.cmc.rts.entity.Request;
import com.cmc.rts.entity.RequestStatusEnum;
import com.cmc.rts.repository.custom.RequestRepositoryCustom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface RequestRepository extends JpaRepository<Request, Long>, RequestRepositoryCustom {

	Request findById(Long id);

	Request findByCode(String code);

	List<Request> getAllByCode(String code);

	@Query(value = "SELECT ui.email "
			+ "FROM user_info ui  "
			+ "INNER JOIN manager_group mg ON ui.id = mg.user_manager_id "
			+ "INNER JOIN request r ON r.manager_group_id = mg.id "
			+ "WHERE r.id = ?1",nativeQuery = true)
	String getEmailManagerGroupFromRequestId(Long id);

	List<Request> findAllByIdIn(List<Long> ids);

	@Query(value = "select distinct r " +
			"from Request r " +
			"left join r.requestAssigns ra " +
			"left join r.managerJobLevels mj " +
			"where (:requestNotEmpty = false or r.id in :requestIds ) " +
			"and (:nameNotEmpty = false or  r.name like %:name% ) " +
			"and (:groupNotEmpty = false or  r.managerGroupId in :groupIds ) " +
			"and (:departmentNotEmpty = false  or r.departmentId in :departmentIds ) " +
			"and (:statusNotEmpty = false or r.requestStatusName in :statuses ) " +
			"and (:areaNotEmpty = false  or r.area.id in :areaIds ) " +
			"and (:hrNotEmpty = false or ra.assignLdap in :hrLdaps )" +
			"and (:priorityNotEmpty = false or r.managerPriority.id in :priority ) " +
			"and ( r.deadlineStr between :startDate and :endDate ) " +
			"and (:languageNotEmpty = false or r.languageId in :language ) " +
			"and ( r.createdDate between :startCreate and :endCreate ) " +
			"order by r.createdDate desc " )
	Page<Request> findAllBySearchParams(@Param("requestIds") List<Long> requestIds,
										@Param("name") String name,
										@Param("groupIds") List<Long> groupIds,
										@Param("departmentIds") List<Long> departmentIds,
										@Param("statuses") List<RequestStatusEnum> statuses,
										@Param("areaIds") List<Long> areaIds,
										@Param("hrLdaps") List<String> hrLdaps,
										@Param("priority") List<Long> priority,
										@Param("startDate") String startDate,
										@Param("endDate") String endDate,
										@Param("language") List<String> language,
										@Param("startCreate") Date startCreate,
										@Param("endCreate") Date endCreate,
										@Param("requestNotEmpty") Boolean requestNotEmpty,
										@Param("nameNotEmpty") Boolean nameNotEmpty,
										@Param("groupNotEmpty") Boolean groupNotEmpty,
										@Param("departmentNotEmpty") Boolean departmentNotEmpty,
										@Param("statusNotEmpty") Boolean statusNotEmpty,
										@Param("areaNotEmpty") Boolean areaNotEmpty,
										@Param("hrNotEmpty") Boolean hrNotEmpty,
										@Param("priorityNotEmpty") Boolean priorityNotEmpty,
										@Param("languageNotEmpty") Boolean languageNotEmpty,
										Pageable pageable);

	@Query(value = "select distinct r " +
			"from Request r " +
			"left join r.requestAssigns ra " +
			"left join r.managerJobLevels mj " +
			"left join r.managerPriority mp " +
			"where (:requestNotEmpty = false or r.id in :requestIds ) " +
			"and (:nameNotEmpty = false or  r.name like %:name% ) " +
			"and (:groupNotEmpty = false or  r.managerGroupId in :groupIds ) " +
			"and (:departmentNotEmpty = false  or r.departmentId in :departmentIds ) " +
			"and (:statusNotEmpty = false or r.requestStatusName in :statuses ) " +
			"and (:areaNotEmpty = false  or r.area.id in :areaIds ) " +
			"and (:hrNotEmpty = false or ra.assignLdap in :hrLdaps or r.createdBy in :hrLdaps )" +
			"and (:priorityNotEmpty = false or r.managerPriority.id in :priority ) " +
			"and ( r.deadlineStr between :startDate and :endDate ) " +
			"and (:languageNotEmpty = false or r.languageId in :language ) " +
			"and ( r.createdDate between :startCreate and :endCreate) " +
			"order by r.createdDate desc " )

		// query for request screen as TA Manager and TA Lead
	Page<Request> findAllBySearchParamsRoleTa(@Param("requestIds") List<Long> requestIds,
											  @Param("name") String name,
											  @Param("groupIds") List<Long> groupIds,
											  @Param("departmentIds") List<Long> departmentIds,
											  @Param("statuses") List<RequestStatusEnum> statuses,
											  @Param("areaIds") List<Long> areaIds,
											  @Param("hrLdaps") List<String> hrLdaps,
											  @Param("priority") List<Long> priority,
											  @Param("startDate") String startDate,
											  @Param("endDate") String endDate,
											  @Param("language") List<String> language,
											  @Param("startCreate") Date startCreate,
											  @Param("endCreate") Date endCreate,
											  @Param("requestNotEmpty") Boolean requestNotEmpty,
											  @Param("nameNotEmpty") Boolean nameNotEmpty,
											  @Param("groupNotEmpty") Boolean groupNotEmpty,
											  @Param("departmentNotEmpty") Boolean departmentNotEmpty,
											  @Param("statusNotEmpty") Boolean statusNotEmpty,
											  @Param("areaNotEmpty") Boolean areaNotEmpty,
											  @Param("hrNotEmpty") Boolean hrNotEmpty,
											  @Param("priorityNotEmpty") Boolean priorityNotEmpty,
											  @Param("languageNotEmpty") Boolean languageNotEmpty,
											  Pageable pageable);

	@Query(value = "select distinct r.id " +
			"from Request r ")
	List<Long> getAllRequestIDs();

	@Query(value = "select * from request as r where r.share_job = :shareJob order by r.last_updated DESC", nativeQuery = true)
	List<Request> getAllByShareJobIs(@Param("shareJob") Boolean shareJob);

	@Query(value = "select new com.cmc.rts.dto.response.RequestGroupByIDGroupDTO(r.managerGroupId, COUNT(r.id) , SUM(r.number)) " +
			"from Request r GROUP BY r.managerGroupId")
	List<RequestGroupByIDGroupDTO> getTotalByGroup();

	@Query(value = "select r from Request r where r.managerGroupId = :groupId")
	List<Request> findAllByManagerGroup(@Param("groupId") Long groupId);

	@Query(value = "select r.area from Request r where r.id = :requestId")
	ManagerArea findByArea(@Param("requestId") Long requestId);

	@Query(value = "select r from ManagerArea r")
	List<ManagerArea> findAllArea();

	@Query(value = "select r.managerPriority from Request r where r.id = :requestId")
	ManagerPriority findByManagerPriority(@Param("requestId") Long requestId);

	@Query(value = "select r from ManagerPriority r")
	List<ManagerPriority> findManagerPriority();

	@Query(value = "SELECT r.id FROM request r WHERE r.id LIKE :requestId%", nativeQuery = true)
	List<Long> findAllByIds(@Param("requestId") String requestId);

	@Query(value = "SELECT r FROM Request r LEFT JOIN r.managerJobLevels mj WHERE r.deadline BETWEEN :startTime AND :endTime ORDER BY r.managerGroupId, r.departmentId ASC")
//	@Query(value = "SELECT * FROM request WHERE deadline BETWEEN :startTime AND :endTime ORDER BY manager_group_id, department_id ASC", nativeQuery = true)
	List<Request> findAllSortedByDate(@Param("startTime") String startTime, @Param("endTime") String endTime);

	@Query(value = "SELECT r FROM Request r LEFT JOIN r.managerJobLevels mj ORDER BY r.managerGroupId, r.departmentId ASC")
//	@Query(value = "SELECT * FROM request ORDER BY manager_group_id, department_id ASC", nativeQuery = true)
	List<Request> findAllSorted();

//	@Query(value = "SELECT * FROM request where id in :id" , nativeQuery = true)
//	List<Request> getDulistById(@Param("id") List<>)
}
