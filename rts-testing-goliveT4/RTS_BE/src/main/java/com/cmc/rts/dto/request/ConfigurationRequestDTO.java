package com.cmc.rts.dto.request;

import java.util.List;

public class ConfigurationRequestDTO {
    private List<String>  lstNameMajor;
    private List<String>  lstNamePosition;
    private List<String>  lstNamePriority;
    private List<String>  lstNameExperience;
    private List<String>  lstNameRequestType;
    private List<String>  lstNameSkillName;
    private List<String>  lstNameLanguage;
    private Integer offset;
    private Integer limit;

    public List<String> getLstNameMajor() {
        return lstNameMajor;
    }

    public void setLstNameMajor(List<String> lstNameMajor) {
        this.lstNameMajor = lstNameMajor;
    }

    public List<String> getLstNamePosition() {
        return lstNamePosition;
    }

    public void setLstNamePosition(List<String> lstNamePosition) {
        this.lstNamePosition = lstNamePosition;
    }

    public List<String> getLstNamePriority() {
        return lstNamePriority;
    }

    public void setLstNamePriority(List<String> lstNamePriority) {
        this.lstNamePriority = lstNamePriority;
    }

    public List<String> getLstNameExperience() {
        return lstNameExperience;
    }

    public void setLstNameExperience(List<String> lstNameExperience) {
        this.lstNameExperience = lstNameExperience;
    }

    public List<String> getLstNameRequestType() {
        return lstNameRequestType;
    }

    public void setLstNameRequestType(List<String> lstNameRequestType) {
        this.lstNameRequestType = lstNameRequestType;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public List<String> getLstNameSkillName() {
        return lstNameSkillName;
    }

    public void setLstNameSkillName(List<String> lstNameSkillName) {
        this.lstNameSkillName = lstNameSkillName;
    }

    public List<String> getLstNameLanguage() {
        return lstNameLanguage;
    }

    public void setLstNameLanguage(List<String> lstNameLanguage) {
        this.lstNameLanguage = lstNameLanguage;
    }
}
