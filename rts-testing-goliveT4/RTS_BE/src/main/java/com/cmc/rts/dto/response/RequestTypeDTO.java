package com.cmc.rts.dto.response;

import com.cmc.rts.entity.ManagerRequestType;

import java.util.List;

public class RequestTypeDTO {
    private List<ManagerRequestType> manager;
    private Integer total_item;
    private Integer total_page;

    public List<ManagerRequestType> getManager() {
        return manager;
    }

    public void setManager(List<ManagerRequestType> manager) {
        this.manager = manager;
    }

    public Integer getTotal_item() {
        return total_item;
    }

    public void setTotal_item(Integer total_item) {
        this.total_item = total_item;
    }

    public Integer getTotal_page() {
        return total_page;
    }

    public void setTotal_page(Integer total_page) {
        this.total_page = total_page;
    }
}
