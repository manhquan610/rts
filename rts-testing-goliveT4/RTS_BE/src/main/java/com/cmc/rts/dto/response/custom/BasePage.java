package com.cmc.rts.dto.response.custom;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.domain.Page;

@Builder
@Data
public class BasePage {
    private int page;
    private int size;
    private int totalPage;
    private long total;

    public static BasePage build(Page page) {
        return BasePage.builder()
                .page(page.getNumber() + 1)
                .size(page.getSize())
                .totalPage(page.getTotalPages())
                .total(page.getTotalElements())
                .build();
    }
}
