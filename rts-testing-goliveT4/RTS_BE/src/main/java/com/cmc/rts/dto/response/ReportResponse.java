package com.cmc.rts.dto.response;


import com.cmc.rts.dto.request.JobTypeRequest;
import com.cmc.rts.entity.RequestStatusEnum;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class ReportResponse {

    private Long unit;
    private String unitName;
    private Long department;
    private String departmentName;
    private Long jobId;
    private String jobType;
    private String jobLevel;
    private Long project;
    private String projectName;
    private String skill;
    private RequestStatusEnum status;
    private String location;
    private Date deadline;

    private Long totalTarget = 0L;

    private Long totalActual = 0L;

    private Long totalGap = 0L;

    private Long totalApplication = 0L;
    private Long currentApplication = 0L;

    private Long totalQualify = 0L;
    private Long currentQualifying = 0L;

    private Long totalInterview = 0L;
    private Long currentInterview = 0L;

    private Long totalOffering = 0L;
    private Long currentOffering = 0L;

    private Long currentOnBoarding = 0L;
    private Long totalOnBoarding = 0L;

    private Long totalRejected = 0L;

    private List<ReportResponse> children;

    public ReportResponse() {
    }
}
