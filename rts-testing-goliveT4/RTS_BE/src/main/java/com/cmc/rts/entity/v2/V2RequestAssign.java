package com.cmc.rts.entity.v2;

import com.cmc.rts.entity.BaseDomain;
import com.cmc.rts.entity.Request;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

/**
 * @author: nthieu10
 * 7/25/2022
 **/
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "request_assign")
@JsonInclude(JsonInclude.Include.NON_NULL)
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "request_assign")
public class V2RequestAssign extends BaseDomain {

    private Long assignId;
    private String assignName;

    private String assignLdap;

    @Column(name = "request_id")
    private Long requestId;

    @Column(name = "target")
    private int target;
    @Column(name = "applied", columnDefinition = "integer default 0")
    private int applied;
    @Column(name = "contacting", columnDefinition = "integer default 0")
    private int contacting;
    @Column(name = "interview", columnDefinition = "integer default 0")
    private int interview;
    @Column(name = "other", columnDefinition = "integer default 0")
    private int other;
    @Column(name = "onboard", columnDefinition = "integer default 0")
    private int onboard;

}
