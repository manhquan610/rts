package com.cmc.rts.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Interview extends BaseDomain {
    @Column(columnDefinition = "varchar(50)")
    private String name;
    @Column(columnDefinition = "varchar(50)")
    private String title;
    @Column
    private Date startTime;
    @Column
    private Date endTime;
    @Column
    private String interviewer;
    @Column(columnDefinition = "varchar(3000)")
    private String comment;
    @Column
    private String createdBy;

    @OneToOne
    @JoinColumn(name = "location_id")
    private Location location;

    @Column(name = "candidate_id")
    private Long candidateId;

    @ManyToOne
    @JoinColumn(name = "request_id", referencedColumnName = "id")
    private Request request;
}
