package com.cmc.rts.dto.requestDto;

import com.cmc.rts.entity.CandidateStateEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class RequestUpdateDto {
    private CandidateStateEnum candidateStateEnum;
    private Long requestId;
    private String assignees;
    private String ta;


}