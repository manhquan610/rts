package com.cmc.rts.repository;

import com.cmc.rts.entity.RequestCandidate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RequestCandidatesRepository extends JpaRepository<RequestCandidate, Long> {

}