package com.cmc.rts.repository.custom;

import com.cmc.rts.entity.ApplicationConfig;

import java.util.List;

public interface ApplicationConfigCustom {
    List<ApplicationConfig> findByName(String name, int offset, int limit);
}
