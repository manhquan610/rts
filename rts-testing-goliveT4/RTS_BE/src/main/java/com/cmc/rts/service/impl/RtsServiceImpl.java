package com.cmc.rts.service.impl;

import com.cmc.rts.entity.*;
import com.cmc.rts.repository.*;
import com.cmc.rts.service.RtsService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class RtsServiceImpl implements RtsService {
//    @Qualifier("getJavaMailSender")
    @Autowired
    JavaMailSender emailSender;
    @Autowired
    EmailQueueRepository emailQueueRepository;
    @Autowired
    LogExceptionRepository logExceptionRepository;
    @Autowired
    UserInfoRepository userInfoRepository;
    @Autowired
    DepartmentRepository departmentRepository;

    public static <T> Set<T> mergeSet(Set<T> a, Set<T> b) {
        return new HashSet<T>() {
            {
                addAll(a);
                addAll(b);
            }
        };
    }

    @Override
    public void initRequestStatus() {
        String str = "Start, New, Pending, Approved, Rejected, In Progress, Published, Closed";
        String[] array = str.split(",", -1);
        for (String tmp : array) {

        }
    }

    @Override
    public EmailQueue sendHtmlMessage(String emailTo, String emailCc, String subject, String htmlBody) throws MessagingException, javax.mail.MessagingException {
        EmailQueue emailQueue = null;
        try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
            if (emailTo != null && !emailTo.isEmpty()) {
                String[] emailTos = emailTo.split(",");
                helper.setTo(emailTos);
            }
            if (emailCc != null && !emailCc.isEmpty()) {
                String[] emailCcs = emailCc.split(",");
                if (emailCcs.length > 0) {
                    helper.setCc(emailCcs);
                }

            }
            helper.setSubject(subject);
            helper.setText(htmlBody, true);
            emailSender.send(message);
            emailQueue = new EmailQueue();
            emailQueue.setUserReceiver(emailTo);
            emailQueue.setSubject(subject);
            emailQueue.setBody(htmlBody);
            emailQueueRepository.save(emailQueue);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return emailQueue;
        }
    }

    @Transactional
    @Override
    public void saveLogException(String object, String errorMessage) {
        LogException logException = new LogException();
        logException.setName(errorMessage);
        logException.setEntity(object);
        logExceptionRepository.save(logException);

    }

    @Override
    @Transactional
    public void getUserAll(String url) throws IOException {
        JSONArray jsonArray = GetDataFromAPI.getData(url);
        if (jsonArray == null) {
            return;
        }
        int length = jsonArray.length();
        for (int i = 0; i < length; i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            if (jsonObject.has("userName")) {
                if (!jsonObject.getString("userName").isEmpty()) {
                    UserInfo userInfo = userInfoRepository.findByUsername(jsonObject.getString("userName"));
                    if (userInfo == null) {
                        userInfo = new UserInfo();
                        userInfo.setUserName(jsonObject.getString("userName"));
                    }
                    if (jsonObject.has("id") && !jsonObject.isNull("id")) {
                        userInfo.setUserId(jsonObject.getString("id"));
                    }
                    if (jsonObject.has("fullName") && !jsonObject.isNull("fullName")) {
                        userInfo.setFullName(jsonObject.getString("fullName"));
                    }
                    if (jsonObject.has("employeeId") && !jsonObject.isNull("employeeId")) {
                        userInfo.setEmployeeId(jsonObject.getString("employeeId"));
                    }
                    if (jsonObject.has("departmentName") && !jsonObject.isNull("departmentName")) {
                        Department department = departmentRepository.findByName(jsonObject.getString("departmentName"), 0, 1).size() > 0 ?
                                departmentRepository.findByName(jsonObject.getString("departmentName"), 0, 1).get(0) : null;
                        userInfo.setDepartmentName(jsonObject.getString("departmentName"));
                        if (department != null) {
                            userInfo.setDepartment(department);
                        }

                    }
                    if (jsonObject.has("createdDate") && !jsonObject.isNull("createdDate")) {
                        userInfo.setCreatedDateStr(jsonObject.getString("createdDate"));
                    }
                    if (jsonObject.has("status") && !jsonObject.isNull("status")) {
                        userInfo.setStatus((Boolean) jsonObject.get("status"));
                    }
                    if (jsonObject.has("updatedDate") && !jsonObject.isNull("updatedDate")) {
                        userInfo.setUpdatedDateStr(jsonObject.getString("updatedDate"));
                    }
                    if (jsonObject.has("startDate") && !jsonObject.isNull("startDate")) {
                        userInfo.setStartDateStr(jsonObject.getString("startDate"));
                    }
                    if (jsonObject.has("startWorkingDay") && !jsonObject.isNull("startWorkingDay")) {
                        userInfo.setStartWorkingDayStr(jsonObject.getString("startWorkingDay"));
                    }
                    if (jsonObject.has("email") && !jsonObject.isNull("email")) {
                        userInfo.setEmail(jsonObject.getString("email"));
                    }
                    userInfoRepository.save(userInfo);
                }
            }
        }
    }

//    @Override
//    public boolean hasPermission(String username, String permissionName) {
//        List<Permission> permissions = permissionRepository.findByName(permissionName, 0, 1);
//        if (permissions == null || permissions.size() == 0) {
//            return false;
//        }
//        UserInfo userInfo = userInfoRepository.findByUsername(username);
//        if (userInfo == null) {
//            return false;
//        }
//        Permission permission = permissions.get(0);
//        Set<Permission> permissionSet = userInfo.getPermissions() != null ? userInfo.getPermissions() : null;
//        Set<Role> roles = userInfo.getRoles();
//        for (Role role : roles) {
//            permissionSet = mergeSet(permissionSet, role.getPermissions());
//        }
//        if (!permissionSet.contains(permission)) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public boolean hasPermission(String username, String[] permissionNames) {
//        for (String permissionName : permissionNames) {
//            if (!hasPermission(username, permissionName)) {
//                return false;
//            }
//        }
//        return true;
//    }
}
