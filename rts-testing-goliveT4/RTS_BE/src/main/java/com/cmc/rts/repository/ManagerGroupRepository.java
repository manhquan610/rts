package com.cmc.rts.repository;

import com.cmc.rts.entity.ManagerGroup;
import com.cmc.rts.repository.custom.ManagerGroupRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ManagerGroupRepository extends JpaRepository<ManagerGroup, Long>, ManagerGroupRepositoryCustom {
}
