package com.cmc.rts.rest;

import com.cmc.rts.dto.DeleteDTO;
import com.cmc.rts.dto.request.ListNameRequest;
import com.cmc.rts.dto.request.NameRequest;
import com.cmc.rts.dto.response.MajorDTO;
import com.cmc.rts.entity.ManagerMajor;
import com.cmc.rts.entity.UserDTO;
import com.cmc.rts.security.AuthenUser;
import com.cmc.rts.service.ManagerMajorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;

@RestController
public class ManagerMajorController {

    @Autowired
    private AuthenUser authenUser;

    @Autowired
    ManagerMajorService managerMajorService;

    @PostMapping(value = "/manager-major")
    public ResponseEntity<?> createManagerMajor(HttpServletRequest request, @RequestBody ManagerMajor managerMajor) throws Exception{

        UserDTO userLogin = authenUser.getUser(request);

        if(managerMajor !=null){
            if(managerMajor.getName() != null && !managerMajor.getName().isEmpty() && managerMajor.getName().length() > 50){
                throw new Exception("You've exceeded the limit by 50 characters");
            }
            if(managerMajor.getDescription() != null && !managerMajor.getDescription().isEmpty() && managerMajor.getDescription().length() > 250){
                throw new Exception("You've exceeded the limit by 250 characters");
            }
        }

        try {
            managerMajorService.addManagerMajor(managerMajor);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Major Name already exist");
        }


        return new ResponseEntity<ManagerMajor>(managerMajor, HttpStatus.OK);
    }

    @PutMapping (value = "/manager-major")
    public ResponseEntity<?> updateManagerMajor(HttpServletRequest request, @RequestBody ManagerMajor managerMajor) throws Exception{

        UserDTO userLogin = authenUser.getUser(request);

        if(managerMajor !=null){
            if(managerMajor.getName() != null && !managerMajor.getName().isEmpty() && managerMajor.getName().length() > 50){
                throw new Exception("You've exceeded the limit by 50 characters");
            }
            if(managerMajor.getDescription() != null && !managerMajor.getDescription().isEmpty() && managerMajor.getDescription().length() > 250){
                throw new Exception("You've exceeded the limit by 250 characters");
            }
        }

        try {
            managerMajorService.addManagerMajor(managerMajor);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Major Name already exist");
        }

        return new ResponseEntity<ManagerMajor>(managerMajor, HttpStatus.OK);
    }

    @GetMapping(value = "/manager-major/{id}")
    public ResponseEntity<?> findOneManagerMajor(@PathVariable Long id) {
        ManagerMajor managerMajor = managerMajorService.findOne(id);
        return new ResponseEntity<ManagerMajor>(managerMajor, HttpStatus.OK);
    }


    @PostMapping(value = "/manager-major/same")
    public ResponseEntity<MajorDTO> all(@RequestBody NameRequest nameRequest) {
        MajorDTO majorDTO = managerMajorService.findAll(nameRequest);
        return new ResponseEntity<MajorDTO>(majorDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/manager-major/exact")
    public ResponseEntity<MajorDTO> allAxact(@RequestBody ListNameRequest listNameRequest) {
        MajorDTO majorDTO = managerMajorService.findAll(listNameRequest);
        return new ResponseEntity<MajorDTO>(majorDTO, HttpStatus.OK);
    }

    @PostMapping (value = "/manager-major/delete")
    public void delete(@RequestBody DeleteDTO deleteDTO) throws Exception{
        Long[] ids = deleteDTO.getIds();
        for(Long item: ids) {
            try {
            managerMajorService.delete(item);
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception("Can't Delete! Because this item already exists in other information");
            }
        }
    }
}
