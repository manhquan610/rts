package com.cmc.rts.service.impl;

import com.cmc.rts.entity.LanguageTmp;
import com.cmc.rts.repository.LanguageTmpRepository;
import com.cmc.rts.service.LanguageService;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.IOException;
import java.util.List;

@Service
public class LanguageServiceImpl implements LanguageService {
    @Autowired
    LanguageTmpRepository languageTmpRepository;
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public void getFromApi() throws IOException {
        String sql = " SELECT id FROM `language` WHERE last_updated != CURDATE() limit 1";
        Query query = entityManager.createNativeQuery(sql);
        try {
            query.getSingleResult();
        } catch (NoResultException e) {
            return;
        }
        String remove = "\t";
        String url = "https://skills.cmcglobal.com.vn/api/setting/getLanguageList";
        JSONArray jsonArray = GetDataFromAPI.getData(url);
        if (jsonArray == null) {
            return;
        }
        int length = jsonArray.length();
        if (length > 0) {
            List<LanguageTmp> languages = languageTmpRepository.findAll();
            if (languages.size() > 0) {
                languageTmpRepository.deleteAll();
            }
        }
        LanguageTmp languageTmp;
        try {
            for (int i = 0; i < length; i++) {
                String tmp = jsonArray.getString(i).trim().replaceAll(remove, "");
                languageTmp = new LanguageTmp();
                languageTmp.setName(tmp);
                languageTmpRepository.save(languageTmp);
            }
            sql = "INSERT INTO `language`\n" +
                    "            (NAME)\n" +
                    "SELECT\n" +
                    "  NAME\n" +
                    "FROM (SELECT\n" +
                    "        NAME\n" +
                    "      FROM language_tmp\n" +
                    "      GROUP BY NAME) a\n" +
                    "WHERE NOT EXISTS(SELECT\n" +
                    "                   1\n" +
                    "                 FROM `language` b\n" +
                    "                 WHERE a.name = b.name)";
            query = entityManager.createNativeQuery(sql);
            query.executeUpdate();
            sql = "update `language` set last_updated = CURDATE()";
            query = entityManager.createNativeQuery(sql);
            query.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }
}
