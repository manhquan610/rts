package com.cmc.rts.repository.custom;

import com.cmc.rts.entity.UserInfo;

import java.util.List;

public interface UserInfoRepositoryCustom {
    List<UserInfo> findByName(String name, int offset, int limit);

    UserInfo findByUsername(String username);

    UserInfo getLastGroupLeaderSubmit(Long requestId);
}
