package com.cmc.rts.entity;

public enum WorkingTypeEnum {
    FullTime,
    PartTime,
    FreeLancer,
    Remote,
    Collaborator,
    Intern
}