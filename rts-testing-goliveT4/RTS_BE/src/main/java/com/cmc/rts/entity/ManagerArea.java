package com.cmc.rts.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name="manager_area")
public class ManagerArea implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", length = 50 , unique=true)
    private String name;

    @Column(name = "description" , length = 250)
    private String description ;

    @Column(name = "deleted", columnDefinition = "boolean default false")
    private Boolean deleted;
}
