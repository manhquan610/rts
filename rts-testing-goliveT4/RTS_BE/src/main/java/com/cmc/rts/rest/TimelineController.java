package com.cmc.rts.rest;

import com.cmc.rts.dto.response.HistoryMoveLogDTO;
import com.cmc.rts.dto.response.TimelineDTO;
import com.cmc.rts.service.TimelineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class TimelineController {
    @Autowired
    private TimelineService timelineService;
    @GetMapping("/timeline")
    public ResponseEntity<TimelineDTO> getTimeline(@RequestParam Long requestId, @RequestParam Long candidateId) {
        TimelineDTO listInterview = timelineService.geTimeline(requestId,candidateId);
        return new ResponseEntity<>(listInterview, HttpStatus.OK);
    }

    @GetMapping("/timeline/{candidateId}")
    public ResponseEntity<?> getHistoryMoveLogCandidate(@PathVariable Long candidateId) {
        List<HistoryMoveLogDTO> responses = timelineService.getHistoryMoveLogCandidate(candidateId);
        return new ResponseEntity<>(responses, HttpStatus.OK);
    }
}
