package com.cmc.rts.service;

import com.cmc.rts.dto.response.ReportResponse;
import com.cmc.rts.dto.response.ReportResponseDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.ByteArrayInputStream;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

public interface ReportService {

    Page<ReportResponse> pageReport(Date startTime, Date endTime, String projectIds, String skill, String groups, String departments, String locations, Pageable pageable) throws ParseException;

    Page<ReportResponse> pageReport(List<ReportResponse> reportResponseList, Pageable pageable) throws ParseException;

    ReportResponseDTO pageReport(ReportResponseDTO responseDTO, Pageable pageable);

    List<ReportResponse> reportRTS() throws ParseException;

    List<ReportResponse> reportRTS(Date startTime, Date endTime, String projects, String skill, String groups, String departments, String locations);

    ByteArrayInputStream exportExcel(List<ReportResponse> contacts);

    ReportResponseDTO v2ReportRTS(Date startTime, Date endTime, String projects, String skill, String groups, String departments, String locations, String statuses, String priorityIds, Pageable pageable);
}
