package com.cmc.rts.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * @author: nthieu10
 * 7/22/2022
 **/
@Data
public class TotalCandidateOfRequestResponse {
    @JsonProperty("jobRtsId")
    private String jobRtsId;
    @JsonProperty("totalElements")
    private int totalElements;
    @JsonProperty("totalSource")
    private int totalSource;
    @JsonProperty("totalQualify")
    private int totalQualify;
    @JsonProperty("allQualify")
    private int allQualify = 0;
    @JsonProperty("totalConfirm")
    private int totalConfirm;
    @JsonProperty("totalInterview")
    private int totalInterview;
    @JsonProperty("totalOffer")
    private int totalOffer;
    @JsonProperty("totalOnboard")
    private int totalOnboard = 0;
    @JsonProperty("totalFailed")
    private int totalFailed;
    @JsonProperty("allConfirm")
    private int allConfirm;
    @JsonProperty("allInterview")
    private int allInterview;
    @JsonProperty("allOffer")
    private int allOffer;
    @JsonProperty("allOnboard")
    private int allOnboard;
    @JsonProperty("allFailed")
    private int allFailed;
}
