package com.cmc.rts.repository;

import com.cmc.rts.dto.response.CommentDTO;
import com.cmc.rts.entity.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
    @Query(value = "SELECT new com.cmc.rts.dto.response.CommentDTO(c.id,c.text, c.createdBy, c.createdDate) " +
            "FROM Comment AS c " +
            "where c.requestId = :requestId and c.candidateId is null order by c.createdDate")
    Page<CommentDTO> getAllCommentsForRequestDetail(@Param(value = "requestId") Long requestId,
                                    Pageable pageable);

    @Query(value = "select new com.cmc.rts.dto.response.CommentDTO(c.id, c.text, c.createdBy, c.createdDate) " +
            "from Comment as c " +
            "where c.requestId = :requestId " +
            "and c.candidateId = :candidateId order by c.createdDate")
    Page<CommentDTO> getAllCommentsForCandidateInRequest(@Param(value ="requestId") Long requestId, @Param(value = "candidateId") Long candidateId, Pageable pageable);

    @Query(value = "select new com.cmc.rts.dto.response.CommentDTO(c.id, c.text, c.createdBy, c.createdDate) " +
            "from Comment as c " +
            "where c.requestId = :requestId " +
            "and c.candidateId = :candidateId  order by c.createdDate desc  ")
    List<CommentDTO> getAllCommentsForTimeline(@Param(value ="requestId") Long requestId, @Param(value = "candidateId") Long candidateId);

}