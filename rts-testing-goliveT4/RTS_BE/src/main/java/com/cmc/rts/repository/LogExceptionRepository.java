package com.cmc.rts.repository;

import com.cmc.rts.entity.LogException;
import com.cmc.rts.repository.custom.LogExceptionRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogExceptionRepository extends JpaRepository<LogException, Long>, LogExceptionRepositoryCustom {
}
