package com.cmc.rts.repository;

import com.cmc.rts.dto.LocationDTO;
import com.cmc.rts.entity.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LocationRepository extends JpaRepository<Location, Long> {

    @Query(value = "SELECT new com.cmc.rts.dto.LocationDTO(l.id, l.address) " +
            " FROM Location l ")
    List<LocationDTO> getAllLocation();
}
