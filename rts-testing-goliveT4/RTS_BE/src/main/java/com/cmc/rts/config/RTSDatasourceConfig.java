//package com.cmc.rts.config;
//
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
//import org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy;
//import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Primary;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.orm.jpa.JpaTransactionManager;
//import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
//import org.springframework.transaction.PlatformTransactionManager;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//
//import javax.persistence.EntityManagerFactory;
//import javax.sql.DataSource;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * @author: nthieu10
// * 7/19/2022
// **/
//@Configuration
//@EnableJpaRepositories(entityManagerFactoryRef = "db1EntityMgrFactory", transactionManagerRef = "db1TransactionMgr",basePackages = "com.cmc.rts.repository")
//@EnableTransactionManagement
//public class RTSDatasourceConfig {
//
//
//    @Bean(name = "datasource1")
//    @ConfigurationProperties(prefix = "spring.db1.datasource")
//    @Primary
//    public DataSource dataSource() {
//        return DataSourceBuilder.create().build();
//    }
//    @Bean(name = "db1EntityMgrFactory")
//    @Primary
//    public LocalContainerEntityManagerFactoryBean db1EntityMgrFactory(
//            final EntityManagerFactoryBuilder builder,
//            @Qualifier("datasource1") final DataSource dataSource) {
//        Map<String, Object> props = new HashMap<>();
//        props.put("hibernate.physical_naming_strategy", SpringPhysicalNamingStrategy.class.getName());
//        props.put("hibernate.implicit_naming_strategy", SpringImplicitNamingStrategy.class.getName());
//        return builder
//                .dataSource(dataSource)
//                .packages("com.cmc.rts")
//                .persistenceUnit("db1")
//                .properties(props)
//                .build();
//    }
//    @Bean(name = "db1TransactionMgr")
//    @Primary
//    public PlatformTransactionManager db1TransactionMgr(
//            @Qualifier("db1EntityMgrFactory") final EntityManagerFactory entityManagerFactory) {
//        return new JpaTransactionManager(entityManagerFactory);
//    }
//}
