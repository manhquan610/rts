package com.cmc.rts.service;

import com.cmc.rts.dto.response.GroupResponse;
import com.cmc.rts.dto.response.GroupResponseDTO;
import com.cmc.rts.dto.response.ProjectResponse;
import com.cmc.rts.entity.ManagerGroup;

import java.util.List;

public interface ManagerGroupService {
    ManagerGroup addManagerGroup(ManagerGroup managerGroup);

    List<ManagerGroup> findAll(String name, int offset, int limit);

    ManagerGroup findOne(Long id);

    void delete(Long id);

    void delete(String ids);

    ManagerGroup findByGroupId(Long id);

    GroupResponse allManagerGroups();

    public String getDUName(Long idGroup, Long idDU);

    public String getGroupName(Long idGroup);

    public String getGroupDUName(Long idGroup, Long idDU);

    public List<ProjectResponse> getAllProject();

    public String getNameProject(Long idProject);
}
