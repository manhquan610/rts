package com.cmc.rts.entity;

import com.cmc.rts.dto.response.UserResponse;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Date;
import java.util.Set;

@Entity
@Getter
@Setter
//@Table(schema = "cmc_rts_dev", name = "request")
@Table
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"code", "rejectReason", "deleted", "version", "deadlineUnused"})
public class Request extends CategoryBaseDomain {
    @Column(columnDefinition = "boolean default false")
    private Boolean published;

    @Column(name = "deadline", nullable = false)
    @JsonProperty("deadlineUnused")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date deadline;

    @Column
    @JsonProperty("deadline")
    private String deadlineStr;

    @Min(0)
    @Max(1000)
    @Column(name = "number", nullable = false)
    private int number;

    @ManyToOne
    @JoinColumn(name = "manager_major_id")
    private ManagerMajor managerMajor;

    @Enumerated(EnumType.STRING)
    @Column
    private RequestStatusEnum requestStatusName;

    @ManyToOne
    @JoinColumn(name = "manager_priority_id", nullable = false)
    private ManagerPriority managerPriority;

    @ManyToOne
    @JoinColumn(name = "manager_experience_id", nullable = false)
    private ManagerExperience managerExperience;

    @ManyToOne
    @JoinColumn(name = "manager_request_type_id", nullable = false)
    private ManagerRequestType managerRequestType;

    private Long projectId;

    @ManyToOne
    @JoinColumn
    private RejectReason rejectReason;

    private Long departmentId;

    private Long managerGroupId;

    @Column
    private String managerName;

    @Column(name = "certificate")
    private String certificate;

    @Column(name = "others", length = 3000)
    private String others;

    @Column(name = "salary")
    private String salary;

    @Column(name = "benefit", length = 3000)
    private String benefit;

    private String languageId;

    private String skillNameId;

    @OneToMany(mappedBy = "request", fetch = FetchType.LAZY)
    @OrderBy("createdDate")
    private Set<RequestAssign> requestAssigns;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(joinColumns = {
            @JoinColumn(nullable = false, updatable = false)}, inverseJoinColumns = {
            @JoinColumn(nullable = false, updatable = false)})
    @OrderBy("createdDate")
    private Set<RequestSubmit> requestSubmits;

    @ManyToOne
    @JoinColumn(name = "area_id")
    private ManagerArea area;

    @ManyToOne
    @JoinColumn(name = "job_type_id")
    private ManagerJobType jobType;

    @Enumerated(EnumType.STRING)
    private WorkingTypeEnum workingType;

    private Boolean shareJob;

    private transient UserResponse lastGroupSubmit;

    private String lastGroupSubmitLdap;

    private String codePosition;

    @Column(length = 3000)
    private String requirement;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(joinColumns = {
            @JoinColumn(nullable = false, updatable = false)}, inverseJoinColumns = {
            @JoinColumn(nullable = false, updatable = false)})
    private Set<ManagerJobLevel> managerJobLevels;

}
