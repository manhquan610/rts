package com.cmc.rts.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
@Table
@NoArgsConstructor
public class HistoryMoveLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "candidate_before")
    private CandidateStateEnum candidateBefore;

    @Enumerated(EnumType.STRING)
    @Column(name = "candidate_after")
    private CandidateStateEnum candidateAfter;

    @Column(name = "created_date", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", nullable = false, updatable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Timestamp createdDate = new Timestamp(java.lang.System.currentTimeMillis());

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "candidate_id")
    private Long candidateId;

    @Column(name ="request_id")
    private Long requestId;

}
