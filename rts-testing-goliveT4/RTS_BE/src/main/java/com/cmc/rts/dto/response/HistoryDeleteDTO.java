package com.cmc.rts.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HistoryDeleteDTO {
    private Long id;
    private Date createdDate;
    private String createdBy;
}
