package com.cmc.rts.dto.response;

import lombok.Data;

/**
 * @author: nthieu10
 * 7/28/2022
 **/
@Data
public class TotalCandidateTAResponseV2 {
    private CustomCandidateDTO customCandidateDTO;
    private Long failed = 0L;
    @Data
    public static class CustomCandidateDTO {
        private String stepRts;
        private Long jobRtsId;
        private String ta;
        private Long count = 0L;
    }
}
