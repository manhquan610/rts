package com.cmc.rts.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProjectResponse {

    private Long projectId;
    private String projectName;
    private String startDate;
    private String endDate;
    private String projectCode;

    public ProjectResponse() {
    }

    public ProjectResponse(Long projectId, String projectName, String startDate, String endDate, String projectCode) {
        this.projectId = projectId;
        this.projectName = projectName;
        this.startDate = startDate;
        this.endDate = endDate;
        this.projectCode = projectCode;
    }
}
