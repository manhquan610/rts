package com.cmc.rts.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class GroupResponse {
    @JsonProperty("id")
    private Long id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("desciption")
    private String desciption;
    @JsonProperty("manager")
    private String manager;
    @JsonProperty("developmentUnit")
    private boolean developmentUnit;
    @JsonProperty("depth")
    private int depth;
    @JsonProperty("groupSale")
    private boolean groupSale;
    @JsonProperty("historyGroup")
    private String historyGroup;
    @JsonProperty("parentId")
    private Long parentId;
    @JsonProperty("listChild")
    private List<GroupResponse> listChild;
    @JsonProperty("saleUsers")
    private String saleUsers;
    @JsonProperty("hide")
    private boolean hide;
    @JsonProperty("disable")
    private boolean disable;
    @JsonProperty("lead")
    private boolean lead;

    public GroupResponse() {
    }

    public GroupResponse(Long id, String name, String desciption, String manager, boolean developmentUnit, int depth, boolean groupSale, String historyGroup, Long parentId, List<GroupResponse> listChild, String saleUsers, boolean hide, boolean disable, boolean lead) {
        this.id = id;
        this.name = name;
        this.desciption = desciption;
        this.manager = manager;
        this.developmentUnit = developmentUnit;
        this.depth = depth;
        this.groupSale = groupSale;
        this.historyGroup = historyGroup;
        this.parentId = parentId;
        this.listChild = listChild;
        this.saleUsers = saleUsers;
        this.hide = hide;
        this.disable = disable;
        this.lead = lead;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesciption() {
        return desciption;
    }

    public void setDesciption(String desciption) {
        this.desciption = desciption;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public boolean isDevelopmentUnit() {
        return developmentUnit;
    }

    public void setDevelopmentUnit(boolean developmentUnit) {
        this.developmentUnit = developmentUnit;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public boolean isGroupSale() {
        return groupSale;
    }

    public void setGroupSale(boolean groupSale) {
        this.groupSale = groupSale;
    }

    public String getHistoryGroup() {
        return historyGroup;
    }

    public void setHistoryGroup(String historyGroup) {
        this.historyGroup = historyGroup;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public List<GroupResponse> getListChild() {
        return listChild;
    }

    public void setListChild(List<GroupResponse> listChild) {
        this.listChild = listChild;
    }

    public String getSaleUsers() {
        return saleUsers;
    }

    public void setSaleUsers(String saleUsers) {
        this.saleUsers = saleUsers;
    }

    public boolean isHide() {
        return hide;
    }

    public void setHide(boolean hide) {
        this.hide = hide;
    }

    public boolean isDisable() {
        return disable;
    }

    public void setDisable(boolean disable) {
        this.disable = disable;
    }

    public boolean isLead() {
        return lead;
    }

    public void setLead(boolean lead) {
        this.lead = lead;
    }
}
