package com.cmc.rts.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table
public class EmailQueue extends BaseDomain {
//    @ManyToOne
//    @JoinColumn
//    private UserInfo userInfoSent;
//    @ManyToOne
//    @JoinColumn
//    private UserInfo userInfoReceiver;
    @Column
    private String userSent;
    @Column
    private String userReceiver;
    @Column
    private String subject;
    @Lob
    private String body;
}
