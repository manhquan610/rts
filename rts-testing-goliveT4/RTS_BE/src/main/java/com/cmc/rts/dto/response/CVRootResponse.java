package com.cmc.rts.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CVRootResponse {
    private List<CVResponse> candidateList;
    private int totalElements;
    private int size;
    private int page;
    private int totalPage;
}
