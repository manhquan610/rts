package com.cmc.rts.service.impl;

import com.cmc.rts.dto.request.ListNameRequest;
import com.cmc.rts.dto.request.NameRequest;
import com.cmc.rts.dto.response.PriorityDTO;
import com.cmc.rts.entity.ManagerPriority;
import com.cmc.rts.repository.ManagerPriorityRepository;
import com.cmc.rts.service.ManagerPriorityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class ManagerPriorityServiceImpl  implements ManagerPriorityService {
    @Autowired
    ManagerPriorityRepository  managerPriorityRepository;

    @Override
    @Transactional
    public ManagerPriority addManagerPriority(ManagerPriority managerPriority) {
        return managerPriorityRepository.save(managerPriority);
    }

    @Override
    public PriorityDTO findAll(NameRequest nameRequest) {
        PriorityDTO priorityDTO = new PriorityDTO();
        String lstName = "";
        if(nameRequest !=null && nameRequest.getLstName() !=null){
            lstName = nameRequest.getLstName();
        }
        Integer offset = null;
        if(nameRequest !=null && nameRequest.getOffset() !=null){
            offset = nameRequest.getOffset();
        }
        Integer limit = null;
        if(nameRequest !=null && nameRequest.getLimit()!=null){
            limit = nameRequest.getLimit();
        }

        Integer first = 0;
        if(offset !=null && limit !=null){
            first = (offset-1)*limit;
        }else {
            limit = 10;
        }
        priorityDTO.setManager(managerPriorityRepository.findByNameSame(first,limit,lstName));
        Integer total_item = (managerPriorityRepository.findByNameSame(null,null,lstName)).size();
        Integer total_page = 0;
        if(limit !=0){
            total_page = total_item / limit ;
            if((total_item % limit) != 0){
                total_page = total_page + 1;
            }
        }
        priorityDTO.setTotal_item(total_item);
        priorityDTO.setTotal_page(total_page);
        return priorityDTO;
    }

    @Override
    public PriorityDTO findAll(ListNameRequest listNameRequest) {
        PriorityDTO priorityDTO = new PriorityDTO();
        List<String> lstName = new ArrayList<>();
        if(listNameRequest !=null && listNameRequest.getLstName() !=null){
            lstName = listNameRequest.getLstName();
        }
        Integer offset = null;
        if(listNameRequest !=null && listNameRequest.getOffset() !=null){
            offset = listNameRequest.getOffset();
        }
        Integer limit = null;
        if(listNameRequest !=null && listNameRequest.getLimit()!=null){
            limit = listNameRequest.getLimit();
        }

        Integer first = 0;
        if(offset !=null && limit !=null){
            first = (offset-1)*limit;
        }else {
            limit = 10;
        }
        priorityDTO.setManager(managerPriorityRepository.findByName(first,limit,lstName));
        Integer total_item = (managerPriorityRepository.findByName(null,null,lstName)).size();
        Integer total_page = 0;
        if(limit !=0){
            total_page = total_item / limit ;
            if((total_item % limit) != 0){
                total_page = total_page + 1;
            }
        }
        priorityDTO.setTotal_item(total_item);
        priorityDTO.setTotal_page(total_page);
        return priorityDTO;
    }

    @Override
    public ManagerPriority findOne(Long id) {
        return managerPriorityRepository.findOne(id);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        managerPriorityRepository.delete(id);
    }
}
