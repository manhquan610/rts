package com.cmc.rts.dto.request;

import java.util.List;

public class ListNameRequest {
    private List<String> lstName;
    private Integer offset;
    private Integer limit;

    public List<String> getLstName() {
        return lstName;
    }

    public void setLstName(List<String> lstName) {
        this.lstName = lstName;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }
}
