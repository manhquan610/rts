package com.cmc.rts.repository.custom;

import com.cmc.rts.entity.LanguageTmp;

import java.util.List;

public interface LanguageTmpRepositoryCustom {
    List<LanguageTmp> findByName(String name);
}
