package com.cmc.rts.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.sql.Timestamp;

@Data
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ChannelResponse {
    @JsonProperty("id")
    private Long id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("source")
    private String source;
    @JsonProperty("createdBy")
    private String createdBy;
    @JsonProperty("createdAt")
    private Timestamp createdAt;
    @JsonProperty("updatedAt")
    private Timestamp updatedAt;
    @JsonProperty("updatedBy")
    private String updatedBy;
}
