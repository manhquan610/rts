package com.cmc.rts.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CVResponse {
    private Long candidateId;
    private String fullName;
    private String email;
    private String phone;
    private String linkCv;
    private Long channel;
    private String channelName;
    private String applySince;
    private String job;
    private Long jobRtsId;
    private Timestamp createdDate;
    private String ta;
    private String stepRts;
    private String jobLevel;
    private String assignees;
    private String createBy;
}
