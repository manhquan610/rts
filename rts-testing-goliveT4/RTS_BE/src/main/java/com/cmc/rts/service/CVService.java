package com.cmc.rts.service;


import com.cmc.rts.dto.request.CVRequest;
import com.cmc.rts.dto.response.CVRootResponse;
import com.cmc.rts.dto.response.CandidateResponse;
import com.cmc.rts.dto.response.CandidateRootResponse;
import com.cmc.rts.entity.CandidateStateEnum;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.ResponseEntity;

public interface CVService {
    CandidateRootResponse searchCandidates(CandidateStateEnum candidateState, String startDate, String endDate, String searchValues, Integer page, Integer size, String hasCV);
    CVRootResponse getAllCV(CandidateStateEnum candidateState, String startDate, String endDate, String searchString, Integer limit, Integer offset, String hasCV);
    ResponseEntity<?> createCV(CVRequest cvRequest) throws Exception;
    CandidateResponse updateCV(CVRequest cvRequest);

}
