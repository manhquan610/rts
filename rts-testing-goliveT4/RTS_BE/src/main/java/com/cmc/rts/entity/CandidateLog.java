package com.cmc.rts.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table
@NoArgsConstructor
public class CandidateLog extends BaseDomain {
    @Column(name = "candidate_id")
    private Long candidateId;

    @Column(name ="request_id")
    private Long requestId;

    @Column
    private String log;
}
