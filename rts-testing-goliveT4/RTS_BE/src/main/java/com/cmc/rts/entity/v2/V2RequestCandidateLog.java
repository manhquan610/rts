package com.cmc.rts.entity.v2;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Entity
@Table(name = "request_candidate_log")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class V2RequestCandidateLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "request_id")
    private Long requestId;

    @Column(name = "previous_step")
    private String previousStep;

    @Column(name = "current_step")
    private String currentStep;

    @Column(name = "candidate_id")
    private Long candidateId;

    @Column(name = "ta_lead")
    private String taLead;

    @Column(name = "ta_member")
    private String taMember;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_date", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", nullable = false, updatable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss", timezone = "Asia/Ho_Chi_Minh")
    private Timestamp createdDate;
}
