package com.cmc.rts.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Table(name = "request_candidate")
@NoArgsConstructor
public class RequestCandidate {
    @EmbeddedId
    private RequestCandidateKey id;

    @ManyToOne
    @MapsId("request_id")
    @JoinColumn(name = "request_id")
    private Request request;

    @ManyToOne
    @MapsId("candidate_id")
    @JoinColumn(name = "candidate_id")
    private Candidate candidate;

    @Column(name = "candidate_state")
    private CandidateStateEnum candidateState;

    public void setId(Long requestId, Long candidateId) {
        this.id = new RequestCandidateKey(requestId, candidateId);
    }
}

@Embeddable
class RequestCandidateKey implements Serializable {
    @Column(name = "request_id")
    private Long requestId;
    @Column(name = "candidate_id")
    private Long candidateId;

    public RequestCandidateKey(Long requestId, Long candidateId) {
        this.requestId = requestId;
        this.candidateId = candidateId;
    }

    public RequestCandidateKey() {

    }
}