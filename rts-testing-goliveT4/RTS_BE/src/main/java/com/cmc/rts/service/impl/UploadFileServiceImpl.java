package com.cmc.rts.service.impl;


import com.cmc.rts.dto.response.FileDTO;
import com.cmc.rts.dto.response.UploadExcelRes;
import com.cmc.rts.dto.response.UploadFileDTO;
import com.cmc.rts.entity.CandidateListRes;
import com.cmc.rts.entity.CandidateLog;
import com.cmc.rts.exception.CustomException;
import com.cmc.rts.repository.CandidateLogRepository;
import com.cmc.rts.service.UploadFileService;
import com.cmc.rts.utils.MultipartInputStreamFileResource;
import com.google.common.io.ByteStreams;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Create by trungtx
 * Date: 11/9/2021
 * Time: 11:57 PM
 * Project: CMC_RTS
 */

@Service
public class UploadFileServiceImpl implements UploadFileService {

    @Value("${api.upload.file.url}")
    private String fileURL;

    @Value("${file.type.param}")
    private String typeFile;

    @Autowired
    private CandidateLogRepository candidateLogRepository;

    String bucketName = "salt";

    @Value("${candidate.list.api.url}")
    private String uploadExcel;

    @Override
    public FileDTO uploadFile(String fileType, MultipartFile file) throws IOException {
        if (!checkTypeFile(fileType)){
            return new FileDTO("Type file does not exist");
        }
        String url = fileURL + "upload" + "?bucketName=" + bucketName +
                "&fileType=" + fileType;
        LinkedMultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", new MultipartInputStreamFileResource(file.getInputStream(), file.getOriginalFilename()));
        FileDTO fileDTO = callAPI(body,url,FileDTO.class);
        fileDTO.setFileName(file.getName());
        return fileDTO;
    }

    @Override
    public UploadFileDTO uploadMultiFile(String fileType, MultipartFile[] files) throws IOException {
        if (!checkTypeFile(fileType)){
            return new UploadFileDTO("Type file does not exist");
        }
        String url = fileURL + "uploadMultiFile" + "?bucketName=" + bucketName +
                "&fileType=" + fileType;
        LinkedMultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        for (MultipartFile file : files) {
            if (!file.isEmpty()) {
                body.add("file", new MultipartInputStreamFileResource(file.getInputStream(), file.getOriginalFilename()));
            }
        }

        UploadFileDTO fileDTO = callAPI(body,url,UploadFileDTO.class);
        List<Object> fileResources = body.get("file");
        for (int i = 0; i < fileDTO.getFiles().size(); i++){
            fileDTO.getFiles().get(i).setFileName(((MultipartInputStreamFileResource) fileResources.get(i)).getFilename());
        }
        return fileDTO;
    }

    @Override
    public UploadExcelRes uploadExcel(String fileInfo, MultipartFile file) throws IOException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (fileInfo == null || fileInfo.equals("")){
            throw new CustomException("fileInfo not null", HttpStatus.NOT_FOUND.value());
        }
        System.out.println("file type:"+file.getContentType());
        String url = uploadExcel + "/uploadMaster";
        LinkedMultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        Resource resource =  new MultipartInputStreamFileResource(file.getInputStream(), file.getOriginalFilename());
        HttpHeaders fileMap = new HttpHeaders();
        fileMap.setContentType(new MediaType("application", "vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
        HttpEntity<Resource> fileEntity = new HttpEntity<>(resource, fileMap);
        body.add("file", fileEntity);
        body.add("fileInfo",fileInfo);
        body.add("ta", authentication.getPrincipal().toString());
        return  callAPI(body,url,UploadExcelRes.class);
    }

    @Override
    public UploadExcelRes uploadExcelStepRTS(String fileInfo, MultipartFile file, String requestId, String stepRTS) throws IOException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (fileInfo == null || fileInfo.equals("")){
            throw new CustomException("fileInfo not null", HttpStatus.NOT_FOUND.value());
        }
        if (requestId == null || requestId.equals("")){
            throw new CustomException("requestId not null", HttpStatus.NOT_FOUND.value());
        }
        if (stepRTS == null || stepRTS.equals("")){
            throw new CustomException("stepRTS not null", HttpStatus.NOT_FOUND.value());
        }
        //?requestId="+requestId+"&stepRts="+stepRTS
        String url = uploadExcel + "api/public/uploadMasterForRTS";
        LinkedMultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        Resource resource =  new MultipartInputStreamFileResource(file.getInputStream(), file.getOriginalFilename());
        HttpHeaders fileMap = new HttpHeaders();
        fileMap.setContentType(new MediaType("application", "vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
        HttpEntity<Resource> fileEntity = new HttpEntity<>(resource, fileMap);
        body.add("file", fileEntity);
        body.add("fileInfo",fileInfo);
        body.add("requestId",requestId);
        body.add("stepRts",stepRTS);
        UploadExcelRes uploadExcelStepRTS = callAPI(body,url,UploadExcelRes.class);
        if (uploadExcelStepRTS != null){
            List<CandidateLog> candidateLogList = new ArrayList<>();
            for (CandidateListRes log : uploadExcelStepRTS.getCandidateList()){
                CandidateLog candidateLog = new CandidateLog();
                candidateLog.setRequestId(Long.parseLong(requestId));
                candidateLog.setCandidateId(log.getCandidateId());
                candidateLog.setCreatedBy(authentication.getPrincipal().toString());
                candidateLog.setLog("Create candidate excel successfully!!!");
                candidateLogList.add(candidateLog);
            }
            candidateLogRepository.save(candidateLogList);
        }
        return uploadExcelStepRTS;
    }


    /*Check type file, if exist then return true else return false*/
    public boolean checkTypeFile(String file){
        List<String> items = Arrays.stream(typeFile.split("\\s*,\\s*"))
                .filter(file::equals)
                .collect(Collectors.toList());
        return items.size() != 0;
    }

    /*Call API upload file*/
    public <T> T callAPI(LinkedMultiValueMap<String, Object> body, String url, Class<T> obj){
        T javaObject = null;

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.ALL_VALUE);
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        HttpEntity<LinkedMultiValueMap<String, Object>> entity = new HttpEntity<>(body, headers);
        ResponseEntity<String> response= restTemplate.postForEntity(url, entity, String.class);
        if (response.getStatusCode().value() != 200){
            throw new CustomException("Error call API upload file: "+response.getBody(),500);
        }
        System.out.println("response: "+response.getBody());
        try {
            Gson gson = new GsonBuilder().create();
            javaObject  = gson.fromJson(response.getBody(), obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return javaObject;
    }
}
