import {
  reasonType,Role
} from '../../components/commons/commonConstant'
import {
  formatDate,
  convertAvatarName
} from "../../components/commons/commons"
import {
  mapMutations,
  mapState,
} from "vuex";
import {SESSION_USER} from "../../model/user";
export default {
  name: "Header",
  data() {
    let subMenuKey = '/rts';
    let parentKey = '/rts';
    const pathName = this.$router.history.current.path
    if (pathName && pathName !== '/' && pathName !== '' && pathName !== undefined) {
      let splitPathname = pathName.split('/');
      if (splitPathname.length > 2) {
        subMenuKey = pathName;
        parentKey = '/' + splitPathname[1];
      } else {
        parentKey = '/' + splitPathname[1];
      }
    }
    return {
      SESSION_USER,
      visible: false,
      dataNotification: [],
      Role,
      parentKey: parentKey,
      subMenuKey: subMenuKey,
      menus: [],
      currentPath: '/rts/requests',
      childrenMenus: {},
      page: 0,
      size: 5,
      colorAvatar: localStorage.getItem('color_avatar')
    };
  },

  watch: {
    getAllNotification: function () {
      //this.getAllNotification()();
    },
  },

  computed: {
    ...mapState({
      roles: state => state.user.userInfo.roles ? state.user.userInfo.roles : "",
      isAuthenticated: (state) => state.user.isAuthenticated,
      userInfo: (state) => state.user.userInfo,
      listNotification: (state) => state.listNotification.data
    }),

    checkRenderSubMenu: function () {
      if(this.menus === undefined ) return []
      let findParentMenu = this.menus.find(item => item.state === this.parentKey);
      if (findParentMenu) return findParentMenu.children;
      else return []
    },
  },
  created() {
    this.getConfigMenus();
    if(localStorage.getItem('currentUrl')) {
      this.currentPath = localStorage.getItem('currentUrl')
    }
    if (this.$router.history.current.path.includes("/report")) {
      this.setDefaultActiveSubMenu("/report");
    } else if (this.$router.history.current.path.includes("/logs")) {
      this.setDefaultActiveSubMenu("/logs");
    }
  },
  mounted() {
    window.addEventListener('popstate', () => {
      const pathName = window.location.pathname
      if (pathName && pathName !== '/' && pathName !== '' && pathName !== undefined) {
        let splitPathname = pathName.split('/');
        this.subMenuKey = pathName;
        this.parentKey = '/' + splitPathname[1];
        this.subMenuKey = pathName;
        this.parentKey = '/' + splitPathname[1];
      }
    })
    if(!this.roles.includes(Role.GUEST)){
     //this.getAllNotification()
    }
  },

  methods: {
    ...mapMutations({
      toggleSidebar: "toggleSidebar"
    }),
    handlePopstate() {

    },
    activeMenu(e) {
      this.parentKey = e.key;
      this.subMenuKey = e.key;
    },
    onSelectSubMenu(e) {
      this.subMenuKey = e.key;
    },
    getAvatarName(fullName) {
      return convertAvatarName(fullName);
    },
    randomColor() {
      const r = () => Math.floor(256 * Math.random());
      return `rgb(${r()}, ${r()}, ${r()})`;
    },
    getConfigMenus() {
      let config = localStorage.getItem('session_login') ? JSON.parse(localStorage.getItem('session_login')) : {};
      if (config) {
        let menuTabs = config.menuTabs ? config.menuTabs : [];
        let menusConfig = []
        if (menuTabs && menuTabs.length > 0) {
          menusConfig = menuTabs[0];
        }
        if (menusConfig) {
          this.menus = menusConfig.items;
        } else this.menus = [];
      }
    },
    setDefaultActiveSubMenu(key) {
      this.subMenuKey = key;
      this.parentKey = key;
    },
    setDefaultActiveKey() {
      const pathName = this.$router.history.current.path
      if (pathName && pathName !== '/' && pathName !== '' && pathName !== undefined) {
        let splitPathname = pathName.split('/');
        if (splitPathname.length > 2) {
          this.subMenuKey = pathName;
          this.parentKey = '/' + splitPathname[1];
        } else {
          this.parentKey = '/' + splitPathname[1];
        }
      }
      else {
        if(!this.roles.includes(Role.GUEST)){
          this.subMenuKey = '/timesheet';
          this.parentKey = '/timesheet';
        }else{
          this.subMenuKey = '/timesheet/request-management';
          this.parentKey = '/timesheet/request-management';
        }
      }
    },
    onHandleClickMenu(e) {
      if (e.key === 'log_out') {
        this.$store.dispatch('user/userLogout')
        this.$router.push({
          path: '/login'
        }).catch(() => {});
        localStorage.removeItem("exact-router");
      }
      if(e.key === 'update-leave') {
        this.$router.push({
          path: '/update-leave'
        }).catch(() => {});
      }
      if(e.key === 'change-password') {
        this.$router.push({
          path:'/change-password'
        }).catch(() => {});
      }
    },
    checkActiveMenu(url) {
      this.currentPath = url
      localStorage.setItem('currentUrl', this.currentPath)
    },
    getAllNotification() {
      this.$store.dispatch("abnormalcase/getListNotification")
        .then(res => {
          let data = [];
          if (res && res.data) {
            let totalNotification = 0;
            data = res.data.map((item, index) => {
              item.key = index
              item.title = item.notification_type ? reasonType[item.notification_type] : ""
              item.notification_date = item.notification_date ? formatDate(item.notification_date) : "";
              if (item.is_read === false) {
                totalNotification++
              }
              return item;
            });
            let dataSource = {
              total: totalNotification,
              data: data,
              total_abnormal: res.page.total_element
            }
            this.dataNotification = dataSource;
          } else if (res.message !== null && res.message !== '') {
            this.$message.error(res.message, 5);
          }

        }).catch(error => {
          this.$message.error(error.message, 5);
        })
    },
    seeAllNotifications(key) {
      let param = {
        employee: this.userInfo.userName,
        page: 1,
        size: 10,
        direction: -1
      }
      sessionStorage.setItem("Abnormal Case", JSON.stringify(param))
      if (key === 'all') {
        this.$router.push({
          path: "/timesheet/abnormal-case",
        }).catch(() => {})
        window.location.reload()
      } else {
        this.$router.push({
          path: "/timesheet/abnormal-case",
          query: { id: key }
        }).catch(() => {})
        // this.$store.dispatch("abnormalcase/readNotification", { id: key }).then(res => {
        //   if (res) {
        //     //this.getAllNotification()
        //   }
        // }).catch(error => {
        //   this.$message.error(error.message, 5);
        // })
      }
      this.setDefaultActiveKey()
    },
  },
};
