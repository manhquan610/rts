import { ACTION_KEY, API_USER_TITLE } from "../components/commons/commonConstant";

export const checkPermission = (page, action, records, userRole) => {
  const key = ACTION_KEY[page + "_" + action];

  if (page === "request") {
    if (
      action === "create" ||
      action === "edit" ||
      action === "detail" ||
      action === "approve" ||
      action === "assign" ||
      action === "assignTALead" ||
      action === "reject" ||
      action === "submit" ||
      action === "close" ||
      action === "share" ||
      action === "move_candidate" ||
      action === "import_candidate"
    ) {
      return checkRoles(key, userRole);
    }
  } else if (page === "source") {
    if (action === "edit" || action === "detail") {
      return true;
    }

    if (action === "create" || action === "import" || action === "assign" || action === "reject") {
      return checkRoles(key, userRole);
    }
  }

  return records.every((record) => {
    return checkRoles(key, userRole) && checkAction(key, record, userRole);
  });
};

export const checkRoles = (key, userRole) => {
  const activities = userRole.activities;
  return activities.some((e) => e.key === key);
};

export const checkUserName = (key, userName) => {
  return userName === key;
};

export const checkAction = (actionKey, record, userRole) => {
  if (!record) return false;
  const userTitle = userRole?.title;
  const status = record.status;
  const createdBy = record.createdBy;
  const userName = JSON.parse(localStorage.getItem("session_login")).userName;

  if (actionKey === ACTION_KEY["share"]) {
    return status === "Assigned";
  }
  if (actionKey === ACTION_KEY["assign"]) {
    return (
      ((status === "Approved2" || status === "Assigned" || status === "Shared") &&
        checkUserName(createdBy, userName)) ||
      userTitle == API_USER_TITLE["HR_MNG"]
    );
  }
  if (actionKey === ACTION_KEY["approve"]) {
    if (userTitle === API_USER_TITLE["G_LEAD"]) {
      return status === "Pending";
    } else if (userTitle === API_USER_TITLE["HR_MNG"]) {
      return status === "Approved";
    } else if (userTitle === API_USER_TITLE["ADMIN"]) {
      return status === "Approved" || status === "Pending";
    }
  }
  if (actionKey === ACTION_KEY["create"]) {
    return true;
  }
  if (actionKey === ACTION_KEY["submit"]) {
    return checkDepartment(userRole, record) && status === "New";
  }
  if (actionKey === ACTION_KEY["edit"]) {
    if (userTitle === API_USER_TITLE["G_LEAD"]) {
      return status === "Pending";
    } else if (userTitle === API_USER_TITLE["HR"]) {
      return status === "Assigned";
    } else if (userTitle === API_USER_TITLE["HR_LEAD"]) {
      return status === "Approved2";
    }
    return checkDepartment(userRole, record) && (status === "New" || status === "Rejected" || status === "Approved2");
  }

  if (actionKey === ACTION_KEY["reject"]) {
    if (userTitle === API_USER_TITLE["G_LEAD"]) {
      return status === "Pending";
    } else if (userTitle === API_USER_TITLE["ADMIN"]) {
      return status === "Pending" || status === "Approve";
    } else if (userTitle === API_USER_TITLE["HR_MNG"]) {
      return status === "Approved";
    }
  }
  if (actionKey === ACTION_KEY["close"]) {
    return checkDepartment(userRole, record) && (status === "New" || status === "Rejected" || status === "Approved");
  }
};

export const checkDepartment = (userRole, record) => {
  if (!record) return false;
  let department = record.department;
  if (typeof record.department === "object") {
    department = record.department?.name;
  }
  if (userRole.title === API_USER_TITLE["DU_LEAD"]) {
    return userRole.department === department;
  }
  return true;
};
