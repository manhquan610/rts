import {API_ACTION_KEY, API_USER_TITLE} from "../components/commons/commonConstant";
import user from "@/store/modules/user";

export const checkRequestPermission = (actionKey, records, userRole) => {
    const key = API_ACTION_KEY[actionKey];
    if(actionKey === 'detail'){
        return true;
    }
    if(actionKey === 'create'){
        return checkUserRole(key, userRole)
    }
    if(actionKey === 'edit'){
        return checkUserRole(key, userRole)
    }
    if(actionKey === 'reject'){
        return checkUserRole(key, userRole)
    }
    return records.every((request)=>{
        return (checkUserRole(key, userRole) && checkAction(key, request, userRole))
    });
}

export const checkUserRole = (key, userRole) => {
    if(key === API_ACTION_KEY['create'] && userRole.title === API_USER_TITLE.HR_MNG){
        return true
    }

    if(userRole.title === 'RTS-EB'){
        return true
    }
    const activities = userRole.activities;
    return (activities.some(e => e.key === key))
}
export const checkUserName = (key, userName) => {
    return userName === key
}
export const checkAction = (actionKey, record, userRole) => {
    if (!record) return false
    const userTitle = userRole?.title
    const status = record.status
    const createdBy = record.createdBy
    const userName = JSON.parse(localStorage.getItem('session_login')).userName

    if (actionKey === API_ACTION_KEY['share']) {
        return status === "Assigned"
    }
    if (actionKey === API_ACTION_KEY['assign']) {
        return ((status === "Approved2" || status === "Assigned" || status === "Shared") && checkUserName(createdBy, userName)) || userTitle == API_USER_TITLE['HR_MNG']
    }
    if (actionKey === API_ACTION_KEY['approve']) {
        if(userTitle === API_USER_TITLE['G_LEAD'] ) {
            return  status === "Pending"
        }else if(userTitle === API_USER_TITLE['HR_MNG'] ){
            return status === "Approved"
        } else if(userTitle === API_USER_TITLE['ADMIN']){
            return status === "Approved" ||  status === "Pending"
        }
    }
    if (actionKey === API_ACTION_KEY['create']) {
        return true
    }
    if (actionKey === API_ACTION_KEY['submit']) {
        return  checkDepartment(userRole,record) && (status === 'New')
    }
    if (actionKey === API_ACTION_KEY['edit']) {
        if(userTitle === API_USER_TITLE['G_LEAD']){
            return (status === 'Pending')
        }else if(userTitle === API_USER_TITLE['HR']){
            return (status === 'Assigned')
        }else if(userTitle === API_USER_TITLE['HR_LEAD']){
            return (status === 'Approved2')
        }
        return checkDepartment(userRole,record) && (status === 'New' || status === 'Rejected' || status === 'Approved2')
    }

    if (actionKey === API_ACTION_KEY['reject']) {
        if(userTitle === API_USER_TITLE['G_LEAD']){
            return status === 'Pending'
        }else if(userTitle === API_USER_TITLE['ADMIN'] ){
            return status === 'Pending' || status === 'Approve'
        }else if(userTitle === API_USER_TITLE['HR_MNG']){
            return  status === 'Approved'
        }
    }
    if (actionKey === API_ACTION_KEY['close']) {
        return checkDepartment(userRole,record) && (status === 'New' || status === 'Rejected' || status === 'Approved')
    }
}

export const checkDepartment = (userRole, record) => {
    if (!record) return false
    let department = record.department
    if(typeof  record.department === 'object'){
         department = record.department?.name
    }
    if(userRole.title === API_USER_TITLE['DU_LEAD']){
        return userRole.department === department
    }
    return true;
}
