import {scaleScreen} from "../../components/commons/commons";

export default {
    name: "Login",
    beforeCreate() {
        this.form = this.$form.createForm(this, {name: 'normal_login'});
    },
    created() {
        this.scaleScreen()
        this.exactRouter = localStorage.getItem('exact-router')

    },
    data() {
        return {
            error_message: undefined,
            loading: false,
            exactRouter: '',
        }
    },
    methods: {
        scaleScreen,
        onHandleLogin(e) {
            e.preventDefault();
            this.form.validateFields((err, values) => {
                if (!err) {
                    this.loading = true;
                    let payload = {
                        username: values.username,
                        password: values.password,
                    };
                    this.$store.dispatch('user/userLogin', payload).then((res) => {
                        if (res) {
                            if(this.exactRouter){
                                this.$router.push(this.exactRouter).catch(() => {
                                });
                            }else {
                                this.$router.push('/rts/requests').catch(() => {
                                });
                            }
                        }
                    }).catch((err) => {
                        if (err.status == 500)
                        {
                            this.error_message = err.message + "";
                        }
                        else {
                            if (err.message != '')
                            {
                                console.log(err.message, 'err.message')
                                this.error_message = 'Account is having problems. Please contact the administrator!'
                                this.$store.dispatch('user/userLogout')
                            }
                        }

                        this.loading = false;
                    })
                }
            });
        },

        onHandleChangePassword() {
            this.$router.push({
                path: '/change-password'
            }).catch(() => {
            })
        }
    },
};
