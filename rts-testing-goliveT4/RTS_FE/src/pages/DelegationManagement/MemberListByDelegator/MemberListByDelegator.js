const smallcolumns = [{
    title: "Staff",
    dataIndex: "name",
    key: "name",
    // sorter: (a, b) => a.title.localeCompare(b.title),
    scopedSlots: {
      customRender: "name"
    },
  },
  {
    title: "From Date",
    dataIndex: "from_date",
    key: "from_date",
    scopedSlots: {
      customRender: "from_date"
    },

  },
  {
    title: "To Date",
    dataIndex: "to_date",
    key: "to_date",
    scopedSlots: {
      customRender: "to_date"
    },
  },
];

import moment from "moment";
import {
  formatDate
} from '../../../components/commons/commons'
export default {
  name: "MemberListByDelegator",
  props: {
    dataDeleList: {
      type: Array
    },
    dataSource: {
      type: Object
    },
    doneDeleteMember: {
      type: Function
    },
    onHandleEdit: {
      type: Function
    },
    showMemberList: {
      type: Function
    }
  },
  data() {
    return {
      smallcolumns,
      currentPage: 1,
      selectedRowKeysStaff: [],
      staffInfo: [],
      staffInfoOriginal: [],
      filterOption: (input, option) => {
        return (
          option.componentOptions.children[0].text.toLowerCase().indexOf(input.toLowerCase()) >= 0
        );
      },
      searchStaff: undefined,
      dataOptionFilterStaff: [],
      fetching: false
    };
  },
  watch:{
    dataSource: function(){
      this.formatStaffList();
    }
  },
  mounted() {
    this.formatStaffList();
  },
  created() {
    this.formatStaffList();
  },

  methods: {
    formatISODate(date) {
      return moment(date, "DD/MM/YYYY").format("YYYY-MM-DD HH:mm:ss");
    },

    onSelectChangeStaff(selectedRowKeys) {
      this.selectedRowKeysStaff = selectedRowKeys;
    },
    onHandleEditDelegator() {
      const {
        dataSource
      } = this;
      this.onHandleEdit({
        mode: 'edit',
        record: dataSource
      })
    },
    formatStaffList() {
      const {
        dataSource
      } = this;
      let staffInfo = [];
      if (dataSource && dataSource.staff_list) {
        dataSource.staff_list.map(item => {
          if (item.staff_info && item.staff_info !== null) {
            staffInfo.push({
              key: item.staff_info.id,
              from_date: item.from_date && item.from_date !== null ? formatDate(item.from_date) : null,
              to_date: item.to_date && item.to_date !== null ? formatDate(item.to_date) : null,
              id: item.staff_info.id,
              name: item.staff_info.name,
              department: item.staff_info.department,
            });
          }
          return item;
        });
      }
      this.staffInfo = staffInfo;
      this.staffInfoOriginal = staffInfo;
    },

    showConfirm() {
      this.$confirm({
        title: 'Do you Want to delete these items?',
        onOk:()=> {
          this.onDeleteStaff();
        },
      });
    },

    onDeleteStaff() {
      const {staffInfoOriginal,dataSource,selectedRowKeysStaff} = this;
      let  notItemDelete =[];
      let itemDoneDelete=[]
      staffInfoOriginal.map(item => {
         if(selectedRowKeysStaff.indexOf(item.id) === -1) {
          notItemDelete.push({
            from_date: this.formatISODate( item.from_date),
            to_date: this.formatISODate( item.to_date),
            staff_id: item.id
           });
           itemDoneDelete.push(item)
         }
         return item;
      })
      if (selectedRowKeysStaff.length === dataSource.staff_list.length){
        let tempDelegator = []
        tempDelegator.push(dataSource.id)
        var data = {
          "delegate_list_id": tempDelegator
        }
        this.$store.dispatch('delegation/deleteDelegation', data).then(res => {
          if (res && res.code === "000000") {
            this.$message.success('Delete Delegator successfully!',5);
            this.doneDeleteMember();
          } else {
            this.$message.error(res.message,5);
          }
        }).catch(error => {
          this.$message.error(error.message,5);
        })
      }else{
      let params ={
        staff_list:notItemDelete,
        id:dataSource.id,
        creator:dataSource.creator.id,
        delegator:dataSource.delegator.id,
      }
      let url = `/tms/delegate/edit-delegator`;
      this.$store.dispatch('delegation/createOrUpdateDelegation', {
        url: url,
        params: params
      }).then(res => {
        if (res) {
          if (res && res.message !== "") {
            this.$message.error(res.message,5);
          } else {
            this.$message.success('Delete Staff successfully!',5);
            this.staffInfo = itemDoneDelete
            this.doneDeleteMember();
          }
        }
      }).catch(error => {
        this.$message.error(error.message,5);
      })
      }
    },

    onChangeSearchStaff(value){
      this.searchStaff = value;
    },

    onSearchStaff(value) {
      this.fetching = true
      let decodeUriVal =value
      if(value && value !== '') decodeUriVal = decodeURI(value)
      if (decodeUriVal.length > 3) {
        const delegatorId = this.dataSource.delegator.id;
        let url = `?delegatorId=${delegatorId}&employee=${decodeUriVal}`;
        this.$store.dispatch('delegation/delegationSearchMemberList', url).then(res => {
          if (res && res.code === '000000') {
            this.dataOptionFilterStaff = res.data;
            this.fetching = false;
          }
        }).catch(error => {
          this.$message.error(error)
          this.fetching = false;
        })
      }
    },

    onHandleSearchStaff(){
      const {searchStaff} = this;
      const delegatorId = this.dataSource.delegator.id;
      let url = `?delegatorId=${delegatorId}&employee=${searchStaff}`;
      this.$store.dispatch('delegation/getDelegationManagementDetail', url).then(res => {
        let staffInfo = [];
        if (res && res.data) {
          staffInfo = res.data.staff_list.map(item => {
            if (item.staff_info && item.staff_info !== null) {
              staffInfo.push({
                key: item.staff_info.id,
                from_date: item.from_date && item.from_date !== null ? formatDate(item.from_date) : null,
                to_date: item.to_date && item.to_date !== null ? formatDate(item.to_date) : null,
                id: item.staff_info.id,
                name: item.staff_info.name
              });
              this.staffInfo = staffInfo
            }
            return item;
          });
        }
      }).catch(error => {
        this.$message.error(error)
      })
    },

    onCleanFilter(){
      this.searchStaff = undefined;
      this.staffInfo = this.staffInfoOriginal;
    }


  },
};