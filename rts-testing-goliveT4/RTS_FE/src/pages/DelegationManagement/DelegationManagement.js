const columns = [{
    title: "Staff",
    dataIndex: "name",
    key: "name",
    scopedSlots: {
      customRender: "name"
    },
  },

  {
    title: "",
    width: "30%",
    key: "action",
    scopedSlots: {
      customRender: "action"
    },
  },
];

const column1 = [{
    key: "name",
    scopedSlots: {
      customRender: "name"
    },
  },

  {
    title: "",
    width: "30%",
    key: "action",
    scopedSlots: {
      customRender: "action"
    },
  },
];

import { mapState} from "vuex";
import FilterLayout from "./../../components/layouts/FilterLayout";
import MemberListByDelegator from "./MemberListByDelegator/MemberListByDelegator.vue";
import moment from "moment";
import {statusColor,statusText,Role} from '../../components/commons/commonConstant';
import {convertObjectToParams,scaleScreen, initFilters} from "../../components/commons/commons"
// import http from '../../store/modules/helpers/httpInterceptor'

export default {
  name: "DelegationManage",
  components: {
    FilterLayout,
    MemberListByDelegator
  },
 
  data() {
    return {
      columns,
      Role,
      columnsList: [{
          title: "Delegator",
          dataIndex: "name",
          key: "name",
          // sorter: (a, b) => a.title.localeCompare(b.title),
          scopedSlots: {customRender: "name"},
        },

        {
          title: "Department",
          dataIndex: "department",
          key: "department",
          scopedSlots: {
            customRender: "department"
          },
        },

        {
          title: "Action",
          width: 100,
          key: "action",
          scopedSlots: {
            customRender: "action"
          },
        },
      ],
      column1,
      statusColor,
      visibleDrawer: false,
      drawerKey: undefined,
      visible: false,
      filterKey: 1,
      currentTab: "delegation_list",
      currentPage: 1,
      pageSize: 10,
      pageSize_explanation: 10,
      direction: 1,
      direction_explanation: -1,
      total_items: 0,
      total_items_explanation: 0,
      dataSource: [],
      dataExplanRequest: [],
      nextStepDisabled: true,
      previousStepDisabled: false,
      current: 0,
      disable: true,
      dataMemberOfCreate: [],
      dataAssignDetail: [],
      dataDelegatorToChange: [],
      selectedRowKeysDelegator: [],
      dataChooseDelegator: [],
      dataOptionFilterDelegator: [],
      dataOptionFilterMember: [],
      dataOptionFilterStaff: [],
      listExistedDelegator: [],
      list_delegator_delete: [],
      from_date_add: '',
      to_date_add: '',
      list_date_add: [],
      listDateVisible: false,
      selectedRowKeys: [],
      selectedRowKeysMember: [],
      dateFormat: 'DD/MM/YYYY',
      customRangeDate: "",
      activeItemStaff: '',
      searchEmployee: undefined,
      delegatorAssign: {},
      dataEditDelegation: {},
      recordDetail: {},
      keyDateRange: '',
      mode: 'new',
      fetching:false,
      is_last: false,
      loadMore: 0,
      disableSearch: true,
      key :[],
      is_last_memberList: false,
      pageDelegator: 1,
      pageMember:0,
      disableComplete: true,
      disableSelect: true,
      status: true,
      filters: {},
      timeOut: null,
      tooltipDelegation: { config: { visible: false } },
      filterOption: (input, option) => {
        return (
          option.componentOptions.children[0].text.toLowerCase().indexOf(input.toLowerCase()) >= 0
        );
      },
    };

  },
  created(){
    this.scaleScreen()
    this.handFilters()
  },
  computed: {
    ...mapState({
      delegateRequestData: (state) => state.timesheet.delegateRequestData,
      delegationTableData: (state) => state.timesheet.delegationTableData,
      delegationList: (state) => state.delegation.delegationList,
      userInfo: (state) => state.user.userInfo,
      roles: state => state.user.userInfo.roles ? state.user.userInfo.roles : "",
    }),
    rowSelection(){
      return {
        selectedRowKeys:this.selectedRowKeysMember,
        onChange: this.onSelectMemberDelegation,
      }
      
    },
  },
  watch: {
    visibleDrawer: function () {
      this.myFunction();
    },
    
    selectedRowKeysMember: function(){
      this.selectedRowKeysMember.length !== 0 ? this.disableComplete = false : this.disableComplete = true;
    },
    list_date_add: function(){
      if(this.list_date_add.length !== 0 && this.activeItemStaff !== ''){  
        this.disableSearch = false; 
        this.disableSelect = false;
      }else{ 
        this.disableSearch = true;
        this.disableSelect = true;
      }
    },
    loadMore: function(){
      if (this.loadMore === 1){     
        if(!this.is_last && this.current === 0){   
            this.chooseDelegator(this.pageDelegator);   
        }
        if (!this.is_last_memberList && this.current === 1){
          this.getListMemberOfCreate()
        }
      }
      return this.dataChooseDelegator;
    },
  },
  mounted(){
  },
  methods: {
    scaleScreen,
    handFilters() {
			const { filters } = this
			initFilters(filters)
			this.filters = { ...this.filters, ...JSON.parse(sessionStorage.getItem(this.page)) }
		},
    handleHover(event, record) {
      event.preventDefault();
      if (record) {
          this.tooltipDelegation.config = {
              visible: true,
              top: (event.pageY - 80) + 'px',
              left: (event.pageX - 650) + 'px',
              
          }
          this.tooltipDelegation.nameList = record.staff.name_list
      }
  },
    handleMouseLeave(){
      this.tooltipDelegation.config = false;
    },
    // get list of people to choose to delegate at create delegation
    chooseDelegator(page) {
      let param = {page: page,
        size: this.pageSize
      }
      this.pageDelegator++;
      let url = convertObjectToParams(param)
      this.$store.dispatch('delegation/chooseDelegatorPageable', url).then(res => {
        let data = [];
        this.is_last = res.page.is_last
        if (res && res.data) {
          data = res.data.map((item, index) => {
            item.key = index;
            return item;
          });
          // for (var item in data) {
          //   let deleteId = this.listExistedDelegator.find(id => id === data[item].id);
          //   if (deleteId) data[item] = "delete";
          // }
          //remove delegator that already be existed
          let keyTemp = []
          this.dataChooseDelegator =  [...this.dataChooseDelegator,...data.filter(dataItem => dataItem !== "delete")];
          // this.dataChooseDelegator = [...this.dataChooseDelegator, ...data]
          this.dataChooseDelegator.forEach(element => {
            keyTemp.push(element.key)
          });
          this.key = keyTemp
        }
      }).catch(error => {
        this.$message.error(error.message,5);

      })
    },
   async next() {
      this.current++;
      this.loadMore = 0;
      await this.getListMemberOfCreate();
      this.eventListener('TableMemberOfCreate');
      this.list_date_add.forEach(element => {
        element.staff.staff_list.forEach((item,index) => {
          if (item === this.delegatorAssign.id){
            element.staff.staff_list.splice(index,1)
          }
        })
      });
      this.dataMemberOfCreate.forEach((element,index) => {
        if (element.key === this.delegatorAssign.id){
          this.dataMemberOfCreate.splice(index,1)
        }
      })  
      this.getDataOptionFilterMember();    
      this.searchEmployee = undefined;
      this.previousStepDisabled = false;
    },
   async prev() {
      await this.current--;
      this.loadMore = 0;
      this.searchEmployee = undefined;
      // this.pageDelegator = 1;
      this.onApplyFilters(this.filters);
    },

    doneDeleteMember() {
      this.visibleDrawer = false
      this.getDelegationList();
    },

    formatDate(date) {
      return moment(date).format("DD/MM/YYYY");
    },
    formatSelectDate(date) {
      return moment(date, "DD/MM/YYYY").format("YYYY-MM-DD HH:mm:ss");
    },

    async onHandleChangeTab(key) {
      this.filters = {};
      this.currentTab = key
      if (key === 'delegation_list') {
        this.getDelegationList();
      } else {
        this.getDelegationExplanationRequest();
      }
    },
    eventListener(name){
      let x = document.getElementsByClassName(name)[0].querySelector('.ant-table-body')
        x.addEventListener('scroll',() =>{ if(x.scrollTop + x.clientHeight >= x.scrollHeight) {
          this.loadMore = 1
        }else{
          this.loadMore = 0
        }
      })
    },
    async showModalCreateOrEditDelegator(data) {
      this.mode = data.mode;
      this.visible = true;
      this.current = 0; 
      if (data.mode === 'new') {
        if (!this.is_last){
        await this.chooseDelegator(this.pageDelegator);
        this.eventListener('TableChooseDelegator');
        }
      } else {
        let record = data.record;
        this.delegatorAssign = record.delegator;
        this.current = 1;
        this.previousStepDisabled = true;
        this.dataEditDelegation = record;
        // this.visibleDrawer = false
        this.visibleDrawer = true
        await this.getListMemberOfCreate();
        this.eventListener('TableMemberOfCreate');
        this.getDelegationManagementDetail();
      }
    },

    onChangePage(page, size) {
      this.currentPage = page;
      this.pageSize = size;
      this.getDelegationList();
    },
    onShowSizeChange(page, pageSize) {
      this.currentPage = page;
      this.pageSize = pageSize;
      this.getDelegationList();
    },
    //function for delegation explanation request tab
    onChangePageExplain(page, size) {
      this.currentPage = page;
      this.pageSize_explanation = size;
      this.getDelegationExplanationRequest();
    },
    onShowSizeChangeExplain(page, pageSize) {
      this.currentPage = page;
      this.pageSize_explanation = pageSize;
      this.getDelegationExplanationRequest();
    },
    //function call in change steps in create delegation modal
    onChange(current) {
      this.current = current;
    },
    //function call when select delegator to delete
    onSelectChange(selectedRowKeys, selectedRows) {
      this.selectedRowKeys = selectedRowKeys
      this.list_delegator_delete = []
      for (let item of selectedRows) {
        this.list_delegator_delete.push(item.id);
      }

    },

    onSetDataFilters(filters) {
      this.filters = filters
    },
    onApplyFilters(filters) {
      const {currentTab} = this;
      this.currentPage = 1
      this.filters = filters;
      
      if (currentTab === 'delegation_list') {
        this.getDelegationList();
      } else {
        this.getDelegationExplanationRequest();
      }
    },
  
    onHandleSelectDelegator(dataChooseDelegator, selectedRows) {
      this.selectedRowKeysDelegator = dataChooseDelegator;
      this.delegatorAssign = selectedRows[0];
      this.nextStepDisabled = false;
    },

    addDateRange(e) {
      clearTimeout(this.timeOut)
      this.timeOut = setTimeout(() => {
        e.preventDefault();
        this.$emit("click");
        const {list_date_add} = this;
        let from_date_add = this.customRangeDate && this.customRangeDate[0] ? this.formatDate(this.customRangeDate[0]) : undefined;
        let to_date_add = this.customRangeDate && this.customRangeDate[1] ? this.formatDate(this.customRangeDate[1]) : undefined;
        let key = moment(this.customRangeDate[0]).valueOf() + moment(this.customRangeDate[1]).valueOf();
        this.activeItemStaff = key;
        let findIndex = list_date_add.findIndex(d => d.key === key);
        if (from_date_add && to_date_add) {
          if (findIndex > -1) {
            this.$message.warning('Item has already existed',5)
          } else {
            this.list_date_add.push({
              key: key,
              from_date: from_date_add,
              to_date: to_date_add,
              // staff_list: [],
              staff: {
                name_list: [],
                staff_list: []
              }
            });
            this.selectedRowKeysMember = [];
            if (this.list_date_add.length > 0) {
              this.listDateVisible = true;
            }
          }
          this.customRangeDate=''
        } else {
          this.$message.warning('Please select date range first!',5)
        }
      // this.selectedRowKeysMember = this.list_date_add.staff_list;
      }, 500)
    },
    onChooseDateRange(item) {
      this.activeItemStaff = item.key;
      this.selectedRowKeysMember = item.staff.staff_list;
      this.tooltipDelegation.config.visible = false
    },
    onDeleteDateRange(key) {
      this.showConfirm('deleteDateRange');
      this.keyDateRange = key;
      this.tooltipDelegation.config.visible = false
    },

    deleteDateRange(key) {
      const {
        list_date_add
      } = this;
      let findIndex = list_date_add.findIndex(d => d.key === key);
      if (findIndex > -1) {
        this.list_date_add.splice(findIndex, 1);
      }
    },

    //function call when change member selection 
    onSelectMemberDelegation(selectedRowKeys) {
      const {
        activeItemStaff,
        list_date_add
      } = this;
      let findCurrentItemIndex = list_date_add.findIndex(item => item.key === activeItemStaff);
      if (findCurrentItemIndex > -1) {
        this.list_date_add[findCurrentItemIndex].staff.staff_list = selectedRowKeys;
        this.selectedRowKeysMember = selectedRowKeys;
      }
    },

    onCancelModal() {
      this.pageDelegator = 0;
      this.pageMember = 0;
      this.loadMore = 0;
      this.selectedRowKeysDelegator = [];
      this.from_date_add = '';
      this.to_date_add = '';
      this.list_date_add = [];
      this.listDateVisible = false;
      this.selectedRowKeys = [];
      this.selectedRowKeysMember = [];
      this.delegatorAssign = {};
      this.dataChooseDelegator = [];
      this.dataEditDelegation = {};
      this.nextStepDisabled = true;
      this.searchEmployee = undefined;
      this.visible=false
    },


    formmatStaffListAssign(list_date_add) {
      let staff_list = [];
      // let staff= {};
      let index = -1;
      list_date_add.map(item => {
        let objStaff = {};
        item.staff.staff_list.map(d => {
          objStaff = {
            from_date: this.formatSelectDate(item.from_date),
            to_date: this.formatSelectDate(item.to_date),
            staff_id: d
          }     
          staff_list.push(objStaff)
          return d;
        });
        staff_list.forEach((element,indexOf) => {
          if (element.staff_id === this.delegatorAssign.id){
            index = indexOf;
          }
        });
        if(index != -1){
         staff_list.splice(index,1)
        }
        return item;
      });
      return staff_list
    },

    onCompleteUpdateDelegator(e) {
      clearTimeout(this.timeOut);
      this.timeOut = setTimeout(() => {
        e.preventDefault();
        this.$emit("click");
        const {list_date_add, delegatorAssign, dataEditDelegation, mode} = this;
        let params = {
          creator: delegatorAssign.id,
          delegator: delegatorAssign.id,
          flag:dataEditDelegation.flag,
          staff_list: this.formmatStaffListAssign(list_date_add)
        }
        let messageSuccess = 'Create delegator successfully!'
        let url = `/tms/delegate/create-delegator`;
        if (mode === 'edit') {
          url = `/tms/delegate/edit-delegator`;
          params.id = dataEditDelegation.id;
          messageSuccess = 'Update delegator successfully!'
        }
        this.$store.dispatch("delegation/createOrUpdateDelegation", {
          url: url,
          params: params
        }).then(res => {
          if (res && res.code === "000000") {
            this.$message.success(messageSuccess,5);
            this.getDelegationList();
            this.recordDetail = res.data
          } else{
            this.$message.error(res.message,5);
          } 
        }).catch(error => {
          this.$message.error(error.message,5);
  
        });
        this.visible = false;
        this.onCancelModal();
      }, 500)
    },

    onShowDrawer() {
      this.visibleDrawer = true;
    },
    onCloseDrawer() {
      this.visibleDrawer = false;
    },

    customRow(record) {
      return {
        on: {
          click: () => {
            this.visibleDrawer = true
            this.recordDetail = record
          }
        }
      }
    },

    //function customRow for Delegation explanation request tab
    customRow1(record) {
      return {
        on: {
          click: () => {
            this.onShowDrawer();
            this.dataAssignDetail = record;
            this.displayModal = true;
          },
        },
      };
    },

    //function get list of delegation
    getDelegationList() {
      this.$store.dispatch('user/loadingRequest')
      const {
        currentPage,
        pageSize,
        direction,
        filters
      } = this;
      let param = {
        ...filters,
        page: currentPage,
        size: pageSize,
        direction: direction,
      }
      const url = convertObjectToParams(param);
      this.$store.dispatch('delegation/getDelegationList', url).then(res => {
        let data = [];
        if (res && res.data) {
          data = res.data.map((item, index) => {
            item.key = index;
            item.name = item.delegator.name?item.delegator.name:null ;
            item.department = item.delegator.department_name &&  item.delegator.department_name !==null && item.delegator.department_name.name_en? item.delegator.department_name.name_en: null;
            item.explanation_date = this.formatDate(item.abnormal_date);
            this.listExistedDelegator.push(item.delegator.id);
            return item;
          });
          this.total_items = res.page.total_element;
          this.dataSource = data;
        }
        this.recordDetail = this.dataSource[0]
        this.$store.dispatch('user/loadingRequest')
      }).catch(error => {
        this.$message.error(error.message,5);
        this.$store.dispatch('user/loadingRequest')
      })
    },

    reduceArray(arr, keyGroup) {
      let array = [];
      array = arr.reduce(function (rv, x) {
        (rv[x[keyGroup]] = rv[x[keyGroup]] || []).push(x);
        return rv;
      }, []);

      let newArray = [];
      for (let key in array) {
        let obj = {
          key: key,
          staff: {},
        }
        obj.staff.staff_list = array[key].map((item, index) => {
          if (index === 0) {
            obj.from_date = item.from_date;
            obj.to_date = item.to_date;
          }
          return item.staff_id;
        });
        obj.staff.name_list = array[key].map((item, index) => {
          if (index === 0) {
            obj.from_date = item.from_date;
            obj.to_date = item.to_date;
          }
          return item.name;
        });
        newArray.push(obj);
      }
      return newArray;
    },
    // reduceArray(arr, keyGroup) {
    //   let array = [];
    //   array = arr.reduce(function (rv, x) {
    //     (rv[x[keyGroup]] = rv[x[keyGroup]] || []).push(x);
    //     return rv;
    //   }, []);

    //   let newArray = [];
    //   for (let key in array) {
    //     let obj = {
    //       key: key,
    //       staff_list: [],
    //       name_list: []
    //     }
    //     obj.staff_list = array[key].map((item, index) => {
    //       if (index === 0) {
    //         obj.from_date = item.from_date;
    //         obj.to_date = item.to_date;
    //       }
    //       return item.staff_id;
    //     });
    //     obj.name_list = array[key].map((item, index) => {
    //       if (index === 0) {
    //         obj.from_date = item.from_date;
    //         obj.to_date = item.to_date;
    //       }
    //       return item.name;
    //     });
    //     newArray.push(obj);
    //   }
    //   return newArray;
    // },
    //function get list of Delegation Management Detail
    getDelegationManagementDetail() {
      const {
        delegatorAssign
      } = this;
      let url = `?delegatorId=${delegatorAssign.id}`;
      this.$store.dispatch('delegation/getDelegationManagementDetail', url).then(res => {
        let data = [];
        if (res && res.data) {
          data = res.data.staff_list.map((item) => {
            return {
              key: moment(item.from_date).valueOf() + moment(item.to_date).valueOf(),
              name: item.staff_info.name,
              staff_id: item.staff_info.id,
              from_date: this.formatDate(item.from_date),
              to_date: this.formatDate(item.to_date),
            };
          });

          let newArray = this.reduceArray(data, 'key');
          this.list_date_add = newArray;
          if (newArray.length > 0) {
            this.activeItemStaff = newArray[0].key;
            this.selectedRowKeysMember = newArray[0].staff.staff_list;
          }

        }
      }).catch(error => {
        this.$message.error(error)
        this.loading = false
      })
    },

    //function get list of delegation explanation request
    getDelegationExplanationRequest() {
      const {
        currentPage,
        pageSize_explanation,
        direction_explanation,
        filters
      } = this;
      const paramFilter = convertObjectToParams({
        ...filters,
        page: currentPage,
        size: pageSize_explanation,
        direction: direction_explanation
      })
      this.$store.dispatch('user/loadingRequest');
      this.$store.dispatch('delegation/getDelegationExplanationRequest', paramFilter).then(res => {
        let data = [];
        if (res && res.data && res.code ==="000000") {
          data = res.data.map((item, index) => {
            item.key = index;
            item.created_by = item.employee;
            item.delegator = item.assignee;
            item.approved_by = item.approver ? item.approver.name : '';
            item.explanation_date = this.formatDate(item.abnormal_date);
            item.create_date = this.formatDate(item.created_date)
            item.statusText = statusText[item.status]
            item.reason_type = item.reason_detail
            return item;
          });
          this.total_items_explanation = res.page.total_element;
          this.dataExplanRequest = data;
          if (data.length > 0) {
            this.dataAssignDetail = data[0]
          }
        } else {
          this.$message.error(res.message,5);
        }
        this.$store.dispatch('user/loadingRequest')
      }).catch(error => {
        this.$message.error(error.message,5);
        this.$store.dispatch('user/loadingRequest')
      })
    },

    getDataOptionFilterDelegator(value){
      let url = `?employee=${value}`;
      this.fetching = true;
      this.$store.dispatch('delegation/chooseDelegator',url).then(res => {
        if (res && res.data) {
          this.dataOptionFilterDelegator =  res.data
        }
        this.fetching = false;
      }).catch(error => {
        this.$message.error(error)
        this.fetching = false;
      })
    },

    

    onChangeSearchModal(value) {
      this.searchEmployee = value;
    },

    onHandleSearchDelegator() {
      const {searchEmployee} = this;
      let url = `?employee=${searchEmployee}`;
      this.selectedRowKeysDelegator = []
      this.$store.dispatch('delegation/searchDelegator', url).then(res => {
          let data = [];
          if (res && res.data) {
            data = res.data.map((item, index) => {
              item.key = index;
              return item;
            });
            // for (var item in data) {
            //   let deleteId = this.listExistedDelegator.find(id => id === data[item].id);
            //   if (deleteId) data[item] = "delete";
            // }
            let keyTemp = []
            // this.dataChooseDelegator =  [...data.filter(dataItem => dataItem !== "delete")];
            this.dataChooseDelegator = [ ...data]
            this.dataChooseDelegator.forEach(element => {
              keyTemp.push(element.key)
            });
            this.key = keyTemp
          }
        }).catch(error => {
          this.$message.error(error.message,5);
  
        })
    },

    onSearchDelegator(value){
      if(value && value !== '' && value.length >= 3){
        this.getDataOptionFilterDelegator(value);
      }
    },
    onSearchMember(value) {
      if (value && value !== '' && value.length > 3) {
        let decodeUriVal = decodeURI(value)
        this.getDataOptionFilterMember(decodeUriVal);
      }
    },

    getDataOptionFilterMember(value){
      const {
        delegatorAssign
      } = this;
      let url = `?delegatorId=${delegatorAssign.id}`
      if (value){
         url = `?delegatorId=${delegatorAssign.id}&employee=${value}`;
      }    
      this.fetching = true;
      this.$store.dispatch('delegation/getListMemberOfCreate', url).then(res => {
        if (res && res.data) {
          this.dataOptionFilterMember = res.data;
          this.fetching = false;
        }
      }).catch(error => {
        this.$message.error(error)
        this.fetching = false;
      })
    },

    onHandleSearchMember(){
      const {searchEmployee,delegatorAssign} = this;
      this.pageMember = 1;
      let url = `?delegatorId=${delegatorAssign.id}`
      if (searchEmployee.length !== 0){
         url = `?delegatorId=${delegatorAssign.id}&employee=${searchEmployee}`;
      }
      this.$store.dispatch('delegation/getListMemberOfCreatePageAble', url).then(res => {
        let data = [];
        if (res && res.data) {
          data = res.data.map((item) => {
            item.key = item.id;
            return item;
          })
          data.forEach(element => {
            this.keyMember = element.key 
          });
          let keyTemp = []        
          this.dataMemberOfCreate = [...data];
          this.dataMemberOfCreate.forEach(element => {
            keyTemp.push(element.key)
          }); 
          this.keyMember = keyTemp
        }
        else {
          this.$message.info(res.message)
          this.dataMemberOfCreate = []
        }
      }).catch(error => {
        this.$message.error(error)
      })
    },

    onCleanFilter(mode){
      this.searchEmployee = undefined;
      this.pageDelegator = 0;
      this.pageMember = 0;
      if(mode === 'delegator'){
        this.dataChooseDelegator = [];
        this.selectedRowKeysDelegator = []
        this.nextStepDisabled = true;
        this.chooseDelegator();
      } else if(mode === 'member'){
        this.dataMemberOfCreate = []
        this.list_date_add.forEach(item => {
          if (item.key === this.activeItemStaff){
            item.staff.staff_list = []
          }
        })
        this.selectedRowKeysMember = []
        this.getListMemberOfCreate();
      }
    },

    //function get list of member when create new delegator
    getListMemberOfCreate(field) {
      const {
        delegatorAssign,
      } = this;
      this.pageMember++;
      let url = `?page=${this.pageMember}&&size=${this.pageSize}&&delegatorId=${delegatorAssign.id}${field ? field : ''}`;
      this.$store.dispatch('delegation/getListMemberOfCreatePageAble', url).then(res => {
        let data = [];
        if (res && res.data) {
          data = res.data.map((item) => {
            item.key = item.id;
            return item;
          })
          let keyTemp = []
          
          this.dataMemberOfCreate = [...this.dataMemberOfCreate,...data];
          data = this.dataMemberOfCreate;
          data.forEach(element => {
            let count = 0;
            this.dataMemberOfCreate.forEach((item,index) => {
             
              if (element.key === item.key){
                count++;
              }
              if(count > 1){
                this.dataMemberOfCreate.splice(index,1)
              }
            });
          });
          this.dataMemberOfCreate.forEach(element => {
            keyTemp.push(element.key)
          }); 
          this.keyMember = keyTemp
        }
      }).catch(error => {
        this.$message.error(error)
      })
    },

    deleteDelegation() {
      var data = {
        "delegate_list_id": this.list_delegator_delete
      }
      this.$store.dispatch('delegation/deleteDelegation', data).then(res => {
        if (res && res.code === "000000") {
          this.$message.success('Delete Delegator successfully!');
          this.getDelegationList();
        } else {
          this.$message.error(res.message,5);
        }
      }).catch(error => {
        this.$message.error(error.message,5);
      })
    },

    onDeleteDelegation() {
      this.showConfirm('deleteDelegator');
      this.selectedRowKeysDelegator = [];
    },
    
    showConfirm(deleteAction) {
      this.$confirm({
        title: 'Do you Want to delete these items?',
        onOk: () => {
          if (deleteAction === 'deleteDelegator') {
            this.deleteDelegation()
          } else if (deleteAction === 'deleteDateRange') {
            this.deleteDateRange(this.keyDateRange);
          }
        },
      });
    },
    myFunction() {
      document.body.style.position = "fixed"
    }
  },
};