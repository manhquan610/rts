import { mapState } from "vuex";
import moment from "moment";
import http from "../../../store/modules/helpers/httpInterceptor";
import { downloadFile} from "../../../components/commons/commons";

export default {
  name: "request-detail",
  props: {
    data: Object,
    statusColor: Object,
    formatDate: {
      type: Function,
    },
    formatTime: {
      type: Function,
    },
    getRequestList: {
      type: Function
    },
    showModalEdit: {
      type: Function
    },
    checkDateWithinApprovedTimesheet: {
      type: Function
    },
    assignee: Object,
    userInfo: Object,
    Role: Object,
    fileList : {type: Array},
    isAvaiableToDownLoad: Boolean,
  },
  data() {
    return {
      formItemLayout: {
        labelCol: { span: 8 },
        wrapperCol: { span: 16 },
      },
      visibleDelegate: false,
      visibleModalReject: false,
      isSingleRecord: false,
      delegator: null,
      dataSource: this.data,
      fetching: false,
      formData: {
        id: '',
        reason_type: '',
        status_type: '',
        description: '',
        created_date: '',
        approver: '',
        last_modified: '',
        start_date: '',
        end_date: '',
        start_time: '',
        end_time: '',
        employee: '',
        leaving_type: '',
      },
      request_type : 'request',
      reasonReject: this.data.reasons_for_rejection || null,
    }
  },

  beforeCreate() {
    this.form = this.$form.createForm(this, { name: 'request_detail' });
  },
  created() {
    this.dataSource.assignee = this.data.approver.name ? (this.data.approver.name + " - " + this.data.approver.department_name.nameEn) : 'Guest-manager'
  },
  watch: {
    data: function () {
      this.dataSource = this.data
      this.dataSource.assignee = this.data.approver.name ? (this.data.approver.name + " - " + this.data.approver.department_name.nameEn) : 'Guest-manager'
    },
  },
  computed: {
    ...mapState({
      roles: state => state.user.userInfo.roles ? state.user.userInfo.roles : "",
      employeeList: state => state.application.employeeList,
    })
  },
  methods: {

    handleDelegate() {
      this.visibleDelegate = !this.visibleDelegate
    },
    handCancelDelegate() {
      this.delegator = null;
      this.visibleDelegate = false;
    },
    handleCancel() {
      this.visibleModalReject = false;
    },
    handleOk() {
      if (this.reasonReject && this.reasonReject.trim() !== '') {
        let payload = {
          reasons_for_rejection: this.reasonReject || '',
          employee: this.data.employee.id,
          id: this.statusPayload.id,
          status_type: 'Rejected',
          approver: this.userInfo.userId
        }
        this.updateStatus(payload, "REJECTED")
        this.visibleModalReject = false
      } else {
        this.$message.error('Message required!', 5)
      }
    },
    checkDisable(record) {
      if (record.status_type.toUpperCase() === "REJECTED" || record.status_type.toUpperCase() === "CANCELLED" || record.checkDate) {
        return true
      }
      return false
    },
    checkDisableApprove(record) {
      if (record.status_type.toUpperCase() === "APPROVED") {
        return true
      }
      return false
    },
    handleSaveDelegate() {
      let payload = {
        data: {
          approver: JSON.parse(this.delegator).id,
          id: this.data.id ? this.data.id : '',
          reason_type: this.data.reason_type ? this.data.reason_type : '',
          status_type: this.data.status_type ? this.data.status_type : '',
          last_modified: this.data.last_modified_date ? this.data.last_modified_date : '',
          employee: this.data.employee.id ? this.data.employee.id : '',
          leaving_type: 'ANNUAL'
        }
      }
      this.$store.dispatch('request/updateRequestDelegate', payload.data).then(res => {
        if (res) {
          this.dataSource.assignee = JSON.parse(this.delegator).label + ' - ' + JSON.parse(this.delegator).department
          this.$message.success('Delegate successfully!', 5);
          this.dataSource.approver.user_name = JSON.parse(this.delegator).user_name
          this.visibleDelegate = false;
          this.getRequestList()
        }
      }).catch(() => {
        this.$message.error('Failed to update status!', 5);
      });
    },
    onSearchDelegator(value) {
      let cloneVal = decodeURI(value);
      if (value && value !== "" && value.length >= 3) {
        this.fetching = true;
        clearTimeout(this.timeout);
        this.timeout = setTimeout(() => {
          this.$store
            .dispatch("application/getEmployeeList", cloneVal)
            .then(() => {
              this.fetching = false;

            })
            .catch(() => {
              this.fetching = false;
            });
        }, 1000);
      }
    },
    onChangeValue(value) {
      return value;
    },
    checkVisible(record) {
      return record.employee.user_name === this.userInfo.userName
    },
    onClickWithDraw(record) {
      if (record.status_type === 'PENDING' || (record.status_type === 'APPROVED')) {
        let payload = {
          employee: this.dataSource.employee.id,
          id: this.dataSource.id,
          status_type: 'Cancelled',
          approver: this.userInfo.userId
        }
        this.$confirm({
          title: 'Confirm',
          content: 'Do you want to withdraw this request?',
          okText: 'Yes',
          onOk: () => {
            this.updateStatus(payload, "CANCELLED")
          },
          cancelText: 'No',
        });
      } else {
        this.$message.error('Timesheet has already been approved', 5);
      }
    },
    onClickReject(record) {
      this.statusPayload = record
      this.visibleModalReject = true
    },
    onClickApprove(record) {
      if (record.status_type === 'PENDING') {
        let payload = {
          employee: record.employee.id,
          id: record.id,
          status_type: 'Approved',
          approver: this.userInfo.userId
        }
        this.updateStatus(payload, 'APPROVED')
      }
    },
    updateStatus(payload, status) {
      this.$store.dispatch('request/updateStatusRequest', payload)
        .then(res => {
          if (res) {
            if (parseInt(res.code) === 3113) {
              this.$message.success('Update status successfully!', 5);
              this.reasonReject = ''
              this.statusPayload = []
              this.dataSource.status_type = status
              if (status === 'REJECTED') {
                this.dataSource.reasons_for_rejection = this.reasonReject
              }
              this.getRequestList()
            } else {
              this.$message.error(res.message, 5);
            }
          }
        }).catch(error => {
          this.$message.error(error.message, 5);
        });
    },
    checkRequestDate(record) {
      const { currentDate } = this
      let temp = true
      if (moment(currentDate).format('DD/MM/YYYY') > this.formatDate(record.start_date)) {
        if (record.status_type === 'APPROVED') {
          temp = false;
        } else {
          temp = true;
        }
      }
      return temp;
    },
  
    handleDownload() {
      const { fileList } = this
      fileList.forEach(file => {
        http.get(`/tms/request-management/${this.request_type}/${this.data.id}/download?name=` + file.name, { responseType: 'blob' })
          .then(res => {
            downloadFile(res.data, file.name)
            return res.data;
          })
          .catch(err => {
            throw err
          })
      })
    }
  },
};
