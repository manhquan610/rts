const columns = [
  {
    title: "Request",
    dataIndex: "request",
    name: "request",
    scopedSlots: { customRender: 'request' },
    width: '10%',
  },
  {
    title: "Staff",
    dataIndex: "display_name",
    class: 'cursor-pointer',
    scopedSlots: { customRender: 'display_name' },
    width: '20%'
  },
  {
    title: "From Date",
    dataIndex: "from_date",
    name: "from_date",
    width: '10%',
  },
  {
    title: "To Date",
    dataIndex: "to_date",
    name: "to_date",
    width: '10%',
  },
  {
    title: "Duration (Day)",
    dataIndex: "quantity_in_days",
    name: "duration_day",
    scopedSlots: { customRender: "duration_day" },
    width: '10%'
  },
  {
    title: "Reason",
    dataIndex: "reason",
    name: "reason",
    width: '13%',
    // scopedSlots: {customRender: "reason"}
  },
  {
    title: "Approver",
    dataIndex: "approver",
    name: "approver",
    scopedSlots: { customRender: "approver" },
    width: '20%'
  },
  {
    title: "Status",
    dataIndex: "statusText",
    scopedSlots: { customRender: "statusText" },
    align: 'center',
    width: '6%'
  },
  {
    title: "",
    dataIndex: "edit",
    scopedSlots: { customRender: "edit" },
    width: '2%'
  },
  // {
  //   title: "Start Time",
  //   dataIndex: "start_time",
  //   name: 'startTime'
  // },
  // {
  //   title: "End Time",
  //   dataIndex: "end_time",
  //   name: "endTime"
  // },
  // {
  //   title: "Duration",
  //   dataIndex: "quantity_in_hours",
  //   name: "duration",
  //   width: "7%",
  //   scopedSlots: { customRender: "quantity_in_hours" },
  // },
  // {
  //   title: "Last Modified Date",
  //   dataIndex: "last_modify_date",
  //   name: "last_modify_date",
  //   // scopedSlots: { customRender: "quantity_in_hours" },
  // },

];

import RequestDetail from "./RequestDetail/RequestDetail.vue";
import FilterLayout from "../../components/layouts/FilterLayout";
import { mapState } from "vuex";
import moment from "moment";
import { convertObjectToParams, formatDate, formatTime, downloadFile, scaleScreen, initFilters } from "../../components/commons/commons";
import { statusColor } from '../../components/commons/commonConstant';
import { Role } from "../../components/commons/commonConstant";
import http from "../../store/modules/helpers/httpInterceptor";

export default {
  name: "RequestManage",
  components: { RequestDetail, FilterLayout },
  // components: {  FilterLayout },
  data() {
    return {
      page: "Request Management",
      columns,
      recordDetail: {},
      dataSource: [],
      pagination: {},
      loading: false,
      filterKey: 1,
      displayModal: true,
      currentPage: 1,
      pageSize: 10,
      field: 'title',
      direction: 'ASC',
      total_items: 0,
      dataFilters: {},
      requestListInfo: {},
      statusColor: statusColor,
      moment,
      filters: {},
      // visible: false,
      clickButtonEdit: false,
      visibleDrawer: false,
      Role,
      firstCall: true,
      visibleDropDown: false,
      formData: {
        id: null,
        request_type: '',
        reason_type: '',
        description: '',
        approver: null,
        created_date: '',
        employee: null,
        end_date: null,
        start_shift: '',
        is_from_sf4c: false,
        last_modified: '',
        start_date: null,
        end_shift: '',
        status_type: '',
        start_time: null,
        end_time: null,
        abnormal_date: null,
        abnormal: null
      },
      approver: {},
      staffOT: [],
      cc_mail: [],
      abnormal_list: [],
      abnormalList: [],
      formRQ: this.$form.createForm(this, { name: 'Request' }),
      visibleModal: false,
      isEdit: false,
      reasons: [],
      types: [],
      tempCheckDate: [],
      request_reasons: [],
      fetching: false,
      isCreated: false,
      isWorkOutSide: false,
      tempMail: '',
      statusPayload: [],
      reasonReject: '',
      visibleModalReject: false,
      isApproved: false,
      salary: '',
      remain_day: 0,
      shiftData: [],
      durations: {
        duration: null,
        startTime: null,
        endTime: null
      },
      wasCalled: false,
      startDate: {},
      endDate: {},
      disableExport: true,
      timeOut: null,
      birthdayLeave: false,
      isMultipleStaffsModal: false,
      isExplanation: false,
      size: window.outerWidth > 1000 ? window.outerWidth * 0.6 : window.outerWidth * 0.55,
      fileList: [],
      isAvaiableToDownLoad: false,
      request_type : 'request'
    };
  },
  watch: {
    visible: function () {
      this.myFunction();
    },
    // formData: function () {
    //   this.formRQ.setFieldsValue(
    //     this.formData
    //   )
    // },
    filters: function () {
      if (this.filters.fromDate !== undefined && this.filters.toDate !== undefined) {
        this.disableExport = false
      } else {
        this.disableExport = true
      }
    }
  },
  created() {
    this.scaleScreen()
    this.handFilters()
    this.firstCall = true;
    this.getApprover();
    this.getLastTimesheetInfo();
    this.getAbnormalOfCurrentUser();
    window.addEventListener('resize', this.resize)
  },

  computed: {
    ...mapState({
      requestList: (state) => state.request.requestList,
      roles: state => state.user.userInfo.roles ? state.user.userInfo.roles : "",
      employeeList: state => state.application.employeeList,
      userInfo: (state) => state.user.userInfo,
      dataRequestSelections: state => state.request.dataRequestSelections,
      lastTimesheet: state => state.application.lastTimesheet
    }),
  },

  methods: {
    formatDate,
    formatTime,
    scaleScreen,
    handFilters() {
			const { filters } = this
			initFilters(filters)
			if (sessionStorage.getItem(this.page)) {
        this.filters = { ...JSON.parse(sessionStorage.getItem(this.page)) }
      } else {
        this.filters = { ...this.filters, ...JSON.parse(sessionStorage.getItem(this.page)) }
      }
		},
    resize() {
      if (document.body.clientWidth > 1000) {
        this.size = document.body.clientWidth * 0.6
      } else {
        this.size = document.body.clientWidth * 0.55
      }
    },
    getRequestList() {
      const { currentPage, pageSize, filters } = this;
      let paramFilter = ''
      if (this.firstCall) {
        paramFilter = convertObjectToParams({
          ...filters,
        });
        this.currentPage = filters.page ? filters.page : 1
        this.pageSize = filters.size ? filters.size : 10
      } else {
        paramFilter = convertObjectToParams({
          ...filters,
          page: currentPage,
          size: pageSize,
        });
      }
      this.$store.dispatch('user/loadingRequest')
      this.$store.dispatch('request/getRequestData', paramFilter).then((res) => {
        if (res.data) {
          let data = [];
          res.data.forEach((element, key) => {
            this.checkDateWithinApprovedTimesheet(element.start_date, key)
          });
          setTimeout(() => {
            let temp = this.tempCheckDate
            data = res.data.map((element, key) => {
              element.key = key
              element.status_type = element.status_type.toUpperCase()
              element.display_name = element.employee.name + ' - ' + element.employee.department_name.name;
              element.from_date = formatDate(element.start_date);
              element.to_date = formatDate(element.end_date);
              element.duration_day = element.quantity_in_days;
              element.last_modify_date = formatDate(element.last_modified_date) ? formatDate(element.last_modified_date) : null;
              element.type = element.request_type_name_en ? element.request_type_name_en : '';
              element.reason = element.reason_type_name ? element.reason_type_name : '';
              temp.forEach(item => {
                if (item.key === key) {
                  element.checkDate = item.check
                }
              });
              this.tempCheckDate = []
              return element;
            })
            this.total_items = res.page.total_element;
            this.dataSource = data
            this.$store.dispatch('user/loadingRequest')
          }, 2000)
          this.firstCall = false;
        } else if (res.message !== null && res.message !== '') {
          this.$message.error(res.message, 5);
          this.$store.dispatch('user/loadingRequest')
        }
      }).catch(error => {
        this.$message.error(error.message, 5);
        this.$store.dispatch('user/loadingRequest')
      })
      sessionStorage.setItem(this.page, JSON.stringify({
        ...filters,
        page: currentPage,
        size: pageSize,
      }))
    },

    exportRequest() {
      const { filters } = this;
      const paramFilter = convertObjectToParams({
        ...filters
      });
      this.$store.dispatch('user/loadingRequest');
      http.get(`tms/excel/request-list${paramFilter}`, { responseType: 'blob' })
        .then(res => {
          if (res.status === 200 && res.data.type !== 'application/json') {
            downloadFile(res.data, `Request_List_${moment(filters.fromDate).format('DD/MM/yyyy')}-${moment(filters.toDate).format('DD/MM/yyyy')}.xlsx`);
            this.$store.dispatch('user/loadingRequest');
            this.$message.success('Request List is exported successfully!', 5);
          } else {
            this.$store.dispatch('user/loadingRequest');
            this.$message.error('No data export', 5);
          }
        })
        .catch(err => {
          this.$store.dispatch('user/loadingRequest');
          this.$message.error(err.response.data.error, 5)
        })
    },

    customRow(record) {
      return {
        on: {
          click: () => {
            this.isAvaiableToDownLoad = false
            this.fileList.splice(0, this.fileList.length)
            if (!this.clickButtonEdit) {
              this.visibleDrawer = true;
              this.recordDetail = record;
              this.getAllListFile(record.id)
              this.onColumns = this.userList;
            } else {
              this.clickButtonEdit = false
            }
          },
        },
      };
    },
    handleOnClickEdit(i) {
      console.log('iiii',i)
      this.fileList.splice(0, this.fileList.length)
      this.getAllListFile(i.id)
      this.clickButtonEdit = true
      if (!this.roles.includes(Role.GUEST)) {
        this.$store.dispatch('request/getParamsDataToSelect').then((res) => {
          if (res.data) {
            this.request_reasons = res.data.request_reason;
            this.formData.request_type = i.request_type;
            if (i.reason_type === 'BIRTHDAY_LEAVE') {
              this.remain_day = res.data.paid_birthday_leave
            } else if (i.reason_type === 'LOA_CORP_11') {
              this.remain_day = res.data.offset_day
            }
            else if (i.reason_type === 'ANN_LEAVE_CORP') {
              this.remain_day = res.data.annual_leave_balance
            }
            this.shiftData = res.data.types;
            this.request_reasons.forEach(request => {
              if (request.request_type === i.request_type_name) {
                this.reasons = request.reasons;
                this.formData.reason_type = i.reason_type;
              }
            })
          } else {
            this.$message.error(res.message, 5);
          }
        })
        this.cc_mail = []
        this.tempMail = ''
        this.salary = i.salary
        this.approver = i.approver
        this.formData = {
          id: i.id,
          description: i.description,
          start_time: i.start_time,
          approver: i.approver,
          created_date: i.createDate,
          employee: i.employee,
          end_date: i.end_date,
          end_time: i.end_time,
          is_from_sf4c: false,
          last_modified: '',
          start_date: i.start_date,
          status_type: i.status_type,
          start_shift: i.start_shift,
          end_shift: i.end_shift,
        }
        this.getDurationTime(i)
        try {
          if (i.mail.length > 0) {
            i.mail.forEach(element => {
              this.cc_mail.push(element.id);
              this.tempMail = this.tempMail + element.user_name + ' '
            });
          }
        } catch (error) {
          this.$message.error(error, 5);
        }
      }
    },
    formatHour(date) {
      return moment(date).format("HH:mm")
    },

    onShowSizeChange(page, pageSize) {
      this.currentPage = page;
      this.pageSize = pageSize;
      this.getRequestList()
    },
    onChangePage(page, pageSize) {
      this.currentPage = page;
      this.pageSize = pageSize;
      this.getRequestList()
    },

    onApplyFilters(filters) {
      this.filters = filters
      this.currentPage = 1;
      this.getRequestList()
    },
    onSetDataFilters(filters) {
      this.filters = filters;
    },
    // showDrawer() {
    //   this.visibleDrawer = true;
    // },
    afterVisibleChange() {
    },
    onClose() {
      this.visibleDrawer = false;
    },
    myFunction() {
      // document.body.style.position = "fixed"
    },
    async showModalCreate(type) {
      this.fileList.splice(0, this.fileList.length)
      this.$store.dispatch('request/getParamsDataToSelect').then((res) => {
        if (res.data) {
          res.data.types.forEach(element => {
            this.types.push(element);
          });
          this.getRequestAndReasons();
          this.cc_mail = []
          this.formData = {}
          this.salary = ''
          this.formRQ.resetFields()
          type === 'multiple' ? this.isMultipleStaffsModal = true : this.isMultipleStaffsModal = false
          this.visibleModal = true;
          this.isEdit = false;
        }
      }).catch(() => {
        this.$message.error('Open modal create request fail!')
      });
    },
    async getRequestAndReasons() {
      await this.$store.dispatch('request/getParamsDataToSelect').then((res) => {
        if (res.data) {
          this.request_reasons = res.data.request_reason;
          this.remain_day = res.data.annual_leave_balance;
          this.shiftData = res.data.types;
        } else {
          this.$message.error(res.message, 5);
        }
      })
    },
    checkVisibleBalances(value) {
      if (value === 'BIRTHDAY_LEAVE' || value === 'LOA_CORP_11' || value === 'ANN_LEAVE_CORP') {
        return true
      }
      return false
    },
    async onChangeRequestReasons(value) {
      if (value === 'EXPLANATION') {
        this.isExplanation = true;
      } else this.isExplanation = false;
      try {
        await this.request_reasons.forEach(item => {
          if (item.request_type_code === value) {
            this.formRQ.setFieldsValue({ reason_type: undefined });
            this.reasons = item.reasons;
          }
        })

      } catch (error) {
        this.$message.error(error, 5);
      }
    },
    onChangeReason(value) {
      try {
        if (value === 'BIRTHDAY_LEAVE') {
          this.birthdayLeave = true
          if (this.formRQ.getFieldValue('start_date')) {
            this.formRQ.setFieldsValue({
              end_date: this.formRQ.getFieldValue('start_date'),
              start_shift: 'MORNING', end_shift: 'AFTERNOON'
            })
            this.formData.start_shift = 'MORNING'
            this.formData.end_shift = 'AFTERNOON'
            this.formData.start_date = this.formRQ.getFieldValue('start_date')
            this.formData.end_date = this.formData.start_date
            this.getDurationTime(this.formData)
          }
          this.remain_day = this.dataRequestSelections.data.paid_birthday_leave
        } else if (value === 'LOA_CORP_11') {
          this.birthdayLeave = false
          this.remain_day = this.dataRequestSelections.data.offset_day
        }
        else if (value === 'ANN_LEAVE_CORP') {
          this.birthdayLeave = false
          this.remain_day = this.dataRequestSelections.data.annual_leave_balance
        } else {
          this.birthdayLeave = false
        }
        this.reasons.forEach(item => {
          if (item.reason_type_code === value) {
            this.salary = item.salary;
          }
        })

      } catch (error) {
        this.$message.error(error, 5);
      }
    },
    handleCancel() {
      this.formRQ.resetFields()
      this.salary = '';
      this.formData = []
      this.birthdayLeave = false
      this.visibleModal = false;
      this.isMultipleStaffsModal = false
      this.wasCalled = false
    },
    onSearchStaff(value, timeout) {
      let cloneVal = decodeURI(value);
      if (value && value !== "" && value.length >= 3) {
        this.fetching = true;
        clearTimeout(this.timeout);
        this.timeout = setTimeout(() => {
          this.$store
            .dispatch("application/getEmployeeList", cloneVal)
            .then(() => {
              this.fetching = false;
            })
            .catch(() => {
              this.fetching = false;
            });
        }, timeout);
      }
    },
    onChangeCCValue(value) {
      this.cc_mail = value
    },
    onChangeAbnormal(value) {
      this.abnormal_list = value
    },
    getAbnormalOfCurrentUser() {
      let params = {
        page: 1,
        size: 20,
        direction: -1,
        employee: this.userInfo.userName
      };
      const url = convertObjectToParams(params)
      this.$store.dispatch('abnormalcase/getAbnormalCaseData', url).then(res => {
        if (res.data) {
          this.abnormalList = res.data
        } else {
          this.$message.error(res.message)
        }
      })
    },
    filterOption(input, option) {
      return (
        option.componentOptions.children[0].text.toLowerCase().indexOf(input.toLowerCase()) >= 0
      );
    },
    getApprover() {
      return http
        .get("/tms/filters/get-approver-for-user?username=" + this.userInfo.userName)
        .then((res) => {
          this.approver = res.data
        })
        .catch(err => {
          throw (err)
        })
    },
    getApproverFromDelegation(value) {
      let params = {
        userName: this.userInfo.userName,
      }
      params.date = value.format('DD-MM-YYYY')
      return http
        .get("/tms/filters/get-approver-for-user?username=" + params.userName + '&date=' + params.date)
        .then((res) => {
          this.approver = res.data
        })
        .catch(err => {
          throw (err)
        })
    },
    getDurationTime(requestBody) {
      let temp = {
        start_shift: requestBody.start_shift ? requestBody.start_shift : null,
        end_shift: requestBody.end_shift ? requestBody.end_shift : null,
        end_date: requestBody.end_date ? moment(requestBody.end_date).format('YYYY-MM-DD 00:00:00') : null,
        start_date: requestBody.start_date ? moment(requestBody.start_date).format('YYYY-MM-DD 00:00:00') : null,
      }
      if (temp.start_date && temp.end_date && temp.start_shift && temp.end_shift) {
        setTimeout(() => {
          this.wasCalled = true
        }, 200)
        this.$store.dispatch('request/getDurationTime', temp)
          .then(res => {
            this.durations.duration = res.data.quantity_in_days
            this.durations.startTime = formatTime(res.data.start_time)
            this.durations.endTime = formatTime(res.data.end_time)
          })
          .catch(err => {
            this.$message.error(err, 5);
          })
      }
    },

    handleSubmit(e) {
      clearTimeout(this.timeOut);
      this.$store.dispatch('user/loadingRequest')
      this.timeOut = setTimeout(() => {
        e.preventDefault();
        this.$emit("click");
        this.formRQ.validateFields((err, values) => {
          let createDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
          if (!err) {
            let payload = {
              request_type: values.request_type,
              reason_type: values.reason_type,
              description: values.description ? values.description : '',
              mail_cc: this.cc_mail ? this.cc_mail : [],
              approver: this.approver.id ? this.approver.id : '',
              created_date: createDate,
              employee: this.isMultipleStaffsModal ? '' : this.userInfo.userId,
              is_from_sf4c: false,
              last_modified: values.last_modified,
              status_type: this.isMultipleStaffsModal ? 'Approved' : 'Pending',
              start_shift: values.start_shift,
              end_shift: values.end_shift,
              start_date: moment(values.start_date).format('YYYY-MM-DD 00:00:00'),
              end_date: moment(values.end_date).format('YYYY-MM-DD 00:00:00'),
              reasons_for_rejection: values.reasons_for_rejection,
              salary: this.salary,
              start_time: moment(values.start_date).format('YYYY-MM-DD') + ' ' + this.durations.startTime + ':00',
              end_time: moment(values.end_date).format('YYYY-MM-DD') + ' ' + this.durations.endTime + ':00'
            }
            if (this.isMultipleStaffsModal) {
              payload.usernames = values.userlist.split(',').map(obj => obj.trim())
              payload.usernames.map(element => { return element.trim() })
            }
            if (!this.isEdit && (payload.end_date >= payload.start_date)) {
              this.$store.dispatch(this.isMultipleStaffsModal ? 'request/createBatchRequest' : 'request/createRequest', payload)
                .then(res => {
                  if (parseInt(res.code) === 201) {
                    this.wasCalled = false;
                    this.$message.success(res.message, 5);
                    this.getRequestList();
                    this.formRQ.resetFields();
                    this.handleUpload(res.id)
                    this.birthdayLeave = false
                    this.visibleModal = false;
                    this.isMultipleStaffsModal = false
                  } else if (typeof (res.message) === 'string') {
                    this.$message.error(res.message, 5);
                  } else {
                    res.message.length ? res.message.forEach(element => {
                      setTimeout(this.$message.error(element, 2), 1000);
                    }) : this.$message.error('Message unavailable', 2);
                  }
                }).catch(error => {
                  this.$message.error(error.message, 5);
                  this.$store.dispatch('user/loadingRequest')
                })
            } else if (this.isEdit) {
              payload.id = this.formData.id;
              this.$store.dispatch('request/updateRequest', payload)
                .then(res => {
                  if (res) {
                    if (parseInt(res.code) === 204) {
                      this.$message.error(res.message, 5);
                    } else {
                      this.$message.success('Update request successfully!', 5);
                      this.wasCalled = false;
                      this.formRQ.resetFields();
                      this.birthdayLeave = false
                      this.getRequestList();
                      this.handleUpload(this.formData.id)
                      this.visibleModal = false;
                      this.isMultipleStaffsModal = false
                    }
                  }
                }).catch(error => {
                  this.$message.error(error.message, 5);
                  this.$store.dispatch('user/loadingRequest')
                })
            } else {
              this.$message.error('End date must higher than start date', 5);
            }
          }
          else {
            this.$message.error(err, 5);

          }
        })
      }, 300)
      this.$store.dispatch('user/loadingRequest')
    },
    async showModalEdit() {
      this.$store.dispatch('user/loadingRequest')
      await this.onSearchStaff(this.tempMail)
      setTimeout(() => {
        this.visibleModal = true
        this.isEdit = true;
        this.$store.dispatch('user/loadingRequest')
      }, 2000)
    },

    handleStartShift(value) {
      this.formData.start_shift = value;
      if (this.formData.start_date === this.formData.end_date && this.formData.end_shift !== 'AFTERNOON') {
        this.formData.end_shift = this.formData.start_shift;
        this.formRQ.setFieldsValue({ 'end_shift': this.formData.start_shift })
      }
      this.getDurationTime(this.formData)
    },
    handleEndShift(value) {
      this.formData.end_shift = value;
      this.getDurationTime(this.formData)
    },
    handleStartDate(value) {
      this.formData.start_date = value.format('YYYY-MM-DD');
      if (this.formData.start_date > this.formData.end_date || this.formData.end_date === null || this.formData.end_date === undefined) {
        this.formData.end_date = moment(moment(this.formData.start_date).format('YYYY-MM-DD'))
        this.formRQ.setFieldsValue({ 'end_date': moment(moment(this.formData.start_date).format('YYYY-MM-DD')) })
      }
      this.formRQ.setFieldsValue({ 'start_shift': 'MORNING' })
      this.formData.start_shift = 'MORNING'
      if (this.formRQ.getFieldValue('reason_type') !== 'BIRTHDAY_LEAVE') {
        this.formRQ.setFieldsValue({ 'end_shift': this.formData.start_shift })
        this.formData.end_shift = this.formData.start_shift
      } else {
        this.formRQ.setFieldsValue({ 'end_shift': 'AFTERNOON' })
        this.formData.end_shift = 'AFTERNOON'
        this.formData.end_date = this.formData.start_date
        this.formRQ.setFieldsValue({ 'end_date': this.formData.start_date })
      }
      this.getApproverFromDelegation(value)
      this.getDurationTime(this.formData)
    },
    handleEndDate(value) {
      this.formData.end_date = value.format('YYYY-MM-DD');
      if (this.formData.start_date === this.formData.end_date
        && this.formData.start_shift === 'AFTERNOON') {
        this.formRQ.setFieldsValue({ 'end_shift': this.formData.start_shift })
        this.formData.end_shift = this.formData.start_shift
      }
      this.getDurationTime(this.formData)
    },

    onChangeDate(value) {
      this.formData.abnormal_date = value
    },

    onChangeStartTime(value) {
      if (moment(moment(value).format('HH:mm:ss'), 'HH:mm:ss').isAfter(moment(moment(this.formRQ.getFieldValue('end_time')).format('HH:mm:ss'), 'HH:mm:ss'))) {
        this.formRQ.setFieldsValue({ end_time: value })
      }
    },
    onChangeEndTime(value) {
      if (moment(moment(value).format('HH:mm:ss'), 'HH:mm:ss').isBefore(moment(moment(this.formRQ.getFieldValue('start_time')).format('HH:mm:ss'), 'HH:mm:ss'))) {
        this.formRQ.setFieldsValue({ start_time: value })
      }
    },
    updateStatus(payload) {
      this.$store.dispatch('request/updateStatusRequest', payload)
        .then(res => {
          if (res) {
            if (parseInt(res.code) === 3113) {
              this.$message.success(res.message, 5);
              this.reasonReject = ''
              this.statusPayload = []
              this.getRequestList()
              this.getRequestAndReasons()
              this.formRQ.resetFields()
              this.birthdayLeave = false
            } else {
              this.$message.error(res.message, 5);
            }
          }
        }).catch(error => {
          this.$message.error(error.message, 5);
        });
    },
    onClickApprove(record) {
      if (record.status_type === 'PENDING') {
        let payload = {
          employee: record.employee.id,
          id: record.id,
          status_type: 'Approved',
          approver: this.userInfo.userId
        }
        this.updateStatus(payload)
      }
    },
    onClickReject(record) {
      this.statusPayload = record
      this.visibleModalReject = true
    },
    onClickWithDraw(record) {
      if (record.status_type === 'PENDING' || (record.status_type === 'APPROVED')) {
        let payload = {
          employee: record.employee.id,
          id: record.id,
          status_type: 'Cancelled',
          approver: this.userInfo.userId
        }
        this.$confirm({
          title: 'Confirm',
          content: 'Do you want to withdraw this request?',
          okText: 'Yes',
          onOk: () => {
            this.updateStatus(payload)
          },
          cancelText: 'No',
        });
      } else {
        this.$message.error('Out of timesheet Approved', 5);
      }

    },
    handleCancelRejectModal() {
      this.visibleModalReject = false;
    },
    checkWithdraw(record) {
      if ((record.status_type === 'PENDING')) {
        return true;
      }
      else if (record.status_type === 'APPROVED') {
        return !this.checkDateInCurrentTimeSheet(record)
      }
      else {
        return false
      }
    },
    handleOk() {
      if (this.reasonReject && this.reasonReject.trim() !== '') {
        let payload = {
          reasons_for_rejection: this.reasonReject || '',
          employee: this.statusPayload.employee.id,
          id: this.statusPayload.id,
          status_type: 'Rejected',
          approver: this.userInfo.userId
        }
        this.updateStatus(payload)
        this.visibleModalReject = false
      } else {
        this.$message.error('Message required!', 5)
      }
    },
    checkApprove(record) {
      let temp = true
      if (record.status_type === 'PENDING') {
        temp = true;
      } else {
        temp = false;
      }

      return temp;
    },
    checkReject(record) {
      return (record.status_type === 'APPROVED' || record.status_type === 'PENDING') ? false : true;
    },
    async checkDateWithinApprovedTimesheet(date, key) {
      const params = moment(date).format('YYYYMMDD').toString();
      await this.$store.dispatch('request/checkDateWithinApprovedTimesheet', params).then((res) => {
        if (res) {
          this.tempCheckDate.push({ key: key, check: res.data })
        } else {
          this.$message.error(res.message, 5);
        }
      }).catch(error => {
        this.$message.error(error.message, 5);
      });
    },
    checkDateInCurrentTimeSheet(record) {
      const params = moment(record.created_date).format('YYYYMMDD').toString();
      this.$store.dispatch('request/checkDateWithinApprovedTimesheet', params).then((res) => {
        if (!res) {
          this.$message.error(res.message, 5);
        }
      }).catch(error => {
        this.$message.error(error.message, 5);
      });
    },
    getLastTimesheetInfo() {
      this.$store.dispatch('application/getEndDateOfLastTimsheet').then((res) => {
        if (!res) {
          this.$message.error(res.message, 5);
        }
      }).catch(error => {
        this.$message.error(error.message, 5);
      });
    },

    disableWeekends1(date) {
      if (new Date().getDate() < this.lastTimesheet.forbidden_request_date) {
        if (new Date(this.lastTimesheet.last_timesheet_period_end_date).getTime() >= new Date(date.format('YYYY-MM-DD')).getTime()) {
          return true
        }
      } else if (new Date().getDate() === this.lastTimesheet.forbidden_request_date) {
        if (new Date().getHours() >= this.lastTimesheet.forbidden_request_time_in_hour) {
          if (new Date(this.lastTimesheet.last_timesheet_period_end_date).getTime() >= new Date(date.format('YYYY-MM-DD')).getTime()) {
            return true
          }
        }
      }
      const day = new Date(date).getDay()
      return day === 0 || day === 6
    },
    disableWeekends2(date) {
      if (new Date().getDate() < this.lastTimesheet.forbidden_request_date) {
        if (new Date(this.lastTimesheet.last_timesheet_period_end_date).getTime() >= new Date(date.format('YYYY-MM-DD')).getTime()) {
          return true
        }
      } else if (new Date().getDate() === this.lastTimesheet.forbidden_request_date) {
        if (new Date().getHours() >= this.lastTimesheet.forbidden_request_time_in_hour) {
          if (new Date(this.lastTimesheet.last_timesheet_period_end_date).getTime() >= new Date(date.format('YYYY-MM-DD')).getTime()) {
            return true
          }
        }
      }
      const day = new Date(date).getDay()
      const dayStart = new Date(this.formData.start_date).getTime()
      const dayLoad = new Date(moment(date).format('YYYY-MM-DD')).getTime();

      return day === 0 || day === 6 || dayLoad < dayStart
    },
    beforeUpload(file) {
      this.fileList = [...this.fileList, file];
      return false;
    },
    handleRemove(file) {
      const index = this.fileList.indexOf(file);
      this.deleteFile(this.formData.id, file.name);
      const newFileList = this.fileList.slice();
      newFileList.splice(index, 1);
      this.fileList = newFileList;
    },
    handleUpload(id) {
      const { fileList } = this;
      const formData = new FormData();
      fileList.forEach(file => {
        formData.append('file', file);
      });
      http.post(`/tms/request-management/${this.request_type}/${id}/upload?file`, formData, {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      })
        .then((res) => {
          this.$message.success("Success");
          fileList.splice(0, fileList.length)
          return res.data;
        })
        .catch((err) => {
          throw err
        })
    },
    getAllListFile(id) {
      const { fileList } = this;
      http.get(`/tms/request-management/${this.request_type}/get-all-file-list?id=` + id,)
        .then((res) => {
          if(res.data) {
            let tempData = res.data
            tempData.forEach(record => {
              fileList.push(record)
            })
            if (fileList.length > 0) {
              this.isAvaiableToDownLoad = true
            }
            else {
              this.isAvaiableToDownLoad = false
            }
            return tempData
          }
        })
        .catch((err) => {
          throw err
        })
    },
    deleteFile(id, name) {
      http.get(`/tms/request-management/${this.request_type}/${id}/delete-file?name=` + name)
        .then(res => {
          this.$message.success(res.data)
          if (this.fileList.length > 0) {
            this.isAvaiableToDownLoad = true
          }
          else {
            this.isAvaiableToDownLoad = false
          }
          return res.data
        })
        .catch(err => {
          throw err
        })
    },
    handleDownload() {
      const { fileList } = this
      fileList.forEach(file => {
        http.get(`/tms/request-management/${this.request_type}/${this.formData.id}/download?name=` + file.name, { responseType: 'blob' })
          .then(res => {
            downloadFile(res.data, file.name)
            return res.data;
          })
          .catch(err => {
            throw err
          })
      })
    }
  },
  props: {
    msg: String,
  },

};
