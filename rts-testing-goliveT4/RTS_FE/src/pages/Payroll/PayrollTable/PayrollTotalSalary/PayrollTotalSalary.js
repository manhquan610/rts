const mockData = [
    {
       key: 1,
       name:'Đỗ Quang Long',
       code: 'CMC00532',
       user_name: 'dqlong',
       role: 'SE01',
       department: 'CMC Global DU3',
       tong_luong_chinh_thuc: '12000000',
       tong_luong_thu_viec: '8500000',
       danh_gia_cong_viec: '96.00',
       he_so_tinh_thuong: '1.00',
       so_cong_chinh_thuc: '0.00',
       so_cong_thu_viec: '15.00',
       tong_luong_va_thuong_danh_gia: '5871429',
       luong_va_thuong_lam_them_gio: '4796160',
       ho_tro_khac: '',
       tong_thu_nhap: '10867590',
       thu_nhap_chiu_thue: '9855901',
       giam_tru_khac: '',
       cac_khoan_giam_tru: '2398323',
       cac_khoan_truy_hoan: '273810',
       cac_khoan_truy_thu: '1621429',
       thuc_linh_trong_ky: '8269266' ,
       isEdit: false
    },
    {
        key: 2,
        name:'Nguyễn Trần Tuấn Anh',
        code: 'CMC00536',
        user_name: 'nttanh',
        role: 'SE01',
        department: 'CMC Global DU3',
        tong_luong_chinh_thuc: '12000000',
        tong_luong_thu_viec: '8500000',
        danh_gia_cong_viec: '96.00',
        he_so_tinh_thuong: '1.00',
        so_cong_chinh_thuc: '0.00',
        so_cong_thu_viec: '15.00',
        tong_luong_va_thuong_danh_gia: '5871429',
        luong_va_thuong_lam_them_gio: '',
        ho_tro_khac: '',
        tong_thu_nhap: '10867590',
        thu_nhap_chiu_thue: '9855901',
        giam_tru_khac: '',
        cac_khoan_giam_tru: '2398323',
        cac_khoan_truy_hoan: '',
        cac_khoan_truy_thu: '',
        thuc_linh_trong_ky: '8269266' ,
        isEdit: false
     },
     {
        key: 3,
        name:'Đỗ Nhật Minh',
        code: 'CMC00534',
        user_name: 'dnminh',
        role: 'SE01',
        department: 'CMC Global DU3',
        tong_luong_chinh_thuc: '12000000',
        tong_luong_thu_viec: '8500000',
        danh_gia_cong_viec: '96.00',
        he_so_tinh_thuong: '1.00',
        so_cong_chinh_thuc: '0.00',
        so_cong_thu_viec: '15.00',
        tong_luong_va_thuong_danh_gia: '5871429',
        luong_va_thuong_lam_them_gio: '4796160',
        ho_tro_khac: '',
        tong_thu_nhap: '10867590',
        thu_nhap_chiu_thue: '9855901',
        giam_tru_khac: '',
        cac_khoan_giam_tru: '2398323',
        cac_khoan_truy_hoan: '273810',
        cac_khoan_truy_thu: '1621429',
        thuc_linh_trong_ky: '8269266' ,
        isEdit: false,
     },
];
const dataTotalMock = {
    tong_luong_chinh_thuc: 0,
    tong_luong_thu_viec: 0,
    tong_luong_va_thuong_danh_gia: 0,
    luong_va_thuong_lam_them_gio: 0,
    ho_tro_khac: 0,
    tong_thu_nhap: 0,
    thu_nhap_chiu_thue: 0,
    giam_tru_khac: 0,
    cac_khoan_giam_tru: 0,
    cac_khoan_truy_hoan: 0,
    cac_khoan_truy_thu: 0,
    thuc_linh_trong_ky: 0 
};
export default {
    name: 'payroll-total-salary',
    data() {
        return {
            mockData,
            columns: [],
            dataTotalMock,
            currentPage: 1,
            pageSize: 10,
            total_items: 0,
            clickButtonEdit: false,
            isEdit: false,
            editHoTro: '',
            editGiamTru: '',
               
        }
    },
    created() {
        
    },
    watch: {
        
    },
    mounted() {
        this.renderColumn()
    },
    methods: {
        renderColumn() {
            let columns = [
                {
                    title: 'Staff',
                    dataIndex: 'staff',
                    key: 'staff',
                    fixed: 'left',
                    scopedSlots: { customRender: 'staff' },
                    children: [
                        {
                            title:'TOTAL',
                            width: 185,
                            dataIndex: 'staff',
                            key: 'staff',
                            class: 'cursor-pointer ',
                            scopedSlots: { customRender: 'staff' },
                        }
                    ],
                },
                {
                    title: 'Account',
                    dataIndex: 'account',
                    key: 'account',
                    fixed: 'left',
                    align:'center',
                    scopedSlots: { customRender: 'account' },
                    children: [
                        {
                            title:'',
                            width: 110,
                            dataIndex: 'account',
                            key: 'account',
                            class: 'cursor-pointer',
                            align:'center',
                            scopedSlots: { customRender: 'account' },
                        }
                    ],
                    
                },
                {
                    title: 'Role/Department',
                    dataIndex: 'role_department',
                    key: 'role_department',
                    fixed: 'left',
                    align:'center',
                    scopedSlots: { customRender: 'role_department' },
                    children: [
                        {
                            title:'',
                            width: 160,
                            dataIndex: 'role_department',
                            key: 'role_department',
                            class: 'cursor-pointer',
                            align:'center',
                            scopedSlots: { customRender: 'role_department' },
                        }
                    ],
                },
                {
                    title: 'Tổng lương chính thức',
                    dataIndex: 'tong_luong_chinh_thuc',
                    key: '1',
                    align:'center',
                    scopedSlots: { customRender: 'tong_luong_chinh_thuc' },
                    children: [
                        {
                            title: `${this.getTotal().tong_luong_chinh_thuc}`,
                            dataIndex: 'tong_luong_chinh_thuc',
                            key: 'tong_luong_chinh_thuc',
                            scopedSlots: { customRender: 'tong_luong_chinh_thuc'},
                            width:'150px',
                            align: 'center'
                        }
                    ],
                },
                {
                    title: 'Tổng lương thử việc',
                    dataIndex: 'tong_luong_thu_viec',
                    key: '2',
                    align:'center',
                    scopedSlots: { customRender: 'tong_luong_thu_viec' },
                    children: [
                        {
                            title: `${this.getTotal().tong_luong_thu_viec}`,
                            dataIndex: 'tong_luong_thu_viec',
                            key: 'tong_luong_thu_viec',
                            scopedSlots: { customRender: 'tong_luong_thu_viec'},
                            width:'150px',
                            align: 'center'
                        }
                    ],
                },
                {
                    title: 'Đánh giá công việc',
                    dataIndex: 'danh_gia_cong_viec',
                    key: '3',
                    align:'center',
                    scopedSlots: { customRender: 'danh_gia_cong_viec' },
                    children: [
                        {
                            title: '',
                            dataIndex: 'danh_gia_cong_viec',
                            key: 'danh_gia_cong_viec',
                            scopedSlots: { customRender: 'danh_gia_cong_viec'},
                            width:'150px',
                            align: 'center'
                        }
                    ],
                },
                {
                    title: 'Hệ số tính thưởng',
                    dataIndex: 'he_so_tinh_thuong',
                    key: '4',
                    align:'center',
                    scopedSlots: { customRender: 'he_so_tinh_thuong' },
                    children: [
                        {
                            title: '',
                            dataIndex: 'he_so_tinh_thuong',
                            key: 'he_so_tinh_thuong',
                            scopedSlots: { customRender: 'he_so_tinh_thuong'},
                            width:'150px',
                            align: 'center'
                        }
                    ],
                },
                {
                    title: 'Số công chính thức',
                    dataIndex: 'so_cong_chinh_thuc',
                    key: '5',
                    align:'center',
                    scopedSlots: { customRender: 'so_cong_chinh_thuc' },
                    children: [
                        {
                            title: '',
                            dataIndex: 'so_cong_chinh_thuc',
                            key: 'so_cong_chinh_thuc',
                            scopedSlots: { customRender: 'so_cong_chinh_thuc'},
                            width:'150px',
                            align: 'center'
                        }
                    ],
                },
                {
                    title: 'Số công thử việc',
                    dataIndex: 'so_cong_thu_viec',
                    key: '6',
                    align:'center',
                    scopedSlots: { customRender: 'so_cong_thu_viec' },
                    children: [
                        {
                            title: '',
                            dataIndex: 'so_cong_thu_viec',
                            key: 'so_cong_thu_viec',
                            scopedSlots: { customRender: 'so_cong_thu_viec'},
                            width:'150px',
                            align: 'center'
                        }
                    ],
                },
                {
                    title: 'Tổng lương và thưởng đánh giá',
                    dataIndex: 'tong_luong_va_thuong_danh_gia',
                    key: '7',
                    align:'center',
                    scopedSlots: { customRender: 'tong_luong_va_thuong_danh_gia' },
                    children: [
                        {
                            title: `${this.getTotal().tong_luong_va_thuong_danh_gia}`,
                            dataIndex: 'tong_luong_va_thuong_danh_gia',
                            key: 'tong_luong_va_thuong_danh_gia',
                            scopedSlots: { customRender: 'tong_luong_va_thuong_danh_gia'},
                            width:'200px',
                            align: 'center'
                        }
                    ],
                },
                {
                    title: 'Lương và thưởng làm thêm giờ',
                    dataIndex: 'luong_va_thuong_lam_them_gio',
                    key: '8',
                    align:'center',
                    scopedSlots: { customRender: 'luong_va_thuong_lam_them_gio' },
                    children: [
                        {
                            title: `${this.getTotal().luong_va_thuong_lam_them_gio}`,
                            dataIndex: 'luong_va_thuong_lam_them_gio',
                            key: 'luong_va_thuong_lam_them_gio',
                            scopedSlots: { customRender: 'luong_va_thuong_lam_them_gio'},
                            width:'200px',
                            align: 'center'
                        }
                    ],
                },
                {
                    title: 'Hỗ trợ khác',
                    dataIndex: 'ho_tro_khac',
                    key: '9',
                    align:'center',
                    scopedSlots: { customRender: 'ho_tro_khac' },
                    children: [
                        {
                            title: `${this.getTotal().ho_tro_khac}`,
                            dataIndex: 'ho_tro_khac',
                            key: 'ho_tro_khac',
                            scopedSlots: { customRender: 'ho_tro_khac'},
                            width:'150px',
                            align: 'center'
                        }
                    ],
                },
                {
                    title: 'Tổng thu nhập',
                    dataIndex: 'tong_thu_nhap',
                    key: '10',
                    align:'center',
                    scopedSlots: { customRender: 'tong_thu_nhap' },
                    children: [
                        {
                            title: `${this.getTotal().tong_thu_nhap}`,
                            dataIndex: 'tong_thu_nhap',
                            key: 'tong_thu_nhap',
                            scopedSlots: { customRender: 'tong_thu_nhap'},
                            width:'150px',
                            align: 'center'
                        }
                    ],
                },
                {
                    title: 'Thu nhập chịu thuế',
                    dataIndex: 'thu_nhap_chiu_thue',
                    key: '11',
                    align:'center',
                    scopedSlots: { customRender: 'thu_nhap_chiu_thue' },
                    children: [
                        {
                            title: `${this.getTotal().thu_nhap_chiu_thue}`,
                            dataIndex: 'thu_nhap_chiu_thue',
                            key: 'thu_nhap_chiu_thue',
                            scopedSlots: { customRender: 'thu_nhap_chiu_thue'},
                            width:'150px',
                            align: 'center'
                        }
                    ],
                },
                {
                    title: 'Giảm trừ khác',
                    dataIndex: 'giam_tru_khac',
                    key: '12',
                    align:'center',
                    scopedSlots: { customRender: 'giam_tru_khac' },
                    children: [
                        {
                            title: `${this.getTotal().giam_tru_khac}`,
                            dataIndex: 'giam_tru_khac',
                            key: 'giam_tru_khac',
                            scopedSlots: { customRender: 'giam_tru_khac'},
                            width:'150px',
                            align: 'center'
                        }
                    ],
                },
                {
                    title: 'Các khoản giảm trừ',
                    dataIndex: 'cac_khoan_giam_tru',
                    key: '13',
                    align:'center',
                    scopedSlots: { customRender: 'cac_khoan_giam_tru' },
                    children: [
                        {
                            title: `${this.getTotal().cac_khoan_giam_tru}`,
                            dataIndex: 'cac_khoan_giam_tru',
                            key: 'cac_khoan_giam_tru',
                            scopedSlots: { customRender: 'cac_khoan_giam_tru'},
                            width:'150px',
                            align: 'center'
                        }
                    ],
                },
                {
                    title: 'Các khoản truy hoàn',
                    dataIndex: 'cac_khoan_truy_hoan',
                    key: '14',
                    align:'center',
                    scopedSlots: { customRender: 'cac_khoan_truy_hoan' },
                    children: [
                        {
                            title: `${this.getTotal().cac_khoan_truy_hoan}`,
                            dataIndex: 'cac_khoan_truy_hoan',
                            key: 'cac_khoan_truy_hoan',
                            scopedSlots: { customRender: 'cac_khoan_truy_hoan'},
                            width:'150px',
                            align: 'center'
                        }
                    ],
                },
                {
                    title: 'Các khoản truy thu',
                    dataIndex: 'cac_khoan_truy_thu',
                    key: '15',
                    align:'center',
                    scopedSlots: { customRender: 'cac_khoan_truy_thu' },
                    children: [
                        {
                            title: `${this.getTotal().cac_khoan_truy_thu}`,
                            dataIndex: 'cac_khoan_truy_thu',
                            key: 'cac_khoan_truy_thu',
                            scopedSlots: { customRender: 'cac_khoan_truy_thu'},
                            width:'150px',
                            align: 'center'
                        }
                    ],
                },
                {
                    title: 'Thực lĩnh trong kỳ',
                    dataIndex: 'thuc_linh_trong_ky',
                    key: '16',
                    align:'center',
                    scopedSlots: { customRender: 'thuc_linh_trong_ky' },
                    children: [
                        {
                            title: `${this.getTotal().thuc_linh_trong_ky}`,
                            dataIndex: 'thuc_linh_trong_ky',
                            key: 'thuc_linh_trong_ky',
                            scopedSlots: { customRender: 'thuc_linh_trong_ky'},
                            width:'150px',
                            align: 'center'
                        }
                    ],
                },
                {
                    title: ' ',
                    dataIndex: 'edit',
                    fixed: 'right',
                    key: 'edit',
                    children: [
                        {
                            title:'',
                            dataIndex: 'edit',
                            width:'80px',
                            key: 'edit',
                            align:'center',
                            scopedSlots: { customRender: 'edit' },
                        }
                    ],
                },
            ];
            this.columns = columns
            return columns
        },
        customRow() {
            return {
                on: {
                    click: () => {
                        
                    },
                },
            };
        },
        handleOnClickEdit(){     
        },
        handCancelEdit(record) {
            record.isEdit = false
            this.editHoTro = '',
            this.editGiamTru = ''
        },
        showInput(record){
            record.isEdit = true
        },
        handleChangeHoTro(e){
            this.editHoTro = e.target.value

        },
        handleChangeGiamTru(e) {
            this.editGiamTru = e.target.value
        },
        handleSave() {
          
        },
        onChangePage(page, size) {
            this.currentPage = page;
            this.pageSize = size;
            // this.getAbnormalCaseData();
        },
        onShowSizeChange(page, size) {
            this.currentPage = page;
            this.pageSize = size;
            // this.getAbnormalCaseData();
        },
        formatNumber(number) {
            return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
        },
        getTotal() {
            const {dataTotalMock,mockData} = this
            let total = {
                tong_luong_chinh_thuc: 0,
                tong_luong_thu_viec: 0,
                tong_luong_va_thuong_danh_gia: 0,
                luong_va_thuong_lam_them_gio: 0,
                ho_tro_khac: 0,
                tong_thu_nhap: 0,
                thu_nhap_chiu_thue: 0,
                giam_tru_khac: 0,
                cac_khoan_giam_tru: 0,
                cac_khoan_truy_hoan: 0,
                cac_khoan_truy_thu: 0,
                thuc_linh_trong_ky: 0 
            }
            mockData.forEach(element => {
                total.tong_luong_chinh_thuc += Number(element.tong_luong_chinh_thuc)
                total.tong_luong_thu_viec += Number(element.tong_luong_thu_viec)
                total.tong_luong_va_thuong_danh_gia += Number(element.tong_luong_va_thuong_danh_gia)
                total.luong_va_thuong_lam_them_gio += Number(element.luong_va_thuong_lam_them_gio)
                total.tong_thu_nhap += Number(element.tong_thu_nhap)
                total.thu_nhap_chiu_thue += Number(element.thu_nhap_chiu_thue)
                total.cac_khoan_giam_tru += Number(element.cac_khoan_giam_tru)
                total.cac_khoan_truy_hoan += Number(element.cac_khoan_truy_hoan)
                total.cac_khoan_truy_thu += Number(element.cac_khoan_truy_thu)
                total.thuc_linh_trong_ky += Number(element.thuc_linh_trong_ky)
                if (element.ho_tro_khac){
                    total.ho_tro_khac += Number(element.ho_tro_khac)
                } else {
                    total.ho_tro_khac = '--'
                }
                if (element.giam_tru_khac){
                    total.giam_tru_khac += Number(element.giam_tru_khac)
                } else {
                    total.giam_tru_khac = '--'
                }
                
            })
            dataTotalMock.tong_luong_chinh_thuc = this.formatNumber(total.tong_luong_chinh_thuc)
            dataTotalMock.tong_luong_thu_viec = this.formatNumber(total.tong_luong_thu_viec)
            dataTotalMock.tong_luong_va_thuong_danh_gia = this.formatNumber(total.tong_luong_va_thuong_danh_gia)
            dataTotalMock.luong_va_thuong_lam_them_gio = this.formatNumber(total.luong_va_thuong_lam_them_gio)
            dataTotalMock.tong_thu_nhap = this.formatNumber(total.tong_thu_nhap)
            dataTotalMock.thu_nhap_chiu_thue = this.formatNumber(total.thu_nhap_chiu_thue)
            dataTotalMock.cac_khoan_giam_tru = this.formatNumber(total.cac_khoan_giam_tru)
            dataTotalMock.cac_khoan_truy_hoan = this.formatNumber(total.cac_khoan_truy_hoan)
            dataTotalMock.cac_khoan_truy_thu = this.formatNumber(total.cac_khoan_truy_thu)
            dataTotalMock.thuc_linh_trong_ky = this.formatNumber(total.thuc_linh_trong_ky)
            dataTotalMock.ho_tro_khac = this.formatNumber(total.ho_tro_khac)
            dataTotalMock.giam_tru_khac = this.formatNumber(total.giam_tru_khac)
            return dataTotalMock
        }
    },
}