const columns = [
    {
        dataIndex: "display_name",
        key: "display_name",
        scopedSlots: { customRender: "display_name" },
        width: '18.5%'
    },
    {
        dataIndex: "account",
        key: "acount",
        scopedSlots: { customRender: "account" },
        width: '8.5%',
    },
    {
        dataIndex: "role",
        key: "role",
        scopedSlots: { customRender: "role" },
        width: '8%',
    },
    {
        dataIndex: "department",
        key: "department",
        scopedSlots: { customRender: "department" },
        width: '12%'
    },
    {
        dataIndex: "payroll_period",
        key: "payroll_period",
        scopedSlots: { customRender: "payroll_period" },
        width: '13%'
    },
    {
        dataIndex: "result",
        key: "result",
        scopedSlots: { customRender: "result" },
        width: '9%'
    },
    {
        dataIndex: "ty_le_tinh_luong",
        key: "ty_le_tinh_luong",
        scopedSlots: { customRender: "ty_le_tinh_luong" },
        width: '10.5%'
    },
    {
        dataIndex: "muc_thuong_danh_gia_thang",
        key: "muc_thuong_danh_gia_thang",
        scopedSlots: { customRender: "muc_thuong_danh_gia_thang" },
        width: '12.5%'
    },
    {
        dataIndex: "muc_thuong_danh_gia_thuc_te",
        key: "muc_thuong_danh_gia_thuc_te",
        scopedSlots: { customRender: "muc_thuong_danh_gia_thuc_te" },
        width: '15%'
    },
];
const mockData = [
    {
        key: 1,
        user_name: 'nphthien',
        name: 'Nguyễn Phan Hải Thiên',
        code: 'CMC003482',
        role: 'SE01',
        department: 'CMC Global DU3',
        payroll_period: 'CMCGLOBAL_2020005',
        result: '98',
        ty_le_tinh_luong: '1',
        muc_thuong_danh_gia_thang: '13500000',
        muc_thuong_danh_gia_thuc_te: '13500000'
    },
    {
        key: 2,
        user_name: 'ndanh',
        name: 'Nguyễn Duy Anh',
        code: 'CMC003467',
        role: 'DEV01',
        department: 'CMC Global DU8',
        payroll_period: 'CMCGLOBAL_2020005',
        result: '92',
        ty_le_tinh_luong: '1',
        muc_thuong_danh_gia_thang: '9500000',
        muc_thuong_danh_gia_thuc_te: '9500000'
    }
];
export default {
    name: 'payroll-bonus-in-month',
    data() {
        return {
            columns,
            mockData,
            currentPage: 1,
            pageSize: 10,
            total_items: 0,
            totalBonus: {},
        }
    },
    created() {
        this.getTotalBonus()
    },
    methods: {
        onChangePage(page, size) {
            this.currentPage = page;
            this.pageSize = size;
            // this.getAbnormalCaseData();
        },
        onShowSizeChange(page, size) {
            this.currentPage = page;
            this.pageSize = size;
            // this.getAbnormalCaseData();
        },
        formatNumber(number) {
            return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
        },
        getTotalBonus(){
            let tempTotal = {
                muc_thuong_danh_gia_thang: 0,
                muc_thuong_danh_gia_thuc_te: 0
            }
            this.mockData.forEach(element => {
                tempTotal.muc_thuong_danh_gia_thang += Number(element.muc_thuong_danh_gia_thang)
                tempTotal.muc_thuong_danh_gia_thuc_te += Number(element.muc_thuong_danh_gia_thuc_te)
            })
            this.totalBonus.muc_thuong_danh_gia_thang = this.formatNumber(tempTotal.muc_thuong_danh_gia_thang)
            this.totalBonus.muc_thuong_danh_gia_thuc_te = this.formatNumber(tempTotal.muc_thuong_danh_gia_thuc_te)

            return this.totalBonus
        }
    },
}