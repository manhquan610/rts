const columns = [
    {
        dataIndex: "collaborator",
        key: "collaborator",
        scopedSlots: { customRender: "collaborator" },
        width: '24.25%'
    },
    {
        dataIndex: "role_department",
        key: "role_department",
        scopedSlots: { customRender: "role_department" },
        width: '16%'
    },
    {
        dataIndex: "ky_tinh_cong",
        key: "ky_tinh_cong",
        scopedSlots: { customRender: "ky_tinh_cong" },
        width: '15.75%'
    },
    {
        dataIndex: "money",
        key: "money",
        scopedSlots: { customRender: "money" },
        width: '15%'
    },
    {
        dataIndex: "acc_number",
        key: "acc_number",
        scopedSlots: { customRender: "acc_number" },
        width: '10%'
    },
    {
        dataIndex: "note",
        key: "note",
        scopedSlots: { customRender: "note" },
        width: '19%',
        align: 'center'
    },
];
const mockData = [
    {
        key: 1,
        name: 'Ngô Mỹ Anh',
        user_name: 'nmanh',
        code: 'CMC002507',
        role: 'SE01',
        department: 'CMC Global DU3',
        ky_tinh_cong: 'Tháng 1/2030',
        money: '8269266',
        acc_number: '12010007304691',
        note: '',
    },
    {
        key: 2,
        name: 'Ngô Thu Hà',
        user_name: 'ntha1',
        code: 'CMC000810',
        role: 'SE01',
        department: 'CMC Global DU3',
        ky_tinh_cong: 'Tháng 1/2030',
        money: '8269266',
        acc_number: '12010007304691',
        note: '',
    }
];
export default {
    name: 'payroll-paying-cash',
    data() {
        return {
            columns,
            mockData,
            currentPage: 1,
            pageSize: 10,
            total_items: 0,
            totalMoney: 0,
        }
    },
    created() {
        this.getTotalMoney()
    },
    methods: {
        onChangePage(page, size) {
            this.currentPage = page;
            this.pageSize = size;
            // this.getAbnormalCaseData();
        },
        onShowSizeChange(page, size) {
            this.currentPage = page;
            this.pageSize = size;
            // this.getAbnormalCaseData();
        },
        getTotalMoney() {
            let total = 0
            this.mockData.forEach(data => {
                total += Number(data.money)
            });
            this.totalMoney = this.formatNumber(total)
            return this.totalMoney
        },
        formatNumber(number) {
            return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
        },
    },
}