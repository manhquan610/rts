import PayrollOvertimeDetail from "./PayrollOvertimeDetail/PayrollOvertimeDetail.vue";
const columns = [
    {
        dataIndex: "display_name",
        key: "display_name",
        scopedSlots: { customRender: "display_name" },
        width: '13%'
    },
    {
        dataIndex: "user_name",
        key: "user_name",
        scopedSlots: { customRender: "user_name" },
        width: '4.5%'
    },
    {
        dataIndex: "role_department",
        key: "role_department",
        scopedSlots: { customRender: "role_department" },
        width: '9.5%'
    },
    {
        dataIndex: "project",
        key: "project",
        scopedSlots: { customRender: "project" },
        width: '7.5%'
    },
    {
        dataIndex: "project_code",
        key: "project_code",
        scopedSlots: { customRender: "project_code" },
        width: '6.5%'
    },
    {
        dataIndex: "fixed_income",
        key: "fixed_income",
        scopedSlots: { customRender: "fixed_income" },
        width: '6.5%'
    },
    {
        dataIndex: "probationary_salary",
        key: "probationary_salary",
        scopedSlots: { customRender: "probationary_salary" },
        width: '5.5%'
    },
    {
        dataIndex: "working_days",
        key: "working_days",
        scopedSlots: { customRender: "working_days" },
        width: '3.5%'
    },
    {
        dataIndex: "working_hours",
        key: "working_hours",
        scopedSlots: { customRender: "working_hours" },
        width: '4%'
    },
    {
        dataIndex: "tien_cong_gio_chinh_thuc",
        key: "tien_cong_gio_chinh_thuc",
        scopedSlots: { customRender: "tien_cong_gio_chinh_thuc" },
        width: '5.5%'
    },
    {
        dataIndex: "probationary_salary_one_hour",
        key: "probationary_salary_one_hour",
        scopedSlots: { customRender: "probationary_salary_one_hour" },
        width: '6.5%'
    },
    {
        dataIndex: "total_salary_overtime",
        key: "total_salary_overtime",
        scopedSlots: { customRender: "total_salary_overtime" },
        width: '5.5%'
    },
    {
        dataIndex: "tong_cong_ho_tro",
        key: "tong_cong_ho_tro",
        scopedSlots: { customRender: "tong_cong_ho_tro" },
        width: '5%'
    },
    {
        dataIndex: "luong_vuot_qua",
        key: "luong_vuot_qua",
        scopedSlots: { customRender: "luong_vuot_qua" },
        width: '4.5%'
    },
    {
        dataIndex: "kpi_bonus",
        key: "kpi_bonus",
        scopedSlots: { customRender: "kpi_bonus" },
        width: '3%'
    },
    {
        dataIndex: "tax_exempt_incomes",
        key: "tax_exempt_incomes",
        scopedSlots: { customRender: "tax_exempt_incomes" },
        width: '5%'
    },
    {
        dataIndex: "tong_luong_ho_tro_ot",
        key: "tong_luong_ho_tro_ot",
        scopedSlots: { customRender: "tong_luong_ho_tro_ot" },
        width: '4.5%'
    },
];
const mockData =[
    {
        key: 1,
        name: 'Trần Linh Nhâm',
        code: '00532',
        user_name: 'tlnham',
        role: 'SE01',
        department: 'CMC Global DU3',
        project: {
            project_name: 'OSTech-ODC',
            project_code: 'GLB172034'
        },
        fixed_income: '12000000',
        probationary_salary: '8500000',
        working_days:'21',
        working_hours:'184.8',
        tien_cong_gio_chinh_thuc:'128247',
        probationary_salary_one_hour:'128246.75',
        total_salary_overtime:'2435.065',
        tong_cong_ho_tro:'1948.052',
        luong_vuot_qua:'405844',
        kpi_bonus:'',
        tax_exempt_incomes:'811688',
        tong_luong_ho_tro_ot:'4788961',

    },
    {
        key: 2,
        name: 'Nguyễn Trần Tuấn Anh',
        code: '00532',
        user_name: 'nttanh3',
        role: 'SE01',
        department: 'CMC Global DU3',
        project: {
            project_name: 'OSTech-ODC',
            project_code: 'GLB172034'
        },
        fixed_income: '12000000',
        probationary_salary: '8500000',
        working_days:'21',
        working_hours:'184.8',
        tien_cong_gio_chinh_thuc:'128247',
        probationary_salary_one_hour:'128246.75',
        total_salary_overtime:'2435.065',
        tong_cong_ho_tro:'1948.052',
        luong_vuot_qua:'405844',
        kpi_bonus:'',
        tax_exempt_incomes:'811688',
        tong_luong_ho_tro_ot:'4788961',
    }
];
export default {
    name: 'payroll-overtime',
    components: {PayrollOvertimeDetail},
    props: {
        timesheetUser: {
            type: Object
        },
        timesheetInfo: {
            type: Object
        },
        formatDate: {
            type: Function,
        },
    },
    data() {
        return {
            columns,
            mockData,
            currentPage: 1,
            pageSize: 10,
            total_items: 0,
            total: {},
            visibleDrawer: false,
            recordDetail: {},
        }
    },
    created() {
        this.getTotal()
    },
    methods: {
        customRow(record) {
            return {
                on: {
                    click: () => {
                        this.recordDetail = record
                        this.visibleDrawer = true
                    },
                },
            };
        },
        onChangePage(page, size) {
            this.currentPage = page;
            this.pageSize = size;
            // this.getAbnormalCaseData();
        },
        onShowSizeChange(page, size) {
            this.currentPage = page;
            this.pageSize = size;
            // this.getAbnormalCaseData();
        },
        onClose() {
            this.visibleDrawer = false
        },
        formatNumber(number) {
            return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
        },
        getTotal(){
            const {mockData, total} = this
            let tempTotal = {
                fixed_income: 0,
                probationary_salary: 0,
                total_salary_overtime: 0,
                tong_cong_ho_tro: 0,
                luong_vuot_qua: 0,
                kpi_bonus: null,
                tax_exempt_incomes: 0,
                tong_luong_ho_tro_ot: 0,
            }
            mockData.forEach(element => {
                tempTotal.fixed_income += Number(element.fixed_income)
                tempTotal.probationary_salary += Number(element.probationary_salary)
                tempTotal.total_salary_overtime += Number(element.total_salary_overtime)
                tempTotal.tong_cong_ho_tro += Number(element.tong_cong_ho_tro)
                tempTotal.luong_vuot_qua += Number(element.luong_vuot_qua)
                tempTotal.tax_exempt_incomes += Number(element.tax_exempt_incomes)
                tempTotal.tong_luong_ho_tro_ot += Number(element.tong_luong_ho_tro_ot)
                if(element.kpi_bonus){
                    tempTotal.kpi_bonus += Number(element.kpi_bonus)
                }else {
                    tempTotal.kpi_bonus = '--'
                }
            });
            total.fixed_income = this.formatNumber(tempTotal.fixed_income)
            total.probationary_salary = this.formatNumber(tempTotal.probationary_salary)
            total.total_salary_overtime = this.formatNumber(tempTotal.total_salary_overtime)
            total.tong_cong_ho_tro = this.formatNumber(tempTotal.tong_cong_ho_tro)
            total.luong_vuot_qua = this.formatNumber(tempTotal.luong_vuot_qua)
            total.kpi_bonus = tempTotal.kpi_bonus
            total.tax_exempt_incomes = this.formatNumber(tempTotal.tax_exempt_incomes)
            total.tong_luong_ho_tro_ot = this.formatNumber(tempTotal.tong_luong_ho_tro_ot)
            return total
        }
    },
}