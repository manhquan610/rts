import {convertAvatarName} from "../../../../../components/commons/commons"
export default {
    name: 'payroll-overtime-detail',
    props: {
        timesheetUser: {
            type: Object
        },
        timesheetInfo: {
            type: Object
        },
        data: { type: Object},
        formatDate: {
            type: Function,
        },
    },
    data() {
        return {
            detailOvertimeA: [
                {
                    title: 'A: Thu nhập làm căn cứ tính làm thêm giờ',
					description: 'thu_nhap_lam_can_cu',
					key: 'thu_nhap_lam_can_cu',
                },
                {
                    title: 'Số ngày công theo quy định',
					description: 'so_ngay_cong_theo_quy_dinh',
					key: 'so_ngay_cong_theo_quy_dinh',
                },
                {
                    title: 'Số giờ công theo quy định',
					description: 'so_gio_cong_theo_quy_dinh',
					key: 'so_gio_cong_theo_quy_dinh',
                },
                {
                    title: 'Lương cơ bản chính thức',
					description: 'luong_co_ban_chinh_thuc',
					key: 'luong_co_ban_chinh_thuc',
                },
                {
                    title: 'Lương cơ bản thử việc',
					description: 'luong_co_ban_thu_viec',
					key: 'luong_co_ban_thu_viec',
                },
                {
                    title: 'Thưởng đánh giá chính thức',
					description: 'thuong_danh_gia_chinh_thuc',
					key: 'thuong_danh_gia_chinh_thuc',
                },
                {
                    title: 'Thưởng đánh giá thử việc',
					description: 'thuong_danh_gia_thu_viec',
					key: 'thuong_danh_gia_thu_viec',
                },
            ],
            detailOvertimeB: [
                {
                    title: 'B: Tiền lương / giờ',
					description: 'tien_luong_gio',
					key: 'tien_luong_gio',
                },
                {
                    title: 'Lương cơ bản chính thức',
					description: 'luong_co_ban_chinh_thuc',
					key: 'luong_co_ban_chinh_thuc',
                },
                {
                    title: 'Lương cơ bản thử việc',
					description: 'luong_co_ban_thu_viec',
					key: 'luong_co_ban_thu_viec',
                },
                {
                    title: 'Thưởng hiệu suất chính thức',
					description: 'thuong_hieu_suat_chinh_thuc',
					key: 'thuong_hieu_suat_chinh_thuc',
                },
                {
                    title: 'Thưởng hiệu suất thử việc',
					description: 'thuong_hieu_suat_thu_viec',
					key: 'thuong_hieu_suat_thu_viec',
                },
            ],
            detailOvertimeC: [
                {
                    title: 'C: Số giờ làm ngoài giờ (chính thức)',
					description: 'so_gio_lam_ngoai_gio_chinh_thuc',
					key: 'so_gio_lam_ngoai_gio_chinh_thuc',
                },
                {
                    title: 'Ngày thường',
					description: 'ngay_thuong',
					key: 'ngay_thuong',
                },
                {
                    title: 'Đêm ngày thường',
					description: 'dem_ngay_thuong',
					key: 'dem_ngay_thuong',
                },
                {
                    title: 'Ngày nghỉ',
					description: 'ngay_nghi',
					key: 'ngay_nghi',
                },
                {
                    title: 'Đêm ngày nghỉ',
					description: 'dem_ngay_nghi',
					key: 'dem_ngay_nghi',
                },
                {
                    title: 'Ngày lễ,Tết',
					description: 'ngay_le_tet',
					key: 'ngay_le_tet',
                },
                {
                    title: 'Đêm ngày lễ,Tết',
					description: 'dem_ngay_le_tet',
					key: 'dem_ngay_le_tet',
                },
            ],
            detailOvertimeD: [
                {
                    title: 'D: Số giờ làm ngoài giờ (Thử việc)',
					description: 'so_gio_lam_ngoai_gio_thu_viec',
					key: 'so_gio_lam_ngoai_gio_thu_viec',
                },
                {
                    title: 'Ngày thường',
					description: 'ngay_thuong',
					key: 'ngay_thuong',
                },
                {
                    title: 'Đêm ngày thường',
					description: 'dem_ngay_thuong',
					key: 'dem_ngay_thuong',
                },
                {
                    title: 'Ngày nghỉ',
					description: 'ngay_nghi',
					key: 'ngay_nghi',
                },
                {
                    title: 'Đêm ngày nghỉ',
					description: 'dem_ngay_nghi',
					key: 'dem_ngay_nghi',
                },
                {
                    title: 'Ngày lễ,Tết',
					description: 'ngay_le_tet',
					key: 'ngay_le_tet',
                },
                {
                    title: 'Đêm ngày lễ,Tết',
					description: 'dem_ngay_le_tet',
					key: 'dem_ngay_le_tet',
                },
            ],
            detailOvertimeE: [
                {
                    title: 'E: Số giờ làm vượt quá (chính thức)',
					description: 'so_gio_lam_ngoai_gio_chinh_thuc',
					key: 'so_gio_lam_ngoai_gio_chinh_thuc',
                },
                {
                    title: 'Ngày thường',
					description: 'ngay_thuong',
					key: 'ngay_thuong',
                },
                {
                    title: 'Đêm ngày thường',
					description: 'dem_ngay_thuong',
					key: 'dem_ngay_thuong',
                },
                {
                    title: 'Ngày nghỉ',
					description: 'ngay_nghi',
					key: 'ngay_nghi',
                },
                {
                    title: 'Đêm ngày nghỉ',
					description: 'dem_ngay_nghi',
					key: 'dem_ngay_nghi',
                },
                {
                    title: 'Ngày lễ,Tết',
					description: 'ngay_le_tet',
					key: 'ngay_le_tet',
                },
                {
                    title: 'Đêm ngày lễ,Tết',
					description: 'dem_ngay_le_tet',
					key: 'dem_ngay_le_tet',
                },
            ],
            detailOvertimeF: [
                {
                    title: 'F: Số giờ làm vượt quá (Thử việc)',
					description: 'so_gio_lam_vuot_qua_thu_viec',
					key: 'so_gio_lam_vuot_qua_thu_viec',
                },
                {
                    title: 'Ngày thường',
					description: 'ngay_thuong',
					key: 'ngay_thuong',
                },
                {
                    title: 'Đêm ngày thường',
					description: 'dem_ngay_thuong',
					key: 'dem_ngay_thuong',
                },
                {
                    title: 'Ngày nghỉ',
					description: 'ngay_nghi',
					key: 'ngay_nghi',
                },
                {
                    title: 'Đêm ngày nghỉ',
					description: 'dem_ngay_nghi',
					key: 'dem_ngay_nghi',
                },
                {
                    title: 'Ngày lễ,Tết',
					description: 'ngay_le_tet',
					key: 'ngay_le_tet',
                },
                {
                    title: 'Đêm ngày lễ,Tết',
					description: 'dem_ngay_le_tet',
					key: 'dem_ngay_le_tet',
                },
            ],
            detailOvertimeG: [
                {
                    title: 'G: Tiền lương ngoài giờ)',
					description: 'tien_luong_ngoai_gio',
					key: 'tien_luong_ngoai_gio',
                },
                {
                    title: 'Ngày thường',
					description: 'ngay_thuong',
					key: 'ngay_thuong',
                },
                {
                    title: 'Đêm ngày thường',
					description: 'dem_ngay_thuong',
					key: 'dem_ngay_thuong',
                },
                {
                    title: 'Ngày nghỉ',
					description: 'ngay_nghi',
					key: 'ngay_nghi',
                },
                {
                    title: 'Đêm ngày nghỉ',
					description: 'dem_ngay_nghi',
					key: 'dem_ngay_nghi',
                },
                {
                    title: 'Ngày lễ,Tết',
					description: 'ngay_le_tet',
					key: 'ngay_le_tet',
                },
                {
                    title: 'Đêm ngày lễ,Tết',
					description: 'dem_ngay_le_tet',
					key: 'dem_ngay_le_tet',
                },
                {
                    title: 'Tổng',
					description: 'tong',
					key: 'tong',
                },
                {
                    title: 'Hỗ trợ',
					description: 'ho_tro',
					key: 'ho_tro',
                },
            ],
            detailOvertimeH: [
                {
                    title: 'H: Tiền lương ngoài giờ (Thử việc)',
					description: 'tien_luong_ngoai_gio_thu_viec',
					key: 'tien_luong_ngoai_gio_thu_viec',
                },
                {
                    title: 'Ngày thường',
					description: 'ngay_thuong',
					key: 'ngay_thuong',
                },
                {
                    title: 'Đêm ngày thường',
					description: 'dem_ngay_thuong',
					key: 'dem_ngay_thuong',
                },
                {
                    title: 'Ngày nghỉ',
					description: 'ngay_nghi',
					key: 'ngay_nghi',
                },
                {
                    title: 'Đêm ngày nghỉ',
					description: 'dem_ngay_nghi',
					key: 'dem_ngay_nghi',
                },
                {
                    title: 'Ngày lễ,Tết',
					description: 'ngay_le_tet',
					key: 'ngay_le_tet',
                },
                {
                    title: 'Đêm ngày lễ,Tết',
					description: 'dem_ngay_le_tet',
					key: 'dem_ngay_le_tet',
                },
                {
                    title: 'Tổng',
					description: 'tong',
					key: 'tong',
                },
                {
                    title: 'Hỗ trợ',
					description: 'ho_tro',
					key: 'ho_tro',
                },
            ],
        }
    },
    methods: {
        getAvatarName(fullName){
			return convertAvatarName(fullName);
		},
		randomColor() {
			const r = () => Math.floor(256 * Math.random());
			return `rgb(${r()}, ${r()}, ${r()})`;
		}
    },
}