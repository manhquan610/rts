const columns = [
    {
        dataIndex: "display_name",
        key: "display_name",
        scopedSlots: { customRender: "display_name" },
        width: '24%'
    },
    {
        dataIndex: "department",
        key: "department",
        scopedSlots: { customRender: "department" },
        width: '17.25%'
    },
    {
        dataIndex: "unit",
        key: "unit",
        scopedSlots: { customRender: "unit" },
        width: '12.25%'
    },
    {
        dataIndex: "bonus",
        key: "bonus",
        scopedSlots: { customRender: "bonus" },
        width: '12.5%'
    },
    {
        dataIndex: "project_code",
        key: "project_code",
        scopedSlots: { customRender: "project_code" },
        width: '12%'
    },
    {
        dataIndex: "project_name",
        key: "project_name",
        scopedSlots: { customRender: "project_name" },
        width: '13%'
    },
    {
        dataIndex: "note",
        key: "note",
        scopedSlots: { customRender: "note" },
        width: '15%',
    },

];
const mockData = [
    {
        key: 1,
        name: 'Nguyễn Phan Hải Thiên',
        user_name:'nphthien',
        code:'CMC003482',
        department: 'CMC Glaobal DU3',
        unit: 'Tháng',
        bonus: '826,087',
        project_code: 'GLB202015',
        project_name: '',
        note: 'Từ 07/07 - 31/07'
    },
    {
        key: 2,
        name: 'Ngô Duy Anh',
        user_name:'ndanh2',
        code:'CMC003467',
        department: 'CMC Glaobal DU8',
        unit: 'Tháng',
        bonus: '1000000',
        project_code: 'GLB202005',
        project_name: 'TSDV Onsite',
        note:''
    },
    {
        key: 3,
        name: 'Tô Thị Tú Linh',
        user_name:'tttlinh',
        code:'CMC003446',
        department: 'CMC Glaobal DU11',
        unit: 'Tháng',
        bonus: '464646',
        project_code: 'GLB051098',
        project_name: 'TSDV Onsite',
        note: 'dau gauuu'
    },
    
];
export default {
    name: 'payroll-onsite',
    data() {
        return {
            columns,
            mockData,
            totalBonus: 0,
            currentPage: 1,
            pageSize: 10,
            total_items: 0,

        }
    },
    created() {
        this.getTotalBonus()
    },
    methods: {
        getTotalBonus() {
            let total = 0
            this.mockData.forEach(data => {
                total += parseInt(data.bonus)
            });
            this.totalBonus = this.formatNumber(total)
            return this.totalBonus
        },
        formatNumber(number) {
            return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
        },
        handleRowClassName(record) {
            let query = this.$route.query;
            if (query && query.id && query.id === record.id) {
              return 'active-row-payroll-onsite';
            }
            else return '';
        },
        onChangePage(page, size) {
            this.currentPage = page;
            this.pageSize = size;
            // this.getAbnormalCaseData();
        },
        onShowSizeChange(page, size) {
            this.currentPage = page;
            this.pageSize = size;
            // this.getAbnormalCaseData();
        },
    },
}