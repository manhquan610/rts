const dataTotalMock = {
    luong_co_ban_chinh_thuc: 0,
    luong_co_ban_thu_viec: 0,
    tong_luong_theo_ngay_cong: 0,
    thuong_ot_va_ho_tro: 0,
    ho_tro_khac: '',
    tong_thu_nhap: 0,
    thu_nhap_chiu_thue: 0,
    giam_tru_khac: '',
    cac_khoan_giam_tru: 0,
    thuc_linh_trong_ky: 0,
};
const mockData = [
    {
        key: 1,
        name: 'Nguyễn Trần Tuấn Anh',
        code: 'CMC005332',
        user_name: 'nttanh2',
        role: 'SE01',
        department: 'CMC Global DU11',
        luong_co_ban_chinh_thuc: '50000000',
        luong_co_ban_thu_viec: '50000000',
        danh_gia_cong_viec: '96.00',
        he_so_tinh_thuong: '1.00',
        so_cong_chinh_thuc: '0.0',
        so_cong_thu_viec: '15.0',
        tong_luong_theo_ngay_cong: '5000000',
        thuong_ot_va_ho_tro: '4700000',
        ho_tro_khac: '',
        tong_thu_nhap: '9900000',
        thu_nhap_chiu_thue: '9778247',
        giam_tru_khac: '',
        cac_khoan_giam_tru: '932.944',
        thuc_linh_trong_ky: '8967056',
        isEdit: false,
    },
    {
        key: 2,
        name: 'Đỗ Hồng Quân',
        code: 'CMC005332',
        user_name: 'nhquan',
        role: 'SE01',
        department: 'CMC Global DU11',
        luong_co_ban_chinh_thuc: '50000000',
        luong_co_ban_thu_viec: '50000000',
        danh_gia_cong_viec: '96.00',
        he_so_tinh_thuong: '1.00',
        so_cong_chinh_thuc: '0.0',
        so_cong_thu_viec: '15.0',
        tong_luong_theo_ngay_cong: '5000000',
        thuong_ot_va_ho_tro: '',
        ho_tro_khac: '',
        tong_thu_nhap: '9900000',
        thu_nhap_chiu_thue: '9778247',
        giam_tru_khac: '',
        cac_khoan_giam_tru: '932.944',
        thuc_linh_trong_ky: '8967056',
        isEdit: false,
    },
];
import PayrollSalaryInPeriodDetail from './PayrollSalaryInPeriodDetail/PayrollSalaryInPeriodDetail.vue'
import { formatDate} from "../../../../components/commons/commons"
export default {
    name: 'payroll-salary-in-period',
    props: {
        timesheetUser: {
            type: Object
        },
        timesheetInfo: {
            type: Object
        },
    },
    components: { 
        PayrollSalaryInPeriodDetail,
    },
    data() {
        return {
            columns: [],
            mockData,
            dataTotalMock,
            currentPage: 1,
            pageSize: 10,
            total_items: 0,
            clickButtonEdit: true,
            recordDetail: {},
            visibleDrawer: false,
            editHoTro: '',
            editGiamTru: '',
        }
    },
    mounted() {
        this.renderColumn();
    }, 
        
    methods: {
        formatDate,
        renderColumn(){
            let columns = [
                {
                    title: 'Staff',
                    dataIndex: 'staff',
                    key: 'staff',
                    fixed: 'left',
                    scopedSlots: { customRender: 'staff' },
                    children: [
                        {
                            title:'TOTAL',
                            width: 185,
                            dataIndex: 'staff',
                            key: 'staff',
                            class: 'cursor-pointer',
                            scopedSlots: { customRender: 'staff' },
                        }
                    ],
                },
                {
                    title: 'Account',
                    dataIndex: 'account',
                    key: 'account',
                    fixed: 'left',
                    align:'center',
                    scopedSlots: { customRender: 'account' },
                    children: [
                        {
                            title:'',
                            width: 110,
                            dataIndex: 'account',
                            key: 'account',
                            class: 'cursor-pointer',
                            align:'center',
                            scopedSlots: { customRender: 'account' },
                        }
                    ],
                    
                },
                {
                    title: 'Role/Department',
                    dataIndex: 'role_department',
                    key: 'role_department',
                    fixed: 'left',
                    align:'center',
                    scopedSlots: { customRender: 'role_department' },
                    children: [
                        {
                            title:'',
                            width: 160,
                            dataIndex: 'role_department',
                            key: 'role_department',
                            class: 'cursor-pointer',
                            align:'center',
                            scopedSlots: { customRender: 'role_department' },
                        }
                    ],
                },
                {
                    title: 'Lương cơ bản chính thức',
                    dataIndex: 'luong_co_ban_chinh_thuc',
                    key: '1',
                    scopedSlots: { customRender: 'luong_co_ban_chinh_thuc' },
                    align:'center',
                    children: [
                        {
                            title: `${this.getTotal().luong_co_ban_chinh_thuc}`,
                            dataIndex: 'luong_co_ban_chinh_thuc',
                            key: 'luong_co_ban_chinh_thuc',
                            width:'200px',
                            align:'center',
                            scopedSlots: { customRender: 'luong_co_ban_chinh_thuc'},
                        }
                    ],
                },
                {
                    title: 'Lương cơ bản thử việc',
                    dataIndex: 'luong_co_ban_thu_viec',
                    key: '2',
                    scopedSlots: { customRender: 'luong_co_ban_thu_viec' },
                    align:'center',
                    children: [
                        {
                            title:`${this.getTotal().luong_co_ban_thu_viec}`,
                            dataIndex: 'luong_co_ban_thu_viec',
                            key: 'luong_co_ban_thu_viec',
                            scopedSlots: { customRender: 'luong_co_ban_thu_viec'},
                            width:'200px',
                            align:'center'
                        }
                    ],
                },
                {
                    title: 'Đánh giá công việc',
                    dataIndex: 'danh_gia_cong_viec',
                    key: '3',
                    scopedSlots: { customRender: 'danh_gia_cong_viec' },
                    align: 'center',
                    children: [
                        {
                            title:'',
                            width: '200px',
                            dataIndex: 'danh_gia_cong_viec',
                            key: 'danh_gia_cong_viec',
                            align:'center',
                            scopedSlots: { customRender: 'danh_gia_cong_viec' },
                        }
                    ],
                },
                {
                    title: 'Hệ số tính thưởng',
                    dataIndex: 'he_so_tinh_thuong',
                    key: '4',
                    scopedSlots: { customRender: 'he_so_tinh_thuong' },
                    align:'center',
                    children: [
                        {
                            title:'',
                            width: '200px',
                            dataIndex: 'he_so_tinh_thuong',
                            key: 'he_so_tinh_thuong',
                            align:'center',
                            scopedSlots: { customRender: 'he_so_tinh_thuong' },
                        }
                    ],
                },
                {
                    title: 'Số công chính thức',
                    dataIndex: 'so_cong_chinh_thuc',
                    key: '5',
                    scopedSlots: { customRender: 'so_cong_chinh_thuc' },
                    align:'center',
                    children: [
                        {
                            title:'',
                            width: '200px',
                            dataIndex: 'so_cong_chinh_thuc',
                            key: 'so_cong_chinh_thuc',
                            align:'center',
                            scopedSlots: { customRender: 'so_cong_chinh_thuc' },
                        }
                    ],
                },
                {
                    title: 'Số công thử việc',
                    dataIndex: 'so_cong_thu_viec',
                    key: '6',
                    scopedSlots: { customRender: 'so_cong_thu_viec' },
                    align:'center',
                    children: [
                        {
                            title:'',
                            width: '175px',
                            dataIndex: 'so_cong_thu_viec',
                            key: 'so_cong_thu_viec',
                            align:'center',
                            scopedSlots: { customRender: 'so_cong_thu_viec' },
                        }
                    ],
                },
                {
                    title: 'Tổng lương theo ngày công',
                    dataIndex: 'tong_luong_theo_ngay_cong',
                    key: '7',
                    align: 'center',
                    scopedSlots: { customRender: 'tong_luong_theo_ngay_cong' },
                    children: [
                        {
                            title:`${this.getTotal().tong_luong_theo_ngay_cong}`,
                            dataIndex: 'tong_luong_theo_ngay_cong',
                            key: 'tong_luong_theo_ngay_cong',
                            scopedSlots: { customRender: 'tong_luong_theo_ngay_cong'},
                            width:'300px',
                            align: 'center'
                        }
                    ],
                },
                {
                    title: 'Thưởng OT và Hỗ trợ',
                    dataIndex: 'thuong_ot_va_ho_tro',
                    key: '8',
                    scopedSlots: { customRender: 'thuong_ot_va_ho_tro' },
                    align: 'center',
                    children: [
                        {
                            title:`${this.getTotal().thuong_ot_va_ho_tro}`,
                            dataIndex: 'thuong_ot_va_ho_tro',
                            key: 'thuong_ot_va_ho_tro',
                            align: 'center',
                            scopedSlots: { customRender: 'thuong_ot_va_ho_tro'},
                            width:'200px',
                        }
                    ],
                },
                {
                    title: 'Hỗ trợ khác',
                    dataIndex: 'ho_tro_khac',
                    key: '9',
                    scopedSlots: { customRender: 'ho_tro_khac'},
                    align: 'center',
                    children: [
                        {
                            title:`${this.getTotal().ho_tro_khac}`,
                            dataIndex: 'ho_tro_khac',
                            key: 'ho_tro_khac',
                            scopedSlots: { customRender: 'ho_tro_khac'},
                            width:'200px',
                            align: 'center',
                        }
                    ],
                },
                {
                    title: 'Tổng thu nhập',
                    dataIndex: 'tong_thu_nhap',
                    key: '10',
                    scopedSlots: { customRender: 'tong_thu_nhap' },
                    align: 'center',
                    children: [
                        {
                            title:`${this.getTotal().tong_thu_nhap}`,
                            dataIndex: 'tong_thu_nhap',
                            key: 'tong_thu_nhap',
                            scopedSlots: { customRender: 'tong_thu_nhap'},
                            width:'200px',
                            align: 'center',
                        }
                    ],
                },
                {
                    title: 'Thu nhập chịu thuế',
                    dataIndex: 'thu_nhap_chiu_thue',
                    key: '11',
                    scopedSlots: { customRender: 'thu_nhap_chiu_thue' },
                    align: 'center',
                    children: [
                        {
                            title:`${this.getTotal().thu_nhap_chiu_thue}`,
                            dataIndex: 'thu_nhap_chiu_thue',
                            key: 'thu_nhap_chiu_thue',
                            scopedSlots: { customRender: 'thu_nhap_chiu_thue'},
                            width:'200px',
                            align: 'center'
                        }
                    ],
                },
                {
                    title: 'Giảm trừ khác',
                    dataIndex: 'giam_tru_khac',
                    key: '12',
                    scopedSlots: { customRender: 'giam_tru_khac' },
                    align: 'center',
                    children: [
                        {
                            title:`${this.getTotal().giam_tru_khac}`,
                            dataIndex: 'giam_tru_khac',
                            key: 'giam_tru_khac',
                            scopedSlots: { customRender: 'giam_tru_khac'},
                            width:'200px',
                            align: 'center',
                        }
                    ],
                },
                {
                    title: 'Các khoản giảm trừ',
                    dataIndex: 'cac_khoan_giam_tru',
                    key: '13',
                    scopedSlots: { customRender: 'cac_khoan_giam_tru' },
                    align: 'center',
                    children: [
                        {
                            title:`${this.getTotal().cac_khoan_giam_tru}`,
                            dataIndex: 'cac_khoan_giam_tru',
                            key: 'cac_khoan_giam_tru',
                            scopedSlots: { customRender: 'cac_khoan_giam_tru'},
                            align: 'center',
                            width:'200px',
                        }
                    ],
                },
                {
                    title: 'Thực lĩnh trong kỳ',
                    dataIndex: 'thuc_linh_trong_ky',
                    key: '14',
                    scopedSlots: { customRender: 'thuc_linh_trong_ky' },
                    align: 'center',
                    children: [
                        {
                            title:`${this.getTotal().thuc_linh_trong_ky}`,
                            dataIndex: 'thuc_linh_trong_ky',
                            key: 'thuc_linh_trong_ky',
                            scopedSlots: { customRender: 'thuc_linh_trong_ky'},
                            width:'200px',
                            align: 'center',
                        }
                    ],
                },
                {
                    title: ' ',
                    dataIndex: 'edit',
                    fixed: 'right',
                    key: 'edit',
                    children: [
                        {
                            title:'',
                            dataIndex: 'edit',
                            width:'70px',
                            align: 'center',
                            key: 'edit',
                            scopedSlots: { customRender: 'edit' },
                        }
                    ],
                },
            ];
            this.columns = columns;
            return columns;
        },
        customRow(record) {
            return {
                on: {
                    click: () => {
                        if(!this.clickButtonEdit) {
                            this.recordDetail = record
                            this.visibleDrawer = true
                        }else {
                            this.clickButtonEdit = false
                        }
                    },
                },
            };
        },
        handleOnClickEdit(){
            this.clickButtonEdit = true
        },
        showInput(record) {
            record.isEdit = true
        },
        handleChangeHoTro(e) {
            this.editHoTro = e.target.value
        },
        handleChangeGiamTru(e) {
            this.editGiamTru = e.target.value
        },
        handCancelEdit(record) {
            record.isEdit = false
            this.clickButtonEdit = true
            this.visibleDrawer = false
            this.editHoTro = '',
            this.editGiamTru = ''
        },
        onChangePage(page, size) {
            this.currentPage = page;
            this.pageSize = size;
            // this.getAbnormalCaseData();
        },
        onShowSizeChange(page, size) {
            this.currentPage = page;
            this.pageSize = size;
            // this.getAbnormalCaseData();
        },
        onClose() {
            this.visibleDrawer = false
        },
        formatNumber(number) {
            return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
        },
        getTotal() {
            const {dataTotalMock,mockData} = this
            let total = {
                luong_co_ban_chinh_thuc:0,
                luong_co_ban_thu_viec: 0,
                tong_luong_theo_ngay_cong: 0,
                thuong_ot_va_ho_tro: 0,
                ho_tro_khac: '',
                tong_thu_nhap: 0,
                thu_nhap_chiu_thue: 0,
                giam_tru_khac: '',
                cac_khoan_giam_tru: 0,
                thuc_linh_trong_ky: 0,
            }
            mockData.forEach(element => {
                total.luong_co_ban_chinh_thuc += Number(element.luong_co_ban_chinh_thuc)
                total.luong_co_ban_thu_viec += Number(element.luong_co_ban_thu_viec)
                total.tong_luong_theo_ngay_cong += Number(element.tong_luong_theo_ngay_cong)
                total.thuong_ot_va_ho_tro += Number(element.thuong_ot_va_ho_tro)
                total.tong_thu_nhap += Number(element.tong_thu_nhap)
                total.thu_nhap_chiu_thue += Number(element.thu_nhap_chiu_thue)
                total.cac_khoan_giam_tru += Number(element.cac_khoan_giam_tru)
                total.thuc_linh_trong_ky += Number(element.thuc_linh_trong_ky)
                if (element.ho_tro_khac){
                    total.ho_tro_khac += Number(element.ho_tro_khac)
                } else {
                    total.ho_tro_khac = '--'
                }
                if (element.giam_tru_khac) {
                    total.giam_tru_khac += Number(element.giam_tru_khac)
                } else {
                    total.giam_tru_khac = '--'
                }
            })
            dataTotalMock.luong_co_ban_chinh_thuc = this.formatNumber(total.luong_co_ban_chinh_thuc)
            dataTotalMock.luong_co_ban_thu_viec = this.formatNumber(total.luong_co_ban_thu_viec)
            dataTotalMock.tong_luong_theo_ngay_cong = this.formatNumber(total.tong_luong_theo_ngay_cong)
            dataTotalMock.thuong_ot_va_ho_tro = this.formatNumber(total.thuong_ot_va_ho_tro)
            dataTotalMock.tong_thu_nhap = this.formatNumber(total.tong_thu_nhap)
            dataTotalMock.thu_nhap_chiu_thue = this.formatNumber(total.thu_nhap_chiu_thue)
            dataTotalMock.cac_khoan_giam_tru = this.formatNumber(total.cac_khoan_giam_tru)
            dataTotalMock.thuc_linh_trong_ky = this.formatNumber(total.thuc_linh_trong_ky)
            dataTotalMock.ho_tro_khac = total.ho_tro_khac
            dataTotalMock.giam_tru_khac = total.giam_tru_khac
            return dataTotalMock
        },
    },
}