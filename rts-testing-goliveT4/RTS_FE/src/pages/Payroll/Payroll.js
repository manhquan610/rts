import { scaleScreen } from "../../components/commons/commons";
import { mapState } from "vuex";
import http from '../../store/modules/helpers/httpInterceptor';
import { getStatusTimeSheet, formatDate } from "../../components/commons/commons";
import { Role } from '../../components/commons/commonConstant';
import FilterLayout from "../../components/layouts/FilterLayout";
import PayrollOnsite from './PayrollTable/PayrollOnsite/PayrollOnsite.vue'
import PayrollOvertime from './PayrollTable/PayrollOvertime/PayrollOvertime.vue'
import PayrollBonusInMonth from "./PayrollTable/PayrollBonusInMonth/PayrollBonusInMonth.vue";
import PayrollSalaryInPeriod from './PayrollTable/PayrollSalaryInPeriod/PayrollSalaryInPeriod.vue'
import PayrollPayingCash from './PayrollTable/PayrollPayingCash/PayrollPayingCash.vue'
import PayrollPayingTransfer from './PayrollTable/PayrollPayingTransfer/PayrollPayingTransfer.vue'
import PayrollTotalSalary from './PayrollTable/PayrollTotalSalary/PayrollTotalSalary.vue'
import Payslip from "./Payslip/Payslip.vue";
const options = [
    {
        key: 'total_salary',
        label: 'Total Salary'
    },
    {
        key: 'salary_in_period',
        label: 'Salary in Period'
    },
    {
        key: 'bonus_in_month',
        label: 'Bonus in Month'
    },
    {
        key: 'onsite',
        label: 'Onsite'
    },
    {
        key: 'overtime',
        label: 'Overtime Pay'
    },
    // {
    //     key: 'basic_salary',
    //     label: 'Basic Salary'
    // },
    {
        key: 'paying_cash',
        label: 'Paying Cash'
    },
    {
        key: 'paying_transfer',
        label: 'Paying Transfer'
    },
];
export default {
    name: "Payroll",
    components: { 
        FilterLayout, 
        PayrollOnsite , 
        PayrollOvertime, 
        PayrollBonusInMonth, 
        PayrollSalaryInPeriod, 
        PayrollPayingCash, 
        PayrollPayingTransfer,
        PayrollTotalSalary,
        Payslip,
    },
    data() {
        return {
            page: 'Payroll',
            dataFilters: {},
            firstCreated: true,
            loading: false,
            timesheetInfo: {},
            role: Role,
            options, 
            chooseOption: '',
            keyTab: 'payroll-list',
            tabChange: false,

        }
    },
    watch: {
        keyTab: function() {
            this.tabChange = true
        },
    },
    mounted() {
        this.keyTab = this.roles.includes(this.role.ADMIN) || this.roles.includes(this.role.HR) ? 'payroll-list' : 'payslip'
    },
    created(){
      this.scaleScreen(),
      this.dataFilters = {...this.dataFilters, ...JSON.parse(sessionStorage.getItem(this.page)) }
      this.chooseOption = 'salary_in_period'
    },
    computed: {
        ...mapState({
            userInfo: state => state.user.userInfo,
            roles: state => state.user.userInfo.roles ? state.user.userInfo.roles : "",
            timesheetUser: (state) => state.timesheet.timesheetUser ? state.timesheet.timesheetUser : {},
        })
    },
    methods: {
      scaleScreen,
      formatDate,
      getTimesheetDetail(timesheetId) {
        http.get(`/tms/timesheets/${timesheetId}`).then(res => {
            this.timesheetInfo = res.data.data;
        });
        },
        onSetDataFilters(filter) {
            filter.payroll_period = this.dataFilters.payroll_period ? this.dataFilters.payroll_period : filter.payroll_period
        },
        onApplyFilters(filters) {
            this.dataFilters = filters;
            if (!this.firstCreated && !this.tabChange) {
                this.configPage.currentPage = 1
            }
            this.getTimesheetDetail(filters.payroll_period);
            if (this.keyTab === 'payroll-list') {
                this.getUserTimesheetDetail(this.userInfo.userId);
            } else {
                // this.getTimesheetData();
            
            }
            this.tabChange = false
        },
        getUserTimesheetDetail(employmentID) {
            const params = {
                employmentID: employmentID,
                timesheetId: this.dataFilters.payroll_period,
            }
            this.$store.dispatch('user/loadingRequest');
            this.$store.dispatch("timesheet/getUserTimesheet", params).then(() => this.$store.dispatch('user/loadingRequest')).catch(() => {
                this.$message.error('Fail to get User Timesheet Data',5);
                this.$store.dispatch('user/loadingRequest');
            });
            sessionStorage.setItem(this.page, JSON.stringify({
                employmentID: employmentID,
                payroll_period: this.dataFilters.payroll_period
            }))
        },
        getStatus() {
            return getStatusTimeSheet(this.timesheetInfo.status);
        },
        handleChange(value) {
            this.chooseOption = value
        },
        handleChangeTab(key) {
            this.keyTab = key
            // if (key === 'payslip') {
            //     this.page = 'Payslip'
            //     this.dataFilters = {...this.dataFilters, ...JSON.parse(sessionStorage.getItem(this.page)) }
            // } else {
            //     this.page = 'Payroll'
            //     this.dataFilters = {...this.dataFilters, ...JSON.parse(sessionStorage.getItem(this.page)) }
            // }
        }
    }
  };