import { convertAvatarName } from "../../../components/commons/commons"
import moment from 'moment'
export default {
    name: 'payslip',
    props: {
        timesheetUser: {
            type: Object
        },
        timesheetInfo: {
            type: Object
        },
        formatDate: {
            type: Function,
        },
    },
    data() {
        return {
            detailPayrollPeriod: [
                {
					title: 'Tổng số ngày công',
					description: 'so_ngay_cong',
					key: 'so_ngay_cong',
				},
                {
					title: 'Ngày công chính thức',
					description: 'ngay_cong_chinh_thuc',
					key: 'ngay_cong_chinh_thuc',
				},
                {
                    title: 'Ngày công thử việc',
                    description: 'ngay_cong_thu_viec',
					key: 'ngay_cong_thu_viec',
                },
                {
                    title: 'Lương cơ bản chính thức',
                    description: 'luong_co_ban_chinh_thuc',
					key: 'luong_co_ban_chinh_thuc',
                },
                {
                    title: 'Lương cơ bản thử việc',
                    description: 'luong_co_ban_thu_viec',
					key: 'luong_co_ban_thu_viec',
                },
                {
                    title: 'Thưởng đánh giá tháng chính thức',
                    description: 'thuong_danh_gia_thang_chinh_thuc',
					key: 'thuong_danh_gia_thang_chinh_thuc',
                },
                {
                    title: 'Thưởng đánh giá tháng thử việc',
                    description: 'thuong_danh_gia_thang_thu_viec',
					key: 'thuong_danh_gia_thang_thu_viec',
                },
                {
                    title: 'Đánh giá công việc',
                    description: 'danh_gia_cong_viec',
					key: 'danh_gia_cong_viec',
                },
                {
                    title: 'Hệ số đánh giá',
                    description: 'he_so_danh_gia',
					key: 'he_so_danh_gia',
                },
            ],
            detailPeriodA: [
                {
                    title: 'A: Các khoản thu nhập',
					description: '',
					key: '',
                },
                {
                    title: 'Lương cơ bản theo ngày chính thức',
					description: 'luong_co_ban_theo_ngay_chinh_thuc',
					key: 'luong_co_ban_theo_ngay_chinh_thuc',
                },
                {
                    title: 'Lương cơ bản theo ngày thử việc',
					description: 'luong_co_ban_theo_ngay_thu_viec',
					key: 'luong_co_ban_theo_ngay_thu_viec',
                },
                {
                    title: 'Thưởng cơ bản theo ngày chính thức',
					description: 'thuong_co_ban_theo_ngay_chinh_thuc',
					key: 'thuong_co_ban_theo_ngay_chinh_thuc',
                },
                {
                    title: 'Thưởng cơ bản theo ngày thử việc',
					description: 'thuong_co_ban_theo_ngay_thu_viec',
					key: 'thuong_co_ban_theo_ngay_thu_viec',
                },
                {
                    title: 'Giảm trừ thưởng đánh giá tháng (50,000 x số lần vi phạm)',
					description: 'giam_tru_danh_gia_thang',
					key: 'giam_tru_danh_gia_thang',
                },
                {
                    title: 'Hỗ trợ Onsite',
					description: 'ho_tro_onsite',
					key: 'ho_tro_onsite',
                },
                {
                    title: 'Lương làm thêm giờ',
					description: 'luong_lam_them_gio',
					key: 'luong_lam_them_gio',
                },
                {
                    title: 'Hỗ trợ làm thêm giờ',
					description: 'ho_tro_lam_them_gio',
					key: 'ho_tro_lam_them_gio',
                },
                {
                    title: 'Làm thêm giờ quá định mức',
					description: 'lam_them_gio_qua_dinh_muc',
					key: 'lam_them_gio_qua_dinh_muc',
                },
                {
                    title: 'Hỗ trợ khác',
					description: 'ho_tro_khac',
					key: 'ho_tro_khac',
                },
                {
                    title: 'Tổng',
					description: 'tong',
					key: 'tong',
                },
            ],
            detailPeriodB: [
                {
					title: 'B: Các khoản giảm trừ',
					description: '',
					key: '',
				},
                {
                    title: 'Bảo hiểm Xã hội (BHXH, BHYT, BHTN)',
                    description: 'bhxh',
                    key: 'bhxh',
                },
                {
                    title: 'Bảo hiểm xã hội',
                    description: 'bhxh_child',
                    key: 'bhxh_child',
                },
                {
                    title: 'Bảo hiểm y tế',
                    description: 'bhyt',
                    key: 'bhyt',
                },
                {
                    title: 'Bảo hiểm thất nghiệp',
                    description: 'bhtn',
                    key: 'bhtn',
                },
                {
                    title: 'Kinh phí công đoàn',
                    description: 'kinh_phi_cong_doan',
                    key: 'kinh_phi_cong_doan',
                },
                {
                    title: 'Số người phụ thuộc',
                    description: 'so_nguoi_phu_thuoc',
                    key: 'so_nguoi_phu_thuoc',
                },
                {
                    title: 'Tiền giảm trừ gia cảnh',
                    description: 'tien_giam_tru_gia_canh',
                    key: 'tien_giam_tru_gia_canh',
                },
                {
                    title: 'Thu nhập miễn tính thuế',
                    description: 'thu_nhap_mien_tinh_thue',
                    key: 'thu_nhap_mien_tinh_thue',
                },
                {
                    title: 'Thu nhập chịu thuế',
                    description: 'thu_nhap_chiu_thue',
                    key: 'thu_nhap_chiu_thue',
                },
                {
                    title: 'Thu nhập tính thuế',
                    description: 'thu_nhap_tinh_thue',
                    key: 'thu_nhap_tinh_thue',
                },
                {
                    title: 'Thuế TNCN',
                    description: 'thue_tncn',
                    key: 'thue_tncn',
                },
                {
                    title: 'Tổng',
                    description: 'tong',
                    key: 'tong',
                },
            ],
            detailPeriodC: [
                {
					title: 'C: Các khoản truy lĩnh',
					description: '',
					key: '',
				},
                {
                    title: 'Truy linh tháng trước',
                    description: 'truy_hoan_thang_truoc',
                    key: 'truy_hoan_thang_truoc',
                },
                {
                    title: 'Quyết toán thuế TNCN 2019',
                    description: 'quyet_toan_thue_tncn',
                    key: 'quyet_toan_thue_tncn',
                },
                {
                    title: 'Chế độ BHXH',
                    description: 'che_do_bhxh',
                    key: 'che_do_bhxh',
                },
                {
                    title: 'Truy lĩnh khác',
                    description: 'truy_hoan_khac',
                    key: 'truy_hoan_khac',
                },
                {
                    title: 'Tổng',
                    description: 'tong',
                    key: 'tong',
                },
            ],
            detailPeriodD: [
                {
					title: 'D: Các khoản truy thu',
					description: '',
					key: '',
				},
                {
					title: 'Lương tháng trước',
					description: 'luong_thang_truoc',
					key: 'luong_thang_truoc',
				},
                {
					title: 'Quyết toán thuế 2019',
					description: 'quyet_toan_thue',
					key: 'quyet_toan_thue',
				},
                {
					title: 'Thanh toán thẻ / thanh toán BHXH',
					description: 'thanh_toan_the_bhxh',
					key: 'thanh_toan_the_bhxh',
				},
                {
					title: 'Truy thu khác',
					description: 'truy_thu_khac',
					key: 'truy_thu_khac',
				},
                {
					title: 'Tổng',
					description: 'tong',
					key: 'tong',
				},
            ],
            isDisplay: false,
        }
    },
    methods: {
        moment,
        getAvatarName(fullName){
			return convertAvatarName(fullName);
		},
		randomColor() {
			const r = () => Math.floor(256 * Math.random());
			return `rgb(${r()}, ${r()}, ${r()})`;
		},
        displayContent(){
            this.isDisplay = !this.isDisplay
        }
    },
}