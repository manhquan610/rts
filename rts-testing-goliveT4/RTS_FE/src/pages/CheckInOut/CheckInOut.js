import FilterLayout from "../../components/layouts/FilterLayout";
import { convertAvatarName, scaleScreen, initFilters } from "../../components/commons/commons";
import { Role } from "../../components/commons/commonConstant";
import moment from 'moment';
import {
	mapState,
} from "vuex";
export default {
	components: {
		FilterLayout
	},

	data() {
		return {
			page: 'Check In-Out Time',
			columns: [
				{	
					title: 'Sat',
					dataIndex: 'Sat',
					key: 'Sat',
					width: '13%',
					scopedSlots: { customRender: 'Sat' },
					class: "bg-gray"
				},
				{
					title: 'Sun',
					dataIndex: 'Sun',
					key: 'Sun',
					width: '13%',
					scopedSlots: { customRender: 'Sun' },
					class: "bg-gray"
				},
				{
					title: 'Mon',
					dataIndex: 'Mon',
					key: 'Mon',
					width: '13%',
					scopedSlots: { customRender: 'Mon' },
				},
				{
					title: 'Tue',
					dataIndex: 'Tue',
					key: 'Tue',
					width: '13%',
					scopedSlots: { customRender: 'Tue' },
				},
				{
					title: 'Wed',
					dataIndex: 'Wed',
					key: 'Wed',
					width: '13%',
					scopedSlots: { customRender: 'Wed' },
				},
				{
					title: 'Thu',
					dataIndex: 'Thu',
					key: 'Thu',
					width: '13%',
					scopedSlots: { customRender: 'Thu' },
				},
				{
					title: 'Fri',
					dataIndex: 'Fri',
					key: 'Fri',
					width: '13%',
					scopedSlots: { customRender: 'Fri' },
				}
			],
			dataSource: [],
			filters: {},
			Role,
			dateFormatList: ["DD/MM/YYYY", "DD/MM/YY"],
			dateFormat: 'YYYY/MM/DD',
			loading: false,
			currentDate: moment(),
			itemListOfDay: [],
			staffInfo: {},
			rowKey: 0,
			visible: false,
			colorAvatar: localStorage.getItem('color_avatar'),
			selectedDay: '',
			fetching: false,
			visibleUpdateJob: false,
			rangeDate: "",
			formUpdateJob: this.$form.createForm(this,{name: 'updateCheckInOut'})
		}
	},
	computed: {
		...mapState({
			userInfo: (state) => state.user.userInfo,
			roles: state => state.user.userInfo.roles ? state.user.userInfo.roles : "",
			employeeList: state => state.application.employeeList,
		}),
	},
	watch: {
		currentDate: function () {
			this.getCheckInOutDetailByDay();
		}
	},

	created() {
		this.scaleScreen()
		this.handFilters()
	},
	methods: {
		moment,
		scaleScreen,
		handFilters() {
			const { filters } = this
			initFilters(filters)
			this.filters = { ...this.filters, ...JSON.parse(sessionStorage.getItem(this.page)) }
		},
		// handleDateClick(arg) {
		// 	if (confirm("Would you like to add an event to " + arg.dateStr + " ?")) {
		// 		this.calendarEvents.push({
		// 			// add new event data
		// 			title: "New Event",
		// 			start: arg.date,
		// 			allDay: arg.allDay
		// 		});
		// 	}
		// },

		onSetDataFilters(filters) {
			this.filters = filters
		},
		setRowKey(index){	
			return index.toString();
		},
		onApplyFilters(filters) {
			this.filters = filters
			this.getCheckInOutData(filters);
			this.getCheckInOutDetailByDay();
			sessionStorage.setItem(this.page, JSON.stringify(filters))
		},

		checkActiveDay(dateCompare) {
			const { currentDate } = this;
			if (moment(dateCompare).isSame(moment(currentDate), 'days')) {
				return 'is-active-check-in-out';
			} else {
				return '';
			}
		},

		getDataManual() {
			this.getDataManualJob();
		},

		/// Get check in-out data list by user
		getCheckInOutData(filters) {
			this.$store.dispatch('user/loadingRequest')
			let startDate = moment(filters.fromDate);
			let endDate = moment(filters.toDate);
			let dayOfStart = moment(filters.fromDate).day();
			let dayOfEnd = moment(filters.toDate).day();

			///caculate start date
			if (dayOfStart === 0) {
				startDate = moment(filters.fromDate).add(-1, 'day');
			} else if (dayOfStart < 6 && dayOfStart > 0) {
				startDate = moment(filters.fromDate).add(-(dayOfStart + 1), 'day');
			}
			///caculate end date
			if (dayOfEnd === 6) {
				endDate = moment(filters.toDate).add(6, 'day');
			}
			else if (dayOfEnd > 0 && dayOfEnd < 6) {
				endDate = moment(filters.toDate).add((7 - dayOfEnd), 'day');
			}
			let username = this.userInfo.userName;
			if (filters && filters.employee && filters.employee.length === 1) {
				username = filters.employee[0]
			}

			let url = `?username=${username}&startDate=${moment(filters.fromDate).format("YYYY-MM-DD")}&endDate=${moment(filters.toDate).format("YYYY-MM-DD")}`
			this.$store.dispatch('checkinout/getCheckInOutList', url).then(res => {
				if (res && res.code === "000000" && res.data !== null) {
					let timesheets = [];
					var startWorkingDay = res.data.start_working_day ? moment(res.data.start_working_day) : null;
					timesheets = res.data.timesheets ? res.data.timesheets : []
					this.dataSource = this.mappingData(startDate, endDate,startWorkingDay, timesheets);
					this.staffInfo = res.data
				} else {
					this.dataSource = this.mappingData(startDate, endDate,startWorkingDay, []);
					this.$message.error(res.message,5);
				}
				this.$store.dispatch('user/loadingRequest');
			}).catch(error => {
				this.$message.error(error.message,5);
				this.dataSource = this.mappingData(startDate, endDate,null, [])
				this.$store.dispatch('user/loadingRequest');
			})
		},

		mappingData(startDate, endDate, startWorkingDay, data) {
			let i = 0;
			let dataSource = [];
			let objItem = {};
			while (startDate.isSameOrBefore(endDate)) {
				i++;
				let cellObj = {};
				let dataIndex = moment(startDate).format('ddd');
				cellObj.date = moment(startDate);
				cellObj.label = moment(startDate).format('DD/MM');
				cellObj.key = dataIndex;
				cellObj.id = Math.random()

				let findIndexNormalDay = data.findIndex(item => item && moment(item.check_in_time).isSame(startDate, 'days'))
				if (findIndexNormalDay > -1) {
					cellObj.start_time = data[findIndexNormalDay].check_in_time ? moment(data[findIndexNormalDay].check_in_time) : undefined;
					cellObj.end_time = data[findIndexNormalDay].check_out_time ? moment(data[findIndexNormalDay].check_out_time) : undefined;
					cellObj.is_early_leaving = data[findIndexNormalDay].is_early_leaving;
					cellObj.is_late_coming = data[findIndexNormalDay].is_late_coming;
				}
				let findIndexFullTimeAbsent = data.findIndex(item => item && !item.is_weekend_or_holiday && item.check_in_time === null && moment(item.date_of_issue).isSame(startDate, 'days') && startDate.isSameOrBefore(new Date()))
				if (findIndexFullTimeAbsent > -1) {
					cellObj.is_early_leaving = true;
					cellObj.is_late_coming = true;
				}

				let findIndexFullTimeOnboard = data.findIndex(item => item && moment(item.date_of_issue).isBefore(startWorkingDay))
				if(findIndexFullTimeOnboard > -1) {
					cellObj.is_early_leaving = false;
					cellObj.is_late_coming = false;
				}
				objItem[dataIndex] = cellObj;
				if (i === 7) {
					dataSource.push(objItem);
					objItem = {};
					i = 0;
				}
				startDate = startDate.add(1, 'day');
			}
			return dataSource;
		},

		onshowModal() {
			this.visibleUpdateJob = true
		},

		handleCancelModalUpdate() {
			this.visibleUpdateJob = false
			this.formUpdateJob.resetFields()
		},

		handleSubmitJob(e) {
			e.preventDefault();
			this.$message.loading('Please wait... !',6)
			this.formUpdateJob.validateFields((err, values) => {
				if(!err) {
					let department_code = ""
					let employee = []
					values.staff.forEach(element => {
						employee.push(JSON.parse(element).value)
					})
					let start_date = moment(this.rangeDate[0],"DD/MM/YYYY").format("YYYY-MM-DD")
					let end_date = moment(this.rangeDate[1],"DD/MM/YYYY").format("YYYY-MM-DD")
					let payload = {
						department_code,
						employee,
						start_date,
						end_date
					}
					this.$store.dispatch('checkinout/updateJobCheckInOut', payload).then((res) => {
						if(res && res.data) {
							this.$message.success('Update Successfully')
							this.visibleUpdateJob = false
							this.formUpdateJob.resetFields();
							this.rangeDate = ""
						}
					}).catch((error) => {
						this.fetching = false
						this.$message.error(error.message)
					})
				}
			})
		}, 
		onSearchStaff(value, timeout) {
			let cloneVal = decodeURI(value);
			if (value && value !== "" && value.length >= 3) {
			  this.fetching = true;
			  clearTimeout(this.timeout);
			  this.timeout = setTimeout(() => {
				this.$store
				  .dispatch("application/getEmployeeList", cloneVal)
				  .then(() => {
					this.fetching = false;
				  })
				  .catch(() => {
					this.fetching = false;
				  });
			  }, timeout);
	  
			}
		},
		getTime(date) {
			if (date && date !== null && date !== undefined) return moment(date).format("HH:mm")
			else return '';
		},
		formatDate(date) {
			if (date) return moment(date).format("DD/MM/YYYY")
			else return '';
		},
		/// Get check in-out data by day
		getCheckInOutDetailByDay() {
			const { currentDate, userInfo, filters } = this;
			let dataFormat = moment(currentDate).format("YYYY-MM-DD")
			let username = userInfo.userName;
			if (filters && filters.employee && filters.employee.length === 1) {
				username = filters.employee[0]
			}
			let url = `${dataFormat}?username=${username}`;
			this.$store.dispatch('checkinout/getCheckInOutDetailByDay', url).then(res => {
				let itemListOfDay = []
				if (res && res.data && res.code === "000000") {
					itemListOfDay = res.data.map((item, index) => {
						item.key = index
						item.time = moment(item.check_time).format("HH:mm:ss");
						return item;
					})
				} else {
					this.$message.error(res.message,5);
				}
				this.itemListOfDay = itemListOfDay
			}).catch(error => {
				this.$message.error(error.message,5);
			})
		},

		getDataManualJob() {
			const { filters } = this;
			let params = {
				start_date: moment(filters.startDate).format("YYYY-MM-DD"),
				end_date: moment(filters.endDate).format("YYYY-MM-DD"),
				employee: filters.employee,
				department_code: filters.department
			}
			this.$store.dispatch('checkinout/getDataManualJob', params).then(res => {
				if (res.message !== "") {
					this.$message.error(res.message,5);
				} else if (res.data) {
					this.$message.success(res.data.status,5);
				}
			}).catch(error => {
				this.$message.error(error.message,5);
			})
		},
		onHandleClickDay(date) {
			this.currentDate = date
			this.showDrawer()
		},
		showDrawer() {
			this.visible = true;
		},
		onClose() {
			this.visible = false;
		},
		getAvatarName(fulname) {
			return convertAvatarName(fulname);
		},
		randomColor() {
			const r = () => Math.floor(256 * Math.random());
			return `rgb(${r()}, ${r()}, ${r()})`;
		}
	},
};
