import { mapState } from "vuex";
import {convertAvatarName} from "../../../components/commons/commons";
export default {
	props: ['drawerKey','timesheetUser','timesheetInfo'],
	data() {
		return {
			detailMonthOfStaff:[
				{
					title: 'Total working days',
					description: 'Total working days',
					key: 'total_working_days'
				},
				{
					title: 'Total paid leave days',
					description: 'Total paid leave days',
					key: 'total_paid_leave_days'
				},
				{
					title: 'Total paid benefits leave days',
					description: 'Total paid benefits leave days',
					key: 'total_paid_benefits_leave_days'
				},
				{
					title: 'Birthday leave',
					description: 'Birthday leave',
					key: 'birthday_leave'
				},
				{
					title: 'Total onsite days',
					description: 'Total onsite days',
					key: 'total_onsite_days'
				},
				{
					title: 'Total public holidays',
					description: 'Total public holidays',
					key: 'total_public_holidays'
				},
				{
					title: 'Total offset leave days',
					description: 'Total offset leave days',
					key: 'total_offset_leave_days'
				},
				{
					title: 'Total leave days that social insurance covers',
					description: 'Total leave days that social insurance covers',
					key: 'total_days_off_social_insurance_covers'
				},
				{
					title: 'Total unpaid leave days',
					description: 'Total unpaid leave days',
					key: 'total_unpaid_leave_days'
				},
				{
					title: 'Total paid days',
					description: 'Total paid days',
					key: 'total_paid_days'
				},
				{
					title: 'Total paid probationary days',
					description: 'Total paid probationary days',
					key: 'total_paid_probationary_days'
				},
				{
					title: 'Total paid official days',
					description: 'Total paid official days',
					key: 'total_paid_official_days'
				}					
			],
			dayOffByStaff: [
				{
					title: 'Total authorized leave days',
					description: 'Total authorized leave days',
					key: 'total_annual_leave'
				},
				{
					title: 'Beginning of period',
					description: 'Beginning of period',
					key: 'annual_leave_beginning_of_period'
				},
				{
					title: 'Used in period',
					description: 'Số ngày phép dùng trong kì',
					key: 'annual_leave_days_used_in_period'
				},
				{
					title: 'End of period',
					description: 'End of period',
					key: 'remaining_paid_leave_days_of_year'
				},
			],
			infringeData: [
				{
					title: 'Approved late comings and early leavings',
					description: 'Approved late comings and early leavings',
					key: 'approved_late_coming_and_early_leaving'
				},
				{
					title: 'Unapproved late comings and early leavings',
					description: 'Unapproved late comings and early leavings',
					key: 'unapproved_late_coming_and_early_leaving'
				},
				{
					title: 'Approved “missed attendance-checks”',
					description: 'Approved “missed attendance-checks”',
					key: 'approved_forget_taking_attendance'
				},
				{
					title: '“Missed attendance-check” days counted as working days',
					description: '“Missed attendance-check” days counted as working days',
					key: 'days_forget_taking_attendance_counted_as_working_day'
				},
				{
					title: '“Missed attendance-check” days counted as half working days',
					description: '“Missed attendance-check” days counted as half working days',
					key: 'days_forget_taking_attendance_counted_as_half_working_day'
				},
				{
					title: '“Missed attendance-check” days counted as unpaid leave days',
					description: '“Missed attendance-check” days counted as unpaid leave days',
					key: 'days_forget_taking_attendance_counted_as_without_working_day'
				},
				{
					title: 'Late coming, early leaving, “Missed attendance-check” days',
					description: 'Late coming, early leaving, “Missed attendance-check” days',
					key: 'days_come_late_leave_early_forget_taking_attendance'
				},
				{
					title: 'Total number of violations',
					description: 'Total number of violations',
					key: 'total_number_of_violations'
				},
				{
					title: 'Total number of violations that exceeds fine level',
					description: 'Total number of violations that exceeds fine level',
					key: 'total_number_of_violations_exceed'
				},				
			],
		}
	},

	methods: {
		getAvatarName(fullName){
			return convertAvatarName(fullName);
		},
		randomColor() {
			const r = () => Math.floor(256 * Math.random());
			return `rgb(${r()}, ${r()}, ${r()})`;
		}
	},
	computed: {
		...mapState({
			notesData: state => state.timesheet.noteList,
			calculationWorking: state => state.timesheet.calculationWorking,
			violationWorking: state => state.timesheet.countWokingMyTS,
			annualLeave: state => state.timesheet.countWokingMyTS,
		  })
    },
    created() {
	},
}