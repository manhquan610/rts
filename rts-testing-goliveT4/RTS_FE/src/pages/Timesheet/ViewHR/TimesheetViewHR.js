import moment from 'moment';
import { mapState } from "vuex";
import Vue from "vue";
import { statusTimesheet } from '../../../components/commons/commonConstant';
import http from '../../../store/modules/helpers/httpInterceptor';
import ContextModifyTable from './ContextModifyTable.vue';
import { convertObjectToParams } from "../../../components/commons/commons";

Vue.filter('formatDayOfWeek', function (value) {
	return moment(value).format('ddd');
});

Vue.filter('formatDayMonth', function (value) {
	return moment(value).format('DD/MM');
});

Vue.directive('click-outside', {
	bind: function (el, binding, vnode) {
		el.clickOutsideEvent = function (event) {
			// here I check that click was outside the el and his children
			if (!(el == event.target || el.contains(event.target))) {
				// and if it did, call method provided in attribute value
				vnode.context[binding.expression](event);
			}
		};
		document.body.addEventListener('click', el.clickOutsideEvent)
	},
	unbind: function (el) {
		document.body.removeEventListener('click', el.clickOutsideEvent)
	},
});

export default {
	name: 'timesheet-View-hr',
	components: { ContextModifyTable },
	props: ['selectedRowKeys', 'dataSource', 'onSelectChange', 'startDate', 'endDate', 'onShowDrawerTimesheetDetail', 'configPage', 'onChangeConfigPage', "timesheetInfo", "reloadData", 'dataFilters'],
	data() {
		return {
			showDrawer: false,
			columns: [],
			checkboxWidth: '',
			changeOrNot: document.body.clientWidth < 1400 ? false : true,
			visibleModal: false,
			isValidComment: true,
			titleModal: '',
			modifyKey: 'row',
			formUpdate: {
				employee: null,
				isAbnormalCase: true,
				value: '',
				date: [],
				isValid: true,
				commentReason: '',
			},
			loading: false,
			statusTimesheet,
			configModify: {
				isVisible: false,
				modifyKey: "row",
				x: 0, y: 0
			},
			scrollAble: { x: 1500, y: 600 },
			tooltipTimesheet: {
				data: [
					{ label: 'Check in-out time', key: 'checkInOut' },
					{ label: 'Request', key: 'request' },
					{ label: 'Edited', key: 'edited' },
					{ label: 'Editor', key: 'editor' },
					{ label: 'Comment', key: 'comment' },
					{ label: 'Total Edited Time', key: 'totalEditedTime' },
					{ label: 'Active Time', key: 'activeTime' },
				],
				config: {
					isVisible: false,
					x: 0, y: 0
				}
			},
			loadingTotalTimesheet: false
		};
	},
	created() {
		window.addEventListener("resize", this.resizeEventHandler);
	},
	destroyed() {
		window.removeEventListener("resize", this.resizeEventHandler);
	},
	mounted() {
		this.renderColumn();
		this.$store.dispatch("timesheet/getRuleIntegration");
	},
	watch: {
		timesheetInfo: function (newVal, oldVal) {
			if (newVal.timesheet_id !== oldVal.timesheet_id) {
				this.renderColumn();
			}
		},
		changeOrNot: function () {
			this.renderColumn();
		},
	},
	computed: {
		...mapState({
			ruleIntegration: state => state.timesheet.ruleIntegration,
		})
	},
	methods: {
		resizeEventHandler() {
			if (document.body.clientWidth > 1400) {
				this.changeOrNot = true
			} else {
				this.changeOrNot = false
			}
		},
		renderColumn() {
			let columns = [
				{
					title: 'Staff',
					dataIndex: 'staff',
					fixed: "left",
					class: 'cursor-pointer',
					align: 'left',
					width: 170,
					scopedSlots: { customRender: 'staff' },
					customCell: this.customCellStaff
				},
				{
					title: 'Role/Department',
					dataIndex: 'role',
					fixed: "left",
					width: 130,
					scopedSlots: { customRender: 'role' },
				},
				{
					title: 'Total',
					dataIndex: 'total_workday',
					fixed: "left",
					width: 55,
					align: 'center',
				}
			];
			let startDate = moment(this.timesheetInfo.from_date);
			let endDate = moment(this.timesheetInfo.to_date);
			if (this.changeOrNot) {
				this.checkboxWidth = '45px'
				this.scrollAble = { x: 1500, y: 600 }
				while (startDate.isSameOrBefore(endDate)) {
					let day = moment(startDate).day()
					let className;
					className = (day === 0 || day === 6) ? 'bg-gray' : ""
					columns.push({
						dataIndex: moment(startDate).format("YYYYMMDD"),
						width: 55,
						align: 'center',
						class: className,
						key: moment(startDate).format("YYYYMMDD"),
						slots: { title: moment(startDate).format("YYYYMMDD") },
						scopedSlots: { customRender: moment(startDate).format("YYYYMMDD"), },
						customHeaderCell: this.customHeaderCell,
						isHolyday: day === 0 || day === 6
					})
					startDate = startDate.add(1, 'day');
				}
			}
			else {
				columns.splice(1, 1)
				this.checkboxWidth = '15px'
				this.scrollAble = { y: 600 }
				while (startDate.isSameOrBefore(endDate)) {
					let day = moment(startDate).day()
					let className;
					className = (day === 0 || day === 6) ? 'bg-gray' : ""
					columns.push({
						dataIndex: moment(startDate).format("YYYYMMDD"),
						width: 17,
						align: 'center',
						class: className,
						key: moment(startDate).format("YYYYMMDD"),
						slots: { title: moment(startDate).format("YYYYMMDD") },
						scopedSlots: { customRender: moment(startDate).format("YYYYMMDD"), },
						customHeaderCell: this.customHeaderCell,
						isHolyday: day === 0 || day === 6
					})
					columns.forEach((element, index) => {
						if (index === 0) {
							columns[index].width = 45;
							columns[index].fixed = false;
						}
						if (index === 1) {
							columns[index].width = 15;
							columns[index].fixed = false;
						}
						if (element.isHolyday) {
							if (index > 0) {
								if (columns[index - 1].isHolyday) {
									columns.splice(index - 1, 2)
									columns.splice(index - 1, 0, {
										title: '',
										dataIndex: '',
										width: 2,
										class: 'bg-yellow',
										align: 'center',
									})
								}
							}
						}
					});
					startDate = startDate.add(1, 'day');
				}
			}
			this.columns = columns;
			return columns;
		},

		// handle right click header cell
		customHeaderCell(column) {
			return {
				on: {
					contextmenu: event => {
						event.preventDefault()
						if ((this.timesheetInfo.status === this.statusTimesheet.draft || this.timesheetInfo.status === this.statusTimesheet.pendingUpdate && this.dataSource.length > 0) && !column.isHolyday) {
							if (!this.configModify.isVisible) {
								const that = this;
								document.addEventListener(`click`, function onClickOutside() {
									that.configModify.isVisible = false;
									document.removeEventListener(`click`, onClickOutside)
								})
							}
							this.configModify = {
								data: column,
								isVisible: true,
								modifyKey: "column",
								x: event.clientX,
								y: event.clientY
							}
						}
					},
					click: event => {
						event.preventDefault();
						this.tooltipTimesheet.config = {
							isVisible: true,
							x: event.clientX,
							y: event.clientY,
							viewMode: 'total'
						}
						const paramFilter = convertObjectToParams({
							location: this.dataFilters.location ? this.dataFilters.location : [],
							department: this.dataFilters.department ? this.dataFilters.department : [],
							role: this.dataFilters.role ? this.dataFilters.role : [],
							employee: this.dataFilters.employee ? this.dataFilters.employee : [],
							actionAbnormalCase: this.dataFilters.abnormal_case ? this.dataFilters.abnormal_case : false,
							request_type: this.dataFilters.request_type ? this.dataFilters.request_type : [],
						});
						if (!this.loadingTotalTimesheet) {
							this.loadingTotalTimesheet = true;
							const date = moment(column.key).format("YYYY-MM-DD");
							let total = 0;
							http.get(`/tms/timesheets/total/${date}${paramFilter}`).then(res => {
								if (res.data.data) {
									total = res.data.data.total_normal + res.data.data.total_authorized + res.data.data.total_halfday_unauthorized;
								}
								this.tooltipTimesheet.total = total;
								this.loadingTotalTimesheet = false;
							}).catch(err => {
								this.loadingTotalTimesheet = false;
								this.$message.error(err.response.data.error,5);
							})
						}
					}
				}
			}
		},

		onHandleRightClickCell(column) {
			this.visibleModal = true;
			this.titleModal = 'Modify Column';
			this.modifyKey = 'column';
			this.formUpdate.date = [moment(column.key), moment(column.key)];
		},

		// handle click cell Staff
		customCellStaff(record, rowIndex) {
			return {
				on: {
					click: () => {
						this.onShowDrawerTimesheetDetail(record, rowIndex)
					}
				},
			};
		},


		// handle right click table row
		customRow(record) {
			return {
				on: {
					contextmenu: event => {
						event.preventDefault()
						if (this.timesheetInfo.status === this.statusTimesheet.draft || this.timesheetInfo.status === this.statusTimesheet.pendingUpdate) {
							if (!this.configModify.isVisible) {
								const that = this;
								document.addEventListener(`click`, function onClickOutside() {
									that.configModify.isVisible = false;
									document.removeEventListener(`click`, onClickOutside)
								})
							}
							this.configModify = {
								data: record,
								isVisible: true,
								modifyKey: "row",
								x: event.clientX,
								y: event.clientY
							}
						}
					}
				},
			};
		},

		onHandleRightClickRow(record) {
			this.titleModal = 'Modify Row'
			this.modifyKey = 'row'
			this.visibleModal = true;
			this.formUpdate.employee = record.employmentID;
		},

		handleSubmitModal() {
			if (!this.formUpdate.commentReason) {
				this.isValidComment = false;
				if (this.formUpdate.from_date === '' || this.formUpdate.to_date === '') {
					this.formUpdate.isValid = false;
				}
			}
			else {
				this.formUpdate.isValid = true;
				this.loading = true;
				const data = {
					abnormal_only: this.formUpdate.isAbnormalCase,
					employee: this.modifyKey === 'column' ? null : this.formUpdate.employee,
					from_date: this.formUpdate.date[0].format('YYYY-MM-DD'),
					to_date: this.formUpdate.date[1].format('YYYY-MM-DD'),
					value: this.formUpdate.value,
					comment: this.formUpdate.commentReason,
					filter: {
						location: this.dataFilters.location ? this.dataFilters.location : [],
						department: this.dataFilters.department ? this.dataFilters.department : [],
						role: this.dataFilters.role ? this.dataFilters.role : [],
						list_employee: this.dataFilters.employee ? this.dataFilters.employee : [],
						abnormal: this.dataFilters.abnormal_case ? this.dataFilters.abnormal_case : false,
						request_type: this.dataFilters.request_type ? this.dataFilters.request_type : [],
					}
				}
				http.put("/tms/timesheets", data).then((res) => {
					this.loading = false;
					this.handleCancelModal();
					if (res.data.data) {
						this.$message.success(`Update ${this.modifyKey} working day successfully.`,5);
						this.reloadData();
					} else {
						this.$message.error(res.data.message,5);
					}
				}).catch((error) => {
					this.loading = false;
					this.$message.error(error.response.data.error,5);
				});
			}
		},

		handleCancelModal() {
			this.visibleModal = false;
			this.resetFormUpdate();
		},
		onChangePage(pageNumber, pageSize) {
			// const config = {...this.configPage}
			this.configPage.pageSize = pageSize;
			this.configPage.currentPage = pageNumber;
			this.onChangeConfigPage(this.configPage);
		},

		disabledDate(current) {
			return current < moment(this.timesheetInfo.from_date) || current > moment(this.timesheetInfo.to_date).endOf('day');
		},

		resetFormUpdate() {
			this.isValidComment = true;
			this.formUpdate = {
				employee: null,
				isAbnormalCase: true,
				value: '',
				date: [],
				isValid: true,
				commentReason: ''
			};
		},

		mouseleaveTimesheet() {
			event.preventDefault();
			this.tooltipTimesheet.config.isVisible = false;
		},

		handleHoverTimeSheet(event, record) {
			event.preventDefault();
			if (record && record.value) {
				const widthScreen = window.innerWidth;
				const heightScreen = window.innerHeight;
				const widthTooltip = 350;
				const heightTooltip = document.getElementById('tooltip-timesheet').offsetHeight;
				const clientX = widthScreen < (event.clientX + widthTooltip) ? event.clientX - widthTooltip : event.clientX;
				const clientY = heightScreen < (event.clientY + heightTooltip) ? event.clientY - heightTooltip : event.clientY;
				this.tooltipTimesheet.config = {
					isVisible: true,
					x: clientX,
					y: clientY + 10,
					viewMode: 'today'
				}
				let history = {};
				const checkIn = record.check_in_time ? moment(record.check_in_time).format("HH:mm") : null;
				const checkOut = record.check_out_time ? moment(record.check_out_time).format("HH:mm") : 'No Data';
				const data = {
					checkInOut: checkIn === null && checkOut === null ? 'No Data' : checkIn + " - " + checkOut,
				};
				if (record.history) {
					history = record.history.history.reduce((a, b) => (a.modified_date > b.modified_date ? a : b));
					data.edited = history.old_value + " -> " + history.new_value + " (" + moment(history.modified_date).format("HH:mm") + ")";
					data.editor = history.modifier.name + " - " + history.modifier.department;
					data.comment = history.comments;
					data.totalEditedTime = record.history.history.length
				}
				if (record.requests) {
					let request = '';
					record.requests.forEach(req => {
						request += req.request_type_name_en ? req.request_type_name_en : req.reason_type_name_vi + '&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp' + req.request_status + '<br/>';
					});
					data.request = request;
				}
				this.tooltipTimesheet.dataDetail = data;
			}
		},

		modifyCell(col, record) {
			if (col.dataIndex !== 'staff' && col.dataIndex !== 'role' && !col.isHolyday &&
				(this.timesheetInfo.status === this.statusTimesheet.draft || this.timesheetInfo.status === this.statusTimesheet.pendingUpdate)
			) {
				this.titleModal = 'Modify Cell'
				this.modifyKey = 'cell'
				this.visibleModal = true;
				this.formUpdate.employee = record.employmentID;
				this.formUpdate.isAbnormalCase = false;
				this.formUpdate.date = [moment(col.key), moment(col.key)];
				this.formUpdate.commentReason = record.commentReason;
			}
		},

		outside() {
			if (!this.loadingTotalTimesheet && this.tooltipTimesheet.config.viewMode === "total") {
				this.tooltipTimesheet.config.isVisible = false;
			}
		},

		handleChangeSort(pagination, filters, sorter) {
			// const config = {...this.configPage}
			this.configPage.direction = sorter.order ? sorter.order.split("end")[0] : "asc";
			this.onChangeConfigPage(this.configPage);
		},

		validRequired(value) {
			return !value ? false : true;
		},
	},
	updated() {
		Array.from(document.getElementsByClassName('no-data-col')).forEach((el) => el.classList.remove('no-data-col'));
		let element = document.getElementsByClassName('edited');
		for (let elm of element) {
			elm.parentElement.parentElement.classList.add("edited-col")
		}
		let elmNoData = document.getElementsByClassName('no-data');
		for (let elm of elmNoData) {
			if (!elm.parentElement.parentElement.classList.contains("bg-gray")) {
				elm.parentElement.parentElement.classList.add("no-data-col")
			}
		}
	},
}