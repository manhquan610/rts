
import moment from 'moment';
import http from "../../../store/modules/helpers/httpInterceptor";
import Vue from "vue";
import { mapState } from "vuex";

Vue.filter('formatDateComment', function (value) {
  if (value) {
    return moment(String(value)).format('MMM DD, YYYY HH:mm')
  }
});

export default {
  props: ["timesheetId"],
  data() {
    return {
      comment: "",
      isValidComment: true,
      commentsData: [],
      cacheData: []
    };
  },
  computed: {
    ...mapState({
      userInfo: state => state.user.userInfo
    })
  },
  methods: {
    getCommentName(comment) {
      const last_modified_by = comment.last_modified_by;
      const created_by = comment.created_by;
      if (last_modified_by) {
        return last_modified_by.name;
      } else {
        return created_by ? created_by.name : "";
      }
    },
    handleDeleteComment(comment) {
      http.delete(`tms/timesheets/${this.timesheetId}/comments/${comment.comment_id}`)
        .then(() => {
          this.getCommentsTimesheet(this.timesheetId);
        })
        .catch((error) => {
          throw error;
        });
    },
    handleChangeComment(e) {
      let value = e.target.value;
      this.isValidComment = value.length !== 0;
    },
    handleAddComment() {
      this.isValidComment = this.comment.length !== 0;
      if (this.isValidComment) {
        const data = { content: this.comment };
        http.post(`tms/timesheets/${this.timesheetId}/comments`, data)
          .then(() => {
            this.comment = "";
            this.getCommentsTimesheet(this.timesheetId);
          })
          .catch((error) => {
            this.$message.error(error.response.data.error,5);
          });
      }
    },
    handleEditComment(id) {
      const newData = [...this.commentsData];
      const target = newData.filter(item => id === item.comment_id)[0];
      if (target) {
        target.editable = true;
        this.commentsData = newData;
      }
    },
    handleUpdateComment(comment) {
      const params = {
        comment_id: comment.comment_id,
        content: comment.content
      };
      if (this.isValidComment) {
        http.put(`tms/timesheets/${this.timesheetId}/comments`, params)
          .then(() => {
            this.handleCancelUpdateComment(comment.comment_id);
            this.getCommentsTimesheet(this.timesheetId);
          })
          .catch((error) => {
            this.$message.error(error.response.data.error,5);
          });
      }
    },
    handleCancelUpdateComment(id) {
      const newData = [...this.commentsData];
      const target = newData.filter(item => id === item.comment_id)[0];
      if (target) {
        Object.assign(
          target,
          this.cacheData.filter(item => id === item.comment_id)[0]
        );
        delete target.editable;
        this.commentsData = newData;
      }
    },
    getCommentsTimesheet() {
      http.get(`tms/timesheets/${this.timesheetId}/comments`).then(res => {
        this.commentsData = res.data.data;
        this.cacheData = res.data.data.map(item => ({ ...item }));
        this.loading = false;
      }).catch(err => {
        this.$message.error(err.response.data.error,5);
      });
    }
  },
  created() {
    this.getCommentsTimesheet();
  },
};
