import StaffTimesheetDetail from "../StaffTimesheetDetail/StaffTimesheetDetail.vue";
import Vue from "vue";
import { mapState } from "vuex";
import moment from "moment";
import { convertAvatarName,getStatusTimeSheet } from "../../../components/commons/commons";

Vue.filter('formatDate', function (value) {
	if (value) {
		return moment(String(value)).format('DD/MM/YYYY')
	}
});

Vue.filter('formatMonth', function (value) {
	if (value) {
		return moment(String(value)).format('MM/YYYY')
	}
});

export default {
	name: 'timesheet-View-staff',
	props: ["timesheetUser", "timesheetInfo"],
	components: {
		StaffTimesheetDetail
	},
	data() {
		return {
			rowsTable: [],
			dataSource: {},
			columnsTableDetailOfDays: [
				{
					title: 'No',
					dataIndex: 'no',
					width: 50,
					render: (row, data, index) => { return index + 1 }

				},
				{
					title: 'Date',
					dataIndex: 'date_of_issue',
					// scopedSlots: { customRender: 'date' },
					render: (row) => {
						return row.value
					}

				},
				{
					title: 'Shift',
					dataIndex: 'shift',
					render: (row, data) => { 
						let working_start_time = data && data.workStartTime ? moment(data.workStartTime).format("HH:mm ") : ''
						let working_end_time = data && data.workEndTime ? moment(data.workEndTime).format("HH:mm") : ''
						return  working_start_time + ' - ' + working_end_time 
					}

				},
				{
					title: 'Checkin time',
					dataIndex: 'check_in_time',
					// scopedSlots: { customRender: 'date' },
					render: (row, data) => { return data && data.check_in_time ? this.formatTime(data.check_in_time) : '' }

				},
				{
					title: 'Checkout time',
					dataIndex: 'check_out_time',
					// scopedSlots: { customRender: 'date' },
					render: (row, data) => { return data && data.check_out_time ? this.formatTime(data.check_out_time) : '' }

				},
				{
					title: 'Working day',
					dataIndex: 'value',
					width: 100
				}
			],

			tooltipTimesheet: {
				data: [
					{ label: 'Check in-out time', key: 'checkInOut' },
					{ label: 'Request', key: 'request' },
					{ label: 'Edited', key: 'edited' },
					{ label: 'Editor', key: 'editor' },
					{ label: 'Total Edited Time', key: 'totalEditedTime' },
					{ label: 'Active Time', key: 'activeTime' },
				],
				config: {
					isVisible: false,
					x: 0, y: 0
				}
			},

			detailMonthOfStaff: [
				{
					title: 'Total working days',
					description: 'Total working days',
					key: 'total_working_days'
				},
				{
					title: 'Total paid leave days',
					description: 'Total paid leave days',
					key: 'total_paid_leave_days'
				},
				{
					title: 'Total paid benefits leave days',
					description: 'Total paid benefits leave days',
					key: 'total_paid_benefits_leave_days'
				},
				{
					title: 'Birthday leave',
					description: 'Birthday leave',
					key: 'birthday_leave'
				},
				{
					title: 'Total onsite days',
					description: 'Total onsite days',
					key: 'total_onsite_days'
				},
				{
					title: 'Total public holidays',
					description: 'Total public holidays',
					key: 'total_public_holidays'
				},
				{
					title: 'Total offset leave days',
					description: 'Total offset leave days',
					key: 'total_offset_leave_days'
				},
				{
					title: 'Total leave days that social insurance covers',
					description: 'Total leave days that social insurance covers',
					key: 'total_days_off_social_insurance_covers'
				},
				{
					title: 'Total unpaid leave days',
					description: 'Total unpaid leave days',
					key: 'total_unpaid_leave_days'
				},
				{
					title: 'Total paid days',
					description: 'Total paid days',
					key: 'total_paid_days'
				},
				{
					title: 'Total paid probationary days',
					description: 'Total paid probationary days',
					key: 'total_paid_probationary_days'
				},
				{
					title: 'Total paid official days',
					description: 'Total paid official days',
					key: 'total_paid_official_days'
				}
			],
			dayOffByStaff: [
				{
					title: 'Total authorized leave days',
					description: 'Total authorized leave days',
					key: 'total_annual_leave'
				},
				{
					title: 'Beginning of period',
					description: 'Beginning of period',
					key: 'annual_leave_beginning_of_period'
				},
				{
					title: 'Used in period',
					description: 'Số ngày phép dùng trong kì',
					key: 'annual_leave_days_used_in_period'
				},
				{
					title: 'End of period',
					description: 'End of period',
					key: 'remaining_paid_leave_days_of_year'
				},
			],
			infringeData: [
				{
					title: 'Approved late comings and early leavings',
					description: 'Approved late comings and early leavings',
					key: 'approved_late_coming_and_early_leaving'
				},
				{
					title: 'Unapproved late comings and early leavings',
					description: 'Unapproved late comings and early leavings',
					key: 'unapproved_late_coming_and_early_leaving'
				},
				{
					title: 'Approved “missed attendance-checks”',
					description: 'Approved “missed attendance-checks”',
					key: 'approved_forget_taking_attendance'
				},
				{
					title: '“Missed attendance-check” days counted as working days',
					description: '“Missed attendance-check” days counted as working days',
					key: 'days_forget_taking_attendance_counted_as_working_day'
				},
				{
					title: '“Missed attendance-check” days counted as half working days',
					description: '“Missed attendance-check” days counted as half working days',
					key: 'days_forget_taking_attendance_counted_as_half_working_day'
				},
				{
					title: '“Missed attendance-check” days counted as unpaid leave days',
					description: '“Missed attendance-check” days counted as unpaid leave days',
					key: 'days_forget_taking_attendance_counted_as_without_working_day'
				},
				{
					title: 'Late coming, early leaving, “Missed attendance-check” days',
					description: 'Late coming, early leaving, “Missed attendance-check” days',
					key: 'days_come_late_leave_early_forget_taking_attendance'
				},
				{
					title: 'Total number of violations',
					description: 'Total number of violations',
					key: 'total_number_of_violations'
				},
				{
					title: 'Total number of violations that exceeds fine level',
					description: 'Total number of violations that exceeds fine level',
					key: 'total_number_of_violations_exceed'
				},
			],
			colorAvatar: localStorage.getItem('color_avatar')
		}
	},
	computed: {
		...mapState({
			notesData: state => state.timesheet.noteList,
			calculationWorking: state => state.timesheet.calculationWorking,
			violationWorking: state => state.timesheet.countWokingMyTS,
			annualLeave: state => state.timesheet.countWokingMyTS,
		})
		
	},
	mounted() {
		this.renderRowTable();
	},
	watch: {
		timesheetInfo: function (newVal, oldVal) {
			if (newVal.timesheet_id !== oldVal.timesheet_id) {
				this.renderRowTable();
			}
		},
		timesheetUser: function () {
			this.timesheetUser.timesheets.forEach(element => {
				let key = moment(element.date_of_issue).format("YYYYMMDD");
				this.timesheetUser[key] = element;
			});
		}
	},
	methods: {
		renderRowTable() {
			let rows = [];
			let startDate = moment(this.timesheetInfo.from_date);
			let endDate = moment(this.timesheetInfo.to_date);
			while (startDate.isSameOrBefore(endDate)) {
				let day = moment(startDate).day()
				let className;
				className = (day === 0 || day === 6) ? 'bg-holiday' : ""
				rows.push({
					dataIndex: moment(startDate).format("YYYYMMDD"),
					class: className,
					key: moment(startDate).format("YYYYMMDD"),
					value: moment(startDate).format("DD/MM/YYYY")
					//add here !!!!
				})
				startDate = startDate.add(1, 'day');
			}
			this.rowsTable = rows;
		},
		getStatus() {
            return getStatusTimeSheet(this.timesheetInfo.status);
        },
		handleHoverTimeSheet(event, timesheet) {
			event.preventDefault();
			if (timesheet && timesheet.value) {
				const widthScreen = window.innerWidth;
				const heightScreen = window.innerHeight;
				const widthTooltip = 350;
				const heightTooltip = document.getElementById('tooltip-timesheet').offsetHeight;
				const clientX = widthScreen < (event.clientX + widthTooltip) ? event.clientX - widthTooltip : event.clientX;
				const clientY = heightScreen < (event.clientY + heightTooltip) ? event.clientY - heightTooltip : event.clientY;
				this.tooltipTimesheet.config = {
					isVisible: true,
					x: clientX,
					y: clientY + 10,
					viewMode: 'today'
				}
				let history = {};
				const checkIn = timesheet.check_in_time ? moment(timesheet.check_in_time).format("HH:mm") : null;
				const checkOut = timesheet.check_out_time ? moment(timesheet.check_out_time).format("HH:mm") : 'No Data';
				const data = {
					checkInOut: checkIn === null && checkOut === null ? 'No Data' : checkIn + " - " + checkOut,
				};
				if (timesheet.history) {
					history = timesheet.history.history.reduce((a, b) => (a.modified_date > b.modified_date ? a : b));
					data.edited = history.old_value + " -> " + history.new_value + " (" + moment(history.modified_date).format("HH:mm") + ")";
					data.editor = history.modifier.name + " - " + history.modifier.department;
					data.totalEditedTime = timesheet.history.history.length
				}
				if (timesheet.requests) {
					let request = '';
					timesheet.requests.forEach(req => {
						request += req.request_type_name_en ? req.request_type_name_en : req.reason_type_name_vi + '&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp' + req.request_status + '<br/>';
					});
					data.request = request;
				}
				this.tooltipTimesheet.dataDetail = data;
			}
		},
		getAvatarName(fullName) {
			return convertAvatarName(fullName);
		},
		formatTime(dateTime) {
			if (dateTime && dateTime !== null && dateTime !== "" && moment(dateTime).format("HH:mm") !== 'Invalid date') {
				return moment(dateTime).format("HH:mm")
			} else return null;
		},
		helper(data, type) {
			if (data) {
				if (type === 'check-in') {
					return data.is_late_coming
				} else if (type === 'check-out') {
					return data.is_early_leaving
				}
			}
			return false;
		}
	}

}