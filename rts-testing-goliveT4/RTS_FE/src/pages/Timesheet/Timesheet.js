import FilterLayout from "../../components/layouts/FilterLayout";
import TimesheetViewHR from "./ViewHR/TimesheetViewHR.vue";
import StaffTimesheetDetail from "./StaffTimesheetDetail/StaffTimesheetDetail.vue";
import TimesheetViewUser from "./ViewUser/TimesheetViewUser.vue";
import CommentsTimesheet from "./ViewComments/CommentsTimesheet.vue";
import { mapState } from "vuex";
import moment from 'moment';
import { convertObjectToParams, getStatusTimeSheet, downloadFile, getUnique, scaleScreen } from "../../components/commons/commons";
import { statusTimesheet, Role } from '../../components/commons/commonConstant';
import http from '../../store/modules/helpers/httpInterceptor';
// import httpExport from '../../store/modules/helpers/httpExportTimesheetInterceptor';

const column = [{
    title: "Staff",
    dataIndex: "staff",
    key: "staff",
    scopedSlots: { customRender: "staff" },
}];

export default {
    name: "Timesheet",
    components: {
        FilterLayout,
        TimesheetViewHR,
        StaffTimesheetDetail,
        TimesheetViewUser,
        CommentsTimesheet
    },

    data() {
        return {
            column,
            page: 'Timesheet',
            showDrawer: false,
            selectedRowKeys: [],
            loading: false,
            titleDrawer: "",
            drawerKey: undefined,
            dataSource: [],
            configPage: {
                pageSize: 20,
                currentPage: 1,
                totalElement: 0,
                sort: 'name',
                direction: 'asc'
            },
            dataFilters: {},
            timesheetInfo: {},
            statusTimesheet,
            visibleModal: false,
            visibleConfirm: false,
            visibleUpdateJob: false,
            commentReject: '',
            isValidComment: true,
            keyTab: 'list',
            role: Role,
            value: 1,
            isExcept: false,
            fetching: false,
            staffListExcept: [],
            haveRowSelect: false,
            firstCreated: true,
            staffSelectedListExcept: [],
            selectOptionData: [],
            tabChange: false,
            formUpdate: this.$form.createForm(this, {name:'updateHistory'})
        };
    },
    watch: {
        showDrawer: function() {
            this.myFunction();
        },
        staffListExcept: function() {
            this.onChangeValue()
        },
        keyTab: function() {
            this.tabChange = true
        }
    },
    created() {
        this.scaleScreen()
        this.$store.dispatch("timesheet/getNotes");
        this.dataFilters = {...this.dataFilters, ...JSON.parse(sessionStorage.getItem(this.page)) }
        this.configPage = {
            pageSize: JSON.parse(sessionStorage.getItem(this.page)) ?  (JSON.parse(sessionStorage.getItem(this.page)).size ? JSON.parse(sessionStorage.getItem(this.page)).size : 20):20,
            currentPage: JSON.parse(sessionStorage.getItem(this.page)) ? (JSON.parse(sessionStorage.getItem(this.page)).page ? JSON.parse(sessionStorage.getItem(this.page)).page : 1) : 1,
            totalElement: 0,
            sort: JSON.parse(sessionStorage.getItem(this.page)) ? (JSON.parse(sessionStorage.getItem(this.page)).sort ? JSON.parse(sessionStorage.getItem(this.page)).sort : 'name') : 'name',
            // direction: JSON.parse(sessionStorage.getItem(this.page)).direction ? JSON.parse(sessionStorage.getItem(this.page)).direction : 'asc'
        }
    },
    computed: {
        ...mapState({
            userInfo: state => state.user.userInfo,
            roles: state => state.user.userInfo.roles ? state.user.userInfo.roles : "",
            timesheetUser: (state) => state.timesheet.timesheetUser ? state.timesheet.timesheetUser : {},
            calculationWorking: (state) => state.timesheet.calculationWorking,
            violationWorking: (state) => state.timesheet.countWokingMyTS,
            annualLeave: (state) => state.timesheet.countWokingMyTS,
            employeeList: state => state.application.employeeList,
        }),
    },
    updated() {},
    props: {
        msg: String
    },
    mounted() {
        this.keyTab = this.roles.includes(this.role.ADMIN) || this.roles.includes(this.role.HR) ? 'list' : 'detail'
    },
    methods: {
        scaleScreen,
        // ...mapActions(["getTimeSheetDrawerData"]), 
        onSetDataFilters(filter) {
            filter.payroll_period = this.dataFilters.payroll_period ? this.dataFilters.payroll_period : filter.payroll_period
        },

        filterOption(input, option) {
            return (
                option.componentOptions.children[0].text.toLowerCase().indexOf(input.toLowerCase()) >= 0
            );
        },

        onSearchStaff(value) {
            let cloneVal = decodeURI(value)
            let cloneSelectOptionData = []
            cloneSelectOptionData = this.selectOptionData;
            if (value && value !== '' && value.length >= 3) {
                this.fetching = true;
                clearTimeout(this.timeout);
                this.timeout = setTimeout(() => {
                    this.$store.dispatch("application/getEmployeeList", cloneVal).then((res) => {
                        cloneSelectOptionData = [
                            ...cloneSelectOptionData,
                            ...res
                        ];
                        this.selectOptionData = getUnique(cloneSelectOptionData, 'id');
                        this.fetching = false;
                    }).catch(() => {
                        this.fetching = false;
                    });
                }, 1000);
            }
        },

        onChangeValue(value) {
            let newStaffList = this.selectOptionData.filter(a => value.includes(a.id));
            this.staffListExcept = newStaffList;
            this.staffSelectedListExcept = value
        },

        onDelete(key) {
            let cloneStaffListExcept = this.staffListExcept.filter(item => item.id !== key);
            let staffSelectedListExcept = [];
            staffSelectedListExcept = cloneStaffListExcept.map(item => {
                return item.id;
            });
            this.staffListExcept = cloneStaffListExcept;
            this.staffSelectedListExcept = staffSelectedListExcept;
        },

        onApplyFilters(filters) {
            this.dataFilters = filters;
            if (!this.firstCreated && !this.tabChange) {
                this.configPage.currentPage = 1
            }
            this.getTimesheetDetail(filters.payroll_period);
            if (this.keyTab !== 'list') {
                this.getUserTimesheetDetail(this.userInfo.userId);
            } else {
                this.getTimesheetData();
            }
            this.tabChange = false
        },

        onSelectChange(selectedRowKeys) {
            this.selectedRowKeys = selectedRowKeys;
            this.haveRowSelect = (selectedRowKeys.length > 0) ? true : false
        },

        exportTimesheet() {
            const data = {
                location: this.dataFilters.location ? this.dataFilters.location : [],
                employee: this.dataFilters.employee ? this.dataFilters.employee : [],
                department: this.dataFilters.department ? this.dataFilters.department : [],
                request_type: this.dataFilters.request_type ? this.dataFilters.request_type : [],
                action_abnormal_case: this.dataFilters.abnormal_case ? this.dataFilters.abnormal_case : false,
                employee_id: this.selectedRowKeys,
                except_employee: []
            };
            this.$store.dispatch('user/loadingRequest');
            http.post(`tms/excel/${this.dataFilters.payroll_period}/time-sheet`, data, { responseType: 'blob' }).then(res => {
                if (res.status === 200 && res.data.type !== 'application/json') {
                    downloadFile(res.data, `Timesheet_List_T${moment(this.timesheetInfo.to_date).format('MM-yyyy')}.xlsx`);
                    this.$store.dispatch('user/loadingRequest');
                    this.$message.success('Timesheet is exported successfully!',5);
                } else {
                    this.$store.dispatch('user/loadingRequest');
                    this.$message.error("No data export",5);
                }
            }).catch(err => {
                this.$store.dispatch('user/loadingRequest');
                if (err.response.data.error) {
                    this.$message.error(err.response.data.error,5);
                } else {
                    this.$message.error("Server Time Out!",5);
                }
            })
        },

        exportTimesheetDetail() {
            if (this.selectedRowKeys.length > 0) {
                const data = {
                    employeeId: this.selectedRowKeys,
                    timesheetId: this.dataFilters.payroll_period
                };
                this.$store.dispatch('user/loadingRequest');
                http.post('tms/excel/zip', data, { responseType: 'blob' }).then(res => {
                    if (res.status === 200 && res.data.type !== 'application/json') {
                        this.$store.dispatch('user/loadingRequest');
                        downloadFile(res.data, `Timesheet_Detail_T${moment(this.timesheetInfo.to_date).format('MM-yyyy')}.zip`);
                        this.$message.success('Timesheet detail is exported successfully!',5);
                    } else {
                        this.$store.dispatch('user/loadingRequest');
                        this.$message.error("No data export",5);
                    }
                }).catch(err => {
                    this.$store.dispatch('user/loadingRequest');
                    if (err.response.data.error) {
                        this.$message.error(err.response.data.error);
                    } else {
                        this.$message.error("Server Time Out!",5);
                    }
                })
            }
        },

        cancelConfirm() {
            this.visibleConfirm = false;
            this.value = 1
        },

        handleUpdateHistory(e) {
            e.preventDefault();
            this.$message.loading('Please wait... !',4)
            this.formUpdate.validateFields((err, values) => {
                if(!err) {
                    let sf4cId = []
                    values.staff.forEach(elemnt => {
                        sf4cId.push(JSON.parse(elemnt).sf4c_id)
                    })
                    let params = {sf4cId}
                    this.$store.dispatch("timesheet/updateHistory",params).then((res) => {
                        if(res && res.data) {
                            this.$message.success('Update Successfully')
                            this.visibleUpdateJob = false
                            this.formUpdate.resetFields()
                        }
                    }).catch((err) => {
                        this.$message.error(err)
                    })
                }
            })
        },
        handleCancel() {
            this.formUpdate.resetFields()
            this.visibleUpdateJob = false;
        },
        sendEmail() {
            this.$store.dispatch('user/loadingRequest');
            const data = {
                location: this.dataFilters.location ? this.dataFilters.location : [],
                employee: this.dataFilters.employee ? this.dataFilters.employee : [],
                department: this.dataFilters.department ? this.dataFilters.department : [],
                request_type: this.dataFilters.request_type ? this.dataFilters.request_type : [],
                action_abnormal_case: this.dataFilters.abnormal_case ? this.dataFilters.abnormal_case : false,
                employee_id: this.selectedRowKeys,
                except_employee: this.staffSelectedListExcept
            }
            http.post(`/tms/timesheets/${this.dataFilters.payroll_period}/send-email`, data).then(() => {
                this.$store.dispatch('user/loadingRequest');
                this.$message.success('The email has been sent successfully!',5);
            }).catch(err => {
                this.$store.dispatch('user/loadingRequest');
                this.$message.error(err.response.data.error,5);
            })
            this.visibleConfirm = false;
        },

        getTimesheetDetail(timesheetId) {
            http.get(`/tms/timesheets/${timesheetId}`).then(res => {
                this.timesheetInfo = res.data.data;
            });
        },

        getTimesheetData() {
            const { pageSize, currentPage, sort, direction } = this.configPage;
            this.$store.dispatch('user/loadingRequest');
            const paramFilter = {
                location: this.dataFilters.location ? this.dataFilters.location : [],
                department: this.dataFilters.department ? this.dataFilters.department : [],
                role: this.dataFilters.role ? this.dataFilters.role : [],
                employee: this.dataFilters.employee ? this.dataFilters.employee : [],
                size: pageSize,
                page: currentPage,
                sort,
                direction,
                actionAbnormalCase: this.dataFilters.abnormal_case ? this.dataFilters.abnormal_case : false,
                request_type: this.dataFilters.request_type ? this.dataFilters.request_type : [],
            };
            const params = {
                timesheetId: this.dataFilters.payroll_period,
                paramFilter: convertObjectToParams(paramFilter)
            }
            this.$store.dispatch("timesheet/getAllUserTimesheet", params).then(res => {
                if (res && res.data) {
                    let data = res.data.map((item) => {
                        item.key = item.id;
                        item.staff = item.name;
                        item.employmentID = item.id;
                        if (item.timesheets) {
                            item.timesheets.forEach(element => {
                                let key = moment(element.date_of_issue).format("YYYYMMDD");
                                item[key] = element;
                            });
                        }
                        return item;
                    })
                    this.configPage.totalElement = res.page.total_element;
                    this.$set(this.$data, "dataSource", data);
                    this.firstCreated = false
                }
                this.$store.dispatch('user/loadingRequest');
                sessionStorage.setItem(this.page, JSON.stringify({ payroll_period: this.dataFilters.payroll_period, ...paramFilter }))
            }).catch(() => {
                this.$message.error('Fail to get Timesheet Data',5);
                this.$store.dispatch('user/loadingRequest');
            });
        },

        getUserTimesheetDetail(employmentID) {
            const params = {
                employmentID: employmentID,
                timesheetId: this.dataFilters.payroll_period,
            }
            this.$store.dispatch('user/loadingRequest');
            this.$store.dispatch("timesheet/getUserTimesheet", params).then(() => this.$store.dispatch('user/loadingRequest')).catch(() => {
                this.$message.error('Fail to get User Timesheet Data',5);
                this.$store.dispatch('user/loadingRequest');
            });
            this.$store.dispatch("timesheet/getCalculationWorkingEmployee", params).catch(() => {
                this.$message.error('Fail to get User Working Data',5);
            });
            this.$store.dispatch("timesheet/getCountWorkingMyTS", params).catch(() => {
                this.$message.error('Fail to get User Violation Data',5);
            });
            this.$store.dispatch("timesheet/getCountWorkingMyTS", params).catch(() => {
                this.$message.error('Fail to get User Annual Leave Data',5);
            });
            sessionStorage.setItem(this.page, JSON.stringify({
                employmentID: employmentID,
                payroll_period: this.dataFilters.payroll_period
            }))
        },

        onShowDrawerTimesheetDetail(record) {
            this.showDrawer = true;
            this.drawerKey = "detail";
            this.titleDrawer = "Timesheet Detail";
            this.getUserTimesheetDetail(record.employmentID);
        },

        onShowDrawerNote() {
            this.showDrawer = true;
            this.drawerKey = "note";
            this.titleDrawer = "Note";
        },

        onCloseDrawer() {
            this.showDrawer = false;
            this.drawerKey = undefined;
            this.titleDrawer = "";
        },

        onShowDrawerComment() {
            this.showDrawer = true;
            this.drawerKey = "comment";
            this.titleDrawer = "All comment";
        },

        onshowModal() {
            this.visibleUpdateJob = true
        },
        
        onChangeConfigPage(config) {
            this.configPage.pageSize = config.pageSize;
            this.configPage.currentPage = config.currentPage;
            this.configPage.direction = config.direction;
            this.getTimesheetData();
        },
        getStatus() {
            return getStatusTimeSheet(this.timesheetInfo.status);
        },

        sendRequestReviewTimesheet() {
            const data = { status: this.statusTimesheet.pendingApproval };
            http.put(`/tms/timesheets/${this.timesheetInfo.timesheet_id}/request-review`, data).then(() => {
                this.getTimesheetDetail(this.timesheetInfo.timesheet_id);
                this.getTimesheetData();
            }).catch(err => {
                this.$message.error(err.response.data.error,5);
            })
        },

        sendReviewTimesheet(status, content) {
            const data = { status };
            if (content) {
                data.content = content;
            }
            http.put(`/tms/timesheets/${this.timesheetInfo.timesheet_id}/review`, data).then(() => {
                this.getTimesheetDetail(this.timesheetInfo.timesheet_id);
                this.getTimesheetData();
            }).catch(err => {
                this.$message.error(err.response.data.error,5);
            });
            this.closeModal();
        },

        sendRequestRejectTimesheet() {
            if (!this.commentReject) {
                this.isValidComment = false;
                return;
            }
            this.sendReviewTimesheet(statusTimesheet.reject, this.commentReject);
        },

        validRequired(value) {
            return !value ? false : true;
        },

        myFunction() {
            document.body.style.position = "fixed"
        },

        closeModal() {
            this.visibleModal = false;
            this.commentReject = '';
            this.isValidComment = true;
        },

        handleChangeTab(key) {
            this.keyTab = key;
            if (key === 'detail') {
                this.page = 'TimesheetUser'
                this.dataFilters = {...this.dataFilters, ...JSON.parse(sessionStorage.getItem(this.page)) }
            } else {
                this.page = 'Timesheet'
                this.dataFilters = {...this.dataFilters, ...JSON.parse(sessionStorage.getItem(this.page)) }
            }

        },

        checkSendRequestReview() {
            return (this.timesheetInfo.status === statusTimesheet.draft || this.timesheetInfo.status === statusTimesheet.pendingUpdate) && moment(this.timesheetInfo.to_date) < moment()
        }
    }
};