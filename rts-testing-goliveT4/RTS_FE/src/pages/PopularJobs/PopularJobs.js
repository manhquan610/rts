import PopularJob from '../../components/PopularJob/PopularJob.vue';
import VuePagination from '../../components/base/VuePagination.vue';
import {mapState} from 'vuex';

export default {
    name: "PopularJobs",
    components: {
        PopularJob,
        VuePagination
    },
    props: {},
    created() {
        this.$store.dispatch('rtsPopularJobs/getListPopularJob')
    },
    computed: {
        ...mapState({
            listPopularJob: state => state.rtsPopularJobs.listPopularJob
        })
    },
    methods: {
        onChangePage() {
        },
        onClick(item) {
            this.$emit('onClickItem',item?.id)
        }
    }
}