import { mapState } from "vuex";
import { statusText } from "../../../components/commons/commonConstant";
import moment from "moment";
import http from "../../../store/modules/helpers/httpInterceptor";
import { downloadFile} from "../../../components/commons/commons";


export default {
    name: "ot-detail",
    props: {
        data: Object,
        otType: Object,
        fileList : {type: Array},
        isAvaiableToDownLoad: Boolean,
        disableWithDraw: { type: Function },
        disableApproveAll: { type: Function },
        disableRejectAll: { type: Function },
        checkOTDateVsLastApprovedTimeSheet:{type : Function},
        checkOTDateVsCurrentDate:{type : Function},
        disableEdit: { type: Function },
        columns: { type: Array },
        userInfo: Object,
        assignee: Object,
        statusColor: Object,
        getOTRequestList: {
            type: Function,
        },
        status_CB: Object,
        status_department: Object,
        formatDateTime: {
            type: Function,
        },
        formatDate: {
            type: Function,
        },
        formatTime: {
            type: Function,
        },
        disableMoreButton: {
            type: Function
        },
        Role: Object
    },

    data() {
        return {
            formItemLayout: {
                labelCol: { span: 7 },
                wrapperCol: { span: 17 },
            },
            offSetY: 0,
            isSingleRecord: false,
            statusText,
            dataSource: this.data,
            visible: false,
            visibleComment: false,
            visibleDelegate: false,
            visibleModalReject: false,
            visibleModalWithdraw: false,
            visibleModalApprove: false,
            visibleModalApproveCheck: false,
            tooltipOnsite: { config: { visible: false } },
            reasonReject: '',
            reasonWithdraw: '',
            comment: '',
            reasonRejectAll: this.data.reject_all_reason ? this.data.reject_all_reason : '',
            delegator: null,
            visibleDrop: false,
            user_name: '',
            currentDate: moment(),
            isAssignee: this.checkAssignee(this.userInfo.userName, this.user_name)
            , checkForUpdateStatus: false,
            checkReject: false,
            statusPayload: {},
            request_type: 'overtime'

        }
    },
    beforeCreate() {
        this.form = this.$form.createForm(this, { name: 'ot-detail' });
    },
    created() {
        this.dataSource.receiver = this.data.assignee !== null ? `${this.data.assignee_ob.name} - ${this.data.assignee_ob.department_name.name}` : 'Guest-manager'
        this.user_name = this.data.assignee_ob ? this.data.assignee_ob.user_name : ''
        this.isAssignee = this.checkAssignee(this.userInfo.userName, this.user_name)
    },
    watch: {
        data: function () {
            this.dataSource = this.data
            this.user_name = this.data.assignee_ob ? this.data.assignee_ob.user_name : ''
            this.isAssignee = this.checkAssignee(this.userInfo.userName, this.user_name)
            this.dataSource.receiver = this.data.assignee !== null ? `${this.data.assignee_ob.name} - ${this.data.assignee_ob.department_name.name}` : 'Guest-manager'
        },
        user_name: function () {
            this.isAssignee = this.checkAssignee(this.userInfo.userName, this.user_name)
        }
    },
    computed: {
        ...mapState({
            roles: state => state.user.userInfo.roles ? state.user.userInfo.roles : "",
            employeeList: state => state.application.employeeList,
        })
    },
    methods: {
        checkAssignee(username, data) {
            return username === data
        },
        onSearchDelegator(value) {
            let cloneVal = decodeURI(value);
            if (value && value !== "" && value.length >= 3) {
                this.fetching = true;
                clearTimeout(this.timeout);
                this.timeout = setTimeout(() => {
                    this.$store
                        .dispatch("application/getEmployeeList", cloneVal)
                        .then(() => {
                            this.fetching = false;
                        })
                        .catch(() => {
                            this.fetching = false;
                        });
                }, 1000);
            }
        },
        handleHover(event) {
            event.preventDefault();
        },
        handleComment() {
            this.visibleComment = true;
        },
        handleCancel() {
            this.visibleComment = false;
            this.visibleModalApprove = false;
            this.tooltipOnsite.config.visible = false;
        },
        handleCancelReject(){
            this.reasonRejectAll = this.data.reject_all_reason ? this.data.reject_all_reason : ''
            this.reasonReject = ''
            this.visibleModalReject = false;
        },
        handleCancelWithdraw() {
            this.reasonWithdraw = this.data.reasonWithdraw ? this.data.reasonWithdraw : ''
            this.visibleModalWithdraw = false;
        },
        handleDelegate() {
            this.visibleDelegate = !this.visibleDelegate;
        },
        handCancelDelegate() {
            this.delegator = null;
            this.visibleDelegate = false;
        },
        handleSaveDelegate() {
            let payload = {
                otRequestId: this.data.id,
                data: { assignee: JSON.parse(this.delegator).id }
            }

            this.$store.dispatch('overtime/updateAssigneeOTRequest', payload).then(res => {
                if (res) {
                    if (parseInt(res.code) === 0) {
                        this.dataSource.receiver = JSON.parse(this.delegator).label + ' - ' + JSON.parse(this.delegator).department
                        this.$message.success('Update assignee successfully!',5);
                        this.getOTRequestList()
                        this.dataSource = this.data
                        this.user_name = JSON.parse(this.delegator).user_name
                        this.visibleDelegate = false;
                    } else {
                        this.$message.error(res.message,5);
                    }
                }
            }).catch(() => {
                this.$message.error('Failed to update status!',5);
            });

        },

        showPopConfirm() {
            this.tooltipOnsite.config.visible = false;
            this.visibleModalApprove = true;
        },
        showPopConfirmCheck(event, record) {
            let temp = window.scrollY
            if (!this.tooltipOnsite.config.visible) {
                this.visibleModalApprove = false;
                this.statusPayload = record
                this.tooltipOnsite.config = {
                    visible: true,
                    top: (event.pageY + 10 - temp) + 'px',
                    left: (event.pageX - window.outerWidth * 0.65 + 15) + 'px',
                }
            }
        },
        confirm() {
            this.onClickApprove(this.statusPayload)
            this.tooltipOnsite.config.visible = false;
        },

        updateStatus(payload) {
            this.$store.dispatch('overtime/updateStatusOTRequest', payload.payload).then(res => {
                if (res.code) {
                    if (parseInt(res.code) === 0) {
                        this.$message.success('Update status successfully!');
                        if (payload.dataWouldChange.type === 'all') {
                            payload.dataWouldChange.additionalInformation.forEach(element => {
                                this.dataSource.staff[element.index].status = element.status
                                this.dataSource.reject_all_reason = element.reason
                            });
                        } else if (payload.dataWouldChange.type === 'single') {
                            this.dataSource.staff.forEach(element => {
                                if (element.employee_id === payload.dataWouldChange.additionalInformation[0].id) {
                                    element.status = payload.dataWouldChange.additionalInformation[0].status
                                    element.approver = this.dataSource.assignee_ob ? this.dataSource.assignee_ob : {}
                                    element.reject_reason = payload.dataWouldChange.additionalInformation[0].reason
                                }
                            });
                        }
                        this.reasonReject = ''
                        this.reasonWithdraw = ''
                        this.statusPayload = []
                        this.getOTRequestList()
                    } else {
                        this.$message.error(res.message);
                    }
                }

            }).catch(() => {
                this.$message.error('Failed to update status!');
            });
        },
        changeStatus(data, status, reasonReject, reasonWithdraw) {
            let dataReturn = { 
                type: '',
                data: { 
                    staff: [], 
                    is_ot_paid_by_offset_day: false,
                    withdraw_reason: reasonWithdraw
                }, 
                additionalInformation: [] 
            }
            if (data.staff) {
                dataReturn.type = 'all'
                data.staff.forEach((element, index) => {
                    let temp = {
                        approver_id: this.userInfo.userId,
                        employee_id: element.employee_id,
                        status: status,
                        reject_reason: reasonReject
                    }
                    dataReturn.data.staff.push(temp)
                    dataReturn.additionalInformation.push({
                        index: index,
                        status: this.statusText[status],
                        reason: reasonReject
                    })
                });
            } else {
                dataReturn.type = 'single'
                dataReturn.data.staff.push({
                    approver_id: this.userInfo.userId,
                    employee_id: data.employee_id,
                    status: status,
                    reject_reason: reasonReject
                })
                dataReturn.additionalInformation.push({
                    id: data.employee_id,
                    status: this.statusText[status],
                    reason: reasonReject
                })
            }
            dataReturn.data.is_ot_paid_by_offset_day = this.dataSource.is_ot_paid_by_offset_day ? this.dataSource.is_ot_paid_by_offset_day : false
            return dataReturn
        },
        onClickApprove(record) {
            let data = this.changeStatus(record, 3, '')
            let payload = {
                dataWouldChange: data,
                payload: {
                    data: data.data,
                    otRequestId: this.dataSource.id
                }
            }
            this.updateStatus(payload)
        },
        onClickReject(record) {
            if (record.staff) {
                this.isSingleRecord = false
            } else {
                this.isSingleRecord = true
            }

            this.statusPayload = record
            this.visibleModalReject = true
        },
        // checkDisableReject(record) {
        //     if (this.isAssignee) {
        //         if (record.status.toUpperCase() !== 'PENDING') {
        //             if (record.status.toUpperCase() === 'APPROVED') {
        //                 let tempStartTime = new Date(this.dataSource.start_time)
        //                 let tempCurrentDate = new Date()
        //                 return (tempStartTime.getTime() < tempCurrentDate.getTime())
        //             }
        //             return true
        //         }
        //         return false
        //     }
        //     return true
        // },
        onClickWithDraw(record) {
            this.visibleModalWithdraw = true
            this.statusPayload = record   
        }, 
        onClickSend() {
            let payload = {
                requestId: this.dataSource.id,
                receiver_id: this.assignee.id,
                data: this.comment
            }

            this.$store.dispatch('overtime/updateComment', payload).then(res => {
                if (res) {
                     if(parseInt(res.code) === 0){
                        this.$message.success('Update status successfully!',5);
                        this.getOTRequestList()
                        this.dataSource.comment.push(this.comment)
                        this.visibleComment = false
                        this.comment = ''
                     }else{
                        this.$message.error('Fail to update comment!',5);
                        this.visibleComment = false
                        this.comment = ''
                     }
                }
            }).catch(() => {
                this.$message.error('Failed to update status!',5);
            });
        },
        handleOk() {
            if (this.reasonReject && this.reasonReject.trim() !== '' || (this.reasonRejectAll && this.reasonRejectAll.trim() !== '')){
                let data = {}
                if (this.isSingleRecord) {
                    data = this.changeStatus(this.statusPayload, 4, this.reasonReject)
                } else {
                    data = this.changeStatus(this.statusPayload, 4, this.reasonRejectAll)
                }
                let payload = {
                    dataWouldChange: data,
                    payload: {
                        data: data.data,
                        otRequestId: this.dataSource.id
                    }
                }
                this.updateStatus(payload)
                this.visibleModalReject = false
            }else{
                this.$message.error('Message required!',5)
            }
        },

        handleWithdraw() {
            if (this.reasonWithdraw && this.reasonWithdraw.trim() !== '') {
                let data = this.changeStatus(this.statusPayload, 5, '', this.reasonWithdraw)  
                let payload = {
                    dataWouldChange: data,
                    payload: {
                        data: data.data,
                        otRequestId: this.dataSource.id
                    }
                }                               
                this.updateStatus(payload)
                this.visibleModalWithdraw = false
            } else {
                this.$message.error('Reasons is required!',5)
            }
        },
        disableRejectSingle(record){   
                if(record.status.toUpperCase() === 'APPROVED'){
                    return this.checkOTDateVsCurrentDate(this.dataSource) || this.checkOTDateVsLastApprovedTimeSheet(this.dataSource)
                }
                if(record.status.toUpperCase() === 'REJECTED' || record.status.toUpperCase() === 'CANCELLED'){
                    return true
                }           
                return false
        },
        disableApproveSingle(record){
            if(this.checkOTDateVsLastApprovedTimeSheet(this.dataSource)){
                return true
            }
            if(record.status.toUpperCase() !== 'PENDING'){
                return true
            }
            return false
        },
        disableDelegate(dataSource){
            if (dataSource.staff[0].status.toUpperCase() === "CANCELLED") {
                return true;
            }
            if(this.disableRejectAll(dataSource) || this.disableApproveAll(dataSource)){
                return true;
            }
            return false
        },
        getAllListFile(id) {
            const { data } = this;
            http.get(`/tms/request-management/${this.request_type}/get-all-file-list?id=` + id,)
              .then((res) => {
                res.data.forEach(record => {
                  data.push(record)
                })
                return res.data
              })
              .catch((err) => {
                throw err
              })
          },
          handleDownload() {
            const { fileList } = this
            fileList.forEach(file => {
              http.get(`/tms/request-management/${this.request_type}/${this.data.id}/download?name=` + file.name, { responseType: 'blob' })
                .then(res => {
                  downloadFile(res.data, file.name)
                  return res.data;
                })
                .catch(err => {
                  throw err
                })
            })
          }
    },
};