import OverTimeDetail from "./OverTimeDetail/OverTimeDetail.vue";
import FilterLayout from "../../components/layouts/FilterLayout";
import { mapState } from "vuex";
import moment from "moment";
import { Role, otType } from "../../components/commons/commonConstant";
import http from '../../store/modules/helpers/httpInterceptor'
import { convertObjectToParams, formatDate, formatTime, downloadFile, scaleScreen, initFilters } from "../../components/commons/commons";
import { statusColor, statusTextReverse, statusText } from '../../components/commons/commonConstant';
const userList = [{
    title: 'Staff',
    dataIndex: 'name',
    key: 'name',
    scopedSlots: { customRender: 'name' },
    width: '30%'
},
{
    title: 'Status',
    dataIndex: 'status',
    key: 'status',
    scopedSlots: { customRender: 'status' },
    width: '10%'
},
{
    title: 'Approver',
    dataIndex: 'approver_rejector',
    key: 'approver_rejector',
    scopedSlots: { customRender: 'approver_rejector' },
    width: '25%'
},
{
    title: 'Reason for rejection',
    dataIndex: 'reason_rejection',
    scopedSlots: { customRender: 'reason_rejection' },
    key: 'reason_rejection',
    width: '20%'
},
{
    title: '',
    dataIndex: 'approve_reject',
    key: 'approve_reject',
    scopedSlots: { customRender: 'approve_reject' },
    width: '15%'
},
];
const recordList = [{
    title: 'Title',
    dataIndex: 'title',
    key: 'title',
    scopedSlots: { customRender: 'title' },
    width: '13%'
},
{
    title: 'Project',
    dataIndex: 'project_name',
    key: 'project_name',
    scopedSlots: { customRender: 'project_name' },
    width: '20%'
},
{
    title: 'OT Date',
    dataIndex: 'ot_date',
    key: 'ot_date',
    scopedSlots: { customRender: 'ot_date' },
    width: '7%'
},
{
    title: 'OT Time',
    dataIndex: 'ot_time',
    scopedSlots: { customRender: 'ot_time' },
    key: 'ot_time',
    width: '7%'
},
{
    title: 'OT Type',
    dataIndex: 'ot_type',
    scopedSlots: { customRender: 'ot_type' },
    key: 'ot_type',
    width: '7%'
},
{
    title: 'Created By',
    dataIndex: 'creator',
    key: 'creator',
    scopedSlots: { customRender: 'creator' },
    width: '15%'
},
{
    title: 'Approver',
    dataIndex: 'approver',
    key: 'approver',
    scopedSlots: { customRender: 'approver' },
    width: '15%'
},
{
    title: 'Status',
    dataIndex: 'status',
    key: 'status',
    scopedSlots: { customRender: 'status' },
    width: '5%'
},
{
    title: '',
    dataIndex: 'edit',
    key: 'edit',
    scopedSlots: { customRender: 'edit' },
    width: '2%'
},
]
const hoverTable = [{
    title: 'Staff',
    dataIndex: 'staff',
    key: 'staff',
    width: '80%',
    scopedSlots: { customRender: 'staff' },
},
{
    title: 'Status',
    dataIndex: 'status',
    key: 'status',
    scopedSlots: { customRender: 'status' },
},
]
export default {
    name: "Overtime-Management",
    components: { OverTimeDetail, FilterLayout },
    data() {
        return {
            OTForm: this.$form.createForm(this, { name: 'OT Request' }),
            page: "Overtime-Management",
            countFiles: 0,
            recordList,
            hoverTable,
            formatDate,
            formatTime,
            statusTextReverse,
            statusText,
            hiddenTable: false,
            clickButtonEdit: false,
            data: [],
            otType,
            headers: {},
            userList,
            Role,
            statusColor,
            currentPage: 1,
            pageSize: 10,
            field: 'Name',
            visibleModal: false,
            sort: { direction: -1 },
            startValue: null,
            endValue: null,
            endOpen: false,
            actual: 1,
            total_items: 0,
            loading: false,
            fetching: false,
            timeout: null,
            source: {},
            filters: {},
            firstCall: true,
            recordDetail: {},
            onColumns: {},
            onStatus: {},
            staffOnsiteList: [],
            projects: {},
            visibleDrawer: false,
            id: 0,
            tooltipOverTime: { config: { visibleShortenDetail: false } },
            cc_mail: [],
            checkTime: {
                minutes: 0,
                hours: 0
            },
            startTime: moment('00:00', 'HH:mm'),
            endTime: moment(this.startTime).add(15, 'm'),
            OTDate: '',
            isCreated: true,
            dateOfLastApprovedTimesheet: '',
            endDateOfCurrentTimesheet: '',
            assignee: {},
            visibleModalReject: false,
            visibleModalWithdraw: false,
            reasonReject: '',
            reasonWithdraw: '',
            statusPayload: [],
            isEdit: false,
            formData: {
                id: '',
                title: '',
                project: '',
                projectId: null,
                projectCode: '',
                OT_date: '',
                category: '',
                ot_type: '',
                start_time: null,
                end_time: null,
                staff: [],
                description: '',
                assignee: ''
            },
            tempMail: '',
            tempCurrentDate: new Date(),
            arrStaffID: [],
            requestListInfo: {},
            hoverTableHeight: 0,
            renderFirstTime: true,
            isDisableExport: true,
            isCheckedOTPaid: null,
            size: window.outerWidth > 800 ? window.outerWidth * 0.6 : window.outerWidth * 0.55,
            tableHeight: window.outerHeight > 800 ? window.outerHeight * 0.6 : window.outerHeight * 0.55,
            durations: {
                duration: null,
                startTime: null,
                endTime: null
            },
            fileList: [],
            isAvaiableToDownLoad: false,
            request_type: 'overtime'
        };
    },


    computed: {
        ...mapState({
            roles: state => state.user.userInfo.roles ? state.user.userInfo.roles : "",
            projectList: state => state.application.projectList,
            dataOTRequestList: state => state.overtime.dataOTRequestList,
            employeeList: state => state.application.employeeList,
            userInfo: (state) => state.user.userInfo,
            sameDUStaff: (state) => state.application.sameDUStaff

        }),
    },
    watch: {
        // formData: function () {
        //     this.OTForm.setFieldsValue(
        //         this.formData
        //     )
        // },
        filters: function () {
            if (this.filters.fromDate !== undefined && this.filters.toDate !== undefined) {
                this.isDisableExport = false
            } else {
                this.isDisableExport = true
            }
        }

    },
    created() {
        this.scaleScreen()
        this.handFilters()
        http
            .get("/tms/timesheets/get-end-date-of-last-approved-timesheet")
            .then((res) => {
                this.tempCurrentDate = new Date(res.data.data)
            })
            .catch((error) => {
                throw (error)
            });
        this.firstCall = true
        this.getEndDateOfLastApprovedTimesheet()
        this.getAssignee()
        window.addEventListener("resize", this.resizeEventHandler);
    },

    mounted() {
        this.onChangeEndTime()
    },

    methods: {
        moment,
        scaleScreen,
        handFilters() {
			const { filters } = this
			initFilters(filters)
            if (sessionStorage.getItem(this.page)) {
                this.filters = { ...JSON.parse(sessionStorage.getItem(this.page)) }
            } else {
                this.filters = { ...this.filters, ...JSON.parse(sessionStorage.getItem(this.page)) }
            }
		},
        scrollEventHandler() {
            this.tooltipOverTime.config.visibleShortenDetail = false
        },
        resizeEventHandler() {
            if (document.body.clientHeight > 800) {
                this.tableHeight = document.body.clientHeight * 0.6
                this.size = document.body.clientWidth * 0.6
            } else {
                this.tableHeight = document.body.clientHeight * 0.55
                this.size = document.body.clientWidth * 0.55
            }
        },
        customRow(record) {
            return {
                on: {
                    click: () => {
                        this.isAvaiableToDownLoad = false
                        this.fileList.splice(0, this.fileList.length)
                        if (!this.clickButtonEdit) {
                            this.visibleDrawer = true;
                            this.getAllListFile(record.id)
                            this.recordDetail = record;
                            this.onColumns = this.userList;
                        } else {
                            this.clickButtonEdit = false
                        }
                    },
                },
            };
        },
        formatDateTime(date) {
            if (date) {
                return moment(date).format("DD/MM/YYYY HH:MM:SS");
            }
            return ''
        },
        getOTRequestList() {
            if (this.renderFirstTime) {
                let x = document.getElementsByClassName('TableOverTime')[0].querySelector('.ant-table-body')
                x.addEventListener('scroll', () => {
                    this.scrollEventHandler()
                })
            }
            this.renderFirstTime = false
            this.$store.dispatch('user/loadingRequest')

            const { pageSize, currentPage, sort, filters } = this
            let param = {}
            if (this.firstCall) {
                param = {
                    ...filters,
                    ...sort
                }
                this.currentPage = filters.page ? filters.page : 1
                this.pageSize = filters.size ? filters.size : 10
            } else {
                param = {
                    ...filters,
                    ...sort,
                    page: currentPage,
                    size: pageSize,
                };
            }
            const url = convertObjectToParams(param);
            this.$store.dispatch("overtime/getOTRequestList", url).then(res => {
                let dataRespone = []
                if (res.page) {
                    this.total_items = res.page.total_element
                }
                if (res.data) {
                    dataRespone = res.data
                    this.isCheckedOTPaid = res.data.is_ot_paid_by_offset_day
                    this.firstCall = false
                }
                this.data = dataRespone
                this.$store.dispatch('user/loadingRequest')
            }).catch(error => {
                this.$message.error(error.message, 5);
                this.$store.dispatch('user/loadingRequest')
            })
            sessionStorage.setItem(this.page, JSON.stringify(param))
        },
        exportRequest() {
            const { filters } = this;
            const paramFilter = convertObjectToParams({
                ...filters,
            });
            this.$store.dispatch('user/loadingRequest');
            http.get(`tms/excel/request-ot${paramFilter}`, { responseType: 'blob' })
                .then(res => {
                    if (res.status === 200 && res.data.type !== 'application/json') {
                        downloadFile(res.data, `Overtime_List_${moment(filters.fromDate).format('DD/MM/yyyy')}-${moment(filters.toDate).format('DD/MM/yyyy')}.xlsx`);
                        this.$store.dispatch('user/loadingRequest', 5);
                        this.$message.success('Request List is exported successfully!');
                    } else {
                        this.$store.dispatch('user/loadingRequest');
                        this.$message.error('No data export', 5);
                    }
                })
                .catch(err => {
                    this.$store.dispatch('user/loadingRequest');
                    this.$message.error(err.response.data.error, 5)
                })
        },
        changeStatus(data, status, reasonReject, reasonWithdraw) {
            let dataReturn = {
                staff: [],
                is_ot_paid_by_offset_day: false,
                withdraw_reason: reasonWithdraw
            }
            data.staff.forEach((element) => {
                let temp = {
                    approver_id: this.userInfo.userId,
                    employee_id: element.employee_id,
                    status: status,
                    reject_reason: reasonReject
                }
                dataReturn.staff.push(temp)
            });
            dataReturn.is_ot_paid_by_offset_day = data.is_ot_paid_by_offset_day ? data.is_ot_paid_by_offset_day : false
            return dataReturn
        },
        updateStatus(payload) {
            this.$store.dispatch('overtime/updateStatusOTRequest', payload).then(res => {
                if (res) {
                    if (parseInt(res.code) === 0) {
                        this.$message.success('Update status successfully!', 5);
                        this.reasonReject = ''
                        this.reasonWithdraw = ''
                        this.statusPayload = []
                        this.getOTRequestList()
                    } else {
                        this.$message.error(res.message, 5);
                    }
                }
            }).catch(() => {
                this.$message.error('Failed to update status!', 5);
            });
        },
        onClickApprove(record) {
            let data = this.changeStatus(record, 3, '')
            let payload = {
                data: data,
                otRequestId: record.id
            }
            this.updateStatus(payload)
        },

        onClickReject(record) {
            this.statusPayload = record
            this.visibleModalReject = true
        },
        onClickWithDraw(record) {
            this.statusPayload = record
            this.visibleModalWithdraw = true
        },
        onChangePage(page, size) {
            this.currentPage = page;
            this.pageSize = size;
            this.tooltipOverTime.config = {
                visibleShortenDetail: false,

            }
            this.getOTRequestList();
        },
        onShowSizeChange(page, size) {
            this.currentPage = page;
            this.pageSize = size;
            this.getOTRequestList();
        },
        onApplyFilters(filters) {
            this.filters = filters;
            this.currentPage = 1
            this.getOTRequestList()
        },
        handleOk() {
            if (this.reasonReject && this.reasonReject.trim() !== '') {
                let data = this.changeStatus(this.statusPayload, 4, this.reasonReject)
                let payload = {
                    data: data,
                    otRequestId: this.statusPayload.id
                }
                this.updateStatus(payload)
                this.visibleModalReject = false
            } else {
                this.$message.error('Message required!', 5)
            }
        },
        handleWithdraw() {
            if (this.reasonWithdraw && this.reasonWithdraw.trim() !== '') {
                let data = this.changeStatus(this.statusPayload, 5, '', this.reasonWithdraw)
                let payload = {
                    data: data,
                    otRequestId: this.statusPayload.id
                }
                this.updateStatus(payload)
                this.visibleModalWithdraw = false
            } else {
                this.$message.error('Reasons is required!', 5)
            }
        },
        handleCancelWithdraw() {
            this.reasonWithdraw = ''
            this.visibleModalWithdraw = false;
        },
        handleCancelRejection() {
            this.visibleModalReject = false
        },
        handleMouseLeave() {
            if (!this.visibleDrawer) {
                this.tooltipOverTime.config.visibleShortenDetail = false;
            }
        },
        handleHoverStatus(event, record) {
            event.preventDefault();
            this.clickButtonEdit = true
            if (record.staff.length > 0) {
                this.tooltipOverTime.config = {
                    visibleShortenDetail: true,
                    top: (event.pageY + 10) + 'px',
                    left: (event.pageX + 5) + 'px',
                }
                this.tooltipOverTime.staff = record.staff
                this.hoverTableHeight = (document.body.clientHeight - event.pageY) / 2
            }
        },

        getAssignee() {
            return http
                .get("/tms/filters/get-approver-for-user?username=" + this.userInfo.userName)
                .then((res) => {
                    this.assignee = res.data
                })
                .catch(err => {
                    throw (err)
                })
        },

        showModalCreate() {
            this.isAvaiableToDownLoad = false
            this.fileList.splice(0, this.fileList.length)
            this.cc_mail = []
            this.formData = {
                assignee: this.assignee.name ? `${this.assignee.name} - ${this.assignee.department_name.name}` : 'Guest-manager'
            }
            this.durations = {},
                this.visibleModal = true;
            this.isEdit = false
            this.OTForm.resetFields()
            this.isCheckedOTPaid = false
            var keys = { 37: 1, 38: 1, 39: 1, 40: 1 };

            function preventDefault(e) {
                e.preventDefault();
            }

            function preventDefaultForScrollKeys(e) {
                if (keys[e.keyCode]) {
                    preventDefault(e);
                    return false;
                }
            }

            
            var supportsPassive = false;
            try {
                window.addEventListener("test", null, Object.defineProperty({}, 'passive', {
                    get: function () { supportsPassive = true; return supportsPassive }
                }));
            } catch (e) { console.log(e) }

            var wheelOpt = supportsPassive ? { passive: false } : false;
            var wheelEvent = 'onwheel' in document.createElement('div') ? 'wheel' : 'mousewheel';

            window.addEventListener('DOMMouseScroll', preventDefault, false); 
            window.addEventListener(wheelEvent, preventDefault, wheelOpt); 
            window.addEventListener('touchmove', preventDefault, wheelOpt); 
            window.addEventListener('keydown', preventDefaultForScrollKeys, false);
        },

        handleCancel() {
            this.OTForm.resetFields()
            this.visibleModal = false;
            this.formData = []
        },
        handleCancel1() {
            this.visibleModalReject = false;
        },
        onChangeCCValue(value) {
            this.cc_mail = value
        },

        onChangeProjects(value) {
            this.projects = value
            if (value) {
                this.formData.projectCode = JSON.parse(value).project_code
            } else {
                this.formData.projectCode = ''
            }
        },

        onSearchStaff(value, timeout) {
            let cloneVal = decodeURI(value);
            if (value && value !== "" && value.length >= 3) {
                this.fetching = true;
                clearTimeout(this.timeout);
                this.timeout = setTimeout(() => {
                    this.$store
                        .dispatch("application/getEmployeeList", cloneVal)
                        .then(() => {
                            this.fetching = false;
                        })
                        .catch(() => {
                            this.fetching = false;
                        });
                }, timeout);

            }
        },

        onSearchSameDUStaff(value, timeout) {
            let cloneVal = decodeURI(value);
            if (value && value !== "" && value.length >= 3) {
                this.fetching = true;
                clearTimeout(this.timeout);
                this.timeout = setTimeout(() => {
                    this.$store
                        .dispatch("application/getSameDUStaffList", cloneVal)
                        .then(() => {
                            this.fetching = false;
                        })
                        .catch(() => {
                            this.fetching = false;
                        });
                }, timeout);

            }
        },

        onChangeStaffOT(value) {
            this.formData.staff = value
        },

        filterOption(input, option) {
            return (
                option.componentOptions.children[0].text.toLowerCase().indexOf(input.toLowerCase()) >= 0
            );
        },

        onChangeDate(date, dateString) {
            if (!date && !dateString) {
                this.formData.category = null
                this.OTForm.resetFields('category')
            } else {
                this.OTDate = dateString
                this.getCategory(dateString)
                this.formData.category = this.OTForm.getFieldValue('category')
            }
        },

        disabledHours() {
            let hours = [];
            for (let index = 0; index < this.checkTime.hours; index++) {
                hours.push(index)
            }
            if (this.checkTime.minutes === 45) {
                hours.push(this.checkTime.hours)
            }
            return hours
        },

        disabledMinutes(hours) {
            let minute = []
            if (hours === this.checkTime.hours) {
                for (let index = 0; index <= this.checkTime.minutes; index += 15) {
                    minute.push(index)
                }
            }
            return minute
        },

        getEndDateOfLastApprovedTimesheet() {
            return http
                .get("/tms/timesheets/get-end-date-of-last-approved-timesheet")
                .then((res) => {
                    this.dateOfLastApprovedTimesheet = {
                        year: parseInt(moment(res.data.data).format("YYYY")),
                        month: parseInt(moment(res.data.data).format("MM")),
                        day: parseInt(moment(res.data.data).format("DD"))
                    }
                })
                .catch((error) => {
                    throw (error)
                });
        },

        getCategory(otDate) {
            return http
                .get("/tms/filters/category?otDate=" + otDate)
                .then((res) => {
                    this.OTForm.setFieldsValue({ category: res.data.data.name })


                })
                .catch(err => {
                    throw (err)
                })
        },

        disabledDate(current) {
            let temp = {
                year: parseInt(moment(current).format("YYYY")),
                month: parseInt(moment(current).format("MM")),
                day: parseInt(moment(current).format("DD"))
            }
            if (temp.year < this.dateOfLastApprovedTimesheet.year) {
                return true;
            } else if (temp.year === this.dateOfLastApprovedTimesheet.year) {
                if (temp.month < this.dateOfLastApprovedTimesheet.month) {
                    return true
                } else if (temp.month === this.dateOfLastApprovedTimesheet.month) {
                    if (temp.day <= this.dateOfLastApprovedTimesheet.day) {
                        return true
                    }
                }
            }
            if(current) {
                return current < moment().startOf('day')
            }
            return false;
        },

        disabledDateUpdate(current) {
            let temp = {
                year: parseInt(moment(current).format("YYYY")),
                month: parseInt(moment(current).format("MM")),
                day: parseInt(moment(current).format("DD"))
            }
            if (temp.year < this.dateOfLastApprovedTimesheet.year) {
                return true;
            } else if (temp.year === this.dateOfLastApprovedTimesheet.year) {
                if (temp.month < this.dateOfLastApprovedTimesheet.month) {
                    return true
                } else if (temp.month === this.dateOfLastApprovedTimesheet.month) {
                    if (temp.day <= this.dateOfLastApprovedTimesheet.day) {
                        return true
                    }
                }
            }
            return false;
        },

        handleOnClickEdit(i) {
            this.fileList.splice(0, this.fileList.length)
            this.getAllListFile(i.id)
            this.clickButtonEdit = true
            if (!this.roles.includes(Role.GUEST)) {
                let tempStaff = ''
                this.tempMail = ''
                this.cc_mail = []
                this.formData = {
                    id: i.id,
                    status: [],
                    title: i.title,
                    project: i.project_name,
                    OT_date: i.ot_date,
                    category: i.category,
                    ot_type: i.ot_type,
                    projectId: i.project_id ? i.project_id : '',
                    projectCode: i.project_code ? i.project_code : '',
                    start_time: i.start_time,
                    end_time: i.end_time,
                    description: i.request ? i.request.description : undefined,
                    assignee: i.assignee_ob ? `${i.assignee_ob.name} - ${i.assignee_ob.department_name.nameEn}` : `Guest-manager`,
                    staff: [],
                }
                this.startTime = moment(i.start_time)
                this.endTime = moment(i.end_time)
                this.calculateDuration(this.formData.start_time, this.formData.end_time)
                this.isCheckedOTPaid = i.is_ot_paid_by_offset_day
                i.staff.forEach((item) => {
                    this.formData.staff.push(item.employee_id)
                    tempStaff = tempStaff + item.employee.user_name + ' '
                    this.formData.status.push(item.status)
                })
                this.onSearchSameDUStaff(tempStaff)
                if (i.mailList.length > 0) {
                    i.mailList.forEach(element => {
                        this.cc_mail.push(element._id)
                        this.tempMail = this.tempMail + element.user_name + ' '
                    });
                }
            }
        },

        handleSubmit(e) {
            clearTimeout(this.timeout)
            this.timeout = setTimeout(() => {
                e.preventDefault();
                this.$emit("click");
                this.OTForm.validateFields((err, values) => {
                    let otDate = moment(values.OT_date).format('YYYY-MM-DD')
                    let createdDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
                    if (!err) {
                        let payload = {
                            title: values.title.trim(),
                            project_id: this.isEdit ? this.formData.projectId : JSON.parse(this.projects).project_id,
                            project_name: this.isEdit ? this.formData.project : JSON.parse(this.projects).project_name,
                            category: values.category,
                            created_date: createdDate,
                            description: values.description ? values.description : '',
                            start_time: `${otDate} ` + moment(this.startTime).format('HH:mm:00'),
                            end_time: `${otDate} ` + moment(this.endTime).format('HH:mm:00'),
                            mail_cc: this.cc_mail,
                            ot_date: otDate,
                            ot_type: values.ot_type,
                            assignee: this.assignee.id,
                            is_ot_paid_by_offset_day: this.isCheckedOTPaid
                        }
                        let temp = []
                        payload.staff = temp
                        if (!this.isEdit) {
                            this.formData.staff.forEach(element => {
                                temp.push({
                                    approver_id: null,
                                    status: 1,
                                    employee_id: element
                                })
                            });
                            payload.comment = []
                            this.$store.dispatch('overtime/createOTRequest', payload)
                                .then(res => {
                                    if (parseInt(res.code) === 0) {
                                        this.$message.success(res.message, 5)
                                        if (res.validationResult.code) {
                                            this.$message.warning(res.validationResult.message, 5)
                                        }
                                        setTimeout(() => this.getOTRequestList(), 500)
                                    } else {
                                        temp = ' '
                                        if (res.listEmployeeFail) {
                                            res.listEmployeeFail.forEach(element => {
                                                temp = temp + " " + element
                                            });
                                        }
                                        this.$message.error((res.message + temp), 5)
                                    }
                                    this.visibleModal = false;
                                    this.formData = {}
                                    this.handleUpload(res.id)
                                    this.OTForm.resetFields()
                                })
                                .catch(error => {
                                    this.$message.error(error.message, 5)
                                })
                        } else {
                            this.formData.staff.forEach((element, index) => {
                                temp.push({
                                    approver_id: null,
                                    status: this.formData.status[index] ? this.statusTextReverse[this.formData.status[index].toUpperCase()] : this.statusTextReverse[this.statusText[1]],
                                    employee_id: element
                                })
                            });
                            payload.last_modified_date = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
                            payload.last_mofifier_id = this.userInfo.userId
                            payload.creator_id = this.userInfo.userId
                            payload.id = this.formData.id
                            this.$store.dispatch('overtime/updateOTRequestData', payload)
                                .then(res => {
                                    if (parseInt(res.code) === 0) {
                                        this.$message.success(res.message, 5);
                                        if (res.validationResult) {
                                            this.$message.warning(res.validationResult.message, 5)
                                        }
                                        setTimeout(() => this.getOTRequestList(), 500)
                                    } else {
                                        this.$message.error(res.message, 5)
                                    }
                                    this.visibleModal = false;
                                    this.handleUpload(this.formData.id)
                                    this.formData = {}
                                    this.OTForm.resetFields()
                                })
                                .catch(error => {
                                    this.$message.error(error.message, 5)
                                })
                        }
                        this.OTForm.resetFields()
                    } else throw (err)
                })
            }, 300)
        },

        // getEndDateOfCurrentTimesheet() {
        //     return http
        //         .get("/tms/timesheets/get-end-date-of-current-timesheet")
        //         .then((res) => {
        //             this.endDateOfCurrentTimesheet = res.data.name
        //         })
        //         .catch(err => {
        //             throw (err)
        //         })
        // },

        async showModalEdit() {
            document.body.setAttribute('style', 'overflow: hidden !important')
            this.$store.dispatch('user/loadingRequest')
            await this.onSearchStaff(this.tempMail)
            this.isEdit = true
            setTimeout(() => {
                this.visibleModal = true
                this.$store.dispatch('user/loadingRequest')
            }, 2000)

        },

        handleChange() { },
        onSetDataFilters(filters) {
            this.filters = filters;
        },
        handleOnClick(record) {
            if (!this.clickButtonEdit) {
                this.visibleDrawer = true;
                this.recordDetail = record;
                this.onColumns = this.userList;
            } else {
                this.clickButtonEdit = false
            }
        },

        onClose() {
            this.visibleDrawer = false;
            this.$refs.setFields.handleCancel();
            this.tooltipOverTime = { config: { visibleModal: false } }
        },
        checkStatusPending(data) {
            data.pendingCounter = 0
            for (let index = 0; index < data.staff.length; index++) {
                if (data.staff[index].status.toUpperCase() === "PENDING") {
                    data.pendingCounter++;
                }
            }

            return data.pendingCounter > 0 ? true : false
        },
        checkStatusCancelled(data) {
            for (let index = 0; index < data.staff.length; index++) {
                if (data.staff[index].status.toUpperCase() === "CANCELLED") {
                    return true;
                }
            }
            return false
        },
        checkOTDateVsCurrentDate(data) {
            let tempStartTime = new Date(data.start_time)
            let tempCurrentDate = new Date()
            return tempStartTime.getTime() <= tempCurrentDate.getTime()
        },
        checkOTDateVsLastApprovedTimeSheet(data) {
            let tempStartTime = new Date(data.start_time)
            return tempStartTime.getTime() <= this.tempCurrentDate.getTime()
        },
        disableWithDraw(data) {
            if (data.staff[0].status.toUpperCase() === "CANCELLED") {
                return true;
            }
            for (let index = 0; index < data.staff.length; index++) {
                if (data.staff[index].status.toUpperCase() === 'APPROVED') {
                    return this.checkOTDateVsCurrentDate(data)
                }
            }
            return this.checkOTDateVsLastApprovedTimeSheet(data)
        },
        disableApproveAll(data) {
            let disable = false
            for (let index = 0; index < data.staff.length; index++) {
                if (data.staff[index].status.toUpperCase() === 'REJECTED' || data.staff[index].status.toUpperCase() === 'CANCELLED') {
                    return true
                }
                if (data.staff[index].status.toUpperCase() === 'APPROVED') {
                    disable = true
                } else {
                    disable = false
                }
            }
            return disable || this.checkOTDateVsLastApprovedTimeSheet(data)
        },
        disableRejectAll(data) {
            let disable = false
            let tempCount = 0
            if (data.staff[0].status.toUpperCase() === 'CANCELLED') {
                return true
            }
            data.staff.forEach(element => {
                if (element.status.toUpperCase() === 'APPROVED') {
                    if (this.checkOTDateVsCurrentDate(data)) {
                        disable = true
                    }
                }
                if (element.status.toUpperCase() === 'REJECTED') {
                    tempCount++
                }
            });
            return this.checkOTDateVsLastApprovedTimeSheet(data) || disable || tempCount === data.staff.length
        },
        disableEdit(data) {
            let disable = false
            data.staff.forEach(element => {
                if (element.status.toUpperCase() !== 'PENDING') {
                    disable = true
                }
            });
            return disable || this.checkOTDateVsLastApprovedTimeSheet(data)
        },
        disableMoreButton(data) {

            if (data.staff[0].status.toUpperCase() === 'CANCELLED') {
                return true
            }
            if (this.checkOTDateVsLastApprovedTimeSheet(data)) {
                return true
            }
            let tempCount = 0
            data.staff.forEach(element => {
                if (element.status.toUpperCase() === 'REJECTED') {
                    tempCount++
                }
            });
            let disable = tempCount === data.staff.length
            return disable

        },

        checkOTPaid(e) {
            this.isCheckedOTPaid = e.target.checked
        },

        onChangeStartTime(time) {
            this.startTime = time
            this.checkTime = {
                hours: parseInt(moment(time).format('HH')),
                minutes: parseInt(moment(time).format('mm'))
            }
            if (this.endTime && this.startTime) {
                let tempStartTime = this.startTime.format('YYYY-MM-DD HH:mm:00')
                let tempEndTime = this.endTime.format('YYYY-MM-DD HH:mm:00')
                this.calculateDuration(tempStartTime, tempEndTime)
            }
        },

        onChangeEndTime(time) {
            this.endTime = time
            if (this.endTime && this.startTime) {
                let tempStartTime = this.startTime.format('YYYY-MM-DD HH:mm:00')
                let tempEndTime = this.endTime.format('YYYY-MM-DD HH:mm:00')
                this.calculateDuration(tempStartTime, tempEndTime)
            }
            if (this.endTime < this.startTime) {
                this.$message.error('End Time must be after Start time!', 5)
                this.isCreated = false
            } else this.isCreated = true

        },

        calculateDuration(startTime, endTime) {
            this.durations.startTime = formatTime(startTime);
            this.durations.endTime = formatTime(endTime);
            let rangeTime = (Date.parse(endTime) - Date.parse(startTime)) / 1000 / 3600
            if (rangeTime >= 20) {
                this.durations.duration = rangeTime - 4;
            } else if (rangeTime >= 15) {
                this.durations.duration = rangeTime - 3;
            } else if (rangeTime >= 10) {
                this.durations.duration = rangeTime - 2;
            } else if (rangeTime >= 5) {
                this.durations.duration = rangeTime - 1;
            } else {
                this.durations.duration = rangeTime;
            }
        },
        beforeUpload(file) {
            this.fileList = [...this.fileList, file];
            return false;
        },
        handleRemove(file) {
            const index = this.fileList.indexOf(file);
            this.deleteFile(this.formData.id, file.name);
            const newFileList = this.fileList.slice();
            newFileList.splice(index, 1);
            this.fileList = newFileList;
        },
        handleUpload(id) {
            const { fileList } = this;
            const formData = new FormData();
            fileList.forEach(file => {
                formData.append('file', file);
            });
            http.post(`/tms/request-management/${this.request_type}/${id}/upload?file`, formData, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            })
                .then((res) => {
                    this.$message.success("Success");
                    fileList.splice(0, fileList.length)
                    return res.data;
                })
                .catch((err) => {
                    throw err
                })
        },
        getAllListFile(id) {
            const { fileList } = this;
            http.get(`/tms/request-management/${this.request_type}/get-all-file-list?id=` + id,)
                .then((res) => {
                    if(res.data) {
                    let tempData = res.data
                    tempData.forEach(record => {
                        fileList.push(record)
                    })
                    if (fileList.length > 0) {
                        this.isAvaiableToDownLoad = true
                    }
                    else {
                        this.isAvaiableToDownLoad = false
                    }
                    return tempData
                    }
                })
                .catch((err) => {
                    throw err
                })
        },
        deleteFile(id, name) {
            http.get(`/tms/request-management/${this.request_type}/${id}/delete-file?name=` + name)
                .then(res => {
                    this.$message.success(res.data)
                    if (this.fileList.length > 0) {
                        this.isAvaiableToDownLoad = true
                    }
                    else {
                        this.isAvaiableToDownLoad = false
                    }
                    return res.data
                })
                .catch(err => {
                    throw err
                })
        },
        handleDownload() {
            const { fileList } = this
            fileList.forEach(file => {
                http.get(`/tms/request-management/${this.request_type}/${this.formData.id}/download?name=` + file.name, { responseType: 'blob' })
                    .then(res => {
                        downloadFile(res.data, file.name)
                        return res.data;
                    })
                    .catch(err => {
                        throw err
                    })
            })
        }
    }
};
