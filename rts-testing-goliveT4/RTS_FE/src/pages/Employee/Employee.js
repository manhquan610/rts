import { scaleScreen } from "../../components/commons/commons";
import FilterLayout from "../../components/layouts/FilterLayout";
import { mapState } from "vuex";
import { Role } from "../../components/commons/commonConstant";
const columns = [
    {
        title: "SF4C ID",
        dataIndex: "sf4c_id",
        key: "sf4c_id",
        scopedSlots: { customRender: "sf4c_id" },
    },
    {
        title: "Username",
        dataIndex: "user_name",
        key: "user_name",
        scopedSlots: { customRender: "user_name" },
    },
    {
        title: "Full Name",
        dataIndex: "full_name",
        key: "full_name",
        scopedSlots: { customRender: "full_name" },
    },
    {
        title: "Birthday",
        dataIndex: "birthday",
        key: "birthday",
        scopedSlots: { customRender: "birthday" },
    },
    {
        title: "Department",
        dataIndex: "department",
        key: "department",
        scopedSlots: { customRender: "department" },
    },
    {
        title: "Job Level",
        dataIndex: "job_level",
        key: "job_level",
        scopedSlots: { customRender: "job_level" },
    },
    {
        title: "Contract Type",
        dataIndex: "contract_type",
        key: "contract_type",
        scopedSlots: { customRender: "contract_type" },
    },
    {
        title: "Remain Annual Leave",
        dataIndex: "remain_annual",
        key: "remain_annual",
        scopedSlots: { customRender: "remain_annual" },
    },
];
const sodaColumns = [
    {
        title: "SDSV ID",
        dataIndex: "sdsv_id",
        key: "sdsv_id",
        scopedSlots: { customRender: "sdsv_id" },
    },
    {
        title: "LDAP ID",
        dataIndex: "username",
        key: "username",
        scopedSlots: { customRender: "username" },
    },
    {
        title: "Start Date",
        dataIndex: "start_date",
        key: "start_date",
        scopedSlots: { customRender: "start_date" },
    },
    {
        title: "End Date",
        dataIndex: "end_date",
        key: "end_date",
        scopedSlots: { customRender: "end_date" },
    },
    {
        title: "",
        dataIndex: "edit",
        key: "edit",
        width: '10px',
        scopedSlots: { customRender: "edit" },
    },

];
const mockData = [
    {
        id: 1,
        username: 'dmlinh',
        sdsv_id: 'SDS25683',
        start_date: '12/05/2021',
        end_date: '20/10/2021'
    }
];
export default {
    name: 'Employee',
    components: {
        FilterLayout,
    },
    computed: {
        ...mapState({
            userInfo: state => state.user.userInfo,
            roles: state => state.user.userInfo.roles ? state.user.userInfo.roles : "",
            timesheetUser: (state) => state.timesheet.timesheetUser ? state.timesheet.timesheetUser : {},
        })
    },
    created(){
        this.scaleScreen(),
        this.dataFilters = {...this.dataFilters, ...JSON.parse(sessionStorage.getItem(this.page))}
        this.tabSelected = 'employeeList'
      },
    data() {
        return {
            page: 'Employee List',
            columns,
            sodaColumns,
            mockData,
            loading: false,
            dataFilters: {},
            role: Role,
            currentPage: 1,
            pageSize: 10,
            total_items: 0,
            form: this.$form.createForm(this, { name: 'Employee' }),
            formSD: this.$form.createForm(this, { name: 'Employee Soda' }),
            visibleModal: false,
            isEdit: false,
            tabSelected: '',
        }
    },
    methods: {
        scaleScreen,
        handleChangeTab(key){
            this.tabSelected = key
        },
        showModalCreate() {
            this.visibleModal = true
            this.isEdit = false
        },
        showModalEdit(){
            this.visibleModal = true
            this.isEdit = true;
        },
        handleCancel(){
            this.visibleModal = false
            this.formSD.resetFields()
        },
        onSetDataFilters(filters) {
            this.dataFilters = filters;
        },
        onApplyFilters(filters) {
            this.dataFilters = filters;
            this.currentPage = 1
            // this.getAbnormalCaseData()
        },
        onChangePage(page, size) {
            this.currentPage = page;
            this.pageSize = size;
            // this.getAbnormalCaseData();
        },
        onShowSizeChange(page, size) {
            this.currentPage = page;
            this.pageSize = size;
            // this.getAbnormalCaseData();
        },
    },
}