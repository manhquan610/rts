import { mapActions, mapState } from "vuex";

import FilterLayout from "../../components/layouts/FilterLayout";
import RequestModal from "../../components/requests/RequestModal";
import RequestTable from "../../components/requests/RequestTable";
import RequestShareModal from "../../components/requests/RequestShareModal";
import RequestButtonDropdown from "../../components/requests/RequestButtonDropdown";
import RequestPopupActionModal from "../../components/requests/RequestPopupAction";
import RequestAssignModal from "../../components/requests/RequestAssignModal";
import RequestAssignTALeadModal from "../../components/requests/RequestAssignTALeadModal";
import {
  configPanigationDefault,
  configSortDefault,
  PAGE_OPTIONS_KEY,
  Role,
  STATUS_COLOR,
  TAB_KEYS,
} from "../../components/commons/commonConstant";
import {
  capitalizeFirstLetter,
  checkExpired,
  clearState,
  compareDate,
  compareString,
  getStage,
  saveStage,
  formatNewStatus,
} from "../../components/commons/commons";
import { checkPermission } from "../../service/permissionService";
import RequestPopupAssigne from "../../components/requests/RequestPopupAssigne";
import moment from "moment";
import http from "../../store/modules/helpers/httpInterceptor";

const columns = [
  {
    title: "ID",
    dataIndex: "id",
    key: "id",
    sorter: {
      compare: (a, b) => a.id - b.id,
      multiple: 3,
    },
    scopedSlots: { customRender: "requestID" },
    ellipsis: true,
    width: "3%",
  },
  {
    title: "JR ID",
    dataIndex: "codePosition",
    key: "codePosition",
    sorter: (a, b) => compareString(a.codePosition, b.codePosition),
    // width: "6%",
  },
  {
    title: "Title",
    dataIndex: "title",
    key: "title",
    sorter: (a, b) => compareString(a.title, b.title),
    ellipsis: true,
    scopedSlots: { customRender: "title_" },
    width: "13%",
  },
  {
    title: "Area",
    dataIndex: "area",
    key: "area",
    // sorter: (a, b) => compareString(a.area, b.area),
    // width: "7%",
  },
  {
    title: "Division", //It was changed from Group to Division follow by request
    dataIndex: "division",
    key: "group",
    // sorter: (a, b) => compareString(a.group, b.group),
    // width: "7%",
  },
  {
    title: "Department",
    dataIndex: "department",
    key: "department",
    // sorter: (a, b) => compareString(a.department, b.department),
    width: "7%",
  },
  {
    title: "Job Level",
    dataIndex: "managerJobLevel",
    ellipsis: true,
    key: "managerJobLevel",
    // sorter: (a, b) => compareString(a.jobType, b.jobType),
    // width: "6%",
    customRender: (value, row, index) => {
      return row && row.managerJobLevel && row.managerJobLevel.length ? row.managerJobLevel[0].name : "";
    },
  },
  {
    title: "Priority",
    dataIndex: "priority",
    key: "priority",
    sorter: (a, b) => compareString(a.priority, b.priority),
    // width: "7%",
  },
  {
    title: "Status",
    dataIndex: "status",
    key: "status",
    sorter: (a, b) => compareString(a.status, b.status),
    scopedSlots: { customRender: "status" },
    width: "7%",
  },
  {
    title: "Language",
    dataIndex: "language",
    key: "language",
    // sorter: (a, b) => compareString(a.language, b.language),
    // width: "7%",
  },
  {
    title: "Target",
    dataIndex: "number",
    key: "number",
    sorter: (a, b) => a.number - b.number,
    width: "5%",
  },
  {
    title: "Deadline",
    dataIndex: "deadline",
    key: "deadline",
    sorter: (a, b) => compareDate(a.deadline, b.deadline),
    scopedSlots: { customRender: "deadline" },
    // width: "8%",
  },

  {
    title: "Applicants",
    dataIndex: "applicants",
    key: "applicants",
    ellipsis: true,
    // sorter: (a, b) => compareString(a.applicants, b.applicants),
    scopedSlots: { customRender: "applicants" },
    // width: "9%",
  },
  {
    title: "Onboard",
    dataIndex: "onboarding",
    key: "onboarding",
    ellipsis: true,
    // sorter: (a, b) => compareString(a.onboarding, b.onboarding),
    scopedSlots: { customRender: "onboarding" },
    // width: "9%",
  },
  {
    title: "TA Lead",
    dataIndex: "createdBy",
    key: "createdBy",
    // sorter: (a, b) => compareString(a.createdBy, b.createdBy),
    width: "5%",
  },
  {
    title: "",
    dataIndex: "action",
    width: "3%",
    scopedSlots: { customRender: "action" },
  },
];

let innerColumns = [
  {
    title: "Name",
    dataIndex: "assignName",
    key: "assignName",
    width: "20%",
    sorter: (a, b) => compareString(a.assignName, b.assignName),
  },
  {
    title: "Target",
    dataIndex: "target",
    key: "target",
    sorter: (a, b) => a.target - b.target,
    scopedSlots: { customRender: TAB_KEYS.GENERAL_INFORMATION },
  },
  {
    title: "Application",
    dataIndex: "applied",
    key: "applied",
    sorter: (a, b) => a.applied - b.applied,
    customRender: (_, row) => {
      return `${row.qualifying}/${row.qualify}`;
    },
    scopedSlots: { customRender: TAB_KEYS.QUALIFY },
  },
  {
    title: "Qualifying",
    dataIndex: "contacting",
    key: "contacting",
    sorter: (a, b) => a.contacting - b.contacting,
    customRender: (_, row) => {
      return `${row.contacting}/${row.contact}`;
    },
    scopedSlots: { customRender: TAB_KEYS.CONFIRM },
  },
  {
    title: "Interview",
    dataIndex: "interview",
    key: "interview",
    sorter: (a, b) => a.interview - b.interview,
    customRender: (_, row) => {
      return `${row.interviewing}/${row.interview}`;
    },
    scopedSlots: { customRender: TAB_KEYS.INTERVIEW },
  },
  {
    title: "Offering",
    dataIndex: "other",
    key: "other",
    sorter: (a, b) => a.other - b.other,
    customRender: (_, row) => {
      return `${row.offering}/${row.offer}`;
    },
    scopedSlots: { customRender: TAB_KEYS.OFFER },
  },
  {
    title: "OnBoarding",
    dataIndex: "onboard",
    key: "onboard",
    sorter: (a, b) => a.onboard - b.onboard,
    customRender: (_, row) => {
      return `${row.onBoarding}/${row.onBoard}`;
    },
    scopedSlots: { customRender: TAB_KEYS.ONBOARD },
  },
  {
    title: "",
    dataIndex: "action",
    scopedSlots: { customRender: "actionAssign" },
  },
];

let defaultItem = {
  title: undefined,
  requestType: undefined,
  priority: undefined,
  deadline: undefined,
  number: undefined,
  position: undefined,
  project: undefined,
  department: undefined,
  group: undefined,
  skills: undefined,
  experience: undefined,
  major: undefined,
  certificate: undefined,
  description: undefined,
  language: undefined,
  salary: undefined,
  benefit: undefined,
  area: undefined,
  managerJobLevel: undefined,
};
const defaultPageOptions = {
  offset: 1,
  limit: localStorage.getItem("PayloadRequest")
    ? JSON.parse(localStorage.getItem("PayloadRequest")).limit
      ? Number(JSON.parse(localStorage.getItem("PayloadRequest")).limit)
      : configPanigationDefault.pageSize
    : configPanigationDefault.pageSize,
  like: "0",
};
export default {
  name: "Requests",
  components: {
    FilterLayout,
    RequestModal,
    RequestTable,
    RequestShareModal,
    RequestPopupActionModal,
    RequestAssignModal,
    RequestAssignTALeadModal,
    RequestButtonDropdown,
    RequestPopupAssigne,
  },
  data() {
    return {
      columns,
      innerColumns,
      loading: false,
      formName: "",
      Role,
      editedItem: defaultItem,
      isEdit: false,
      isVisibleAssign: false,
      currentAssignee: "",
      currentAssigneeId: "",
      currentAssigneeTarget: null,
      statusColor: STATUS_COLOR,
      singleActions: [
        "detail",
        "edit",
        "submit",
        "close",
        "approve",
        "reject",
        "view",
        "assign",
        "assignTALead",
        "share",
        "copy",
      ],
      multiActions: ["submit", "close", "approve", "reject"],
      slots: ["status"],
      pageOptions: defaultPageOptions,
      isDisableActions: true,
      typeAction: null,
      isShowActionModal: false,
      isShowAssignModal: false,
      isShowAssignTALeadModal: false,
      dialogMessageActionModal: "",
      titleActionModal: "",
      actionData: [],
      actionType: "",
      maximumTarget: 0,
      defaultPopupItem: null,
      assignRequestId: null,
      dataFilters: getStage(PAGE_OPTIONS_KEY),
      listHr: [],
      currentStatus: "",
      currentProjectTarget: 0,
    };
  },
  created() {
    this.refreshSelect();
    this.$store.dispatch("rtsApplication/getHumanResourceList");
    this.$store.dispatch("rtsApplication/getGroupList");
    this.$store.dispatch("request/updateDataDuplicate", { check: false, data: {} });
  },
  computed: {
    ...mapState({
      userRole: (state) => state.user.userRole,
      requests: (state) => state.rtsRequest.requestList,
      roles: (state) => (state.user.userInfo.roles ? state.user.userInfo.roles : ""),
      pages: (state) => state.rtsRequest.pages,
      groups: (state) => state.rtsApplication.groupList,
      departments: (state) => state.rtsApplication.departmentList,
      projects: (state) => state.rtsApplication.projectList,
      titles: (state) => state.rtsApplication.titleList,
      humanResources: (state) => state.rtsApplication.humanResourceList,
      assignee: (state) => state.rtsRequest.assigneeList,
      assigneeList: (state) => state.rtsRequest.assigneeList,
      userInfo: (state) => state.user.userInfo,
    }),
    selectedRow: {
      get() {
        return this.$store.state.rtsApplication.selectedRow;
      },
      set(newVal) {
        this.$store.commit("rtsApplication/SET_SELECTED_ROW", newVal);
      },
    },
  },
  watch: {
    $route: {
      immediate: true,
      handler(to, from) {
        document.title = to.meta.title || "Recruitment Tracking System";
      },
    },
    selectedRow: function () {
      this.isDisableActions = this.selectedRow.length === 0;
    },
  },
  methods: {
    ...mapActions({
      setLoading: "user/loadingRequest",
      stopLoading: "user/stopLoading",
    }),
    checkPermission,
    capitalizeFirstLetter,
    formatNewStatus,
    checkDeadline(date) {
      return moment(date, "DD/MM/YYYY").isBefore(moment(new Date()).subtract(1, "days"));
    },
    onCleanFilter() {
      this.refreshSelect();
    },

    onApplyFilters(dataFilters) {
      this.pageOptions = getStage(PAGE_OPTIONS_KEY) ? getStage(PAGE_OPTIONS_KEY) : this.getPageOptions(dataFilters);
      clearState(PAGE_OPTIONS_KEY);
      this.getRequestData(this.pageOptions);
    },
    getPageOptions(dataFilters) {
      return {
        offset: configPanigationDefault.currentPage,
        limit: localStorage.getItem("PayloadRequest")
          ? JSON.parse(localStorage.getItem("PayloadRequest")).limit
            ? Number(JSON.parse(localStorage.getItem("PayloadRequest")).limit)
            : configPanigationDefault.pageSize
          : configPanigationDefault.pageSize,
        like: "0",
        orderBy: configSortDefault.orderBy,
        orderType: configSortDefault.orderType,
        requestIds: dataFilters.requestIds,
        name: dataFilters.titleRequest,
        group: dataFilters.group,
        department: dataFilters.department,
        statusIds: dataFilters.status,
        project: dataFilters.project,
        hrIds: dataFilters.hr,
        areaIds: dataFilters.area,
        priorityIds: dataFilters.priority,
        startDate: dataFilters.fromDate ? moment(dataFilters.fromDate).utc().format("YYYY/MM/DD") : null,
        endDate: dataFilters.toDate ? moment(dataFilters.toDate).utc().format("YYYY/MM/DD") : null,
        languageIds: dataFilters.language,
      };
    },
    refreshSelect() {
      this.selectedRow = [];
      this.isShowAssignModal = false;
    },
    onSelect(payload) {
      this.selectedRow = payload;
    },
    onDelete() {},
    handleUpdateAssign(payload) {
      this.isVisibleAssign = true;
      this.currentAssignee = payload.assignLdap;
      this.currentAssigneeId = payload.assignId;
      this.currentAssigneeTarget = payload.target;
      this.currentProjectTarget = payload.projectTarget;
    },
    handleSubmitUpdateAssignee(payload) {
      const newPayload = {
        id: this.currentAssigneeId,
        name: payload.assign_ldap,
        target: parseInt(payload.target),
      };
      this.$store
        .dispatch("rtsRequest/updateAssignRtsRequest", newPayload)
        .then((res) => {
          this.$store.dispatch("user/stopLoading");
          this.$message.success(res.message);
          this.isVisibleAssign = false;
          this.getRequestData(this.pageOptions);
        })
        .catch((err) => {
          this.$message.error(err.message, 5);
          this.$store.dispatch("user/stopLoading");
        });
    },
    handleCloseAssign() {
      this.isVisibleAssign = false;
    },
    onChangePage(payload) {
      let payloadRequest = localStorage.getItem("PayloadRequest")
        ? JSON.parse(localStorage.getItem("PayloadRequest"))
        : {};
      payloadRequest.limit = payload.limit;
      localStorage.setItem("PayloadRequest", JSON.stringify(payloadRequest));

      this.pageOptions.limit = Number(payload.limit);
      this.pageOptions.offset = Number(payload.offset);
      this.getRequestData(this.pageOptions);

      this.refreshSelect();
    },
    onChange(payload) {
      configSortDefault.orderBy = payload.sorter.order;
      configSortDefault.orderType = payload.sorter.field;

      this.pageOptions.orderBy = payload.sorter.order;
      this.pageOptions.orderType = payload.sorter.field;

      this.getRequestData(this.pageOptions);
    },
    clearFilter() {
      if (!this.$refs.filterRef) return;
      this.pageOptions = defaultPageOptions;
      this.$refs.filterRef.filters = {};
    },
    getRequestData(payload) {
      this.$store.dispatch("user/loadingRequest");
      this.$store
        .dispatch("rtsRequest/getRtsRequestList", payload)
        .then(() => {
          this.$store.dispatch("user/stopLoading");
        })
        .catch((err) => {
          this.$message.error(err.message, 5);
          this.$store.dispatch("user/stopLoading");
        });
    },

    onSingleAction(payload) {
      this.currentStatus = payload?.data?.status;
      this.actionType = payload.type;
      if (payload.data && payload.data.length > 0) {
        this.actionData = payload.data[0];
      } else {
        this.actionData = [];
      }
      if (this.actionType === "detail") {
        let requestId = this.actionData.id;
        this.goDetailPage(requestId);
      } else if (this.actionType === "edit") {
        let requestId = this.actionData.id;
        saveStage(PAGE_OPTIONS_KEY, this.pageOptions);
        this.goEditPage(requestId);
      } else if (this.actionType === "create") {
        this.goEditPage();
      } else if (this.actionType === "assign") {
        this.assignRequestId = this.actionData.id;
        this.maximumTarget = this.actionData.number;
        this.defaultItem = defaultItem;
        this.showAssignModal();
      } else if (this.actionType === "assignTALead") {
        this.showAssignTALeadModal(payload);
      } else if (this.actionType === "duplicate") {
        this.$store.dispatch("rtsRequestDetailGenenalInfo/getRtsRequestDetailById", this.actionData.id).then((res) => {
          this.$store.dispatch("request/updateDataDuplicate", { check: true, data: res });
        });
        setTimeout(() => {
          this.goEditPage();
        }, [500]);
        // this.showAssignTALeadModal(payload);
      } else if (
        this.actionType === "approve" ||
        this.actionType === "approve2" ||
        this.actionType === "reject" ||
        this.actionType === "submit" ||
        this.actionType === "close" ||
        this.actionType === "share"
      ) {
        // let deadline = this.actionData?.deadline;
        // if (!checkExpired([deadline])) {
        //   this.$message.error("This request is out of date !");
        //   return;
        // }
        this.handleAction();
      }
    },
    showAssignModal() {
      this.$store.dispatch("rtsRequest/getAssigneeList").then(() => {
        this.isShowAssignModal = true;
      });
    },
    showAssignTALeadModal(payload) {
      this.isShowAssignTALeadModal = true;
      this.$store.dispatch("rtsRequest/getAssigneeTALeadList");
    },
    onOkeAssignModal(payload) {
      this.isShowAssignModal = false;
      this.$store.dispatch("user/loadingRequest");
      this.$store
        .dispatch("rtsRequest/assignRtsRequest", {
          recruiters: payload,
          requestId: this.assignRequestId,
        })
        .then(() => {
          this.$store.dispatch("user/loadingRequest");
          this.$message.success(`Assign request successfully!`);
          this.getRequestData(this.pageOptions);
        })
        .catch((err) => {
          this.$message.error(err.message, 5);
          this.$store.dispatch("user/loadingRequest");
        })
        .finally(() => {});
    },
    onCancelAssignModal() {
      this.isShowAssignModal = false;
    },
    onOkeAssignModalTALead(payload) {
      this.isShowAssignTALeadModal = false;
      this.$store
        .dispatch("rtsRequest/assignTALeadRtsRequest", { createdBy: payload, requestId: this.actionData.id })
        .then((res) => {
          this.$store.dispatch("user/loadingRequest");
          setTimeout(() => {
            if (res.status === 200) {
              this.$message.success(`Assign TA Lead successfully!`);
              this.getRequestData(this.pageOptions);
            } else {
              this.$message.error(res.message, 5);
            }
          }, 500);
        })
        .catch((err) => {
          this.$message.error(err.message, 5);
        });
    },

    onCancelAssignModalTALead() {
      this.isShowAssignTALeadModal = false;
    },

    goEditPage(id) {
      this.$router.push({ name: "RequestEdit", params: { id: id } });
    },
    goDetailPage(id, tab = "general-information") {
      this.$router.push({ name: "RequestDetail", params: { id: id, tab } });
    },
    getApproveList() {
      this.$store.dispatch("rtsRequest/getApproveList", this.userInfo.userName);
    },

    onMultiAction(payload) {
      this.actionType = payload.type;
      this.actionData = payload.data;

      // let dateList = this.actionData.map((item) => {
      //   return item.deadline;
      // });
      //
      //
      // if (!checkExpired(dateList)) {
      //   console.log('go here')
      //   this.$message.error("This request is out of date !");
      //   return;
      // }
      if (this.actionType === "submit") {
        if (this.actionData.length > 0) {
          this.defaultPopupItem = 1020;
        }
        this.getApproveList();
      }
      this.handleAction();
    },
    handleAction() {
      this.actionData = this.wrapDataIntoArray(this.actionData);
      if (this.actionType === "submit") {
        // mock data
        // this.defaultPopupItem = 1220
        this.getApproveList();
      }
      this.showActionConfirm(this.actionType);
    },
    wrapDataIntoArray(data) {
      return [data].flat();
    },
    showActionConfirm(type) {
      switch (type) {
        case "approve":
        case "reject":
        case "close":
          this.titleActionModal = "Please Confirm";
          this.dialogMessageActionModal = `Do you want to ${type} this requests?`;
          break;
        case "share":
          this.titleActionModal = "Please Confirm";
          this.dialogMessageActionModal = `Do you want to ${type} this requests?`;
          break;
        case "submit":
          this.titleActionModal = "Submit request";
          this.dialogMessageActionModal = "";
          break;
        default:
          break;
      }
      this.isShowActionModal = true;
    },
    onCancelPopupAction() {
      this.isShowActionModal = false;
    },
    onOkPopupAction(payload) {
      const { additionalData } = payload;
      let ids = this.actionData.map((item) => item.id);
      this.submitAction(this.actionType, {
        ids: ids,
        additionalData: additionalData,
      });
      this.isShowActionModal = false;
    },
    submitAction(type, data) {
      if (type === "share") {
        this.$store.dispatch("rtsRequest/getRtsRequestById", data.ids.toString()).then((res) => {
          res.requestAssigns.map((item) => {
            this.listHr.push(item.assignLdap);
          });
          if (this.listHr.toString().includes(this.userInfo.userName) || this.userInfo.userRoles[1] == "RTS-Admin") {
            this.$store.dispatch("user/loadingRequest");
            this.$store
              .dispatch(`rtsRequest/${type}RtsRequest`, data)
              .then(() => {
                this.refreshSelect();
                this.$store.dispatch("user/loadingRequest");
                this.$message.success(`${capitalizeFirstLetter(type)} request successfully!`);
                setTimeout(() => {
                  let routeData = this.$router.resolve({
                    name: "JobPosting",
                    params: { candidateId: "0" },
                  });
                  window.open(routeData.href, "_blank");
                }, 200);
              })
              .catch((err) => {
                this.$message.error(err.message, 5);
                this.$store.dispatch("user/loadingRequest");
              });
          } else {
            this.$message.error("You don't have permission");
          }
        });
      } else {
        if (this.currentStatus === "Approved" && type !== "reject") {
          this.$store.dispatch("user/loadingRequest");
          this.$store
            .dispatch(`rtsRequest/approve2RtsRequest`, data)
            .then(() => {
              this.refreshSelect();
              this.$store.dispatch("user/loadingRequest");
              this.$message.success(`${capitalizeFirstLetter(type)} request successfully!`);
            })
            .catch((err) => {
              this.$message.error(err.message, 5);
              this.$store.dispatch("user/loadingRequest");
            });
        } else {
          this.$store.dispatch("user/loadingRequest");
          this.$store
            .dispatch(`rtsRequest/${type}RtsRequest`, data)
            .then(() => {
              this.refreshSelect();
              this.$store.dispatch("user/loadingRequest");
              this.$message.success(`${capitalizeFirstLetter(type)} request successfully!`);
            })
            .catch((err) => {
              this.$message.error(err.message, 5);
              this.$store.dispatch("user/loadingRequest");
            });
        }
      }
    },
  },
};
