import {mapState, mapActions} from 'vuex'
export default {
    computed:{
        ...mapState({
            listPeople:state => state.rtsProgressHistory.listPeople
        })
    },
    methods: {
        ...mapActions(['rtsProgressHistory/RtsRequestDetailHistory'])
      },

    mounted() {
        const requestId = this.$route.params.id
        if(requestId) this['rtsProgressHistory/RtsRequestDetailHistory'](requestId);
    },  
}