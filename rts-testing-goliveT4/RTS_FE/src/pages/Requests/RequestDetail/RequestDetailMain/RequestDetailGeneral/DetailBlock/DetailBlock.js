import moment from "moment";

export default {
  name: "DetailBlock",
  components: {},
  props: {
    requestDetail: {
      type: Object,
    },
  },
  async created() {},
  computed: {},
  methods: {
    checkDeadline(date) {
      return moment(date, "YYYY-MM-DD").isBefore(moment(new Date()).subtract(1, "days"));
    },
    //         formatNumberToVND(data) {
    //         try {
    //         let salary = parseInt(data);
    //         if (!isNaN(salary) && typeof salary == 'number') {
    //             salary = salary.toLocaleString(undefined, {minimumFractionDigits: 0, currency: "VND"});
    //             salary = String(salary) + ' VND/Month';
    //             return salary
    //         } else {
    //             return data;
    //         }
    //     }   catch (error) {
    //         return data
    //     }
    // }
  },
};
