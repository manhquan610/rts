
import { mapState, mapActions } from "vuex";
import ContentInTabComments from "../../../../../../components/ContentInTab/ContentInTabComments";
import ContentsInTabHistory from "../../../../../../components/ContentInTab/ContentsInTabHistory";
import ContentInTabAll from '../../../../../../components/ContentInTab/ContentInTabAll';
export default {
  components: {
    ContentInTabComments,
    ContentsInTabHistory,
    ContentInTabAll
  },
  data(){
    return {
    }
  },
  async created(){
     const payload = {
       page:1,
       requestId:this.$route.params.id
     }
     await this.getListComment(payload)
     await this.getListHistory()
  },
  computed: {
    ...mapState({
      tabs: (state) => state.rtsActivity.tabs,
      listComment: (state) => state.rtsActivity.listComment,
      listHistory: (state) => state.rtsActivity.listHistory,
    }),
    activityKeyTab: {
      get() {
        return this.$store.state.rtsActivity.keyTab
      },
      set(newVal) {
        this.$store.commit('rtsActivity/SET_ACTIVITY_TAB',newVal)
      },
    },
  },
  methods: {
    ...mapActions({
      getListComment: 'rtsActivity/getListComment',
      getListHistory: 'rtsActivity/getListHistory',
    }),
    handleChangeTab() {},
  },
};
