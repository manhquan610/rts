import {mapState, mapActions} from 'vuex'
export default {
    computed:{
        ...mapState({
            progress: state => state.rtsProgressHistory.progress ,
            progressStep: state => state.rtsProgressHistory.progressStep ,
        })
    },
    methods:{
        ...mapActions(['rtsProgressHistory/RtsRequestDetailHistory']),
        renderEl(des, name){
            return <a href="#">{des}</a>
        },
    },
    mounted() {
        const requestId = this.$route.params.id
        if(requestId) this['rtsProgressHistory/RtsRequestDetailHistory'](requestId);
    },
}
