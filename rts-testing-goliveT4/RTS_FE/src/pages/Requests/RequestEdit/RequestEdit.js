import moment from "moment";
import { mapState } from "vuex";
import { activeStage, isNumeric } from "../../../components/commons/commons";
import VueSelect from "../../../components/base/VueSelect";
import { MESSAGE_DURATION, PAGE_OPTIONS_KEY } from "../../../components/commons/commonConstant";
import { checkRequestPermission } from "../../../service/authorService";
import VueTinyEditor from "@/components/base/VueTinyEditor";

export default {
  name: "RequestDetail",
  data() {
    return {
      // listJobTypes: [],
      listJobLevels: [],
      checkRequestPermission,
      isCreateAnother: false,
      isEdit: false,
      firstJobType: "",
      workingTime: [
        { name: "FullTime" },
        { name: "PartTime" },
        { name: "FreeLancer" },
        { name: "Remote" },
        { name: "Collaborator" },
        { name: "Intern" },
      ],
      request: {
        title: "",
        requestType: "",
        priority: "",
        deadline: "",
        number: "",
        position: "",
        project: "",
        department: "",
        group: "",
        skills: "",
        experience: "",
        major: "",
        certificate: "",
        description: "",
        language: "",
        salary: "",
        benefit: "",
        area: "",
        positionSF4C: "",
        jobType: "",
        jobLevel: "",
        requirement: "",
        workingTime: "",
        codePosition: "",
      },
      rules: {
        title: [
          { required: true, message: "Please input title", trigger: "change" },
          { max: 255, message: "You’ve exceeded the limit by 255 characters", trigger: "change" },
        ],
        requestType: [{ required: true, message: "Please choose request type", trigger: "change" }],
        priority: [{ required: true, message: "Please choose priority", trigger: "change" }],
        deadline: [{ required: true, message: "Please choose date", trigger: "change" }],
        number: [
          { required: true, message: "Please input target", trigger: "change" },
          { validator: this.isNumberAndLessThan1000, trigger: "change" },
        ],
        position: [{ required: true, message: "Please choose position", trigger: "change" }],
        group: [{ required: true, message: "Please choose group", trigger: "change" }],
        department: [{ required: true, message: "Please choose department", trigger: "change" }],

        skills: [{ required: true, message: "Please choose skill", trigger: "change" }],
        experience: [{ required: true, message: "Please choose experience", trigger: "change" }],
        major: [],
        certificate: [{ max: 255, message: "You’ve exceeded the limit by 255 characters", trigger: "change" }],
        description: [
          { required: true, message: "  ", trigger: "change" },
          { max: 4000, message: "  ", trigger: "change" },
        ],
        language: [],
        salary: [{ required: false, message: "Please input number", trigger: "change" }],
        benefit: [{ max: 4000, message: "You’ve exceeded the limit by 4000 characters", trigger: "change" }],
        requirement: [{ max: 4000, message: "You’ve exceeded the limit by 4000 characters", trigger: "change" }],
        area: [{ required: true, message: "Please choose are", trigger: "change" }],
        codePosition: [
          { required: true, message: "Please input position sf4c", trigger: "change" },
          { max: 255, message: "You’ve exceeded the limit by 255 characters", trigger: "change" },
        ],
        jobType: [
          { required: true, message: "Please choose position", trigger: "change" },
          /*{max: 255, message: 'You’ve exceeded the limit by 255 characters', trigger: 'change'},*/
        ],
        jobLevel: [
          { required: true, message: "Please choose job code", trigger: "change" },
          /*{max: 255, message: 'You’ve exceeded the limit by 255 characters', trigger: 'change'},*/
        ],
        workingTime: [],
      },
    };
  },
  components: {
    VueSelect,
    VueTinyEditor,
  },
  computed: {
    ...mapState({
      userRole: (state) => state.user.userRole,
      skills: (state) => state.rtsApplication.skillList,
      languages: (state) => state.rtsApplication.languageList,
      groups: (state) => state.rtsApplication.groupList,
      projects: (state) => state.rtsApplication.projectList,
      departments: (state) => state.rtsApplication.departmentList,
      majors: (state) => state.major.majorList,
      requestTypes: (state) => state.requestType.requestTypeList,
      experiences: (state) => state.experience.experienceList,
      priorities: (state) => state.priority.priorityList,
      areas: (state) => state.rtsShareJob.area,
      listJobTypes: (state) => state.rtsShareJob.jobType,
      dataDuplicate: (state) => state.request.dataDuplicate,
      /*positionSF4C: (state) => state.rtsShareJob.positionSF4C,
            jobTypes: (state) => state.jobType.jobLevelList1*/
    }),
  },
  async created() {
    let pageOptions = {
      offset: 1,
      limit: 1000,
      like: "0",
    };
    /*let payloadJobLevel = {
            page: 1,
            limit: 1000,
            nameSearch: this.request.positionSF4C
        }*/
    await this.$store.dispatch("user/loadingRequest");
    await this.$store.dispatch("rtsApplication/getProjectList");
    await this.$store.dispatch("rtsApplication/getGroupList");
    await this.$store.dispatch("rtsApplication/getSkillList", "");
    await this.$store.dispatch("rtsApplication/getLanguageList");
    await this.$store.dispatch("major/getMajorList", pageOptions);
    await this.$store.dispatch("requestType/getRequestTypeList", pageOptions);
    await this.$store.dispatch("experience/getExperienceList", pageOptions);
    await this.$store.dispatch("priority/getPriorityList", pageOptions);
    await this.$store.dispatch("rtsShareJob/getAreas");
    /*await this.$store.dispatch("rtsShareJob/getPositionSF4C")
        await this.$store.dispatch("jobType/getJobLevelList", payloadJobLevel)*/
    await this.$store.dispatch("rtsShareJob/getJobType");

    const requestId = this.$route.params.id;
    if (requestId) {
      this.isEdit = true;
      await this.getRequestById(requestId).then((res) => {
        this.request = res;
        this.firstJobType = res.jobType;
        if (!res.department) {
          this.request.department = res.group;
        }

        /*const codePosition = this.request.codePosition
                for (let i = 0; i < this.positionSF4C.length; i++) {
                    if (String(this.positionSF4C[i].codeName).trim().toUpperCase() === String(codePosition).trim().toUpperCase()) {
                        this.request.positionSF4C = this.positionSF4C[i]
                        break;
                    }
                }*/
      });
    }
    await this.$store.dispatch("user/loadingRequest");
  },

  watch: {
    "request.group": {
      immediate: true,
      handler(newVal) {
        if (this.departments.length === 0) {
          this.departments.push(newVal);
          this.request.department = newVal;
        }
      },
    },
    /*'request.positionSF4C': {
            immediate: true,
            handler(newVal) {
                let payloadJobLevel = {
                    page: 1,
                    limit: 1000,
                    nameSearch: newVal.name
                }
                this.$store.dispatch("jobType/getJobLevelList1", payloadJobLevel).then(res => {
                    this.request.jobLevel = '';
                    this.request.jobType = '';

                    this.jobTypes.forEach((value) => {
                        if (value.name == newVal.name) {
                            this.request.jobLevel = {
                                id: value.jobType.id,
                                name: value.jobType.name
                            };
                            this.request.jobType = {
                                id: value.id,
                                name: value.name
                            };

                            this.request.salary = this.findSalary(value.salaryMin, value.salaryMax)
                        }
                    });
                });

            }
        },*/
    "request.jobType": {
      immediate: true,
      handler(newVal) {
        if (newVal === this.firstJobType) {
          let payloadJobLevel = {
            page: 1,
            limit: 1000,
            idJobType: newVal.id,
          };
          this.$store.dispatch("jobType/getJobLevelList", payloadJobLevel).then((res) => {
            this.listJobLevels = res.content;
          });
        } else {
          let payloadJobLevel = {
            page: 1,
            limit: 1000,
            idJobType: newVal.id,
          };
          this.$store.dispatch("jobType/getJobLevelList", payloadJobLevel).then((res) => {
            this.listJobLevels = res.content;
          });
          this.request.jobLevel = undefined;
          this.request.salary = "";
        }
      },
    },

    "request.jobLevel": {
      immediate: true,
      handler(newVal) {
        if (newVal) {
          this.request.jobLevel = newVal;
          this.request.salary = this.findSalary(newVal.salaryMin, newVal.salaryMax);
        }
      },
    },
  },
  beforeRouteUpdate(to, from, next) {
    this.resetData();
    next();
  },
  beforeMount() {
    if (this.dataDuplicate && this.dataDuplicate.check) {
      const data = this.dataDuplicate.data;
      this.request = data;
      this.firstJobType = data.jobType;
      if (!data.department) {
        this.request.department = data.group;
      }
    }
  },
  methods: {
    resetData() {
      this.request = {
        title: "",
        requestType: "",
        priority: "",
        deadline: "",
        number: "",
        position: "",
        project: "",
        department: "",
        group: "",
        skills: "",
        experience: "",
        major: "",
        certificate: "",
        description: "",
        language: "",
        salary: "",
        benefit: "",
        requirement: "",
        area: "",
        jobType: "",
        workingType: "",
        jobLevel: "",
      };
      this.isCreateAnother = false;
      this.isEdit = false;
    },
    getRequestById(id) {
      return this.$store.dispatch("rtsRequestDetailGenenalInfo/getRtsRequestDetailById", id).then((res) => {
        return res;
      });
    },

    getDepartmentListByGroup(ids) {
      this.$store.dispatch("rtsApplication/getDepartmentListByGroup", ids);
    },
    isNumberAndLessThan1000(rule, value, callback) {
      if (value) {
        if (!isNumeric(value)) {
          callback("Input must be type of number!");
        }
        if (Number(value) > 1000) {
          callback("Number must be less than 1000!");
        }
        if (Number(value) == 0) {
          callback("Number must be greater than zero");
        }
      }
      callback();
    },
    checkPrice(rule, value, callback) {
      if (value) {
        if (!isNumeric(value)) {
          callback("Input must be type of number!");
        }
        if (Number(value) <= 0) {
          callback("Salary must be greater than zero");
        }
      }
      callback();
    },
    filterOption(input, option) {
      return option.componentOptions.children[0].text.toLowerCase().indexOf(input.toLowerCase()) >= 0;
    },
    disabledDate(current) {
      // Can not select days before today and today
      return current && current < moment().startOf("day");
    },
    onCancel(e) {
      e.preventDefault();
      let text = "Do you want to cancel this request";
      this.showConfirmDialog("danger", text, this.goRequestListPage);
    },
    onSubmit(e) {
      e.preventDefault();
      let isPass = this.checkValidate();
      this.$store.commit("rtsApplication/SET_IS_PASS", isPass);
      if (isPass) {
        let text = this.isEdit ? `Do you want to change this request?` : `Do you want to save this request?`;
        this.showConfirmDialog(null, text, this.handleSubmit);
      }
    },
    checkValidate() {
      let flag = false;
      this.$refs.ruleForm.validate((valid) => {
        flag = valid;
      });
      return flag;
    },
    showConfirmDialog(okType, text, handleOke) {
      this.$confirm({
        title: "Please Confirm",
        content: text,
        okText: "Confirm",
        okType: okType,
        onOk: () => {
          handleOke();
          this.$store.commit("rtsApplication/SET_IS_PASS", true);
        },
        cancelText: "Cancel",
      });
    },
    async handleSubmit() {
      this.showLoading();
      try {
        let request = await this.formatDataBeforeSave(this.request);
        let res = await this.submitData(request);
        this.goNextPage(() => {
          this.$store.dispatch("request/updateDataDuplicate", { check: false, data: {} });
          this.showMessage("success", res.message, MESSAGE_DURATION);
        });
      } catch (err) {
        this.showMessage("error", err.message, MESSAGE_DURATION);
      }
      this.hideLoading();
    },
    goNextPage(callBack) {
      this.isCreateAnother ? this.goRequestDetailPage(callBack) : this.goRequestListPage(callBack);
    },
    showMessage(type, message, delay) {
      if (type === "success") {
        this.$message.success(message, delay);
      }
      if (type === "error") {
        this.$message.error(message, delay);
      }
    },
    showLoading() {
      this.$store.dispatch("user/loadingRequest");
    },
    hideLoading() {
      this.$store.dispatch("user/stopLoading");
    },
    submitData(payload) {
      return new Promise((resolve, reject) => {
        if (this.isEdit) {
          
          this.$store
            .dispatch("rtsRequest/updateRtsRequest", payload)
            .then((res) => {
              resolve({
                data: res,
                message: "Request was update successfully!",
              });
            })
            .catch((err) => {
              reject(err);
            });
        } else {
          delete payload.id
          this.$store
            .dispatch("rtsRequest/createRtsRequest", payload)
            .then((res) => {
              resolve({
                data: res,
                message: "Request was created successfully!",
              });
            })
            .catch((err) => {
              reject(err);
            });
        }
      });
    },
    findSalary(min, max) {
      return this.formatNumberToVND(min) + " - " + this.formatNumberToVND(max);
    },
    formatNumberToVND(data) {
      try {
        let salary = parseInt(data);
        if (!isNaN(salary) && typeof salary == "number") {
          salary = salary.toLocaleString(undefined, { minimumFractionDigits: 0, currency: "VND" });
          salary = String(salary) + " VND";
          return salary;
        } else {
          return data;
        }
      } catch (error) {
        return data;
      }
    },
    goRequestListPage(callBack) {
      if (this.isEdit) {
        activeStage(PAGE_OPTIONS_KEY);
      }
      this.$router.push("/rts/requests", callBack);
      this.$store.dispatch("request/updateDataDuplicate", { check: false, data: {} });
    },
    goRequestDetailPage(callBack) {
      if (this.isEdit) {
        this.$router.push("/rts/requests/edit", callBack);
      } else {
        this.resetData();
        callBack();
      }
    },
    formatDataBeforeSave(payload) {
      return {
        id: payload.id ? payload.id : undefined,
        title: payload.title,
        positionId: 172,
        deadline: moment(payload.deadline).format("YYYY-MM-DD"),
        number: payload.number,
        description: payload.description,
        skillId: payload.skills ? this.convertObjectArrToIdArr(payload.skills)?.toString() : "",
        priorityId: payload.priority ? payload.priority.id : null,
        experienceId: payload.experience ? payload.experience.id : null,
        certificate: payload.certificate,
        major: payload.major ? payload.major.id : null,
        salary: payload.salary,
        benefit: payload.benefit,
        requirement: payload.requirement,
        languageId: payload.language ? this.convertObjectArrToIdArr(payload.language)?.toString() : "",
        requestTypeId: payload.requestType ? payload.requestType.id : null,
        projectId: payload.project?.projectId,
        managerGroupId: payload.group?.id,
        departmentId: payload.department?.id,
        area: payload.area,
        positionSF4C: payload.positionSF4C,
        jobType: payload.jobType ? payload.jobType : [null],
        idJobLevel: payload.jobLevel ? [payload.jobLevel.id] : [null],
        workingType: payload.workingTime?.name,
        codePosition: payload.codePosition,
        // codePosition: payload.positionSF4C.codeName
      };
    },
    convertObjectArrToIdArr(arr) {
      return arr?.map((item) => item.id);
    },
    getDepartmentByGroup() {
      this.request.department = "";
      this.$refs.ruleForm.clearValidate("group");
      if (!this.request.group) {
        this.$refs.ruleForm.resetFields("department");
      }
      const groupId = this.request.group ? this.request.group.id : null;
      this.$store.dispatch("rtsApplication/getDepartmentListByGroup", [groupId]);
    },
  },
};
