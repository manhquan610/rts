import ConfigurationTable from "../../../components/configurations/ConfigurationTable";
import { mapState } from "vuex";
import { months, currentYear } from "../../../store/modules/configurationModule/kpi.js";
import { Object } from "core-js";

let columns = [
    {
        title: "Name",
        dataIndex: "fullName",
        key: "fullName",
        sorter: (a, b) => a.fullName.localeCompare(b.fullName),
    },
    {
        title: "Total",
        dataIndex: "total",
        key: "total",
        scopedSlots: { customRender: "total" },
        width: '6%',
        'background-color': '#ddd'
    }
];
let firstLoad = true;

export default {
    name: "KPI",
    components: {
        ConfigurationTable,
    },
    props: {
        filters: {type: Object},
        getExportConfig:{type: Function}
    },
    data() {
        if(firstLoad){
            months.forEach(m => {
                columns.push({
                    title: m,
                    dataIndex: m.toLowerCase().substring(0, 3)+'Quantity',
                    key: m.toLowerCase().substring(0, 3)+'Quantity',
                    scopedSlots: { customRender: "monthKPI" },
                    width: '6%',
                })
            });
            columns.push({
                title: "Action",
                scopedSlots: { customRender: "action" },
                width: '5%',
            });
            firstLoad = false;
        }
        return {
            columns: columns
        };
    },
    computed: {
        ...mapState({
            userInfo: (state) => state.user.userInfo,
            kpiList: (state) => state.kpi.empKPIList,
            kpiListBak: (state) => state.kpi.empKPIListBak,
            filtersStore: (state) => state.kpi.filtersStore,
            //exam of a change when click search or reset from configurations.js then currentPage and pageSize update on default
            pages: (state) => state.configurations.pages,
            editPermissed: (state) => ['RTS-Admin', 'RTS-TA-MNG'].includes(state.user.userInfo.userRoles[1])
        })
    },
    created() {
       this.getExportConfig = this.getKPIExportConfig; 
    },
    mounted() { },
    watch:{
    },
    methods: {
        editHandle(record) {
            let payload = {
                record: record,
                urlParams: {
                    year: this.filtersStore['year'],
                    username: record.username
                }
            }
            this.$confirm({
                title: "Save KPI",
                content: "Do you really want to save KPI?",
                okText: "Confirm",
                okType: 'primary',
                onOk: () => {
                    this.$store
                    .dispatch("kpi/updateKPI",  payload)
                    .then((res) => {
                        //record = {...res.data}
                        //console.log('dispatch save kpi record this.$confirm: ', this.$confirm);
                        this.$message.success('Save succeeded!');
                        this.updateRecord(record, res.data);
                        this.recordEditModeToggle(record);
                    })
                    .catch((err) => {
                        this.$message.error(err);
                        this.$store.dispatch("user/loadingRequest");
                    });
                },
                cancelText: "Cancel",
              });
            //console.log('onEdit(payload): ', record, this.filtersStore);
        },
        onDelete(payload) {
            this.$emit("actionDelete", payload);
        },
        onSelect(payload) {
            this.$emit("actionSelect", payload);
        },
        onChangePage(page, size) {
            //console.log('kpi onChangePage', page, size, this.userInfo, this.editPermissed);
            this.pages.pageSize = size;
            this.pages.currentPage = page;
            let payload = {...this.filtersStore};
            payload['limit']  = size;
            payload['offset']  = page;
            this.$store
            .dispatch("kpi/getKPIList", payload)
            .then(() => {
            })
            .catch((err) => {
                this.$message.error(err.message, 5);
                this.$store.dispatch("user/loadingRequest");
            });
        },
        onShowTotal(total, range) {
            return `${range[0]}-${range[1]} of ${total} items`;
        },
        isRecordOnEdit(record){
            return(record['onEdit']);
        },
        editItem(record, key, val){
            console.log('editItem(record, key, val): ', record, key, val);
        },
        recordEditModeToggle(record){
            console.log('recordEditModeToggle, record: ', record);
            record['onEdit'] = !record['onEdit'];
            if(record['onEdit']){
                Array.from(document.getElementsByClassName('editOff'+record.key)).forEach(e => {
                    this.hideElement(e);
                });
                Array.from(document.getElementsByClassName('editOn'+record.key)).forEach(e => {
                    this.showElement(e);
                });
            }else{
                Array.from(document.getElementsByClassName('editOff'+record.key)).forEach(e => {
                    this.showElement(e);
                });
                Array.from(document.getElementsByClassName('editOn'+record.key)).forEach(e => {
                    this.hideElement(e);
                });
            }
        },
        hideElement(element){
            element.style.display = 'none';
        },
        showElement(element){
            element.style.display = 'block';
        },
        discardRecordChange(record){
            this.$confirm({
                title: "Save KPI",
                content: "Do you want to discard changes?",
                okText: "Yes",
                okType: 'primary',
                onOk: () => {
                    let recordBak = JSON.parse(JSON.stringify(this.kpiListBak[this.kpiList.indexOf(record)]));
                    months.forEach(m=>{
                        let monthKey = m.toLowerCase().substring(0,3) + 'Quantity';
                        //document.getElementById('input'+record.key+monthKey).value = recordOldVals[monthKey].value;
                        record[monthKey].value = recordBak[monthKey].value;
                    });
                    this.recordEditModeToggle(record);
                }
            });
            //console.log('this.kpiListBak[this.kpiList.indexOf(record)]: ', this.kpiList.indexOf(record), this.kpiListBak[this.kpiList.indexOf(record)], record);
        },
        updateRecord(old, new1){
            let recordBak = this.kpiListBak.find(k => k.username == old.username);
            let index = this.kpiListBak.indexOf(recordBak);
            this.kpiListBak[index] = JSON.parse(JSON.stringify(new1));
            months.forEach(m=>{
                let monthKey = m.toLowerCase().substring(0,3) + 'Quantity';
                old[monthKey].value = new1[monthKey].value;
            });
            old['total'] = new1['total'];
            console.log('updateRecord(record)4: ', this.kpiListBak, this.kpiList, recordBak, index);
        },
        justSetColorParent(childId){
            let child = document.getElementById(childId);
            //let child1 = document.getElementById('pTotalntdung10');
            //console.log('justSetColorParent(childId): ', childId, child, child1);
            if(!child){
                setTimeout(()=>{this.justSetColorParent(childId)}, 300);
                return null;
            }
            child.parentElement.style.backgroundColor = "#e4e4e4";
            return null;
        }
    },
};
