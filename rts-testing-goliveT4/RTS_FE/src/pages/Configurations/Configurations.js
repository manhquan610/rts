import FilterLayout from "../../components/layouts/FilterLayout";
import { mapState } from "vuex";
import ConfigurationModal from "../../components/configurations/ConfigurationModal";
import ConfigurationModalJobLevel from "../../components/configurations/ConfigurationModalJobLevel";
import ConfirmDialog from "../../components/base/confirmDialog";
import ConfirmCancelDialog from "../../components/base/confirmDialog";
import Positions from "./Positions/Positions.vue";
import KPI from "./KPI/KPI.vue";
import Priority from "./Priority/Priority.vue";
import Major from "./Major/Major.vue";
import RequestType from "./RequestType/RequestType.vue";
import Experience from "./Experience/Experience.vue";
import Area from "./Area/Area.vue";
import JobType from "./JobType/JobType.vue";
import JobLevelTable from "./JobType/JobLevelTable.vue";
import {
  camalize,
  capitalizeFirstLetter,
  convertObjectToParams
} from "../../components/commons/commons";
import { configPanigationDefault } from "../../components/commons/commonConstant";
import axios from 'axios'

const tabs = [
  { title: "KPI", index: 1 },
  { title: "Priority", index: 2 },
  { title: "Experience", index: 3 },
  { title: "Major", index: 4 },
  { title: "Request Type", index: 5 },
  { title: "Area", index: 6 },
  { title: "Job Type", index: 7 },
];
const columnsJobLevel = [
  {
    title: "Title",
    dataIndex: "name",
    key: "name",
    sorter: (a, b) => a.name.localeCompare(b.name),

  },
  {
    title: "Description",
    dataIndex: "description",
    key: "description",
    sorter: (a, b) => a.description.localeCompare(b.description),
  },
  {
    title: "Min Salary",
    dataIndex: "salaryMin",
    key: "salaryMin",
    sorter: (a, b) => a.salaryMin.localeCompare(b.salaryMin),
  },
  {
    title: "Max Salary",
    dataIndex: "salaryMax",
    key: "salaryMax",
    sorter: (a, b) => a.salaryMax.localeCompare(b.salaryMax),
  },
  {
    title: "",
    dataIndex: "action",
    scopedSlots: { customRender: "action" },
  },
];
export default {
  name: "Configurations",
  components: {
    FilterLayout,
    ConfigurationModal,
    ConfirmDialog,
    ConfirmCancelDialog,
    KPI,
    Positions,
    Priority,
    Experience,
    Major,
    RequestType,
    Area,
    JobType,
    JobLevelTable,
    ConfigurationModalJobLevel
  },

  data() {
    return {
      tabs,
      page: "Configurations",
      showDrawer: false,
      columnsJobLevel,
      loading: false,
      isShowModal: false,
      isShowJobLevel: false,
      isShowConfirmJobLevel: false,
      idJobLevel: '',
      editedJobLevel: {},
      dialogMessageJobLevel: '',
      isShowConfirm: false,
      detailJobType: {

      },
      visibleJobLevel: false,
      isEdit: false,
      isEditJobLevel: false,
      isDelete: false,
      isDisableDelete: true,
      formName: "",
      editedItem: {
        name: "",
        description: "",
      },
      deletedItems: [],
      dialogMessage: "",
      dialogMessageCancel:"",
      isShowConfirmCancel: false,
      drawerKey: undefined,
      keyTab: "Priority",
      isHideAddAndDelete: false,
      dataFilters: {},
      currentPage: 1,
      pageSize: 10,
      total_items: 0,
    };
  },
  computed: {
    ...mapState({
      userInfo: (state) => state.user.userInfo,
      pages: (state) => state.configurations.pages,
      jobLevel: (state) => state.jobType.jobLevelList,
      downloadKPIExportLink: (state) => state.kpi.downloadExportLink
    }),
    selectedRowKeys: {
      get: function() {
        return this.$store.state.configurations.selectedRowKeys;
      },
      set: function(newValue) {
        this.$store.commit("configurations/SET_SELECTED_ROW_KEY", newValue);
      },
    },
  },

  watch: {
    $route: {
      immediate: true,
      handler(to, from) {
        document.title = to.meta.title || "Human Resource Management System";
      },
    },
    selectedRowKeys: function() {
      this.isDisableDelete = this.selectedRowKeys.length === 0;
    },
    showDrawer: function() {
      this.myFunction();
    },
    // staffListExcept: function() {
    //   this.onChangeValue();
    // },
    keyTab: function() {
      this.isDisableDelete = true;
      this.dataFilters = {};
      this.selectedRowKeys = [];
      this.$store.commit("configurations/SET_SEARCH_FILTER", []);
    },
    isShowModal: {
      handler: function(newVal) {
        if (newVal === false) {
          this.clearForm();
        }
      },
    },
    isShowJobLevel: {
      handler: function(newVal) {
        if (newVal === false) {
          this.editedJobLevel = {};
        }
      },
    },
  },
  created() {
    // this.scaleScreen();
  },
  updated() {},
  props: {
    isHiddenDelete: {
        default: false,
        type: Boolean,
    }
  },
  mounted() {
    // this.keyTab = this.roles.includes(this.role.ADMIN) || this.roles.includes(this.role.HR) ? 'list' : 'detail'
  },
  methods: {
    // scaleScreen,
    onSetDataFilters(filters) {
      this.dataFilters = filters;
    },
    onApplyFilters(filters) {
      this.dataFilters = filters;
      this.$store.commit("configurations/SET_SELECTED_ROW_KEY", []);
      this.getConfigurationData();
    },
    formatTab(tab){
      if(tab === 'Job Type'){
        return 'Position'
      }else {
        return tab
      }
    },
    filterOption() {},
    onCancel() {

      this.isShowConfirmCancel = true;
      //this.isShowModal = false;
    },
    onCloseJobLevel(){
      this.visibleJobLevel= false
    },
    onOpenJobLevel(payload){
      this.detailJobType = payload
      this.visibleJobLevel= true
      const newPayload = {
        limit : 1000,
        page : 1,
        idJobType: this.detailJobType.id
      };
      this.$store.dispatch("user/loadingRequest");
      this.$store
        .dispatch("jobType/getJobLevelList", newPayload)
        .then((res) => {
          this.$store.dispatch("user/loadingRequest");
        })
        .catch((err) => {
          this.$message.error(err.message, 5);
          this.$store.dispatch("user/loadingRequest");
        });
    },
    onSubmit(payload) {
      if (this.keyTab === "Positions") {
        this.handleSubmit("positions", payload);
      }
      if (this.keyTab === "KPI") {
        this.handleSubmit("kpi", payload);
      }
      if (this.keyTab === "Priority") {
        this.handleSubmit("priority", payload);
      }
      if (this.keyTab === "Experience") {
        this.handleSubmit("experience", payload);
      }
      if (this.keyTab === "Major") {
        this.handleSubmit("major", payload);
      }
      if (this.keyTab === "Request Type") {
        this.handleSubmit("requestType", payload);
      }
      if (this.keyTab === "Area") {
        this.handleSubmit("area", payload);
      }
      if (this.keyTab === "Job Type") {
        this.handleSubmit("jobType", payload);
      }
      this.isShowModal = false;
    },
    handleSubmit(storeModel, payload) {
      const fnName = this.keyTab.replace(/\s/g, "");
      this.$store.dispatch("user/loadingRequest");
      if (this.isDelete) {
        this.$store
          .dispatch(`${storeModel}/delete${fnName}`, { ids: payload })
          .then(() => {
            this.updatePaginate(payload);
            this.fetchPageData(storeModel, this.pages.currentPage);
            this.$message.success("Successfully Deleted!");
            this.$store.dispatch("user/loadingRequest");
          })
          .catch((err) => {
            this.$message.error(err.message, 5);
            this.$store.dispatch("user/loadingRequest");
          });
        // this.deleteElementsInArray(payload, arr);
        this.isDelete = false;
      } else if (this.isEdit) {
        this.$store
          .dispatch(`${storeModel}/update${fnName}`, payload)
          .then(() => {
            this.$message.success("Successfully Changed");
            this.$store.dispatch("user/loadingRequest");
          })
          .catch((err) => {
            this.$message.error(err.message, 5);
            this.$store.dispatch("user/loadingRequest");
          });
        // if response success
        // let foundIndex = arr.findIndex((x) => x.id === payload.id);
        // this.$set(arr, foundIndex, payload);
      } else {
        this.$store
          .dispatch(`${storeModel}/create${fnName}`, payload)
          .then(() => {
            this.fetchPageData(storeModel, 1);
            this.$message.success("Successfully Added");
            this.$store.dispatch("user/loadingRequest");
          })
          .catch((err) => {
            this.$message.error(err.message, 5);
            this.$store.dispatch("user/loadingRequest");
          });
        // if response success
        // payload.id = this.getMaxId(arr) + 1;
        // arr.push(payload);
      }
    },
    fetchPageData(model, offset) {
      if(model === 'area') {
        this.$store.dispatch(`${model}/get${capitalizeFirstLetter(model)}List`, {
          page: offset,
          limit: 10,
          nameSearch:""
        });
      }else if (model === 'jobType'){
        this.$store.dispatch(`${model}/getJobTypeList`, {
          page: offset,
          limit: 10,
          nameSearch:""
        });
      }
      else {
        this.$store.dispatch(`${model}/get${capitalizeFirstLetter(model)}List`, {
          offset: offset,
          limit: 10,
        });
      }
    },
    updatePaginate(payload) {
      if (!payload) return;
      this.pages.total_items -= payload.length;
    },
    onShowDrawerNote() {
      this.showDrawer = true;
      this.drawerKey = "note";
      this.titleDrawer = "Note";
    },

    onCloseDrawer() {
      this.showDrawer = false;
      this.drawerKey = undefined;
      this.titleDrawer = "";
    },

    // validRequired(value) {
    //   return !value ? false : true;
    // },

    myFunction() {
      document.body.style.position = "fixed";
    },

    // closeModal() {
    //   this.visibleModal = false;
    //   this.commentReject = "";
    //   this.isValidComment = true;
    // },

    handleChangeTab(key) {
      this.keyTab = key;
      this.getConfigurationData();
    },
    onAddNew() {
      this.formName = `Add ${this.formatTab(this.keyTab)}`;
      this.isEdit = false;
      this.dialogMessageCancel = `Do you want to cancel create ${this.formatTab(this.keyTab)} ?`
      this.isShowModal = true;
    },
    onAddJobLevel() {
      this.editedJobLevel = {}
      this.isEditJobLevel = false
      this.isShowJobLevel = true 
    },
    onSubmitJobLevel(values){
      values.idJobType = this.detailJobType.id
      const newPayload = {
        limit : 1000,
        page : 1,
        idJobType: this.detailJobType.id
      };
      if(this.isEditJobLevel){
        this.$store.dispatch("jobType/updateJobLevel",values).then((res)=>{
          if(res.status == 200){
            this.$message.success("Successfully Updated!");
          }else{
            this.$message.error(res.message);
          }
          this.$store
          .dispatch("jobType/getJobLevelList", newPayload)
        });
      }else {
        this.$store.dispatch("jobType/createJobLevel",values).then((res)=>{
          if(res.status == 200){
            this.$message.success("Successfully Added!");
          }else{
            this.$message.error(res.message);
          }
          this.$store
          .dispatch("jobType/getJobLevelList", newPayload)
        });
      }
      
      this.isShowJobLevel = false 
    },
    onDeleteJobLevel(record){
      this.isShowConfirmJobLevel = true;
      this.idJobLevel = record.id
      this.dialogMessageJobLevel = 'Do you want to delete this Job Code'
    },
    handleOkeJobLevel(){
      const payload = {
        idJobLevel: this.idJobLevel
      }
      const newPayload = {
        limit : 1000,
        page : 1,
        idJobType: this.detailJobType.id
      };
      this.$store.dispatch("jobType/deleteJobLevel",payload).then((res) => {
          if(res.status == 200){
            this.$message.success("Successfully Deleted!");
         }
        this.$store
        .dispatch("jobType/getJobLevelList", newPayload)
        
      }).catch((err) => {
        this.$message.error(err.message, 5);
      });
      this.isShowConfirmJobLevel = false
    },
    handleCancelJobLevel(){
      this.isShowConfirmJobLevel = false
    },
    onEditJobLevel(record){
      this.editedJobLevel = record
      this.isEditJobLevel = true
      this.isShowJobLevel = true 
    },
    onCancelJobLevel(){
      this.isShowConfirmCancel = true
      this.dialogMessageCancel = `Do you want to cancel ${this.isEditJobLevel ? 'edit' : 'add'} this Job Code?`;
    },
    onEdit(payload) {
      this.formName = `Edit ${this.formatTab(this.keyTab)}`;
      this.editedItem = payload;
      this.dialogMessageCancel = `Do you want to cancel edit this ${this.formatTab(this.keyTab)}?`;
      this.isEdit = true;
      this.isShowModal = true;
    },

    onDelete(payload) {
      if (payload) {
        this.deletedItems = [payload.id];
      }
      this.isDelete = true;
      this.dialogMessage = `Do you want to delete this ${this.formatTab(this.keyTab)}?`;
      this.isShowConfirm = true;
    },

    handleOke() {
      this.onSubmit(this.deletedItems);
      this.selectedRowKeys = [];
      this.isShowConfirm = false;
    },
    handleCancel() {
      this.isShowConfirm = false;
    },
    handleOkeCancel(){
      this.isShowConfirmCancel = false;
      this.isShowJobLevel = false;
      this.isEditJobLevel = false
      this.editedJobLevel = {}
      this.isShowModal = false
    },
    handleCancelDialog(){
      this.isShowConfirmCancel = false;
    },
    onSelect(payload) {
      this.selectedRowKeys = payload;
      this.deletedItems = payload;
    },

    clearForm() {
      this.editedItem = {
        name: "",
        description: "",
      };
    },
    getMaxId(arr) {
      if (arr && arr.length === 0) {
        return 1;
      }
      let max = arr.reduce(function(prev, current) {
        if (+current.id > +prev.id) {
          return current;
        } else {
          return prev;
        }
      });
      return max.id;
    },

    getConfigurationData() {
      //console.log('getConfigurationData');
      let { keyTab } = this;
      let fnName = keyTab.replace(/\s/g, "");
      let camelCaseTab = camalize(keyTab);
      let name = "";
      if ("configurations" in this.dataFilters) {
        name = this.dataFilters["configurations"];
      }
      let payload = {
        offset: configPanigationDefault.currentPage,
        limit: configPanigationDefault.pageSize,
        lstName: name,
      };
      Object.keys(this.dataFilters).forEach(element => {
        payload[element] = this.dataFilters[element];
      });
      if (keyTab === "Area" || keyTab === "Job Type") {
        let params = {
          page: configPanigationDefault.currentPage,
          limit: configPanigationDefault.pageSize,
          nameSearch: name,
        };
        this.$store.dispatch("user/loadingRequest");
        this.$store
          .dispatch(`${camelCaseTab}/get${fnName}List`, params)
          .then(() => {
            this.$store.dispatch("user/loadingRequest");
          })
          .catch((err) => {
            this.$message.error(err.message, 5);
            this.$store.dispatch("user/loadingRequest");
          });
      } else {
        this.$store.dispatch("user/loadingRequest");
        this.$store
          .dispatch(`${camelCaseTab}/get${fnName}List`, payload)
          .then(() => {
            this.$store.dispatch("user/loadingRequest");
          })
          .catch((err) => {
            this.$message.error(err.message, 5);
            this.$store.dispatch("user/loadingRequest");
          });
      }
    },
    downloadItem(){
      let downloadLink = this['download'+this.keyTab+ 'ExportLink'];
      console.log('conf downloadItem: ', downloadLink);
      let fileName = this.keyTab+'-Export.xlsx';
      axios.get(downloadLink, { responseType: 'blob' })
        .then(response => {
          console.log('response: ', response, fileName)
          const blob = new Blob([response.data], { type: response.type })
          const link = document.createElement('a')
          link.href = URL.createObjectURL(blob)
          link.download = fileName
          link.click()
          URL.revokeObjectURL(link.href)
        }).catch(console.error)
    }
  },
};
