import ConfigurationTable from "../../../components/configurations/ConfigurationTable";
import { mapState } from "vuex";

const columns = [
  {
    title: "Request Type Name",
    dataIndex: "name",
    key: "name",
    sorter: (a, b) => a.name.localeCompare(b.name),
  },
  {
    title: "Description",
    dataIndex: "description",
    key: "description",
    sorter: (a, b) => a.description.localeCompare(b.description),
  },
  {
    title: "",
    dataIndex: "action",
    scopedSlots: { customRender: "action" },
  },
];
export default {
  name: "RequestType",
  components: {
    ConfigurationTable,
  },
  data() {
    return {
      columns: columns,
      loading: false,
    };
  },
  computed: {
    ...mapState({
      requestsType: (state) => state.requestType.requestTypeList,
      //exam of a change when click search or reset from configurations.js then currentPage and pageSize update on default
      pages: (state) => state.configurations.pages,
    }),
  },
  watch: {},
  created() {},
  updated() {},
  props: {
    actionEdit: {
      type: Function,
    },
    actionDelete: {
      type: Function,
    },
    actionSelect: {
      type: Function,
    },
  },
  mounted() {},
  methods: {
    onEdit(payload) {
      this.$emit("actionEdit", payload);
    },
    onDelete(payload) {
      this.$emit("actionDelete", payload);
    },
    onSelect(payload) {
      this.$emit("actionSelect", payload);
    },
    onChangePage(payload) {
      const { page, size } = payload;
      payload = {
        limit: size,
        offset: page,
      };
      this.$store.dispatch("user/loadingRequest");
      this.$store
        .dispatch("requestType/getRequestTypeList", payload)
        .then(() => {
          this.$store.dispatch("user/loadingRequest");
        })
        .catch((err) => {
          this.$message.error(err.message, 5);
          this.$store.dispatch("user/loadingRequest");
        });
    },
  },
};
