import ConfigurationTable from "../../../components/configurations/ConfigurationTable";
import { mapState } from "vuex";

const columns = [
  {
    title: "Priority Name",
    dataIndex: "name",
    key: "name",
    sorter: (a, b) => a.name.localeCompare(b.name),
  },
  {
    title: "Description",
    dataIndex: "description",
    key: "description",
    sorter: (a, b) => a.description.localeCompare(b.description),
  },
  {
    title: "",
    dataIndex: "action",
    key: "action",
    scopedSlots: { customRender: "action" },
  },
];
export default {
  name: "Priority",
  components: {
    ConfigurationTable,
  },
  data() {
    return {
      columns: columns,
    };
  },
  computed: {
    ...mapState({
      priorities: (state) => state.priority.priorityList,
      //exam of a change when click search or reset from configurations.js then currentPage and pageSize update on default
      pages: (state) => state.configurations.pages,
    }),
  },
  created() {},
  mounted() {},
  methods: {
    onEdit(payload) {
      this.$emit("actionEdit", payload);
    },
    onDelete(payload) {
      this.$emit("actionDelete", payload);
    },
    onSelect(payload) {
      this.$emit("actionSelect", payload);
    },
    onChangePage(payload) {
      const { page, size } = payload;
      payload = {
        limit: size,
        offset: page,
      };
      this.$store.dispatch("user/loadingRequest");
      this.$store
        .dispatch("priority/getPriorityList", payload)
        .then(() => {
          this.$store.dispatch("user/loadingRequest");
        })
        .catch((err) => {
          this.$message.error(err.message, 5);
          this.$store.dispatch("user/loadingRequest");
        });
    },
  },
};
