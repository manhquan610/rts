import { mapActions, mapMutations, mapState } from "vuex";
import VueSearch from "../../components/base/VueSearch";
import { API_ACTION_KEY, API_USER_TITLE } from "../../components/commons/commonConstant";
import {
  compareDate,
  compareString,
  PAGE_OPTIONS_MANAGEMENT_CV,
  TIME_DELAY_CANDIDATE,
} from "../../components/commons/commons";
import ManagementCVTable from "../../components/managementCV/ManagementCVTable.vue";
import UploadFileModal from "./UploadCV/UploadFileModal";
import ConfirmDialog from "../../components/base/confirmDialog";
import VuePagination from "../../components/base/VuePagination.vue";
import { checkPermission } from "../../service/permissionService";
import CreateCVModal from "../../components/managementCV/CreateCVModal";
import ViewDetailCandidateModal from "../../components/managementCV/ViewDetailCandidateModal";
import CreateAssignModal from "../../components/managementCV/CreateAssignModal";
import http from "../../store/modules/helpers/httpInterceptor";

const columnsTA = [
  {
    title: "ID",
    dataIndex: "candidateId",
    key: "candidateId",
    sorter: (a, b) => compareString(a.candidateId, b.candidateId),
    scopedSlots: { customRender: "candidateId" },
    width: "5%",
  },
  {
    title: "Name",
    dataIndex: "fullName",
    key: "fullName",
    sorter: (a, b) => compareString(a.fullName, b.fullName),
    scopedSlots: { customRender: "fullName" },
    width: "20%",
  },
  {
    title: "Job Id",
    dataIndex: "jobRtsId",
    key: "jobRtsId",
    sorter: (a, b) => compareString(a.jobRtsId, b.jobRtsId),
    scopedSlots: { customRender: "jobRtsId" },
    width: "5%",
  },
  {
    title: "Job Title",
    dataIndex: "job",
    key: "job",
    sorter: (a, b) => compareString(a.job, b.job),
    scopedSlots: { customRender: "job" },
    width: "17%",
  },
  {
    title: "Job Level",
    dataIndex: "jobLevel",
    key: "jobLevel",
    sorter: (a, b) => compareString(a.jobLevel, b.jobLevel),
    scopedSlots: { customRender: "jobLevel" },
    width: "15%",
  },
  {
    title: "StepRts",
    dataIndex: "stepRts",
    key: "stepRts",
    sorter: (a, b) => compareString(a.stepRts, b.stepRts),
    scopedSlots: { customRender: "stepRts" },
    width: "10%",
  },
  {
    title: "Channels",
    dataIndex: "channelName",
    key: "channelName",
    sorter: (a, b) => compareString(a.channelName, b.channelName),
    scopedSlots: { customRender: "channelName" },
    width: "10%",
  },
  {
    title: "Date Apply",
    dataIndex: "applySince",
    key: "applySince",
    sorter: (a, b) => compareDate(a.applySince, b.applySince),
    scopedSlots: { customRender: "applySince" },
    width: "10%",
  },
  {
    title: "Create by",
    dataIndex: "createBy",
    key: "createBy",
    width: "10%",
  },
  {
    title: "Assign Level 1",
    dataIndex: "assignees",
    key: "ta1",
    // sorter: (a, b) => compareDate(a.assignees, b.assignees),
    scopedSlots: { customRender: "assignees" },
    width: "10%",
  },
  {
    title: "Assign Level 2",
    dataIndex: "ta",
    key: "ta2",
    // sorter: (a, b) => compareDate(a.ta, b.ta),
    scopedSlots: { customRender: "ta" },
    width: "10%",
  },
  {
    title: "CV",
    dataIndex: "linkCv",
    key: "linkCv",
    scopedSlots: { customRender: "linkCv" },
    width: 100,
  },
  {
    title: "Action",
    dataIndex: "action",
    width: "4%",
    fixed: "right",
    align: "center",
    scopedSlots: { customRender: "action" },
  },
];

const columnsTALead = [
  {
    title: "ID",
    dataIndex: "candidateId",
    key: "candidateId",
    sorter: (a, b) => compareString(a.candidateId, b.candidateId),
    scopedSlots: { customRender: "candidateId" },
    width: "5%",
  },
  {
    title: "Name",
    dataIndex: "fullName",
    key: "fullName",
    sorter: (a, b) => compareString(a.fullName, b.fullName),
    scopedSlots: { customRender: "fullName" },
    width: "18%",
  },
  {
    title: "Job Id",
    dataIndex: "jobRtsId",
    key: "jobRtsId",
    sorter: (a, b) => compareString(a.jobRtsId, b.jobRtsId),
    scopedSlots: { customRender: "jobRtsId" },
    width: "7%",
  },
  {
    title: "Job Title",
    dataIndex: "job",
    key: "job",
    sorter: (a, b) => compareString(a.job, b.job),
    scopedSlots: { customRender: "job" },
    width: "15%",
  },
  {
    title: "Job Level",
    dataIndex: "jobLevel",
    key: "jobLevel",
    sorter: (a, b) => compareString(a.jobLevel, b.jobLevel),
    scopedSlots: { customRender: "jobLevel" },
    width: "8%",
  },
  {
    title: "StepRts",
    dataIndex: "stepRts",
    key: "stepRts",
    sorter: (a, b) => compareString(a.stepRts, b.stepRts),
    scopedSlots: { customRender: "stepRts" },
    width: "10%",
  },
  {
    title: "Channels",
    dataIndex: "channelName",
    key: "channelName",
    sorter: (a, b) => compareString(a.channelName, b.channelName),
    scopedSlots: { customRender: "channelName" },
    width: "10%",
  },
  {
    title: "Date Apply",
    dataIndex: "applySince",
    key: "applySince",
    sorter: (a, b) => compareDate(a.applySince, b.applySince),
    scopedSlots: { customRender: "applySince" },
    width: "10%",
  },
  {
    title: "Create by",
    dataIndex: "createBy",
    key: "createBy",
    width: "10%",
  },
  {
    title: "Assign Level 1",
    dataIndex: "assignees",
    key: "ta1",
    // sorter: (a, b) => compareDate(a.assignees, b.assignees),
    scopedSlots: { customRender: "assignees" },
    width: "10%",
  },
  {
    title: "Assign Level 2",
    dataIndex: "ta",
    key: "ta2",
    // sorter: (a, b) => compareDate(a.ta, b.ta),
    scopedSlots: { customRender: "ta" },
    width: "10%",
  },
  {
    title: "CV",
    dataIndex: "linkCv",
    key: "linkCv",
    scopedSlots: { customRender: "linkCv" },
    width: 100,
  },
  {
    title: "Action",
    dataIndex: "action",
    width: "4%",
    fixed: "right",
    align: "center",
    scopedSlots: { customRender: "action" },
  },
];

export default {
  name: "ManagementCV",
  components: {
    VueSearch,
    ManagementCVTable,
    UploadFileModal,
    ConfirmDialog,
    VuePagination,
    CreateCVModal,
    CreateAssignModal,
    ViewDetailCandidateModal,
  },
  data() {
    return {
      columnsTA,
      columnsTALead,
      visibleDialog: false,
      visibleConfirm: false,
      componentKey: 0,
      message: "Do you want to finish this process ?",
      files: [],
      API_ACTION_KEY,
      isShowUpLoadCVModal: false,
      isShowAssignModal: false,
      isShowDetailModal: false,
      dataModal: {},
      singleActions: ["edit", "assign", "reject", "detail"],
      dataSearch: "",
      dataDetailModal: {},
    };
  },
  created() {
    const payload = { ...PAGE_OPTIONS_MANAGEMENT_CV };
    this.$store.dispatch("rtsManagementCV/getListCv", payload);
    this.$store.dispatch("rtsManagementCV/getListChannel");
    this.$store.commit("rtsListCandidate/SET_CANDIDATE_SUCCESS", false);
    this.$store.commit("rtsListCandidate/SET_ASSIGN_CANDIDATE_SUCCESS", false);
  },
  computed: {
    ...mapState({
      rejectStatus: (state) => state.rtsJobPostingDetail.rejectStatus,
      requests: (state) => state.rtsManagementCV.listCv,
      pages: (state) => state.rtsManagementCV.pages,
      pageOptions: (state) => state.rtsManagementCV.pageOptions,
      totalItems: (state) => state.rtsManagementCV.total_items,
      statusCreate: (state) => state.rtsManagementCV.statusCreating,
      isEdit: (state) => state.rtsManagementCV.isEdit,
      userRole: (state) => state.user.userRole,
      addCandidateSuccess: (state) => state.rtsListCandidate.addCandidateSuccess,
      assignCandidateSuccess: (state) => state.rtsListCandidate.assignCandidateSuccess,
    }),
    requests: {
      get() {
        return this.$store.state.rtsManagementCV.listCv;
      },
      set(newVal) {
        this.$store.commit("rtsManagementCV/SET_LIST_CV", newVal);
      },
    },
    isEdit: {
      get() {
        return this.$store.state.rtsManagementCV.isEdit;
      },
      set(newVal) {
        this.$store.commit("rtsManagementCV/SET_IS_Edit", newVal);
      },
    },
  },
  watch: {
    rejectStatus: function (val) {
      if (val === "None") {
        this.submmitLoading = false;
        this.validateStatus = "success";
        this.errorMsg = "";
      }
      if (val === "Done") {
        this.onCancelPopup();
      }
      if (val === "Success") {
        this.changeCreateStatus("Done");
        this.$message.success(`Reject CV is successfully.`);
      }
    },
    addCandidateSuccess() {
      let options = {
        ...this.pageOptions,
        offset: 1,
      };
      options.searchString = this.payload;
      this.setPageOption(options);
      this.getListCVWithDelay(this.pageOptions, TIME_DELAY_CANDIDATE);

      this.$store.commit("rtsListCandidate/SET_CANDIDATE_SUCCESS", false);
    },
    assignCandidateSuccess() {
      let options = {
        ...this.pageOptions,
        offset: 1,
      };
      options.searchString = this.payload;
      this.setPageOption(options);
      this.getListCVWithDelay(this.pageOptions, TIME_DELAY_CANDIDATE);

      this.$store.commit("rtsListCandidate/SET_ASSIGN_CANDIDATE_SUCCESS", false);
    },
  },
  methods: {
    checkPermission,
    ...mapMutations({
      setPageOption: "rtsManagementCV/SET_PAGE_OPTIONS",
      isAdd: "rtsManagementCV/SET_IS_ADD",
      statusCreating: "rtsManagementCV/SET_STATUS_CREATING",
    }),
    ...mapActions({
      candidateRejectCv: "rtsJobPostingDetail/candidateRejectCv",
      changeCreateStatus: "rtsJobPostingDetail/changeCreateStatus",
      getListCv: "rtsManagementCV/getListCv",
      setLoading: "user/loadingRequest",
      stopLoading: "user/stopLoading",
    }),
    onSearch() {
      console.log("value", this.dataSearch);
      if (this.dataSearch && this.dataSearch.trim() !== "") {
        http
          .get(`/candidate-check?candidateCheck=${this.dataSearch}`)
          .then((res) => {
            console.log("🚀 ~ file: ManagementCV.js ~ line 333 ~ http.get ~ res", res);
            this.$message.warn(res.data.data.ldapTA);
          })
          .catch((error) => {
            console.log("Error: ", error);
          });
      } else {
        this.$message.error("Nhập để tìm kiếm");
        this.dataSearch = "";
      }
    },
    onSingleAction(payload) {
      if (payload.type == "create") {
        this.$store.commit("rtsManagementCV/SET_IS_Edit", false);
        this.dataModal = null;
        this.isShowUpLoadCVModal = true;
      } else if (payload.type == "edit") {
        this.$store.commit("rtsManagementCV/SET_IS_Edit", true);
        this.dataModal = payload.data;
        this.isShowUpLoadCVModal = true;
      } else if (payload.type == "assign") {
        this.dataModal = payload.data;
        this.isShowAssignModal = true;
      } else if (payload.type == "reject") {
        this.dataModal = payload.data;
        const param = {
          id: this.dataModal.candidateId,
          address: this.dataModal.address ? this.dataModal.address : "",
          applySince: this.dataModal.applySince ? this.dataModal.applySince : "",
          birthDay: this.dataModal.birthDay ? this.dataModal.birthDay : "",
          candidateType: this.dataModal.candidateType,
          channel: this.dataModal.channel ? this.dataModal.channel : "",
          degree: this.dataModal.degree ? this.dataModal.degree : "",
          email: this.dataModal.email ? this.dataModal.email : "",
          experience: this.dataModal.experience ? this.dataModal.experience : "",
          facebook: this.dataModal.facebook ? this.dataModal.facebook : "",
          freelancerEmail: this.dataModal.freelancerEmail ? this.dataModal.freelancerEmail : "",
          freelancerMobile: this.dataModal.freelancerMobile ? this.dataModal.freelancerMobile : "",
          freelancerName: this.dataModal.freelancerName ? this.dataModal.freelancerName : "",
          fullName: this.dataModal.fullName ? this.dataModal.fullName : "",
          headhunterName: this.dataModal.headhunterName ? this.dataModal.headhunterName : "",
          introducer: this.dataModal.introducer ? this.dataModal.introducer : "",
          job: this.dataModal.job ? this.dataModal.job : "",
          language: this.dataModal.language ? this.dataModal.language : "",
          lastCompany: this.dataModal.lastCompany ? this.dataModal.lastCompany : "",
          level: this.dataModal.level ? this.dataModal.level : "",
          linkCv: this.dataModal.linkCv ? this.dataModal.linkCv : "",
          location: this.dataModal.location ? this.dataModal.location : "",
          phone: this.dataModal.phone ? this.dataModal.phone : "",
          refererUser: this.dataModal.refererUser ? this.dataModal.refererUser : "",
          requestId: this.dataModal.requestId ? this.dataModal.requestId : "",
          roleName: this.dataModal.roleName ? this.dataModal.roleName : "",
          school: this.dataModal.school ? this.dataModal.school : "",
          sex: this.dataModal.sex ? this.dataModal.sex : "",
          skill: this.dataModal.skill ? this.dataModal.skill : "",
          skypeID: this.dataModal.skypeID ? this.dataModal.skypeID : "",
          source: this.dataModal.source ? this.dataModal.source : "",
          stepRts: this.dataModal.stepRts ? this.dataModal.stepRts : "",
          ta: API_USER_TITLE.EB !== this.userRole.title ? null : this.dataModal.ta ? this.dataModal.ta : "",
          assignees:
            API_USER_TITLE.EB === this.userRole.title ? null : this.dataModal.assignees ? this.dataModal.assignees : "",
        };
        this.candidateRejectCv(param);
        this.$store.commit("rtsListCandidate/SET_ASSIGN_CANDIDATE_SUCCESS", true);
        this.$store.commit("rtsJobPostingDetail/CHANGE_REJECT_STATUS", "None");
      } else if (payload.type === "detail") {
        this.isShowDetailModal = true;
        this.dataDetailModal = payload.data;
      }

      /*this.isAdd(true)
            const {requests} = this;
            if(!this.statusCreate) {
              const newData = {candidateId:0,fullName:"",email:"",phone:"",linkCv:"",job:"",channelName:"",channel:"",applySince:""};
              this.requests = [newData, ...requests];
            }
            this.$store.commit('rtsManagementCV/SET_STATUS_CREATING',true) */
    },
    onActionTimeLine(payload) {
      if (payload.type === "detail") {
        this.dataModal = payload.dataTimeLine;
      }
    },
    resetModalData() {
      this.modalData = null;
    },
    forceRerender() {
      this.componentKey += 1;
    },
    handleOke(data) {
      this.visibleDialog = false;
      this.files = data;
    },
    handleCancel() {
      this.visibleConfirm = true;
    },
    handleOkeConfirm() {
      this.visibleDialog = false;
      this.visibleConfirm = false;
    },
    handleCancelConfirm() {
      this.visibleConfirm = false;
    },
    onChangePage(payload) {
      this.pageOptions.offset = payload.offset;
      this.pageOptions.limit = payload.limit;
      let options = { ...this.pageOptions };
      this.setPageOption(options);
      this.getListCVWithDelay(this.pageOptions, TIME_DELAY_CANDIDATE);
    },
    onSearchPage(payload) {
      this.handleSearch(payload);
    },
    onClearPage(payload) {
      this.handleSearch(payload);
    },
    handleSearch(payload) {
      let options = {
        ...this.pageOptions,
        offset: 1,
      };
      options.searchString = payload.inputValue;
      if (payload.hasCv !== -1) {
        options.hasCV = payload.hasCv;
      } else {
        delete options.hasCV;
      }
      options.startDate = payload.startDate;
      options.endDate = payload.endDate;
      this.setPageOption(options);
      this.getListCVWithDelay(this.pageOptions, TIME_DELAY_CANDIDATE);
    },
    showDialogUpload() {
      this.componentKey += 1;
      this.$forceUpdate();
      this.visibleDialog = true;
    },
    getListCVWithDelay(pageOptions, timer) {
      return new Promise((resolve) => {
        this.setLoading();
        setTimeout(() => {
          this.getListCv(pageOptions).then(() => {
            this.stopLoading();
            resolve();
          });
        }, timer);
      });
    },
    handleCloseUpload() {
      this.isShowUpLoadCVModal = false;
    },
    handleCloseAssignModal() {
      this.isShowAssignModal = false;
    },
    handleCloseDetailModal() {
      this.isShowDetailModal = false;
    },
  },
};
