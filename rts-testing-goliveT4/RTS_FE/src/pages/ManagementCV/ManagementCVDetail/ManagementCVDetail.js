import http from "../../../store/modules/helpers/httpInterceptor";
import VueCollapse from "../../../components/base/VueCollapse";
import RequestButtonDropdown from "../../../components/managementCV/RequestButtonDropdown";
import CreateAssignModal from "../../../components/managementCV/CreateAssignModal";

export default {
  name: "ManagementCVDetail",
  components: {
    VueCollapse,
    RequestButtonDropdown,
    CreateAssignModal,
  },
  data() {
    return {
      requestId: null,
      formItem: [
        {
          key: "candidateId",
          label: "Id",
        },
        {
          key: "fullName",
          label: "Full Name",
        },
        {
          key: "email",
          label: "Email",
        },
        {
          key: "location",
          label: "Location",
        },
        {
          key: "phone",
          label: "Phone",
        },
        {
          key: "address",
          label: "Address",
        },
        {
          key: "applySince",
          label: "Date Apply",
        },
        {
          key: "createBy",
          label: "Created By",
        },
        {
          key: "assignees",
          label: "Assign Level 1",
        },
        {
          key: "ta",
          label: "Assign Level 2",
        },
        {
          key: "birthDay",
          label: "BirthDay",
        },
        {
          key: "channelName",
          label: "Channel",
        },
        // {
        //   key: "linkCv",
        //   label: "CV",
        // },
      ],
      columns: [
        {
          title: "Request Id",
          dataIndex: "requestId",
          key: "requestId",
          scopedSlots: { customRender: "requestId" },
          width: 100,
        },
        {
          title: "Step Before",
          dataIndex: "candidateBefore",
          key: "candidateBefore",
          width: 100,
        },
        {
          title: "Step After",
          dataIndex: "candidateAfter",
          key: "candidateAfter",
          width: 100,
        },
        {
          title: "Creator",
          dataIndex: "createdBy",
          key: "createdBy",
          width: 100,
        },
        {
          title: "Create Date",
          dataIndex: "createdDate",
          key: "createdDate",
          width: 150,
        },
      ],
      layoutForm: {
        labelCol: { span: 6 },
        wrapperCol: { span: 18 },
      },
      candidateData: {},
      candidateHistory: [],
      actions: ["edit", "assign", "reject"],
      visibleAssignModal: false,
    };
  },
  created() {
    this.requestId = this.$route.params.id;
    this.getCandidateDetail();
    this.getCandidateHistory();
  },
  computed: {},
  watch: {},
  methods: {
    getCandidateDetail() {
      http.get(`/candidate/${this.requestId}`).then((res) => {
        this.candidateData = res.data;
      });
    },
    getCandidateHistory() {
      http.get(`/timeline/${this.requestId}`).then((res) => {
        this.candidateHistory = res.data;
      });
    },
    handleClickRequestId(id) {
      this.$router.push({ name: "RequestDetail", params: { id } });
    },
    handleBack() {
      this.$router.push({ name: "ManagementCV" });
    },
    emitAction(payload) {
      const { data, type } = payload;
      if (type === "assign") {
        this.visibleAssignModal = true;
      }
    },
    handleCloseAssignModal() {
      this.visibleAssignModal = false;
    },
  },
  mounted() {
    console.log("requestId: ", this.requestId);
  },
};
