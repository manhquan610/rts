import { scaleScreen } from "../../components/commons/commons";
export default {
  name: "Login",
  beforeCreate() {
    this.form = this.$form.createForm(this, { name: 'normal_login' });
  },
  created() {
    this.scaleScreen()
  },
  data(){
    return {
      error_message:undefined,
      loading:false
    }
  },
  methods: {
    scaleScreen,
    onHandleLogin(e){
       e.preventDefault();
        this.form.validateFields((err, values) => {
        if (!err) {
          this.loading = true
          let payload = {
            username:values.username,
            password:values.password
          }
          
          this.$store.dispatch('user/userLogin',payload).then((res)=>{
            if(res){
              this.$router.push('/').catch(() => {});
            }
          }).catch((err) => {
            this.loading = false
            this.error_message = err.response.data.errorMessage + "";
         })
        }
      });
    },
    onHandleChangePassword() {
      this.$router.push({
        path: '/change-password'
      }).catch(() => {})
    }
  },
};


