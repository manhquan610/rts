import { scaleScreen } from "../../components/commons/commons";
export default {

  beforeCreate() {
  },
  created() {
    this.scaleScreen()
  },
  data(){
    return {
      reports: [
        {
          id: 1,
          name: "Request",
          url: "report/ticket-report",
          class: 'tickets',
          imgLink: "plane-tickets.png"
        },
        {
          id: 3,
          name: "TA",
          url: "report/ta-report",
          class: 'ta',
          imgLink: "network.png"
        },
        {
          id: 4,
          name: "Channel",
          url: "",
          class: 'channel',
          imgLink: "global-network.png"
        }
      ]
    }
  },
  methods: {
    scaleScreen,
    goToReport(url){
      this.$router.push(url)
    }
  },
};


