import FilterLayout from "../../../components/layouts/FilterLayout";
import {mapActions, mapState} from "vuex";
import {
    convertMillisecondsToDate,
    formatNewStatus,
    downloadFileExcel,
} from "../../../components/commons/commons";
const columns = [
    {
        title: '',
        dataIndex: '',
        width: "0.5%",
    },
    {
        title: 'Name',
        dataIndex: 'unitName',
        ellipsis: true,
        width: "12%",
        key: 'unitName',
    },
    {
        title: 'Skill',
        dataIndex: 'skill',
        ellipsis: true,
    },
    {
        title: 'Status',
        dataIndex: 'status',
        scopedSlots: { customRender: "statusProcess" },
        ellipsis: true,
    },
    {
        title: 'Location',
        dataIndex: 'location',
        key: 'address',
        ellipsis: true,
    },
    {
        title: 'Deadline',
        dataIndex: 'deadline',
        key: 'deadline',
        scopedSlots: { customRender: "deadline" },
        ellipsis: true,
    },
    {
        title: 'Target',
        dataIndex: 'target',
        width: "4%",
        key: 'target',
    },
    {
        title: 'Actual',
        width: "4%",
        dataIndex: 'actual',
        key: 'actual',
    },
    {
        title: '%',
        dataIndex: 'shortCount',
        width: "4%",
        key: 'short',
        customRender: (text, row, index) => {
            if (row.shortCount!==0){
               return `${row.shortCount}%`
            }
            else{
                return `${row.shortCount}`
            }
        }
    },
    {
        title: 'Status',
        dataIndex: 'statusNumber',
        key: 'statusNumber',
        children: [
            {
                title: 'High',
                dataIndex: 'statusHigh',
                key: 'statusHigh',
                width: "3%",
                align: 'center',
            },
            {
                title: 'Medium',
                dataIndex: 'statusMedium',
                key: 'statusMedium',
                width: "5%",
                align: 'center',
            },
            {
                title: 'Low',
                dataIndex: 'statusLow',
                key: 'statusLow',
                width: "3%",
                align: 'center',
            }]

    },
    {
        title: '',
        dataIndex: '',
        key: '',
        width: "1%"
    },
    {
        title: 'Total',
        dataIndex: 'total',
        key: 'total',
        children: [
            {
                title: 'Application',
                dataIndex: 'totalApplication',
                key: '1',
                width: '7%',
                align: 'center',
            },
            {
                title: 'Qualified',
                dataIndex: 'totalQualify',
                key: '2',
                align: 'center',
            },
            {
                title: 'Interview',
                dataIndex: 'totalInterview',
                key: '3',
                align: 'center',
            },

            {
                title: 'Offering',
                dataIndex: 'totalOffering',
                key: '5',
                align: 'center',
            },
            {
                title: 'Onboarding',
                dataIndex: 'totalOnboarding',
                key: '6',
                width: '7%',
                align: 'center',
            },
        ]
    },
]
export default {
    props: {

    },
    data() {
        return {
            dataFilters: {},
            columns,
            currentPage: 1,
            pageSize: 10,
            total_items: 0,
        }
    },
    components: {
        FilterLayout,
    },
    async created() {

        await this.$store.dispatch("rtsApplication/getGroupList");
        const payload = {
            pageOptions: {
                offset: this.currentPage,
                limit: this.pageSize
            },
            dataFilter: this.dataFilters,
        }
        await this.getTaReportData(payload)
        this.total_items = this.pages.total_items
    },
    computed: {
      ...mapState({
          taReport: state => state.rtsReport.taReport,
          departments: state => state.rtsApplication.departmentAll,
          pages: state => state.rtsReport.pages,
      })
    },
    watch: {
        currentPage: function (){
            this.total_items = this.pages.total_items
        },
    },
    methods: {
        ...mapActions({
            setLoading: 'user/loadingRequest',
            stopLoading: 'user/stopLoading',
        }),
        convertMillisecondsToDate,
        formatNewStatus,
        downloadFileExcel,

        getTaReportData(payload){
            this.setLoading()
            this.$store.dispatch("rtsReport/GET_TA_REPORT", payload).then(res => {
                this.stopLoading()
                this.total_items =  this.pages.total_items
            })
        },
        onSetDataFilters(filters){
            this.dataFilters = filters
        },

        onApplyFilters(filter){
            const payload = {
                pageOptions : {
                    offset: this.currentPage,
                    limit: this.pageSize,

                },
                dataFilter: filter,
            }
            this.getTaReportData(payload)
        },
        onCleanFilter(){

        },
        onShowTotal(total, range) {

        },
        onChangePage(offset, limit) {
            this.currentPage = offset
            this.pageSize = limit
            const payload ={
                pageOptions : {
                    offset: offset,
                    limit: limit,

                },
                dataFilter: this.dataFilters,
            }
            this.getTaReportData(payload)
        }
    }
}