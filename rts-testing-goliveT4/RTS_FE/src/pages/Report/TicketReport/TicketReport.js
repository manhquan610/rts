import FilterLayout from "../../../components/layouts/FilterLayout";
import { mapActions, mapMutations, mapState } from "vuex";
import {
  convertMillisecondsToDate,
  downloadFileExcel,
  convertObjectToParams,
} from "../../../components/commons/commons";
import moment from "moment";
const columns = [
  {
    title: "",
    dataIndex: "",
    width: "1%",
  },
  {
    title: "Unit",
    dataIndex: "unitName",
    width: "8%",
    key: "unit",
    scopedSlots: { customRender: "unit" },
  },
  {
    title: "Job ID",
    dataIndex: "jobId",
    width: "4%",
    key: "jobId",
    ellipsis: true,
  },
  {
    title: "Job Title",
    dataIndex: "jobType",
    width: "8%",
    key: "jobType",
    ellipsis: true,
  },
  {
    title: "Job Level",
    dataIndex: "jobLevel",
    width: "6%",
    key: "jobLevel",
    scopedSlots: {
      customRender: (value, row, index) => {
        return row && row.managerJobLevel && row.managerJobLevel.length ? row.managerJobLevel[0].name : "";
      },
    },
    ellipsis: true,
  },
  // Remove follow as client requirement
  // {
  //     title: 'Project',
  //     dataIndex: 'projectName',
  //     key: 'project',
  //     width: "10%",
  //     ellipsis: true,
  // },
  // {
  //     title: 'Location',
  //     dataIndex: 'location',
  //     key: 'location',
  //     ellipsis: true,
  // },
  {
    title: "Deadline",
    dataIndex: "deadline",
    key: "deadline",
    scopedSlots: { customRender: "deadline" },
    ellipsis: true,
  },
  {
    title: "Target",
    dataIndex: "totalTarget",
    key: "totalTarget",
  },
  {
    title: "Actual",
    dataIndex: "totalActual",
    key: "totalActual",
  },
  {
    title: "Gap",
    dataIndex: "totalGap",
    key: "totalGap",
  },
  //   {
  //     title: "Short",
  //     dataIndex: "shortCount",
  //     key: "shortCount",
  //   },
  // {
  //     title: 'Status',
  //     dataIndex: 'status',
  //     key: 'status',
  //     children: [
  //         {
  //             title: 'High',
  //             dataIndex: 'statusHigh',
  //             key: 'statusHigh',
  //             align: 'center',
  //         },
  //         {
  //             title: 'Medium',
  //             dataIndex: 'statusMedium',
  //             key: 'statusMedium',
  //             align: 'center',
  //         },
  //         {
  //             title: 'Low',
  //             dataIndex: 'statusLow',
  //             key: 'statusLow',
  //             align: 'center',
  //         }]

  // },
  {
    title: "",
    dataIndex: "",
    width: "2%",
  },
  {
    title: "Application",
    dataIndex: "totalApplication",
    key: "totalApplication",
    customRender: (value, row, index) => {
      return `${row.currentApplication}/${row.totalApplication}`;
    },

    align: "center",
  },
  {
    title: "Qualified",
    dataIndex: "totalQualify",
    key: "totalQualify",
    customRender: (value, row, index) => {
      return `${row.currentQualifying}/${row.totalQualify}`;
    },
    align: "center",
  },
  {
    title: "Interview",
    dataIndex: "totalInterview",
    key: "totalInterview",
    customRender: (value, row, index) => {
      return `${row.currentInterview}/${row.totalInterview}`;
    },
    align: "center",
  },
  // {
  //   title: "Review Offering",
  //   dataIndex: "totalReviewOffering",
  //   key: "totalReviewOffering",
  //   align: "center",
  //   width: "8%",
  // },
  {
    title: "Offer",
    dataIndex: "totalOffering",
    key: "totalOffering",
    customRender: (_, row) => {
      return `${row.currentOffering}/${row.totalOffering}`;
    },
    align: "center",
  },
  {
    title: "Onboard",
    dataIndex: "totalOnBoarding",
    key: "totalOnBoarding",
    customRender: (_, row) => {
      return `${row.currentOnBoarding}/${row.totalOnBoarding}`;
    },
    align: "center",
  },
  {
    title: "Reject",
    dataIndex: "totalRejected",
    key: "totalRejected",
    align: "center",
  },
];
export default {
  props: {},
  components: {
    FilterLayout,
  },
  data() {
    return {
      dataFilters: {},
      columns,
      pageSize: 10,
      currentPage: 1,
      total_items: 0,
      tickets: [],
      summary: [
        {
          key: "target",
          keyCurrent: "targetCurrent",
          label: "Target",
        },
        {
          key: "actual",
          keyCurrent: "actualCurrent",
          label: "Actual",
        },
        {
          key: "gap",
          keyCurrent: "gapCurrent",
          label: "Gap",
        },
        {
          key: "application",
          keyCurrent: "applicationCurrent",
          label: "Application",
        },
        {
          key: "qualified",
          keyCurrent: "qualifiedCurrent",
          label: "Qualified",
        },
        {
          key: "interview",
          keyCurrent: "interviewCurrent",
          label: "Interview",
        },
        {
          key: "offering",
          keyCurrent: "offeringCurrent",
          label: "Offer",
        },
        {
          key: "onboarding",
          keyCurrent: "onboardingCurrent",
          label: "Onboard",
        },
        {
          key: "reject",
          label: "Reject",
        },
      ],
      dataSummary: {},
    };
  },
  async created() {
    this.total_items = this.pages.total_items;
    await this.$store.dispatch("rtsApplication/getGroupList");
    await this.$store.dispatch("rtsApplication/getProjectList");
    const payload = {
      pageOptions: {
        offset: this.currentPage,
        limit: this.pageSize,
      },
      dataFilter: this.dataFilters,
    };
    // await this.$store.dispatch("rtsReport/GET_TICKET_REPORT", payload);
    this.tickets = this.ticketReport?.filter((item) => item.children !== null);
  },
  watch: {
    currentPage: function () {
      this.total_items = this.pages.total_items;
    },
    "pages.total_items": function (newValue) {
      this.total_items = this.pages.total_items;
    },
    ticketReport: function (newVal) {
      this.tickets = newVal?.filter((item) => item.children !== null);
    },
  },
  computed: {
    ...mapState({
      ticketReport: (state) => state.rtsReport.ticketReport,
      departments: (state) => state.rtsApplication.departmentAll,
      projects: (state) => state.rtsApplication.projectList,
      pages: (state) => state.rtsReport.pages,
    }),
  },
  methods: {
    ...mapActions({
      setLoading: "user/loadingRequest",
      stopLoading: "user/stopLoading",
    }),
    downloadFileExcel,
    convertMillisecondsToDate,
    convertObjectToParams,
    onSetDataFilters(filters) {
      this.dataFilters = filters;
    },
    onApplyFilters(filter) {
      const payload = {
        pageOptions: {
          offset: this.currentPage,
          limit: this.pageSize,
        },
        dataFilter: filter,
      };
      this.getTicketData(payload);
    },
    onCleanFilter() {
      this.total_items = this.pages.total_items;
    },
    onShowTotal(total, range) {
      console.log(total, range);
    },
    onChangePage(offset, limit) {
      this.currentPage = offset;
      this.pageSize = limit;
      const payload = {
        pageOptions: {
          offset: offset,
          limit: limit,
        },
        dataFilter: this.dataFilters,
        departments: this.departments,
        projects: this.projects,
      };
      this.getTicketData(payload);
    },
    onShowSizeChange(offset, limit) {
      console.log("onChangePage", { offset, limit });
    },

    getTicketData(payload) {
      this.setLoading();
      this.$store.dispatch("rtsReport/GET_TICKET_REPORT", payload).then((res) => {
        this.dataSummary = {
          target: res.totalTarget,
          actual: res.totalActual,
          gap: res.totalGap,
          application: res.totalApplicant,
          applicationCurrent: res.totalCurrentApplicant,
          qualified: res.totalQualified,
          qualifiedCurrent: res.totalCurrentQualified,
          interview: res.totalInterview,
          interviewCurrent: res.totalCurrentInterview,
          offering: res.totalOffering,
          offeringCurrent: res.totalCurrentOffering,
          onboarding: res.totalOnBoarding,
          onboardingCurrent: res.totalCurrentOnBoarding,
          reject: res.totalRejected,
        };
        this.stopLoading();
      });
    },

    exportTicketData() {
      const payload = {
        startTime: this.dataFilters.fromDate ? moment(this.dataFilters.fromDate).utc().format("YYYY/MM/DD") : null,
        endTime: this.dataFilters.toDate ? moment(this.dataFilters.toDate).utc().format("YYYY/MM/DD") : null,
      };
      const param = convertObjectToParams(payload);
      downloadFileExcel(`report/ticket/export${param}`, "TicketReport");
    },
  },
};
