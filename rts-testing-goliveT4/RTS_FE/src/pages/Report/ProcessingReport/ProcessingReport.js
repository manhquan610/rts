import FilterLayout from "../../../components/layouts/FilterLayout";
import {mapActions, mapState} from "vuex";
import {convertMillisecondsToDate, downloadFileExcel} from "../../../components/commons/commons";
const columns = [
    {
        title: '',
        dataIndex: '',
        width: '1%'
    },
    {
        title: 'Unit',
        dataIndex: 'unitName',
        width: "8%",
        key: 'unit',
        scopedSlots: { customRender: "unit" },
    },
    {
        title: 'Skill',
        dataIndex: 'skill',
        key: 'skill',
        ellipsis: true,
    },
    {
        title: 'Status',
        dataIndex: 'status',
        key: 'status',
        ellipsis: true,
    },
    {
        title: 'Location',
        dataIndex: 'location',
        key: 'location',
        ellipsis: true,
    },
    {
        title: 'Deadline',
        dataIndex: 'deadline',
        key: 'deadline',
        scopedSlots: { customRender: "deadline" },
        ellipsis: true,
    },
    {
        title: 'Target',
        dataIndex: 'target',
        key: 'target',
    },
    {
        title: 'Actual',
        dataIndex: 'actual',
        key: 'actual',
    },
    {
        title: 'Short',
        dataIndex: 'shortCount',
        key: 'shortCount',
    },
    {
        title: 'Status',
        dataIndex: 'status',
        key: 'statusNumber',
        children: [
            {
                title: 'High',
                dataIndex: 'statusHigh',
                key: 'statusHigh',
                align: 'center',
            },
            {
                title: 'Medium',
                dataIndex: 'statusMedium',
                key: 'statusMedium',
                align: 'center',
            },
            {
                title: 'Low',
                dataIndex: 'statusLow',
                key: 'statusLow',
                align: 'center',
            }]

    },
    {
        title: '',
        dataIndex: '',
        width: "2%"
    },
    {
        title: 'Total',
        dataIndex: 'total',
        key: 'total',
        children: [
            {
                title: 'Application',
                dataIndex: 'totalApplication',
                key: 'totalApplication',
                align: 'center',
            },
            {
                title: 'Qualified',
                dataIndex: 'totalQualify',
                key: 'totalQualify',
                align: 'center',
            },
            {
                title: 'Interview',
                dataIndex: 'totalInterview',
                key: 'totalInterview',
                align: 'center',
            },
            {
                title: 'Review Offering',
                dataIndex: 'totalReviewOffering',
                key: 'totalReviewOffering',
                align: 'center',
                width: "8%"
            },
            {
                title: 'Offering',
                dataIndex: 'totalOffering',
                key: 'totalOffering',
                align: 'center',
            },
            {
                title: 'Onboarding',
                dataIndex: 'totalOnboarding',
                key: 'totalOnboarding',
                align: 'center',
            },
        ]
    },
]
export default {
    props: {

    },
    components: {
        FilterLayout
    },
    data() {
        return {
            dataFilters: {},
            columns,
            pageSize: 10,
            currentPage: 1,
            total_items: 0,
            processings: []

        }
    },
    async created() {
        this.total_items = this.pages.total_items
        const params = {
            limit :1000,
            offset :1,
            name : ''
        }
        await this.getSkillList(params)
        await this.$store.dispatch("rtsApplication/getGroupList");
        const payload = {
            pageOptions: {
                offset: this.currentPage,
                limit: this.pageSize
            },
            dataFilter: this.dataFilters,
        }
        await this.$store.dispatch("rtsReport/GET_PROCESSING_REPORT", payload)
        this.processings = this.processingReport?.filter(item => item.children !== null)

    },
    watch: {
        currentPage: function (){
            this.total_items = this.pages.total_items

        },
        processingReport: function (newVal) {
            this.tickets = newVal?.filter(item => item.children !== null)
        }
    },
    computed: {
      ...mapState({
          processingReport: state => state.rtsReport.processingReport,
          skills: (state) => state.rtsApplication.skillList,
          departments: state => state.rtsApplication.departmentAll,
          pages: state => state.rtsReport.pages,
      })
    },
    methods: {
        ...mapActions({
            setLoading: 'user/loadingRequest',
            stopLoading: 'user/stopLoading',
            getSkillList: 'rtsApplication/getSkillList',
        }),
        downloadFileExcel,
        convertMillisecondsToDate,
        onSetDataFilters(filters){
            this.dataFilters = filters
        },
        onApplyFilters(filter){
            console.log(filter)

            const payload = {
                pageOptions : {
                    offset: this.currentPage,
                    limit: this.pageSize,

                },
                dataFilter: filter,

            }
            this.getTicketData(payload)
        },
        onCleanFilter(){
            this.total_items = this.pages.total_items
        },
        onShowTotal(total, range) {
            console.log(total, range)
        },
        onChangePage(offset, limit) {
            this.currentPage = offset
            this.pageSize = limit
            const payload ={
                pageOptions : {
                    offset: offset,
                    limit: limit,

                },
                dataFilter: this.dataFilters,
                departments: this.departments,
                projects: this.projects
            }
            this.getTicketData(payload)
        },
        onShowSizeChange(offset, limit) {
            console.log("onChangePage", {offset, limit});
        },

        getTicketData(payload){
           //this.setLoading();
            //this.$store
            //    .dispatch("rtsReport/GET_PROCESSING_REPORT", payload).then(res => {
                  //  this.stopLoading();


            //})

        },


    }

}