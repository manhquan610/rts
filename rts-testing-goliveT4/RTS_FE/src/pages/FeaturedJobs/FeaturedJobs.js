import FeaturedJob from '../../components/FeaturedJob/FeaturedJob.vue';
import VuePagination from '../../components/base/VuePagination.vue';
import { mapState, mapMutations } from 'vuex';
export default {
    name:"FeaturedJobs",
    components:{
        FeaturedJob,
        VuePagination
    },
    props:{
        jobList: Array
    },
    computed:{
        ...mapState({
            pageOptions: (state) => state.rtsShareJob.pageOptions,
            totalItems: (state) => state.rtsShareJob.total_items,
        })
    },
    methods:{
      ...mapMutations({
        setPageOption: 'rtsShareJob/SET_PAGE_OPTIONS'
      }),

      onChangePage(payload){
        this.$emit('onChangePage', payload)
      }
    }
}
