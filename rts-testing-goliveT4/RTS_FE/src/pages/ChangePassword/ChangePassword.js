import { scaleScreen } from "../../components/commons/commons";
export default {
    name: "change-password",

    created() {
        this.scaleScreen()
    },

    data() {
        return {
            error_message: undefined,
            loasing: false
        }
    },

    methods: {
        scaleScreen,
        onHandleChangePassword(e) {
            e.preventDefault();
        },
        handleBackLogin() {
            this.$store.dispatch('user/userLogout')
            this.$router.push({
                path: '/login'
            }).catch(() => {})
        },
    },
};