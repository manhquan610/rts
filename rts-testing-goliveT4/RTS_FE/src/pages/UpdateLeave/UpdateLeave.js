import { scaleScreen } from "../../components/commons/commons";
import { mapState } from "vuex";
export default {
    name: "update-leave",
    created() {
        this.scaleScreen()
        this.$store.dispatch('application/getRemainTypeLeave');
    },
    computed: {
        ...mapState({
            employeeList: state => state.application.employeeList,
            remainTypeLeaveList: state => state.application.remainTypeLeaveList,
        })
    },
    data() {
        return {
            fetching: false,
            timeout: null,
            form: this.$form.createForm(this, { name: 'updateLeave' }),
        }
    },

    methods: {
        scaleScreen,
        onSearch(value) {
            let cloneVal = decodeURI(value);
            if (value && value !== "" && value.length >= 3) {
                this.fetching = true;
                clearTimeout(this.timeout);
                this.timeout = setTimeout(() => {
                    this.$store
                        .dispatch("application/getEmployeeList", cloneVal)
                        .then(() => {
                            this.fetching = false;
                        })
                        .catch(() => {
                            this.fetching = false;
                        });
                }, 1000);
            }
        },
        handleSubmit(e) {
            e.preventDefault();
            this.form.validateFields((err,values) => {
              if (!err) {
                let updateStaff = []
                   let  words = values.reason.split("-");
                   let temp = []
                    for (let i = 1; i < words.length; i++) {
                        words[i] = words[i].substring(0, 1).toUpperCase() + words[i].substring(1).toLowerCase();
                        
                    }
                    let title = words.join(" ");
                        title = title.replaceAll(' ', '')
                    values.updateStaff.forEach(element => {

                      temp[title] = values.leave
                     let objectEmployeeUpdate = {...temp,
                         id: JSON.parse(element).id}
                    updateStaff.push(objectEmployeeUpdate)
                  });
                  let payload = {url: values.reason, data: updateStaff}
                  this.$store.dispatch('application/updateLeavingRemain', payload).then(res => {
                      if(res){
                          res.forEach(element => {
                            this.$message.success(element);
                          });
                      }
                  }
                  ).catch(() => {
                    this.fetching = false;
                  });
              }
            });
          },
        handleBack() {
            this.$router.push({
                path: '/timesheet'
            }).catch(() => {})
        },
    },
};