const userList = [{
    title: "Staff",
    dataIndex: "name",
    key: "name",
    width: '25%',
    scopedSlots: { customRender: 'name' },
},
{
    title: "Username",
    dataIndex: "user_name",
    key: "user_name",
    width: '15%',
},
{
    title: "Department",
    dataIndex: "code",
    key: "code",
    width: '16%',
},
{
    title: "Start Time",
    dataIndex: "startTime",
    key: "startTime",
    width: '15%',
},
{
    title: "End Time",
    dataIndex: "endTime",
    key: "endTime",
    width: '15%',
},
{
    title: "Duration",
    dataIndex: "duration",
    key: "duration",
    width: '14%',
},
]
const columns = [
    {
        title: 'Title',
        dataIndex: 'title',
        name: 'title',
        width:'10%'
    },
    {
        title: 'Project',
        dataIndex: 'project',
        name: 'project',
        scopedSlots: {customRender: 'project'},
        width:'15%'
    },
    {
        title: 'From - To',
        dataIndex: 'from_to',
        name: 'from_to',
        scopedSlots: { customRender: 'from_to'},
        width:'10%'
    },
    {
        title: 'Type',
        dataIndex: 'type',
        name: 'type',
        scopedSlots: { customRender: 'type'},
        width:'10%'
    },
    {
        title: 'Approver',
        dataIndex: 'on_approver',
        name: 'on_approver',
        scopedSlots: {customRender: 'on_approver'},
        width:'15%'
    },
    {
        title: 'Created By',
        dataIndex: 'creator',
        name: 'creator',
        scopedSlots: {customRender: 'creator'},
        width:'15%'
    },
    {
        title: 'Status',
        dataIndex: 'status',
        name: 'status',
        scopedSlots: {customRender: 'status'},
        width: '23%'
    },
    {
        title: '',
        dataIndex: 'edit',
        name: 'edit',
        scopedSlots: {customRender: 'edit'},
        width: '2%'
    }
];
import OnsiteDetail from "./OnsiteDetail/OnsiteDetail.vue";
import FilterLayout from "../../components/layouts/FilterLayout";
import { mapState } from "vuex";
import moment from "moment";
import { scaleScreen, initFilters } from "../../components/commons/commons";
import { statusColor } from '../../components/commons/commonConstant';
import { Role } from "../../components/commons/commonConstant";
export default {
    name: "Onsite-Management",
    components: { OnsiteDetail, FilterLayout },
    data() {
        return {
            form: this.$form.createForm(this, { name: 'Onsite Request' }),
            countFiles: 0,
            userList,
            files: '',
            page: "Onsite Management",
            hiddenTable: false,
            clickButtonEdit: false,
            columns,
            data: [
                {
                    id: 1,
                    title: "Nghi onsite",
                    projectName: "RequestOnsiteA",
                    projectCode: "ASD",
                    fromDate: '01/03/2010',
                    toDate: '31/03/2021',

                    createdDate: "10/02/2021",

                    approver: {
                        name: "Nguyen Thu Trang B",
                        code: "CMC003466",
                        user_name: 'nttrang'
                    },
                    description: "LOLLLLLLLLLL1213",
                    lastModifiedDate: "19/02/2021",
                    adjustment: "yes",
                    createdBy: { name: "Nguyen Hoang Phan Hai Thien", code: "CMC003488", user_name: 'hhlinh' },
                    status_CB: {
                        status: "pending",
                        stage: "C&B",
                        status_num: 'P'
                    },
                    status_department: {
                        status: "approved",
                        stage: "Department",
                        status_num: 'A'
                    },
                    employeeList: [{
                            name: "Nguyen Thu Trang A",
                            code: "CMC003486",
                            user_name: 'nttrang',
                            type: "fulltime",
                            startTime: "08:30",
                            endTime: "17:30",
                            duration: "8:00",
                        },
                        {
                            name: "Nguyen Thu Trang B",
                            code: "CMC003487",
                            user_name: 'nttrang',
                            type: "fulltime",
                            startTime: "08:30",
                            endTime: "17:30",
                            duration: "8:00",
                        },
                        {
                            name: "Nguyen Thu Trang C",
                            code: "CMC003464",
                            user_name: 'nttrang',
                            type: "fulltime",
                            startTime: "08:30",
                            endTime: "17:30",
                            duration: "8:00",
                        }, {
                            name: "Nguyen Thu Trang A",
                            code: "CMC003423",
                            user_name: 'nttrang',
                            type: "fulltime",
                            startTime: "08:30",
                            endTime: "17:30",
                            duration: "8:00",
                        },
                        {
                            name: "Nguyen Thu Trang B",
                            code: "CMC003412",
                            user_name: 'nttrang',
                            type: "fulltime",
                            startTime: "08:30",
                            endTime: "17:30",
                            duration: "8:00",
                        },
                        {
                            name: "Nguyen Thu Trang C",
                            code: "CMC003456",
                            user_name: 'nttrang',
                            type: "fulltime",
                            startTime: "08:30",
                            endTime: "17:30",
                            duration: "8:00",
                        }, {
                            name: "Nguyen Thu Trang A",
                            code: "CMC003421",
                            user_name: 'nttrang',
                            type: "fulltime",
                            startTime: "08:30",
                            endTime: "17:30",
                            duration: "8:00",
                        },
                        {
                            name: "Nguyen Thu Trang B",
                            code: "CMC003433",
                            user_name: 'nttrang',
                            type: "fulltime",
                            startTime: "08:30",
                            endTime: "17:30",
                            duration: "8:00",
                        },
                        {
                            name: "Nguyen Thu Trang C",
                            code: "CMC003444",
                            user_name: 'nttrang',
                            type: "fulltime",
                            startTime: "08:30",
                            endTime: "17:30",
                            duration: "8:00",
                        }, {
                            name: "Nguyen Thu Trang A",
                            code: "CMC003455",
                            user_name: 'nttrang',
                            type: "fulltime",
                            startTime: "08:30",
                            endTime: "17:30",
                            duration: "8:00",
                        },
                        {
                            name: "Nguyen Thu Trang B",
                            code: "CMC003466",
                            user_name: 'nttrang',
                            type: "fulltime",
                            startTime: "08:30",
                            endTime: "17:30",
                            duration: "8:00",
                        },
                        {
                            name: "Nguyen Thu Trang C",
                            code: "CMC003477",
                            user_name: 'nttrang',
                            type: "fulltime",
                            startTime: "08:30",
                            endTime: "17:30",
                            duration: "8:00",
                        }
                    ]
                },
                {
                    id: 2,
                    title: "Nghi onsite",
                    projectName: "RequestOnsiteB",
                    projectCode: "ASD",
                    fromDate: '01/03/2010',
                    toDate: '31/03/2021',
                    startTime: "08:30",
                    endTime: "17:30",
                    duration: "8:00",
                    createdDate: "10/02/2021",
                    approver: {
                        name: "Nguyen Thu Trang B",
                        code: "CMC003488",
                        user_name: 'nttrang'
                    },
                    description: "LOLLLLLLLLLL1213",
                    lastModifiedDate: "19/02/2021",
                    adjustment: "yes",
                    createdBy: { name: "Nguyen Hoang Phan Hai Thien", code: "CMC003222", user_name: 'tlnham' },
                    status_CB: {
                        status: "approved",
                        stage: "C&B",
                        status_num: 'A'
                    },
                    status_department: {
                        status: "approved",
                        stage: "Department",
                        status_num: 'A'
                    },
                    employeeList: [{
                            name: "Nguyen Thu Trang A",
                            code: "CMC003499",
                            user_name: 'nttrang',
                            type: "fulltime",
                            startTime: "08:30",
                            endTime: "17:30",
                            duration: "8:00",
                        },
                        {
                            name: "Nguyen Thu Trang B",
                            code: "CMC003332",
                            user_name: 'nttrang',
                            type: "fulltime",
                            startTime: "08:30",
                            endTime: "17:30",
                            duration: "8:00",
                        },
                        {
                            name: "Nguyen Thu Trang C",
                            code: "CMC003473",
                            user_name: 'nttrang',
                            type: "fulltime",
                            startTime: "08:30",
                            endTime: "17:30",
                            duration: "8:00",
                        }
                    ]
                },
                {
                    id: 3,
                    title: "Nghi onsite",
                    projectName: "RequestOnsiteC",
                    projectCode: "ASD",
                    fromDate: '01/03/2010',
                    toDate: '31/03/2021',
                    description: "LOLLLLLLLLLL1213",
                    createdDate: "10/02/2021",
                    approver: {
                        name: "Nguyen Thu Trang B",
                        code: "CMC003498",
                        user_name: 'nttrang'
                    },
                    lastModifiedDate: "19/02/2021",
                    adjustment: "yes",
                    createdBy: { name: "Nguyen Hoang Phan Hai Thien", code: "CMC006682", user_name: 'tlnham' },
                    status_CB: {
                        status: "approved",
                        stage: "C&B",
                        status_num: 'A'
                    },
                    status_department: {
                        status: "approved",
                        stage: "Department",
                        status_num: 'A'
                    },
                    employeeList: [{
                        name: "Nguyen Thu Trang A",
                        code: "CMC001182",
                        user_name: 'nttrang',
                        type: "fulltime",
                        startTime: "08:30",
                        endTime: "17:30",
                        duration: "8:00",
                    }]
                },

                {
                    id: 4,
                    title: "Nghi onsite",
                    projectName: "RequestOnsiteC",
                    projectCode: "ASD",
                    fromDate: '01/03/2010',
                    toDate: '31/03/2021',
                    description: "LOLLLLLLLLLL1213",
                    createdDate: "10/02/2021",
                    approver: {
                        name: "Nguyen Thu Trang B",
                        code: "CMC007582",
                        user_name: 'nttrang'
                    },
                    lastModifiedDate: "19/02/2021",
                    adjustment: "yes",
                    createdBy: { name: "Nguyen Hoang Phan Hai Thien", code: "CMC000482", user_name: 'tlnham' },
                    status_CB: {
                        status: "approved",
                        stage: "C&B",
                        status_num: 'A'
                    },
                    status_department: {
                        status: "approved",
                        stage: "Department",
                        status_num: 'A'
                    },
                    employeeList: [{
                        name: "Nguyen Thu Trang A",
                        code: "CMC005682",
                        user_name: 'nttrang',
                        type: "fulltime",
                        startTime: "08:30",
                        endTime: "17:30",
                        duration: "8:00",
                    }]
                },
                {
                    id: 5,
                    title: "Nghi onsite",
                    projectName: "RequestOnsiteD",
                    projectCode: "ASD",
                    fromDate: '01/03/2010',
                    toDate: '31/03/2021',
                    startTime: "08:30",
                    endTime: "17:30",
                    duration: "8:00",
                    createdDate: "10/02/2021",
                    description: "LOLLLLLLLLLL1213",
                    approver: {
                        name: "Nguyen Thu Trang B",
                        code: "CMC004682",
                        user_name: 'nttrang'
                    },
                    lastModifiedDate: "19/02/2021",
                    adjustment: "yes",
                    createdBy: { name: "Nguyen Hoang Phan Hai Thien", code: "CMC006482", user_name: 'tlnham' },
                    status_CB: {
                        status: "approved",
                        stage: "C&B",
                        status_num: 'A'
                    },
                    status_department: {
                        status: "approved",
                        stage: "Department",
                        status_num: 'A'
                    },
                    employeeList: [{
                        name: "Nguyen Thu Trang A",
                        code: "CMC007282",
                        user_name: 'nttrang',
                        type: "fulltime",
                        startTime: "08:30",
                        endTime: "17:30",
                        duration: "8:00",
                    }]
                },
            ],
            currentPage: 1,
            pageSize: 10,
            field: 'Name',
            visible: false,
            sort: { direction: -1 },
            startValue: null,
            endValue: null,
            endOpen: false,
            actual: 1,
            total_items: 0,
            selectedItem : {},
            loading: false,
            fetching: false,
            timeout: null,
            source: {},
            EditVisible : false,
            dataSource: [],
            filters: {},
            statusColor: statusColor,
            Role,
            firstCall: true,
            recordDetail: {},
            onColumns: {},
            onStatus: {},
            staffOnsiteList: [{ userName: '', department: '', index: 0 }],
            projects: {},
            visibleDrawer: false,
            id: 1,
            tooltipOnsite: { config: { visible: false } },
            size: window.outerWidth > 1000 ? window.outerWidth * 0.6 : window.outerWidth * 0.55,
        };
    },
    beforeCreate() {
        this.staffOnsiteList_form = this.$form.createForm(this, { name: 'staffOnsiteList_form' });
        this.staffOnsiteList_form.getFieldDecorator('keys', { initialValue: [0], preserve: true });
    },

    created() {
        this.scaleScreen()
        window.addEventListener('resize',this.resize)
        this.handFilters()
        this.firstCall = false
        
    },
    computed: {
        ...mapState({
            roles: state => state.user.userInfo.roles ? state.user.userInfo.roles : "",
            projectList: state => state.application.projectList,
            employeeList: state => state.application.employeeList,
            userInfo: (state) => state.user.userInfo,
        }),
    },

    methods: {
        moment,
        scaleScreen,
        handFilters() {
			const { filters } = this
			initFilters(filters)
			this.filters = { ...this.filters, ...JSON.parse(sessionStorage.getItem(this.page)) }
		},
        resize(){
            if(document.body.clientWidth > 1000){
                this.size = document.body.clientWidth * 0.6
            }else{
                this.size = document.body.clientWidth * 0.55
            }
        },
        handleMouseLeave() {
            this.tooltipOnsite.config.visible = false;
        },
        handleHover(event, record, stage) {
            event.preventDefault();
            if (record && stage ) {
                this.tooltipOnsite.config = {
                    visible: true,
                    top: (event.pageY + 10) + 'px',
                    left: (event.pageX + 5) + 'px',
                }
                this.tooltipOnsite.employeeList = record.employeeList
                // this.tooltipOnsite.stage = stage
            }
        },
        showModal() {
            this.visible = true;
        },
        showEditModal() {
            this.EditVisible = true;
        },
        remove(k) {
            const { staffOnsiteList_form } = this;
            const keys = staffOnsiteList_form.getFieldValue('keys');
            if (keys.length === 0) {
                return;
            }
            // can use data-binding to set
            staffOnsiteList_form.setFieldsValue({
                keys: keys.filter(key => key !== k),
            });
            this.staffOnsiteList.splice(k, 1, { userName: '', department: '', index: k })
        },

        add() {
            this.scrollToEnd()
            const { staffOnsiteList_form, staffOnsiteList } = this;
            staffOnsiteList.push({ userName: '', department: '', index: this.id })
            const keys = staffOnsiteList_form.getFieldValue('keys');
            const nextKeys = keys.concat(this.id++);
            // important! notify form to detect changes
            staffOnsiteList_form.setFieldsValue({
                keys: nextKeys,
            });
        },
        getItem(i){
            this.selectedItem = i ;
        },

        handleChange(info) {
            if (info.file.status === 'done') {
                this.$message.success(`${info.file.name} file uploaded successfully`,5);
            } else if (info.file.status === 'error') {
                this.$message.error(`${info.file.name} file upload failed.`,5);
            }
            this.countFiles = info.fileList.length
        },

        scrollToEnd() {
            // var container = document.querySelector(".staffOnsiteList");
            var container = document.getElementsByClassName("staffOnsiteList");
            // var container = this.$refs.staffOnsiteList;
            container[0].scrollTop = (container[0].scrollHeight);
        },

        handleCancel() {
            this.visible = false;
        },
        customRow(record) {
            return {
                on: {
                    click: () => {
                        if(!this.clickButtonEdit){
                            this.recordDetail = record;
                            this.displayModal = true;
                            this.onColumns= userList;
                            this.showDrawer()
                        }else{
                            this.clickButtonEdit = false
                        }
                    },
                },
            };
        },
        onSetDataFilters(filters) {
            this.filters = filters;
        },
        onApplyFilters(filters) {
            this.filters = filters;
            this.currentPage = 1
            // this.getOTRequestList()
        },
        handleOnClickEdit() {
            this.clickButtonEdit = true
        },
        showDrawer() {
            this.visibleDrawer = true;
        },
        handelCloseEdit(){
            this.EditVisible = false;
        },
        handleOnClick(record) {
            if (!this.clickButtonEdit) {
                this.visibleDrawer = true;
                this.recordDetail = record;
                this.onColumns = this.userList;
            } else {
                this.clickButtonEdit = false
            }
        },
        onClose() {
            this.visibleDrawer = false;
            this.tooltipOnsite = { config: { visible: false } }
        },
        onChangeValue(value, index) {
            if (value === undefined) {
                this.staffOnsiteList.splice(index, 1, { userName: '', department: '', index: index })
            } else {
                let temp = JSON.parse(value);
                let newValue = true;
                if (this.staffOnsiteList.length > 0) {
                    this.staffOnsiteList.forEach(element => {
                        if (element.index === temp.index) {
                            this.staffOnsiteList.splice(element.index, 1, temp)
                            newValue = false
                        }
                    });
                }
                if (newValue) {
                    this.staffOnsiteList.push(temp)
                }
            }
        },

        onSearchStaff(value) {
            let cloneVal = decodeURI(value);
            if (value && value !== "" && value.length >= 3) {
                this.fetching = true;
                clearTimeout(this.timeout);
                this.timeout = setTimeout(() => {
                    this.$store
                        .dispatch("application/getEmployeeList", cloneVal)
                        .then(() => {
                            this.fetching = false;
                        })
                        .catch(() => {
                            this.fetching = false;
                        });
                }, 1000);

            }
        },

        filterOption(input, option) {
            return (
                option.componentOptions.children[0].text.toLowerCase().indexOf(input.toLowerCase()) >= 0
            );
        },
        onShowSizeChange(page, size) {
            this.currentPage = page;
            this.pageSize = size;
            // this.getAbnormalCaseData();
          },
          onChangePage(page, size) {
            this.currentPage = page;
            this.pageSize = size;
            // this.getAbnormalCaseData();
          }
    }
};