
export default {
      name: "onsite-detail",
      props: {
        data: Object,
        columns: Object,
        statusColor: {
          type: Function,
        },
        status_CB: Object,
        status_department: Object,
      },
      data() {
        return {
          fileList:[],
          editAssignee: false,
          searchEmployee: this.data.approver.name,
          delegator: this.data.approver.name,
          dataOptionFilterDelegator: [],
          formItemLayout: {
            labelCol: { span: 7 },
            wrapperCol: { span: 17 },
          },
          tooltipOnsite: {config: {visible:false}},
          size: window.outerWidth > 300 ? window.outerWidth * 0.4 : window.outerWidth * 0.3,
        }
      },
      beforeCreate() {
        this.form = this.$form.createForm(this, { name: 'onsite_detail' });
      },
      created() {
        window.addEventListener('resize',this.resize)
      },
      methods: {
        resize(){
          if(document.body.clientWidth > 1000){
              this.size = document.body.clientWidth * 0.4
          }else{
              this.size = document.body.clientWidth * 0.3
          }
        },
        handleDelegate(){
          this.editAssignee = !this.editAssignee
        },
        onClickClose(){
          this.editAssignee = false
          // this.searchEmployee = this.data.approver.name
        },
        onFileChange(fileList){
          this.fileList = fileList.fileList
        },
        onClickSave(){
          this.editAssignee = false
          this.data.approver.name = this.delegator
        },
        onSelect(item){this.delegator = item.name},
        getDataOptionFilterDelegator(value){
          let url = `?employee=${value}`;
          this.fetching = true;
          this.$store.dispatch('delegation/chooseDelegator',url).then(res => {
            if (res && res.data) {
              this.dataOptionFilterDelegator =  res.data
            }
            this.fetching = false;
          }).catch(error => {
            this.$message.error(error)
            this.fetching = false;
          })
        },
        onSearchDelegator(value){
          if(value && value !== '' && value.length >= 3){
            this.getDataOptionFilterDelegator(value);
          }
        },
        onChangeSearchModal(value) {
          this.searchEmployee = value;
        },
        onHandleSearchMember(){
          const {searchEmployee,delegatorAssign} = this;
          this.pageMember = 1;
          let url = `?delegatorId=${delegatorAssign.id}`
          if (searchEmployee.length !== 0){
             url = `?delegatorId=${delegatorAssign.id}&employee=${searchEmployee}`;
          }
          this.$store.dispatch('delegation/getListMemberOfCreatePageAble', url).then(res => {
            let data = [];
            if (res && res.data) {
              data = res.data.map((item) => {
                item.key = item.id;
                return item;
              })
              data.forEach(element => {
                this.keyMember = element.key 
              });
              let keyTemp = []        
              this.dataMemberOfCreate = [...data];
              this.dataMemberOfCreate.forEach(element => {
                keyTemp.push(element.key)
              }); 
              this.keyMember = keyTemp
            }
          }).catch(error => {
            this.$message.error(error)
          })
        },
        
        handleMouseLeave(){
          this.tooltipOnsite.config.visible = false;
      },
      handleHover(event,record,stage){
          event.preventDefault();
          if(record && stage){
          this.tooltipOnsite.config = {
              visible: true,
              top: (event.pageY+10) + 'px',
              left: (event.pageX-window.outerWidth*0.65 +5) + 'px',
          }
          this.tooltipOnsite.employeeList = record.employeeList
          }
        },
      },
      
    };
  