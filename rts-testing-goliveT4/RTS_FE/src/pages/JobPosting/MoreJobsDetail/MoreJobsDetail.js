import MoreJobsItem from '../../../components/MoreJobsItem/MoreJobsItem.vue';
import { mapState } from 'vuex';
export default {
  name: "MoreJobsDetail",
  components: {
    MoreJobsItem,
  },
  props: {
    moreJobList: Array,
  },
  computed: {
    ...mapState({
      pageOptions: (state) => state.rtsShareJob.pageOptions,
      totalItems: (state) => state.rtsShareJob.total_items,
    })
  },
  methods: {
  }
}