import MoreJobsDetail from '../MoreJobsDetail/MoreJobsDetail.vue';
import FeaturedJobs from '../../../pages/FeaturedJobs/FeaturedJobs.vue';
import JobFilterBlock from "../JobFilterBlock/JobFilterBlock";
import { mapActions, mapState, mapMutations } from "vuex";
import { PAGE_OPTIONS_MOREJOB_DETAIL, convertIdArrayToObjectArray } from "../../../components/commons/commons";
import CreateCvUploadModel from "../../../components/cv/CreateCvUploadModel";
import moment from "moment";
import JobPostingHeader from "../JobPostingHeader/JobPostingHeader";

export const PAGE_OPTIONS_LIST_JOB = {
    page: 1,
    size: 15,
}
export default {
    components: {
        JobFilterBlock,
        MoreJobsDetail,
        CreateCvUploadModel,
        FeaturedJobs,
        JobPostingHeader,
    },

    data() {
        return {
            isShowUpLoadCVModal: false,
            isShowConfirm: false,
            isShowDelete: false,
            page: 'detail',
            jobId: null,
            firstLoad: true,
            filters: {},
            pageOptions: {},
        }
    },

    computed: {
        ...mapState({
            jobPublicDetail: state => state.rtsJobPostingDetail.jobPublicDetail,
            categoryId: state => state.rtsJobPostingDetail.categoryId,
            listMoreJobDetail: state => state.rtsJobPostingDetail.listMoreJobDetail,
            skills: (state) => state.rtsApplication.skillList,
            loading: state => state.user.loading,
            listJob: state => state.rtsShareJob.jobList,
        })
    },

    watch: {
        categoryId: function () {
            if (this.categoryId && Number(this.categoryId) > 0) {
                const params = {
                    page: PAGE_OPTIONS_MOREJOB_DETAIL.offset,
                    size: PAGE_OPTIONS_MOREJOB_DETAIL.limit,
                    jobType: this.categoryId
                }
                this.getlistMoreJobDetail(params)
            }
        }
    },

    async created() {
        this.$store.commit('rtsShareJob/SET_CURRENT_PAGE', 'detail')
        this.jobId = this.$route.params.jobId
        if (this.jobId && this.jobId > 0) {
            await this.setLoading();
            await this.getJobPostingDetail(this.jobId);
            await this.getSkillList(' ');
            await this.stopLoading();
        }
    },

    beforeUpdate() {
        if (this.categoryId > 0) {
            this.jobPublicDetail.skillName = this.mapSkillString()
        }
    },

    methods: {
        ...mapActions({
            getSkillList: 'rtsApplication/getSkillList',
            getJobPostingDetail: 'rtsJobPostingDetail/getJobPostingDetail',
            getlistMoreJobDetail: 'rtsJobPostingDetail/getlistMoreJobDetail',
            getListJob: 'rtsShareJob/getListJobs',
            setLoading: 'user/loadingRequest',
            stopLoading: 'user/stopLoading',
            changeCreateStatus: "rtsJobPostingDetail/changeCreateStatus",
        }),
        ...mapMutations({
            detailSearch: 'rtsShareJob/SET_DETAIL_SEARCH',
        }),
        openUploadCV() {
            this.changeCreateStatus("None")
            this.isShowUpLoadCVModal = true;
        },

        onCancelPopup() {
            this.isShowUpLoadCVModal = false;
        },

        formatDate(date) {
            return moment(date).format("DD-MM-YYYY");
        },

        onApplyFilter(filters) {
            if(!filters.keyword && !filters.area && !filters.deadline && !filters.jobType && !filters.experiences &&!filters.salary ) return
            this.$store.commit("rtsShareJob/SET_CURRENT_FILTER", filters)
            this.filters = { ...filters };
            let params = {
                ...filters,
                size: PAGE_OPTIONS_LIST_JOB.size,
                page: PAGE_OPTIONS_LIST_JOB.page
            };
            this.pageOptions = { ...params }
            let paramCategoryId = this.$route.params.categoryId
            if (paramCategoryId) {
                params.jobType = paramCategoryId
            }
            this.addLoadingEvent(this.getListJob, this.pageOptions)
            this.detailSearch(params)
            this.goDetailCategoryPage(0, this.pageOptions)
        },

        goDetailCategoryPage(id,params) {
            if (!id && id !== 0) return;
            this.$router.push(`/rts/job-posting/${id}`)
            .catch(() => { });
        },

        onChangePage(payload) {
            this.addLoadingEvent(this.getListJob, {
                ...this.pageOptions,
                size: PAGE_OPTIONS_LIST_JOB.size,
                page: payload ? payload : PAGE_OPTIONS_LIST_JOB.page,
                jobType: this.categoryId
            })
        },

        addLoadingEvent(promiseFunc) {
            this.setLoading();
            promiseFunc(arguments[1]).then(() => {
                this.stopLoading();
            })

        },

        showJobByCategory(id) {
            return new Promise(resolve => {
                this.page = "search";
                this.categoryId = id;
                this.getListJob({
                    size: PAGE_OPTIONS_LIST_JOB.size,
                    page: PAGE_OPTIONS_LIST_JOB.page,
                    jobType: id
                }).then(() => {
                    resolve();
                })
            });
        },

        mapSkillString() {
            let skillNames = convertIdArrayToObjectArray(this.jobPublicDetail?.skillNameId?.split(''), 'id', this.skills)
            skillNames = skillNames.map(item => item.name)
            return skillNames.join(', ')
        }
    }
}
