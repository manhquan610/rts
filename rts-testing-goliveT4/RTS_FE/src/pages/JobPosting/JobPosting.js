import FeaturedJobs from '../FeaturedJobs/FeaturedJobs.vue';
import PopularJobs from '../PopularJobs/PopularJobs.vue';
import JobFilterBlock from "./JobFilterBlock/JobFilterBlock";
import JobPostingHeader from "./JobPostingHeader/JobPostingHeader"
import {mapActions, mapState} from "vuex";
import {PAGE_OPTIONS_FEATUREDJOB} from "../../components/commons/commons";


export const PAGE_OPTIONS_LIST_JOB = {
    page: 1,
    size: 15,
}
export default {
    components: {
        JobFilterBlock,
        FeaturedJobs,
        PopularJobs,
        JobPostingHeader,
    },
    data() {
        return {
            page: 'home',
            categoryId: null,
            firstLoad: true,
            filters: {},
            pageOptions : {}
        }
    },
    watch: {
        detailSearch: {
            immediate: true,
            handler(newVal) {
                if(newVal.length === 0 ) return

              this.page = 'search'
              this.addLoadingEvent(this.getListJob,newVal)
            },
          },
    },
    computed: {
        ...mapState({
            listFeturedJob: state => state.rtsShareJob.listFeturedJob,
            listJob: state => state.rtsShareJob.jobList,
            detailSearch: state => state.rtsShareJob.detailSearch,
            isClearFilter: (state) => state.rtsShareJob.isClear,
            currentPage: (state) => state.rtsShareJob.currentPage,
        })
    },
    created() {
        this.isClearFilter ?  this.page= 'home': ''
        this.$store.commit('rtsShareJob/SET_CURRENT_PAGE', 'home')
        const params = {
            page: PAGE_OPTIONS_FEATUREDJOB.offset,
            size: PAGE_OPTIONS_FEATUREDJOB.limit
        }
        this.categoryId = this.$route.params.categoryId
        if (this.categoryId > 0) {
            this.addLoadingEvent(this.showJobByCategory, this.categoryId)
        } else {
            this.addLoadingEvent(this.getListFeaturedJob,  params)
        }
    },
    beforeRouteUpdate(to, from, next) {
        if (to.name === 'JobPosting') {
            let cateId = to.params?.categoryId
            this.categoryId = to.params?.categoryId
            if (cateId > 0 || Number(cateId) === 0) {
                this.page= 'search'
                this.$store.commit('rtsShareJob/SET_CURRENT_PAGE', 'home')
                this.addLoadingEvent(this.showJobByCategory, cateId)
            } else {
                this.firstLoad = true;
                this.page = 'home'
                this.$store.commit('rtsShareJob/SET_CURRENT_PAGE', 'home')
                this.addLoadingEvent(this.getListFeaturedJob, {
                    page: PAGE_OPTIONS_FEATUREDJOB.offset,
                    size: PAGE_OPTIONS_FEATUREDJOB.limit,
                    jobType: this.categoryId
                })
            }
        }
        next();
    },
    methods: {
        ...mapActions({
            getListJob: 'rtsShareJob/getListJobs',
            getListFeaturedJob: 'rtsShareJob/getListFeaturedJob',
            setLoading: 'user/loadingRequest',
            stopLoading: 'user/stopLoading',
        }),
        onApplyFilter(filters) {
            if(!filters.keyword && !filters.area && !filters.deadline && !filters.jobType && !filters.experiences &&!filters.salary ) return
            this.categoryId = filters?.jobType ? filters.jobType : this.categoryId
            this.filters = {...filters};
            let params = {
                ...filters,
                size: PAGE_OPTIONS_LIST_JOB.size,
                page: PAGE_OPTIONS_LIST_JOB.page,
                jobType: Number(this.categoryId)
            };

            this.pageOptions = {...params}
            let paramCategoryId = this.$route.params.categoryId

            // case 1 :job posting detail page
            if(Number(paramCategoryId) > 0) {
                // fetch data with delay
                this.addLoadingEvent(this.getListJob,params)
            }
            // case 2 :job posting home page
            else if(this.filters) {
                this.page = 'search'
                this.addLoadingEvent(this.getListJob,params)
            }
        },

        onCleanFilter(){
            this.pageOptions = {}
            if(this.categoryId > 0) {
                this.goDetailCategoryPage(this.categoryId);
                this.showJobByCategory(this.categoryId)
                this.$router.push(`/rts/job-posting`).catch(()=>{});
            }else{
                this.page= 'home'
                this.$router.push(`/rts/job-posting`).catch(()=>{});
            }

        },
        goDetailCategoryPage(id) {
            if(!id && id !==0 ) return;
            this.categoryId = id;
            this.$router.push(`/rts/job-posting/${id}`).catch(()=>{});
        },

        onChangePage(payload) {
            if (this.page === 'home') {
                const params = {
                    ...this.pageOptions,
                    page: payload,
                    size: PAGE_OPTIONS_FEATUREDJOB.limit,
                    jobType: this.categoryId,
                }
                this.addLoadingEvent(this.getListFeaturedJob, params)
            } else if (this.page === 'search') {
                this.addLoadingEvent(this.getListJob, {
                    ...this.pageOptions,
                    size: PAGE_OPTIONS_LIST_JOB.size,
                    page: payload ? payload : PAGE_OPTIONS_LIST_JOB.page,
                    jobType: this.categoryId,
                })
            }
        },
        showJobByCategory(id) {
            return new Promise(resolve => {
                this.page = "search";
                this.categoryId = id;
                this.getListJob({
                    size: PAGE_OPTIONS_LIST_JOB.size,
                    page: PAGE_OPTIONS_LIST_JOB.page,
                    jobType: id
                }).then(() => {
                    resolve();
                })
            });
        },
        addLoadingEvent(promiseFunc) {
            this.setLoading();
            promiseFunc(arguments[1]).then(() => {
                this.stopLoading();
            })

        }
    }
}
