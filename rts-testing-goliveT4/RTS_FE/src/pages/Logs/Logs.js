const dataSelectSearch = ['timesheet_detail', 'random_table']
const logListColumns = [
  {
    id: 1,
    title: "Staff",
    key: "name",
    dataIndex: "name",
    sorter: true,
  },

  {
    id: 2,
    title: "Action",
    dataIndex: "action",
    key: "action",
    scopedSlots: { customRender: 'action' }
  },
  {
    id: 3,
    title: "Time",
    dataIndex: "time",
    key: "time",
    ellipsis: true,
  },
  {
    id: 4,
    title: "Table",
    dataIndex: "table",
    key: "table",
    scopedSlots: { customRender: 'table' }
  },
  {
    id: 5,
    title: "Field",
    dataIndex: "field",
    key: "field",
    ellipsis: true,
  },
  {
    id: 6,
    title: "Employee Modified",
    dataIndex: "employee_modified",
    key: "employee_modified",
    ellipsis: true,
  },
  {
    id: 7,
    title: "Workday Modified",
    dataIndex: "workday_modified",
    key: "workday_modified",
    ellipsis: true,
  },
  {
    id: 8,
    title: "New Value",
    dataIndex: "new_value",
    key: "new_value",
    ellipsis: true,
  },
  {
    id: 9,
    title: "Old Value",
    dataIndex: "old_value",
    key: "old_value",
    ellipsis: true,
  }
];
const connectionHistoryColumns = [
  {
    id: 1,
    title: "Date and time",
    dataIndex: "time",
    key: "time",
    width: 180
  },
  {
    id: 2,
    title: "Error",
    dataIndex: "code",
    key: "code",
    width: 150
  },
  {
    id: 3,
    title: "Details",
    dataIndex: "error_details",
    key: "error_details",
    ellipsis: true,
  },
];

import { mapState } from "vuex";
import FilterLayout from '../../components/layouts/FilterLayout'
import {
  convertObjectToParams,scaleScreen
  //formatDate
} from "../../components/commons/commons";
import moment from 'moment'
export default {
  name: "Logs",
  components: { FilterLayout },
  data() {
    return {
      total_items: 0,
      total_itemConnection: 0,
      keyPagination: 0,
      logListColumns,
      dataSelectSearch,
      connectionHistoryColumns,
      columns: logListColumns,
      pagination: {},
      dataLogsList: [],
      dataConnection: [],
      pageSizeLogsList: 10,
      currentPageLogsList: 1,
      pageSizeConnectionHistory: 10,
      currentPageConnectionHistory: 1,
      dataSource: [],
      currentTab: 'log_list',
      filters: {
        fromDate: moment().startOf('month'),
        toDate: moment().endOf('month'),
      },
      filtersLogsList: {
        fromDate: moment().startOf('month'),
        toDate: moment().endOf('month'),
      },
      filtersConnectionHistory: {
        fromDate: moment().startOf('month'),
        toDate: moment().endOf('month'),
      },
      tabList: [
        {
          key: 'log_list',
          label: 'Logs List'
        },
        {
          key: 'connection_history',
          label: 'Connection History'
        },
      ],
      firstCallLogsList: true,
      firstCallConnectionHistory: true,
      page: 'Logs List'
    };
  },
  computed: {
    ...mapState({
      logsListData: (state) => state.logs.logsListData,
      connectionHistoryData: (state) => state.logs.connectionHistoryData
    })
  },
  watch: {
    // currentTab: function () {
    //   this.filters = {
    // 		fromDate: moment().startOf('month'),
    // 		toDate: moment().endOf('month'),
    // 	}
    //   this.getDataSource(this.filters);
    // }
  },
  created() {
    this.scaleScreen()
    this.filters = { ...this.filters, ...JSON.parse(sessionStorage.getItem(this.page)) }
  },
  mounted() {
  },
  methods: {
    scaleScreen,
    //// Get Logs
    getLogsList(filters) {
      const { pageSizeLogsList, currentPageLogsList, } = this;
      let param = {}
      if (this.firstCallLogsList) {
        param = {
          ...filters
        }
        this.currentPageLogsList = filters.page ? filters.page : 1
        this.pageSizeLogsList = filters.size ? filters.size : 10
      } else {
        param = {
          ...filters,
          page: currentPageLogsList,
          size: pageSizeLogsList,
        };
      }
      let url = convertObjectToParams(param);
      this.$store.dispatch('getLogsListData', url).then((res) => {
        let dataSource = []
        if (res && res.data) {
          dataSource = res.data.map((item, key) => {
            item.key = key
            item.name = item.creator ? item.creator.name : null;
            item.employee_modified = item.employeeId ? item.employeeId.name : null;
            item.workday_modified = item.dateOfIssue ? this.formatDate(item.dateOfIssue) : null
            if (item.status === 0) {
              item.action = 'Created'
              item.time = this.formatDateAndTime(item.create_date)
            } else if (item.status === 1) {
              item.action = 'Deleted'
              item.time = this.formatDateAndTime(item.modify_date)
            } else if (item.status === 2) {
              item.action = 'Updated'
              item.time = this.formatDateAndTime(item.modify_date)
            }
            return item
          })
          this.total_items = res.page.total_element
          this.dataLogsList = dataSource;
        } else if (res && res.message !== null && res.message !== undefined && res.message !== '') {
          this.$message.error(res.message,5);
        }
        this.dataSource = dataSource;
        this.$store.dispatch('user/loadingRequest')
      }).catch(error => {
        this.$message.error(error.message,5);
        this.dataSource = [];
        this.$store.dispatch('user/loadingRequest')
      })
      return param
    },

    /// get connecttion hisotry
    getConnectionList(filters) {
      const { pageSizeConnectionHistory, currentPageConnectionHistory, } = this;
      let param = {}
      if (this.firstCallConnectionHistory) {
        param = {
          ...filters
        }
        this.currentPageConnectionHistory = filters.page ? filters.page : 1
        this.pageSizeConnectionHistory = filters.size ? filters.size : 10
      } else {
        param = {
          ...filters,
          page: currentPageConnectionHistory,
          size: pageSizeConnectionHistory,
        };
      }
      let url = convertObjectToParams(param);
      this.$store.dispatch('getConnectionHistoryData', url).then((res) => {
        let dataSource = [];
        if (res && res.data) {
          dataSource = res.data.map(item => {
            item.time = this.formatDateAndTime(item.date_time)
            return item
          })
          this.dataConnection = dataSource;
          this.total_items = res.page.total_element
        }
        else if (res && res.message !== null && res.message !== undefined && res.message !== '') {
          this.$message.error(res.message,5);
        }
        this.dataSource = dataSource;
        this.$store.dispatch('user/loadingRequest')
      }).catch(error => {
        this.$message.error(error.message,5);
        this.dataSource = [];
        this.$store.dispatch('user/loadingRequest')
      })
      return param
    },

    formatDateAndTime(date) {
      if (date && date !== null) {
        return moment(date).format('DD/MM/YYYY HH:mm:ss')
      }
    },

    formatDate(date) {
      return moment(date).format("DD/MM/YYYY");
    },

    getDataSource(filters) {
      this.$store.dispatch('user/loadingRequest')
      const { currentTab } = this;
      let param = {}
      if (currentTab === 'log_list') {
        param = this.getLogsList(filters);
        this.firstCallLogsList = false
      } else {
        param = this.getConnectionList(filters);
        this.firstCallConnectionHistory = false
      }
      sessionStorage.setItem(this.page, JSON.stringify(param))
    },

    onSetDataFilters() {
    },

    onApplyFilters(filters) {
      this.filters = filters;
      if (this.currentTab === 'log_list') {
        this.currentPageLogsList = 1;
      } else {
        this.currentPageConnectionHistory = 1;
      }
      this.getDataSource(filters)
    },

    onShowSizeChange(page, pageSize) {
      if (this.currentTab === 'log_list') {
        this.currentPageLogsList = page;
        this.pageSizeLogsList = pageSize;
      } else {
        this.currentPageConnectionHistory = page;
        this.pageSizeConnectionHistory = pageSize;
      }
      this.getDataSource(this.filters);
    },

    onChangePage(page, pageSize) {
      if (this.currentTab === 'log_list') {
        this.currentPageLogsList = page;
        this.pageSizeLogsList = pageSize;
      } else {
        this.currentPageConnectionHistory = page;
        this.pageSizeConnectionHistory = pageSize;
      }
      this.getDataSource(this.filters);
    },

    onChangeTabs(key) {
      this.currentTab = key;
      if (key !== 'log_list') {
        this.page = 'Connection History'
        this.filters = { ...this.filtersConnectionHistory, ...JSON.parse(sessionStorage.getItem(this.page)) }
        this.columns = connectionHistoryColumns
      } else {
        this.page = 'Logs List'
        this.filters = { ...this.filtersLogsList, ...JSON.parse(sessionStorage.getItem(this.page)) }
        this.columns = logListColumns
      }
      this.getDataSource(this.filters)
    },
    handleSelectChange(value) {
      this.filter = 'table';
      this.valueToFilter = value;
    },
    handleOnClick() {
      this.getLogsList();
    }
  },
};
