import { mapState } from "vuex";
import moment from "moment";
import { statusText } from '../../../components/commons/commonConstant';
import http from "../../../store/modules/helpers/httpInterceptor";
import { downloadFile} from "../../../components/commons/commons";
export default {
    name: "abnormal-detail",
    props: {
      data: Object,
      Role :Object,
      userInfo: Object,
      fileList : {type: Array},
      isAvaiableToDownLoad: Boolean,
      getAbnormalCaseData:{
        type: Function
      },
      checkMoreVisible:{
        type: Function
      },
      statusColor: Object,
      formatDate: {
        type: Function,
      },
      formatTime: {
        type: Function,
      },
      checkEditVisible:{
        type: Function
      },
      checkEditChangeStatusVisible:{
        type: Function
      },
      checkDateInApprovedSchedule:{
        type: Function
      },
    },
    data() {
      return {
        statusPayload: {},
        delegator: null,
        assignee: (this.data.assignee && this.data.assignee.department_name && this.data.assignee.name) ? `${this.data.assignee.name}  -   ${this.data.assignee.department_name.nameEn}` : '',
        reasonReject: this.data.request ? (this.data.request.reasons_for_rejection ? this.data.request.reasons_for_rejection : ''): '',
        formItemLayout: {
          labelCol: { span: 8 },
          wrapperCol: { span: 16 },
        },
        visibleDelegate: false,
        visibleModalReject: false,
        isSingleRecord: false,
        dataSource: this.data,
        statusDetail: this.data.statusText,
        request_type: 'abnormal-case',
      }
    },
    beforeCreate() {
      this.form = this.$form.createForm(this, { name: 'abnormal_detail' });
    },
    computed: {
      ...mapState({
          roles: state => state.user.userInfo.roles ? state.user.userInfo.roles : "",
          employeeList: state => state.application.employeeList,
      })
  },
  watch:{
    data: function(){
      this.dataSource = this.data
      this.assignee =  this.data.assignee && this.data.assignee.department_name ? `${this.data.assignee.name}  -   ${this.data.assignee.department_name.nameEn}` : ''
    }
  },
    methods: {
      onSearchDelegator(value) {
        let cloneVal = decodeURI(value);
        if (value && value !== "" && value.length >= 3) {
            this.fetching = true;
            clearTimeout(this.timeout);
            this.timeout = setTimeout(() => {
                this.$store
                    .dispatch("application/getEmployeeList", cloneVal)
                    .then(() => {
                        this.fetching = false;
                    })
                    .catch(() => {
                        this.fetching = false;
                    });
            }, 1000);
        }
    },
      handleDelegate(){
        this.visibleDelegate = !this.visibleDelegate
      },
      handCancelDelegate() {
        this.delegator = null;
        this.visibleDelegate = false;
    },
      handleCancel() {
        this.visibleModalReject = false;
    },
      handleSaveDelegate(){
       const {data, dataSource} = this
        let payload ={ 
            id: data.request ? data.request.id : '',
            reason_type: data.reason_detail,
            status_type: dataSource.statusText,
            approver: JSON.parse(this.delegator).id,
            description: data.request ? data.request.description : '',
            created_date: moment(data.created_date).format('YYYY-MM-DD'),
            last_modified: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
            start_date: data.request ? data.request.start_date : '',
            end_date: data.request ? data.request.end_date : '',
            start_time: data.request ? data.request.start_time : '',
            end_time: data.request ? data.request.end_time : '',
            employee: data.employee ? data.employee.id : '',
            leaving_type: data.request ? data.request.leaving_type : '',
            mail_cc: data.request ? data.request.mail_cc : [],
        
      }

        this.$store.dispatch('request/updateRequestDelegate',payload).then(res => {
          if(res){
            if (parseInt(res.code) === 0) {
              this.assignee = JSON.parse(this.delegator).label + ' - ' +  JSON.parse(this.delegator).department
            this.$message.success('Update status successfully!',5);
            dataSource.assignee ={}
            dataSource.assignee.user_name = JSON.parse(this.delegator).value
            this.visibleDelegate=false;
            this.getAbnormalCaseData()
            }else{
              this.$message.error(res.message,5);
            }      
          }
      }).catch(() => {
          this.$message.error('Failed to update status!',5);
      });
        
      },
      
      onClickApproved(record,status) {
        this.statusPayload = {data:{
          status_type: statusText[status],
          approve_id: this.userInfo.userId,
          reasons_for_rejection: '',
          request_id: record.request ? record.request.id : this.message.error("No request created", 5)
        },id: record.id
        }
        this.updateStatus(this.statusPayload,3)
      },
      checkApproved(record){
        if(record.status ===  3){
          return true;
        }
        return false;
      },
      updateStatus(payload,status) {
        this.$store.dispatch('abnormalcase/updateStatusAbnormal',  {data: payload.data, id: payload.id}).then(res => {
          if (res) {
            if(parseInt(res.code) === 0){
            this.$message.success('Update status successfully!',5);
            this.reasonReject = this.dataSource.reasonReject 
            this.dataSource.statusText = payload.data.status_type
            this.dataSource.status = status
            }else{
              this.$message.error(res.message,5)
            }
            this.getAbnormalCaseData()
          }
        }).catch(() => {
          this.$message.error('Failed to update status!',5);
        });
      },
      handleOk(){
        if(this.dataSource.reasonReject && this.dataSource.reasonReject.trim() !== ''){
        this.statusPayload.data.reasons_for_rejection = this.reasonReject
        this.updateStatus(this.statusPayload,4)
        this.visibleModalReject = false
        }else{
          this.$message.error('Message required!', 5)
        }
      },
      onClickReject(record,status) {
        this.statusPayload = {
          data:{
          status_type: statusText[status],
          approve_id: this.userInfo.userId,
          request_id: record.request ? record.request.id : this.message.error("No request created", 5)
        },id: record.id
        }
        this.visibleModalReject = true
      },
      handleCancelRejectModel(){
        this.visibleModalReject = false
      },
      getAllListFile(id) {
        const { data } = this;
        http.get(`/tms/request-management/${this.request_type}/get-all-file-list?id=` + id,)
          .then((res) => {
            res.data.forEach(record => {
              data.push(record)
            })
            return res.data
          })
          .catch((err) => {
            throw err
          })
      },
      handleDownload() {
        const { fileList } = this
        fileList.forEach(file => {
          http.get(`/tms/request-management/${this.request_type}/${this.data.id}/download?name=` + file.name, { responseType: 'blob' })
            .then(res => {
              downloadFile(res.data, file.name)
              return res.data;
            })
            .catch(err => {
              throw err
            })
        })
      }
      
    },
  };