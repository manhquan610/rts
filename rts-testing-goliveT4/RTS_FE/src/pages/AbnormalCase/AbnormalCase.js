const columns = [
  {
    title: "Staff",
    dataIndex: "display_name",
    key: "display_name",
    scopedSlots: { customRender: "display_name" },
    width: '15%',
  },
  {
    title: "Abnormal Date",
    dataIndex: "ab_date",
    key: "ab_date",
    width: '10%'
  },
  {
    title: "Type",
    dataIndex: "type",
    key: "type",
    width: '12%'
  },
  {
    title: "Check In / Check Out",
    dataIndex: "check_in_out",
    key: "check_in_out",
    scopedSlots: { customRender: "check_in_out" },
    width: '12%'
  },
  {
    title: "Absent Time (min)",
    dataIndex: "minute_violation",
    key: "minute_violation",
    width: '12%'
  },
  {
    title: "Actual",
    dataIndex: "actualType",
    key: "actualType",
    width: '12%'
  },
  {
    title: "Approver",
    dataIndex: "approver_name",
    key: "approver_name",
    scopedSlots: { customRender: "approver_name" },
    width: '20%'
  },
  {
    title: "Status",
    dataIndex: "statusType",
    key: "statusType",
    scopedSlots: { customRender: "statusType" },
    width: '5%'
  },
  {
    title: "",
    dataIndex: "edit",
    scopedSlots: { customRender: "edit" },
    width: '2%'
  },
  // {
  //   title: "Action",
  //   key: "action",
  //   width: 100,
  //   align:'center',
  //   scopedSlots: { customRender: "action" },
  // },
];
import AbnormalCaseDetail from "./AbnormalCaseDetail/AbnormalCaseDetail.vue";
import FilterLayout from "../../components/layouts/FilterLayout";
import { mapState } from "vuex";
import moment from "moment";
import { convertObjectToParams, formatDate, formatTime, scaleScreen, downloadFile, initFilters } from "../../components/commons/commons";
import { statusColor, statusText } from '../../components/commons/commonConstant';
import { Role, leavingType } from "../../components/commons/commonConstant";
import http from '../../store/modules/helpers/httpInterceptor'
export default {
  name: "Abnormal-case",
  components: { AbnormalCaseDetail, FilterLayout },
  data() {
    return {
      page: "Abnormal Case",
      columns,
      leavingType,
      currentPage: 1,
      pageSize: 10,
      field: 'Name',
      visible: false,
      reasonReject: '',
      visibleModalReject: false,
      visibleUpdateAbnomal: false,
      sort: { direction: 1 },
      startValue: null,
      endValue: null,
      endOpen: false,
      actual: 1,
      statusPayload: {},
      total_items: 0,
      loading: false,
      timeOut: null,
      source: {},
      form: this.$form.createForm(this, { name: 'Explanation' }),
      formUpdateJob: this.$form.createForm(this, { name: 'updateAbnormal' }),
      dataSource: [],
      filters: {},
      recordDetail: {},
      fetching: false,
      formData: {},
      statusColor: statusColor,
      Role,
      statusText,
      tempCheckDateInApprovedTimeSheet: [],
      firstCall: true,
      isEdit: false,
      clickButtonEdit: false,
      visibleDrawer: false,
      sizeReason: document.body.clientWidth / 6 + 'px',
      tooltipOnsite: { config: { visible: false } },
      isDisableExport: true,
      size: window.outerWidth > 1000 ? window.outerWidth * 0.6 : window.outerWidth * 0.55,
      fileList: [],
      isAvaiableToDownLoad: false,
      request_type: 'abnormal-case',
      download_id : '',
    };
  },
  created() {
    this.scaleScreen()
    this.$store.dispatch('abnormalcase/getParamsDataToSelectActual')
    this.$store.dispatch("application/getReasonRequestList");
    this.getLastTimesheetInfo();
    window.addEventListener('resize', () => this.reportWindowSize());
    this.handFilters()
    this.firstCall = true
  },
  computed: {
    ...mapState({
      reasonTypeList: state => state.application.reasonTypeList,
      userInfo: (state) => state.user.userInfo,
      abnormalList: (state) => state.abnormalcase.abnormalList.data,
      employeeList: state => state.application.employeeList,
      absentTypeList: state => state.application.absentTypesList,
      reasonRequestList: state => state.application.reasonRequestList,
      roles: state => state.user.userInfo.roles ? state.user.userInfo.roles : "",
      dataActual: state => state.abnormalcase.dataActual,
      lastTimesheet: state => state.application.lastTimesheet,
    }),
  },
  watch: {
    // formData: function () {
    //   this.form.setFieldsValue(this.formData)
    // },
    filters: function () {
      if (this.filters.fromDate !== undefined && this.filters.toDate !== undefined) {
        this.isDisableExport = false
      } else {
        this.isDisableExport = true
      }
    }
  },
  methods: {
    formatTime,
    formatDate,
    scaleScreen,
    handFilters() {
			const { filters } = this
			initFilters(filters)
      if (sessionStorage.getItem(this.page)) {
        this.filters = { ...JSON.parse(sessionStorage.getItem(this.page)) }
      } else {
        this.filters = { ...this.filters, ...JSON.parse(sessionStorage.getItem(this.page)) }
      }
		},
    handleHover(event, description) {
      event.preventDefault();
      if (description) {
        this.tooltipOnsite.config = {
          visible: true,
          top: (event.pageY + 10) + 'px',
          left: (event.pageX + 5) + 'px',
        }
        this.tooltipOnsite.description = description
      }
    },
    handleMouseLeave() {
      this.tooltipOnsite.config.visible = false;
    },
    reportWindowSize() {
      this.sizeReason = document.body.clientWidth / 6 + 'px'
      if (document.body.clientWidth > 1000) {
        this.size = document.body.clientWidth * 0.6
      } else {
        this.size = document.body.clientWidth * 0.55
      }
    },
    checkEditVisible(record) {
      return record.employee.user_name === this.userInfo.userName && !record.is_expired
    },
    checkMoreVisible(record) {
      return (record.status === 1 || record.status === 3) && (this.checkEditChangeStatusVisible(record) || this.checkEditVisible(record))
    },
    checkEditChangeStatusVisible(record) {
      return ((record.assignee ? (record.assignee.user_name === this.userInfo.userName) : (record.assignee && record.assignee.user_name === this.userInfo.userName)) && (record.status === 1 || record.status === 3))
    },
    customRow(record) {
      return {
        on: {
          click: () => {
            this.isAvaiableToDownLoad = false
            this.fileList.splice(0, this.fileList.length)
            if (!this.clickButtonEdit) {
              if (!this.visible) {
                this.visibleDrawer = true;
                this.recordDetail = record;
                this.getAllListFile(record.id);
              } else {
                this.visible = true
              }
            } else {
              this.clickButtonEdit = false
            }
          },
        },
      };
    },
    onSearchStaff(value, timeout) {
      let cloneVal = decodeURI(value);
      if (value && value !== "" && value.length >= 3) {
        this.fetching = true;
        clearTimeout(this.timeout);
        this.timeout = setTimeout(() => {
          this.$store
            .dispatch("application/getEmployeeList", cloneVal)
            .then(() => {
              this.fetching = false;
            })
            .catch(() => {
              this.fetching = false;
            });
        }, timeout);

      }
    },
    onClose() {
      this.visibleDrawer = false;
    },

    handleRowClassName(record) {
      let query = this.$route.query;
      if (query && query.id && query.id === record.id) {
        // let payload = {
        //   id: query.id
        // }
        // this.$store.dispatch("abnormalcase/readNotification", payload)
        return 'active-row-abnormal-case';
      }
      else return '';
    },
    onClickMore(record) {

      this.clickButtonEdit = true
      if (!this.roles.includes(Role.GUEST)) {
        let temp = ''
        if (record.request.mail) {
          record.request.mail.forEach(element => {
            temp = temp + ' ' + element.user_name
          });
        }
        this.formData = {
          request: record.request_type,
          staff: `${record.employee.name} - ${record.employee.department_name.nameEn}`,
          type: record.reason_type,
          date: record.ab_date,
          assigner: record.assigner,
          approver: record.approver_name,
          from: record.from_hour ? record.from_hour : undefined,
          to: record.to_hour ? record.to_hour : undefined,
          description: record.request ? record.request.description : undefined,
          cc: record.request.mail_cc ? record.request.mail_cc : undefined,
          actual_type: record.request.leaving_type
        }
        this.onSearchStaff(temp)
      }
    },
    onSetDataFilters(filters) {
      this.filters = filters;
    },
    onClickEdit(record) {
      this.fileList.splice(0, this.fileList.length)
      this.getAllListFile(record.id)
      this.download_id = record.id
      this.$store.dispatch('user/loadingRequest')
      this.source = record
      this.isEdit = true
      setTimeout(() => {
        this.visible = true
        this.$store.dispatch('user/loadingRequest')
      }, 500)
    },
    getAbnormalCaseData() {
      this.$store.dispatch('user/loadingRequest');
      const { pageSize, currentPage, sort, filters } = this
      let param = {}
      if (this.firstCall) {
        param = {
          ...filters,
          ...sort
        }
        this.currentPage = filters.page ? filters.page : 1
        this.pageSize = filters.size ? filters.size : 10
      } else {
        param = {
          ...filters,
          ...sort,
          page: currentPage,
          size: pageSize,
        };
      }
      const url = convertObjectToParams(param);
      this.$store.dispatch('abnormalcase/getAbnormalCaseData', url).then((res) => {
        let data = []

        if (res.data) {
          res.data.forEach(element => {
            this.checkDateInApprovedSchedule(element.abnormal_date)
          });
          setTimeout(() => {
            data = res.data.map((element, index) => {
              element.display_name = element.employee.name
              if (element.employee.department_name) {
                element.department = element.employee.department_name.name_en
              } else {
                element.department = element.employee.department
              }
              this.dataActual.forEach(type => {
                if (element.request.leaving_type === type.code) {
                  element.actualType = type.message
                }
              });
              element.ab_date = formatDate(element.abnormal_date)
              element.approver_name = (element.assignee && element.assignee.name && element.assignee.department_name) ? `${element.assignee.name} - ${element.assignee.department_name.nameEn}` : 'Guest-manager'
              element.assigner = element.assignee ? (element.assignee.name && element.assignee.name !== null ? (element.assignee.name + ' - ' + element.assignee.department_name.nameEn) : 'Guest-manager') : 'No assignee'
              element.statusText = statusText[element.status];
              element.type = element.reason_type ? element.reason_type : '';
              element.reason = element.description ? element.description : ''
              element.dateInApprovedTimeSheet = this.tempCheckDateInApprovedTimeSheet[index]
              return element;
            })
            this.total_items = res.page.total_element;
            this.dataSource = data;
            this.tempCheckDateInApprovedTimeSheet = [];
            this.firstCall = false;
            this.$store.dispatch('user/loadingRequest')
          }, 2000)
        } else if (res.message !== null && res.message !== '') {
          this.$message.error(res.message, 5);
          this.$store.dispatch('user/loadingRequest')
        } else {
          this.$store.dispatch('user/loadingRequest')
        }
      }).catch(error => {
        this.$message.error(error.message, 5);
        this.$store.dispatch('user/loadingRequest')
      })
      sessionStorage.setItem(this.page, JSON.stringify(param))
    },
    exportRequest() {
      const { filters, currentPage, pageSize } = this;
      this.currentPage = filters.page ? filters.page : 1
      this.pageSize = filters.size ? filters.size : 10
      const paramFilter = convertObjectToParams({
        ...filters,
        page: currentPage,
        size: pageSize,
      });
      this.$store.dispatch('user/loadingRequest');
      http.get(`tms/excel/abnormal-case${paramFilter}`, { responseType: 'blob' })
        .then(res => {
          if (res.status === 200 && res.data.type !== 'application/json') {
            downloadFile(res.data, `AbnormalCase_List_${moment(filters.fromDate).format('DD/MM/yyyy')}-${moment(filters.toDate).format('DD/MM/yyyy')}.xlsx`);
            this.$store.dispatch('user/loadingRequest', 5);
            this.$message.success('Request List is exported successfully!');
          } else {
            this.$store.dispatch('user/loadingRequest');
            this.$message.error('No data export', 5);
          }
        })
        .catch(err => {
          this.$store.dispatch('user/loadingRequest');
          this.$message.error(err.response.data.error, 5)
        })
    },
    onChangeStartTime(value) {
      if (moment(moment(value).format('HH:mm:ss'), 'HH:mm:ss').isAfter(moment(moment(this.form.getFieldValue('to')).format('HH:mm:ss'), 'HH:mm:ss'))) {
        this.form.setFieldsValue({ to: value })
      }
    },
    onChangeEndTime(value) {
      if (moment(moment(value).format('HH:mm:ss'), 'HH:mm:ss').isBefore(moment(moment(this.form.getFieldValue('from')).format('HH:mm:ss'), 'HH:mm:ss'))) {
        this.form.setFieldsValue({ from: value })
      }
    },
    handleSubmit(e) {
      clearTimeout(this.timeOut)
      this.timeOut = setTimeout(() => {
        e.preventDefault();
        this.$emit("click");
        this.form.validateFields((err, values) => {
          if (!err) {
            let date = new Date();
            date = moment(date).format('YYYY-MM-DD HH:MM:SS');
            let abnormal_id = this.source.id
            let data = {
              status_type: 'PENDING',
              employee: this.source.employee.id,
              description: values.description,
              created_date: date,
              last_modified: date,
              id: null,
              start_date: moment(this.source.abnormal_date).format('YYYY-MM-DD') + ' ' + moment(values.from).format('HH:mm:ss'),
              end_date: moment(this.source.abnormal_date).format('YYYY-MM-DD') + ' ' + moment(values.to).format('HH:mm:ss'),
              start_time: moment(this.source.abnormal_date).format('YYYY-MM-DD') + ' ' + moment(values.from).format('HH:mm:ss'),
              end_time: moment(this.source.abnormal_date).format('YYYY-MM-DD') + ' ' + moment(values.to).format('HH:mm:ss'),
              leaving_type: values.actual_type,
              reason_type: values.actual_type,
              request_type: this.source.request_type_code,
              mail_cc: values.cc && values.cc.length > 0 ? values.cc : []
            }
            if (this.isEdit) {
              data.id = this.source.request.id
              this.$store.dispatch('abnormalcase/updateAbnormalCaseData', { id: abnormal_id, data: data }).then(res => {
                if (res) {
                  if (parseInt(res.code) === 201) {
                    this.$message.success('Update Explanation Successfully!');
                    this.getAbnormalCaseData();
                    this.visible = false;
                    this.handleUpload(abnormal_id)
                    this.form.resetFields();
                  } else {
                    this.$message.error(res.message, 5);
                  }
  
                }
              }).catch(error => {
                this.$message.error(error.message, 5);
              })
            } else {
              this.$store.dispatch('abnormalcase/createExplanation', { id: abnormal_id, data: data }).then(res => {
                if (res) {
                  if (parseInt(res.code) === 201) {
                    this.$message.success('Create Explanation Successfully!', 5);
                    this.handleUpload(abnormal_id)
                    this.getAbnormalCaseData();
                    this.visible = false;
                    this.form.resetFields();
                  } else {
                    this.$message.error(res.message, 5);
                  }
  
                }
  
              }).catch(error => {
                this.$message.error(error.message, 5);
              })
            }
          }
        });
      }, 300)
    },
    filterOption(input, option) {
      return (
        option.componentOptions.children[0].text.toLowerCase().indexOf(input.toLowerCase()) >= 0
      );
    },
    // onChangeTable(pagination, filters, sorter){
    //     if(sorter.order === 'ascend'){
    //       this.sort.direction = 1
    //     }else if(sorter.order === 'descend'){
    //       this.sort.direction = -1
    //     }
    //     if (sorter.field === 'display_name'){
    //       this.sort.field = 'display_name'
    //     }else {
    //       this.sort.field = sorter.field
    //     }
    //     this.getAbnormalCaseData()
    //   },
    showModal(record) {
      if (record.status === 0 && !record.is_expired && record.employee.user_name === this.userInfo.userName && this.checkExplanable(record.abnormal_date)) {
        this.source = record
        this.formData = {
          actual_type: record.request.leaving_type,
          request: record.request_type,
          staff: `${record.employee.name} - ${record.employee.department_name.nameEn}`,
          type: record.reason_type,
          date: record.ab_date,
          from: record.from_hour ? record.from_hour : undefined,
          to: record.to_hour ? record.to_hour : undefined,
          approver: record.approver_name,
          assigner: record.assigner
        }
        this.visible = true;
        this.form.resetFields();
        this.isEdit = false
      }
    },
    onChangeActual(value) {
      this.formData.actual_type = value
    },
    checkUser(record) {
      if (record.employee.user_name === this.userInfo.userName) {
        return true
      }
      return false
    },
    handleCancel() {
      this.formData = {};
      this.form.resetFields();
      this.visible = false;
    },
    onApplyFilters(filters) {
      this.filters = filters;
      this.currentPage = 1
      this.getAbnormalCaseData()

    },
    onShowSizeChange(page, size) {
      this.currentPage = page;
      this.pageSize = size;
      this.getAbnormalCaseData();
    },
    onChangePage(page, size) {
      this.currentPage = page;
      this.pageSize = size;
      this.getAbnormalCaseData();
    },
    checkDateInApprovedSchedule(date) {
      let url = '?date=' + moment(date).format('YYYYMMDD');
      this.$store.dispatch('application/checkDateInApprovedPeriod', url).then(res => {
        if (res) {
          this.tempCheckDateInApprovedTimeSheet.push(res.data)
        }
      }).catch(() => {
        this.tempCheckDateInApprovedTimeSheet.push(true)
        this.$message.error('Failed to check date in approved Timesheet!', 5);
      });
    },
    updateStatus(payload) {
      this.$store.dispatch('abnormalcase/updateStatusAbnormal', { data: payload.data, id: payload.id }).then(res => {
        if (res) {
          if (parseInt(res.code) === 0) {
            this.$message.success('Update status successfully!', 5);
          } else {
            this.$message.error(res.message, 5)
          }
          this.statusPayload = []
          this.getAbnormalCaseData()
        }
      }).catch(() => {
        this.$message.error('Failed to update status!', 5);
      });
    },
    onClickApproved(record, status) {
      this.statusPayload = {
        data: {
          status_type: statusText[status],
          approve_id: this.userInfo.userId,
          reasons_for_rejection: '',
          request_id: record.request ? record.request.id : this.message.error("No request created", 5)
        }, id: record.id
      }
      this.updateStatus(this.statusPayload)
    },
    checkExplanable(date) {
      if (new Date().getDate() < this.lastTimesheet.forbidden_request_date) {
        if (new Date(this.lastTimesheet.last_timesheet_period_end_date).getTime() >= new Date(date).getTime()) {
          return false
        }
      } else if (new Date().getDate() === this.lastTimesheet.forbidden_request_date) {
        if (new Date().getHours() >= this.lastTimesheet.forbidden_request_time_in_hour) {
          if (new Date(this.lastTimesheet.last_timesheet_period_end_date).getTime() >= new Date(date).getTime()) {
            return false
          }
        }
      }
      return true
    },
    handleOk() {
      if (this.reasonReject && this.reasonReject.trim() !== '') {
        this.statusPayload.data.reasons_for_rejection = this.reasonReject
        this.updateStatus(this.statusPayload)
        this.reasonReject = ''
        this.visibleModalReject = false
      } else {
        this.$message.error('Message required!', 5)
      }
    },
    onClickReject(record, status) {
      this.statusPayload = {
        data: {
          status_type: statusText[status],
          approve_id: this.userInfo.userId,
          request_id: record.request ? record.request.id : this.message.error("No request created", 5)
        }, id: record.id
      }
      this.visibleModalReject = true
    },
    handleCancelRejectModel() {
      this.visibleModalReject = false
    },
    onshowModalUpdate() {
      this.visibleUpdateAbnomal = true
    },
    handleCancelModalUpdate() {
      this.formUpdateJob.resetFields()
      this.visibleUpdateAbnomal = false
    },
    getLastTimesheetInfo() {
      this.$store.dispatch('application/getEndDateOfLastTimsheet').then((res) => {
        if (!res) {
          this.$message.error(res.message, 5);
        }
      }).catch(error => {
        this.$message.error(error.message, 5);
      });
    },
    handleSubmitJob(e) {
      clearTimeout(this.timeOut)
      this.timeOut = setTimeout(() => {
        e.preventDefault()
        this.$emit("click")
        this.formUpdateJob.validateFields((err, values) => {
          if (!err) {
            let employee = []
            let date = moment(values.date).format("YYYY-MM-DD")
            values.staff.forEach(element => {
              employee.push(JSON.parse(element).value)
            })
            let payload = {
              employee,
              date,
            }
            this.$store.dispatch('abnormalcase/updateJobAbnormal', payload).then((res) => {
              if (res) {
                this.$message.success(res.message)
                this.visibleUpdateAbnomal = false
                this.formUpdateJob.resetFields()
              }
            }).catch((err) => {
              this.$message.error(err.message, 5)
            })
          }
        })
      }, 2000);
    },
    beforeUpload(file) {
      this.fileList = [...this.fileList, file];
      return false;
    },
    handleRemove(file) {
      const index = this.fileList.indexOf(file);
      this.deleteFile(this.formData.id, file.name);
      const newFileList = this.fileList.slice();
      newFileList.splice(index, 1);
      this.fileList = newFileList;
    },
    handleUpload(id) {
      const { fileList } = this;
      const formData = new FormData();
      fileList.forEach(file => {
        formData.append('file', file);
      });
      http.post(`/tms/request-management/${this.request_type}/${id}/upload?file`, formData, {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      })
        .then((res) => {
          this.$message.success("Success");
          fileList.splice(0, fileList.length)
          return res.data;
        })
        .catch((err) => {
          throw err
        })
    },
    getAllListFile(id) {
      const { fileList } = this;
      http.get(`/tms/request-management/${this.request_type}/get-all-file-list?id=` + id,)
        .then((res) => {
          if(res.data) {
            let tempData = res.data
            tempData.forEach(record => {
              fileList.push(record)
            })
            if (fileList.length > 0) {
              this.isAvaiableToDownLoad = true
            }
            else {
              this.isAvaiableToDownLoad = false
            }
            return tempData
          }
        })
        .catch((err) => {
          throw err
        })
    },
    deleteFile(id, name) {
      http.get(`/tms/request-management/${this.request_type}/${id}/delete-file?name=` + name)
        .then(res => {
          this.$message.success(res.data)
          if (this.fileList.length > 0) {
            this.isAvaiableToDownLoad = true
          }
          else {
            this.isAvaiableToDownLoad = false
          }
          return res.data
        })
        .catch(err => {
          throw err
        })
    },
    handleDownload() {
      const { fileList } = this
      fileList.forEach(file => {
        http.get(`/tms/request-management/${this.request_type}/${this.download_id}/download?name=` + file.name, { responseType: 'blob' })
          .then(res => {
            downloadFile(res.data, file.name)
            return res.data;
          })
          .catch(err => {
            throw err
          })
      })
    }
  },
};
