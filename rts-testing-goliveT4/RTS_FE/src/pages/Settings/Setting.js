const workTypeColumns = [
  {
    title: 'Registration type on SF4C',
    dataIndex: 'sf4c_registration_type',
    width: '20%',
    type: 'text',
    scopedSlots: { customRender: 'category' },
  },
  {
    title: 'Definition',
    dataIndex: 'tms_definition',
    width: '20%',
    type: 'text',
    scopedSlots: { customRender: 'description' },
  },
  {
    title: 'SF4C notation',
    dataIndex: 'sf4c_notation',
    width: '20%',
    type: 'text',
    scopedSlots: { customRender: 'sf4c_notation' },

  },
  {
    title: 'TMS-Payroll notation',
    dataIndex: 'tms_notation',
    width: '20%',
    type: 'text',
    scopedSlots: { customRender: 'code' },
  },
];
const workPeriodColumns = [
  {
    title: 'From Date - To Date',
    dataIndex: 'from_date',
    type: 'fromDate',
    scopedSlots: { customRender: 'from_date' },
  },
  {
    title: 'Standard working days',
    dataIndex: 'working_days',
    type: 'text',
  },
  {
    title: 'Timesheets',
    dataIndex: 'timesheet',
    type: 'text',
    scopedSlots: { customRender: 'timesheet' },
  },
  {
    title: 'Payroll',
    dataIndex: '',
    type: 'text',
    scopedSlots: { customRender: 'payroll' },
  },
  {
    title: 'Lock',
    dataIndex: 'is_lock',
    type: 'checkbox',
    scopedSlots: { customRender: 'is_lock' },
  },
  {
    title: 'Company',
    dataIndex: 'company_id',
    width: '15%',
    type: 'text',
  },
];

import EditingTable from "./EditingTable/EditingTable.vue";
import { scaleScreen } from "../../components/commons/commons";
export default {
  name: "Settings",
  components: { EditingTable },
  data() {
    return {
      workTypeColumns: workTypeColumns,
      workPeriodColumns: workPeriodColumns,
      workTypeList: [],
      payrollPeriodList: [],
      currentTab: 'work_type',
      loading: false,
      field: "code",
      direction: "DESC",
      total_element_period: 0,
      total_element_workType: 0
    };
  },
  computed: {

  },
  created() {
    this.scaleScreen()
    if (this.currentTab === 'work_type') {
      this.getWorkTypeList();
    } else {
      this.getPayrollPeriodList();
    }
  },
  methods: {
    scaleScreen,
    onSelectTab(key) {
      if (key == "work_type") {
        this.getWorkTypeList();
      }
      if (key == "payroll_period") {
        this.getPayrollPeriodList();
      }
      this.currentTab = key
    },

    ////Function get list of work type
    getWorkTypeList(url) {
      // const { currentPage, pageSize } = this;
      this.$store.dispatch('user/loadingRequest');
      // let url = `?page=${currentPage}&size=${pageSize}`;
      if (url === undefined) {
        url = ""
      }
      this.$store.dispatch('settings/getWorkTypeList', url).then(res => {
        let data = [];
        if (res && res.data) {
          data = res.data.map((item, index) => {
            item.key = index;
            return item;
          });
          this.total_element_workType = res.page;
        } else if (res.message !== null && res.message !== '') {
          this.$message.error(res.message,5);
        }
        this.$set(this.$data, 'workTypeList', data);
        this.$store.dispatch('user/loadingRequest');
      }).catch(error => {
        this.$message.error(error.message,5);
        this.$store.dispatch('user/loadingRequest');
      })
    },

    //Function get list of payroll periods
    getPayrollPeriodList(url) {
      // const { currentPage, pageSize } = this;
      this.$store.dispatch('user/loadingRequest');
      // let url = `?page=${currentPage}&size=${pageSize}`;
      if (url === undefined) {
        url = ""
      }
      this.$store.dispatch('settings/getPayrollPeriodList', url).then(res => {
        let data = [];
        if (res && res.data) {
          data = res.data.map((item, index) => {
            item.key = index;
            return item;
          });
          this.total_element_period = res.page;
        } else if (res.message !== null && res.message !== '') {
          this.$message.error(res.message,5);
        }
        this.$set(this.$data, 'payrollPeriodList', data);
        this.$store.dispatch('user/loadingRequest');
      }).catch(error => {
        this.$message.error(error.message,5);
        this.$store.dispatch('user/loadingRequest');
      })
    },
    cancelWorkTypeSubmit() {
      this.getWorkTypeList()
    },
    myFunction() {
      document.body.style.position = "absolute"
    }
  },
  mounted() {
    this.myFunction()
  },
  updated() {

  }
};