import moment from 'moment';
import { getStatusTimeSheet } from '../../../components/commons/commons'
export default {
  name: "EditingTable",
  props: {
    dataSource: {
      type: Array,
      required: true,
      default: null
    },
    columns: {
      type: Array,
      required: true,
      default: null
    },
    totalElement: {
      type: [Number, Object]
    },
    getPage: {
      type: Function,
    }
  },
  data() {
    return {
      editingKey: '',
      isAddNewItem: false,
      size: 10,
      page: 1,
    };
  },
  methods: {
    getStatusTimeSheet,
    handleChange(value, record, column) {
      let newData = this.dataSource;
      const target = newData.filter(item => record.key === item.key)[0];
      if (target) {
        target[column] = value;
        this.dataSource = newData;
      }
    },
    formatDate(date) {
      return moment(date).format("DD/MM/YYYY");
    },
    checkValidation(data) {
      let isValid = true;
      let listColNotValid = [];
      if (data) {
        Object.keys(data).map(key => {
          if (data[key] === undefined || data[key] === null || data[key] === '') {
            listColNotValid.push(key)
            isValid = false
          }
          return key
        })
      }
      return isValid
    },
    onShowSizeChange(page, pageSize) {
      this.page = page;
      this.size = pageSize;
      let url = `?page=${page}&size=${pageSize}`;
      this.getPage(url)
    },
    onChangePage(page, pageSize) {
      this.page = page;
      this.size = pageSize;
      let url = `?page=${page}&size=${pageSize}`;
      this.getPage(url)
    },
  },
};