<template>
  <div>
    <a-drawer
        placement="right"
        :closable="false"
        :visible="isVisible"
        :after-visible-change="afterVisibleChange"
        @close="onClose"
        width="650px"
        class="drawer"
    >
      <div class="drawer-container">
        <div class="drawer-header drawer-content">
          <h1 v-if="interviewDetail">{{interviewDetail.title}}</h1>
          <div class="btn-group">
            <a-button :class="reactStatus === true ? 'btn-like btn-active': 'btn-like' " @click="onLike">
              <a-icon type="like" />
              {{listReact.like}}
            </a-button>
            <a-button :class="reactStatus===false ? 'btn-like btn-active': 'btn-like' " @click="onDislike">
              <a-icon type="dislike" />
              {{listReact.dislike}}
            </a-button>
          </div>
        </div>
        <div class="drawer-content drawer-body">
          <VueCollapse title="Activities" :isActive="isActive['activity']" class="activity">
            <div slot="content">
              <a-tabs default-active-key="1">
                <a-tab-pane key="1" tab="Interview">
                 <ContentInTabDetailInterview :detail="interviewDetail"/>
                </a-tab-pane>
                <a-tab-pane key="2" tab="Timeline" force-render>
                  <ContentInTabTimeline
                      :listComment="listComment"
                      :listTimeline="listTimeline"
                      :listInterview="listInterview"
                      :candidateId="candidateId"
                  />
                </a-tab-pane>
                <a-tab-pane key="3" tab="Comment">
                  <ContentInTabComments
                      :listComment="listComment"
                      :candidateId="candidateId"
                      @onPostComment="onPostComment"
                      :requestId="requestId"
                      :keyTab="'detailInterview'"
                  />
                </a-tab-pane>
              </a-tabs>
            </div>
          </VueCollapse>
          <VueCollapse title="Profile" :isActive="isActive['profile']" class="activity">
            <div slot="content">
            <iframe  :src="information.linkCv.includes('https://media.cmcglobal.com.vn') ?information.linkCv : 'https://media.cmcglobal.com.vn'+information.linkCv "
                    frameborder="0" height="600" width="100%" class="preview"
                    v-if="information.linkCv && information.linkCv.length>0"/>
              <a-empty v-else></a-empty>
            </div>
          </VueCollapse>
        </div>
      </div>
    </a-drawer>
  </div>
</template>
<script>
import VueCollapse from "../../../components/base/VueCollapse";
import {mapActions, mapState} from "vuex";
import ContentInTabComments from "../../../components/ContentInTab/ContentInTabComments";
import ContentInTabTimeline from "../../../components/ContentInTab/ContentInTabTimeline";
import ContentInTabDetailInterview from "../../../components/ContentInTab/ContenInTabDetailInterview";
const isActive = {
  activity: true,
  profile: true,
};
export default {
  components: {
    VueCollapse,
    ContentInTabComments,
    ContentInTabTimeline,
    ContentInTabDetailInterview
  },
  props: {
    visible: {
      type: Boolean
    },
    candidateId: {
      type: Number
    },
    requestId: {
      type: Number
    }
  },
  watch: {
    visible: {
      immediate: true,
      handler(newVal) {
        this.isVisible = newVal
        this.getAllTimeLine()
      }
    },
    candidateId : {
      immediate : true,
      async handler (newVal) {
        if (newVal) {
          await this.getListReact(newVal);
          await this.setReactStatus();
          await this.getInformation(newVal)
        }
      }
    },
  },
  computed: {
    ...mapState({
      listComment: (state) => state.rtsActivityCandidate.listComment,
      listTimeline: (state) => state.rtsActivityCandidate.listTimeline,
      listInterview: (state) => state.rtsActivityCandidate.listInterview,
      interviewDetail: (state) => state.rtsInterviewTab.interviewDetail,
      listReact: (state) => state.rtsActivityCandidate.listReact,
      information: (state) => state.rtsActivityCandidate.information,
    }),
  },
  data() {
    return {
      isVisible: false,
      isActive,
      like: 20,
      dislike: 10,
      reactStatus: null,

    };
  },
  methods: {
    ...mapActions({
      getListComment: 'rtsActivityCandidate/getListComment',
      getListTimeline: 'rtsActivityCandidate/getListTimeline',
      getListInterview: 'rtsActivityCandidate/getListInterview',
      getInterviewDetail: 'rtsInterviewTab/getInterviewDetail',
      getListReact: 'rtsActivityCandidate/getListReact',
      actionLike: 'rtsActivityCandidate/actionLike',
      getInformation: 'rtsActivityCandidate/getInformation',

    }),
    afterVisibleChange(val) {
    },
    showDrawer() {
      this.isVisible = true;
    },
    onClose() {
      this.$emit("onClose");
    },
    onPostComment (){
      this.getAllTimeLine()
    },
    getAllTimeLine(){
      const payload = {
        page: 1,
        candidateId: this.candidateId,
        requestId: this.requestId
      }

      const params = {
        candidateId: this.candidateId,
        requestId: this.requestId
      }
      this.getListComment(payload)
      this.getListInterview(payload)
      this.getListTimeline(params)
      this.getInterviewDetail(params)
    },
    onLike() {
      if (this.reactStatus !== true) {
        const payload = {
          candidateID: this.candidateId,
          liked: true
        }
        this.actionLike(payload).then(() => {
          this.getListReact(this.candidateId);
        });
        this.reactStatus = true
      }
    },
    onDislike() {
      if (this.reactStatus !== false) {
        const payload = {
          candidateID: this.candidateId,
          liked: false
        }
        this.actionLike(payload).then(() => {
          this.getListReact(this.candidateId);
        });
        this.reactStatus = false
      }
    },
    setReactStatus() {
      new Promise ((resolve) => {
        if (!this.listReact.dislikedUser || !this.listReact.likedUser) return
        this.listReact.dislikedUser.map((item) => {
          if (item === this.userInfo) {
            this.reactStatus = false
          }
        })
        this.listReact.likedUser.map((item) => {
          if (item === this.userInfo) {
            this.reactStatus = true
          }
        })
        resolve()
      });
    }
  },
};
</script>
<style>
.drawer-header{
  display: flex;
  justify-content: space-between;
  align-items: center;
}
.drawer-header h1{
  color: #000000;
  font-size: 32px;
  max-width: 300px;
}
.btn-like{
  margin-left: 5px;
}
.drawer-content{
  margin: 1rem;
  border-bottom: 1px solid #DEE1E7;
}
.activity{
  background-color: #FFFFFF;
  border: none;
}
.ant-collapse-header {
  color: #172B4D;
  font-weight: 600;
  font-size: 16px;
}

</style>