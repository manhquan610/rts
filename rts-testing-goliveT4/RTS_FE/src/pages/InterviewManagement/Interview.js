import FilterInterview from "./FilterInterview/FilterInterview.vue";
import Scheduler from "./SchedulerBlock/Scheduler.vue";
import {mapActions, mapState} from "vuex";
export default {
    name:"Interview",
    components: {
        FilterInterview,
        Scheduler
    },
    data() {
        return {
            componentKey:0
        }
    },
    computed: {
        ...mapState({
            listInterview: state => state.rtsInterviewTab.listAllInterview,
        }),

    },
    methods: {
        ...mapActions({
            getListInterview: 'rtsInterviewTab/getAllInterview',
        }),
        handleSearch(payload) {
            this.getListInterview(payload)
        },
        forceRerender() {
            this.componentKey += 1;
        },
        handleClear(a,b) {
            // if(b) {
            //     this.forceRerender()
            // }
            // b = false
        }
    }

}