export const today = new Date();

export const data = [
  {
    movieId: 1,
    text: "Java interview",
    startDate: new Date('2021-11-24T16:30:00.000Z'),
    endDate: new Date('2021-11-24T1:30:00.000Z'),
    recurrenceRule: 'FREQ=WEEKLY',
  },
];
export const moviesData = [
  {
    id: 1,
    text: "Java interview",
  },
];
// export const moviesData = [{
//   id: 1,
//   text: 'His Girl Friday',
//   image: 'https://js.devexpress.com/Demos/WidgetsGallery/JSDemos/images/movies/HisGirlFriday.jpg',
// }, {
//   id: 2,
//   text: 'Royal Wedding',
//   image: 'https://js.devexpress.com/Demos/WidgetsGallery/JSDemos/images/movies/RoyalWedding.jpg',
// }, {
//   id: 3,
//   text: 'A Star Is Born',
//   image: 'https://js.devexpress.com/Demos/WidgetsGallery/JSDemos/images/movies/AStartIsBorn.jpg',
// }, {
//   id: 4,
//   text: 'The Screaming Skull',
//   image: 'https://js.devexpress.com/Demos/WidgetsGallery/JSDemos/images/movies/ScreamingSkull.jpg',
// }, {
//   id: 5,
//   text: 'It\'s a Wonderful Life',
//   image: 'https://js.devexpress.com/Demos/WidgetsGallery/JSDemos/images/movies/ItsAWonderfulLife.jpg',
// }];
