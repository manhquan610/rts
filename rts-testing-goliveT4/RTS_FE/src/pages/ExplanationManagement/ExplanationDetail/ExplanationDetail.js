import { mapState } from 'vuex';
import moment from "moment";
import { Role } from '../../../components/commons/commonConstant'
export default {
  name: "ExplanationDetail",
  props: {
    explanationDetail: {
      type: Object
    },
    refreshPage: {
      type: Function
    },
    onClose: {
      type: Boolean
    }
  },
  data() {
    return {
      visible: false,
      status: 0,
      actual: 1,
      resetFields: {},
      Role,
      formItemLayout: {
        labelCol: { span: 8 },
        wrapperCol: { span: 16 },
      },
      radioStyle: {
        display: 'block',
        height: '30px',
        lineHeight: '30px',
      },
      dataOptionFilterChangeDelegator: [],
      actualList: []
    };
  },
  watch: {
    explanationDetail: function () {
      return {
        explanationDetail: this.setFields(),
      }
    },
    onClose(newData) {
      if (newData === false) {
        this.setFields();
        this.visible = false;
      }
    }
  },
  beforeCreate() {
    this.form = this.$form.createForm(this, { name: 'explanation-detail' });
  },
  computed: {
    ...mapState({
      reasonTypeList: state => state.application.reasonTypeList,
      absentTypeList: state => state.application.absentTypesList,
      roles: state => state.user.userInfo.roles ? state.user.userInfo.roles : "",
      employeeList: state => state.application.employeeList,
      reasonRequestList: state => state.application.reasonRequestList
    }),

  },
  mounted() {
    this.setFields()
  },
  methods: {
    setFields() {
      const { explanationDetail } = this;
      this.dataOptionFilterChangeDelegator = [explanationDetail.assignee];
      this.reasonRequestList.forEach(element => {
        if (element.sf4c_registration_type === explanationDetail.actual) {
          explanationDetail.actual = element.sf4c_notation
        }
      });
      let initialValues = {
        title: explanationDetail.title,
        employee: explanationDetail.employee ? explanationDetail.employee.name + ' - ' + explanationDetail.department : null,
        department: explanationDetail.employee ? explanationDetail.employee.department_name.name_en : null,
        explanation_date: explanationDetail.abnormalDate,
        explanation_reason: explanationDetail.reason,
        assignee: explanationDetail.assignee.user_name,
        description: explanationDetail.description,
        created_date: explanationDetail.createdDate,
        status: explanationDetail.statusText,
        approved_by: explanationDetail.approver ? explanationDetail.approver.name : null,
        actual: explanationDetail.actual
      }
      this.form.setFieldsValue(
        initialValues
      );
    },
    handleSubmit(e) {
      e.preventDefault();
      this.form.validateFields((err, value) => {
        if (!err) {
          let assigneeId = '';
          for (let item of this.dataOptionFilterChangeDelegator) {
            if (item.user_name === value.assignee) {
              assigneeId = item.id;
            }
          }
          const { explanationDetail } = this;
          this.actualList.forEach(element => {
            if (element.name === this.explanationDetail.actual) {
              this.actual = element.type
            }
          })
          let date = new Date();
          date = moment(date).format('YYYY-MM-DD HH:MM:SS');
          let data = {
            abnormal_date: explanationDetail.abnormal_date,
            actual: value.actual,
            assignee: assigneeId,
            created_date: explanationDetail.created_date,
            description: value.description,
            employee: explanationDetail.employee.id,
            id: explanationDetail.id,
            modify_date: date,
            reason_detail: explanationDetail.reason_detail,
            status: this.status === 0 ? explanationDetail.status : this.status,
            title: value.title
          }
          this.reasonTypeList.forEach(element => {
            if (this.explanationDetail.reason_detail === element.name) {
              data.reason_detail = element.type
            }
          });
          this.absentTypeList.forEach(element => {
            if (this.explanationDetail.absent_type === element.name) {
              data.absent_type = element.type
            }
          })
          this.visible = false;
          this.$store.dispatch('explanation/updateExplanationData', data).then(res => {
            if (res) {
              this.$message.success('Update Status Successfully!',5);
              this.refreshPage();
            }
            this.visible = false;
          }).catch(error => {
            this.$message.error(error)
            this.$message.error('Failed to edit!',5);
          });
        }
      })

    },
    onChangeTitle(e) {
      this.visible = true
      const { explanationDetail } = this;
      let cloneExplanationDetail = {
        ...explanationDetail,
        ['title']: e.target.value
      }
      this.explanationDetail = cloneExplanationDetail;
    },
    onChangeDescription() {
      this.visible = true
    },
    onChange() {
      this.visible = true
    },
    onChangeRadio() {
      this.visible = true
    },
    onClickCancel(e) {
      this.status = 5;
      this.handleSubmit(e)
    },
    onClickReject(e) {
      this.status = 4;
      this.handleSubmit(e)
    },
    onClickApprove(e) {
      this.status = 3;
      this.handleSubmit(e)
    },
    onHandleChange(value) {
      this.visible = true
      if (this.explanationDetail.assignee.user_name !== value) {
        this.status = 6;
      } else {
        this.status = this.explanationDetail.status
      }
    },
    onSearchDelegatorToChange(value) {
      if (value && value !== '' && value.length >= 3) {
        let decodeUriVal = decodeURI(value);
        let url = `?employee=${decodeUriVal}`;
        this.chooseDelegatorToChange(url);
      }
    },

    chooseDelegatorToChange(url) {
      this.fetching = true;
      this.$store.dispatch('delegation/chooseDelegatorToChange', url).then(res => {
        if (res && res.data) {
          this.dataOptionFilterChangeDelegator = res.data;
          this.fetching = false;
        }
      }).catch(error => {
        this.$message.error(error)
        this.fetching = false;
      })
    },
  },
};