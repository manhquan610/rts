const columns = [
  {
    title: "Title",
    key: "title",
    dataIndex: "title",
    sorter: (a, b) => a.title.localeCompare(b.title),
    width: "15%"
  },
  {
    title: "Created By",
    dataIndex: "created_by",
    key: "created_by",
    scopedSlots: { customRender: "created_by" },
    width: "15%"
  },
  {
    title: "Department",
    dataIndex: "department",
    key: "department",
    width: "12%"
  },
  {
    title: "Approved By",
    dataIndex: "approved_by",
    key: "approved_by",
    scopedSlots: { customRender: "approved_by" },
    width: "15%"
  },
  {
    title: "Reason Type",
    dataIndex: "reason",
    key: "reason",
    width: "15%"
  },
  {
    title: "Created Date",
    dataIndex: "createdDate",
    key: "createdDate",
    width: "10%",
  },
  {
    title: "Explanation Date",
    dataIndex: "abnormalDate",
    key: "abnormalDate",
    width: "10%",
  },

  {
    title: "Status Type",
    dataIndex: "status",
    key: "status",
    scopedSlots: { customRender: "status" },
    width: "7%",
  },
];

import ExplanationDetail from "../ExplanationManagement/ExplanationDetail/ExplanationDetail.vue";
import FilterLayout from "./../../components/layouts/FilterLayout";
import { mapState } from "vuex";
import { convertObjectToParams, formatDate, scaleScreen  } from "../../components/commons/commons"
import { statusColor, statusText } from '../../components/commons/commonConstant';
import { Role } from "../../components/commons/commonConstant";
import moment from 'moment'
export default {
  name: "ExplanationManagement",
  props: {},
  components: { ExplanationDetail, FilterLayout },
  data() {
    return {
      columns,
      page: 'Explanation management',
      loading: false,
      filterKey: 2,
      visible: false,
      currentPage: 1,
      pageSize: 10,
      dataSource: [],
      total_items: 0,
      direction: -1,
      field: 'title',
      recordDetail: {},
      filters: {
        fromDate: moment().startOf('month'),
        toDate: moment().endOf('month'),
      },
      statusColor: statusColor,
      Role,
    };
  },

  computed: {
    ...mapState({
      explanationData: (state) => state.explanation.explanationData,
      pagination: (state) => state.explanation.explanationData.pageable,
      roles: state => state.user.userInfo.roles ? state.user.userInfo.roles : "",
    }),
  },
  watch: {
    visible: function () {
      this.myFunction();
    }
  },
  created() {
    if (sessionStorage.getItem(this.page)){
    this.filters = {  ...JSON.parse(sessionStorage.getItem(this.page)) }
    }else{
      this.filters = { ...this.filters, ...JSON.parse(sessionStorage.getItem(this.page)) }
    }
    this.scaleScreen()
    this.getExplanationList();
  },

  mounted() {
  },

  updated() {
    this.handleSubmit();
  },

  methods: {
    scaleScreen,
    getExplanationList() {
      const { filters } = this;
      const paramFilter = convertObjectToParams(filters);
      this.$store.dispatch('user/loadingRequest');
      this.$store.dispatch("explanation/getExplanationDrawerData", paramFilter).then((res) => {
        let data = []
        if (res && res.data) {
          data = res.data.map((item, index) => {
            item.key = index;
            item.department = item.employee && item.employee.department_name ? item.employee.department_name.name_en : '';
            item.abnormalDate = item.abnormal_date ? formatDate(item.abnormal_date) : "";
            item.createdDate = item.created_date ? formatDate(item.created_date) : "";
            item.statusText = statusText[item.status];
            item.reason = item.reason_detail
            //item.reason  = reasons[item.reason_detail]
            return item;
          });

          this.total_items = res.page.total_element;
          this.dataSource = data;
          if (data.length > 0) {
            this.recordDetail = data[0]
          }
          else {
            this.recordDetail = null
          }
          this.$store.dispatch('user/loadingRequest')
        } else if (res.message !== null && res.message !== '') {
          this.$message.error(res.message,5);
          this.$store.dispatch('user/loadingRequest')
        }
        this.visible = false;
      }).catch(error => {
        this.$message.error(error.message,5);
        this.$store.dispatch('user/loadingRequest')
      });
    },

    updateStatus() {
      let data = {
        abnormalId: this.recordDetail.id,
        status: this.recordDetail.status
      }
      this.$store.dispatch('explanation/updateStatus', data).then(res => {
        if (res) {
          this.$message.success('Update Status Successfully!',5);
          this.getExplanationList()
        } else {
          this.$message.error('Failed To Update Status!',5);
        }
      }
      )

    },
    customRow(record) {
      return {
        on: {
          click: () => {
            this.showDrawer();
            this.recordDetail = record;
          },
        },
      };
    },
    onApplyFilters(filters) {
      this.filters = {
        ...filters,
        page: this.currentPage,
        size: this.pageSize,
        fieldToSort: this.field,
        direction: this.direction,
      };
      this.currentPage = 1
      this.getExplanationList();
      sessionStorage.setItem(this.page, JSON.stringify(this.filters))
    },

    onShowSizeChange(page, pageSize) {
      this.currentPage = page;
      this.pageSize = pageSize;
      this.getExplanationList();
    },
    showDrawer() {
      this.visible = true;
    },
    afterVisibleChange() {
    },
    onClose() {
      this.visible = false;
    },
    onChangePage(page, size) {
      this.currentPage = page;
      this.pageSize = size;
      this.getExplanationList();
    },
    myFunction() {
      document.body.style.position = "fixed"
    }
  },
};