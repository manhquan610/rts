import moment from "moment";
import logs from "../../store/modules/logs";
import axios from "axios";

export const formatDate = (date) => {
  if (date) return moment(date).format("DD/MM/YYYY");
  else return "";
};
export const formatTime = (date) => {
  if (date) return moment(date).format("HH:mm");
  return "";
};
export const convertObjectToParams = (filters, key) => {
  let params = "";
  if (filters)
    Object.keys(filters).map((it) => {
      let val = filters[it];
      val = val === 0 ? "0" : val;
      if ((it === "fromDate" || it === "toDate") && filters[it]) {
        if (key === "checkInOut-TMS-report") {
          val = moment(filters[it]).format("YYYYMMDD");
        } else {
          val = moment(filters[it]).format("DD-MM-YYYY");
        }
      }
      if (Array.isArray(val)) {
        if (val.length > 0) {
          if (params.indexOf("?") > -1) {
            params += `&${it}=${val}`;
          } else {
            params += `?${it}=${val}`;
          }
        }
      } else if (val) {
        if (params.indexOf("?") > -1) {
          params += `&${it}=${val}`;
        } else {
          params += `?${it}=${val}`;
        }
      }
      return params;
    });
  return params;
};

export const initFilters = (filters) => {
  if (moment().date() >= 26) {
    if (moment().add(1, "month").endOf("month").date() === 31) {
      filters.fromDate = moment().startOf("month").add(25, "days");
      filters.toDate = moment().add(1, "month").endOf("month").subtract(6, "days");
    } else if (moment().add(1, "month").endOf("month").date() === 30) {
      filters.fromDate = moment().startOf("month").add(25, "days");
      filters.toDate = moment().add(1, "month").endOf("month").subtract(5, "days");
    } else if (moment().add(1, "month").endOf("month").date() === 28) {
      filters.fromDate = moment().startOf("month").add(25, "days");
      filters.toDate = moment().add(1, "month").endOf("month").subtract(3, "days");
    } else if (moment().add(1, "month").endOf("month").date() === 29) {
      filters.fromDate = moment().startOf("month").add(25, "days");
      filters.toDate = moment().add(1, "month").endOf("month").subtract(4, "days");
    }
  } else {
    if (moment().subtract(1, "month").endOf("month").date() === 31) {
      filters.fromDate = moment().subtract(1, "month").endOf("month").subtract(5, "days");
      filters.toDate = moment().startOf("month").add(24, "days");
    } else if (moment().subtract(1, "month").endOf("month").date() === 30) {
      filters.fromDate = moment().subtract(1, "month").endOf("month").subtract(4, "days");
      filters.toDate = moment().startOf("month").add(24, "days");
    } else if (moment().subtract(1, "month").endOf("month").date() === 29) {
      filters.fromDate = moment().subtract(1, "month").endOf("month").subtract(3, "days");
      filters.toDate = moment().startOf("month").add(24, "days");
    } else if (moment().subtract(1, "month").endOf("month").date() === 28) {
      filters.fromDate = moment().subtract(1, "month").endOf("month").subtract(2, "days");
      filters.toDate = moment().startOf("month").add(24, "days");
    }
  }
  return filters;
};

export const getStatusTimeSheet = (status) => {
  switch (status) {
    case 0:
      return { text: "Draft", color: "" };
    case 2:
      return { text: "Pending update", color: "blue" };
    case 3:
      return { text: "Pending approval", color: "orange" };
    case 4:
      return { text: "Approved", color: "green" };
    default:
      return { text: "", color: "white" };
  }
};

export const downloadFile = (data, fileName) => {
  var fileURL = URL.createObjectURL(
    new Blob([data], {
      type: "application/vnd.ms-excel",
    }),
  );
  var fileLink = document.createElement("a");
  fileLink.href = fileURL;
  fileLink.setAttribute("download", fileName);
  document.body.appendChild(fileLink);
  fileLink.click();
};

export const convertAvatarName = (fullName) => {
  if (fullName) {
    let words = fullName.split(" - ");
    words = words[0].split(" ");
    return words[0].charAt(0) + words[words.length - 1].charAt(0);
  }
  return "";
};

export const getDurationDate = (startDate, endDate) => {
  let duration = undefined;
  if (startDate && startDate !== null && startDate !== "null" && endDate && endDate !== null && endDate !== "") {
    duration = moment.duration(endDate.diff(startDate));
  }
  return duration;
};

export const getUnique = (arr, comp) => {
  if (arr) {
    const unique = arr
      .map((e) => e[comp])
      // store the keys of the unique objects
      .map((e, i, final) => final.indexOf(e) === i && i)
      // eliminate the dead keys & store unique objects
      .filter((e) => arr[e])
      .map((e) => arr[e]);
    return unique;
  }
};

export const scaleScreen = () => {
  let content = `width=device-width,initial-scale=0.45`;
  if (document.body.clientWidth <= 768) {
    document.querySelector('meta[name="viewport"]').setAttribute("content", content);
  }
};

export const camalize = (str) => {
  return str.toLowerCase().replace(/[^a-zA-Z0-9]+(.)/g, (m, chr) => chr.toUpperCase());
};
export const capitalizeFirstLetter = (str) => {
  if (!str) return "";
  str = str.toLowerCase();
  return str.charAt(0).toUpperCase() + str.slice(1);
};
export const compareString = (a, b) => {
  if (!a) return -1;
  if (!b) return 1;
  return a.localeCompare(b);
};
export const compareDate = (dateTimeA, dateTimeB) => {
  var momentA = moment(dateTimeA, "DD/MM/YYYY");
  var momentB = moment(dateTimeB, "DD/MM/YYYY");
  if (momentA > momentB) return 1;
  else if (momentA < momentB) return -1;
  else return 0;
};

// format date must be 'DD/MM/YYYY'
export const checkExpired = (dateList) => {
  if (!dateList) return false;
  let currentDate = moment();
  return dateList.every((date) => {
    let deadline = moment(moment(date, "DD/MM/YYYY").subtract("days", -1).format("YYYY-MM-DD"));
    return deadline >= currentDate;
  });
};
export const convertMillisecondsToDate = (milliseconds) => {
  if (!milliseconds) {
    return null;
  }
  return moment(milliseconds).format("DD/MM/YYYY");
};
export const convertMillisecondsToHour = (milliseconds) => {
  if (!milliseconds) {
    return null;
  }
  return moment(milliseconds);
};
export const convertUnixToDate = (unix_timestamp) => {
  if (!unix_timestamp) {
    return null;
  }
  // Create a new JavaScript Date object based on the timestamp
  // multiplied by 1000 so that the argument is in milliseconds, not seconds.
  var date = new Date(unix_timestamp * 1000);
  // Hours part from the timestamp
  var hours = date.getHours();
  // Minutes part from the timestamp
  var minutes = "0" + date.getMinutes();
  // Seconds part from the timestamp
  var seconds = "0" + date.getSeconds();

  // Will display time in 10:30:23 format
  return hours + ":" + minutes.substr(-2) + ":" + seconds.substr(-2);
};
export const isNumeric = (str) => {
  return (
    !isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
    !isNaN(parseFloat(str))
  ); // ...and ensure strings of whitespace fail
};

export const saveStage = (key, value, isActive = false) => {
  value.isActive = isActive;
  localStorage.setItem(key, JSON.stringify(value));
};
export const activeStage = (key) => {
  let res = JSON.parse(localStorage.getItem(key));
  res.isActive = true;
  localStorage.setItem(key, JSON.stringify(res));
};
export const getStage = (key) => {
  let res = JSON.parse(localStorage.getItem(key));
  return res && res.isActive ? res : null;
};
export const clearState = (key) => {
  localStorage.removeItem(key);
};

export const getLastWord = (str) => {
  let n = str.split(" ");
  return n[n.length - 1];
};

let arr = [];
arr.filter;
export const showDateTimeFromMilliSecond = (date) => {
  if (date) return moment(date).format("DD/MM/YYYY - HH:mm");
  return "";
};

export const PAGE_OPTIONS_CANDIDATE = {
  offset: 1,
  limit: 5,
  searchString: "",
};

export const PAGE_OPTIONS_FEATUREDJOB = {
  offset: 1,
  limit: 9,
};

export const PAGE_OPTIONS_POPULARJOB = {
  offset: 1,
  limit: 8,
};

export const PAGE_OPTIONS_MOREJOB_DETAIL = {
  offset: 1,
  limit: 5,
};

export const PAGE_OPTIONS_MANAGEMENT_CV = {
  offset: 1,
  limit: 10,
  searchString: "",
};

export const TIME_DELAY_CANDIDATE = 200;

export const findObjectById = (searchId, searchProp, arr) => {
  if (!arr) return null;
  return arr?.find((item) => {
    if (item[searchProp] === searchId) {
      return item;
    }
  });
};

export const convertIdArrayToObjectArray = (ids, searchProp, arr) => {
  if (!ids) {
    return;
  }
  let res = [];
  ids.forEach((id) => {
    let data = findObjectById(Number(id), searchProp, arr);
    if (data !== undefined) {
      res.push(data);
    }
  });
  return res;
};

export const formatNumberToVND = (data) => {
  try {
    let salary = parseInt(data);
    if (!isNaN(salary) && typeof salary == "number") {
      salary = new Intl.NumberFormat("de-DE", { minimumFractionDigits: 0, style: "currency", currency: "VND" }).format(
        salary,
      );
      salary = String(salary).replace("₫", "");
      return salary;
    } else {
      return data;
    }
  } catch (error) {
    return data;
  }
};
export const formatNewStatus = (status) => {
  if (status === "New") {
    return "Opened";
  } else if (status === "Pending") {
    return "Submitted";
  } else if (status === "Shared") {
    return "Posted";
  } else return status;
};

export const downloadFileExcel = (url, fileName) => {
  let today = new Date();
  let dateToday = today.getFullYear() + "" + (today.getMonth() + 1) + "" + today.getDate();
  axios.get(url, { responseType: "blob" }).then((res) => {
    if (res.data) {
      const url = URL.createObjectURL(
        new Blob([res.data], {
          type: "application/vnd.ms-excel",
        }),
      );
      const link = document.createElement("a");
      const filename = `${fileName}-${dateToday}.xlsx`;
      link.setAttribute("download", filename);
      link.href = url;
      document.body.appendChild(link);
      link.click();
    }
  });
};
export const validation = {
  required: "required",
  checkMaxLength: "checkMaxLength",
  validFullName: "validFullName",
  validEmail: "validEmail",
  validPhone: "validPhone",
};
