import { validation } from './commons';
const checkValidation = (validationType , validationData) => {
    if (validationType === validation.required) {
      return checkRequired(validationData)
    } else if (validationType === validation.validFullName) {
        return checkFullName(validationData)
    } else if (validationType === validation.validEmail) {
        return checkEmail(validationData)
    } else if (validationType === validation.validPhone) {
        return checkPhone(validationData)
    }
}

export const findAndCheck = (rules,obj) => {
    let res = []
    
   for(const objKey in obj) {
    let afterValidation = []

    if(!rules[objKey]) continue
    rules[objKey].forEach(validation => {
        if(!checkValidation(validation.type, obj[objKey]) ) {
            afterValidation.push({
                type:validation.type,
                message:validation.message
            })
            res.push({
                key:objKey,
                afterValidation
            })
        }
    });
   }
   return res;
}


const checkRequired = (value) => {
    return value !== '' && value !== null && value !== undefined
}

const checkFullName = (value) => {
    const re = /[^a-z0-9A-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]/u
    return re.test(value)
}
const checkEmail = (value) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(value);
}

const checkPhone = (value) => {
    var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    return re.test(value);
}

// const checkMaxLength = (value) => {
//     return value.length < 50
// }