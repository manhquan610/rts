//// constant json menu
export const menus = [
  {
    id: 1,
    label: "TimeSheet",
    state: "/timesheet",
    component: "Timesheet",
    roles: ["admin"],
    parentId: 0,
    activeKey: "timesheet",
    children: [
      {
        id: 7,
        label: "TimeSheet",
        state: "/timesheet",
        component: "Timesheet",
        roles: ["admin"],
        parentId: 1,
        activeKey: "timesheet",
      },
      {
        id: 8,
        label: "Request Management",
        state: "/timesheet/request-management",
        component: "RequestManage",
        roles: ["admin"],
        parentId: 1,
        activeKey: "request-management",
      },
      {
        id: 9,
        label: "Onsite Management",
        state: "/timesheet/onsite-management",
        component: "OnsiteManagement",
        roles: ["admin"],
        parentId: 1,
        activeKey: "onsite-management",
      },
      {
        id: 10,
        label: "Explanation Management",
        state: "/timesheet/explanation-management",
        component: "ExplainManage",
        roles: ["admin"],
        parentId: 1,
        activeKey: "explanation-management",
      },
      {
        id: 11,
        label: "Delegation Management",
        state: "/timesheet/delegation-management",
        component: "DelegationManage",
        roles: ["admin"],
        parentId: 1,
        activeKey: "delegation-management",
      },
      {
        id: 12,
        label: "Abnormal Case",
        state: "/timesheet/abnormal-case",
        component: "AbnormalCase",
        roles: ["admin"],
        parentId: 1,
        activeKey: "abnormal-case",
      },
      {
        id: 13,
        label: "Check In-Out Time",
        state: "/timesheet/checkin-checkout",
        component: "CheckinOut",
        roles: ["admin"],
        parentId: 1,
        activeKey: "checkin-checkout",
      },
    ],
  },
  {
    id: 2,
    label: "Payroll",
    state: "/payroll",
    component: "Payroll",
    roles: ["admin"],
    parentId: 0,
    activeKey: "payroll",
    children: [
      {
        id: 14,
        label: "Payroll Report Detail",
        state: "/payroll/payroll-report-detail",
        component: "PayrollReportDetail",
        roles: ["admin"],
        parentId: 2,
        activeKey: "payroll-detail",
      },
      {
        id: 15,
        label: "Check In - Check Out Report",
        state: "/report/checkInOut-TMS-report",
        component: "PayrollReportCheckInOut",
        roles: ["admin"],
        parentId: 2,
        activeKey: "payroll-inout",
      },
    ],
  },
  {
    id: 3,
    label: "Report",
    state: "/report",
    component: "Report",
    roles: ["admin"],
    children: [],
    parentId: 0,
    activeKey: "report",
  },
  {
    id: 5,
    label: "Setting",
    state: "/settings",
    component: "Setting",
    roles: ["admin"],
    children: [],
    parentId: 0,
    activeKey: "settings",
  },
  {
    id: 6,
    label: "Logs",
    state: "/logs",
    component: "Logs",
    roles: ["admin"],
    children: [],
    parentId: 0,
    activeKey: "logs",
  },
  {
    id: 7,
    label: "RTS",
    state: "/rts/requests",
    component: "Logs",
    roles: ["admin"],
    children: [],
    parentId: 0,
    activeKey: "logs",
  },
];

//// constant json filters
export const filtersJson = {
  timesheet: {
    page: "Timesheet",
    key: "/timesheet",
    controls: [
      {
        label: "Choose a Payroll Period",
        placeholder: "Choose a Payroll Period",
        name: "payroll_period",
        type: "select",
      },
      {
        label: "Search",
        placeholder: "Search by staff name, ID",
        name: "employee",
        type: "auto-complete",
      },
      {
        label: "Location",
        placeholder: "Location",
        name: "location",
        type: "multiple-select",
      },
      {
        label: "Department",
        placeholder: "Department",
        name: "department",
        type: "multiple-select",
      },
      {
        label: "Role",
        placeholder: "Role",
        name: "role",
        type: "multiple-select",
        isToggle: true,
      },
      {
        label: "Search by request",
        placeholder: "Search by reason",
        name: "reason_type",
        type: "multiple-select",
        isToggle: true,
      },
      {
        label: "Abnormal Value",
        placeholder: "Abnormal Value",
        name: "abnormal_case",
        type: "checkbox",
      },
    ],
  },
  timesheet_user: {
    page: "TimesheetUser",
    key: "/timesheet",
    controls: [
      {
        label: "Choose a Payroll Period",
        placeholder: "Choose a Payroll Period",
        name: "payroll_period",
        type: "select",
      },
    ],
  },
  request_management: {
    page: "Request Management",
    key: "/timesheet/request-management",
    controls: [
      {
        label: "Period",
        placeholder: "Period",
        name: "period",
        type: "dateRange",
      },
      {
        label: "Search",
        placeholder: "Search by staff name, ID",
        name: "employee",
        type: "auto-complete",
      },
      {
        label: "Search by status",
        placeholder: "Search by status",
        name: "status",
        type: "multiple-select",
      },
      {
        label: "Department",
        placeholder: "Department",
        name: "department",
        type: "multiple-select",
      },
      {
        label: "Search",
        placeholder: "Search by approver",
        name: "approver",
        type: "auto-complete",
      },
      {
        label: "Request reason",
        placeholder: "Search by reason",
        key: "request",
        name: "reason_type",
        type: "multiple-select",
      },
    ],
  },
  request_management_by_staff: {
    page: "Request Management",
    key: "/timesheet/request-management",
    controls: [
      {
        label: "Period",
        placeholder: "Period",
        name: "period",
        type: "dateRange",
      },
      {
        label: "Search by status",
        placeholder: "Search by status",
        name: "status",
        type: "multiple-select",
      },
      {
        label: "Request reason",
        placeholder: "Search by reason",
        key: "request",
        name: "reason_type",
        type: "multiple-select",
      },
    ],
  },
  explanation_management: {
    page: "Explanation management",
    key: "/timesheet/explanation-management",
    controls: [
      {
        label: "Period",
        placeholder: "Period",
        name: "period",
        type: "dateRange",
      },
      {
        label: "Search",
        placeholder: "Search by staff name, ID",
        name: "employee",
        type: "auto-complete",
      },
      {
        label: "Status",
        placeholder: "Status",
        name: "status",
        type: "multiple-select",
      },
      {
        label: "Department",
        placeholder: "Department",
        name: "department",
        type: "multiple-select",
      },
      {
        label: "Reason Type",
        placeholder: "Reason Type",
        name: "reason_type",
        type: "multiple-select",
      },
    ],
  },
  explanation_management_by_staff: {
    page: "Explanation management",
    key: "/timesheet/explanation-management",
    controls: [
      {
        label: "Period",
        placeholder: "Period",
        name: "period",
        type: "dateRange",
      },
      {
        label: "Status",
        placeholder: "Status",
        name: "status",
        type: "multiple-select",
      },
      {
        label: "Reason Type",
        placeholder: "Reason Type",
        name: "reason_type",
        type: "multiple-select",
      },
    ],
  },
  onsite_management: {
    page: "Onsite Management",
    key: "/timesheet/onsite-management",
    controls: [
      {
        label: "Period",
        placeholder: "Period",
        name: "period",
        type: "dateRange",
      },
      {
        label: "Search",
        placeholder: "Search by staff name, ID",
        name: "employee",
        type: "auto-complete",
      },
      {
        label: "Status",
        placeholder: "Status",
        name: "status-onsite",
        type: "multiple-select",
      },
      {
        label: "Department",
        placeholder: "Department",
        name: "department",
        type: "multiple-select",
      },
      {
        label: "Search",
        placeholder: "Search by approver",
        name: "approver",
        type: "auto-complete",
      },
      {
        label: "Adjustment",
        placeholder: "Adjustment",
        name: "adjustment",
        type: "select",
      },
      {
        label: "Project",
        placeholder: "Search by Project",
        name: "projects",
        type: "multiple-select",
      },
    ],
  },
  overtime_management_staff: {
    page: "Overtime-Management",
    key: "/timesheet/overtime-management",
    controls: [
      {
        label: "Period",
        placeholder: "Period",
        name: "period",
        type: "dateRange",
      },
      {
        label: "Status",
        placeholder: "Status",
        name: "status",
        type: "multiple-select",
      },
      {
        label: "Department",
        placeholder: "Department",
        name: "department",
        type: "multiple-select",
      },
      {
        label: "OT type",
        placeholder: "Overtime type",
        name: "otType",
        type: "multiple-select",
      },
      {
        label: "Project",
        placeholder: "Search by Project",
        name: "projects",
        type: "multiple-select",
      },
    ],
  },
  overtime_management: {
    page: "Overtime-Management",
    key: "/timesheet/overtime-management",
    controls: [
      {
        label: "Period",
        placeholder: "Period",
        name: "period",
        type: "dateRange",
      },
      {
        label: "Search",
        placeholder: "Search by staff name, ID",
        name: "employee",
        type: "auto-complete",
      },
      {
        label: "Status",
        placeholder: "Status",
        name: "status",
        type: "multiple-select",
      },
      {
        label: "Department",
        placeholder: "Department",
        name: "department",
        type: "multiple-select",
      },
      {
        label: "Search",
        placeholder: "Search by approver",
        name: "assignee",
        type: "auto-complete",
      },
      {
        label: "OT type",
        placeholder: "Overtime type",
        name: "otType",
        type: "multiple-select",
      },
      {
        label: "Project",
        placeholder: "Search by Project",
        name: "projects",
        type: "multiple-select",
      },
    ],
  },
  abnormal_case: {
    page: "Abnormal Case",
    key: "/timesheet/abnormal-case",
    controls: [
      {
        label: "Period",
        placeholder: "Period",
        name: "period",
        type: "dateRange",
      },
      {
        label: "Search",
        placeholder: "Search by staff name, ID",
        name: "employee",
        type: "auto-complete",
      },
      {
        label: "Status",
        placeholder: "Search by Status",
        name: "status",
        type: "multiple-select",
      },
      {
        label: "Department",
        placeholder: "Search by Department",
        name: "department",
        type: "multiple-select",
      },
      {
        label: "Search",
        placeholder: "Search by approver",
        name: "assignee",
        type: "auto-complete",
      },
      {
        label: "Reason type",
        placeholder: "Reason type",
        name: "request_type",
        type: "multiple-select",
      },
      {
        label: "Actual",
        placeholder: "Search by actual",
        name: "actual",
        type: "multiple-select",
      },
      // {
      // 	label: 'Role',
      // 	placeholder: 'Search by Role',
      // 	name: 'role',
      // 	type: 'multiple-select'
      // },
      // {
      // 	label: 'Absent type',
      // 	placeholder: 'Absent type',
      // 	name: 'absent_type',
      // 	type: 'multiple-select'
      // },
    ],
  },
  abnormal_case_staff: {
    page: "Abnormal Case",
    key: "/timesheet/abnormal-case",
    controls: [
      {
        label: "Period",
        placeholder: "Period",
        name: "period",
        type: "dateRange",
      },
      {
        label: "Status",
        placeholder: "Search by Status",
        name: "status",
        type: "multiple-select",
      },
      {
        label: "Reason type",
        placeholder: "Reason type",
        name: "request_type",
        type: "multiple-select",
      },
      {
        label: "Actual",
        placeholder: "Search by actual",
        name: "actual",
        type: "multiple-select",
      },
      // {
      // 	label: 'Absent type',
      // 	placeholder: 'Absent type',
      // 	name: 'absent_type',
      // 	type: 'multiple-select'
      // },
    ],
  },
  delegation_management: {
    delegation_list: {
      page: "Delegation Management",
      key: "/timesheet/delegation-management",
      tab: "delegation_list",
      controls: [
        {
          label: "Period",
          placeholder: "Period",
          name: "period",
          type: "dateRange",
        },
        {
          label: "Member",
          placeholder: "Search by Members ",
          name: "member",
          type: "auto-complete",
        },
        {
          label: "Search",
          placeholder: "Search by Delegator",
          name: "employee",
          type: "auto-complete",
        },
        {
          label: "Department",
          placeholder: "Department",
          name: "department",
          type: "multiple-select",
        },
      ],
    },
    // delegation_expalination_request: {
    // 	page: 'Delegation Management',
    // 	key: '/timesheet/delegation-management',
    // 	tab: 'delegation_expalination_request',
    // 	controls: [
    // 		{
    // 			label: 'Period',
    // 			placeholder: 'Period',
    // 			name: 'period',
    // 			type: 'dateRange'
    // 		},
    // 		{
    // 			label: 'Department',
    // 			placeholder: 'Search by Department',
    // 			name: 'department',
    // 			type: 'multiple-select'
    // 		},

    // 		{
    // 			label: 'Status',
    // 			placeholder: 'Search by Status',
    // 			name: 'status',
    // 			type: 'multiple-select'
    // 		},
    // 		{
    // 			label: 'Search',
    // 			placeholder: 'Search by staff name, ID',
    // 			name: 'employee',
    // 			type: 'auto-complete'
    // 		},
    // 		{
    // 			label: 'Delegator',
    // 			placeholder: 'Search by Delegator',
    // 			name: 'members',
    // 			type: 'auto-complete'
    // 		},
    // 	],
    // }
  },
  checkin_checkout: {
    page: "Check In-Out Time",
    key: "/timesheet/checkin-checkout",
    controls: [
      {
        label: "Staff Name",
        placeholder: "Staff Name",
        name: "employee",
        type: "auto-complete",
      },
      {
        label: "Payroll period",
        placeholder: "Payroll period",
        name: "period",
        type: "dateRange",
      },
    ],
  },
  checkin_checkout_by_staff: {
    page: "Check In-Out Time",
    key: "/timesheet/checkin-checkout",
    controls: [
      {
        label: "Payroll period",
        placeholder: "Payroll period",
        name: "period",
        type: "dateRange",
      },
    ],
  },

  payroll_report_detail: {
    page: "Payroll Report Detail",
    key: "/payroll/payroll-report-detail",
    controls: [
      {
        label: "Choose a Payroll Period",
        placeholder: "Choose a Payroll Period",
        name: "timesheetId",
        type: "select",
      },
      {
        label: "Search",
        placeholder: "Search by staff name, ID",
        name: "userList",
        type: "auto-complete",
      },
      {
        label: "Department",
        placeholder: "Search by Department",
        name: "departments",
        type: "multiple-select",
      },
    ],
  },
  payroll_report_summary: {
    page: "Payroll Report Summary",
    key: "/payroll/payroll-report-detail",
    controls: [
      {
        label: "Choose a Payroll Period",
        placeholder: "Choose a Payroll Period",
        name: "timesheetId",
        type: "select",
      },
      {
        label: "Search",
        placeholder: "Search by staff name, ID",
        name: "userList",
        type: "auto-complete",
      },
      {
        label: "Department",
        placeholder: "Search by Department",
        name: "departments",
        type: "multiple-select",
      },
    ],
  },

  checkIO_report: {
    page: "Check In - Check Out Report",
    key: "/report/checkInOut-TMS-report",
    controls: [
      {
        label: "Period",
        placeholder: "Period",
        name: "period",
        type: "dateRange",
      },
      {
        label: "Search",
        placeholder: "Search by staff name, ID",
        name: "userList",
        type: "auto-complete",
      },
      {
        label: "Department",
        placeholder: "Search by Department",
        name: "departments",
        type: "multiple-select",
      },
      {
        label: "Project",
        placeholder: "Search by Project",
        name: "projects",
        type: "multiple-select",
      },
    ],
  },

  logs: {
    page: "Logs",
    key: "/logs",
    log_list: {
      page: "Logs List",
      tab: "log_list",
      controls: [
        {
          label: "Period",
          placeholder: "Period",
          name: "period",
          type: "dateRange",
        },
        {
          label: "Search",
          placeholder: "Search by staff name, ID,",
          name: "employee",
          type: "auto-complete",
        },
        {
          label: "Search by table",
          placeholder: "Search by table",
          name: "table",
          type: "multiple-select",
        },
      ],
    },
    connection_history: {
      page: "Connection History",
      tab: "connection_history",
      controls: [
        {
          label: "Period",
          placeholder: "Period",
          name: "period",
          type: "dateRange",
        },
      ],
    },
  },
  payroll_list: {
    page: "Payroll",
    key: "/payroll",
    controls: [
      {
        label: "Choose a Payroll Period",
        placeholder: "Choose a Payroll Period",
        name: "payroll_period",
        type: "select",
      },
      {
        label: "Search",
        placeholder: "Search by staff name, ID",
        name: "employee",
        type: "auto-complete",
      },
      {
        label: "Department",
        placeholder: "Department",
        name: "department",
        type: "multiple-select",
      },
      {
        label: "Role",
        placeholder: "Role",
        name: "role",
        type: "multiple-select",
      },
      {
        label: "Project",
        placeholder: "Search by Project",
        name: "projects",
        type: "multiple-select",
      },
      {
        label: "Payment Method",
        placeholder: "Search by Payment Method",
        name: "method",
        type: "multiple-select",
      },
    ],
  },
  payslip: {
    page: "Payslip",
    key: "/payroll",
    controls: [
      {
        label: "Choose a Payroll Period",
        placeholder: "Choose a Payroll Period",
        name: "payroll_period",
        type: "select",
      },
    ],
  },
  employeeList: {
    page: "Employee",
    key: "/employee",
    controls: [
      {
        label: "Search",
        placeholder: "Search by staff name, ID",
        name: "employee",
        type: "auto-complete",
      },
      {
        label: "Search",
        placeholder: "Search by SF4C ID",
        name: "employee_sf4c",
        type: "auto-complete",
      },
      {
        label: "Department",
        placeholder: "Department",
        name: "department",
        type: "multiple-select",
      },
    ],
  },
  employeeSoda: {
    page: "Employee Soda",
    key: "/employee",
    controls: [
      {
        label: "Search",
        placeholder: "Search by SDSV ID",
        name: "employee_sds",
        type: "auto-complete",
      },
      {
        label: "Search",
        placeholder: "Search by LDAP ID",
        name: "employee",
        type: "auto-complete",
      },
    ],
  },
  configurations: {
    page: "Configurations",
    key: "/configurations",
    controls: [
      {
        label: "Name",
        placeholder: "Search by Name",
        name: "configurations",
        type: "auto-complete",
      },
    ],
    Positions: {
      page: "Positions",
      tab: "Positions",
      controls: [
        {
          label: "Name",
          placeholder: "Search by Name",
          name: "configurations",
          type: "auto-complete",
        },
      ],
    },
    KPI: {
      page: "KPI",
      tab: "KPI",
      controls: [
        {
          label: "Year",
          placeholder: "Search by Year",
          name: "year",
          type: "yearPicker",
        },
        {
          label: "Name",
          placeholder: "Search by Name",
          name: "name",
          type: "textLike",
        },
      ],
    },
    Priority: {
      page: "Priority",
      tab: "Priority",
      controls: [
        {
          label: "Name",
          placeholder: "Search by Name",
          name: "configurations",
          type: "auto-complete",
        },
      ],
    },
    Experience: {
      page: "Experience",
      tab: "Experience",
      controls: [
        {
          label: "Name",
          placeholder: "Search by Name",
          name: "configurations",
          type: "auto-complete",
        },
      ],
    },
    Major: {
      page: "Major",
      tab: "Major",
      controls: [
        {
          label: "Name",
          placeholder: "Search by Name",
          name: "configurations",
          type: "auto-complete",
        },
      ],
    },
    ["Request Type"]: {
      page: "Request Type",
      tab: "Request Type",
      controls: [
        {
          label: "Name",
          placeholder: "Search by Name",
          name: "configurations",
          type: "auto-complete",
        },
      ],
    },
    Area: {
      page: "Area",
      tab: "Area",
      controls: [
        {
          label: "Name",
          placeholder: "Search by Name",
          name: "configurations",
          type: "auto-complete",
        },
      ],
    },
    ["Job Type"]: {
      page: "Job Type",
      tab: "Job Type",
      controls: [
        {
          label: "Name",
          placeholder: "Search by Name",
          name: "configurations",
          type: "auto-complete",
        },
      ],
    },
  },
  requests: {
    page: "Requests",
    key: "/requests",
    controls: [
      {
        label: "Deadline",
        placeholder: "Deadline",
        name: "deadline",
        type: "dateRange",
      },
      {
        label: "RequestID",
        placeholder: "Search by Id",
        name: "requestIds",
        type: "auto-complete",
      },
      {
        label: "Title",
        placeholder: "Search by Title",
        name: "titleRequest",
        type: "textLike",
      },
      {
        label: "Hr",
        placeholder: "Choose TA",
        name: "hr",
        type: "multiple-select",
        keyMatchCount: true,
      },
      {
        label: "Area",
        placeholder: "Area",
        name: "area",
        type: "multiple-select",
      },
      {
        label: "Group",
        placeholder: "Choose Division",
        name: "group",
        type: "multiple-select",
      },
      {
        label: "Department",
        placeholder: "Choose Department",
        name: "department",
        type: "multiple-select",
      },
      // {
      //     label: 'Project',
      //     placeholder: 'Choose Project',
      //     name: 'project',
      //     type: 'multiple-select'
      // },
      {
        label: "Status",
        placeholder: "Choose Status",
        name: "status",
        type: "multiple-select",
      },
      {
        label: "Priority",
        placeholder: "Priority",
        name: "priority",
        type: "multiple-select",
      },
      {
        label: "Language",
        placeholder: "Language",
        name: "language",
        type: "multiple-select",
      },
    ],
  },
  requestDetailInterview: {
    page: "Request Detail Interview",
    key: "/request-detail-interview",
    controls: [
      {
        label: "Name",
        placeholder: "Search by name",
        name: "interviewName",
        type: "auto-complete",
      },
      {
        label: "location",
        placeholder: "Location",
        name: "interviewLocation",
        type: "multiple-select",
      },
      {
        label: "interviewer",
        placeholder: "Interviewer",
        name: "interviewInterviewer",
        type: "multiple-select",
      },
      {
        label: "status",
        placeholder: "Status",
        name: "interviewStatus",
        type: "multiple-select",
      },
    ],
  },
  requestDetailCandidate: {
    page: "Request Detail Candidate",
    key: "/request-detail-candidate",
    controls: [
      {
        label: "Name",
        placeholder: "Search by name",
        name: "candidate",
        type: "auto-complete",
      },
    ],
  },
  requestDetailInterviewer: {
    page: "Request Detail Interviewer",
    key: "/request-detail-candidate",
    controls: [
      {
        label: "Name",
        placeholder: "Choose interviewers",
        name: "interviewer",
        type: "auto-complete",
      },
    ],
  },
  requestDetail: {
    page: "Request Detail Source",
    key: "/request-detail",
    controls: [
      {
        label: "Name",
        placeholder: "Search by name",
        name: "name",
        type: "auto-complete",
      },
    ],
  },
  shareJob: {
    page: "Share Job",
    key: "/shareJob",
    controls: [
      {
        label: "Name",
        placeholder: "Enter Keyword",
        name: "keyword",
        type: "auto-complete",
      },
      {
        label: "Area",
        placeholder: "Area",
        name: "area",
        type: "multiple-select",
      },
    ],
  },
  shareJobDetail: {
    page: "Share Job Detail",
    key: "/shareJobDetail",
    controls: [
      {
        label: "Name",
        placeholder: "Enter Keyword",
        name: "keyword",
        type: "auto-complete",
      },
      {
        label: "Area",
        placeholder: "Location",
        name: "area",
        type: "select",
      },
      {
        label: "Job Type",
        placeholder: "Job Type",
        name: "jobType",
        type: "select",
      },
      {
        label: "Deadline",
        placeholder: "Deadline",
        name: "deadline",
        type: "datePicker",
      },

      {
        label: "Experience",
        placeholder: "Experience",
        name: "experience",
        type: "select",
      },
      {
        label: "Salary",
        placeholder: "Salary Estimate",
        name: "salary",
        type: "select",
      },
      {
        name: "button",
        type: "button",
      },
    ],
  },
  ticketReport: {
    page: "Ticket Report",
    key: "/ticketReport",
    controls: [
      {
        label: "Deadline",
        placeholder: "Deadline",
        name: "deadline",
        type: "dateRange",
      },
      {
        label: "Status",
        placeholder: "Status",
        name: "status",
        type: "multiple-select",
      },
      {
        label: "Priority",
        placeholder: "Priority",
        name: "priority",
        type: "multiple-select",
      },
      // {
      //   label: "Project",
      //   placeholder: "Search by Project",
      //   name: "project",
      //   type: "multiple-select",
      // },
      {
        label: "Group",
        placeholder: "Choose Division",
        name: "group",
        type: "multiple-select",
      },
      {
        label: "Department",
        placeholder: "Choose Department",
        name: "department",
        type: "multiple-select",
      },
      {
        label: "Location",
        placeholder: "Location",
        name: "area",
        type: "multiple-select",
      },
    ],
  },
  taReport: {
    page: "Ta Report",
    key: "/taReport",
    controls: [
      {
        label: "Deadline",
        placeholder: "Deadline",
        name: "deadline",
        type: "dateRange",
      },
      {
        label: "Group",
        placeholder: "Choose Division",
        name: "group",
        type: "select",
      },
      {
        label: "Department",
        placeholder: "Choose Department",
        name: "department",
        type: "select",
      },
      {
        label: "Location",
        placeholder: "Location",
        name: "area",
        type: "select",
      },
    ],
  },
  processingReport: {
    page: "Processing Report",
    key: "/processingReport",
    controls: [
      {
        label: "Deadline",
        placeholder: "Deadline",
        name: "deadline",
        type: "datePicker",
      },
      {
        label: "Skills",
        placeholder: "Search by skill",
        name: "keyword",
        type: "auto-complete",
      },
      {
        label: "Group",
        placeholder: "Choose Group",
        name: "group",
        type: "multiple-select",
      },
      {
        label: "Department",
        placeholder: "Choose Department",
        name: "department",
        type: "multiple-select",
      },
      {
        label: "Location",
        placeholder: "Location",
        name: "area",
        type: "multiple-select",
      },
    ],
  },
};

// constant status timesheet
export const statusTimesheet = {
  draft: 0,
  reject: 1,
  pendingUpdate: 2,
  pendingApproval: 3,
  approved: 4,
};

export const statusText = {
  1: "PENDING",
  3: "APPROVED",
  4: "REJECTED",
  5: "CANCELLED",
  0: "EXPLAIN",
  10: "EXPIRED",
};

export const statusTextReverse = {
  PENDING: 1,
  APPROVED: 3,
  REJECTED: 4,
  CANCELLED: 5,
  EXPLAIN: 0,
  EXPIRED: 10,
};

export const statusColor = {
  DONE: "blue",
  EXPLAIN: "blue",
  APPROVED: "green",
  REJECTED: "red",
  CANCELLED: "",
  PENDING: "orange",
  DELEGATED: "purple",
  EXPIRED: "",
  DELETED: "magenta",
  CLOSED: "#42526E",
  ASSIGNED: "blue",
};

export const otType = {
  OT_IN_OFFICE: "OT in office",
  OT_FROM_HOME: "OT from home",
  WORK_OUTSIDE: "Work Outside",
};

export const absentType = {
  1: "Full-time leave",
  2: "Morning leave",
  3: "Afternoon leave",
};

export const Role = {
  STAFF: "TMS_Staff_1",
  DULEAD: "TMS_DL_1",
  GROUP_LEAD: "TMS_GL_1",
  HR: "TMS_HR_1",
  ADMIN: "TMS_Admin_1",
  GUEST: "TMS_GUEST_1",
};

export const reasonType = {
  1: "Unauthorized Early-Leaving",
  2: "Unauthorized Late-Coming",
  3: "Unauthorized part of the day leave",
  4: "No Check-in",
  5: "No Check-out",
  6: "No Check-in, No Check-out",
};

export const leavingType = [
  {
    type: "ANNUAL",
    name: "Annual Leave",
  },
  { type: "OFFSET", name: "Offset Day" },
];

export const configPanigationDefault = {
  currentPage: 1,
  pageSize: 10,
};

export const configSortDefault = {
  orderBy: "DESC",
  orderType: "id",
};

export const UPDATED_STATUS = {
  approve: "Approved",
  approve2: "Approved2",
  reject: "Rejected",
  submit: "Pending",
  close: "Closed",
  assign: "Assigned",
  share: "Shared",
};

export const STATUS_COLOR = {
  Pending: "orange",
  Assigned: "#0052cc",
  Approved: "green",
  Approved2: "#2b9987",
  Rejected: "red",
  Closed: "gray",
  New: "violet",
  Shared: "#1597E5",
};

export const ACTION_KEY = {
  request_create: "rts-add-request",
  request_edit: "rts-edit-request",
  request_delete: "rts-delete-request",
  request_approve: "rts-approve-request",
  request_assign: "rts-assign-request",
  request_assignTALead: "rst-assignTALead-request",
  request_reject: "rts-reject-request",
  request_submit: "rts-submit-request",
  request_close: "rts-close-request",
  request_share: "rts-share-request",
  request_import_candidate: "rts-import-candidate",
  request_move_candidate: "rts-move-candidate",
  request_delete_candidate: "rts-delete-candidate",
  request_book_interview_candidate: "rts-book-interview-candidate",
  source_create: "rts-add-candidate",
  source_import: "rts-import-candidate",
  source_assign: "rts-assigned-source",
  source_reject: "rts-reject-source",
};

export const API_ACTION_KEY = {
  viewDetail: "REQUEST.SHOW",
  create: "rts-add-request",
  edit: "rts-edit-request",
  approve: "rts-approve-request",
  //approve2: "rts-approve-request",
  assign: "rts-assign-request",
  reject: "rts-reject-request",
  submit: "rts-submit-request",
  close: "rts-close-request",
  makeCandidate: "makeCandidate",
  share: "rts-share-request",
  moveCandidate: "rts-move-candidate",
  deleteCandidate: "rts-delete-candidate",
  bookInterviewCandidate: "rts-book-interview-candidate",
  createCV: "rts-create-cv",
};

export const API_USER_TITLE = {
  DU_LEAD: "RTS-DULead",
  G_LEAD: "RTS-GLead",
  HR_LEAD: "RTS-TALead",
  HR_MNG: "RTS-TA-MNG",
  HR: "RTS-TA",
  ADMIN: "RTS-Admin",
  EB: "RTS-EB",
};

export const MESSAGE_DURATION = 2;

export const PAGE_OPTIONS_KEY = "pageOptions";

export const TAB_KEYS = {
  GENERAL_INFORMATION: "general-information",
  SOURCE: "source",
  QUALIFY: "qualify",
  CONFIRM: "confirm",
  INTERVIEW: "interview",
  OFFER: "offer",
  ONBOARD: "onboard",
  FAILED: "failed",
};

export const ACTION_KEYS = {
  BACK: "back",
  ASSIGNEE: "assignee",
  CALENDAR: "calender",
  MAIL: "mail",
  MESSAGE: "message",
  DELETE: "delete",
};
