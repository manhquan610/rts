import { listCandidate } from "./mockData/listCandidate.json";
import moment from "moment";
import http from "./helpers/httpInterceptor";
import axios from "axios";
import { convertObjectToParams, PAGE_OPTIONS_CANDIDATE } from "../../components/commons/commons";

const getDefaultState = () => {
  return {
    listCandidates: [],
    tabCandidateNumber: {
      source: 0,
      qualify: 0,
      confirm: 0,
      interview: 0,
      onboard: 0,
      offer: 0,
      failed: 0,
    },
    pageOptions: { ...PAGE_OPTIONS_CANDIDATE },
    total_items: 10,
    checkAll: false,
    selectedRowKeys: [],
    candidateId: 0,
    aciveKeyTabs: 1,
    addCandidateSuccess: false,
    assignCandidateSuccess: false,
  };
};
const state = getDefaultState();

const mutations = {
  SET_LIST_CANDIDATE(state, { data, requestId }) {
    state.listCandidates = data.map((user) => {
      user.history = moment(user.history).format("DD-MM-YYYY HH:mm:ss");
      user.checked = false;
      user.clicked = false;
      const listIds = JSON.parse(user.removeJobRtsIds);
      if (user.removeJobRtsIds != null) {
        for (let i = 0; i < listIds.length; i++) {
          if (listIds[i] == requestId) {
            user.checkCandidateFaild = true;
          }
        }
        // if(requestId === listIds[0]){
        //   user.checkCandidateFaild = true
        // }
      }
      return user;
    });
  },
  SET_TAB_CANDIDATE_NUMBER(state, data) {
    state.tabCandidateNumber = data;
  },
  SET_CANDIDATE_SUCCESS(state, data) {
    state.addCandidateSuccess = data;
  },
  SET_ASSIGN_CANDIDATE_SUCCESS(state, data) {
    state.assignCandidateSuccess = data;
  },
  SET_PAGE_OPTIONS(state, data) {
    state.pageOptions = {};
    state.pageOptions = data;
  },
  SET_TOTAL_CANDIDATE(state, data) {
    state.total_items = data;
  },
  SET_SELECTED_ROW_KEY(state, data) {
    state.selectedRowKeys = data;
  },
  SET_CHECK_ALL(state, data) {
    state.checkAll = data;
  },
  SET_CHECKED_USER(state, id) {
    state.listCandidates.map((user) => {
      if (user.candidateId == id) user.checked = !user.checked;
      return user;
    });
  },
  SET_SELECT_ALL_USER(state, check) {
    state.listCandidates.map((user) => {
      if (!check) {
        user.checked = true;
      } else {
        user.checked = false;
      }
      return user;
    });
  },
  ORDER_BY_DATE(state) {
    const listSortDate = state.listCandidates.sort((a, b) => {
      return new Date(a.history) - new Date(b.history);
    });
    state.listCandidates = listSortDate;
  },
  SET_CANDIDATE_ID(state, id) {
    state.candidateId = id;
  },
  SET_ACTIVE_KEYTAB(state, data) {
    state.aciveKeyTabs = data;
  },
  SET_USER_DELETED(state, data) {
    state.listCandidates = state.listCandidates.filter((user) => {
      return !data.includes(user.candidateId);
    });
  },
  RESET_STATE(state) {
    Object.assign(state, getDefaultState());
  },
};

const actions = {
  getListCandidate({ commit }, payload) {
    const params = convertObjectToParams(payload);
    return http
      .post(`candidate/search${params}`)
      .then((res) => {
        let data = res.data;
        commit("SET_LIST_CANDIDATE", { data: data.candidateList, requestId: payload.requestId });
        commit("SET_TOTAL_CANDIDATE", data.totalElements);
        commit("SET_TAB_CANDIDATE_NUMBER", {
          source: data.totalSource,
          qualify: `${data.totalQualify} / ${data.allQualify}`,
          interview: `${data.totalInterview} / ${data.allInterview}`,
          offer: `${data.totalOffer} / ${data.allOffer}`,
          onboard: `${data.totalOnboard} / ${data.allOnboard}`,
          confirm: `${data.totalConfirm} / ${data.allConfirm}`,
          failed: `${data.totalFailed}`,
        });
        return data;
      })
      .catch((error) => console.log("error", error));
  },
  checkedCandidate({ commit }, id) {
    commit("SET_CHECKED_USER", id);
  },
  changeCandidateState({ commit }, payload) {
    const { candidateStateCurrent } = payload;
    return http
      .put(`/candidate/${payload.ids}?candidateStateCurrent=${candidateStateCurrent}`, payload)
      .then((res) => {
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  checkAllUser({ commit }, check) {
    commit("SET_SELECT_ALL_USER", check);
  },
  orderByDate({ commit }) {
    commit("ORDER_BY_DATE");
  },
  deleteCandidate({ commit }, payload) {
    const { candidateId, requestId } = payload;
    return http
      .delete(`/candidate/${candidateId}?requestId=${requestId}`)
      .then((res) => {
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
