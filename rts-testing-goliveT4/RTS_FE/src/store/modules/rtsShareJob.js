import http from "./helpers/httpInterceptor";
import { convertObjectToParams, PAGE_OPTIONS_FEATUREDJOB } from "../../components/commons/commons"
import moment from 'moment';

const state = () => ({
    jobList: [],
    area: [],
    priority: [],
    positionSF4C: [],
    jobType: [],
    currentPage: '',
    listFeturedJob: [],
    pageOptions: {
        offset: PAGE_OPTIONS_FEATUREDJOB.offset,
        limit: PAGE_OPTIONS_FEATUREDJOB.limit,
        totalItems: 0
    },
    currentFilter: {},
    isClear: false,
    detailSearch:[]
})
const mutations = {
    SET_JOB_LIST(state, data) {
        state.jobList = data
    },
    SET_AREA(state, data) {
        state.area = data
    },
    SET_PRIORITY(state, data) {
        state.priority = data
    },
    SET_POSITION_SF4C(state, data) {
        state.positionSF4C = data
    },
    SET_JOB_TYPE(state, data) {
        state.jobType = data
    },
    SET_LIST_FEATURE_JOB(state, data) {
        state.listFeturedJob = data
    },
    SET_PAGE_OPTIONS(state, data) {
        state.pageOptions = {}
        state.pageOptions = data
    },
    SET_TOTAL_FEATURED(state, data) {
        state.pageOptions = data
    },
    SET_DETAIL_SEARCH(state, data) {
        state.detailSearch =  data
    },
    SET_CURRENT_PAGE(state, data) {
        state.currentPage =  data
    },
    SET_CURRENT_FILTER(state, data) {
        state.currentFilter =  data
    },
    SET_CLEAR_STATE(state,data){
        state.isClear = data
    }
}
const actions = {
    getListFeaturedJob({ commit }, params) {
        const { page, size } = params

        return http.get('findJobPosting', { params })
            .then((res) => {
                commit("SET_TOTAL_FEATURED", {
                    totalItems: res.data.totalElements,
                    offset: page,
                    limit: size,
                })
                commit('SET_LIST_FEATURE_JOB', res.data.content)
            })
            .catch((error) => console.log('error', error))
    },

    getListJobs({ commit }, payload) {
        let salary = payload?.salary
        let date = payload?.deadline
        let page = payload?.page
        let size = payload?.size
        let newDate = date ? moment(date).toISOString() : null;
        let skillList = payload?.keyword
        let experience = payload?.experiences
        let positionSF4C = payload?.positionSF4C
        let jobType = payload?.jobType
        let area = payload?.area
        let newSalary = salary
        delete payload.salary
        delete payload.date
        delete payload.keyword
        if (Number(jobType) === 0) {
            jobType = null;
        }
        const newPayload = {
            startSalary: newSalary,
            deadLine: newDate,
            page: page,
            size: size,
            skills: skillList,
            experience: experience,
            area: area,
            positionSF4C: positionSF4C,
            jobType: jobType,
            endSalary: (Number(newSalary) + 5000000)
        };
        const paramFilter = convertObjectToParams(newPayload);
        return http.get(`/findJobPosting${paramFilter}`)
            .then(res => {
                commit("SET_JOB_LIST", res.data.content);
                commit("SET_TOTAL_FEATURED", {
                    totalItems: res.data.totalElements,
                    offset: page,
                    limit: size,
                });
                return res.data.content
            })
            .catch(error => console.log('error', error))
    },
    getPositionSF4C({ commit }) {
        return http.get(`/sf4c/position/list`)
            .then(res => {
                commit("SET_POSITION_SF4C", res.data);
                return res.data
            })
            .catch(error => console.log('error', error))
    },
    getJobType({ commit }) {
        return http.get(`/manager-jobType/getAll?limit=100&page=1`)
            .then(res => {
                commit("SET_JOB_TYPE", res.data.content);
                return res.data.content
            })
            .catch(error => console.log('error', error))
    },
    getAreas({ commit }) {
        return http.get(`/manager-area/getAll?limit=100&page=1`)
            .then(res => {
                commit("SET_AREA", res.data.content);
                return res.data.content
            })
            .catch(error => console.log('error', error))
    },
    getPriority({ commit }) {
        const payload = {"offset":1,"limit":1000,"like":"0"}
        return http.post(`/manager-priority/exact`, payload)
            .then(res => {
                commit("SET_PRIORITY", res.data.manager);
                return res.data.manager
            })
            .catch(error => console.log('error', error))
    }
}
export default {
    namespaced: true,
    state,
    actions,
    mutations,
}
