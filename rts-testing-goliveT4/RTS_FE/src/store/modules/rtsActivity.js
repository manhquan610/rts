import { tabs, listComment, listHistory } from "./mockData/activity.json";
import http from './helpers/httpInterceptor';
const state = () => ({
  tabs: tabs,
  listComment: [],
  listHistory: [],
  keyTab : ""
});

const mutations = {
  SET_LIST_COMMENT(state, data) {
    state.listComment = data;
  },
  SET_LIST_HISTORY(state, data) {
    state.listHistory = data;
  },
  SET_ACTIVITY_TAB(state, data) {
    state.keyTab = data;
  },
};

const actions = {
  getListComment({ commit }, payload) {
    const {page, requestId} = payload
    return http.get(`/comment?limit=100&page=${page}&requestId=${requestId}`)
    .then(res => {
      commit("SET_LIST_COMMENT", res.data.content.reverse());
      return res.data
    })
    .catch(error => console.log('error', error))
  },
  getListHistory({ commit }) {
    let res = {
      data: listHistory,
    };
    commit("SET_LIST_HISTORY", res.data);
  },
  postComment({commit},data){
    return http.post('/comment', data)
           .then(res => {
           })
           .then(() => {
            const payload = {
              page:1,
              requestId:this.$route.params.id
            }
            if(res.status == 201) this.$store.dispatch('rtsActivity/getListComment', payload)
           })
           .catch(error => console.log('error', error))
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
