/* eslint-disable no-prototype-builtins */
import http from "./helpers/httpInterceptor";
import {findObjectById, showDateTimeFromMilliSecond} from "../../components/commons/commons";

const getDefaultState = () => {
    return {
        requestDetail: {},
        requestDetailHistory: [],
        requestDetailHistoryInfoMapStatus: {},
        historyStatus: [
            'New',
            'Pending',
            'Edited',
            'Approved',
            'Approved2',
            'Assigned',
            'Rejected',
            'Closed'
        ],
        historyStatusRefactor: [
            {
                key: "New",
                label: ""
            },
            {
                key: "Pending",
                label: ""
            },
            {
                key: "Edited",
                label: ""
            },
            {
                key: "Approved",
                label: ""
            },
            {
                key: "Approved2",
                label: ""
            },
            {
                key: "Assigned",
                label: ""
            },
            {
                key: "Rejected",
                label: ""
            },
            {
                key: "Closed",
                label: ""
            },

        ],
        isDone: false
    }
}
const state = getDefaultState();

const mutations = {
    REQUEST_DETAIL_HISTORY_LIST(state, data) {
        state.requestDetailHistory = data;
    },
    REQUEST_DETAIL(state, data) {
        state.requestDetail = data;
    },
    REQUEST_DETAIL_HISTORY_MAP_STATUS(state, data) {
        state.requestDetailHistoryInfoMapStatus = data
    },
    ADD_STATUS_HISTORY(state) {
        state.requestDetailHistoryInfoMapStatus = {}
        state.historyStatus.forEach(element => {
            state.requestDetailHistoryInfoMapStatus[element] = []
        });
    },
    ADD_DATA_HISTORY(state) {
        if (state.requestDetail.hasOwnProperty('id')) {
            state.requestDetailHistoryInfoMapStatus['New'] = [{
                id: state.requestDetail.id,
                createdBy: state.requestDetail.createdBy,
                reason: state.requestDetail.reason,
                statusRequestType: "New",
                createdDate: state.requestDetail.createdDate
            }]
        }
        state.requestDetailHistory.forEach(item => {
            if (state.historyStatus.indexOf(item.statusRequestType) < 0) {
                state.historyStatus.push(item.statusRequestType)
            }
            state.requestDetailHistoryInfoMapStatus[item.statusRequestType].push({
                id: item.id,
                createdBy: item.createdBy,
                reason: item.reason,
                statusRequestType: item.statusRequestType,
                createdDate: showDateTimeFromMilliSecond(item.createdDate)
            })
        })
        state.isDone = true
    },
    RESET_STATE(state) {
        Object.assign(state, getDefaultState())
    }
}


const actions = {
    getRtsRequestDetailHistory({commit}, id) {
        return http
            .get(`/request-history/${id}?id=${id}`)
            .then((res) => {
                commit("REQUEST_DETAIL_HISTORY_LIST", res.data);
                return res.data
            })
            .catch((error) => {
                commit("ADD_DATA_HISTORY");
                throw error;
            });
    },
    getRtsRequestDetailById({commit, rootState}, id) {
        let groups = rootState.rtsApplication.groupList
        let departments = rootState.rtsApplication.departmentAll
        let projects = rootState.rtsApplication.projectList
        let skills = rootState.rtsApplication.skillList
        let languages = rootState.rtsApplication.languageList
        return http
            .get(`/request/${id}`)
            .then((res) => {
                let data = {
                    id: res.data.id,
                    title: res.data.name,
                    requestType: res.data.managerRequestType,
                    priority: res.data.managerPriority,
                    experience: res.data.managerExperience,
                    group: findObjectById(res.data.managerGroupId, 'id', groups),
                    project: findObjectById(res.data.projectId, 'projectId', projects),
                    department: findObjectById(res.data.departmentId, 'id', departments),
                    position: res.data.managerPosition,
                    description: res.data?.description,
                    benefit: res.data?.benefit,
                    requirement: res.data?.requirement,
                    certificate: res.data?.certificate,
                    number: res.data?.number,
                    deadline: (res.data.deadline),
                    major: res.data.managerMajor,
                    status: res.data.requestStatusName,
                    skills: convertIdArrayToObjectArray(res.data?.skillNameId?.split(','),'id',skills),
                    language: convertIdArrayToObjectArray(res.data?.languageId?.split(','),'id',languages),
                    salary: res.data.salary,
                    createdBy: res.data.createdBy,
                    createdDate: formatDeadline(res.data.createdDate),
                    recruiters: res.data.requestAssigns,
                    area: res.data.area,
                    taMng: findTaMng(res.data.requestSubmits),
                    jobType: res.data.jobType,
                    jobLevel: res.data.managerJobLevels[0],
                    workingTime: res.data.workingType,
                    codePosition: res.data.codePosition,
                    positionSF4C: res.data.positionSF4C
                };
                commit("REQUEST_DETAIL", data);
                return data
            })
            .catch((error) => {
                throw error;
            });
    },
};

export default {
    namespaced: true,
    state,
    mutations,
    actions
};


function convertIdArrayToObjectArray(ids, searchProp, arr ) {
    if(!ids) {
        return
    }
    let res = []
    ids.forEach((id) => {
        let data = findObjectById(Number(id), searchProp, arr)
        if(data !== undefined){
            res.push(data)
        }
    })
    return res;
}
function findTaMng(process){
    let taLdap = process.filter(item => {
        return item.requestStatus = 'Approved2'
    })

    return taLdap.length > 0 ? taLdap[0].createdBy : ''

}
function formatDeadline(strDate) {
    if (strDate != undefined && strDate != null && String(strDate).length > 0) {
        return strDate.split('-').reverse().join('/');
    } else {
        return strDate || ''
    }
}
