import { PAGE_OPTIONS_FEATUREDJOB } from "../../components/commons/commons";
import http from "./helpers/httpInterceptor";

const getDefaultState = () => {
    return {
        listFeturedJob:[],
        pageOptions :{...PAGE_OPTIONS_FEATUREDJOB},
        total_items: 9
    }
}

const state = getDefaultState()

const mutations = {
  SET_LIST_FEATURE_JOB(state,data){
      state.listFeturedJob = data
  },
  SET_PAGE_OPTIONS(state, data) {
    state.pageOptions = {}
    state.pageOptions = data
  },
  SET_TOTAL_FEATURED(state, data) {
    state.total_items = data
  },
}
const actions = {
   getListFeaturedJob({commit},params){
       return http.get('findJobPosting',{params})
                  .then((res) => {
                    commit("SET_TOTAL_FEATURED", res.data.totalElements)  
                    commit('SET_LIST_FEATURE_JOB',res.data.content)
                  })
                  .catch((error) => console.log('error',error))
   }
}


export default {
  namespaced: true,
  state,
  actions,
  mutations,
}