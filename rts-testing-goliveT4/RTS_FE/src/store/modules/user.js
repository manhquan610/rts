import http from "../modules/helpers/httpInterceptor";
import { getLastWord } from "../../components/commons/commons";
import { API_USER_TITLE } from "../../components/commons/commonConstant";

const state = () => ({
  userInfo: localStorage.getItem("session_login") ? JSON.parse(localStorage.getItem("session_login")) : {},
  userLogout: null,
  isAuthenticated: localStorage.getItem("session_login") ? true : false,
  loading: false,
  userRole: JSON.parse(localStorage.getItem("user_role")),
});

// mutations
const mutations = {
  USER_ROLE(state, data) {
    state.userRole = data;
  },
  USER_LOGIN(state, data) {
    state.userInfo = data;
    state.isAuthenticated = true;
  },
  USER_LOGOUT(state) {
    state.userInfo = {};
    state.isAuthenticated = false;
  },
  LOADING_REQUEST(state) {
    state.loading = !state.loading;
  },
  STOP_LOADING(state) {
    state.loading = false;
  },
};

// actions
const actions = {
  userLogin({ commit }, payload) {
    return http
      .post("/authen/login", payload)
      .then((res) => {
        localStorage.setItem("session_login", JSON.stringify(res.data));
        const r = () => Math.floor(256 * Math.random());
        localStorage.setItem("color_avatar", `rgb(${r()}, ${r()}, ${r()})`);
        commit("USER_LOGIN", res.data);
        let userRoles = getUserRole(res.data);
        localStorage.setItem("user_role", JSON.stringify(userRoles));
        commit("USER_ROLE", userRoles);
        return res.data;
      })
      .catch((error) => {
        commit("USER_LOGIN", "success");
        throw error;
      });
  },

  userLogout({ commit }) {
    return new Promise((resolve) => {
      commit("USER_LOGOUT");
      localStorage.clear();
      // localStorage.removeItem('user_role');
      // localStorage.removeItem('session_login');
      // localStorage.removeItem('color_avatar');
      sessionStorage.clear();
      resolve();
    });
  },
  loadingRequest({ commit }) {
    return new Promise((resolve) => {
      commit("LOADING_REQUEST");
      resolve();
    });
  },
  stopLoading({ commit }) {
    return new Promise((resolve) => {
      commit("STOP_LOADING");
      resolve();
    });
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};

function getUserRole(session_login) {
  let userRole = {};
  userRole.activities = session_login?.user?.permissions[0]?.activities;
  // userRole.activities.push({ name: "Assign TA Lead", key: "rst-assignTALead-request", enable: true });
  userRole.department = getLastWord(session_login.department);
  userRole.title = session_login.userRoles[1];

  // if(userRole.title === API_USER_TITLE.HR_LEAD || userRole.title === API_USER_TITLE.HR_MNG){
  //   if (!userRole.activities.some(e => e.key === 'rts-add-request')) {
  //     userRole.activities.push({
  //       name: "Add request",
  //       key: "rts-add-request",
  //       enabled: true,
  //     })
  //   }
  // }
  //
  // if(userRole.title === API_USER_TITLE.HR_MNG){
  //   if (!userRole.activities.some(e => e.key === 'rts-edit-request')) {
  //     userRole.activities.push({
  //       name: "Edit request",
  //       key: "rts-edit-request",
  //       enabled: true,
  //     })
  //   }
  // }
  //
  // if(userRole.title === API_USER_TITLE.HR_MNG){
  //   if (!userRole.activities.some(e => e.key === 'rts-delete-request')) {
  //     userRole.activities.push({
  //       name: "Delete request",
  //       key: "rts-delete-request",
  //       enabled: true,
  //     })
  //   }
  // }
  //
  // if(userRole.title === API_USER_TITLE.G_LEAD || userRole.title === API_USER_TITLE.HR_MNG){
  //   if (!userRole.activities.some(e => e.key === 'rts-approve-request')) {
  //     userRole.activities.push({
  //       name: "Approve request",
  //       key: "rts-approve-request",
  //       enabled: true,
  //     })
  //   }
  // }
  //
  // if(userRole.title === API_USER_TITLE.G_LEAD || userRole.title === API_USER_TITLE.HR_MNG){
  //   if (!userRole.activities.some(e => e.key === 'rts-reject-request')) {
  //     userRole.activities.push({
  //       name: "Reject request",
  //       key: "rts-reject-request",
  //       enabled: true,
  //     })
  //   }
  // }
  //
  // if(userRole.title === API_USER_TITLE.HR || userRole.title === API_USER_TITLE.HR_LEAD || userRole.title === API_USER_TITLE.HR_MNG
  //     || userRole.title === API_USER_TITLE.ADMIN || userRole.title === API_USER_TITLE.EB) {
  //   if (!userRole.activities.some(e => e.key === 'rts-add-candidate')) {
  //     userRole.activities.push({
  //       name: "Add candidate",
  //       key: "rts-add-candidate",
  //       enabled: true,
  //     })
  //   }
  // }
  //
  // if(userRole.title !== API_USER_TITLE.G_LEAD || userRole.title !== API_USER_TITLE.DU_LEAD){
  //   if (!userRole.activities.some(e => e.key === 'rts-import-candidate')) {
  //     userRole.activities.push({
  //       name: "Import candidate",
  //       key: "rts-import-candidate",
  //       enabled: true,
  //     })
  //   }
  // }
  //
  // if(userRole.title !== API_USER_TITLE.G_LEAD || userRole.title !== API_USER_TITLE.DU_LEAD){
  //   if (!userRole.activities.some(e => e.key === 'rts-move-candidate')) {
  //     userRole.activities.push({
  //       name: "Move Candidate",
  //       key: "rts-move-candidate",
  //       enabled: true,
  //     })
  //   }
  //
  //
  //   if (userRole.title === API_USER_TITLE.HR_LEAD || userRole.title === API_USER_TITLE.HR_MNG || userRole.title === API_USER_TITLE.ADMIN
  //     || userRole.title === API_USER_TITLE.EB) {
  //   if (!userRole.activities.some(e => e.key === 'rts-assign-candidate')) {
  //     userRole.activities.push({
  //       name: "Assign candidate",
  //       key: "rts-assign-candidate",
  //       enabled: true,
  //     })
  //   }
  // }
  //
  // if (userRole.title === API_USER_TITLE.HR_LEAD || userRole.title === API_USER_TITLE.HR_MNG || userRole.title === API_USER_TITLE.ADMIN
  //     || userRole.title === API_USER_TITLE.EB) {
  //   if (!userRole.activities.some(e => e.key === 'rts-reject-candidate')) {
  //     userRole.activities.push({
  //       name: "Reject candidate",
  //       key: "rts-reject-candidate",
  //       enabled: true,
  //     })
  //   }
  // }
  //
  // if(userRole.title === API_USER_TITLE.HR || userRole.title === API_USER_TITLE.ADMIN){
  //   if (!userRole.activities.some(e => e.key === 'rts-share-request'))
  //   {
  //     userRole.activities.push({
  //       name: "Share Request",
  //       key: "rts-share-request",
  //       enabled: true,
  //     })
  //   }
  // }
  //
  //   if (!userRole.activities.some(e => e.key === 'rts-delete-candidate')) {
  //     userRole.activities.push({
  //       name: "Delete Candidate",
  //       key: "rts-delete-candidate",
  //       enabled: true,
  //     })
  //   }
  //
  //   if (!userRole.activities.some(e => e.key === 'rts-book-interview-candidate')) {
  //     userRole.activities.push({
  //       name: "Book interview Candidate",
  //       key: "rts-book-interview-candidate",
  //       enabled: true,
  //     })
  //   }
  //
  //   if (!userRole.activities.some(e => e.key === 'rts-create-cv')) {
  //     userRole.activities.push({
  //       name: "Create CV",
  //       key: "rts-create-cv",
  //       enabled: true,
  //     })
  //   }
  // }

  return userRole;
}
