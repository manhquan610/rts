// import axios from "axios";
// import config from "../../config";
import http from './helpers/httpInterceptor'

const TIMESHEET_ROOT = "tms/timesheets";

const state = () => ({
  timesheetPayrollPeriodList: [],
  noteList: [],
  timesheetAllUser: [],
  timesheetUser: {},
  commentList: [],
  calculationWorking: {},
  violationWorking: {},
  annualLeave: {},
  ruleIntegration: [], 
  countWokingMyTS: [],
  updateHistory: [],
});

// mutations
const mutations = {
  TIMESHEET_BY_PAYROLL_PERIOD_LIST(state, data) {
    state.timesheetPayrollPeriodList = data;
  },
  NOTE_LIST(state, data) {
    state.noteList = data;
  },
  ALL_USER_TIMESHEET(state, data) {
    state.timesheetAllUser = data;
  },
  USER_TIMESHEET(state, data) {
    state.timesheetUser = data;
  },
  COMMENT_LIST(state, data) {
    state.commentList = data;
  },
  CALCULATION_WORKING(state, data) {
    state.calculationWorking = data;
  },
  VIOLATION_WORKING(state, data) {
    state.violationWorking = data;
  },
  ANNUAL_LEAVE(state, data) {
    state.annualLeave = data;
  },
  RULE_INTEGRATION(state, data) {
    state.ruleIntegration = data;
  },
  COUNT_WORKING_MYTS(state, data){
    state.countWokingMyTS = data
  },
  UPDATE_HISTORY(state, data) {
    state.updateHistory = data
  }
};

// actions
const actions = {

  getTimesheetByPayrollPeriod({ commit, dispatch }, url) {
    dispatch('user/loadingRequest', true)
    return http.get(TIMESHEET_ROOT + url).then(res => {
      commit("TIMESHEET_BY_PAYROLL_PERIOD_LIST", res.data);
      return res.data
    }).catch(error => {
      throw (error)
    })
  },

  getNotes({ commit }) {
    return http.get('tms/settings/notes').then(res => {
      commit("NOTE_LIST", res.data.data);
      return res.data
    }).catch(error => {
      throw (error)
    })
  },

  getAllUserTimesheet({ commit }, params) {
    return http.get(TIMESHEET_ROOT + `/${params.timesheetId}/statistic${params.paramFilter}`).then(res => {
      commit("ALL_USER_TIMESHEET", res.data.data);
      return res.data
    }).catch(error => {
      throw (error)
    })
  },

  getUserTimesheet({ commit }, params) {
    return http.get(TIMESHEET_ROOT + `/${params.timesheetId}/employee/${params.employmentID}`).then(res => {
      commit("USER_TIMESHEET", res.data.data);
      return res.data
    }).catch(error => {
      throw (error)
    })
  },

  getCommentsTimesheet({ commit }, timesheetId) {
    return http.get(TIMESHEET_ROOT + `/${timesheetId}/comments`).then(res => {
      commit("COMMENT_LIST", res.data.data);
      return res.data
    }).catch(error => {
      throw (error)
    })
  },

  getCalculationWorkingEmployee({ commit }, params) {
    return http.get(TIMESHEET_ROOT + `/get-calculate-my-timesheet?id=${params.timesheetId}&employeeId=${params.employmentID}`).then(res => {
      commit("CALCULATION_WORKING", res.data.data);
      return res.data
    }).catch(error => {
      throw (error)
    })
  },

  getViolationWorkingEmployee({ commit }, params) {
    return http.get(TIMESHEET_ROOT + `/get-violation-working/${params.timesheetId}/${params.employmentID}`).then(res => {
      commit("VIOLATION_WORKING", res.data.data);
      return res.data
    }).catch(error => {
      throw (error)
    })
  },

  getAnnualLeaveEmployee({ commit }, params) {
    return http.get(TIMESHEET_ROOT + `/get-annual-leave/${params.timesheetId}/${params.employmentID}`).then(res => {
      commit("ANNUAL_LEAVE", res.data.data);
      return res.data
    }).catch(error => {
      throw (error)
    })
  },

  getCountWorkingMyTS({commit}, params) {
    return http.get(TIMESHEET_ROOT + `/count-working-time-my-timesheet?timesheetId=${params.timesheetId}&employeeId=${params.employmentID}`).then(res => {
      commit("COUNT_WORKING_MYTS", res.data.data);
      return res.data
    }).catch(error => {
      throw error
    })
  },
  
  getRuleIntegration({ commit }) {
    return http.get('tms/rule-integration/value').then(res => {
      commit("RULE_INTEGRATION", res.data);
      return res.data
    }).catch(error => {
      throw (error)
    })
  },

  updateHistory({commit}, params) {
    return http 
    .put(`/tms/timesheets/update-job-history?userId=${params.sf4cId}`)
    .then((res) => {
      commit('UPDATE_HISTORY', res)
      return res
    }).catch((err) => {
      throw err
    })
  }

};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
