import {listPeople} from './mockData/listPeople.json';

const state = () => ({
    listPeople:[]
})

const mutations = {
  SET_LIST_PEOPLE(state, data){
      state.listPeople = data
  }
}

const actions = {
  getListPeople({commit}){
    const res = {
        data:listPeople
    }  
    commit('SET_LIST_PEOPLE', res.data)
  }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}