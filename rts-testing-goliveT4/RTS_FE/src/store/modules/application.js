import http from "./helpers/httpInterceptor";

const state = () => ({
  requestTypeList: [],
  payrollPeiodList: [],
  locationList: [],
  requestStatusList: [],
  // delegationStatusList: [],
  // abnormalStatusList: [],
  reportStatusList: [],
  employeeList: [],
  absentTypesList: [],
  statusList: [],
  reasonTypeList: [],
  departmentList: [],
  memberList: [],
  tableList: [],
  roleList: [],
  reasonRequestList: [],
  projectList: [],
  sameDUStaff: [],
  otTypeList: [],
  checkDateInApprovedPeriod: {},
  lastTimesheet: {},
  remainTypeLeaveList: [],
  skillList: [],
  languageList: [],
  groupList: [],
});

// mutations
const mutations = {
  REQUEST_TYPE_DATA_LIST(state, data) {
    state.requestTypeList = data;
  },

  PAYROLL_PERIOD_DATA_LIST(state, data) {
    state.payrollPeiodList = data;
  },

  LOCATION_DATA_LIST(state, data) {
    state.locationList = data;
  },

  REQUEST_STATUS_DATA_LIST(state, data) {
    state.requestStatusList = data;
  },

  // DELEGATION_STATUS_DATA_LIST(state, data) {
  // 	state.delegationStatusList = data
  // },

  // ABNORMAL_STATUS_DATA_LIST(state, data) {
  // 	state.abnormalStatusList = data
  // },

  REPORT_STATUS_DATA_LIST(state, data) {
    state.reportStatusList = data;
  },

  EMPLOYEE_DATA_LIST(state, data) {
    state.employeeList = data.slice(0, 20).map((item) => {
      return {
        value: item.user_name,
        label: item.name,
        department: item.department_name?.nameEn || "",
        id: item.id,
        sf4c_id: item.sf4c_id,
      };
    });
  },

  SAME_DU_STAFF(state, data) {
    state.sameDUStaff = data.slice(0, 20).map((item) => {
      return {
        value: item.user_name,
        label: item.name,
        department: item.department_name?.nameEn || "",
        id: item.id,
      };
    });
  },

  MEMBER_DATA_LIST(state, data) {
    // state.memberList = data
    state.memberList = data.slice(0, 20).map((item) => {
      return {
        value: item.user_name,
        label: item.name,
      };
    });
  },

  ABSENT_DATA_LIST(state, data) {
    state.absentTypesList = data;
  },
  STATUS_LIST(state, data) {
    state.statusList = data;
  },
  REASON_TYPE_LIST(state, data) {
    state.reasonTypeList = data;
  },
  DEPARTMENT_LIST(state, data) {
    state.departmentList = data.filter(
      (v, i, a) =>
        a.findIndex((t) => t.deparment_code === v.deparment_code) === i
    );
  },
  TABLE_LIST(state, data) {
    state.tableList = data;
  },
  ROLE_LIST(state, data) {
    state.roleList = data;
  },
  REASON_LIST(state, data) {
    state.reasonRequestList = data;
  },
  PROJECT_LIST(state, data) {
    state.projectList = data;
  },
  GROUP_LIST(state, data) {
    state.groupList = data;
  },
  OT_TYPE(state, data) {
    state.otTypeList = data;
  },
  DATE_PERIOD(state, data) {
    state.checkDateInApprovedPeriod = data;
  },
  LAST_TIMESHEET(state, data) {
    state.lastTimesheet = data;
  },
  REMAIN_TYPE_LEAVE_LIST(state, data) {
    state.remainTypeLeaveList = data;
  },
  SKILL_LIST(state, data) {
    state.skillList = data;
  },
  LANGUAGE_LIST(state, data) {
    state.languageList = data;
  },
};
const actions = {
  ///Action get Request type
  getListRequestTypeList({ commit }) {
    return http
      .get("/tms/filters/requests")
      .then((res) => {
        commit("REQUEST_TYPE_DATA_LIST", res.data);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  ///Action get Payroll period
  getListPayrollPeriodList({ commit }) {
    return http
      .get("/tms/filters/periods")
      .then((res) => {
        commit("PAYROLL_PERIOD_DATA_LIST", res.data.data);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  ////Action get location list
  getLocationList({ commit }) {
    return http
      .get("/tms/filters/location")
      .then((res) => {
        commit("LOCATION_DATA_LIST", res.data.data);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  ////Action get request status list
  getRequestStatusList({ commit }) {
    return http
      .get("/tms/filters/request-status")
      .then((res) => {
        commit("REQUEST_STATUS_DATA_LIST", res.data);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // ////Action get delegation status list
  // getDelegationStatusList({ commit }) {
  // 	return http
  // 		.get("/tms/filters/delegation-status")
  // 		.then((res) => {
  // 			commit("DELEGATION_STATUS_DATA_LIST", res.data);
  // 			return res.data
  // 		})
  // 		.catch((error) => {
  // 			throw (error)
  // 		});
  // },

  // ////Action get Abnormal status list
  // getAbnormalStatusList({ commit }) {
  // 	return http
  // 		.get("/tms/filters/abnormal-case-status")
  // 		.then((res) => {
  // 			commit("ABNORMAL_STATUS_DATA_LIST", res.data);
  // 			return res.data
  // 		})
  // 		.catch((error) => {
  // 			throw (error)
  // 		});
  // },

  ////Action get report status list
  getReportStatusList({ commit }) {
    return http
      .get("/tms/filters/report-status")
      .then((res) => {
        commit("REPORT_STATUS_DATA_LIST", res.data);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  ////Action get employee list
  getEmployeeList({ commit }, value) {
    return http
      .get("/tms/filters/employee?filter=" + value)
      .then((res) => {
        commit("EMPLOYEE_DATA_LIST", res.data.data);
        let data = [];
        data = res.data.data.slice(0, 20).map((item) => {
          return {
            value: item.user_name,
            label: item.name,
            department: item.department_name ? item.department_name.nameEn : "",
            id: item.id,
            sf4c_id: item.sf4c_id,
          };
        });
        return data;
      })
      .catch((error) => {
        throw error;
      });
  },

  getSameDUStaffList({ commit }, value) {
    return http
      .get("/tms/filters/employee-ot?username=" + value)
      .then((res) => {
        commit("SAME_DU_STAFF", res.data.data);
        let data = [];
        data = res.data.data.slice(0, 20).map((item) => {
          return {
            value: item.user_name,
            label: item.name,
            department: item.department_name?.nameEn || null,
            id: item.id,
          };
        });
        return data;
      })
      .catch((error) => {
        throw error;
      });
  },

  ////Action get mebmber list
  getMemberList({ commit }) {
    return http
      .get("/tms/filters/get-all-member-for-delegate")
      .then((res) => {
        commit("MEMBER_DATA_LIST", res.data.data);
        return res.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  ////Action get absent type list
  getAbsentList({ commit }) {
    return http
      .get("/tms/filters/absent")
      .then((res) => {
        commit("ABSENT_DATA_LIST", res.data.data);
        return res.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  ////Action get status list
  getStatus({ commit }) {
    return http
      .get("/status?offset=0&limit=1000")
      .then((res) => {
        commit("STATUS_LIST", res.data.data);
        return res.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  ////Action get reason type list
  getReasonTypeList({ commit }) {
    return http
      .get("/tms/filters/reason-types")
      .then((res) => {
        commit("REASON_TYPE_LIST", res.data.data);
        return res.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  ////Action get departments list
  getDepartmentList({ commit }) {
    return http
      .get("/department?offset=0&limit=1000")
      .then((res) => {
        commit("DEPARTMENT_LIST", res.data);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  ////Action get table list
  getTableList({ commit }) {
    return http
      .get("/tms/filters/table")
      .then((res) => {
        commit("TABLE_LIST", res.data.data);
        return res.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  getRoleList({ commit }) {
    return http
      .get("/tms/filters/role")
      .then((res) => {
        commit("ROLE_LIST", res.data.data);
      })
      .catch((error) => {
        throw error;
      });
  },
  ////Action get table list
  getReasonRequestList({ commit }) {
    return http
      .get("/tms/settings/work-types")
      .then((res) => {
        commit("REASON_LIST", res.data.data);
        return res.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  //Action get project list
  getProjectList({ commit }) {
    return http
      .get("project?offset=0&limit=1000")
      .then((res) => {
        commit("PROJECT_LIST", res.data);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  getGroupList({ commit }) {
    return http
        .get("manager-group?offset=0&limit=1000")
        .then((res) => {
          commit("GROUP_LIST", res.data);
          return res.data;
        })
        .catch((error) => {
          throw error;
        });
  },
  getOTTypeList({ commit }) {
    return http
      .get("tms/filters/ot-type")
      .then((res) => {
        commit("OT_TYPE", res.data.data);
        return res.data.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  getEndDateOfLastTimsheet({ commit }) {
    return http
      .get("/tms/timesheets/get-end-date-of-last-timesheet")
      .then((res) => {
        commit("LAST_TIMESHEET", res.data);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  checkDateInApprovedPeriod({ commit }, payload) {
    return http
      .get("/tms/timesheets/is-date-from-approved-timesheet" + payload)
      .then((res) => {
        commit("DATE_PERIOD", res.data);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  getRemainTypeLeave({ commit }) {
    return http
      .get("/tms/employee/leaving-type")
      .then((res) => {
        commit("REMAIN_TYPE_LEAVE_LIST", res.data);
        return res.data;
      })
      .catch((err) => {
        throw err;
      });
  },
  updateLeavingRemain({ commit }, payload) {
    return http
      .put(`/tms/employee/remain-leave/${payload.url}`, payload.data)
      .then((res) => {
        commit("UPDATE_LEAVING_REMAIN_DATA", res.data);
        return res.data;
      })
      .catch((err) => {
        throw err;
      });
  },
  //Action get skill list
  getSkillList({ commit }) {
    return http
      .get("skill-name?limit=2000&offset=0")
      .then((res) => {
        commit("SKILL_LIST", res.data);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  //Action get language list
  getLanguageList({ commit }) {
    return http
      .get("language?limit=1000&offset=0")
      .then((res) => {
        commit("LANGUAGE_LIST", res.data);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
