import http from "./helpers/httpInterceptor";

const state = () => ({
    jobPublicDetail: {
        code: '200',
    },
    categoryId: 0,
    listMoreJobDetail: [],
    urlPreview: "",
    createStatus: "None",
    assignStatus: "None",
    rejectStatus: "None",
    messageCreateStatusError: "",

})

const mutations = {
    SET_JOB_POSTING_DETAIL(state, data) {
        state.jobPublicDetail = data
        if (data.code !== '404') {
            state.jobPublicDetail.salary = data.salary
        }
    },
    SET_JOB_ID(state, data) {
        state.categoryId = data
    },
    SET_LIST_MORE_JOB(state, data) {
        state.listMoreJobDetail = data
    },
    SET_LINK_PREVIEW(state, url) {
        state.urlPreview = url;
    },
    CHANGE_CREATE_STATUS(state, status) {
        state.createStatus = status;
    },
    CHANGE_ASSIGN_STATUS(state, status) {
        state.assignStatus = status;
    },
    CHANGE_REJECT_STATUS(state, status) {
        state.rejectStatus = status;
    },
    CHANGE_MESSAGE_CREATE_STATUS(state, status) {
        state.messageCreateStatusError = status;
    }
}

const actions = {
    getJobPostingDetail({ commit }, jobId) {
        return http.get(`/jobPostingDetail?id=${jobId}`)
            .then(res => {
                if (res.data?.code === '404') {
                    commit('SET_JOB_POSTING_DETAIL', res.data)
                    return res.data
                } else {
                    commit("SET_JOB_POSTING_DETAIL", res.data);
                    commit("SET_JOB_ID", res.data.jobType.id);
                }
                return res.data;
            })
            .catch(error => {
                commit('SET_JOB_POSTING_DETAIL', null)
                commit("SET_JOB_ID", 0);
                throw error;
            })
    },

    getlistMoreJobDetail({ commit }, params) {
        return http.get('findJobPosting', { params })
            .then((res) => {
                commit('SET_LIST_MORE_JOB', res.data.content)
            })
            .catch((error) => {
                commit('SET_LIST_MORE_JOB', [])
            })
    },

    uploadFileCV({ commit }, payload) {
        let formData = new FormData();
        formData.append("file", payload.file.originFileObj);
        formData.append("fileType", "CV");
        return http
            .post("/upload", formData)
            .then((res) => {
                commit("SET_LINK_PREVIEW", res.data.previewUrl);
                return res.data;
            })
            .catch((error) => {
                throw error;
            });
    },

    setUrlPreview({ commit }, url) {
        commit("SET_LINK_PREVIEW", url);
    },

    candidateUploadCv({ commit }, payload) {
        return http.post("/createCV", payload)
            .then((res) => {
                commit("CHANGE_CREATE_STATUS", "Success");
                commit("CHANGE_MESSAGE_CREATE_STATUS", "");
                return res;
            })
            .catch((error) => {
                commit("CHANGE_CREATE_STATUS", "Error");
                commit("CHANGE_MESSAGE_CREATE_STATUS", "");
                if (typeof error === "object") {
                    commit("CHANGE_MESSAGE_CREATE_STATUS", error.message);
                } else {
                    if (error === "Email and phone number is existed!") {
                        commit("CHANGE_MESSAGE_CREATE_STATUS", "Your email and phone is existed!");
                    } else {
                        commit("CHANGE_MESSAGE_CREATE_STATUS", error.message);
                    }
                }
            })
    },

    candidateUpdateCv({ commit }, payload) {
        return http.put("/updateCV", payload)
            .then((res) => {
                commit("CHANGE_CREATE_STATUS", "Success");
                commit("CHANGE_MESSAGE_CREATE_STATUS", "");
                return res;
            })
            .catch((error) => {
                commit("CHANGE_CREATE_STATUS", "Error");
                commit("CHANGE_MESSAGE_CREATE_STATUS", "");
                if (typeof error === "object") {
                    commit("CHANGE_MESSAGE_CREATE_STATUS", error.message);
                } else {
                    if (error === "Email and phone number is existed!") {
                        commit("CHANGE_MESSAGE_CREATE_STATUS", "Your email and phone is existed!");
                    } else {
                        commit("CHANGE_MESSAGE_CREATE_STATUS", error.message);
                    }
                }
            })
    },

    candidateAssignCv({ commit }, payload) {
        return http.put("/updateCV", payload)
            .then((res) => {
                commit("CHANGE_ASSIGN_STATUS", "Success");
                commit("CHANGE_MESSAGE_CREATE_STATUS", "");
                return res;
            })
            .catch((error) => {
                commit("CHANGE_ASSIGN_STATUS", "Error");
                commit("CHANGE_MESSAGE_CREATE_STATUS", "");
            })
    },

    candidateRejectCv({ commit }, payload) {
        return http.put("/updateCV", payload)
            .then((res) => {
                commit("CHANGE_REJECT_STATUS", "Success");
                commit("CHANGE_MESSAGE_CREATE_STATUS", "");
                return res;
            })
            .catch((error) => {
                commit("CHANGE_REJECT_STATUS", "Error");
                commit("CHANGE_MESSAGE_CREATE_STATUS", "");
            })
    },

    changeCreateStatus({ commit }, status) {
        commit("CHANGE_CREATE_STATUS", status);
    },

    setMessageCreateStatus({ commit }, message) {
        commit("CHANGE_MESSAGE_CREATE_STATUS", message);
    }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations,
}


