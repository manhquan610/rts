import {
  tabs,
  tabsOffering,
  listTimeline,
  listComment,
  profiles,
  listInterview,
  information,
} from "./mockData/actitvityCandidate.json";
import http from "./helpers/httpInterceptor";

const getDefaultState = () => ({
  tabs: tabs,
  tabsOffer: tabsOffering,
  listComment: [],
  listTimeline: {},
  listInterview: [],
  profiles: [],
  information: {},
  listReact: [],
});
const state = getDefaultState();
const mutations = {
  SET_LIST_TIMELINE(state, data) {
    state.listTimeline = data;
  },
  SET_LIST_COMMENT(state, data) {
    state.listComment = data;
  },
  SET_LIST_PROFILE(state, data) {
    state.profiles = data;
  },
  SET_LIST_INTERVIEW(state, data) {
    state.listInterview = data;
  },
  SET_INFOMATION(state, data) {
    state.information = data;
  },
  SET_LIST_REACT(state, data) {
    state.listReact = data;
  },
  RESET_STATE(state) {
    Object.assign(state, getDefaultState());
  },
};

const actions = {
  getListComment({ commit }, payload) {
    if (!payload) return;
    const { candidateId, requestId } = payload;
    if (!candidateId || !requestId) return;
    return http
          .get(
            `/comment/candidate?candidateId=${candidateId}&limit=100&page=1&requestId=${requestId}`
          )
          .then((res) => {
            commit("SET_LIST_COMMENT", res.data.content.reverse());
            return res.data;
          })
          .catch((error) => console.log("error", error));
  },
  postComment({ commit }, data) {
    if(!data) return
    return http
          .post("/comment", data)
          .then((res) => {
            if (res.status === 201)
              this.$store.dispatch("rtsActivityCandidate/getListComment", data);
          })
          .catch((error) =>{

          });
  },
  actionLike({ commit },payload) {
    const {candidateID, liked} = payload
    return http
          .post(`/liked?candidateID=${candidateID}&liked=${liked}`)
          .then((res) => {
            if (res.status === 201)
              this.$store.dispatch("rtsActivityCandidate/getListReact", candidateID);
          })
          .catch((error) => console.log("error", error));
  },
  getListTimeline({ commit }, params) {
    if (!params) return
    const { candidateId, requestId } = params;
    if (!candidateId || !requestId) return;
    return http
      .get(`/timeline?candidateId=${candidateId}&requestId=${requestId}`)
      .then((res) => {
        commit("SET_LIST_TIMELINE", res.data);
      })
      .catch((error) => console.log("error", error));
  },
  getProfiles({ commit }) {
    let res = {
      data: profiles,
    };
    commit("SET_LIST_PROFILE", res.data);
  },
  getListInterview({ commit }) {
    let res = {
      data: listInterview,
    };
    commit("SET_LIST_INTERVIEW", res.data);
  },
  getInformation({ commit }, payload) {
    const candidateId = payload;
    if (!candidateId) {
      commit("SET_INFOMATION", {});
      return;
    }
    return http
        .get(`candidate/${candidateId}`)
        .then((res) => {
          commit("SET_INFOMATION", res.data);
        })
        .catch((error) => console.log("error", error));
  },
  getListReact({ commit }, payload) {
      const candidateId = payload;
      if (!candidateId) {
          commit("SET_LIST_REACT", {});
          return;
      }
      return http
          .get(`totalReact?candidateID=${candidateId}`)
          .then((res) => {
            commit("SET_LIST_REACT", res.data);
          })
          .catch((error) => console.log("error", error));
          }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
