import http from "./helpers/httpInterceptor";
import { listJob } from "./mockData/popularJob.json";
import {PAGE_OPTIONS_POPULARJOB} from "../../components/commons/commons";
const getDefaultState = () => {
  return {
    listPopularJob: [],
    pageOptions :{...PAGE_OPTIONS_POPULARJOB},
    total_items: 8
  };
};

const state = getDefaultState();

const mutations = {
  SET_LIST_POPULAR_JOB(state, data) {
    state.listPopularJob = data;
  },
  SET_PAGE_OPTIONS(state, data) {
    state.pageOptions = {}
    state.pageOptions = data
  },
  SET_TOTAL_POPULARJOB(state, data) {
    state.total_items = data
  },
};
const actions = {
  getListPopularJob({ commit }) {
    return http
      .get("/getJobCategories")
      .then((res) => {
        commit("SET_TOTAL_POPULARJOB", res.data.totalElements) 
        commit("SET_LIST_POPULAR_JOB", res.data.content);
      })
      .catch((error) => console.log("error", error));
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
