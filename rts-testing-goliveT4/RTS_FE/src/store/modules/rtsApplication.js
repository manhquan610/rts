import http from "./helpers/httpInterceptor";
import { convertObjectToParams } from "../../components/commons/commons";
import {
  HRs,
  InterviewInterviewers,
  InterviewLocations,
  InterviewStatuses,
  Statuses,
} from "./mockData/application.json";

const state = () => ({
  titleList: [],
  humanResourceList: [],
  skillList: [],
  languageList: [],
  groupList: [],
  departmentList: [],
  departmentAll: [],
  projectList: [],
  statusList: [],
  selectedRow: [],
  skillName: "",
  interviewLocationList: [],
  interviewInterviewerList: [],
  interviewStatusList: [],
  isPass: null,
});

// mutations
const mutations = {
  SET_SELECTED_ROW(state, data) {
    state.selectedRow = data;
  },
  SET_TITLE_LIST(state, data) {
    state.titleList = data;
  },
  SET_HUMAN_RESOURCE_LIST(state, data) {
    state.humanResourceList = data;
  },
  SET_GROUP_LIST(state, data) {
    state.groupList = data;
  },
  SET_DEPARTMENT_LIST_BY_GROUP(state, data) {
    let groupIds = data;
    if (!Array.isArray(groupIds)) return;
    let listChild = groupIds.reduce((total, item) => {
      let groupObj = state.groupList.find((x) => x.id === item);
      if (groupObj && groupObj.listChild) {
        return total.concat(groupObj.listChild);
      }
      return total.concat([]);
    }, []);
    state.departmentList = listChild;
  },
  SET_DEPARTMENT_ALL(state, data) {
    let groupIds = data;
    if (!Array.isArray(groupIds)) return;
    let listChild = groupIds.reduce((total, item) => {
      if (item.listChild) {
        return total.concat(item.listChild);
      }
      return total.concat([]);
    }, []);
    state.departmentAll = listChild;
  },
  SET_PROJECT_LIST(state, data) {
    state.projectList = data.map((item) => {
      item.name = item.projectName;
      return item;
    });
  },
  SET_STATUS_LIST(state, data) {
    state.statusList = data;
  },
  SET_SKILL_LIST(state, data) {
    state.skillList = data;
  },
  SET_LANGUAGE_LIST(state, data) {
    state.languageList = data;
  },
  // interview
  SET_INTERVIEW_LOCATION_LIST(state, data) {
    state.interviewLocationList = data;
  },
  SET_INTERVIEW_INTERVIEWER_LIST(state, data) {
    state.interviewInterviewerList = data;
  },
  SET_INTERVIEW_STATUS_LIST(state, data) {
    state.interviewStatusList = data;
  },
  SET_SKILL_NAME(state, data) {
    state.skillName = data;
  },
  SET_IS_PASS(state, payload) {
    state.isPass = payload;
  },
};
const actions = {
  ///Action get Request type
  getTitleList({ commit }, payload) {
    payload = {
      ...payload,
      offset: 1,
      limit: 10,
      like: "0",
    };
    const params = convertObjectToParams(payload);
    return http
      .get(`/request${params}`)
      .then((res) => {
        commit("SET_TITLE_LIST", res.data.content);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  getHumanResourceList({ commit }) {
    let res = {
      data: HRs,
    };
    commit("SET_HUMAN_RESOURCE_LIST", res.data);
  },
  //Action get skill list
  getSkillList({ commit }, payload) {
    const name = payload?.name;
    const params = {
      limit: 1000,
      offset: 1,
      name: name,
    };
    return http
      .get(`skill-name${convertObjectToParams(params)}`)
      .then((res) => {
        commit("SET_SKILL_LIST", res.data);
        commit("SET_SKILL_NAME", name);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  //Action get language list
  getLanguageList({ commit }) {
    return http
      .get("language?limit=1000&offset=0")
      .then((res) => {
        commit("SET_LANGUAGE_LIST", res.data);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  getGroupList({ commit }) {
    const today = new Date();
    const year = today.getFullYear();
    const month = today.getMonth() + 1;
    return (
      http
        .get("manager-group?offset=1&limit=1000")
        // .get(`departmentCMC?year=${year}&month=${month}`, {
        //   baseURL: "https://group-dashboard.cmcglobal.com.vn/api/",
        //   withCredentials: false,
        // })
        .then((res) => {
          let groupList = res.data.listChild;
          commit("SET_GROUP_LIST", groupList);
          commit("SET_DEPARTMENT_ALL", groupList);
          return res.data;
        })
        .catch((error) => {
          throw error;
        })
    );
  },
  getDepartmentListByGroup({ commit }, payload) {
    commit("SET_DEPARTMENT_LIST_BY_GROUP", payload);
  },
  getProjectList({ commit }) {
    return http
      .get("project/all", {
        baseURL: "https://backend-dashboard.cmcglobal.com.vn/api/",
        withCredentials: false,
      })
      .then((res) => {
        commit("SET_PROJECT_LIST", res.data);
        return res.data;
      })
      .catch((error) => {
        console.log(error);
      });
  },
  getStatusList({ commit }) {
    commit("SET_STATUS_LIST", Statuses);
  },
  getRequestNameList({ commit }, payload) {
    const { requestId } = payload;
    return http
      .get(`/interview-search?limit=1000&page=1&request_id=${requestId}`)
      .then((res) => {
        commit("SET_INTERVIEW_LOCATION_LIST", res.data?.content);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  getLocationList({ commit }) {
    return http
      .get(`/location-interview`)
      .then((res) => {
        commit("SET_INTERVIEW_LOCATION_LIST", res.data);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  getInterviewerList({ commit }, payload) {
    const { requestId } = payload;
    let res = {
      data: InterviewInterviewers,
    };
    commit("SET_INTERVIEW_INTERVIEWER_LIST", res.data);
  },
  getInterviewStatusList({ commit }) {
    let res = {
      data: InterviewStatuses,
    };
    commit("SET_INTERVIEW_STATUS_LIST", res.data);
  },
};

const getGroupList = (data) => {
  return data.listChild;
};
const getDepartMentList = (data) => {
  return data.listChild;
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
