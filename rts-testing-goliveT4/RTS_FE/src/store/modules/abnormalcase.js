// import axios from "axios";
// import config from "../../config";
import http from './helpers/httpInterceptor'
const state = () => ({
  abnormalList: [],
  editAbnormalData: {},
  listNotification: {},
  updateNotification: {},
  createExplanation: {},
  dataStatusUpdated: {},
  dataActual: [],
  updateJobAbnormal: []
});

// mutations
const mutations = {
  ABNORMAL_CASE_DATA(state, data) {
    state.abnormalList = data;
  },
  EDIT_ABNORMAL_DATA(state, data) {
    state.editAbnormalData = data;
  },
  GET_LIST_NOTIFICATION(state, data) {
    state.listNotification = data
  },
  READ_NOTIFICATION(state, data) {
    state.updateNotification = data
  },
  CREATE_ABNORMAL_DATA(state, data) {
    state.createExplanation = data
  },
  UPDATE_ABNORMAL_STATUS_DATA(state, data) {
    state.dataStatusUpdated = data
  },
  ACTUAL_SELECTIONS(state, data) {
    state.dataActual = data
  },
  UPDATE_ABNORMAL(state, data) {
    state.updateJobAbnormal = data
  }
};

// actions
const actions = {
  createExplanation({ commit }, payload) {
    return http.post(`/tms/request-management/create-explanation/${payload.id}`,payload.data).then(res => {
      commit('CREATE_ABNORMAL_DATA', res.data);
      return res.data;
    }).catch(error => {
      throw (error)
    })
  },
  getAbnormalCaseData({ commit }, url) {
    return http
      .get("/tms/abnormal-case/" + url)
      .then((res) => {
        commit("ABNORMAL_CASE_DATA", res.data);
        return res.data;
      })
      .catch((error) => {
        throw (error)
      });
  },
  updateAbnormalCaseData({ commit }, payload) {
    return http.put(`/tms/request-management/edit-explanation/${payload.id}`,payload.data).then(res => {
      commit('EDIT_ABNORMAL_DATA', res.data);
      return res.data;
    }).catch(error => {
      throw (error)
    })
  },
  getListNotification({ commit },) {
    commit('GET_LIST_NOTIFICATION', []);
    // return http.get("/tms/notifications?page=1&size=5")
    //   .then(res => {
    //     commit('GET_LIST_NOTIFICATION', res.data);
    //     return res.data;
    //   }).catch(error => {
    //     throw (error)
    //   })
  },
  readNotification({ commit }, payload) {
    commit('READ_NOTIFICATION', []);
    console.log(payload);
    // return http.put(`/tms/notifications/${payload.id}`)
    //   .then(res => {
    //     commit('READ_NOTIFICATION', res.data);
    //     return res.data;
    //   }).catch(error => {
    //     throw (error)
    //   })
  },
  getParamsDataToSelectActual({ commit }) {
    return http.get("/tms/abnormal-case/actual").then((res) => {
      commit("ACTUAL_SELECTIONS", res.data);
      return res.data;
    }).catch((error) => {
      throw (error)
    })
  },
  updateStatusAbnormal({ commit }, payload) {
    return http
      .put(`/tms/request-management/update-status-explanation/${payload.id}`, payload.data)
      .then((res) => {
        commit("UPDATE_ABNORMAL_STATUS_DATA", res.data);
        return res.data;
      })
      .catch((err) => {
        throw err;
      });
  },
  updateJobAbnormal({commit}, payload) {
    return http 
    .delete(`/tms/abnormal-case/rerun-abnormal-case?date=${payload.date}&employee=${payload.employee}`, payload)
    .then((res) => {
      commit('UPDATE_ABNORMAL', res.data)
      return res.data
    })
    .catch((err) => {
      throw(err)
    })
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
