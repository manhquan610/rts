import http from "./helpers/httpInterceptor";

const state = () => ({
  Request: [],
  requestList: [],
  dataRequestUpdated: [],
  dataStatusUpdated: [],
  dataAssignee: {},
  dateDateApprovedTimeSheet: {},
  dataRequestSelections: [],
  dataDurationTime: {},
  dataBatchRequest: {},
  dataDuplicate: {},
});

// mutations
const mutations = {
  REQUEST(state, data) {
    state.Request = data;
  },
  REQUEST_DATA_LIST(state, data) {
    state.requestList = data;
  },
  UPDATE_REQUEST(state, data) {
    state.dataRequestUpdated = data;
  },
  UPDATE_REQUEST_STATUS(state, data) {
    state.dataStatusUpdated = data;
  },
  UPDATE_REQUEST_ASSIGNEE_DATA(state, data) {
    state.dataAssignee = data;
  },
  CHECK_DATE_APPROVED_TIEMSHEET(state, data) {
    state.dateDateApprovedTimeSheet = data;
  },
  REQUEST_SELECTIONS(state, data) {
    state.dataRequestSelections = data;
  },
  GET_DURATION_TIME(state, data) {
    state.dataDurationTime = data;
  },
  BATCH_REQUEST(state, data) {
    state.dataBatchRequest = data;
  },
  DATA_DUPLICATE(state, data) {
    state.dataDuplicate = data;
  },
};

// actions
const actions = {
  getRequestData({ commit }, url) {
    return http
      .get("/tms/request-management" + url)
      .then((res) => {
        commit("REQUEST_DATA_LIST", res.data);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  getParamsDataToSelect({ commit }) {
    return http
      .get("/tms/request-management/create-request-selections")
      .then((res) => {
        commit("REQUEST_SELECTIONS", res.data);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  createRequest({ commit }, payload) {
    return http
      .post("/tms/request-management/create-request/", payload)
      .then((res) => {
        commit("REQUEST", res.data);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  createBatchRequest({ commit }, payload) {
    return http
      .post("/tms/request-management/batch-create-request/", payload)
      .then((res) => {
        commit("BATCH_REQUEST", res.data);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  updateRequestDelegate({ commit }, payload) {
    return http
      .put(`/tms/request-management/update-assignee`, payload)
      .then((res) => {
        commit("UPDATE_REQUEST_ASSIGNEE_DATA", res.data);
        return res.data;
      })
      .catch((err) => {
        throw err;
      });
  },
  updateRequest({ commit }, payload) {
    return http
      .put("/tms/request-management/edit-request", payload)
      .then((res) => {
        commit("UPDATE_REQUEST", res.data);
        return res.data;
      })
      .catch((err) => {
        throw err;
      });
  },
  updateStatusRequest({ commit }, payload) {
    return http
      .put("/tms/request-management/update-status", payload)
      .then((res) => {
        commit("UPDATE_REQUEST_STATUS", res.data);
        return res.data;
      })
      .catch((err) => {
        throw err;
      });
  },
  checkDateWithinApprovedTimesheet({ commit }, params) {
    return http
      .get(`tms/timesheets/is-date-from-approved-timesheet?date=${params}`)
      .then((res) => {
        commit("CHECK_DATE_APPROVED_TIEMSHEET", res.data);
        return res.data;
      })
      .catch((err) => {
        throw err;
      });
  },

  getDurationTime({ commit }, payload) {
    return http
      .post("/tms/request-management/time-and-duration", payload)
      .then((res) => {
        commit("GET_DURATION_TIME", res.data);
        return res.data;
      })
      .catch((err) => {
        throw err;
      });
  },
  updateDataDuplicate({ commit }, payload) {
    commit("DATA_DUPLICATE", payload);
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
