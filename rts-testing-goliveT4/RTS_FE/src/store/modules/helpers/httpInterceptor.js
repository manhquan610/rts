import axios from "axios";
import {CONSTANTS} from './constant';
axios.defaults.baseURL = process.env.VUE_APP_SERVICE_API;
console.log(
  "🚀 ~ file: httpInterceptor.js ~ line 4 ~ process.env.VUE_APP_SERVICE_API",
  process.env.VUE_APP_SERVICE_API
);

let http = axios.create({
    baseURL: process.env.VUE_APP_SERVICE_API,
    headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "*",
        "Access-Control-Allow-Headers": "*"
    },
    withCredentials: true
});

http.interceptors.request.use(config => {
    let userInfo = localStorage.getItem('session_login') ? JSON.parse(localStorage.getItem('session_login')) : {};
    let token = userInfo.token;
    if (token !== null && token !== undefined) {
        config.headers["Authorization"] = "Bearer " + token;
    }

    return config;
}, function (error) {
    return Promise.reject(error)
});

http.interceptors.response.use(response => {
    const {data} = response;
    // if data.code == 333 || 555 || 777 => logout
    if (data.code === CONSTANTS.TOKEN_EXPIRED || data.code === CONSTANTS.TOKEN_INCORRECT || data.code === CONSTANTS.TOKEN_NOT_EXIST) {
        localStorage.removeItem('session_login');
        window.location.href = '/';
    }

    return response
}, error => {
    let userInfo = JSON.parse(localStorage.getItem('session_login'));
    if (userInfo) {
        if (error.response.status === 401) {
            localStorage.removeItem('session_login');
            window.location.replace('/login')
        }
    }
    return Promise.reject(error.response.data)
})

export default http;
