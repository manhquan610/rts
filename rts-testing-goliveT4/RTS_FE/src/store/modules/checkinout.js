
import http from './helpers/httpInterceptor'
const state = () => ({
	checkInOutList: {},
	checkInOutDetailByDay: {},
	checkInOutManualJobList: {},
	updateJobCheckInOut: {},
});

// mutations
const mutations = {
	CHECK_IN_OUT(state, data) {
		state.checkInOutData = data;
	},
	CHECK_IN_OUT_DETAIL_BY_DAY(state, data) {
		state.checkInOutDetailByDay = data;
	},

	CHECK_IN_OUT_MANUAL_JOB(state, data) {
		state.checkInOutManualJobList = data;
	},

	UPDATE_JOB_CHECK_IN_OUT(state, data) {
		state.updateJobCheckInOut = data
	}
};

// actions
const actions = {
	getCheckInOutList({ commit }, url) {
		return http
			.get("/tms/check-in-out" + url)
			.then((res) => {
				commit("CHECK_IN_OUT", res.data);
				return res.data;
			})
			.catch((error) => {
				throw (error)
			});
	},

	getCheckInOutDetailByDay({ commit }, url) {
		return http
			.get("/tms/check-in-out/" + url)
			.then((res) => {
				commit("CHECK_IN_OUT_DETAIL_BY_DAY", res.data);
				return res.data;
			})
			.catch((error) => {
				throw (error)
			});
	},

	getDataManualJob({ commit }, payload) {
		return http
			.put("/tms/check-in-out/manual", payload)
			.then((res) => {
				commit("CHECK_IN_OUT_MANUAL_JOB", res.data);
				return res.data;
			})
			.catch((error) => {
				throw (error)
			});
	},

	updateJobCheckInOut({ commit }, payload){
		return http 
		.put("/tms/check-in-out/manual", payload)
		.then((res) => {
			commit('UPDATE_JOB_CHECK_IN_OUT',res)
			return res
		}).catch((error) => {
			throw (error)
		})				
	}
};

export default {
	namespaced: true,
	state,
	actions,
	mutations
};
