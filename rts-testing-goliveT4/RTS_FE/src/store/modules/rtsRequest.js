import { requests } from "./mockData/requests.json";
import http from "./helpers/httpInterceptor";
import {
  capitalizeFirstLetter,
  convertMillisecondsToDate,
  convertObjectToParams,
  convertUnixToDate,
  findObjectById,
} from "../../components/commons/commons";
import { configPanigationDefault, UPDATED_STATUS } from "../../components/commons/commonConstant";
import { approve } from "./mockData/listPeople.json";
const state = () => ({
  requestList: [],
  pages: {
    currentPage: 1,
    pageSize: localStorage.getItem("PayloadRequest")
      ? JSON.parse(localStorage.getItem("PayloadRequest")).limit
        ? Number(JSON.parse(localStorage.getItem("PayloadRequest")).limit)
        : configPanigationDefault.pageSize
      : configPanigationDefault.pageSize,
    total_items: requests.length,
  },
  approveList: {},
  assigneeList: [],
  assigneeTALeadList: [],
  assigneeListTA: [],
  idList: [],
});

const mutations = {
  SET_RTS_REQUEST_LIST(state, data) {
    let { requests, groups, departments, projects } = data;
    let res = [];
    if (requests) {
      res = requests.map((element) => formatRequest(element, groups, departments, projects));
    }

    state.requestList = res;
  },
  SET_ID_LIST(state, data) {
    state.idList = data;
  },
  SET_APPROVE_LIST(state, data) {
    state.approveList = data;
  },
  SET_ASSIGNEE_LIST(state, data) {
    state.assigneeList = data;
  },
  SET_ASSIGNEE_TALEAD_LIST(state, data) {
    state.assigneeTALeadList = data;
  },
  SET_ASSIGNEE_LIST_TA(state, data) {
    state.assigneeListTA = data;
  },
  SET_PAGE(state, data) {
    const { offset, limit, total_items } = data;
    if (offset || offset === 0) {
      state.pages.currentPage = offset;
    }
    if (limit || limit === 0) {
      state.pages.pageSize = limit;
    }
    if (total_items || total_items === 0) {
      state.pages.total_items = total_items;
    }
  },
  CREATE_REQUEST(state, data) {
    state.requestList.unshift(data);
  },
  UPDATE_REQUEST(state, data) {
    const request = data;
    let requests = [...state.requestList];
    requests = requests.map(function (item) {
      if (item.id === request.id) return item;
    });
    state.requestList = requests;
  },
  UPDATE_REQUEST_BY_STATUS(state, data) {
    const { ids, action } = data;
    if (!Array.isArray(ids)) return;
    const requests = [...state.requestList];
    requests.forEach((r) => {
      if (ids.includes(r.id)) {
        r.status = UPDATED_STATUS[action];
      }
    });
    state.requestList = requests;
  },
  UPDATE_REQUEST_BY_ASSIGN(state, data) {
    let { requestId, recruiters } = data;
    recruiters = recruiters.map((item) => {
      return {
        ...item,
        assignName: item.assignLdap,
      };
    });
    const requests = [...state.requestList];
    requests.forEach((req) => {
      if (req.id === requestId) {
        req.recruiters = recruiters;
      }
    });
    state.requestList = requests;
  },
};

const actions = {
  getRtsRequestList({ commit, rootState }, payload) {
    const { offset, limit } = payload;
    const params = convertObjectToParams(payload);
    console.log("🚀 ~ file: rtsRequest.js ~ line 111 ~ getRtsRequestList ~ params", params);
    return http
      .get(`/request${params}`)
      .then((res) => {
        commit("SET_PAGE", {
          offset: offset,
          limit: limit,
        });
        commit("SET_RTS_REQUEST_LIST", {
          requests: res.data.content,
          groups: rootState.rtsApplication.groupList,
          departments: rootState.rtsApplication.departmentAll,
          projects: rootState.rtsApplication.projectList,
        });

        commit("SET_PAGE", {
          total_items: res.data.totalElements,
        });
        return res.data.content;
      })
      .catch((error) => {
        throw error;
      });
  },
  getIdList({ commit }, payload) {
    return http
      .get(`request/getId?id=${payload}`)
      .then((res) => {
        commit("SET_ID_LIST", res.data);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  getApproveList({ commit }, payload) {
    //commit('SET_APPROVE_LIST', approve)
    return http
      .get(`/get-glead?username=${payload}`)
      .then((res) => {
        commit("SET_APPROVE_LIST", res.data);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  getAssigneeLeadList({ commit }) {
    return http
      .get(`/hr-member-lead`)
      .then((res) => {
        commit("SET_ASSIGNEE_LIST", res.data.item);
        return res.data.item;
      })
      .catch((error) => {
        throw error;
      });
  },
  getAssigneeList({ commit }) {
    return http
      .get(`/hr-member-lead`)
      .then((res) => {
        commit("SET_ASSIGNEE_LIST", res.data.item);
        return res.data.item;
      })
      .catch((error) => {
        throw error;
      });
  },
  getAssigneeTALeadList({ commit }) {
    return http
      .get(`/hr-lead`)
      .then((res) => {
        commit("SET_ASSIGNEE_LIST", res.data.item);
        return res.data.item;
      })
      .catch((error) => {
        throw error;
      });
  },
  getAssigneeListByTA({ commit }, id) {
    return http
      .get(`/get-ta/${id}?id=${id}`)
      .then((res) => {
        commit("SET_ASSIGNEE_LIST_TA", res.data);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  // getAssigneeTALeadList({ commit }, id) {
  //   return http
  //     .get(`/get-ta/${id}?id=${id}`)
  //     .then((res) => {
  //       commit("SET_ASSIGNEE_TALEAD_LIST", res.data);
  //       return res.data;
  //     })
  //     .catch((error) => {
  //       throw error;
  //     });
  // },
  getRtsRequestById({ commit }, id) {
    return http
      .get(`/request/${id}`)
      .then((res) => {
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  createRtsRequest({ commit }, payload) {
    return http
      .post("/request/", payload)
      .then((res) => {
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  updateRtsRequest({ commit }, payload) {
    let id = payload.id;
    delete payload.id;
    return http
      .post("/request/" + id, payload)
      .then((res) => {
        commit("UPDATE_REQUEST", res.data);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  assignRtsRequest({ commit }, payload) {
    let { requestId, recruiters } = payload;
    recruiters = removeEmptyElement(recruiters);
    return http
      .post(`/request/${requestId.toString()}/assign`, recruiters)
      .then((res) => {
        commit("UPDATE_REQUEST_BY_STATUS", {
          ids: [requestId],
          action: "assign",
        });
        commit("UPDATE_REQUEST_BY_ASSIGN", {
          requestId: requestId,
          recruiters: res.data,
        });
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  assignTALeadRtsRequest({ commit }, payload) {
    const { createdBy, requestId } = payload;
    return http
      .post(`/request-change-created-by/${requestId}`, { createdBy })
      .then((res) => {
        // commit("UPDATE_REQUEST_BY_STATUS", {
        //   ids: [requestId],
        //   action: "assign",
        // });
        // commit("UPDATE_REQUEST_BY_ASSIGN", {
        //   requestId: requestId,
        //   recruiters: res.data,
        // });
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  approveRtsRequest({ commit }, payload) {
    const { ids } = payload;
    return http
      .get(`/request-approve/${ids.toString()}`)
      .then((res) => {
        commit("UPDATE_REQUEST_BY_STATUS", {
          ids: ids,
          action: "approve",
        });
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  approve2RtsRequest({ commit }, payload) {
    const { ids } = payload;
    return http
      .get(`/request-approve2/${ids.toString()}`)
      .then((res) => {
        commit("UPDATE_REQUEST_BY_STATUS", {
          ids: ids,
          action: "approve2",
        });
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  updateAssignRtsRequest({ commit }, payload) {
    return http
      .post(`/reassigned`, payload)
      .then((res) => {
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  rejectRtsRequest({ commit }, payload) {
    const { ids, additionalData } = payload;
    return http
      .get(`/request-reject/${ids.toString()}?reason=${additionalData.toString()}`)
      .then((res) => {
        commit("UPDATE_REQUEST_BY_STATUS", {
          ids: ids,
          action: "reject",
        });
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  submitRtsRequest({ commit }, payload) {
    const { ids, additionalData } = payload;
    return http
      .get(`/request-submit/${ids.toString()}?receiver=${additionalData}`)
      .then((res) => {
        commit("UPDATE_REQUEST_BY_STATUS", {
          ids: ids,
          action: "submit",
        });
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  closeRtsRequest({ commit }, payload) {
    const { ids } = payload;
    return http
      .get(`/request-close/${ids.toString()}`)
      .then((res) => {
        commit("UPDATE_REQUEST_BY_STATUS", {
          ids: ids,
          action: "close",
        });
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  shareRtsRequest({ commit }, payload) {
    const { ids } = payload;
    return http
      .put(`/request/${ids.toString()}`)
      .then((res) => {
        commit("UPDATE_REQUEST_BY_STATUS", {
          ids: ids,
          action: "share",
        });
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
};

function formatRequest(request, groups, departments, projects) {
  let res = {
    id: request.id,
    title: request.name,
    status: capitalizeFirstLetter(request.requestStatusName),
    position: request.managerPositionName,
    deadline: convertMillisecondsToDate(request.deadline),
    number: request.number,
    project: findObjectById(request.projectId, "projectId", projects)?.projectName,
    // department: findObjectById(request.departmentId, "id", departments)?.name,
    department: request.department,
    division: request.division,
    group: findObjectById(request.managerGroupId, "id", groups)?.name,
    groupUser: request.managerGroup?.userManager,
    recruiters: request.requestAssigns,
    area: request.area.name,
    priority: request.priority.name,
    jobType: request.jobType.name,
    jobLevel: request.managerJobLevels,
    workingTime: request.workingType,
    codePosition: request.codePosition,
    managerJobLevel: request.managerJobLevel,
    createdBy: request.createdBy,
    applicantsCurrent: request.totalCurrentApplicant,
    applicants: request.totalApplicant,
    onboardingCurrent: request.totalCurrentOnboarding,
    onboarding: request.totalOnboarding,
    language: request.language,
  };
  return res;
}

function removeEmptyElement(arr) {
  let temp = [];
  for (let i of arr) i && temp.push(i);
  return temp;
}
export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
