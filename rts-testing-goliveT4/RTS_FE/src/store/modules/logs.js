import helpers from './helpers/httpInterceptor';

const logs = {
	state: () => ({
		logsListData: {},
		connectionHistoryData: {}

	}),
	mutations: {
		LOGS_LIST_DATA(state, payload) {
			state.logsListData = payload;
		},
		CONNECTION_HISTORY_DATA(state, payload) {
			state.connectionHistoryData = payload;
		}
	},
	actions: {
		getLogsListData({ commit }, url) {
			return helpers
				.get(`/tms/logs/log-lists` + url)
				.then((res) => {
					commit("LOGS_LIST_DATA", res.data);
					return res.data;
				})
				.catch((err) => {
					throw (err)
				})
		},
		getConnectionHistoryData({ commit }, url) {
			return helpers
				.get(`/tms/logs/connection-histories` + url)
				.then((res) => {
					commit("CONNECTION_HISTORY_DATA", res.data);
					return res.data;
				}).catch((err) => {
					throw (err)
				})
		}
	},
	getters: {},
};
export default logs;
