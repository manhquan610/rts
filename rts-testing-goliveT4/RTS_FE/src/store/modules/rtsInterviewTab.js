import { tabs } from './mockData/activityInterview.json';
import http from "./helpers/httpInterceptor";
import moment from "moment";
import {
    convertMillisecondsToDate,
    convertMillisecondsToHour,
    convertObjectToParams
} from "../../components/commons/commons";

const getDefaultState = () => ({
    tabs : tabs,
    isShowInterview: true,
    keyTab: 1,
    listInterviewer: [],
    interviewDetail: {},
    listAllInterview: {
        data:[],
        listQueryInterview:[]
    },
    saveDatePayload:{
        startDate:null,
        endDate:null
    }
})

const state = getDefaultState();
const mutations = {
    RESET_STATE(state){
        Object.assign(state, getDefaultState())
    },
    SET_INTERVIEW(state) {
        return state.isShowInterview =!state.isShowInterview;
    },
    SET_KEY_TAB(state, data){
        return state.keyTab = data;
    },
    SET_LIST_INTERVIEWER(state, data) {
        return state.listInterviewer = data
    },
    SET_PAYLOAD_DATE(state, payload) {
       return state.saveDatePayload = payload;
    },
    SET_LIST_ALL_INTERVIEW(state, data) {
        if(data.status !== 404){
            const newData = data.map((item,index) => {
                return {
                    id:index + 1,
                    text: item.title,
                    startDate: new Date(item.startTime),
                    endDate: moment(item.startTime).add('60','m')._d,
                    requestId: item.requestId,
                    candidateId: item.candidateId,
                    createdBy: item.createBy,
                    name: item.name
                }
            })
            const listQuery = data?.map((item,index) => {
                return {
                    id:index + 1,
                    text: item.title
                }
            })
            state.listAllInterview = {
                data:newData,
                listQueryInterview:listQuery
            }
        }

    },
    //Get detail interview and format date time format UTC 
    SET_INTERVIEW_DETAIL(state,data){
        if(!data) return;
        let  startTime  = data.startTime;
        data.date = convertMillisecondsToDate(startTime)
        data.time = convertMillisecondsToHour(startTime)
        return state.interviewDetail = data
    },
    RESET_INTERVIEW_DETAIL(state){
        state.interviewDetail = null
    },
}
const actions = {
  createInterview({ commit }, payload){
      return http
          .post("/createInterview", payload)
          .then((res) => {
              return res.data;
          })
          .catch((error) => {
              throw error;
          });
    },
    getAllInterview({commit}, payload){
        const { startDate, endDate, text_search } = payload;
        return http
            .get(`/interviewer/getInterview/timeline?endDate=${endDate}&startDate=${startDate}&text_search=${text_search ? text_search : ''}`)
            .then((res) => {
                commit('SET_LIST_ALL_INTERVIEW',res.data)
                return res.data;
            })
            .catch((error) => {
                throw error;
            });
    },
    getInterviewer({ commit },payload) {
        const searchValue = payload
        return http
            .get(`/interviewer?name=${searchValue}`)
            .then((res) => {
                let arr = res.data.items.map(function(item){
                    const { fullName: name, ...rest } = item;
                    return {name, ...rest}
                })
                commit("SET_LIST_INTERVIEWER", arr);
                return arr;
            })
            .catch((error) => {
                throw error;
            });
    },
    toggleInterview({ commit }) {
          commit("SET_INTERVIEW")
          commit("SET_KEY_TAB")
      },
    getInterviewDetail({ commit }, payload) {
      const {candidateId, requestId} = payload
      if(!candidateId || !requestId ) return
      return http.get(`interview/get?candidate_id=${candidateId}&request_id=${requestId}`)
                 .then(res => {
                      commit("SET_INTERVIEW_DETAIL", res.data[res.data.length-1]);
                      return res.data
                 })
                  .catch(error => console.log('error', error))
    }
}
export default {
    namespaced: true,
    state,
    actions,
    mutations,
  };