import http from './helpers/httpInterceptor'
const state = () => ({
  DataReportDetail: [],
  DataReportSummary: [],
  dataCheckIOReport: [],
  ExportReport: [],
});

const mutations = {
  REPORT_DETAIL_DATA(state, payload) {
    state.DataReportDetail = payload;
  },
  REPORT_SUMMARY_DATA(state, payload) {
    state.DataReportSummary = payload;
  },
  REPORT_CHECKIO_DATA(state, payload) {
    state.dataCheckIOReport = payload;
  },
  DOWNLOAD_EXCEL_DATA(state, payload) {
    state.ExportReport = payload;
  }
};
const actions = {
  getDataReportDetail({ commit }, url) {
    return http
      .get("/tms/report/detail" + url)
      .then((res) => {
        commit("REPORT_DETAIL_DATA", res.data);
        return res.data;
      })
      .catch((err) => {
        throw err
      });
  },

  getDataReportSummary({ commit }, url) {
    return http
      .get("/tms/report/summary" + url)
      .then((res) => {
        commit("REPORT_SUMMARY_DATA", res.data);
        return res.data;
      })
      .catch((err) => {
        throw err;
      });
  },

  getDataCheckIOReport({ commit }, url) {
    return http
      .get("tms/report/check-time" + url)
      .then(res => {
        commit("REPORT_CHECKIO_DATA", res.data);
        return res.data
      })
      .catch(err => {
        throw (err)
      })
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
