import { interviews } from "./mockData/interviews.json";
import http from "./helpers/httpInterceptor";
import {capitalizeFirstLetter, convertMillisecondsToDate, convertObjectToParams} from "../../components/commons/commons";

const state = () => ({
    interviewList: [],
    pages: {
        currentPage: 1,
        pageSize: 10,
        total_items: 0,
    },
});

// mutations
const mutations = {
    SET_INTERVIEW_LIST(state, data) {
        let newData = [];
        if (data) {
            newData = data.map((element) => (formatInterview(element)));
        }
        state.interviewList = newData;
    },
    SET_INTERVIEW_PAGE(state, data) {
        state.pages = data;
    },
};
const actions = {

    ///Action get Request type
    getInterviewList({commit}, payload) {
        if(!payload){
            return
        }
        const { page, limit } = payload
        return http
            .get(`/interview-search${convertObjectToParams(payload)}`)
            .then((res) => {
                commit("SET_INTERVIEW_LIST", res.data?.content);
                commit("SET_INTERVIEW_PAGE", {
                    currentPage: page,
                    pageSize: limit,
                    total_items: res.data?.totalElements,
                });
                return res.data;
            })
            .catch((error) => {
                throw error;
            });
    },
}
function formatInterview(interview) {
    return {
        ...interview,
        startTime: convertMillisecondsToDate(interview.startTime),
        endTime: convertMillisecondsToDate(interview.endTime),
        status: interview.status? "new" : "Onboard"
    }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations,
};
