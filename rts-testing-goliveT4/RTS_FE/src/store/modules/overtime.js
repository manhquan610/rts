import http from './helpers/httpInterceptor'

const state = () => ({
    OTRequest: [],
    dataOTRequestList: [],
    dataStatusUpdated: [],
    dataOTUpdated: [],
    dataAssignee: {},
    dataComment: [],
});

// mutations
const mutations = {
    OT_REQUEST(state, data) {
        state.OTRequest = data;
    },
    OVERTIME_REQUEST_LIST(state, data) {
        state.dataOTRequestList = data
    },
    UPDATE_OT_REQUEST_STATUS_DATA(state, data) {
        state.dataStatusUpdated = data
    },
    UPDATE_OT_REQUEST_DATA(state, data) {
        state.dataOTUpdated = data
    },
    UPDATE_OT_ASSIGNEE_DATA(state, data) {
        state.dataAssignee = data
    },
    UPDATE_OT_COMMENT_DATA(state,data){
        state.dataComment = data
    }
};

// actions
const actions = {
    createOTRequest({ commit }, payload) {
        return http
            .post("/tms/overtime-request/", payload)
            .then((res) => {
                commit("OT_REQUEST", res.data);
                return res.data;
            })
            .catch((error) => {
                throw (error)
            });
    },

    getOTRequestList({ commit }, payload) {
        return http
            .get("/tms/overtime-request" + payload)
            .then((res) => {
                commit("OVERTIME_REQUEST_LIST", res.data);
                return res.data;
            })
            .catch((error) => {
                throw (error)
            });
    },

    updateOTRequestData({ commit }, payload) {
        return http
            .put('/tms/overtime-request', payload)
            .then(res => {
                commit("UPDATE_OT_REQUEST_DATA", res.data);
                return res.data
            })
            .catch(err => {
                throw err
            })
    },

    updateStatusOTRequest({ commit }, payload) {
        return http
            .put(`/tms/overtime-request/${payload.otRequestId}/update-status`, payload.data)
            .then((res) => {
                commit("UPDATE_OT_REQUEST_STATUS_DATA", res.data);
                return res.data;
            })
            .catch((err) => {
                throw err;
            });
    },
    updateAssigneeOTRequest({ commit }, payload) {
        return http
            .put(`/tms/overtime-request/${payload.otRequestId}/update-assignee`, payload.data)
            .then((res) => {
                commit("UPDATE_OT_ASSIGNEE_DATA", res.data);
                return res.data;
            })
            .catch((err) => {
                throw err;
            });
    },
    updateComment({commit},payload){
        return http
        .put(`/tms/overtime-request/${payload.requestId}/${payload.receiver_id}/comment`, payload.data)
        .then((res) => {
            commit("UPDATE_OT_COMMENT_DATA", res.data);
            return res.data;
        })
        .catch((err) => {
            throw err;
        });
    }
};

export default {
    namespaced: true,
    state,
    actions,
    mutations
};