import http from './helpers/httpInterceptor'

const state = () => ({
    delegationList: [],
    delegationManagementDetail: [],
    updateStaffList: {},
    deleteDelegation: {},
    createDelegation: {},
    delegationExplanationRequest: [],
    listMemberOfCreate: [],
    chooseDelegator: [],
    editDelegator: [],
    createOrUpdateDelegation: {},
    chooseDelegatorToChange: {},
    delegationMemberList: [],
    chooseDelegatorPageable: [],
    getListMemberCreatePageable: [],
});

//mutations
const mutations = {
    GET_DELEGATION_LIST(state, payload) {
        state.delegationList = payload;
    },
    GET_DELEGATION_MANAGEMENT_DETAIL(state, payload) {
        state.delegationManagementDetail = payload;
    },
    UPDATE_STAFF_LIST(state, data) {
        state.updateStaffList = data;
    },
    DELETE_DELEGATION(state, payload) {
        state.deleteDelegation = payload;
    },
    CREATE_DELEGATION(state, payload) {
        state.createDelegation = payload;
    },
    GET_DELEGATION_EXPLANATION_REQUEST(state, payload) {
        state.delegationExplanationRequest = payload;
    },
    GET_LIST_MEMBER_OF_CREATE(state, payload) {
        state.listMemberOfCreate = payload;
    },
    CHOOSE_DELEGATOR(state, payload) {
        state.chooseDelegator = payload;
    },
    EDIT_DELEGATOR(state, payload) {
        state.editDelegator = payload;
    },
    CREATE_OR_UPDATE_DELEGATION(state, payload) {
        state.createOrUpdateDelegation = payload;
    },
    CHOOSE_DELEGATOR_TO_CHANGE(state, payload) {
        state.chooseDelegatorToChange = payload;
    },
    DELEGATION_SEARCH_MEMBER_LIST(state, payload) {
        state.delegationMemberList = payload;
    },
    CHOOSE_DELEGATOR_PAGEABLE(state, payload) {
        state.chooseDelegatorPageable = payload;
    },
    GET_LIST_MEMBER_OF_CREATE_PAGEABLE(state, payload) {
        state.getListMemberCreatePageable = payload;
    }
}

// actions
const actions = {
    getDelegationList({ commit }, url) {
        return http.get("/tms/delegate/get-all-delegate" + url).then(res => {
            commit('GET_DELEGATION_LIST', res.data);
            return res.data;
        }).catch(error => {
            throw (error)
        })
    },
    getDelegationManagementDetail({ commit }, url) {
        return http.get("/tms/delegate/get-detail-delegate" + url).then(res => {
            commit('GET_DELEGATION_MANAGEMENT_DETAIL', res.data);
            return res.data;
        }).catch(error => {
            throw (error)
        })
    },

    getDelegationExplanationRequest({ commit }, url) {
        return http.get("/tms/delegate/delegation-explanation" + url).then(res => {
            commit('GET_DELEGATION_EXPLANATION_REQUEST', res.data);
            return res.data;
        }).catch(error => {
            throw (error)
        })
    },

    getListMemberOfCreate({ commit }, url) {
        return http.get("/tms/delegate/choose-member-for-delegator" + url).then(res => {
            commit('GET_LIST_MEMBER_OF_CREATE', res.data);
            return res.data;
        }).catch(error => {
            throw (error)
        })
    },
    getListMemberOfCreatePageAble({ commit }, url) {
        return http.get("/tms/delegate/search-member-for-delegator" + url).then(res => {
            commit('GET_LIST_MEMBER_OF_CREATE_PAGEABLE', res.data);
            return res.data;
        }).catch(error => {
            throw (error)
        })
    },
    deleteDelegation({ commit }, listId) {
        return http.put(`/tms/delegate/delete-delegator`, listId).then((res) => {
            commit("DELETE_DELEGATION", res.data);
            return res.data;
        })
            .catch((err) => {
                throw (err)
            })
    },
    chooseDelegator({ commit }, url) {
        return http.get(`/tms/delegate/choose-delegator${url ? url : ''}`).then(res => {
            commit('CHOOSE_DELEGATOR', res.data);
            return res.data;
        }).catch(error => {
            throw (error)
        })
    },
    searchDelegator({ commit }, url) {
        return http.get(`/tms/delegate/search-delegator${url ? url : ''}`).then(res => {
            commit('CHOOSE_DELEGATOR', res.data);
            return res.data;
        }).catch(error => {
            throw (error)
        })
    },
    chooseDelegatorPageable({ commit }, url) {
        return http.get(`/tms/delegate/choose-delegator-pageable${url ? url : ''}`).then(res => {
            commit('CHOOSE_DELEGATOR_PAGEABLE', res.data);
            return res.data;
        }).catch(error => {
            throw (error)
        })
    },
    createOrUpdateDelegation({ commit }, data) {
        return http.put(data.url, data.params)
            .then((res) => {
                commit("CREATE_OR_UPDATE_DELEGATION", res.data);
                return res.data;
            })
            .catch((err) => {
                throw (err)
            })
    },
    chooseDelegatorToChange({ commit }, url) {
        return http.get(`/tms/delegate/choose-delegator-to-delegate${url}`).then(res => {
            commit('CHOOSE_DELEGATOR_TO_CHANGE', res.data);
            return res.data;
        }).catch(error => {
            throw (error)
        })
    },
    delegationSearchMemberList({ commit }, url) {
        return http.get(`/tms/delegate/get-member-delegator${url}`).then(res => {
            commit('DELEGATION_SEARCH_MEMBER_LIST', res.data);
            return res.data;
        }).catch(error => {
            throw (error)
        })
    },
};

export default {
    namespaced: true,
    state,
    actions,
    mutations
};
