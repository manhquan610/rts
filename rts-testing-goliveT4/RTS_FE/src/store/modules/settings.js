import http from './helpers/httpInterceptor'

const state = () => ({
  workTypeList: [],
  editWorkType: {},
  deleteWorkType: {},
  addNewWorkType: {},
  payrollPeriodList: [],
  // editPayrollPeriod: {},
  // deletePayrollPeriod: {},
  // addNewPayrollPeriod: {},
});

// mutations
const mutations = {
  GET_WORK_TYPE_LIST(state, data) {
    state.workTypeList = data;
  },
  GET_PAYROLL_PERIOD_LIST(state, data) {
    state.payrollPeriodList = data;
  },
};

// actions
const actions = {
  getWorkTypeList({ commit }, url) {
    return http.get(`/tms/settings/get-work-types${url}`).then(res => {
      commit('GET_WORK_TYPE_LIST', res.data);
      return res.data;
    }).catch(error => {
      throw (error)
    })
  },
  addNewWorkType({ commit }, payload) {
    return http.post(`/tms/settings/create`, payload).then(res => {
      commit('ADD_NEW_WORK_TYPE', res.data);
      return res.data;
    }).catch(error => {
      throw (error)
    })
  },
  getPayrollPeriodList({ commit }, url) {
    return http.get(`/tms/settings/work-periods${url}`).then(res => {
      commit('GET_PAYROLL_PERIOD_LIST', res.data);
      return res.data;
    }).catch(error => {
      throw (error)
    })
  },

};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
