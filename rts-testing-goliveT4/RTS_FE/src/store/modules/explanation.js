import http from "./helpers/httpInterceptor";

const state = () => ({
  explanationData: [],
  pagination: {},
  dataUpdated: {},
  statusUpdated: {},
  delegatorList: [],
});

// mutations
const mutations = {
  EXPLANATION_MANAGE_DRAWER_DATA(state, payload) {
    state.explanationData = payload;
  },
  UPDATE_EXPLANATION_DATA(state, payload) {
    state.dataUpdated = payload;
  },
  UPDATE_STATUS_EXPLANTION(state, payload) {
    state.statusUpdated = payload
  },
  GET_DELEGATOR_LIST(state, payload) {
    state.getDelegatorList = payload
  }
};
// actions
const actions = {
  getExplanationDrawerData({ commit }, url) {
    return http
      .get("/tms/explanation-management" + url)
      .then((res) => {
        commit("EXPLANATION_MANAGE_DRAWER_DATA", res.data);
        return res.data;
      })
      .catch((err) => {
        throw err;
      });
  },
  updateExplanationData({ commit }, payload) {
    return http
      .put("/tms/explanation-management/update-explanation", payload)
      .then((res) => {
        commit("UPDATE_EXPLANATION_DATA", res.data);
        return res.data;
      })
      .catch((err) => {
        throw err;
      });
  },
  updateStatus({ commit }, payload) {
    return http
      .put("/tms/explanation-management/status", payload)
      .then(res => {
        commit("UPDATE_STATUS_EXPLANTION", res.data);
        return res.data;
      })
      .catch(err => {
        throw err;
      })
  },
  getDelegatorList({ commit }, payload) {
    return http
      .get("/tms/delegate/choose-delegator-to-delegate?employee=" + payload)
      .then((res) => {
        commit("EXPLANATION_MANAGE_DRAWER_DATA", res.data);
        return res.data;
      })
      .catch((err) => {
        throw err;
      });
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
