import { listCV, listChannels } from "./mockData/manegamentCV.json";
import axios from "axios";

import { PAGE_OPTIONS_MANAGEMENT_CV, convertObjectToParams } from "../../components/commons/commons";
import http from "./helpers/httpInterceptor";
const getDefaultState = () => {
  return {
    listCv: [],
    pageOptions: { ...PAGE_OPTIONS_MANAGEMENT_CV },
    pages: {
      currentPage: 1,
      pageSize: 10,
      total_items: 0,
    },
    total_items: 10,
    isAdd: false,
    isEdit: false,
    statusCreating: false,
    setLinkPreview: "",
    listSource: [],
    listChannel: [],
    listChannelBySource: [],
  };
};

const state = getDefaultState();

const mutations = {
  SET_LIST_CV(state, data) {
    state.listCv = data;
  },
  SET_PAGE_OPTIONS(state, data) {
    state.pageOptions = {};
    state.pageOptions = data;
  },
  SET_TOTAL_CV(state, data) {
    state.total_items = data;
  },
  SET_IS_ADD(state, payload) {
    state.isAdd = payload;
  },
  SET_IS_Edit(state, payload) {
    state.isEdit = payload;
  },
  SET_STATUS_CREATING(state, payload) {
    state.statusCreating = payload;
  },
  SET_LINK_PREVIEW(state, data) {
    state.setLinkPreview = data;
  },
  SET_LIST_CHANNEL(state, data) {
    state.listChannel = data;
  },
  SET_LIST_SOURCE(state, data) {
    state.listSource = data;
  },
  SET_LIST_CHANNEL_SOURCE(state, data) {
    state.listChannelBySource = data;
  },
  RESET_STATE(state) {
    Object.assign(state, getDefaultState());
  },
  SET_PAGE_LIST_CV(state, data) {
    state.pages = data;
  },
};

const actions = {
  getListCv({ commit }, payload) {
    const params = convertObjectToParams(payload);
    return http
      .post(`/allCV${params}`)
      .then((res) => {
        if (res.data) {
          const { candidateList, page, size, totalElements } = res.data;
          commit("SET_LIST_CV", candidateList);
          commit("SET_TOTAL_CV", totalElements);
          commit("SET_PAGE_LIST_CV", { currentPage: page, pageSize: size, total_items: totalElements });
        }
      })
      .catch((error) => console.log("error", error));
  },
  uploadFile({ commit }, payload) {
    const { file, fileType } = payload;
    let formData = new FormData();
    formData.append("file", file.file.originFileObj);
    formData.append("fileType", fileType);
    return http
      .post("/upload", formData)
      .then((res) => {
        commit("SET_LINK_PREVIEW", res.data.previewUrl);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  uploadMultiFile({ commit }, payload) {
    const { file, fileType } = payload;
    let formData = new FormData();
    for (let i = 0; i < file.fileList.length; i++) {
      let fileObj = file.fileList[i];
      formData.append("file", fileObj.originFileObj);
    }
    formData.append("fileType", fileType);

    return http
      .post("/uploadMultiFile", formData)
      .then((res) => {
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  editCvCandidate({ commit }, payload) {
    const { candidateId, cvRequest } = payload;
    return http
      .put(`/updateCV`, cvRequest)
      .then((res) => {
        return res;
      })
      .catch((error) => {
        console.log("error", error);
        return error;
      });
  },
  createCvCandidate({ commit }, payload) {
    const { candidateId, cvRequest } = payload;
    return http
      .post("createCV", cvRequest)
      .then((res) => {
        return res;
      })
      .catch((error) => {
        console.log("error", error);
        return error;
      });
  },
  getListChannelbySource({ commit }, payload) {
    return http
      .get(`getChannelBySource?source=${payload}`)
      .then((res) => {
        commit("SET_LIST_CHANNEL_SOURCE", res.data.channelList);
      })
      .catch((error) => console.log("error", error));
  },
  getListChannel({ commit }) {
    return http
      .get("/getChannel")
      .then((res) => {
        commit("SET_LIST_CHANNEL", res.data.channellList);
      })
      .catch((error) => console.log("error", error));
  },
  getListSource({ commit }) {
    return http
      .get("/getAllSource")
      .then((res) => {
        res.data.map((item) => {
          item.name = item.source;
        });
        commit("SET_LIST_SOURCE", res.data);
      })
      .catch((error) => console.log("error", error));
  },
  uploadFileExcel({ commit }, payload) {
    const { file, fileInfo } = payload;
    //let infos = '[{ "fileName": "Phiếu yêu cầu mở FW_1.0.xlsx", "previewUrl": "/preview/salt/cv/salt_1636445679627_2021-11-09_334.xlsx" },{ "fileName": "Phiếu-yêu-cầu-mở-VPN_1.0.xlsx", "previewUrl": "/preview/salt/cv/salt_1636445679672_2021-11-09_456.xlsx" }]'
    let formData = new FormData();
    formData.append("file", file.file.originFileObj);
    formData.append("fileInfo", JSON.stringify(fileInfo.files));
    return http
      .post("/uploadExcel", formData)
      .then((res) => {
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  uploadFileExcelByStep({ commit }, payload) {
    const { file, fileInfo, requestId } = payload;
    let formData = new FormData();
    formData.append("file", file.file.originFileObj);
    formData.append("fileInfo", JSON.stringify(fileInfo.files));
    formData.append("requestID ", requestId);
    formData.append("stepRTS", "qualify");
    return http
      .post("/uploadExcelStepRTS", formData)
      .then((res) => {
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
