import {candidates} from "./mockData/candidates.json"
import http from "./helpers/httpInterceptor";
import {convertMillisecondsToDate, convertObjectToParams} from "../../components/commons/commons";
import {configPanigationDefault} from "../../components/commons/commonConstant";

const state = () => ({
    candidateList: [],
    pages: {
        currentPage: configPanigationDefault.currentPage,
        pageSize: configPanigationDefault.pageSize,
    },
    candidateTitleList:[]
});
const mutations = {
    SET_CANDIDATE_LIST(state, data) {
        let newData = [];
        if (data) {
            newData = data.map((element) => (formatCandidate(element)));
        }
        state.candidateList = newData
    },
    SET_CANDIDATE_TITLE_LIST(state, data) {
       state.candidateTitleList = data;
    },
    SET_CANDIDATE_PAGE(state, data) {
        state.pages = data;
    },
    DELETE_CANDIDATE_LIST(state, data) {
        const {ids} = data;
        state.candidateList = state.candidateList.filter((m) => !ids.includes(m.id));
    },
};
const actions = {
    getCandidateTitleList({commit}, payload) {
        const {requestId, searchString} = payload
        return http
            .post(`/candidate/search?limit=100&offset=1&requestId=${requestId}&searchString=${searchString}`)
            .then((res) => {
                commit("SET_CANDIDATE_TITLE_LIST", res.data?.list);
                return res.data;
            })
            .catch((error) => {
                throw error;
            });
    },
    getCandidateList({commit}, payload) {
        if (!payload) {
            return
        }
        const {offset, limit} = payload
        console.log(convertObjectToParams(payload))
        return http
            .post(`/candidate/search${convertObjectToParams(payload)}`)
            .then((res) => {
                commit("SET_CANDIDATE_LIST", res.data?.list);
                commit("SET_CANDIDATE_PAGE", {
                    currentPage: offset,
                    pageSize: limit,
                    total_items: res.data?.total,
                });
                return res.data;
            })
            .catch((error) => {
                throw error;
            });
    },

    deleteCandidates({commit}, payload) {
        let ids = (JSON.parse(JSON.stringify(payload)))
        return http
            .post(`/candidate-delete`,ids)
            .then((res) => {
                if (res.status === 200) {
                    commit("DELETE_CANDIDATE_LIST", {
                        ids : ids
                    });
                }
            })
            .catch((error) => {
                throw error;
            });
    },
}

function formatCandidate(candidate) {
    return {
        ...candidate,
        birthday: convertMillisecondsToDate(candidate.birthday),
        cvAttach: candidate.cvAttach + ".pdf",
        status: candidate.status? "new" : "Onboard"
    }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations,
};
