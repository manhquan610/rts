import http from "./helpers/httpInterceptor";

const getDefaultState = () => {
    return {
        listPeople: [],
        progressStep: 0,
        progress: {
            New: {
                createdBy: "",
                createdDate: "",
                id: null,
                priority: null,
                reason: "",
                statusRequestType: "Created by",
                step: 0
            },
            Edited: {
                createdBy: "",
                createdDate: "",
                id: null,
                priority: null,
                reason: "",
                statusRequestType: "Edited by",
                step: 1
            },
            Pending: {
                createdBy: "",
                createdDate: "",
                id: null,
                priority: null,
                reason: "",
                statusRequestType: "Submited by",
                step: 2
            },
            Approved: {
                createdBy: "",
                createdDate: "",
                id: null,
                priority: null,
                reason: "",
                statusRequestType: "Approved by",
                step: 3
            },
            Approved2: {
                createdBy: "",
                createdDate: "",
                id: null,
                priority: null,
                reason: "",
                statusRequestType: "Approved by",
                step: 4
            },
            Rejected: {
                createdBy: "",
                createdDate: "",
                id: null,
                priority: null,
                reason: "",
                statusRequestType: "Rejected by",
                step: 3
            },
            Assigned: {
                createdBy: "",
                createdDate: "",
                id: null,
                priority: null,
                reason: "",
                statusRequestType: "Assigned by",
                step: 5
            },
            conducted: {
                createdBy: "",
                createdDate: "",
                id: null,
                priority: null,
                reason: "",
                statusRequestType: "Conducted by",
                step: 6
            },

            Closed: {
                createdBy: "",
                createdDate: "",
                id: null,
                priority: null,
                reason: "",
                statusRequestType: "Closed by",
                step: 7
            },

        },
        listHistory: [],
    }
}

// initial state
const state = getDefaultState();

const mutations = {
    SET_LIST_PEOPLE(state, data) {
        state.listPeople = data;
    },
    SET_HISTORY_PROGRESS(state, data) {
        let hasAssignItem = false;
        data.forEach((item) => {
            let temp = {...item};
            let step = state.progress[item.statusRequestType]?.step;
            let oldStep = state.progressStep

            if (temp.statusRequestType === "New") {
                temp.statusRequestType = "Created";
            } else if (temp.statusRequestType === "Approved" || temp.statusRequestType === "Approved2" ) {
                delete state.progress['Rejected'];
            } else if (temp.statusRequestType === "Rejected") {
                delete state.progress['Approved'];
                delete state.progress['Approved2'];
            } else if (temp.statusRequestType === "Assigned") {
                hasAssignItem = true;
                temp.statusRequestType = "Published";
                step = state.progress["conducted"].step;
                state.progress["conducted"] = {
                    createdBy: getAllAssnees(
                        (state.listPeople && state.listPeople) || []
                    ), createdDate: "(...)",
                    id: "(...)",
                    reason: "(...)",
                    statusRequestType: "Conducted by",
                };
            }

            temp.statusRequestType = temp.statusRequestType + " by";
            state.progress[item.statusRequestType] = temp;

            state.progressStep = (step > oldStep) ? step : oldStep;
        });
    },
    SET_LIST_HISTORY(state, data) {

        data.map((item) => {
            if (item.statusRequestType === 'New') {
                item.statusRequestType = 'Created'
            }

            item.statusRequestType = item.statusRequestType + " by";
            return item
        })
        state.listHistory = data.reverse();

    },
    RESET_STATE(state) {
        Object.assign(state, getDefaultState())
    }
};

const getters = {
    progressCompleted: (state) => {
        const todoCompleted = state.progress.filter((todo) => todo.complete).length;
        return Math.round((todoCompleted / state.progress.length) * 100);
    },
};

const actions = {
    RtsRequestDetailHistory({commit}, id) {
        return http
            .get(`/request-history/${id}?id=${id}`)
            .then((res) => {
                commit("SET_HISTORY_PROGRESS", res.data.process);
                commit("SET_LIST_PEOPLE", res.data.assignees);
                return res.data;
            })
            .catch((error) => {
                console.log("error", error);
                throw error;
            });
    },
    RtsRequestDetailHistoryActivity({commit}, id) {
        return http
            .get(`/request-history/${id}?id=${id}`)
            .then((res) => {
                commit("SET_LIST_HISTORY", res.data.process);
                return res.data;
            })
            .catch((error) => {
                console.log("error", error);
                throw error;
            });
    },
};

function renderText(text) {
    let title = "";
    if (text == "Assigned") {
        title = "Published by";
    } else if (text == "Approved") {
        title = "Approved by";
    }else if (text == "Approved2") {
        title = "Approved by";
    } else if (text == "Pending") {
        title = "Submited by";
    } else if (text == "Edited") {
        title = "Edited by";
    } else if (text == "Rejected") {
        title = "Rejected by";
    } else if (text == "Created") {
        title = "Created by";
    }
    return title;
}

function getAllAssnees(list) {
    const getNameAssign = list.map((name) => name.ldap);
    return getNameAssign.join(" , ");
}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
};
