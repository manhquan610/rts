import { convertObjectToParams } from "../../../components/commons/commons";
import http from "../helpers/httpInterceptor";
import { JobType } from "../mockData/configurations.json";
const state = () => ({
  jobTypeList: [],
  pages: {
    currentPage: 1,
    pageSize: 10,
    total_items: 0,
  },
  jobLevelList: [],
  jobLevelList1: []
});

const mutations = {
  SET_JOB_TYPE_LIST(state, data) {
    state.jobTypeList = data;
  },
  SET_JOB_LEVEL_LIST(state,data){
    state.jobLevelList = data
  },
  SET_JOB_LEVEL_LIST_1(state,data){
    state.jobLevelList1 = data
  },
  UPDATE_JOB_TYPE_LIST(state, data) {
    let jobTypes = [...state.jobTypeList];
    const { id, name, description } = data;
    const obj = jobTypes.find((m) => m.id === id);
    if (obj) {
      obj["name"] = name;
      obj["description"] = description;
    }
    state.jobTypeList = jobTypes;
  },
  DELETE_JOB_TYPE_LIST(state, data) {
    const { ids } = data;
    state.jobLevelList = state.jobLevelList.filter((m) => !ids.includes(m.id));
  },
  DELETE_JOB_LEVEL_LIST(state, data) {
    const { ids } = data;
    state.jobTypeList = state.jobTypeList.filter((m) => !ids.includes(m.id));
  },
  SET_PAGES(state, data) {
    state.pages = data;
  },
};

const actions = {
  getJobLevelList1({ commit }, params) {
    const { page, limit, nameSearch } = params;
    return http
        .get(
            `/jobLevel/searchName${convertObjectToParams(params)}`
        )
        .then((res) => {
          commit("SET_JOB_LEVEL_LIST_1", res.data.content);
          return res.data;
        })
        .catch((error) => {
          throw error;
        });
  },
  getJobLevelList({ commit }, params) {
    const { page, limit, idJobType } = params;
      return http
        .get(
          `/jobLevel/searchByJobType${convertObjectToParams(params)}`
        )
        .then((res) => {
          commit("SET_JOB_LEVEL_LIST", res.data.content);
          return res.data;
        })
        .catch((error) => {
          throw error;
        });
  },
  getJobTypeList({ commit }, params) {
    const { page, limit, nameSearch } = params;
      return http
        .get(
          `/manager-jobType/exact${convertObjectToParams(params)}`
        )
        .then((res) => {
          commit("SET_JOB_TYPE_LIST", res.data.content);
          commit(
            "configurations/SET_PAGES",
            {
              currentPage: page,
              pageSize: limit,
              total_items: res.data.totalElements || 0,
            },
            { root: true }
          );
          return res.data;
        })
        .catch((error) => {
          throw error;
        });
  },
  createJobType({ commit }, payload) {
    return http
      .post("/createJobType", payload)
      .then((res) => {
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  updateJobType({ commit }, payload) {
    return http
      .put("/updateJobType", payload)
      .then((res) => {
        commit("UPDATE_JOB_TYPE_LIST", res.data);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  deleteJobType({ commit }, payload) {
    return http
      .post(`/deleteJobTypes`, payload)
      .then((res) => {
        if (res.status === 200) {
          commit("DELETE_JOB_TYPE_LIST", payload);
        }
      })
      .catch((error) => {
        throw error;
      });
  },
  createJobLevel({ commit }, payload) {
    return http
      .post("/jobLevel/insert", payload)
      .then((res) => {
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  deleteJobLevel({ commit }, payload) {
    const {idJobLevel} = payload
    return http
      .get(`/jobLevel/delete?idJobLevel=${idJobLevel}`)
      .then((res) => {
         return res
      })
      .catch((error) => {
        throw error;
      });
  },
  updateJobLevel({ commit }, payload) {
    return http
      .post("/jobLevel/update", payload)
      .then((res) => {
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
