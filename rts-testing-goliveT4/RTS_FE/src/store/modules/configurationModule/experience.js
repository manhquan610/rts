import http from "../helpers/httpInterceptor";

const state = () => ({
  experienceList: [],
});

const mutations = {
  SET_EXPERIENCE_LIST(state, data) {
    state.experienceList = data;
  },
  UPDATE_EXPERIENCE_LIST(state, data) {
    let experiences = [...state.experienceList];
    const { id, name, description } = data;
    const obj = experiences.find((m) => m.id === id);
    if (obj) {
      obj["name"] = name;
      obj["description"] = description;
    }
    state.experienceList = experiences;
  },
  DELETE_EXPERIENCE_LIST(state, data) {
    const { ids } = data;
    state.experienceList = state.experienceList.filter((m) => !ids.includes(m.id));
  },

};

const actions = {
  getExperienceList({ commit }, payload) {
    const { offset, limit } = payload;
    if (!payload.lstName) delete payload.lstName;
    return http
      .post(`/manager-experience/exact`, payload)
      .then((res) => {
        commit("SET_EXPERIENCE_LIST", res.data.manager);
        commit("configurations/SET_PAGES", {
          currentPage: offset,
          pageSize: limit,
          total_items: res.data.total_item || 0,
        },{root: true});
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  createExperience({ commit }, payload) {
    return http
      .post("/manager-experience", payload)
      .then((res) => {
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  updateExperience({ commit }, payload) {
    console.log(payload);
    return http
      .put("/manager-experience", payload)
      .then((res) => {
        commit("UPDATE_EXPERIENCE_LIST", res.data);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  deleteExperience({ commit }, payload) {
    return http
      .post(`/manager-experience/delete`, payload)
      .then((res) => {
        if (res.status === 200) {
          commit("DELETE_EXPERIENCE_LIST", payload);
        }
      })
      .catch((error) => {
        throw error;
      });
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
