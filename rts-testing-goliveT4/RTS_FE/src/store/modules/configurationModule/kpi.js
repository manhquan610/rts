import http from "../helpers/httpInterceptor";
import {capitalizeFirstLetter, convertMillisecondsToDate, convertObjectToParams} from "../../../components/commons/commons";

import * as Vue from 'vue' // in Vue 3
import axios from 'axios'
import VueAxios from 'vue-axios'

export const StartYear=2020;
export const currentYear = new Date().getFullYear();
let yearsPool = [];
for(let i=StartYear; i<= currentYear; i++)
    yearsPool.push(i);
export let FiltersDefaults = {
    year:{
        default: currentYear,
        pool: yearsPool
    } 
}

export const months = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"];


const mockupKPIApiRes = [
    {username: 'NVA', monthKPI: [3,5,2,11,30,6,7,80,70,100,31,24]},
    {username: 'TTB', monthKPI: [6,10,4,6,15,60,17,81,7,10,13,42]},
    {username: 'DTC', monthKPI: [31,51,21,61,35,16,73,85,70,10,31,14]}
]

const state = () => ({
    empKPIList: [],
});

const mutations = {
    SET_KPI_LIST(state, data) {
        data.forEach(k =>{
            adjustmentGetEmpKPI(k);
        });
        state.empKPIList = data;
        state.empKPIListBak = JSON.parse(JSON.stringify(data));
    },
    SET_KPI_FILTERSSTORE(state, filtersStore){
        //console.log('SET_KPI_FILTERSSTORE: ', filtersStore, yearsPool);
        state.filtersStore = filtersStore;
        state.filterYearsPool = yearsPool;
    },
    SET_KPI_DOWNLOADEXPORTLINK(state, link){
        //console.log('SET_KPI_DOWNLOADEXPORTLINK: ', link);
        state.downloadExportLink = link;
    },
    // CREATE_POSITION_LIST(state, data) {
    //     state.empKPIList.unshift(data);
    // },
    // UPDATE_POSITION_LIST(state, data) {
    //     let positions = [...state.empKPIList];
    //     const { id, name, description } = data;
    //     const obj = positions.find((m) => m.id === id);
    //     if (obj) {
    //         obj["name"] = name;
    //         obj["description"] = description;
    //     }
    //     state.empKPIList = positions;
    // },
    // DELETE_POSITION_LIST(state, data) {
    //     const { ids } = data;
    //     state.empKPIList = state.empKPIList.filter((m) => !ids.includes(m.id));
    // },
    SET_PAGES(state, data) {
        state.pages = data;
    },
};

const actions = {
    getKPIList({ commit }, payload) {
        //console.log('getKPIList: ', payload, payload['year']);
        const { offset, limit } = payload;
        if(!payload['year'])
            payload['year']= currentYear;
        let filters={
            year: payload['year'],
            name: payload['name']
        }
        return http
            .get(`/manage-kpi${convertObjectToParams(payload)}`)
            .then((res) => {
                commit("SET_KPI_LIST", res.data.content);
                commit("SET_KPI_FILTERSSTORE", filters);
                commit("SET_KPI_DOWNLOADEXPORTLINK", axios.defaults.baseURL+`manage-kpi/report${convertObjectToParams(filters)}`);
                commit("configurations/SET_PAGES", {
                    currentPage: offset,
                    pageSize: limit,
                    total_items: res.data.totalElements || 0,
                }, { root: true });
            })
            .catch((error) => {
                throw error;
            });
    },
    // createPositions({ commit }, payload) {
    //     return http
    //         .post("/manager-position", payload)
    //         .then((res) => {
    //             return res.data;
    //         })
    //         .catch((error) => {
    //             console.log(error)
    //             throw error;
    //         });
    // },
    updateKPI({ commit }, payload) {
        console.log('updateKPI payload: ', payload);
        return http
            .patch(`/manage-kpi/update-kpi${convertObjectToParams(payload.urlParams)}`, adjustmentSaveEmpKPI(payload.record))
            .then((res) => {
                adjustmentGetEmpKPI(res.data);
                return res;
            })
            .catch((error) => {
                throw error;
            });
    },
    // deletePositions({ commit }, payload) {
    //     return http
    //         .post(`/manager-position/delete`, payload)
    //         .then((res) => {
    //             if (res.status === 200) {
    //                 commit("DELETE_POSITION_LIST", payload);
    //             }
    //         })
    //         .catch((error) => {
    //             throw error;
    //         });
    // },
};
function adjustmentSaveEmpKPI(record){
    let clone = {...record};
    months.forEach((v, i) => {
        let monthKey = v.toLowerCase().substring(0,3) + 'Quantity';
        clone[monthKey] = record[monthKey].value;
    });
    return clone;
}

function adjustmentGetEmpKPI(record){
    record['key']=record.username;
    record['total']=0;
    months.forEach((v) => {
        let monthKey = v.toLowerCase().substring(0,3) + 'Quantity';
        record[monthKey] = {
            value: record[monthKey],
            monthKey: monthKey
        }
        record['total']+= record[monthKey].value
    });
}

export default {
    namespaced: true,
    state,
    actions,
    mutations,
};
