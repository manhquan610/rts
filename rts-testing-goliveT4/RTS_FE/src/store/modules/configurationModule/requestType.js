import http from "../helpers/httpInterceptor";

const state = () => ({
  requestTypeList: [],
  pages: {
    currentPage: 1,
    pageSize: 10,
    total_items: 0,
  },
});

const mutations = {
  SET_REQUEST_TYPE_LIST(state, data) {
    state.requestTypeList = data;
  },
  CREATE_REQUEST_TYPE_LIST(state, data) {
    state.requestTypeList.unshift(data);
  },
  UPDATE_REQUEST_TYPE_LIST(state, data) {
    let requestTypeList = [...state.requestTypeList];
    const { id, name, description } = data;
    const obj = requestTypeList.find((m) => m.id === id);
    if (obj) {
      obj["name"] = name;
      obj["description"] = description;
    }
    state.requestTypeList = requestTypeList;
  },
  DELETE_REQUEST_TYPE_LIST(state, data) {
    const { ids } = data;
    state.requestTypeList = state.requestTypeList.filter((m) => !ids.includes(m.id));
  },
  SET_PAGES(state, data) {
    state.pages = data;
  },
};

const actions = {
  getRequestTypeList({ commit }, payload) {
    const { offset, limit } = payload;
    if (!payload.lstName) delete payload.lstName;
    return http
      .post(`/manager-request-type/exact`, payload)
      .then((res) => {
        commit("SET_REQUEST_TYPE_LIST", res.data.manager);
        commit("configurations/SET_PAGES", {
          currentPage: offset,
          pageSize: limit,
          total_items: res.data.total_item || 0,
        },{root:true});
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  createRequestType({ commit }, payload) {
    return http
      .post("/manager-request-type", payload)
      .then((res) => {
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  updateRequestType({ commit }, payload) {
    return http
      .put("/manager-request-type", payload)
      .then((res) => {
        commit("UPDATE_REQUEST_TYPE_LIST", res.data);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  deleteRequestType({ commit }, payload) {
    return http
      .post(`/manager-request-type/delete`, payload)
      .then((res) => {
        if (res.status === 200) {
          commit("DELETE_REQUEST_TYPE_LIST", payload);
        }
      })
      .catch((error) => {
        throw error;
      });
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
