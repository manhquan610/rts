import http from "../helpers/httpInterceptor";

const state = () => ({
    priorityList: [],
    pages: {
        currentPage: 1,
        pageSize: 10,
        total_items: 0,
    },
});

const mutations = {
    SET_PRIORITY_LIST(state, data) {
        state.priorityList = data;
    },
    CREATE_PRIORITY_LIST(state, data) {
        state.priorityList.unshift(data);
    },
    UPDATE_PRIORITY_LIST(state, data) {
        let priorities = [...state.priorityList];
        const {id, name, description} = data;
        const obj = priorities.find((m) => m.id === id);
        if (obj) {
            obj["name"] = name;
            obj["description"] = description;
        }
        state.priorityList = priorities;
    },
    DELETE_PRIORITY_LIST(state, data) {
        const {ids} = data;
        state.priorityList = state.priorityList.filter((m) => !ids.includes(m.id));
    },
    SET_PAGES(state, data) {
        state.pages = data;
    },
};

const actions = {
    getPriorityList({commit}, payload) {
        const {offset, limit} = payload;
        if (!payload.lstName) delete payload.lstName;
        console.log(payload, 'payloadpayload')
        return http
            .post(`/manager-priority/exact`, payload)
            .then((res) => {
                commit("SET_PRIORITY_LIST", res.data.manager);
                commit("configurations/SET_PAGES", {
                    currentPage: offset,
                    pageSize: limit,
                    total_items: res.data.total_item || 0,
                }, {root: true});
                return res.data;
            })
            .catch((error) => {
                throw error;
            });
    },
    createPriority({commit}, payload) {
        return http
            .post("/manager-priority", payload)
            .then((res) => {
                return res.data;
            })
            .catch((error) => {
                throw error;
            });
    },
    updatePriority({commit}, payload) {
        return http
            .put("/manager-priority", payload)
            .then((res) => {
                commit("UPDATE_PRIORITY_LIST", res.data);
                return res.data;
            })
            .catch((error) => {
                throw error;
            });
    },
    deletePriority({commit}, payload) {
        return http
            .post(`/manager-priority/delete`, payload)
            .then((res) => {
                if (res.status === 200) {
                    commit("DELETE_PRIORITY_LIST", payload);
                }
            })
            .catch((error) => {
                throw error;
            });
    },
};

export default {
    namespaced: true,
    state,
    actions,
    mutations,
};
