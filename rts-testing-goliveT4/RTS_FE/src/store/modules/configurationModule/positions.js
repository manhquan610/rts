import http from "../helpers/httpInterceptor";

const state = () => ({
  positionList: [],
});

const mutations = {
  SET_POSITION_LIST(state, data) {
    state.positionList = data;
  },
  CREATE_POSITION_LIST(state, data) {
    state.positionList.unshift(data);
  },
  UPDATE_POSITION_LIST(state, data) {
    let positions = [...state.positionList];
    const { id, name, description } = data;
    const obj = positions.find((m) => m.id === id);
    if (obj) {
      obj["name"] = name;
      obj["description"] = description;
    }
    state.positionList = positions;
  },
  DELETE_POSITION_LIST(state, data) {
    const { ids } = data;
    state.positionList = state.positionList.filter((m) => !ids.includes(m.id));
  },
  SET_PAGES(state, data) {
    state.pages = data;
  },
};

const actions = {
  getPositionsList({ commit }, payload) {
    const { offset, limit } = payload;
    if (!payload.lstName) delete payload.lstName;
    return http
      .post(`/manager-position/exact`, payload)
      .then((res) => {
        commit("SET_POSITION_LIST", res.data.manager);
        commit("configurations/SET_PAGES", {
          currentPage: offset,
          pageSize: limit,
          total_items: res.data.total_item || 0,
        },{ root: true });
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  createPositions({ commit }, payload) {
    console.log('create pos')
    return http
      .post("/manager-position", payload)
      .then((res) => {
        console.log('res',res)
        return res.data;
      })
      .catch((error) => {
        console.log(error)
        throw error;
      });
  },
  updatePositions({ commit }, payload) {
    return http
      .put("/manager-position", payload)
      .then((res) => {
        commit("UPDATE_POSITION_LIST", res.data);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  deletePositions({ commit }, payload) {
    return http
      .post(`/manager-position/delete`, payload)
      .then((res) => {
        if (res.status === 200) {
          commit("DELETE_POSITION_LIST", payload);
        }
      })
      .catch((error) => {
        throw error;
      });
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
