import salary from "../mockData/salary.json"

const state = () =>({
    salary : []
})

const mutations = {
    SET_SALARY_LIST(state, data){
       state.salary = data
    }
}
const actions = {
    getListSalary({ commit }) {
        let res = {
            data: salary.salary,
        };

        commit("SET_SALARY_LIST", res.data);
    },
}
export default {
    namespaced: true,
    state,
    actions,
    mutations,
};