import { configPanigationDefault } from "../../../components/commons/commonConstant";
import http from "../helpers/httpInterceptor";
const state = () => ({
  selectedRowKeys: [],
  configurationList: [],
  pages: {
    currentPage: 1,
    pageSize: 10,
    total_items: 0,
  },
});

const mutations = {
  SET_SELECTED_ROW_KEY(state, data) {
    state.selectedRowKeys = data;
  },
  SET_SEARCH_FILTER(state, data) {
    state.configurationList = data;
  },
  SET_PAGES(state, data) {
    state.pages = data;
  },
};

const actions = {
  getConfigurationByTab({ commit }, payload) {
    let url;
    const { name, tab } = payload;
    switch (tab) {
      case "Positions":
        url = `manager-position`;
        break;
      case "Priority":
        url = `manager-priority`;
        break;
      case "Experience":
        url = `manager-experience`;
        break;
      case "Major":
        url = `manager-major`;
        break;
      case "Request Type":
        url = `manager-request-type`;
        break;
      case "Area":
        url = `manager-area`;
        break;
      case "Job Type":
        url = `manager-jobType`;
        break;
      default:
        break;
    }
    if (tab === "Area" || tab === "Job Type") {
      return http
        .get(
          `/${url}/same?limit=${configPanigationDefault.pageSize}&nameSearch=${name}&page=${configPanigationDefault.currentPage}`
        )
        .then((res) => {
          commit("SET_SEARCH_FILTER", res.data.content);
          return res.data;
        })
        .catch((error) => {
          throw error;
        });
    } else {
      return http
        .post(`/${url}/same`, {
          limit: configPanigationDefault.pageSize,
          offset: configPanigationDefault.currentPage,
          lstName: name,
        })
        .then((res) => {
          commit("SET_SEARCH_FILTER", res.data.manager);
          return res.data;
        })
        .catch((error) => {
          throw error;
        });
    }
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
