import http from "../helpers/httpInterceptor";

const state = () => ({
  majorList: [],
});

const mutations = {
  SET_MAJOR_LIST(state, data) {
    state.majorList = data;
  },
  UPDATE_MAJOR_LIST(state, data) {
    let majors = [...state.majorList];
    const { id, name, description } = data;
    const obj = majors.find((m) => m.id === id);
    if (obj) {
      obj["name"] = name;
      obj["description"] = description;
    }
    state.majorList = majors;
  },
  DELETE_MAJOR_LIST(state, data) {
    const { ids } = data;
    state.majorList = state.majorList.filter((m) => !ids.includes(m.id));
  },
};

const actions = {
  getMajorList({ commit }, payload) {
    const { offset, limit } = payload;
    if (!payload.lstName) delete payload.lstName;
    return http
      .post(`/manager-major/exact`, payload)
      .then((res) => {
        commit("SET_MAJOR_LIST", res.data.manager);
        commit("configurations/SET_PAGES", {
          currentPage: offset,
          pageSize: limit,
          total_items: res.data.total_item || 0,
        },{root: true});
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  createMajor({ commit }, payload) {
    return http
      .post("/manager-major", payload)
      .then((res) => {
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  updateMajor({ commit }, payload) {
    return http
      .put("/manager-major", payload)
      .then((res) => {
        commit("UPDATE_MAJOR_LIST", res.data);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  deleteMajor({ commit }, payload) {
    console.log(payload, 'major')
    return http
      .post(`/manager-major/delete`, payload)
      .then((res) => {
        console.log('res', res)
        if (res.status === 200) {
          commit("DELETE_MAJOR_LIST", payload);
        }
      })
      .catch((error) => {
        throw error;
      });
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
