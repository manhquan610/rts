import { convertObjectToParams } from "../../../components/commons/commons";
import http from "../helpers/httpInterceptor";
import { Areas } from "../mockData/configurations.json";
const state = () => ({
  areaList: [],
  pages: {
    currentPage: 1,
    pageSize: 10,
    total_items: 0,
  },
});

const mutations = {
  SET_AREA_LIST(state, data) {
    state.areaList = data;
  },
  UPDATE_AREA_LIST(state, data) {
    let areas = [...state.areaList];
    const { id, name, description } = data;
    const obj = areas.find((m) => m.id === id);
    if (obj) {
      obj["name"] = name;
      obj["description"] = description;
    }
    state.areaList = areas;
  },
  DELETE_AREA_LIST(state, data) {
    const { ids } = data;
    state.areaList = state.areaList.filter((m) => !ids.includes(m.id));
  },
  SET_PAGES(state, data) {
    state.pages = data;
  },
};

const actions = {
  getAreaList({ commit }, params) {
    if(!params) return
    const { page, limit, nameSearch } = params;
      return http
        .get(`/manager-area/exact${convertObjectToParams(params)}`)
        .then((res) => {
          commit("SET_AREA_LIST", res.data.content);
          commit(
            "configurations/SET_PAGES",
            {
              currentPage: page,
              pageSize: limit,
              total_items: res.data.totalElements || 0,
            },
            { root: true }
          );
          return res.data;
        })
        .catch((error) => {
          throw error;
        });
  },
  createArea({ commit }, payload) {
    return http
      .post("/createArea", payload)
      .then((res) => {
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  updateArea({ commit }, payload) {
    return http
      .put("/updateArea", payload)
      .then((res) => {
        commit("UPDATE_AREA_LIST", res.data);
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  deleteArea({ commit }, payload) {
    return http
      .post(`/deleteAreas`, payload)
      .then((res) => {
        if (res.status === 200) {
          commit("DELETE_AREA_LIST", payload);
        }
      })
      .catch((error) => {
        throw error;
      });
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
