import http from "./helpers/httpInterceptor";
import { tickets, taReport } from "./mockData/report.json";
import { convertMillisecondsToDate, findObjectById, convertObjectToParams } from "../../components/commons/commons";
import moment from "moment";

const state = () => ({
  ticketReport: [],
  processingReport: [],
  taReport: [],
  ticketLink: "",
  pages: {
    currentPage: 1,
    pageSize: 10,
    total_items: 0,
  },
});
const mutations = {
  SET_TICKET_REPORT(state, data) {
    state.ticketReport = data;
  },
  SET_TA_REPORT(state, data) {
    data.forEach((ta) => {
      ta["completeRating"] = (ta.actual / ta.target) * 100;
      ta.children.forEach((child0) => {
        child0["completeRating"] = (child0.actual / child0.target) * 100;
        child0.children.forEach((child1) => {
          child1["completeRating"] = (child1.actual / child1.target) * 100;
        });
      });
    });
    state.taReport = data;
  },
  SET_PROCESSING_REPORT(state, data) {
    state.processingReport = data;
  },
  SET_PAGE(state, data) {
    const { offset, limit, total_items } = data;
    if (offset || offset === 0) {
      state.pages.currentPage = offset;
    }
    if (limit || limit === 0) {
      state.pages.pageSize = limit;
    }
    if (total_items || total_items === 0) {
      state.pages.total_items = total_items;
    }
  },
  SET_EXPORT_LINK(state, data) {
    state.ticketLink = data;
  },
};
const actions = {
  GET_TICKET_REPORT({ commit }, payload) {
    const { pageOptions, dataFilter } = payload;
    const newPayload = {
      page: pageOptions.offset,
      size: pageOptions.limit,
      startTime: dataFilter.fromDate ? moment(dataFilter.fromDate).utc().format("YYYY/MM/DD") : null,
      endTime: dataFilter.toDate ? moment(dataFilter.toDate).utc().format("YYYY/MM/DD") : null,
      departmentIds: dataFilter.department,
      groupIds: dataFilter.group,
      projectIds: dataFilter.project,
      locationIds: dataFilter.area,
      priorityIds: dataFilter.priority,
      statusIds: dataFilter.status,
    };
    const param = convertObjectToParams(newPayload);
    return http
      .get(`report/ticket-v2${param}`)
      .then((res) => {
        commit("SET_PAGE", {
          offset: pageOptions.offset,
          limit: pageOptions.limit,
        });
        commit("SET_TICKET_REPORT", res.data.reportResponses);
        commit("SET_PAGE", {
          total_items: res.data.basePage.total,
        });
        return res.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  // GET_TICKET_REPORT({ commit }, payload) {
  //   const { pageOptions, dataFilter } = payload;
  //   const newPayload = {
  //     offset: pageOptions.offset,
  //     limit: pageOptions.limit,
  //     startTime: dataFilter.fromDate ? moment(dataFilter.fromDate).utc().format("YYYY/MM/DD") : null,
  //     endTime: dataFilter.toDate ? moment(dataFilter.toDate).utc().format("YYYY/MM/DD") : null,
  //     departmentIds: dataFilter.department,
  //     groupIds: dataFilter.group,
  //     projectIds: dataFilter.project,
  //     locationIds: dataFilter.area,
  //     priorityIds: dataFilter.priority,
  //     statusIds: dataFilter.status,
  //   };
  //   const param = convertObjectToParams(newPayload);
  //   return http
  //     .get(`report/ticket${param}`)
  //     .then((res) => {
  //       commit("SET_PAGE", {
  //         offset: pageOptions.offset,
  //         limit: pageOptions.limit,
  //       });
  //       commit("SET_TICKET_REPORT", res.data.content);
  //       commit("SET_PAGE", {
  //         total_items: res.data.totalElements,
  //       });
  //       return res.data.content;
  //     })
  //     .catch((error) => {
  //       throw error;
  //     });
  // },
  GET_TA_REPORT({ commit }, payload) {
    const { pageOptions, dataFilter } = payload;
    const newPayload = {
      offset: pageOptions.offset,
      limit: pageOptions.limit,
      startTime: dataFilter.fromDate ? moment(dataFilter.fromDate).utc().format("YYYY/MM/DD") : null,
      endTime: dataFilter.toDate ? moment(dataFilter.toDate).utc().format("YYYY/MM/DD") : null,
      departmentIds: dataFilter.department,
      groupIds: dataFilter.group,
      projectIds: dataFilter.project,
      locationIds: dataFilter.area,
    };
    const param = convertObjectToParams(newPayload);
    return http
      .get(`report/ta${param}`)
      .then((res) => {
        commit("SET_PAGE", {
          offset: pageOptions.offset,
          limit: pageOptions.limit,
        });
        commit("SET_TA_REPORT", res.data.content);
        commit("SET_PAGE", {
          total_items: res.data.totalElements,
        });
        return res.data.content;
      })
      .catch((error) => {
        throw error;
      });
  },
  GET_PROCESSING_REPORT({ commit }, payload) {
    const { pageOptions, dataFilter } = payload;
    let res = {
      data: taReport,
    };
    console.log(res.data);
    commit("SET_PROCESSING_REPORT", res.data);
    return res.data;
    // const newPayload = {
    //     offset: pageOptions.offset,
    //     limit: pageOptions.limit,
    //     deadline: dataFilter.deadline ? moment(dataFilter.deadline).utc().format('YYYY/MM/DD') : null,
    //     departmentIds: dataFilter.department,
    //     groupIds: dataFilter.group,
    //     skillIds: dataFilter.keyword,
    //     locationIds: dataFilter.area,
    // }
    // const param = convertObjectToParams(newPayload)
    // return http
    //     .get(`report/processing${param}`)
    //     .then((res) => {
    //         commit("SET_PAGE", {
    //             offset: pageOptions.offset,
    //             limit: pageOptions.limit,
    //         });
    //         commit("SET_PROCESSING_REPORT", res.data.content);
    //         commit("SET_PAGE", {
    //             total_items: res.data.totalElements,
    //         });
    //         return res.data.content
    //
    //     })
    //     .catch((error) => {
    //         throw error;
    //     });
  },
};
function formatTickets(report) {
  let res = report;
  res.map((item) => {
    item.children?.map((child) => {
      // if(child.unit !== item.unitId){
      //     child.unit = findObjectById(child.unit, 'id', departments)?.name
      // } else {
      //     child.unit = item.unit
      // }
      child.deadline = convertMillisecondsToDate(child.deadline);
      if (child.deadline === "Invalid date") {
        child.deadline = "";
      }
    });
  });
  return res;
}
export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
