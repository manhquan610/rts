import Vue from "vue";
import Vuex from "vuex";
import logs from "./modules/logs";
import timesheet from "./modules/timesheet";
import user from "./modules/user";
import application from "./modules/application";
import report from "./modules/report";
import explanation from "./modules/explanation";
import settings from "./modules/settings";
import abnormalcase from "./modules/abnormalcase";
import request from "./modules/request";
import delegation from "./modules/delegation";
import checkinout from "./modules/checkinout";
import overtime from "./modules/overtime";
import kpi from "./modules/configurationModule/kpi";
import positions from "./modules/configurationModule/positions";
import priority from "./modules/configurationModule/priority";
import experience from "./modules/configurationModule/experience";
import major from "./modules/configurationModule/major";
import salary from "./modules/configurationModule/salary"
import requestType from "./modules/configurationModule/requestType";
import configurations from "./modules/configurationModule";
import rtsRequest from "./modules/rtsRequest";
import rtsInterview from "./modules/rtsInterview";
import rtsCandidate from "./modules/rtsCandidate";
import rtsApplication from "./modules/rtsApplication";
import rtsRequestDetailGenenalInfo from './modules/rtsRequestDetailGenenalInfo'
import rtsListPeopleAssign from './modules/rtsListPeopleAssigness'
import rtsProgressHistory from './modules/rtsProgressHistory'
import rtsActivity from './modules/rtsActivity'
import rtsActivityCandidate from './modules/rtsActivityCandidate'
import rtsListCandidate from './modules/rtsListCandidate';
import rtsInterviewTab from './modules/rtsInterviewTab';
import rtsFeaturedJobs from './modules/rtsFeaturedJobs';
import rtsPopularJobs from './modules/rtsPopularJobs';
import rtsShareJob from "./modules/rtsShareJob";
import rtsJobPostingDetail from "./modules/rtsJobPostingDetail";
import area from './modules/configurationModule/area';
import jobType from './modules/configurationModule/jobType';
import rtsManagementCV from './modules/rtsManagementCV';
import rtsReport from "./modules/rtsReport";
Vue.use(Vuex);
// const debug = process.env.NODE_ENV !== 'production'
export default new Vuex.Store({
  modules: {
    logs,
    timesheet,
    user,
    application,
    report,
    explanation,
    settings,
    overtime,
    abnormalcase,
    request,
    delegation,
    checkinout,
    kpi,
    positions,
    priority,
    experience,
    major,
    salary,
    requestType,
    configurations,
    rtsRequest,
    rtsRequestDetailGenenalInfo,
    rtsInterview,
    rtsCandidate,
    rtsApplication,
    rtsListPeopleAssign,
    rtsProgressHistory,
    rtsActivity,
    rtsActivityCandidate,
    rtsListCandidate,
    rtsInterviewTab,
    rtsFeaturedJobs,
    rtsPopularJobs,
    rtsShareJob,
    rtsJobPostingDetail,
    area,
    jobType,
    rtsManagementCV,
    rtsReport
  },
  strict: false,
});
