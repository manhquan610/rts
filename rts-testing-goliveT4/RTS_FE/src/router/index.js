import Vue from "vue";
import VueRouter from "vue-router";
import Layout from '../layout/pageLayout/PageLayout.vue';
import JobPostingLayout from "../layout/JobPostingLayout";
import Timesheet from "../pages/Timesheet/Timesheet.vue";
import ExplanationMangement from "../pages/ExplanationManagement/ExplanationMangement.vue";
import DelegationManagement from "../pages/DelegationManagement/DelegationManagement.vue";
import RequestManagement from "../pages/RequestManagement/RequestManagement.vue";
import AbnormalCase from "../pages/AbnormalCase/AbnormalCase.vue";
import OnsiteManagement from "../pages/OnsiteManagement/OnsiteManagement.vue";
import OvertimeManagement from "../pages/OverTimeManagement/OverTimeManagement.vue";
import CheckInOut from "../pages/CheckInOut/CheckinOut.vue";
import Payroll from "../pages/Payroll/Payroll.vue";
import Employee from "../pages/Employee/Employee.vue";
//import Report from "../pages/Report/Report.vue";
// import TMSReport from "../pages/Report/TMSReport/TMSReport.vue";
import Settings from "../pages/Settings/Settings.vue";
import Logs from "../pages/Logs/Logs.vue";
import Login from "../pages/Login/Login.vue"
import ChangePassword from "../pages/ChangePassword/ChangePassword.vue"
import UpdateLeave from "../pages/UpdateLeave/UpdateLeave.vue"

// dev new
import Requests from "../pages/Requests/Requests.vue";
import RequestEdit from "../pages/Requests/RequestEdit/RequestEdit.vue";
import RequestDetail from "../pages/Requests/RequestDetail/RequestDetail.vue";
import JobPosting from "@/pages/JobPosting/JobPosting.vue";
import JobPostingDetail from "@/pages/JobPosting/JobPostingDetail/JobPostingDetail.vue";
import CV from "../pages/CV/CV.vue";
import Interview from "../pages/Interview/Interview.vue";
import Report from "../pages/Report/Report.vue";
import TicketReport from "../pages/Report/TicketReport/TicketReport.vue";
import Configurations from "../pages/Configurations/Configurations.vue";
import ManagementCV from "../pages/ManagementCV/ManagementCV.vue";
import ManagementCVDetail from "../pages/ManagementCV/ManagementCVDetail/ManagementCVDetail.vue";
import InterviewManagement from "../pages/InterviewManagement/Interview.vue"
import store from "../store/store";
import ProcessingReport from "../pages/Report/ProcessingReport/ProcessingReport.vue";
import TaReport from "../pages/Report/TaReport/TaReport.vue"

Vue.use(VueRouter);

const ifNotAuthenticated = (to, from, next) => {
    if (!store.state.user.isAuthenticated) {
        next();
        return;
    }
    next("/");
};
const ifAuthenticated = (to, from, next) => {
    localStorage.setItem('exact-router', to.fullPath)
    if (store.state.user.isAuthenticated) {
        next();
        return;
    }
    next("/login");
};

export default new VueRouter({
    mode: "hash",
    routes: [
        {
            path: "/login",
            component: Login,
            beforeEnter: ifNotAuthenticated
        },
        {
            path: "/change-password",
            component: ChangePassword,
        },
        {
            path: "/update-leave",
            component: UpdateLeave,
        },
        {
            path: "/rts",
            component: JobPostingLayout,
            name: "JobPostingLayout",
            meta: { title: 'Recruitment Tracking System' },
            children: [
                {
                    path: "/rts/job-posting/:categoryId?",
                    component: JobPosting,
                    name: "JobPosting",
                    meta: { title: 'Recruitment Tracking System' }
                },
                {
                    path: "/rts/job-posting/detail/:jobId?",
                    component: JobPostingDetail,
                    name: "JobPostingDetail",
                    meta: { title: 'Recruitment Tracking System' }
                },
            ]
        },
        {
            path: "/",
            name: "layoutRTS",
            redirect: '/rts/requests',
            component: Layout,
            beforeEnter: ifAuthenticated,
            meta: { title: 'Recruitment Tracking System' },
            children: [
                {
                    path: "rts/requests",
                    component: Requests,
                    name: "Requests",
                    meta: { title: 'Recruitment Tracking System' }
                },
                {
                    path: "rts/requests/edit/:id?",
                    component: RequestEdit,
                    name: "RequestEdit",
                    meta: { title: 'Recruitment Tracking System' },
                },
                {
                    path: "rts/requests/detail/:id?",
                    component: RequestDetail,
                    name: "RequestDetail",
                    meta: { title: 'Recruitment Tracking System' },
                },
                {
                    path: "rts/cv",
                    component: CV,
                    name: "CV",
                    meta: { title: 'Recruitment Tracking System' },
                },
                {
                    path: "rts/interview",
                    component: Interview,
                    name: "Interview",
                    meta: { title: 'Recruitment Tracking System' },
                },
                {
                    path: "rts/report",
                    component: Report,
                    name: "Report",
                },
                {
                    path: "rts/report/ticket-report",
                    component: TicketReport,
                    name: "TicketReport",
                },
                {
                    path: "rts/report/ta-report",
                    component: TaReport,
                    name: "TaReport",
                },
                {
                    path: "rts/report/processing-report",
                    component: ProcessingReport,
                    name: "ProcessingReport",
                },
                {
                    path: "rts/configurations",
                    component: Configurations,
                    name: "Configurations",
                    meta: { title: 'Recruitment Tracking System' },
                },
                {
                    path: "rts/managementCV",
                    component: ManagementCV,
                    name: "ManagementCV",
                    meta: {title: 'Recruitment Tracking System'},
                },
                {
                    path: "rts/interviews",
                    component: InterviewManagement,
                    name: "InterviewManagement",
                    meta: {title: 'Recruitment Tracking System'},
                },
                {
                    path: "rts/managementCV/detail/:id?",
                    component: ManagementCVDetail,
                    name: "ManagementCVDetail",
                    meta: { title: 'Recruitment Tracking System' },
                }
            ]
        },

        {
            path: "/",
            name: "layout",
            redirect: '/timesheet',
            component: Layout,
            beforeEnter: ifAuthenticated,
            children: [
                {
                    path: '/timesheet',
                    name: 'timesheet',
                    component: Timesheet
                },
                {
                    path: "/timesheet/request-management",
                    name: "Request Management",
                    component: RequestManagement,
                },

                {
                    path: "/timesheet/onsite-management",
                    name: "Onsite Management",
                    component: OnsiteManagement,
                },
                {
                    path: "/timesheet/overtime-management",
                    name: "Overtime Management",
                    component: OvertimeManagement,
                },
                {
                    path: "/timesheet/explanation-management",
                    component: ExplanationMangement,
                    name: "Explanation Management",
                },
                {
                    path: "/timesheet/delegation-management",
                    name: "Delegation Management",
                    component: DelegationManagement,
                },
                {
                    path: "/timesheet/abnormal-case",
                    name: "Abnormal case",
                    component: AbnormalCase,
                },
                {
                    path: "/timesheet/checkin-checkout",
                    name: "Check In-Out Time",
                    component: CheckInOut,

                },
                {
                    path: "/payroll",
                    name: "Payroll",
                    component: Payroll,
                },
                {
                    path: "/employee/employee-management",
                    name: "Employee",
                    component: Employee,
                },
                {
                    path: "/employee",
                    name: "Employee",
                    component: Employee,
                },
                // {
                //   path: "/report",
                //   component: Report,
                //   name: "Report",
                // },
                {
                    path: "/settings",
                    component: Settings,
                    name: "Seetings",
                },
                {
                    path: "/logs",
                    component: Logs,
                    name: "Logs",
                },
                // {
                //   path: "/report/detail",
                //   name: "TMS Detail",
                //   component: TMSReport,
                // },
                // {
                //   path: "/report/summary",
                //   name: "TMS Summary",
                //   component: TMSReport,
                // },
                // {
                //   path: "/report/checkTime",
                //   name: "Check In - Check Out",
                //   component: TMSReport,
                // },
            ]
        },
    ]
});
